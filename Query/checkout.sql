use megamil_claro_gn;

create table operadoras (
	id_operadora smallint primary key auto_increment,
	operadora varchar (10)
);

create table marcas_aparelhos (

	id_marca smallint primary key auto_increment,
	marca varchar(50)

);

create table modelo_aparelhos (

	id_modelo int primary key auto_increment,
	fk_marca smallint,
	modelo varchar(200),

	FOREIGN KEY (fk_marca) REFERENCES marcas_aparelhos(id_marca)

);

insert into marcas_aparelhos (id_marca,marca) values (1,'ALCATEL'),(2,'MOTOROLA'),(3,'lenovo'),(4,'SONY'),(5,'LG'),(6,'ZTE'),(7,'ASUS'),(8,'NOKIA'),(9,'XIAOMI'),(10,'SAMSUNG');

insert into modelo_aparelhos (modelo,fk_marca) values('OT-4009E PIXI 3 3,5 DUAL PRETO VIVO- TCDAL0160',1),('OT-4009E PIXI 3 3,5 DUAL PRETO - TCDAL0165',1),('ALCATEL OT-4028E PIXI 3 4,5 PRETO- TCDAL0161',1),('ALCATEL OT-5016J POP 3 5 PRETO - TCDAL0162',1),('ALCATEL OT-6039 IDOL 3 4.7 DARK GRAY - TCDAL0149',1);
insert into modelo_aparelhos (modelo,fk_marca) values('Moto G',2),('XT1506 MOTO E (2&ordf; GERA&Ccedil;&Atilde;O) DS BRANCO 8GB- TCDM0288',2),('XT1506 MOTO E (2&ordf; GERA&Ccedil;&Atilde;O) DS PRETO 8GB- TCDM0287',2),('XT1514 Moto E (2&ordf; Gera&ccedil;&atilde;o) Dual 4G Preto - TCDM0286',2),('XT1544 MOTO G&trade; (3&ordf; GERA&Ccedil;&Atilde;O) COLORS HDTV PRETO - TCDM0298',2),('XT1544 MOTO G&trade; (3&ordf; GERA&Ccedil;&Atilde;O) COLORS HDTV BRANCO - TCDM0299',2),('XT1225 MOTO MAXX PRETO - TCDM0277',2),('XT1553 MOTO G&trade; (3&ordf; GERA&Ccedil;&Atilde;O) MUSIC PRETO - TCDM0310',2),('XT1543 MOTO G&trade; (3&ordf; GERA&Ccedil;&Atilde;O) COLORS 16GB PRETO - TCDM0312',2),('XT1543 MOTO G&trade; (3&ordf; GERA&Ccedil;&Atilde;O) COLORS 16GB BRANCO - TCDM0313',2),('XT1543 MOTO G&trade; (3&ordf; GERA&Ccedil;&Atilde;O) 8GB PRETO - TCDM0327',2),('XT1543 MOTO G&trade; (3&ordf; GER.) COLORS BRANCO 16GB e 2GB RAM - TCDM0321',2),('XT1580 MOTO X&trade; FORCE 64GB PRETO - TCDM0322',2);
insert into modelo_aparelhos (modelo,fk_marca) values('VIBE C2',3),('VIBE K5',3),('VIBE A7010',3);
insert into modelo_aparelhos (modelo,fk_marca) values('XPERIA T3 BRANCO CLARO - TCDE0261',4),('E2363 XPERIA M4 AQUA DUAL PRETO - TCDE0292',4),('XPERIA C5 ULTRA DUAL PRETO - TCDE0302',4),('XPERIA C5 ULTRA DUAL BRANCO - TCDE0303',4),('D6643 XPERIA Z3 CLARO BRANCO - TCDE0271',4),('D6643 XPERIA Z3 CLARO PRETO - TCDE0270',4),('E6533 XPERIA Z3+ BRANCO - TCDE0298',4),('E6533 XPERIA Z3+ PRETO - TCDE0297',4),('E6603 XPERIA Z5 PRETO- TCDE0309',4);
insert into modelo_aparelhos (modelo,fk_marca) values('K130F.ABRAKU K4 INDIGO BLUE- TCDL0728',5),('K130F.ABRAWH K4 BRANCO- TCDL0729',5),('K430TV.ABRAKG K10 TV GOLD - TCDL0725',5),('K430TV.ABRAKU K10 TV INDIGO BLUE - TCDL0726',5),('K430TV.ABRAWH K10 TV BRANCO- TCDL0726',5),('H540T G4 Stylus HDTV Titanium- TCDL0687',5),('H540T G4 Stylus HDTV Branco- TCDL0688',5),('G2 - D805 BRANCO - TCDL0486',5),('G2 - D805 PRETO - TCDL0485',5),('D855 G3 BRANCO - TCDL0560',5),('G4 DUAL H818P ABRABD DOURADO - TCDL0669',5),('G4 DUAL H818P ABRAVK TITANIUM - TCDL0667',5),('G4 DUAL H818P ABRAWH BRANCO - TCDL0668',5),('G4 DUAL H818P ABRALB COURO MARROM - TCDL0670',5),('G4 DUAL H818P ABRALR DUAL COURO VINHO- TCDL0692',5),('G4 DUAL H818P ABRALR DUAL COURO PRETO - TCDL0680',5);
insert into modelo_aparelhos (modelo,fk_marca) values('Kis Preto - C341 - TCDZ0094',6);
insert into modelo_aparelhos (modelo,fk_marca) values('ZENFONE GO BLACK - TCDAS0011',7),('LIVE TV PRETO/ AZUL - TCDAS0013',7),('ZENFONE LASER PRETO - TCDAS0009',7),('ZENFONE LASER BRANCO - TCDAS0010',7),('ZENFONE LASER PRATA - TCDAS0016',7),('ZENFONE LASER DOURADO - TCDAS0015',7),('ZENFONE LASER VERMELHO - TCDAS0014',7),('ZENFONE 2 PRETO - TCDAS0007',7),('ZENFONE 2 VERMELHO - TCDAS0012',7),('ZENFONE 2 DOURADO - TCDAS0008',7),('ZENFONE SELFIE PRATA- TCDAS0005',7),('ZENFONE SELFIE GOLD- TCDAS0006',7);
insert into modelo_aparelhos (modelo,fk_marca) values('Lumia 535 DS Branco - TCDN0425',8),('Lumia 630 DS Branco - TCDN0379',8),('Lumia 640 Dual SIM DTV Branco - TCDN0434',8),('Lumia 640 Dual SIM DTV Preto - TCDN0433',8),('X Branco - TCDN0385',8),('X Preto - TCDN0384',8),('X Verde - TCDN0386',8);
insert into modelo_aparelhos (modelo,fk_marca) values('REDMI 2 GRAY IMP- TCDX0002',9),('REDMI 2 PRO CINZA NAC - TCDX0005',9);
insert into modelo_aparelhos (modelo,fk_marca) values('G318M GALAXY ACE 4 NEO DUOS BRANCO - TCDS0843',10),('G318M GALAXY ACE 4 NEO DUOS PRETO - TCDS0844',10),('J110L GALAXY J1 ACE 3G DUOS PRETO - TCDS0845',10),('J110L GALAXY J1 ACE 3G DUOS BRANCO - TCDS0846',10),('J120H GALAXY J1 3G DUOS 2016 BRANCO- TCDS0938',10),('J120H GALAXY J1 3G DUOS 2016 PRETO - TCDS0939',10),('J120H GALAXY J1 3G DUOS 2016 DOURADO - TCDS0940',10),('J200B SAMSUNG GALAXY J2 4G DUOS TV DOURADO- TCDS0883',10),('J200B SAMSUNG GALAXY J2 4G DUOS TV PRETO - TCDS0881',10),('J200B SAMSUNG GALAXY J2 4G DUOS TV BRANCO - TCDS0882',10),('G531H GALAXY GRAN PRIME DUOS CINZA - TCDS0857',10),('G531H GALAXY GRAN PRIME DUOS DOURADO - TCDS0859',10),('G531H GALAXY GRAN PRIME DUOS BRANCO - TCDS0858',10),('J320M GALAXY J3 4G DUOS BRANCO - TCDS0945',10),('J320M GALAXY J3 4G DUOS PRETO - TCDS0946',10),('J320M GALAXY J3 4G DUOS DOURADO - TCDS0947',10),('J500B GALAXY J5 4G DUOS DOURADO - TCDS0849',10),('J500B GALAXY J5 4G DUOS PRETO - TCDS0847',10),('J500B GALAXY J5 4G DUOS BRANCO - TCDS0848',10),('J700M GALAXY J7 4G DUOS DOURADO - TCDS0850',10),('J700M GALAXY J7 4G DUOS PRETO - TCDS0835',10),('J700M GALAXY J7 4G DUOS BRANCO - TCDS0836',10),('A510M GALAXY A5 2016 DUOS 4G - Preto - TCDS0921',10),('A510M GALAXY A5 2016 DUOS 4G - Rose - TCDS0922',10),('A510M GALAXY A5 2016 DUOS 4G - Dourado - TCDS0923',10),('A710M GALAXY A7 2016 DUOS 4G- PRETO - TCDS0924',10),('A710M GALAXY A7 2016 DUOS 4G- ROSE - TCDS0925',10),('A710M GALAXY A7 2016 DUOS 4G- DOURADO - TCDS0926',10),('G920I SAMSUNG GALAXY S6 4G DOURADO - TCDS0776',10),('G920I SAMSUNG GALAXY S6 4G PRETO - TCDS0768',10),('G920I SAMSUNG GALAXY S6 4G BRANCO - TCDS0769',10),('G925I Samsung Galaxy S6 Edge 32GB Dourado - TCDS0785',10),('G925I Samsung Galaxy S6 Edge 32GB Preto - TCDS0784',10),('G925I Samsung Galaxy S6 Edge 32GB Branco - TCDS07823',10),('N920 SAMSUNG GALAXY NOTE 5 DOURADO - TCDS0885',10),('N920 SAMSUNG GALAXY NOTE 5 PRETO - TCDS0884',10),('G928G SAMSUNG GALAXY S6 EDGE PLUS 32GB PRETO - TCDS0855',10),('G928G SAMSUNG GALAXY S6 EDGE PLUS 32GB DOURADO - TCDS0856',10),('G925I Samsung Galaxy S6 Edge 64GB Preto - TCDS0770',10),('G925I Samsung Galaxy S6 Edge 64GB Branco - TCDS0771',10),('G925I Samsung Galaxy S6 Edge 64GB Dourado - TCDS0781',10),('N910C Samsung Galaxy Note 4 Preto - TCDS0710',10),('T113NU Samsung Galaxy Tab E 7.0 WIFI - Preto- TABS0105',10),('T113NU Samsung Galaxy Tab E 7.0 WIFI - Branco- TABS0104',10),('T116BU Samsung Galaxy Tab E 7.0 3G - PRETO - TABS0107',10),('T116BU Samsung Galaxy Tab E 7.0 3G - Branco - TABS0106',10),('T560M SAMSUNG GALAXY TAB E 9.6 WIFI BRANCO- TABS0108',10),('T560M SAMSUNG GALAXY TAB E 9.6 WIFI PRETO- TABS0109',10),('T561M SAMSUNG GALAXY TAB E 9.6 3G BRANCO- TABS0111',10),('T561M SAMSUNG GALAXY TAB E 9.6 3G PRETO- TABS0110',10),('SM-P550N Samsung Galaxy Tab A Note 9.7 WIFI - Branco - TABS0116',10),('SM-P550N Samsung Galaxy Tab A Note 9.7 WIFI - Cinza - TABS0117',10),('SM-P555N Samsung Galaxy Tab A Note 9.7 4G - Branco - TABS0114',10),('SM-P555N Samsung Galaxy Tab A Note 9.7 4G - Cinza - TABS0115',10),('SM- P355M SAMSUNG GALAXY TAB A NOTE 8.0 4G CINZA- TABS0113',10),('SM- P355M SAMSUNG GALAXY TAB A NOTE 8.0 4G Branco - TABS0112',10),('SM-R7200ZKAZTO RELOGIO SAMSUNG R7200 GEAR S 2 SPORT CINZA - AOS0659',10),('SM-R7200ZWAZTO RELOGIO SAMSUNG R7200 GEAR S 2 SPORT PRATA - AOS0660',10),('SM-R7320ZKAZTO RELOGIO SAMSUNG R7320 GEAR S 2 CLASSIC PRETO - AOS0661',10),('J1 mini',10);

insert into operadoras (id_operadora,operadora)	values (1,'Claro'),(2,'Oi'),(3,'Vivo'),(4,'Tim');

create table checkout_atendimento (

	id_atendimento int primary key auto_increment,
	atendimento_telefonia boolean,
	atendimento_aberto boolean,
	atendimento_mobile boolean,
	qtd_vendedor smallint,
	loja smallint, /*Qtd. Func. (Geral)"*/
	qts_assistentes smallint,
	qts_caixas smallint,
	setor varchar(100),
	balcao_vitrine boolean,
	balcao_degustador boolean

);

create table checkout_faturamento (

	id_faturamento int primary key auto_increment,
	faturamento_pdv decimal,
	faturamento_setor decimal,
	va_f1 int,
	va_f2 int,
	va_f3 int,
	va_f4 int,
	va_f5 int,
	va_f6 int

);

create table checkout_promotor (

	id_promotor int primary key auto_increment,
	promotor varchar(100),
	tempo_pdv smallint,
	agencia varchar(100),
	supervisor varchar(100)

);

create table checkout_atendepn(

	id_atendepn int primary key auto_increment,
	apresentacao varchar(1), /*A Atende, P Parcial, N Não atende*/
	conhecimento varchar(1), /*A Atende, P Parcial, N Não atende*/
	book varchar(1) /*A Atende, P Parcial, N Não atende*/

);


create table checkout (

	id_checkout int primary key auto_increment,
	fk_pdv int,
	fk_usuario int,
	fk_atendimento int,
	fk_faturamento int,
	fk_promotor int,
	fk_atendepn int,
	data_checkout timestamp,
	endereco_exato text,
	observacoes text,

	FOREIGN KEY (fk_pdv) REFERENCES pdv(id_pdv),
	FOREIGN KEY (fk_usuario) REFERENCES usuarios(id_usuario),
	FOREIGN KEY (fk_atendimento) REFERENCES checkout_atendimento(id_atendimento),
	FOREIGN KEY (fk_faturamento) REFERENCES checkout_faturamento(id_faturamento),
	FOREIGN KEY (fk_promotor) REFERENCES checkout_promotor(id_promotor),
	FOREIGN KEY (fk_atendepn) REFERENCES checkout_atendepn(id_atendepn)
	

);

create table checkout_informacao_vendas(

	id_informacao_vendas int primary key auto_increment,
	fk_operadora smallint,
	fk_checkout int,
	venda int,
	tipo varchar(1), /*P pré-pago C Controle*/

	FOREIGN KEY (fk_operadora) REFERENCES operadoras(id_operadora),
	FOREIGN KEY (fk_checkout) REFERENCES checkout(id_checkout)

);

create table checkout_mais_vendidos(

	id_mais_vendidos int primary key auto_increment,
	fk_marca smallint,
	fk_modelo int,
	fk_checkout int,
	valor int,
	media decimal,

	FOREIGN KEY (fk_marca) REFERENCES marcas_aparelhos(id_marca),
	FOREIGN KEY (fk_modelo) REFERENCES modelo_aparelhos(id_modelo),
	FOREIGN KEY (fk_checkout) REFERENCES checkout(id_checkout)

);

create table checkout_part_time(

	id_part_time int primary key auto_increment,
	fk_operadora smallint,
	fk_checkout int,
	fixo int,
	part_time int,
	free int,

	FOREIGN KEY (fk_operadora) REFERENCES operadoras(id_operadora),
	FOREIGN KEY (fk_checkout) REFERENCES checkout(id_checkout)

);



create table checkout_campanha(

	id_campanha int primary key auto_increment,
	fk_operadora smallint,
	fk_checkout int,
	campanha varchar(100),
	produto varchar(100),
	periodo varchar(100),

	FOREIGN KEY (fk_operadora) REFERENCES operadoras(id_operadora),
	FOREIGN KEY (fk_checkout) REFERENCES checkout(id_checkout)

);

create table checkout_marketing(

	id_marketing int primary key auto_increment,
	fk_operadora smallint,
	fk_checkout int,
	material varchar(100),

	FOREIGN KEY (fk_operadora) REFERENCES operadoras(id_operadora),
	FOREIGN KEY (fk_checkout) REFERENCES checkout(id_checkout)

);


/*Input de vendas*/

create table input_vendas (

id_input_vendas int primary key auto_increment,
filial smallint,
data_input date,
pre int,
controle_facil int,
controle_giga int,
recarga int,
banda_pre int,
migracao int,
chip_vendedor int,
hc_oficial int,
hc_campo int,
fk_usuario int,
data_envio timestamp default current_timestamp,
aspectos text,
habitado boolean


);