CREATE TABLE usuarios (   

	id_usuario int NOT NULL AUTO_INCREMENT primary key,
	usuario character varying(15) NOT NULL unique,
	email character varying(40) unique,
	senha character varying(32) NOT NULL,
	login int not null unique,
	matricula int not null unique,
	telefone character varying(11),
	nome character varying(40),
	ativo boolean,

	meta_pre int,
	meta_controle int,
	meta_boleto int,
	meta_total int,
	meta_recarga int,
	meta_migracao int,

	filial_responsavel int

);

Create table grupos (  

	id_grupo int NOT NULL AUTO_INCREMENT primary key,
	nome_grupo character varying(15) NOT NULL unique,
	descricao_grupo text

);

Create table aplicacoes (  

	id_aplicacao int NOT NULL AUTO_INCREMENT primary key,
	aplicacao character varying(30) NOT NULL unique,
	titulo_aplicacao character varying(30),
	descricao_aplicacao character varying(30)

);

CREATE TABLE grupo_aplicacoes (   

	id_grupo_aplicacao int NOT NULL AUTO_INCREMENT primary key,
	fk_grupo int,
	fk_aplicacao int,
  
	FOREIGN KEY (fk_grupo) 
        REFERENCES grupos(id_grupo),
        
	FOREIGN KEY (fk_aplicacao) 
        REFERENCES aplicacoes(id_aplicacao)
);


CREATE TABLE grupo_usuarios (   

id_grupo_usuarios int NOT NULL AUTO_INCREMENT primary key,
fk_grupo int,
fk_usuario int,
  
    FOREIGN KEY (fk_grupo) 
        REFERENCES grupos(id_grupo),
	FOREIGN KEY (fk_usuario) 
        REFERENCES usuarios(id_usuario)
);
/*=========================================*/
/*Para inserir um usu·rio use: (Esta senha È 123 em padr„o MD5).*/
Insert into usuarios (usuario,email,senha,nome,telefone,ativo,matricula,login)values ('Administrador','email@admin.com','202cb962ac59075b964b07152d234b70','Usuário administrativo','11962782329',TRUE,101010,101011);
/*Criando Grupo Administrador:*/
Insert into grupos (nome_grupo,descricao_grupo) values ('Administrador', 'Acesso Total');

/*Inserindo as aplicaÁıes iniciais:*/
Insert into aplicacoes (aplicacao,titulo_aplicacao,descricao_aplicacao) values ('view_proprios_usuarios', 'Editar conta' ,'Edição de usuário.');
Insert into aplicacoes (aplicacao,titulo_aplicacao,descricao_aplicacao) values ('view_listar_grupos', 'Grupos' ,'Exibir grupos existentes.');
Insert into aplicacoes (aplicacao,titulo_aplicacao,descricao_aplicacao) values ('view_novos_usuarios', 'Novo Usuário' , 'Criar novo usuário.');
Insert into aplicacoes (aplicacao,titulo_aplicacao,descricao_aplicacao) values ('view_listar_usuarios', 'Usuários' ,'Exibir usuários existentes.');
Insert into aplicacoes (aplicacao,titulo_aplicacao,descricao_aplicacao) values ('view_editar_usuarios', 'Editar Usuários' ,'Alterar dados dos usuários e atribuir grupos.');
Insert into aplicacoes (aplicacao,titulo_aplicacao,descricao_aplicacao) values ('view_editar_grupos', 'Editar Grupo' , 'Alterar dados dos grupos e atribuir aplicações a eles.');
Insert into aplicacoes (aplicacao,titulo_aplicacao,descricao_aplicacao) values ('view_novos_grupos', 'Novo Grupo' ,'Criar novo grupo.');

Insert into aplicacoes (aplicacao,titulo_aplicacao,descricao_aplicacao) values ('view_novo_relatorio', 'Novo Relatório' ,'Criar novo relatório.');
Insert into aplicacoes (aplicacao,titulo_aplicacao,descricao_aplicacao) values ('view_novo_comunicados', 'Novo Comunicado' ,'Criar novo comunicado.');
Insert into aplicacoes (aplicacao,titulo_aplicacao,descricao_aplicacao) values ('view_novo_destaques', 'Novo Destaque' ,'Criar novo destaque.');
Insert into aplicacoes (aplicacao,titulo_aplicacao,descricao_aplicacao) values ('view_nova_concorrencia', 'Nova Consulta a Concorrência' ,'Criar Consulta a Concorrência');
Insert into aplicacoes (aplicacao,titulo_aplicacao,descricao_aplicacao) values ('view_novo_atento', 'Novo Fique Atento' ,'Criar Fique Atento.');
Insert into aplicacoes (aplicacao,titulo_aplicacao,descricao_aplicacao) values ('view_novo_pdv', 'Novo PDV' ,'Criar PDV.');

Insert into aplicacoes (aplicacao,titulo_aplicacao,descricao_aplicacao) values ('view_listar_relatorio', 'Listar todos Relatórios' ,'Listar todos Relatórios.');
Insert into aplicacoes (aplicacao,titulo_aplicacao,descricao_aplicacao) values ('view_listar_comunicados', 'Listar todos Comunicados' ,'Listar todos Comunicados.');
Insert into aplicacoes (aplicacao,titulo_aplicacao,descricao_aplicacao) values ('view_listar_destaques', 'Listar todos Destaque' ,'Listar todos Destaque.');
Insert into aplicacoes (aplicacao,titulo_aplicacao,descricao_aplicacao) values ('view_listar_concorrencia', 'Listar todas Consultas' ,'Listar todas Consultas');
Insert into aplicacoes (aplicacao,titulo_aplicacao,descricao_aplicacao) values ('view_listar_atento', 'Listar todos Fique Atento' ,'Listar todos Fique Atento.');
Insert into aplicacoes (aplicacao,titulo_aplicacao,descricao_aplicacao) values ('view_listar_pdv', 'Listar todos PDV' ,'Listar todos PDV.');

Insert into aplicacoes (aplicacao,titulo_aplicacao,descricao_aplicacao) values ('view_editar_relatorio', 'Editar Relatório' ,'Editar relatório.');
Insert into aplicacoes (aplicacao,titulo_aplicacao,descricao_aplicacao) values ('view_editar_comunicados', 'Editar Comunicado' ,'Editar comunicado.');
Insert into aplicacoes (aplicacao,titulo_aplicacao,descricao_aplicacao) values ('view_editar_destaques', 'Editar Destaque' ,'Editar destaque.');
Insert into aplicacoes (aplicacao,titulo_aplicacao,descricao_aplicacao) values ('view_editar_concorrencia', 'Editar Consulta a Concorrência' ,'Editar Consulta a Concorrência');
Insert into aplicacoes (aplicacao,titulo_aplicacao,descricao_aplicacao) values ('view_editar_atento', 'Editar Fique Atento' ,'Editar Fique Atento.');
Insert into aplicacoes (aplicacao,titulo_aplicacao,descricao_aplicacao) values ('view_editar_pdv', 'Editar PDV' ,'Editar PDV.');

Insert into aplicacoes (aplicacao,titulo_aplicacao,descricao_aplicacao) values ('view_relatorio_inputvendas', 'Relatório Input de vendas' ,'Relatório Input de vendas');
Insert into aplicacoes (aplicacao,titulo_aplicacao,descricao_aplicacao) values ('view_relatorio_checkout', 'Relatório Checkout' ,'Relatório Checkout');

Insert into aplicacoes (aplicacao,titulo_aplicacao,descricao_aplicacao) values ('view_listar_carteira', 'Carteira' ,'Carteira');
Insert into aplicacoes (aplicacao,titulo_aplicacao,descricao_aplicacao) values ('view_crud_carteira', 'Carteira' ,'Exclui e inclui');

Insert into aplicacoes (aplicacao,titulo_aplicacao,descricao_aplicacao) values ('view_listar_faq', 'Listar FAQ' ,'Listar FAQ');
Insert into aplicacoes (aplicacao,titulo_aplicacao,descricao_aplicacao) values ('view_editar_faq', 'Editar FAQ' ,'Editar FAQ');
Insert into aplicacoes (aplicacao,titulo_aplicacao,descricao_aplicacao) values ('view_novo_faq', 'Criar FAQ' ,'Criar FAQ');

Insert into aplicacoes (id_aplicacao,aplicacao,titulo_aplicacao,descricao_aplicacao) values (40,'view_listar_relatorios', 'Lista dos relatorios' ,'Lista dos relatorios');
Insert into aplicacoes (id_aplicacao,aplicacao,titulo_aplicacao,descricao_aplicacao) values (41,'view_horario_relatorios', 'Horário médio' ,'Exibe a média de horas que um GN atuou, usando seu primeiro e último checkout do dia como base.');
Insert into aplicacoes (id_aplicacao,aplicacao,titulo_aplicacao,descricao_aplicacao) values (42,'view_visitaspdvc_relatorios', 'Visitas PDVs' ,'Exibe a média das visitas a PDVs comparando os da carteira e os outros.');
Insert into aplicacoes (id_aplicacao,aplicacao,titulo_aplicacao,descricao_aplicacao) values (43,'view_visitargerais_relatorios', 'Visitas Gerais PDVs' ,'Exibe a média geral das visitas a PDVs.');
Insert into aplicacoes (id_aplicacao,aplicacao,titulo_aplicacao,descricao_aplicacao) values (44,'view_mediatam_relatorios', 'Média Telefonia, Aberto e Mobile Por GN' ,'Média Telefonia, Aberto e Mobile Por GN');

Insert into aplicacoes (id_aplicacao,aplicacao,titulo_aplicacao,descricao_aplicacao) values (45,'view_recorrencia_relatorios', 'Recorrência de visitas' ,'Informa a recorrência das visitas aos PDVs');

Insert into aplicacoes (id_aplicacao,aplicacao,titulo_aplicacao,descricao_aplicacao) values (46,'view_faturamento_relatorios', 'Faturamento' ,'Informa o faturamento por PDV');
Insert into aplicacoes (id_aplicacao,aplicacao,titulo_aplicacao,descricao_aplicacao) values (47,'view_promotor_relatorios', 'Promotores' ,'Informação dos Promotores Claro e Concorrêntes');
Insert into aplicacoes (id_aplicacao,aplicacao,titulo_aplicacao,descricao_aplicacao) values (48,'view_gnatente_relatorios', 'Atende' ,'Informação se atende, parcial ou não');
Insert into aplicacoes (id_aplicacao,aplicacao,titulo_aplicacao,descricao_aplicacao) values (49,'view_campanhas_relatorios', 'Campanhas' ,'Exibe a palavra chave usada nas campanhas');

Insert into aplicacoes (id_aplicacao,aplicacao,titulo_aplicacao,descricao_aplicacao) values (50,'view_lista_roteiro', 'Roteiros' ,'Lista de Roteiro');
Insert into aplicacoes (id_aplicacao,aplicacao,titulo_aplicacao,descricao_aplicacao) values (51,'view_aprovacao_roteiro', 'Aprovação Roteiros' ,'Tela para Aprovação');

Insert into aplicacoes (id_aplicacao,aplicacao,titulo_aplicacao,descricao_aplicacao) values (52,'view_historico_inputvendas', 'Histórico Input de vendas' ,'Histórico Input de vendas');

Insert into aplicacoes (id_aplicacao,aplicacao,titulo_aplicacao,descricao_aplicacao) values (53,'view_geral_inputvendas', 'Relatório Geral Input de vendas' ,'Relatório Geral  Input de vendas');

Insert into aplicacoes (id_aplicacao,aplicacao,titulo_aplicacao,descricao_aplicacao) values (54,'view_filial1_inputvendas', 'Relatório Filial 1 Input de vendas' ,'Relatório Filial 1 Input de vendas');

Insert into aplicacoes (id_aplicacao,aplicacao,titulo_aplicacao,descricao_aplicacao) values (55,'view_filial2_inputvendas', 'Relatório Filial 2 Input de vendas' ,'Relatório Filial 2 Input de vendas');

Insert into aplicacoes (id_aplicacao,aplicacao,titulo_aplicacao,descricao_aplicacao) values (56,'view_editar_inputvendas', 'Editar Input' ,'Editar Input');
Insert into grupo_aplicacoes (fk_grupo, fk_aplicacao) values (1,56); 

Insert into aplicacoes (id_aplicacao,aplicacao,titulo_aplicacao,descricao_aplicacao) values (57,'view_metas_editar_usuarios', 'Editar Metas' ,'Editar Metas');
Insert into grupo_aplicacoes (fk_grupo, fk_aplicacao) values (1,57); 

/*Dando permisão de acesso ao grupo Administrador:*/
Insert into grupo_aplicacoes (fk_grupo, fk_aplicacao) values (1,1); 
Insert into grupo_aplicacoes (fk_grupo, fk_aplicacao) values (1,2);
Insert into grupo_aplicacoes (fk_grupo, fk_aplicacao) values (1,3);
Insert into grupo_aplicacoes (fk_grupo, fk_aplicacao) values (1,4);
Insert into grupo_aplicacoes (fk_grupo, fk_aplicacao) values (1,5);
Insert into grupo_aplicacoes (fk_grupo, fk_aplicacao) values (1,6);
Insert into grupo_aplicacoes (fk_grupo, fk_aplicacao) values (1,7);

Insert into grupo_aplicacoes (fk_grupo, fk_aplicacao) values (1,8);
Insert into grupo_aplicacoes (fk_grupo, fk_aplicacao) values (1,9);
Insert into grupo_aplicacoes (fk_grupo, fk_aplicacao) values (1,10);
Insert into grupo_aplicacoes (fk_grupo, fk_aplicacao) values (1,11);
Insert into grupo_aplicacoes (fk_grupo, fk_aplicacao) values (1,12);
Insert into grupo_aplicacoes (fk_grupo, fk_aplicacao) values (1,13);

Insert into grupo_aplicacoes (fk_grupo, fk_aplicacao) values (1,14);
Insert into grupo_aplicacoes (fk_grupo, fk_aplicacao) values (1,15);
Insert into grupo_aplicacoes (fk_grupo, fk_aplicacao) values (1,16);
Insert into grupo_aplicacoes (fk_grupo, fk_aplicacao) values (1,17);
Insert into grupo_aplicacoes (fk_grupo, fk_aplicacao) values (1,18);
Insert into grupo_aplicacoes (fk_grupo, fk_aplicacao) values (1,19);

Insert into grupo_aplicacoes (fk_grupo, fk_aplicacao) values (1,20);
Insert into grupo_aplicacoes (fk_grupo, fk_aplicacao) values (1,21);
Insert into grupo_aplicacoes (fk_grupo, fk_aplicacao) values (1,22);
Insert into grupo_aplicacoes (fk_grupo, fk_aplicacao) values (1,23);
Insert into grupo_aplicacoes (fk_grupo, fk_aplicacao) values (1,24);
Insert into grupo_aplicacoes (fk_grupo, fk_aplicacao) values (1,25);

Insert into grupo_aplicacoes (fk_grupo, fk_aplicacao) values (1,26);
Insert into grupo_aplicacoes (fk_grupo, fk_aplicacao) values (1,27);

Insert into grupo_aplicacoes (fk_grupo, fk_aplicacao) values (1,28);
Insert into grupo_aplicacoes (fk_grupo, fk_aplicacao) values (1,29);

Insert into grupo_aplicacoes (fk_grupo, fk_aplicacao) values (1,30);
Insert into grupo_aplicacoes (fk_grupo, fk_aplicacao) values (1,31);
Insert into grupo_aplicacoes (fk_grupo, fk_aplicacao) values (1,32);

Insert into grupo_aplicacoes (fk_grupo, fk_aplicacao) values (1,40);
Insert into grupo_aplicacoes (fk_grupo, fk_aplicacao) values (1,41);
Insert into grupo_aplicacoes (fk_grupo, fk_aplicacao) values (1,42);
Insert into grupo_aplicacoes (fk_grupo, fk_aplicacao) values (1,43);
Insert into grupo_aplicacoes (fk_grupo, fk_aplicacao) values (1,44);
Insert into grupo_aplicacoes (fk_grupo, fk_aplicacao) values (1,45);
Insert into grupo_aplicacoes (fk_grupo, fk_aplicacao) values (1,46);
Insert into grupo_aplicacoes (fk_grupo, fk_aplicacao) values (1,47);
Insert into grupo_aplicacoes (fk_grupo, fk_aplicacao) values (1,48);
Insert into grupo_aplicacoes (fk_grupo, fk_aplicacao) values (1,49);

Insert into grupo_aplicacoes (fk_grupo, fk_aplicacao) values (1,50);
Insert into grupo_aplicacoes (fk_grupo, fk_aplicacao) values (1,51);

Insert into grupo_aplicacoes (fk_grupo, fk_aplicacao) values (1,52);
Insert into grupo_aplicacoes (fk_grupo, fk_aplicacao) values (1,53);
Insert into grupo_aplicacoes (fk_grupo, fk_aplicacao) values (1,54);
Insert into grupo_aplicacoes (fk_grupo, fk_aplicacao) values (1,55);




/*Adicionando o usuário ao grupo:*/
Insert into grupo_usuarios(fk_grupo, fk_usuario) values (1,1);
/*FIM DOS SQL*/;

/*tabelas Claro GN*/

create table tokens(

	id_token int not null auto_increment primary key,
	token varchar(300),
	fk_usuario int,
	aparelho int, /*1 iOS 2 Android*/
	data_registro timestamp DEFAULT CURRENT_TIMESTAMP,

	FOREIGN KEY (fk_usuario) 
        REFERENCES usuarios(id_usuario)

);

create table pdv (

	id_pdv int auto_increment primary key,
    cod_agente varchar(4),
    cnpj varchar(14),
    rede varchar(20),
    razao_social varchar(40),

	lougradouro varchar(200),
    numero_local varchar(10),
    complemento varchar (50),
    bairro varchar (30),
    cep varchar(9),
    cidade varchar(20),

    filial int,
    cod_interno_rede varchar(5),
    varejo varchar(60),
    key_account int,
    curva varchar(1),

    envio_direto boolean,
    data_criacao date,
    data_edicao date,
    fk_usuario_criou int,

    lat_pdv FLOAT(10,6),
	lng_pdv FLOAT(10,6),


    FOREIGN KEY (key_account) 
        REFERENCES usuarios(id_usuario),
    FOREIGN KEY (fk_usuario_criou) 
        REFERENCES usuarios(id_usuario)
    
);

create table destaques (

	id_destaque int not null auto_increment primary key,
	titulo varchar (140),
	url_imagem varchar(200),
	descricao varchar(500),
	data_limite date,
	data_upload timestamp DEFAULT CURRENT_TIMESTAMP
);

/*Adicionado para ser usado de filtro dos comunicados*/
create table lista_redes (

	id_rede int not null auto_increment primary key,
	rede varchar(20)

);


create table comunicados (

	id_comunicado int not null auto_increment primary key,
	titulo varchar (140),
	url_imagem varchar(200),
	descricao varchar(500),
	fk_rede int,
	data_limite date,
	data_upload timestamp DEFAULT CURRENT_TIMESTAMP,

	FOREIGN KEY (fk_rede) 
        REFERENCES lista_redes(id_rede)

);

create table relatorio (

	id_relatorio int not null auto_increment primary key,
	titulo varchar (140),
	url_imagem varchar(200),
	descricao varchar(500),
	data_limite date,
	data_upload timestamp DEFAULT CURRENT_TIMESTAMP
);

create table concorrencia (

	id_concorrencia int not null auto_increment primary key,
	titulo varchar (140),
	url_imagem varchar(200),
	descricao varchar(500),
	operadora int, /*1: Vivo, 2: Oi, 3: Tim*/
	data_limite date,
	data_upload timestamp DEFAULT CURRENT_TIMESTAMP
);

create table atento (

	id_atento int not null auto_increment primary key,
	titulo varchar (140),
	url_imagem varchar(200),
	descricao varchar(500),
	data_limite date,
	data_upload timestamp DEFAULT CURRENT_TIMESTAMP
);

/*Tabela para PDVs não localizados*/

create table pdv_revisar(

	id_pdv_revisar int not null auto_increment primary key,
	fk_checkout int,
	revisar_cod_agente varchar(4),
	revisar_rede varchar(20),
	revisar_filial varchar(10),
	revisar_cnpj varchar(20),
	revisar_lougradouro varchar(200),
	revisar_bairro varchar (30),
	revisar_cep varchar(9),
	revisar_cidade varchar(20),


	FOREIGN KEY (fk_checkout) 
        REFERENCES checkout(id_checkout)

);

create table endereco_adicional_pdv(

	id_endereco_adicional_pdv int not null auto_increment primary key,
	fk_pdv int,
	adicional_lougradouro varchar(200),
	adicional_bairro varchar (30),
	adicional_cep varchar(9),
	adicional_cidade varchar(20),
	adicional_numero_local varchar(10),

	FOREIGN KEY (fk_pdv) 
        REFERENCES pdv(id_pdv)

);

create table faq (

	id_faq int not null auto_increment primary key,
	titulo_faq varchar(140),
    descricao_faq text,
    data_faq timestamp DEFAULT CURRENT_TIMESTAMP,
    tipo_faq int /*Android ou iOS*/

);

insert into lista_redes (rede) values ('AMERICANAS');
insert into lista_redes (rede) values ('C&A');
insert into lista_redes (rede) values ('CARREFOUR');
insert into lista_redes (rede) values ('CBD');
insert into lista_redes (rede) values ('Claro Matriz SP');
insert into lista_redes (rede) values ('Compart');
insert into lista_redes (rede) values ('COOP');
insert into lista_redes (rede) values ('DON PACO');
insert into lista_redes (rede) values ('D´AVÓ');
insert into lista_redes (rede) values ('FAST SHOP');
insert into lista_redes (rede) values ('LOJAS CEM');
insert into lista_redes (rede) values ('MAGAZINE LUIZA');
insert into lista_redes (rede) values ('MAIS VALDIR');
insert into lista_redes (rede) values ('OUTROS');
insert into lista_redes (rede) values ('PERNAMBUCANAS');
insert into lista_redes (rede) values ('RIACHUELO');
insert into lista_redes (rede) values ('VIA VAREJO');
insert into lista_redes (rede) values ('WAL MART');

create table lista_razao_social (

	id_razao_social int not null auto_increment primary key,
	razao_social varchar(40)

);


create table dicionario_campanha (

	id_dicionario int not null auto_increment primary key,
	fk_campanha int,
	tipo varchar(1), /*Campanha ou Produto*/
	palavra varchar(40),

	FOREIGN KEY (fk_campanha) 
        REFERENCES checkout_campanha(id_campanha)


);


insert into lista_razao_social (razao_social) values ('DON PACO MOVEIS LTDA');
insert into lista_razao_social (razao_social) values ('LOJAS CEM SA');
insert into lista_razao_social (razao_social) values ('COOP - COOPERATIVA DE CONSUMO');
insert into lista_razao_social (razao_social) values ('VIA VAREJO S/A');
insert into lista_razao_social (razao_social) values ('MAGAZINE LUIZA S/A');
insert into lista_razao_social (razao_social) values ('LOJAS AMERICANAS S.A.');
insert into lista_razao_social (razao_social) values ('ARTHUR LUNDGREN TECIDOS S A CASAS PERNAM');
insert into lista_razao_social (razao_social) values ('C&A MODAS LTDA.');
insert into lista_razao_social (razao_social) values ('WMB COMERCIO ELETRONICO LTDA');
insert into lista_razao_social (razao_social) values ('CIA BRASILEIRA DE DISTRIBUIÇAO S/A');
insert into lista_razao_social (razao_social) values ('CARREFOUR COMERCIO E INDUSTRIA LTDA');
insert into lista_razao_social (razao_social) values ('FAST SHOP S.A');
insert into lista_razao_social (razao_social) values ('LOJAS RIACHUELO SA');
insert into lista_razao_social (razao_social) values ('DAVO SUPERMERCADOS LTDA');
insert into lista_razao_social (razao_social) values ('SE SUPERMERCADOS LTDA.');
insert into lista_razao_social (razao_social) values ('WAL MART BRASIL LTDA');
insert into lista_razao_social (razao_social) values ('BF UTILIDADES DOMESTICAS LTDA');
insert into lista_razao_social (razao_social) values ('BERTHO BONO LOJA DE DEPARTAMENTOS LTDA');
insert into lista_razao_social (razao_social) values ('J MAHFUZ LTDA');
insert into lista_razao_social (razao_social) values ('MEGA COLOR LTDA - EPP');
insert into lista_razao_social (razao_social) values ('NOVA PONTOCOM COMERCIO ELETRONICO S.A.');
insert into lista_razao_social (razao_social) values ('SIRI COMERCIO E SERVICOS LTDA.');
insert into lista_razao_social (razao_social) values ('NOVASOC COMERCIAL LTDA');
insert into lista_razao_social (razao_social) values ('COMERCIAL DE ALIMENTOS CARREFOUR LTDA.');
insert into lista_razao_social (razao_social) values ('Claro Matriz SP');
insert into lista_razao_social (razao_social) values ('Compart');

create view view_badge_hoje as
select 

(select count(*) from atento where data_upload >= current_date()) as atento,
(select count(*) from comunicados where data_upload >= current_date()) as comunicados,
(select count(*) from concorrencia where data_upload >= current_date()) as concorrencia,
(select count(*) from destaques where data_upload >= current_date()) as destaques,
(select count(*) from relatorio where data_upload >= current_date()) as relatorio;

select 
/*(date_format(max(data_checkout), '%H:%i') - date_format(min(data_checkout), '%H:%i')) horas*/
distinct(date(cc.data_checkout)), 
(select TIMEDIFF(max(data_checkout),min(data_checkout)) from checkout where date(data_checkout) = (date(cc.data_checkout)))
from checkout cc
where date(data_checkout) <= current_date()
and date(data_checkout) > '00/00/0000'
and fk_usuario = 1;


/*ROTEIRO*/
create table roteiro(

	id_roteiro int not null auto_increment primary key,
	fk_usuario int,
	inicio_roteiro date,
	fim_roteiro date,
	data_solicitacao timestamp DEFAULT CURRENT_TIMESTAMP,
	data_aprovacao date,
	status int,
	horas_total text,
	km_total text,
	obs_roteiro text,

	FOREIGN KEY (fk_usuario) 
        REFERENCES usuarios(id_usuario)

);

create table roteiro_enderecos (

	id_roteiro_endereco int not null auto_increment primary key,
	fk_roteiro int,
	endereco_google text,
	km_google int,
	tempo_google int,
	tempo_adicional int,

	FOREIGN KEY (fk_roteiro) 
        REFERENCES roteiro(id_roteiro)

);

create table carteira_gn (

	id_carteira int not null auto_increment primary key,
	fk_pdv int,
	fk_usuario int,

	FOREIGN KEY (fk_pdv) REFERENCES pdv(id_pdv),
	FOREIGN KEY (fk_usuario) REFERENCES usuarios(id_usuario)

);
/*****************************FUNÇÃO USADA PARA CALCULO DE DISTANCIA********************************/
/*SUBSTITUIR FLOAT POR DOUBLE MELHOROU A PRECISÃO A CURTA DISTÂNCIA*/
delimiter //
CREATE FUNCTION Geo(lat_ini DOUBLE(18,10), lon_ini DOUBLE(18,10),lat_fim DOUBLE(18,10), lon_fim DOUBLE(18,10))
RETURNS DOUBLE(18,10)
NOT DETERMINISTIC
BEGIN
DECLARE Theta DOUBLE(18,10);
DECLARE Dist DOUBLE(18,10);
DECLARE Miles DOUBLE(18,10);
DECLARE kilometers DOUBLE(18,10);

SET Theta = lon_ini - lon_fim;
SET Dist  = SIN(RADIANS(lat_ini)) * SIN(RADIANS(lat_fim)) +  COS(RADIANS(lat_ini)) * COS(RADIANS(lat_fim)) * COS(RADIANS(Theta));
SET Dist  = ACOS(Dist);
SET Dist  = DEGREES(Dist);
SET Miles = Dist * 60 * 1.1515;
SET kilometers = Miles * 1.609344;

RETURN kilometers;

END;//
delimiter ;















