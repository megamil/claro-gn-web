<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

	public function index($dados = null) {

		if ($this->session->userdata('online')) { /*Valida se o usuário já estava logado*/

			$dados = array (

				'tela' => 'Dashboard | CLARO GN',
				'aviso' => array (
					'tipo' => 'sucesso', /*Escolha entre: erro, aviso e sucesso*/
					'titulo' =>	'Seja Bem Vindo!', /*Mensagem que ficará no título do aviso*/
					'mensagem' => 'Bem vindo '.$this->session->userdata('nome').'.', /*Mensagem do aviso*/
					'badge' => $this->model_webservice->badges()
				)

			);

			$this->load->view('estrutura/header',$dados);
			$this->load->view('home');
			$this->load->view('estrutura/footer');

		} else { /*Caso não esteja logado*/
			
			if(is_null($dados)){ // se não existir um aviso

				$dados = array (

					'tela' => 'Login',
					'aviso' => array (

						'tipo' => '', 
						'titulo' =>	'', 
						'mensagem' => '', 
						'badge' => ''
					) //aviso

				); //dados

			}//if interno

			$this->load->view('index',$dados);

		} //else

	}

	public function login() {

		$login = array ( /*Campos que foram digitados*/

			'usuario' => $this->input->post('user'),
			'senha' => md5($this->input->post('pass'))

			);

		$usuario = $this->model_seguranca->validar($login);

		if($usuario) { /*Valida dados*/

			$secao = array ( /*Inicia a seção*/

				'usuario' => $this->input->post('user'),
				'nome' => $usuario->row()->nome,
				'online' => true,
				'id_usuario' => $usuario->row()->id_usuario

			);

			$this->session->set_userdata($secao);

			$this->index();

		} else { /*Carrega a tela para logar*/

			$dados = array (

				'tela' => 'Login',
				'aviso' => array (
					'tipo' => 'erro',
					'titulo' =>	'Erro de acesso!',
					'mensagem' => 'Usuário ou senha inválidos!',
					'badge' => $this->model_webservice->badges()
				)

			);

			$this->index($dados);

		}

	}

	public function Sair() {

		$nome = $this->session->userdata('usuario');

		$this->session->sess_destroy();

			$dados = array (

				'tela' => 'Login',
				'aviso' => array (
					'tipo' => 'sucesso',
					'titulo' =>	'Até logo!',
					'mensagem' => 'Usuário '.$nome.' saiu do sistema com sucesso!.',
					'badge' => $this->model_webservice->badges()
				)

			);

			$this->load->view('index',$dados);

	}

	public function privacidade(){

		$this->load->view('termos_privacidade');

	}

	public function erro() {

		if (!$this->session->userdata('online')) {

			$dados = array (

					'tela' => 'Login',
					'aviso' => array (

						'tipo' => 'aviso', 
						'titulo' =>	'Offline!', 
						'mensagem' => 'Você precisa estar online para acessar o sistema!',
						'badge' => $this->model_webservice->badges() 
					) //aviso

			); //dados

			$this->index($dados);

		} else {

			$dados = array (

					'tela' => 'Erro 404',
					'aviso' => array (
						'tipo' => 'erro',
						'titulo' =>	'Erro 404',
						'mensagem' => 'Página não encontrada!',
						'badge' => $this->model_webservice->badges()
					)
				);

			$this->load->view('estrutura/header',$dados);
			$this->load->view('erros/pagina_nao_encontrada');
			$this->load->view('estrutura/footer');

		}

	}

	public function main_function(){

		$q1 = $this->input->get('query');
		$q2 = $this->input->post('query');

		$s2 = $this->input->get('s');
		$s1 = $this->input->post('s');

		if (isset($q1)) {
			$q = $q1;
		} else {
			$q = $q2;
		}

		if (isset($s1)) {
			$s = true;
		} else if (isset($s2)) {
			$s = true;
		} else  {
			$s = false;
		}

		header('Content-Type: application/json; charset=utf-8');
		header("access-control-allow-origin: *");

		echo json_encode($this->model_webservice->mainFunction($q,$s));

	}


	public function redirecionar(){

		if (!$this->session->userdata('online')) {

			$dados = array (

					'tela' => 'Login',
					'aviso' => array (

						'tipo' => 'aviso', 
						'titulo' =>	'Offline!', 
						'mensagem' => 'Você precisa estar online para acessar o sistema!',
						'badge' => $this->model_webservice->badges() 
					) //aviso

			); //dados

			$this->index($dados);

		} else {

			//Verifica se for passado algum argumento inicio.
			$continuar = true; //Verifica se deve continuar procurando por argumentos na URL.
			$i = 4; //Começa no argumento 4 da url.
			$where = array(); //Armazena os argumentos.

			while($continuar) { // Enquanto tiver argumento irá armazenando no array.

				if($this->uri->segment($i) != '') { //confirma um argumento na posição i.
					
					$where[]  = $this->uri->segment($i); //Adicionar argumento no array.
					$i++; //Incrementa para procurar outro argumento na proxima passagem pelo if.

				} else {

					$continuar = false; //para o while.

				}

			}
			//Verifica se for passado algum argumento fim.

			$tela = $this->depois('-', $this->uri->segment(3));

			if($this->model_seguranca->acesso($tela) == 1 and $this->session->userdata('online')) { //validar acesso

				$endereco = str_replace("-", "/",$this->uri->segment(3)); /*troca o - por uma identificação de diretório*/

				$funcao = $this->depois('_', $tela); //Pega do nome da view a função correspondente.

				$model = explode('_',$funcao);

				$model = 'model_'.($model[count($model)-1]); //Pega do nome da função o model correspondente.

				if(method_exists($this->$model, $funcao)) {

					$dados_iniciais = $this->$model->$funcao($where);

				} else {

					$dados_iniciais = '';

				}

				$dados = array (

					'tela' => $this->model_seguranca->titulo($tela)->titulo_aplicacao, //Carregar do banco de dados.
					'dados' => $dados_iniciais,
					'aviso' => array (
						'tipo' => $this->session->flashdata('tipo'), /*Escolha entre: erro, aviso e sucesso*/
						'titulo' =>	$this->session->flashdata('titulo'), /*Mensagem que ficará no título do aviso*/
						'mensagem' => $this->session->flashdata('mensagem'), /*Mensagem do aviso*/
						'badge' => $this->model_webservice->badges()
					)
				);

				$this->load->view('estrutura/header',$dados);
				$this->load->view($endereco);
				$this->load->view('estrutura/footer');

			} else { // Redirecionar para tela com aviso que não possui permissão de acesso.

				$dados = array (

					'tela' => 'Sem permissão de acesso.', //Carregar do banco de dados.
					'aviso' => array (
						'tipo' => 'aviso',
						'titulo' =>	'Sem permissão de acesso.',
						'mensagem' => 'O Grupo do seu usuário não possuí permissão para acessar está página.',
						'badge' => $this->model_webservice->badges()
					)

				);

				$this->load->view('estrutura/header',$dados);
				$this->load->view('erros/sem_permissao');
				$this->load->view('estrutura/footer');

			}

		}

	}

	function depois ($deste, $texto) { // Pegar restante da String.
	    
	        if (!is_bool(strpos($texto, $deste)))
	        return substr($texto, strpos($texto,$deste)+strlen($deste));
	    
	}

}

?>