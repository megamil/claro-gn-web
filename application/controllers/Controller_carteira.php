<?php defined('BASEPATH') OR exit('No direct script access allowed');

	class controller_carteira extends CI_Controller {

		public function buscar() {

			$dados = $this->model_carteira->listar_por_cnpj($this->input->post('cnpj'));

			echo '<table class="mdl-data-table mdl-js-data-table mdl-shadow--8dp">
			  <thead>
			    <tr>
			      <th>Usuários</th>
			      <th>Na carteira</th>
			      <th>Filial do GN</th>
			      <th>PDVs</th>
			    </tr>
			  </thead>
			  <tbody>';

	    		 foreach ($dados as $usuario) {
			    	echo '<tr>';
					echo '<td>'.$usuario->nome.'</td>';
					echo '<td>'.$usuario->total.'</td>';
					echo '<td>'.$usuario->filial_responsavel.'</td>';
					echo '<td>'.anchor('main/redirecionar/editar-view_crud_carteira/'.$usuario->id_usuario, '<i class="material-icons">mode_edit</i>PDVs', array('class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent', 'title' => 'PDVs Associados.', 'alt' => 'PDVs Associados..', 'style' => 'margin-top: -7px;')).'</td>';
					echo '</tr>';
				} 

			  echo '</tbody>
			</table>';


		}

		public function novo(){

			$dados = array (

				'fk_pdv' => $this->input->post('id_pdv'),
				'fk_usuario' => $this->input->post('id_usuario')

			);


			//Verifica se não existe já o registro
			if($this->model_carteira->associar($dados)) {
				$this->session->set_flashdata('tipo','sucesso');
				$this->session->set_flashdata('titulo','Adicionado com sucesso');
				$this->session->set_flashdata('mensagem','Adicionado com sucesso');
			} else {
				$this->session->set_flashdata('tipo','erro');
				$this->session->set_flashdata('titulo','Erro ao inserir');
				$this->session->set_flashdata('mensagem','Registro já existe.');
			}

			

			redirect('main/redirecionar/editar-view_crud_carteira/'.$this->input->post('id_usuario'));


		}

		public function remover() {

			$id_pdv = $this->uri->segment(3);
			$id_usuario = $this->uri->segment(4);

			$this->model_carteira->deletar_vinculo($id_pdv,$id_usuario);

			$this->session->set_flashdata('tipo','sucesso');
			$this->session->set_flashdata('titulo','Editado com sucesso');
			$this->session->set_flashdata('mensagem','Editado com sucesso');

			redirect('main/redirecionar/editar-view_crud_carteira/'.$this->uri->segment(4));

		}


	}