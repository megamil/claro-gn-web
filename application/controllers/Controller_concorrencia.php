<?php defined('BASEPATH') OR exit('No direct script access allowed');

	class controller_concorrencia extends CI_Controller {


		public function novo() {

			if($_FILES['imagemdescricao']['name'] != ''){ //Caso seja somente um upload para descrição

				$config['upload_path'] = $_SERVER['DOCUMENT_ROOT'].'/'.base_url().'upload/concorrencia';
				$config['allowed_types'] = 'gif|jpg|png';

				//Tira os ascentos e espaços etc..
				$nome = strtolower(strtr(utf8_decode(trim($_FILES['imagemdescricao']['name'])), utf8_decode("áàãâéêíóôõúüñçÁÀÃÂÉÊÍÓÔÕÚÜÑÇ"),"aaaaeeiooouuncAAAAEEIOOOUUNC-"));

				$nome = str_replace(array('.','?',' '), '-', $nome);

				//Quando tirar o ponto da extenção.
				$nome = str_replace('-png', '.png', $nome);
				$nome = str_replace('-gif', '.gif', $nome);
				$nome = str_replace('-jpg', '.jpg', $nome);

				//Gera um nome único para o arquivo
				$arquivo = basename(time().uniqid(md5($nome)).$nome);

				$config['file_name'] = $arquivo;
				
				$this->load->library('upload', $config);
			
				$this->upload->do_upload('imagemdescricao');

				//set_flashdata
				$campos = array (

					'titulo_concorrencia' => $this->input->post('titulo_concorrencia'),
					'data_limite' => $this->input->post('data_limite'),
					'operadora' => $this->input->post('operadora'),
					'descricao_concorrencia' => $this->input->post('descricao_concorrencia').'<img src="'.base_url().'upload/concorrencia/'.$arquivo.'">'

				);

				$this->session->set_flashdata($campos);

				redirect('main/redirecionar/cadastro-view_nova_concorrencia');

			} else {

				$data = explode('/',$this->input->post('data_limite'));
				$data_limite = $data[2].'-'.$data[1].'-'.$data[0];

				$dados = array (
					//replace pq o tinymce troca o local por ../
					'descricao' => str_replace('../../', base_url(), $this->input->post('descricao_concorrencia')),
					'data_limite' => $data_limite,
					'operadora' => $this->input->post('operadora'),
					'titulo' => $this->input->post('titulo_concorrencia')
				);

				$id = $this->model_concorrencia->novo_Registro($dados);

				$config['upload_path'] = $_SERVER['DOCUMENT_ROOT'].'/'.base_url().'upload/concorrencia';
				$config['allowed_types'] = 'gif|jpg|png';
				$config['file_name'] = 'concorrencia'.$id;
				
				$this->load->library('upload', $config);
			
				if (!$this->upload->do_upload('imagem')) {

					$campos = array (

						'titulo_concorrencia' => $this->input->post('titulo_concorrencia'),
						'data_limite' => $this->input->post('data_limite'),
						'operadora' => $this->input->post('operadora'),
						'descricao_concorrencia' => $this->input->post('descricao_concorrencia')

					);

					$this->session->set_flashdata($campos);

					$this->model_concorrencia->deletar_Registro($id);

					$this->session->set_flashdata('tipo','erro');
					$this->session->set_flashdata('titulo','Erro no upload.');
					$this->session->set_flashdata('mensagem',$this->upload->display_errors());

					if(is_null($this->input->post('mobile'))){
						redirect('main/redirecionar/cadastro-view_nova_concorrencia/');
					} else {
						$array = array ("code" => "0", "message" => "Falha");
						echo json_encode ( $array );  
					}


				} else {

					$arquivo = $this->upload->data();

					$dados = array (

						'url_imagem' => $arquivo['file_name'],
						'id_concorrencia' => $id
						
					);

					$this->model_concorrencia->atualizar_Registro($dados);

					$this->session->set_flashdata('tipo','sucesso');
					$this->session->set_flashdata('titulo','Sucesso.');
					$this->session->set_flashdata('mensagem',"Upload realizado com sucesso!");

					if(is_null($this->input->post('mobile'))){

						$title = "Novo concorrencia";
						$message = $this->input->post('titulo_concorrencia'); // Usa o título da publicação como mensagem no PUSH
						$retornar = "cadastro-view_nova_concorrencia";

						redirect('controller_notificacoes/notificando?title='.$title.'&message='.$message.'&retornar='.$retornar);

					} else {
						$array = array ("code" => "1", "message" => "Sucesso");
						echo json_encode ( $array );  
					}
					
					
				}

			} //else upload de imagem para descrição


		}

	public function editar() {
		if($_FILES['imagemdescricao']['name'] != ''){ //Caso seja somente um upload para descrição

				$config['upload_path'] = $_SERVER['DOCUMENT_ROOT'].'/'.base_url().'upload/concorrencia';
				$config['allowed_types'] = 'gif|jpg|png';

				//Tira os ascentos e espaços etc..
				$nome = strtolower(strtr(utf8_decode(trim($_FILES['imagemdescricao']['name'])), utf8_decode("áàãâéêíóôõúüñçÁÀÃÂÉÊÍÓÔÕÚÜÑÇ"),"aaaaeeiooouuncAAAAEEIOOOUUNC-"));

				$nome = str_replace(array('.','?',' '), '-', $nome);

				//Quando tirar o ponto da extenção.
				$nome = str_replace('-png', '.png', $nome);
				$nome = str_replace('-gif', '.gif', $nome);
				$nome = str_replace('-jpg', '.jpg', $nome);

				//Gera um nome único para o arquivo
				$arquivo = basename(time().uniqid(md5($nome)).$nome);

				$config['file_name'] = $arquivo;
				
				$this->load->library('upload', $config);
			
				$this->upload->do_upload('imagemdescricao');

				$campos = array (

					'titulo_concorrencia' => $this->input->post('titulo_concorrencia'),
					'data_limite' => $this->input->post('data_limite'),
					'operadora' => $this->input->post('operadora'),
					'descricao_concorrencia' => $this->input->post('descricao_concorrencia').'<img src="'.base_url().'upload/concorrencia/'.$arquivo.'">'

				);

				$this->session->set_flashdata($campos);

				redirect('main/redirecionar/editar-view_editar_concorrencia/'.$this->input->post('id_concorrencia'));

			} else {

				$data = explode('/',$this->input->post('data_limite'));
				$data_limite = $data[2].'-'.$data[1].'-'.$data[0];

				if($_FILES['imagem']['name'] == "") {

					$dados = array (
						'id_concorrencia' => $this->input->post('id_concorrencia'),
						'data_limite' => $data_limite,
						'operadora' => $this->input->post('operadora'),
						//replace pq o tinymce troca o local por ../, só que 3vezes
						'descricao' => str_replace('../../../', base_url(), $this->input->post('descricao_concorrencia')),
						'titulo' => $this->input->post('titulo_concorrencia')
					);

				} else {

					$arquivoAtual = "upload/concorrencia/".$this->input->post('imagemAtual');
					if (file_exists($arquivoAtual)) {
						unlink($arquivoAtual);
					}

					$config['upload_path'] = $_SERVER['DOCUMENT_ROOT'].'/'.base_url().'upload/concorrencia';
					$config['allowed_types'] = 'gif|jpg|png';
					$config['file_name'] = 'concorrencia'.$this->input->post('id_concorrencia');
					
					$this->load->library('upload', $config);
				
					if (!$this->upload->do_upload('imagem')) {

						$campos = array (

							'titulo_concorrencia' => $this->input->post('titulo_concorrencia'),
							'data_limite' => $this->input->post('data_limite'),
							'operadora' => $this->input->post('operadora'),
							'descricao_concorrencia' => $this->input->post('descricao_concorrencia')

						);

						$this->session->set_flashdata($campos);

						$this->session->set_flashdata('tipo','erro');
						$this->session->set_flashdata('titulo','Erro no upload, imagem anterior deletada.');
						$this->session->set_flashdata('mensagem',$this->upload->display_errors());

						redirect('main/redirecionar/editar-view_editar_concorrencia/'.$this->input->post('id_concorrencia'));

					} else {

						$arquivo = $this->upload->data();
						$data = explode('/',$this->input->post('data_limite'));
						$data_limite = $data[2].'-'.$data[1].'-'.$data[0];

						$dados = array (
							'id_concorrencia' => $this->input->post('id_concorrencia'),
							'descricao' => $this->input->post('descricao_concorrencia'),
							'titulo' => $this->input->post('titulo_concorrencia'),
							'operadora' => $this->input->post('operadora'),
							'data_limite' => $data_limite,
							'url_imagem' => $arquivo['file_name']
						);

					}

				}

				if($this->model_concorrencia->update($dados)) {

					$this->session->set_flashdata('tipo','sucesso');
					$this->session->set_flashdata('titulo','Sucesso.');
					$this->session->set_flashdata('mensagem',"Registro atualizado com sucesso!");

					redirect('main/redirecionar/editar-view_editar_concorrencia/'.$this->input->post('id_concorrencia'));

				} else {

					$this->session->set_flashdata('tipo','erro');
					$this->session->set_flashdata('titulo','Falha ao atualizar.');
					$this->session->set_flashdata('mensagem',"Erro ao atualizar.");

					redirect('main/redirecionar/editar-view_editar_concorrencia/'.$this->input->post('id_concorrencia'));

				}

			} //else upload de imagem para descrição


		}


		public function deletar() {

			if(!$this->model_concorrencia->del($this->uri->segment(3))) {

				$this->session->set_flashdata('tipo','erro');
				$this->session->set_flashdata('titulo','Erro.');
				$this->session->set_flashdata('mensagem',"Falha ao deletar!");
				redirect('main/redirecionar/listar-view_listar_concorrencia/');

			} else {

				$arquivoAtual = "upload/concorrencia/".$this->uri->segment(4);
				if (file_exists($arquivoAtual)) {
					unlink($arquivoAtual);
				}

				$this->session->set_flashdata('tipo','sucesso');
				$this->session->set_flashdata('titulo','Sucesso.');
				$this->session->set_flashdata('mensagem',"Registro deletado com sucesso!");
				redirect('main/redirecionar/listar-view_listar_concorrencia/');

			}

		}


	}