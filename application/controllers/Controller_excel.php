<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/Sao_Paulo');

	class Controller_excel extends CI_Controller {

		public function excel_roteiro(){

			$dados = $this->model_roteiro->lista_roteiro();

			$html = '<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" />
			<table>
	  <thead>
	    <tr>
	      <th>GN</th>
	      <th>Início</th>
	      <th>Fim</th>
	      <th>Data da Solicitação</th>
	      <th>Status</th>
	      <th>KM Total</th>
	      <th>Total em Horas</th>
	    </tr>
	  </thead>
	  <tbody>';
	    
	    foreach ($dados as $roteiro) {
	    	$html .= '<tr>';
			$html .= '<td>'.$roteiro->nome.'</td>';
			$html .= '<td>'.$roteiro->inicio.'</td>';
			$html .= '<td>'.$roteiro->fim.'</td>';
			$html .= '<td>'.$roteiro->solicitacao.'</td>';
			$html .= '<td>'.$roteiro->status.'</td>';
			$html .= '<td>'.$roteiro->km.'</td>';
			$html .= '<td></td>';
			$html .= '</tr>';
		} 

	  $html .='</tbody>
	</table>';


			$arquivo = 'excel_roteiros.xls';

			// Configurações header para forçar o download
			header ("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
			header ("Cache-Control: must-revalidate, post-check=0, pre-check=0"); 
			header ("Content-Transfer-Encoding: binary"); 
			header ("Content-Type: application/vnd.ms-excel"); 
			header ("Expires: 0"); 
			header ("Content-Disposition: attachment; filename=\"{$arquivo}\"");
			header ("Content-Description: PHP Generated Data");

			// Envia o conteúdo do arquivo
			chr(255).chr(254).iconv("UTF-8", "UTF-16LE//IGNORE", $html); 
			echo $html;
			exit;

		}

		public function excel_visitaspdvc(){

		$data_filtro = $this->input->get('data_filtro');
		$de = $this->input->get('de');
		$ate = $this->input->get('ate');
		$filial_filtro = $this->input->get('filial_filtro');
		$gn_filtro = $this->input->get('gn_filtro');

		$nomeGN = $this->input->get('nomeGN'); //Usado somente para leitura

		$filtro1 = $this->model_relatorios->ajax_visitaspdvc_filtro($data_filtro,$de,$ate,$filial_filtro,$gn_filtro);

		$media_checkout = '';
		$total_checkout = '';
		$fora_carteira = '';
		$filiais1 = '';
		$filiais2 = '';

		$total_filiais = 0;

		$perc_fora_carteira = 0;
		$perc_filial1 = 0;
		$perc_filial2 = 0;

		foreach ($filtro1 as $filtro) {
			$media_checkout = $filtro->media_checkout;
			$total_checkout = $filtro->total_checkout;
			$fora_carteira = $filtro->fora_carteira;
			$filiais1 = $filtro->filiais1;
			$filiais2 = $filtro->filiais2;
		}

		if ($fora_carteira > 0 && $total_checkout > 0) {
			$perc_fora_carteira = (($fora_carteira * 100) / $total_checkout);
		}

		$total_filiais = $filiais1 + $filiais2;

		if ($total_filiais > 0 && $filiais1 > 0) {
			$perc_filial1 = (($filiais1 * 100) / $total_filiais);
		}

		if ($total_filiais > 0 && $filiais2 > 0) {
			$perc_filial2 = (($filiais2 * 100) / $total_filiais);
		}


		// Definimos o nome do arquivo que será exportado
		$arquivo = 'excel_visitaspdvc.xls';
		
		// Criamos uma tabela HTML com o formato da planilha
		$html = '<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" /><table><tbody>';

		$html .= '<tr><td><strong>CLARO GN - RELATÓRIO DE VISITAS MÉDIAS</strong></td></tr><tr></tr>';

		$html .= '<tr><td><strong>PERÍODO</strong></td>  <td>'.$de.' - '.$ate.'</td></tr>';

		$html .= '<tr><td><strong>FILIAIS</strong></td>  <td>'.$this->label_filial($filial_filtro).'</td></tr>';
		  
		  
		  $html .= '<tr><td><strong>GN</strong></td>  <td>'.strtoupper($nomeGN).'</td></tr>';


		  $html .= '<tr></tr>';

		  $html .= '
		  <tr>

		  	<td align="center"><strong>MÉDIA DE VISITAS</strong></td>
		  	<td align="center"><strong>VISITAS FORA DA CARTEIRA</strong></td>
		  	<td align="center"><strong>TOTAL DE VISITAS F1 + F2</strong></td>

		  </tr>
		  <tr>

		  	<td align="center">'.$media_checkout.'</td>
		  	<td align="center">'.$fora_carteira.'</td>
		  	<td align="center">'.$total_checkout.'</td>

		  </tr>
		  <tr>

		  	<td align="center">-</td>
		  	<td align="center">'.number_format($perc_fora_carteira, 2, '.', ',').'%</td>
		  	<td align="center">-</td>

		  </tr>';



		  $html .= '
		  <tr>

		  	<td align="center"><strong>VISITAS TOTAIS FILIAL 1</strong></td>
		  	<td align="center"><strong>VISITAS TOTAIS FILIAL 2</strong></td>

		  </tr>
		   <tr>

		  	<td align="center">'.$filiais1.'</td>
		  	<td align="center">'.$filiais2.'</td>

		  </tr>
		   <tr>

		  	<td align="center">'.number_format($perc_filial1, 2, '.', ',').'%</td>
		  	<td align="center">'.number_format($perc_filial2, 2, '.', ',').'%</td>

		  </tr>';
		  
		  

		  $html .= '
		  <tr></tr>
		  </tbody>
			</table>';		  


		$html .= '<table>
		  <thead>
		    <tr>
		        <th align="left">GN</th>
				<th align="left">Média do GN por período</th>
				<th align="left">Fora da Carteira</th>
		    </tr>
		  </thead>
		  <tbody>';
			

			$resposta = $this->model_relatorios->ajax_visitaspdvc_filtro_lista($data_filtro,$de,$ate,$filial_filtro,$gn_filtro);

			    foreach ($resposta as $visitar_gn) {
			    	$html .= '<tr>';
					$html .= '<td align="left">'.$visitar_gn->usuario.'</td>';
					$html .= '<td align="left">'.$visitar_gn->total.'</td>';
					$html .= '<td align="left">'.$visitar_gn->fora.'</td>';
					$html .= '</tr>';
				} 

			  $html .= "
			  </tbody>
			</table>";

			$resposta = $this->model_relatorios->ajax_visitaspdvc_detalhes_excel($data_filtro,$de,$ate);

			$html .= '<strong>DETALHES DE VISITAÇÃO - "GN"</strong>
			<table class="mdl-data-table mdl-js-data-table mdl-shadow--8dp semDataTable" style="width: 100%">
			<thead>
				<tr>
					<th>CNPJ</th>
					<th>GN</th>
					<th>REDE</th>
					<th>LOUGRADOURO</th>
					<th>CURVA</th>
					<th>CARTEIRA</th>
				</tr>
			</thead>
			<tbody>';

			foreach ($resposta as $valor) {
				$html .= '<tr>';
				$html .= '<td>'.$valor->cnpj.'</td>';
				$html .= '<td>'.$valor->usuario.'</td>';
				$html .= '<td>'.$valor->rede.'</td>';
				$html .= '<td>'.$valor->lougradouro.'</td>';
				$html .= '<td>'.$valor->curva.'</td>';
				if ($valor->fora == 1) {
					$html .= '<td>FORA DA CARTEIRA (PDVs)</td>';
				} else {
					$html .= '<td>CARTEIRA</td>';
				}
				$html .= '</tr>';
			}

			$html .= '<tr></tr>';

				$html .= '<tr><td><strong>DATA DA EXPORTAÇÃO</strong></td> <td><strong>'.date('d/m/Y').'</strong></td></tr>';
				$html .= '<tr><td>HORA DA EXPORTAÇÃO</td> <td>'.date('H:i:s').'</td></tr>';	

			$html .= '		<tr></tr>
						<tr></tr>
					  	<tr></tr>
					  	<tr></tr>
					  	<tr></tr>
					  	<tr></tr> 
					  	<tr></tr>
					  	<tr></tr>
					  	<tr><td>powered by</td></tr>
					  	<tr><td>NAVILLE MARKETING LTDA</td></tr>
			  		</tbody>
			  	</table>';

			// Configurações header para forçar o download
			header ("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
			header ("Cache-Control: must-revalidate, post-check=0, pre-check=0"); 
			header ("Content-Transfer-Encoding: binary"); 
			header ("Content-Type: application/vnd.ms-excel"); 
			header ("Expires: 0"); 
			header ("Content-Disposition: attachment; filename=\"{$arquivo}\"");
			header ("Content-Description: PHP Generated Data");

			// Envia o conteúdo do arquivo
			chr(255).chr(254).iconv("UTF-8", "UTF-16LE//IGNORE", $html); 
			echo $html;
			exit;

		}

	public function excel_recorrencia(){

		$cnpj = str_replace(array('.','/','-'),'', $this->input->get('cnpj'));
		$de = $this->input->get('de');
		$ate = $this->input->get('ate');
		$curva_filtro = $this->input->get('curva_filtro');
		$filial_filtro = $this->input->get('filial_filtro');
		$gn_filtro = $this->input->get('gn_filtro');

		$nomeGN = strtoupper($this->input->get('nomeGN')); //Usado somente para leitura
		
		$dados = $this->model_relatorios->recorrencia_ajax($cnpj,$de,$ate,$curva_filtro,$filial_filtro,$gn_filtro);

		// Definimos o nome do arquivo que será exportado
		$arquivo = 'excel_recorrencia.xls';
		
		// Criamos uma tabela HTML com o formato da planilha
		$html = '<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" />';

		$html .= '<table>
			<tbody>
			<tr><td>RANKING de VISITAS a PDVs</td></tr>
			<tr></tr>
			<tr><td><strong>PERÍODO </strong></td> <td>'.$de.' - '.$ate.'</td></tr>
			<tr><td><strong>CURVA </strong></td> <td>'.$this->label_curva($curva_filtro).'</td></tr>
			<tr><td><strong>FILIAL </strong></td> <td>'.$this->label_filial($filial_filtro).'</td></tr>
			<tr><td><strong>GN </strong></td> <td>'.$nomeGN.'</td></tr>
			<tr><td><strong>CNPJ </strong></td> <td>'.$this->input->get('cnpj').'</td></tr>
			<tr></tr>
			</tbody>
		</table>';

		$dados = $this->model_relatorios->recorrencia_ajax($cnpj,$de,$ate,$curva_filtro,$filial_filtro,$gn_filtro);

		$html .= '<table class="mdl-data-table mdl-js-data-table mdl-shadow--8dp semDataTable" style="width: 100%;">
		<thead>
			<tr>
				<th width="20%" class="mdl-data-table__cell--non-numeric">REDE</th>
				<th width="20%" class="mdl-data-table__cell--non-numeric">LOJAS ATIVAS</th>
				<th width="15%" class="mdl-data-table__cell--non-numeric">LOJAS ATENDIDAS</th>
				<th width="15%" class="mdl-data-table__cell--non-numeric">VISITAS REALIZADAS</th>
				<th width="15%" class="mdl-data-table__cell--non-numeric">% REDE ATENDIDA</th>
				<th width="15%" class="mdl-data-table__cell--non-numeric">% REDE NÃO ATENDIDA</th>
			</tr>
		</thead>
	  <tbody>';

	  foreach ($dados as $pdvs) {
		  	$html .='<tr>';
		  		$html .='<td width="20%" class="mdl-data-table__cell--non-numeric">'.$pdvs->rede.'</td>';

		  		$html .='<td width="20%" class="mdl-data-table__cell--non-numeric listarAtivas" style="cursor: pointer" rede="'.$pdvs->rede.'">
		  		'.$pdvs->lojas_ativas.'</td>';

		  		$html .='<td width="15%" class="mdl-data-table__cell--non-numeric listarAtendidas" style="cursor: pointer" rede="'.$pdvs->rede.'">
		  		'.$pdvs->lojas_atendidas.'</td>';

		  		$html .='<td width="15%" class="mdl-data-table__cell--non-numeric">'.$pdvs->visitas_realizadas.'</td>';

		  		$perc_atendidas = $this->percentual($pdvs->lojas_ativas,$pdvs->lojas_atendidas);

		  		$html .='<td width="15%" class="mdl-data-table__cell--non-numeric">'.number_format($perc_atendidas, 2, ',', ' ').'%</td>';
		  		$html .='<td width="15%" class="mdl-data-table__cell--non-numeric">'.number_format((100 - $perc_atendidas), 2, ',', ' ').'%</td>';
		  	$html .='</tr>';
	  }

	  $rede = $this->model_relatorios->recorrencia_ativos(
	  			"", /*REDE NÃO É PASSADA, PARA EXIBIR DE TODAS*/
				$cnpj,
				$curva_filtro,
				$filial_filtro
				);

			$html .='<tr></tr>
				<tr>
					<td>
						<strong>LOJAS ATIVAS</strong>
					</td>
				</tr>
				<tr>
					<th width="20%">REDE</th>
					<th width="60%">LOUGRADOURO</th>
					<th width="20%">CURVA</th>
				</tr>';

			foreach ($rede as $value) {
				$html .='<tr>';

				$html .='<td>'.$value->rede.'</td>';
				$html .='<td>'.$value->endereco.'</td>';
				$html .='<td>'.$value->curva.'</td>';

				$html .='</tr>';
			}


		$rede = $this->model_relatorios->recorrencia_atendidos("",$cnpj,$de,$ate,$curva_filtro,$filial_filtro,$gn_filtro);


			$html .='<tr></tr>
				<tr>
					<td>
						<strong>LOJAS ATENDIDAS</strong>
					</td>
				</tr>
				<tr>
					<th width="20%">REDE</th>
					<th width="40%">LOUGRADOURO</th>
					<th width="20%">GN</th>
					<th width="20%">CURVA</th>
				</tr>';

			foreach ($rede as $value) {
				$html .='<tr>';

				$html .='<td>'.$value->rede.'</td>';
				$html .='<td>'.$value->endereco.'</td>';
				$html .='<td>'.$value->usuario.'</td>';
				$html .='<td>'.$value->curva.'</td>';

				$html .='</tr>';
			}


		$html .= '<tr></tr>';
		$html .= '<tr><td><strong>DATA DA EXPORTAÇÃO</strong></td> <td><strong>'.date('d/m/Y').'</strong></td></tr>';
		$html .= '<tr><td>HORA DA EXPORTAÇÃO</td> <td>'.date('H:i:s').'</td></tr>';	

			$html .= "
				<tr></tr>
				<tr></tr>
			  	<tr></tr>
			  	<tr></tr>
			  	<tr></tr>
			  	<tr></tr> 
			  	<tr></tr>
			  	<tr></tr>
			  	<tr><td>powered by</td></tr>
			  	<tr><td>NAVILLE MARKETING LTDA</td></tr>

			  </tbody>
			</table>";


		// Configurações header para forçar o download
			header ("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
			header ("Cache-Control: must-revalidate, post-check=0, pre-check=0"); 
			header ("Content-Transfer-Encoding: binary"); 
			header ("Content-Type: application/vnd.ms-excel"); 
			header ("Expires: 0"); 
			header ("Content-Disposition: attachment; filename=\"{$arquivo}\"");
			header ("Content-Description: PHP Generated Data");

			// Envia o conteúdo do arquivo
			chr(255).chr(254).iconv("UTF-8", "UTF-16LE//IGNORE", $html); 
			echo $html;
			exit;

	}

	public function percentual($total = null, $quantidade = null){
			
			if ($quantidade > 0 && $total > 0) {
				return (($quantidade * 100) / $total);
			} else {
				return 0;
			}


		}

	public function horario_excel(){

		$nomeGN = strtoupper($this->input->get('nomeGN')); //Usado somente para leitura

		$de = $this->input->get('de');
		$ate = $this->input->get('ate');
		$gn_filtro = $this->input->get('gn_filtro');

		$consulta = $this->model_relatorios->ajaxHorarioE($gn_filtro,$de,$ate);
			
		$horas['filial1'] = $consulta['tempo_medio_f1'];
		$horas['filial2'] = $consulta['tempo_medio_f12'];
		$horas['media'] = $consulta['tempo_medio'];
		$horas['min'] = $consulta['entrou_media'];
		$horas['max'] = $consulta['saiu_media'];


		// Definimos o nome do arquivo que será exportado
		$arquivo = 'excel_horario.xls';
		
		// Criamos uma tabela HTML com o formato da planilha
		$html = '<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" />';

		$html .= '<table>
			<tbody>
			<tr><td>ANALÍSE de HORAS TRABALHADAS</td></tr>
			<tr></tr>
			<tr><td><strong>PERÍODO </strong></td> <td>'.$de.' - '.$ate.'</td></tr>
			<tr><td><strong>GN </strong></td> <td>'.$nomeGN.'</td></tr>
			<tr></tr>
			<tr><td><strong>MÉDIA FILIAL 1 </strong></td> <td>'.$horas['filial1'].'</td></tr>
			<tr><td><strong>MÉDIA FILIAL 2  </strong></td> <td>'.$horas['filial2'].'</td></tr>
			<tr><td><strong>MÉDIA PRIMEIRO CHECK-IN </strong></td> <td>'.$horas['min'].'</td></tr>
			<tr><td><strong>MÉDIA ÚLTIMO CHECK-OUT </strong></td> <td>'.$horas['max'].'</td></tr>
			<tr><td><strong>MÉDIA DE HORAS TRABALHADAS  </strong></td> <td>'.$horas['media'].'</td></tr>
			<tr><td><strong>CHECKOUT FILIAL 1</strong></td> <td>'.$horas['media'].'</td></tr>
			<tr><td><strong>CHECKOUT FILIAL 2</strong></td> <td>'.$horas['media'].'</td></tr>
			<tr><td><strong>CHECKOUT TOTAL</strong></td> <td>'.$horas['media'].'</td></tr>
			<tr></tr>
			<tr><td><strong>GN </strong></td> <td><strong>MÉDIA 1° CHECK-IN </strong></td> <td><strong>MÉDIA ÚLTIMO CHECK-OUT </strong></td> <td><strong>MÉDIA DE HORAS TRABALHADAS </strong></td>
			<td><strong>CHECKOUT FILIAL 1 </strong></td>
			<td><strong>CHECKOUT FILIAL 2 </strong></td>
			<td><strong>CHECKOUT TOTAL </strong></td></tr>';

		$html .= $this->model_relatorios->lista_horas($de,$ate);


		$html .= '<tr></tr>';
		$html .= '<tr><td><strong>DATA DA EXPORTAÇÃO</strong></td> <td><strong>'.date('d/m/Y').'</strong></td></tr>';
		$html .= '<tr><td>HORA DA EXPORTAÇÃO</td> <td>'.date('H:i:s').'</td></tr>';	

			$html .= "
				<tr></tr>
				<tr></tr>
			  	<tr></tr>
			  	<tr></tr>
			  	<tr></tr>
			  	<tr></tr> 
			  	<tr></tr>
			  	<tr></tr>
			  	<tr><td>powered by</td></tr>
			  	<tr><td>NAVILLE MARKETING LTDA</td></tr>

			  </tbody>
			</table>";

		// Configurações header para forçar o download
		header ("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
		header ("Cache-Control: must-revalidate, post-check=0, pre-check=0"); 
		header ("Content-Transfer-Encoding: binary"); 
		header ("Content-Type: application/vnd.ms-excel"); 
		header ("Expires: 0"); 
		header ("Content-Disposition: attachment; filename=\"{$arquivo}\"");
		header ("Content-Description: PHP Generated Data");

		// Envia o conteúdo do arquivo
		chr(255).chr(254).iconv("UTF-8", "UTF-16LE//IGNORE", $html); 
		echo $html;
		exit;

	}


	public function excel_mediatam(){


		$nomeGN = strtoupper($this->input->get('nomeGN')); //Usado somente para leitura
		$cnpj = str_replace(array('.','/','-'),'', $this->input->get('cnpj'));
		$de = $this->input->get('de');
		$ate = $this->input->get('ate');
		$gn_filtro = $this->input->get('gn_filtro');

		$total = $this->model_relatorios->mediatam_ajax($gn_filtro,"",$de,$ate,$cnpj);
		$filial1 = $this->model_relatorios->mediatam_ajax($gn_filtro," and filial = 1",$de,$ate,$cnpj);
		$filial2 = $this->model_relatorios->mediatam_ajax($gn_filtro," and filial = 2",$de,$ate,$cnpj);

		// Definimos o nome do arquivo que será exportado
		$arquivo = 'excel_mediatam.xls';
		
		// Criamos uma tabela HTML com o formato da planilha
		$html = '<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" />';


		$html .= '<table>
			<tbody>
			<tr><td>ANÁLISE de PDVs (Tipo de PDVs)</td></tr>
			<tr></tr>
			<tr><td><strong>PERÍODO </strong></td> <td>'.$de.' - '.$ate.'</td></tr>
			<tr><td><strong>GN </strong></td> <td>'.$nomeGN.'</td></tr>
			<tr><td><strong>CNPJ </strong></td> <td>'.$this->input->get('cnpj').'</td></tr>
			<tr></tr>
			<tr><td><strong>Total Geral </strong></td> <td>'.$total->row()->total.'</td></tr>
			<tr><td><strong>Total FILIAL 1  </strong></td> <td>'.$filial1->row()->total.'</td></tr>
			<tr><td><strong>Total FILIAL 2 </strong></td> <td>'.$filial2->row()->total.'</td></tr>';

		$html .= '<tr></tr>';

		$html .= '<tr>
					<td></td>
					<td>TOTAL</td>
					<td>FILIAL 1</td>
					<td>FILIAL 2</td>
				</tr>';

		$html .= '<tr>
					<td>MOBILE</td>
					<td>'.$total->row()->mobile.'</td>
					<td>'.$filial1->row()->mobile.'</td>
					<td>'.$filial2->row()->mobile.'</td>
				</tr>';

		$html .= '<tr>
					<td>TELEFONIA</td>
					<td>'.$total->row()->telefonia.'</td>
					<td>'.$filial1->row()->telefonia.'</td>
					<td>'.$filial2->row()->telefonia.'</td>
				</tr>';

		$html .= '<tr>
					<td>ABERTO</td>
					<td>'.$total->row()->aberto.'</td>
					<td>'.$filial1->row()->aberto.'</td>
					<td>'.$filial2->row()->aberto.'</td>
				</tr>';

		$html .= '<tr></tr>';
		$html .= '<tr><td><strong>DATA DA EXPORTAÇÃO</strong></td> <td><strong>'.date('d/m/Y').'</strong></td></tr>';
		$html .= '<tr><td>HORA DA EXPORTAÇÃO</td> <td>'.date('H:i:s').'</td></tr>';	

			$html .= "
				<tr></tr>
				<tr></tr>
			  	<tr></tr>
			  	<tr></tr>
			  	<tr></tr>
			  	<tr></tr> 
			  	<tr></tr>
			  	<tr></tr>
			  	<tr><td>powered by</td></tr>
			  	<tr><td>NAVILLE MARKETING LTDA</td></tr>

			  </tbody>
			</table>";

		// Configurações header para forçar o download
		header ("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
		header ("Cache-Control: must-revalidate, post-check=0, pre-check=0"); 
		header ("Content-Transfer-Encoding: binary"); 
		header ("Content-Type: application/vnd.ms-excel"); 
		header ("Expires: 0"); 
		header ("Content-Disposition: attachment; filename=\"{$arquivo}\"");
		header ("Content-Description: PHP Generated Data");

		// Envia o conteúdo do arquivo
		chr(255).chr(254).iconv("UTF-8", "UTF-16LE//IGNORE", $html); 
		echo $html;
		exit;

	}

	public function excel_promotor(){


		$nomeGN = strtoupper($this->input->get('nomeGN')); //Usado somente para leitura
		$fk_usuario = $this->input->post('id_usuario');
		$de = $this->input->get('de');
		$ate = $this->input->get('ate');
		$filial = $this->input->get('filial_filtro');
		$curva = $this->input->get('curva');
		$gn_filtro = $this->input->get('gn_filtro');

		// Definimos o nome do arquivo que será exportado
		$arquivo = 'excel_promotor.xls';
		
		// Criamos uma tabela HTML com o formato da planilha
		$html = '<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" />';


		$html .= '<table>
			<tbody>
			<tr><td>PROMOTORES CLARO em PDVs X CONCORRÊNCIA (VIVO, TIM E OI)</td></tr>
			<tr></tr>
			<tr><td><strong>PERÍODO </strong></td> <td>'.$de.' - '.$ate.'</td></tr>
			<tr><td><strong>GN </strong></td> <td>'.$nomeGN.'</td></tr>
			<tr><td><strong>FILIAIS</strong></td>  <td>'.$this->label_filial($filial).'</td></tr>
			<tr><td><strong>CURVA</strong></td>  <td>'.$this->label_curva($curva).'</td></tr>
			<tr></tr>';

		$resultado = $this->model_relatorios->promotor_ajax($de,$ate,$filial,$fk_usuario,$curva);
		$dados = array();

		$dados['claro_pt'] = 0;
		$dados['claro_free'] = 0;
		$dados['claro_fixo'] = 0;
		
		$dados['oi_pt'] = 0;
		$dados['oi_free'] = 0;
		$dados['oi_fixo'] = 0;
		
		$dados['vivo_pt'] = 0;
		$dados['vivo_free'] = 0;
		$dados['vivo_fixo'] = 0;
		
		$dados['tim_pt'] = 0;
		$dados['tim_free'] = 0;
		$dados['tim_fixo'] = 0;

		foreach ($resultado as $operadora) {
				
			if ($operadora->operadora == 'claro') {
				$dados['claro_pt'] = $operadora->pt;
				$dados['claro_free'] = $operadora->free;
				$dados['claro_fixo'] = $operadora->fixo;
			}

			if ($operadora->operadora == 'oi') {
				$dados['oi_pt'] = $operadora->pt;
				$dados['oi_free'] = $operadora->free;
				$dados['oi_fixo'] = $operadora->fixo;
			}

			if ($operadora->operadora == 'vivo') {
				$dados['vivo_pt'] = $operadora->pt;
				$dados['vivo_free'] = $operadora->free;
				$dados['vivo_fixo'] = $operadora->fixo;
			}

			if ($operadora->operadora == 'tim') {
				$dados['tim_pt'] = $operadora->pt;
				$dados['tim_free'] = $operadora->free;
				$dados['tim_fixo'] = $operadora->fixo;
			}

		}

		$html .= '<tr>
					<td></td>
					<td>CLARO</td>
					<td>OI</td>
					<td>VIVO</td>
					<td>TIM</td>
				</tr>';

		$html .= '<tr>
					<td>PART TIME</td>
					<td>'.$dados['claro_pt'].'</td>
					<td>'.$dados['oi_pt'].'</td>
					<td>'.$dados['vivo_pt'].'</td>
					<td>'.$dados['tim_pt'].'</td>
				</tr>';

		$html .= '<tr>
					<td>FREE</td>
					<td>'.$dados['claro_free'].'</td>
					<td>'.$dados['oi_free'].'</td>
					<td>'.$dados['vivo_free'].'</td>
					<td>'.$dados['tim_free'].'</td>
				</tr>';

		$html .= '<tr>
					<td>FIXO</td>
					<td>'.$dados['claro_fixo'].'</td>
					<td>'.$dados['oi_fixo'].'</td>
					<td>'.$dados['vivo_fixo'].'</td>
					<td>'.$dados['tim_fixo'].'</td>
				</tr>';

		$html .= '<tr>
					<td>Total de Promotores</td>
					<td>'.($dados['claro_pt'] + $dados['claro_free'] + $dados['claro_fixo']).'</td>
					<td>'.($dados['oi_pt'] + $dados['oi_free'] + $dados['oi_fixo']).'</td>
					<td>'.($dados['vivo_pt'] + $dados['vivo_free'] + $dados['vivo_fixo']).'</td>
					<td>'.($dados['tim_pt'] + $dados['tim_free'] + $dados['tim_fixo']).'</td>
				</tr>';

		$html .= '<tr></tr>';
		$html .= '<tr><td><strong>DATA DA EXPORTAÇÃO</strong></td> <td><strong>'.date('d/m/Y').'</strong></td></tr>';
		$html .= '<tr><td>HORA DA EXPORTAÇÃO</td> <td>'.date('H:i:s').'</td></tr>';	

			$html .= "
				<tr></tr>
				<tr></tr>
			  	<tr></tr>
			  	<tr></tr>
			  	<tr></tr>
			  	<tr></tr> 
			  	<tr></tr>
			  	<tr></tr>
			  	<tr><td>powered by</td></tr>
			  	<tr><td>NAVILLE MARKETING LTDA</td></tr>

			  </tbody>
			</table>";

		// Configurações header para forçar o download
		header ("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
		header ("Cache-Control: must-revalidate, post-check=0, pre-check=0"); 
		header ("Content-Transfer-Encoding: binary"); 
		header ("Content-Type: application/vnd.ms-excel"); 
		header ("Expires: 0"); 
		header ("Content-Disposition: attachment; filename=\"{$arquivo}\"");
		header ("Content-Description: PHP Generated Data");

		// Envia o conteúdo do arquivo
		chr(255).chr(254).iconv("UTF-8", "UTF-16LE//IGNORE", $html); 
		echo $html;
		exit;

	}

	public function excel_campanhas(){

		$cnpj = str_replace(array('.','/','-'),'', $this->input->get('cnpj'));
		$de = $this->input->get('de');
		$ate = $this->input->get('ate');
		$filial = $this->input->get('filial_filtro');

		// Definimos o nome do arquivo que será exportado
		$arquivo = 'excel_campanhas.xls';
		
		// Criamos uma tabela HTML com o formato da planilha
		$html = '<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" />';


		$html .= '<table>
			<tbody>
			<tr><td>5 PALAVRAS QUE MAIS SE REPETEM</td></tr>
			<tr></tr>
			<tr><td><strong>PERÍODO </strong></td> <td>'.$de.' - '.$ate.'</td></tr>
			<tr><td><strong>CNPJ </strong></td> <td>'.$this->input->get('cnpj').'</td></tr>
			<tr><td><strong>FILIAIS</strong></td>  <td>'.$this->label_filial($filial).'</td></tr>
			<tr></tr>';

		$dados = $this->model_relatorios->campanha_ajax($cnpj,$de,$ate,$filial);

		$html .= '<tr>
					<td>CLARO CAMPANHA</td>';

		foreach ($dados['campanhac']->result() as $value) {
			$html .= '<td><strong>'.$value->palavra.'</strong> <br> com ('.$value->quantidade.') Repetições</td>';
		}

		$html .= '</tr>';

		$html .= '<tr>
					<td>CLARO PRODUTOS</td>';

		foreach ($dados['produtoc']->result() as  $value) {
			$html .= '<td><strong>'.$value->palavra.'</strong> <br> com ('.$value->quantidade.') Repetições</td>';
		}

		$html .= '<tr>
					<td>OI CAMPANHA</td>';

		foreach ($dados['campanhao']->result() as $value) {
			$html .= '<td><strong>'.$value->palavra.'</strong> <br> com ('.$value->quantidade.') Repetições</td>';
		}

		$html .= '</tr>';

		$html .= '<tr>
					<td>OI PRODUTOS</td>';

		foreach ($dados['produtoo']->result() as  $value) {
			$html .= '<td><strong>'.$value->palavra.'</strong> <br> com ('.$value->quantidade.') Repetições</td>';
		}

		$html .= '<tr>
					<td>VIVO CAMPANHA</td>';

		foreach ($dados['campanhav']->result() as $value) {
			$html .= '<td><strong>'.$value->palavra.'</strong> <br> com ('.$value->quantidade.') Repetições</td>';
		}

		$html .= '</tr>';

		$html .= '<tr>
					<td>VIVO PRODUTOS</td>';

		foreach ($dados['produtov']->result() as  $value) {
			$html .= '<td><strong>'.$value->palavra.'</strong> <br> com ('.$value->quantidade.') Repetições</td>';
		}

		$html .= '<tr>
					<td>TIM CAMPANHA</td>';

		foreach ($dados['campanhat']->result() as $value) {
			$html .= '<td><strong>'.$value->palavra.'</strong> <br> com ('.$value->quantidade.') Repetições</td>';
		}

		$html .= '</tr>';

		$html .= '<tr>
					<td>TIM PRODUTOS</td>';

		foreach ($dados['produtot']->result() as  $value) {
			$html .= '<td><strong>'.$value->palavra.'</strong> <br> com ('.$value->quantidade.') Repetições</td>';
		}



		$html .= '</tr>';


		$html .= '<tr></tr>';
		$html .= '<tr><td><strong>DATA DA EXPORTAÇÃO</strong></td> <td><strong>'.date('d/m/Y').'</strong></td></tr>';
		$html .= '<tr><td>HORA DA EXPORTAÇÃO</td> <td>'.date('H:i:s').'</td></tr>';	

			$html .= "
				<tr></tr>
				<tr></tr>
			  	<tr></tr>
			  	<tr></tr>
			  	<tr></tr>
			  	<tr></tr> 
			  	<tr></tr>
			  	<tr></tr>
			  	<tr><td>powered by</td></tr>
			  	<tr><td>NAVILLE MARKETING LTDA</td></tr>

			  </tbody>
			</table>";

		// Configurações header para forçar o download
		header ("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
		header ("Cache-Control: must-revalidate, post-check=0, pre-check=0"); 
		header ("Content-Transfer-Encoding: binary"); 
		header ("Content-Type: application/vnd.ms-excel"); 
		header ("Expires: 0"); 
		header ("Content-Disposition: attachment; filename=\"{$arquivo}\"");
		header ("Content-Description: PHP Generated Data");

		// Envia o conteúdo do arquivo
		chr(255).chr(254).iconv("UTF-8", "UTF-16LE//IGNORE", $html); 
		echo $html;
		exit;

	}

	public function excel_gnatende(){


		$de = str_replace('-', '/', $this->input->get('de'));
		$ate = str_replace('-', '/', $this->input->get('ate'));
		$filial = $this->input->get('filial_filtro');

		// Definimos o nome do arquivo que será exportado
		$arquivo = 'excel_gnatende.xls';
		
		// Criamos uma tabela HTML com o formato da planilha
		$html = '<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" />';

		$html .= '<table>
			<tbody>
			<tr><td>ANÁLISE Promotor no PDV</td></tr>
			<tr></tr>
			<tr><td><strong>PERÍODO </strong></td> <td>'.$de.' - '.$ate.'</td></tr>
			<tr><td><strong>FILIAIS</strong></td>  <td>'.$this->label_filial($filial).'</td></tr>
			<tr></tr>';

		$dados = $this->model_relatorios->atende_ajax($filial,$de,$ate);

			function resultado($valor = null,$total = null){

				if($total > 0 && $valor > 0){
					$perc = (($valor * 100) / $total);
					return number_format($perc, 2, '.', '');
				} else {
					return 0;
				}

			}

			$html .=  '
			        <tr align="center">
			          <td widtd="40%"  align="center"></td>
			          <td widtd="20%" colspan="3" align="center">Atende</td>
			          <td widtd="20%" colspan="3" align="center">Atende Parcialmente</td>
			          <td widtd="20%" colspan="3" align="center">Não Atende</td>
			        </tr>

			        <tr  align="center">
			          <td colspan="1"  align="center"></td>

			          <td align="center">A</td>
			          <td align="center">B</td>
			          <td align="center">C</td>

			          <td align="center">A</td>
			          <td align="center">B</td>
			          <td align="center">C</td>

			          <td align="center">A</td>
			          <td align="center">B</td>
			          <td align="center">C</td>
			        </tr>';

			$html .= '<tr>
			          <td>APRESENTAÇÃO</td>
			          <td align="center">'.resultado($dados->qts_apresentacao_a,$dados->total).'%</td>
			          <td align="center">'.resultado($dados->qts_apresentacao_b,$dados->total).'%</td>
			          <td align="center">'.resultado($dados->qts_apresentacao_c,$dados->total).'%</td>
			          <td align="center">'.resultado($dados->qts_apresentacao_a_p,$dados->total).'%</td>
			          <td align="center">'.resultado($dados->qts_apresentacao_b_p,$dados->total).'%</td>
			          <td align="center">'.resultado($dados->qts_apresentacao_c_p,$dados->total).'%</td>
			          <td align="center">'.resultado($dados->qts_apresentacao_a_n,$dados->total).'%</td>
			          <td align="center">'.resultado($dados->qts_apresentacao_b_n,$dados->total).'%</td>
			          <td align="center">'.resultado($dados->qts_apresentacao_c_n,$dados->total).'%</td>
			        </tr>

			        <tr>
			          <td>BOOK</td>
			          <td align="center">'.resultado($dados->qts_book_a,$dados->total).'%</td>
			          <td align="center">'.resultado($dados->qts_book_b,$dados->total).'%</td>
			          <td align="center">'.resultado($dados->qts_book_c,$dados->total).'%</td>
			          <td align="center">'.resultado($dados->qts_book_a_p,$dados->total).'%</td>
			          <td align="center">'.resultado($dados->qts_book_b_p,$dados->total).'%</td>
			          <td align="center">'.resultado($dados->qts_book_c_p,$dados->total).'%</td>
			          <td align="center">'.resultado($dados->qts_book_a_n,$dados->total).'%</td>
			          <td align="center">'.resultado($dados->qts_book_b_n,$dados->total).'%</td>
			          <td align="center">'.resultado($dados->qts_book_c_n,$dados->total).'%</td>
			        </tr>

			        <tr>
			          <td>CONHECIMENTO</td>
			          <td align="center">'.resultado($dados->qts_conhecimento_a,$dados->total).'%</td>
			          <td align="center">'.resultado($dados->qts_conhecimento_b,$dados->total).'%</td>
			          <td align="center">'.resultado($dados->qts_conhecimento_c,$dados->total).'%</td>
			          <td align="center">'.resultado($dados->qts_conhecimento_a_p,$dados->total).'%</td>
			          <td align="center">'.resultado($dados->qts_conhecimento_b_p,$dados->total).'%</td>
			          <td align="center">'.resultado($dados->qts_conhecimento_c_p,$dados->total).'%</td>
			          <td align="center">'.resultado($dados->qts_conhecimento_a_n,$dados->total).'%</td>
			          <td align="center">'.resultado($dados->qts_conhecimento_b_n,$dados->total).'%</td>
			          <td align="center">'.resultado($dados->qts_conhecimento_c_n,$dados->total).'%</td>
			        </tr>';


		$html .= '<tr></tr>';
		$html .= '<tr><td><strong>DATA DA EXPORTAÇÃO</strong></td> <td><strong>'.date('d/m/Y').'</strong></td></tr>';
		$html .= '<tr><td>HORA DA EXPORTAÇÃO</td> <td>'.date('H:i:s').'</td></tr>';	

			$html .= "
				<tr></tr>
				<tr></tr>
			  	<tr></tr>
			  	<tr></tr>
			  	<tr></tr>
			  	<tr></tr> 
			  	<tr></tr>
			  	<tr></tr>
			  	<tr><td>powered by</td></tr>
			  	<tr><td>NAVILLE MARKETING LTDA</td></tr>

			  </tbody>
			</table>";

		// Configurações header para forçar o download
		header ("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
		header ("Cache-Control: must-revalidate, post-check=0, pre-check=0"); 
		header ("Content-Transfer-Encoding: binary"); 
		header ("Content-Type: application/vnd.ms-excel"); 
		header ("Expires: 0"); 
		header ("Content-Disposition: attachment; filename=\"{$arquivo}\"");
		header ("Content-Description: PHP Generated Data");

		// Envia o conteúdo do arquivo
		chr(255).chr(254).iconv("UTF-8", "UTF-16LE//IGNORE", $html); 
		echo $html;
		exit;

	}

	
	public function excel_faturamento(){


		$cnpj = str_replace(array('.','/','-'),'', $this->input->get('cnpj'));
		$de = $this->input->get('de');
		$ate = $this->input->get('ate');

		$dados = $this->model_relatorios->faturamento_ajax($de,$ate,$cnpj);

		// Definimos o nome do arquivo que será exportado
		$arquivo = 'excel_faturamento.xls';
		
		// Criamos uma tabela HTML com o formato da planilha
		$html = '<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" />';

		$html .= '<table>
			<tbody>
			<tr><td>MÉDIA DE FATURAMENTO</td></tr>
			<tr></tr>
			<tr><td><strong>PERÍODO </strong></td> <td>'.$de.' - '.$ate.'</td></tr>
			<tr><td><strong>CNPJ 	</strong></td>  <td>'.$this->input->get('cnpj').'</td></tr>
			<tr></tr>
			<tr><td><strong>FATURAMENTO </strong></td>  <td>'.$dados->row()->faturamento.'</td></tr>
			<tr><td><strong>CURVA 		</strong></td>  <td>'.$dados->row()->curva.'</td></tr>
			<tr></tr>';

		$html .= '<tr>
						<th width="80%">Venda de Aparelhos - Faixa</th>
						<th width="20%">Total</th>
					</tr>
				 <tr>
				 	<th width="80%" class="mdl-data-table__cell--non-numeric">até R$ 500,00</th>
					<th width="20%">'.$dados->row()->va_f1.'</th>
				 </tr>

				  <tr>
				 	<th width="80%" class="mdl-data-table__cell--non-numeric">de R$ 501,00 a R$ 999,00</th>
					<th width="20%">'.$dados->row()->va_f2.'</th>
				 </tr>

				  <tr>
				 	<th width="80%" class="mdl-data-table__cell--non-numeric">de R$ 1.000,00 a R$ 1.499,00</th>
					<th width="20%">'.$dados->row()->va_f3.'</th>
				 </tr>

				  <tr>
				 	<th width="80%" class="mdl-data-table__cell--non-numeric">de R$ 1.500,00 a R$ 1.999,00</th>
					<th width="20%">'.$dados->row()->va_f4.'</th>
				 </tr>

				  <tr>
				 	<th width="80%" class="mdl-data-table__cell--non-numeric">de R$ 2.000,00 a R$ 2.499,00</th>
					<th width="20%">'.$dados->row()->va_f5.'</th>
				 </tr>

				  <tr>
				 	<th width="80%" class="mdl-data-table__cell--non-numeric">acima de R$ 2.500,00</th>
					<th width="20%">'.$dados->row()->va_f6.'</th>
				 </tr>

				 <tr>
				 	<th width="80%" class="mdl-data-table__cell--non-numeric">Total de vendas</th>
					<th width="20%">'.($dados->row()->va_f1 + $dados->row()->va_f2 + $dados->row()->va_f3 + $dados->row()->va_f4 + $dados->row()->va_f5 + $dados->row()->va_f6).'</th>
				 </tr>
				 
				 ';	



		$html .= '<tr></tr>';
		$html .= '<tr><td><strong>DATA DA EXPORTAÇÃO</strong></td> <td><strong>'.date('d/m/Y').'</strong></td></tr>';
		$html .= '<tr><td>HORA DA EXPORTAÇÃO</td> <td>'.date('H:i:s').'</td></tr>';	

			$html .= "
				<tr></tr>
				<tr></tr>
			  	<tr></tr>
			  	<tr></tr>
			  	<tr></tr>
			  	<tr></tr> 
			  	<tr></tr>
			  	<tr></tr>
			  	<tr><td>powered by</td></tr>
			  	<tr><td>NAVILLE MARKETING LTDA</td></tr>

			  </tbody>
			</table>";

		// Configurações header para forçar o download
		header ("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
		header ("Cache-Control: must-revalidate, post-check=0, pre-check=0"); 
		header ("Content-Transfer-Encoding: binary"); 
		header ("Content-Type: application/vnd.ms-excel"); 
		header ("Expires: 0"); 
		header ("Content-Disposition: attachment; filename=\"{$arquivo}\"");
		header ("Content-Description: PHP Generated Data");

		// Envia o conteúdo do arquivo
		chr(255).chr(254).iconv("UTF-8", "UTF-16LE//IGNORE", $html); 
		echo $html;
		exit;

	}

	public function label_filial($filial = null){

		if ($filial == "") {
			return "AMBAS";
		} else {
			return $filial;
		}

	}

	public function label_curva($curva = null){

		if ($curva == "") {
			return "TODAS";
		} else {
			return $curva;
		}

	}

	public function convert_horas_numeros($horas = null){

		if($horas != ''){

			$temp = explode(':',$horas);
			return $temp[0].'.'.$temp[1];

		} else {

			return 1;

		}

	}

	public function media_horas($horas = null){

		if (count($horas) > 0) {
			return date('H:i:s',array_sum($horas) / count($horas));
		} else {
			return 0;
		}

	}

}
