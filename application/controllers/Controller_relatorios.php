<?php defined('BASEPATH') OR exit('No direct script access allowed');

	class controller_relatorios extends CI_Controller {

		public function ajax_horario_especifico(){

			$consulta = $this->model_relatorios->ajaxHorarioE($this->input->post('gn_filtro'),$this->input->post('de'),$this->input->post('ate'));
			
			$horas['filial1'] = $consulta['tempo_medio_f1'];
			$horas['filial2'] = $consulta['tempo_medio_f12'];
			$horas['media'] = $consulta['tempo_medio'];
			$horas['min'] = $consulta['entrou_media'];
			$horas['max'] = $consulta['saiu_media'];

			//Usado para exibir no gráfico
			$horas['graficof1'] = $this->convert_horas_numeros($horas['filial1']);
			$horas['graficof2'] = $this->convert_horas_numeros($horas['filial2']);
			
			header('Content-Type: application/json');

			echo json_encode($horas);	

		}

		public function ajax_detalhes_horarios(){

			echo $this->model_relatorios->lista_horas($this->input->post('de'),$this->input->post('ate'));


		}

		//Usado para que o gráfico saiba dividir os valores na pizza.
		public function convert_horas_numeros($horas = null){

			if($horas != ''){

				$temp = explode(':',$horas);
				return $temp[0].'.'.$temp[1];

			} else {

				return 1;

			}

		}
		################################# view_visitaspdvc_relatorios #################################
		//Quando é usado o filtro
		public function ajax_visitaspdvc_filtro(){

			$data_filtro = $this->input->post('data_filtro');
			$de = $this->input->post('de');
			$ate = $this->input->post('ate');
			$filial_filtro = $this->input->post('filial_filtro');
			$gn_filtro = $this->input->post('gn_filtro');

			header('Content-Type: application/json');

			$resposta = $this->model_relatorios->ajax_visitaspdvc_filtro($data_filtro,$de,$ate,$filial_filtro,$gn_filtro);

			echo json_encode($resposta);


		}

		public function ajax_visitaspdvc_detalhes(){

			$data_filtro = $this->input->post('data_filtro');
			$de = $this->input->post('de');
			$ate = $this->input->post('ate');
			$gn_filtro = $this->input->post('gn_filtro');

			if ($this->input->post('fora') == 1) {
				$fora = true;
			} else {
				$fora = false;
			}

			$resposta = $this->model_relatorios->ajax_visitaspdvc_detalhes($data_filtro,$de,$ate,$gn_filtro,$fora);

			$usuario = "GN";
			$pegar_usuario = true;
			$tabela = "";

			$tabela .= '<table class="mdl-data-table mdl-js-data-table mdl-shadow--8dp semDataTable" style="width: 100%">
			<thead>
				<tr>
					<th>CNPJ</th>
					<th>REDE</th>
					<th>LOUGRADOURO</th>
					<th>CURVA</th>
				</tr>
			</thead>
			<tbody>';

			foreach ($resposta as $valor) {

				if ($pegar_usuario) {
					$usuario = strtoupper($valor->usuario);
					$pegar_usuario = false;
				}

				$tabela .= '<tr>';
				$tabela .= '<td>'.$valor->cnpj.'</td>';
				$tabela .= '<td>'.$valor->rede.'</td>';
				$tabela .= '<td>'.$valor->lougradouro.'</td>';
				$tabela .= '<td>'.$valor->curva.'</td>';
				$tabela .= '</tr>';
			}

			$tabela .= '</tbody></table>';

			if ($this->input->post('fora') == 1) {
				echo '<h4>DETALHES DE VISITAÇÃO FORA DA CARTEIRA - "'.$usuario.'"</h4>';
			} else {
				echo '<h4>DETALHES DE VISITAÇÃO CARTEIRA - "'.$usuario.'"</h4>';
			}

			echo $tabela;


		}

		//Lista dos GNs
		public function ajax_visitaspdvc_filtro_lista(){

			$data_filtro = $this->input->post('data_filtro');
			$de = $this->input->post('de');
			$ate = $this->input->post('ate');
			$filial_filtro = $this->input->post('filial_filtro');
			$gn_filtro = $this->input->post('gn_filtro');

			header('Content-Type: application/json');

			$resposta = $this->model_relatorios->ajax_visitaspdvc_filtro_lista($data_filtro,$de,$ate,$filial_filtro,$gn_filtro);

			echo '<table class="mdl-data-table mdl-js-data-table mdl-shadow--8dp semDataTable" style="width: 100%">
			<thead>
				<tr>
					<th width="40%" class="mdl-data-table__cell--non-numeric">GN</th>
					<th width="20%">Média do GN por período</th>
					<th width="20%">Total de Visitas a PDVs</th>
					<th width="20%">Fora da Carteira (PDVs)</th>
				</tr>
			</thead>
		  <tbody>'; 

	  			foreach ($resposta as $visitar_gn) {
  					echo '<tr>
				   		<td width="80%" class="mdl-data-table__cell--non-numeric">'.$visitar_gn->usuario.'</td>
				   		<td>'.$visitar_gn->media.'</td>
				   		<td cod="'.$visitar_gn->id_usuario.'" class="dadosTotais" width="20%" style="cursor: pointer;"><a href="#detaques">'.$visitar_gn->total.'</a></td>
				   		<td cod="'.$visitar_gn->id_usuario.'" class="dadosFora" width="20%" style="cursor: pointer;"><a href="#detaques">'.$visitar_gn->fora.'</a></td>
				   	</tr>';
	  			}

		 echo '</tbody>
		</table>';


		}


		//Exibe os detalhes da consulta
		public function listar($dados = null,$titulo = null){

			$tipos = array ('diaria','semanal','mensal','semestral','anual');

			echo  '<div class="mdl-grid">
					<div class="mdl-cell mdl-cell--1-col">
						'.$titulo.'
					</div>';

			for ($i=0; $i < count($tipos); $i++) { 
			
				echo '<div class="mdl-cell mdl-cell--2-col resultados">';

					$carteira = number_format($dados[$tipos[$i]]['carteira'], 2, '.', '');
					$outros = number_format($dados[$tipos[$i]]['outros'], 2, '.', '');
					$total = number_format(($carteira + $outros), 2, '.', '');

					if($total > 0 && $carteira > 0) {
						$soma = (($carteira * 100) / $total);
						$percCarteira = number_format($soma, 2, '.', '');	
					} else {
						$percCarteira = 0;
					}

					if($total > 0 && $outros > 0) {
						$soma = (($outros * 100) / $total);
						$percOutros = number_format($soma, 2, '.', '');	
					} else {
						$percOutros = 0;
					}

					echo $total.'<br>';
					echo '<span class="detailsc" align="left">Carteira: '.$carteira.' ('.$percCarteira.'%)</span><br>';
					echo '<span class="detailsnc" align="left">Fora da carteira: '.$outros.' ('.$percOutros.'%)</span>';

				echo '</div>';

			}

			echo '</div>';

		}

		################################# view_mediatamrelatorios #################################

		public function ajax_mediatam(){

			$id = $this->input->post('id_usuario');
			$de = $this->input->post('de');
			$ate = $this->input->post('ate');
			$cnpj = str_replace(array('.','/','-'),'', $this->input->post('cnpj'));

			$total = $this->model_relatorios->mediatam_ajax($id,"",$de,$ate,$cnpj);
			$filial1 = $this->model_relatorios->mediatam_ajax($id," and filial = 1",$de,$ate,$cnpj);
			$filial2 = $this->model_relatorios->mediatam_ajax($id," and filial = 2",$de,$ate,$cnpj);

			$dados = array();

			$dados['total_couts'] = $total->row()->total;
			$dados['total_telefonia'] = $total->row()->telefonia;
			$dados['total_aberto'] = $total->row()->aberto;
			$dados['total_mobile'] = $total->row()->mobile;

			$totalTemp = $total->row()->telefonia + $total->row()->aberto + $total->row()->mobile;

			$dados['total_perc_t'] = $this->percentual_Grafico($totalTemp, $total->row()->telefonia);
			$dados['total_perc_a'] = $this->percentual_Grafico($totalTemp, $total->row()->aberto);
			$dados['total_perc_m'] = $this->percentual_Grafico($totalTemp, $total->row()->mobile);

			$dados['f1_couts'] = $filial1->row()->total;
			$dados['f1_telefonia'] = $filial1->row()->telefonia;
			$dados['f1_aberto'] = $filial1->row()->aberto;
			$dados['f1_mobile'] = $filial1->row()->mobile;

			$totalTemp = $filial1->row()->telefonia + $filial1->row()->aberto + $filial1->row()->mobile;

			$dados['f1_perc_t'] = $this->percentual_Grafico($totalTemp, $filial1->row()->telefonia);
			$dados['f1_perc_a'] = $this->percentual_Grafico($totalTemp, $filial1->row()->aberto);
			$dados['f1_perc_m'] = $this->percentual_Grafico($totalTemp, $filial1->row()->mobile);

			$dados['f2_couts'] = $filial2->row()->total;
			$dados['f2_telefonia'] = $filial2->row()->telefonia;
			$dados['f2_aberto'] = $filial2->row()->aberto;
			$dados['f2_mobile'] = $filial2->row()->mobile;

			$totalTemp = $filial2->row()->telefonia + $filial2->row()->aberto + $filial2->row()->mobile;

			$dados['f2_perc_t'] = $this->percentual_Grafico($totalTemp, $filial2->row()->telefonia);
			$dados['f2_perc_a'] = $this->percentual_Grafico($totalTemp, $filial2->row()->aberto);
			$dados['f2_perc_m'] = $this->percentual_Grafico($totalTemp, $filial2->row()->mobile);

			header('Content-Type: application/json');

			echo json_encode($dados);	


		}

		public function percentual_Grafico($total = null, $quantidade = null){
			
			if ($quantidade > 0 && $total > 0) {
				return (($quantidade * 100) / $total);
			} else {
				return 0;
			}


		}

		//USAR COM FUNÇÃO ACIMA, SE PEDIDO **************************
		public function porcentagem_ajax_mediatam($valor){

			if($valor > 0){

				$valor = (($valor * 100) / $total);
				$valor = number_format($valor, 2, '.', '');
				return $valor;

			} else {
				return 0;
			}

		}

		################################# view_recorrencia_relatorios #################################

		public function ajax_recorrencia(){

			$cnpj = str_replace(array('.','/','-'),'', $this->input->post('cnpj'));
			$de = $this->input->post('de');
			$ate = $this->input->post('ate');
			$curva_filtro = $this->input->post('curva_filtro');
			$filial_filtro = $this->input->post('filial_filtro');
			$gn_filtro = $this->input->post('gn_filtro');
			

			$dados = $this->model_relatorios->recorrencia_ajax($cnpj,$de,$ate,$curva_filtro,$filial_filtro,$gn_filtro);

			echo '<table class="mdl-data-table mdl-js-data-table mdl-shadow--8dp semDataTable" style="width: 100%;">
			<thead>
				<tr style="background-color: #dedede">
					<th width="20%" class="mdl-data-table__cell--non-numeric">REDE</th>
					<th width="20%" class="mdl-data-table__cell--non-numeric">LOJAS ATIVAS</th>
					<th width="15%" class="mdl-data-table__cell--non-numeric">LOJAS ATENDIDAS</th>
					<th width="15%" class="mdl-data-table__cell--non-numeric">VISITAS REALIZADAS</th>
					<th width="15%" class="mdl-data-table__cell--non-numeric">% REDE ATENDIDA</th>
					<th width="15%" class="mdl-data-table__cell--non-numeric">% REDE NÃO ATENDIDA</th>
				</tr>
			</thead>
		  <tbody>';

		  foreach ($dados as $pdvs) {
			  	echo '<tr>';
			  		echo '<td width="20%" class="mdl-data-table__cell--non-numeric">'.$pdvs->rede.'</td>';

			  		echo '<td width="20%" class="mdl-data-table__cell--non-numeric listarAtivas" style="cursor: pointer" rede="'.$pdvs->rede.'">
			  		'.$pdvs->lojas_ativas.'</td>';

			  		echo '<td width="15%" class="mdl-data-table__cell--non-numeric listarAtendidas" style="cursor: pointer" rede="'.$pdvs->rede.'">'.$pdvs->lojas_atendidas.'</td>';

			  		echo '<td width="15%" class="mdl-data-table__cell--non-numeric listarAtendidas" style="cursor: pointer" rede="'.$pdvs->rede.'">'.$pdvs->visitas_realizadas.'</td>';

			  		$perc_atendidas = $this->percentual_Grafico($pdvs->lojas_ativas,$pdvs->lojas_atendidas);

			  		echo '<td width="15%" class="mdl-data-table__cell--non-numeric">'.number_format($perc_atendidas, 2, ',', ' ').'%</td>';
			  		echo '<td width="15%" class="mdl-data-table__cell--non-numeric">'.number_format((100 - $perc_atendidas), 2, ',', ' ').'%</td>';
			  	echo '</tr>';
		  }

		  echo '</tbody>
		</table>';


		}

		public function ajax_recorrencia_ativos(){

			$rede = $this->model_relatorios->recorrencia_ativos(
				$this->input->post('rede'),
				$this->input->post('cnpj'),
				$this->input->post('curva_filtro'),
				$this->input->post('filial_filtro')
				);


			echo '<h3>LOJAS ATIVAS</h3>
			<table class="mdl-data-table mdl-js-data-table mdl-shadow--8dp semDataTable" style="width: 100%;">
			<thead>
				<tr style="background-color: #dedede">
					<th width="20%">REDE</th>
					<th width="60%">LOUGRADOURO</th>
					<th width="20%">CURVA</th>
				</tr>
			</thead>
		  <tbody>';

			foreach ($rede as $value) {
				echo '<tr>';

				echo '<td>'.$value->rede.'</td>';
				echo '<td>'.$value->endereco.'</td>';
				echo '<td>'.$value->curva.'</td>';

				echo '</tr>';
			}

			echo '</tbody>
		</table>';

		}

		public function ajax_recorrencia_atendidos(){

			$rede = $this->input->post('rede');
			$cnpj = str_replace(array('.','/','-'),'', $this->input->post('cnpj'));
			$de = $this->input->post('de');
			$ate = $this->input->post('ate');
			$curva_filtro = $this->input->post('curva_filtro');
			$filial_filtro = $this->input->post('filial_filtro');
			$gn_filtro = $this->input->post('gn_filtro');

			$rede = $this->model_relatorios->recorrencia_atendidos($rede,$cnpj,$de,$ate,$curva_filtro,$filial_filtro,$gn_filtro);


			echo '<h3>LOJAS ATENDIDAS</h3>
			<table class="mdl-data-table mdl-js-data-table mdl-shadow--8dp semDataTable" style="width: 100%;">
			<thead>
				<tr style="background-color: #dedede">
					<th width="20%">REDE</th>
					<th width="40%">LOUGRADOURO</th>
					<th width="20%">GN</th>
					<th width="20%">CURVA</th>
				</tr>
			</thead>
		  <tbody>';

			foreach ($rede as $value) {
				echo '<tr>';

				echo '<td>'.$value->rede.'</td>';
				echo '<td>'.$value->endereco.'</td>';
				echo '<td>'.$value->usuario.'</td>';
				echo '<td>'.$value->curva.'</td>';

				echo '</tr>';
			}

			echo '</tbody>
		</table>';

		}

		################################# view_recorrencia_relatorios #################################

		public function ajax_atende(){

			$de = $this->input->post('de');
			$ate = $this->input->post('ate');
			$filial = $this->input->post('filial');

			$dados = $this->model_relatorios->atende_ajax($filial,$de,$ate);

			function resultado($valor = null,$total = null){
				if($total > 0 && $valor > 0){
					$perc = (($valor * 100) / $total);
					return number_format($perc, 2, '.', '');
				} else {
					return 0;
				}
			}

			echo '<tr>
			          <td>APRESENTAÇÃO</td>
			          <td>'.resultado($dados->qts_apresentacao_a,$dados->total).'%</td>
			          <td>'.resultado($dados->qts_apresentacao_b,$dados->total).'%</td>
			          <td>'.resultado($dados->qts_apresentacao_c,$dados->total).'%</td>
			          <td>'.resultado($dados->qts_apresentacao_a_p,$dados->total).'%</td>
			          <td>'.resultado($dados->qts_apresentacao_b_p,$dados->total).'%</td>
			          <td>'.resultado($dados->qts_apresentacao_c_p,$dados->total).'%</td>
			          <td>'.resultado($dados->qts_apresentacao_a_n,$dados->total).'%</td>
			          <td>'.resultado($dados->qts_apresentacao_b_n,$dados->total).'%</td>
			          <td>'.resultado($dados->qts_apresentacao_c_n,$dados->total).'%</td>
			        </tr>

			        <tr>
			          <td>BOOK</td>
			          <td>'.resultado($dados->qts_book_a,$dados->total).'%</td>
			          <td>'.resultado($dados->qts_book_b,$dados->total).'%</td>
			          <td>'.resultado($dados->qts_book_c,$dados->total).'%</td>
			          <td>'.resultado($dados->qts_book_a_p,$dados->total).'%</td>
			          <td>'.resultado($dados->qts_book_b_p,$dados->total).'%</td>
			          <td>'.resultado($dados->qts_book_c_p,$dados->total).'%</td>
			          <td>'.resultado($dados->qts_book_a_n,$dados->total).'%</td>
			          <td>'.resultado($dados->qts_book_b_n,$dados->total).'%</td>
			          <td>'.resultado($dados->qts_book_c_n,$dados->total).'%</td>
			        </tr>

			        <tr>
			          <td>CONHECIMENTO</td>
			          <td>'.resultado($dados->qts_conhecimento_a,$dados->total).'%</td>
			          <td>'.resultado($dados->qts_conhecimento_b,$dados->total).'%</td>
			          <td>'.resultado($dados->qts_conhecimento_c,$dados->total).'%</td>
			          <td>'.resultado($dados->qts_conhecimento_a_p,$dados->total).'%</td>
			          <td>'.resultado($dados->qts_conhecimento_b_p,$dados->total).'%</td>
			          <td>'.resultado($dados->qts_conhecimento_c_p,$dados->total).'%</td>
			          <td>'.resultado($dados->qts_conhecimento_a_n,$dados->total).'%</td>
			          <td>'.resultado($dados->qts_conhecimento_b_n,$dados->total).'%</td>
			          <td>'.resultado($dados->qts_conhecimento_c_n,$dados->total).'%</td>
			        </tr>';

		}

		################################# view_faturamento_relatorios #################################

		public function ajax_faturamento(){

			$cnpj = str_replace(array('.','/','-'),'', $this->input->post('cnpj'));
			$de = $this->input->post('de');
			$ate = $this->input->post('ate');

			$dados = $this->model_relatorios->faturamento_ajax($de,$ate,$cnpj);

			if($dados->row()->rede == ""){

				echo '<div class="mdl-grid">

			  		<div class="mdl-cell mdl-cell--12-col">
			  			<strong>CNPJ INVÁLIDO OU NENHUM REGISTRO ENCONTRADO NO PERÍODO</strong>
			  		</div>

			  	</div>

			  	<script type="text/javascript">
			  		$(document).ready(function(){
			  			$("#valorTotal").text("R$ ");
			  			$("#curvaText").hide();
			  			$("#curva").text("");
			  		});
			  	</script>

			  	';

			} else {

				echo '<table class="mdl-data-table mdl-js-data-table mdl-shadow--8dp semDataTable" style="width: 100%;">
						<thead>
							<tr style="background-color: #dedede">
								<th width="80%" class="mdl-data-table__cell--non-numeric">Venda de Aparelhos - Faixa</th>
								<th width="20%">Total</th>
							</tr>
						</thead>
					  <tbody>

						 <tr>
						 	<th width="80%" class="mdl-data-table__cell--non-numeric">até R$ 500,00</th>
							<th width="20%">'.$dados->row()->va_f1.'</th>
						 </tr>

						  <tr>
						 	<th width="80%" class="mdl-data-table__cell--non-numeric">de R$ 501,00 a R$ 999,00</th>
							<th width="20%">'.$dados->row()->va_f2.'</th>
						 </tr>

						  <tr>
						 	<th width="80%" class="mdl-data-table__cell--non-numeric">de R$ 1.000,00 a R$ 1.499,00</th>
							<th width="20%">'.$dados->row()->va_f3.'</th>
						 </tr>

						  <tr>
						 	<th width="80%" class="mdl-data-table__cell--non-numeric">de R$ 1.500,00 a R$ 1.999,00</th>
							<th width="20%">'.$dados->row()->va_f4.'</th>
						 </tr>

						  <tr>
						 	<th width="80%" class="mdl-data-table__cell--non-numeric">de R$ 2.000,00 a R$ 2.499,00</th>
							<th width="20%">'.$dados->row()->va_f5.'</th>
						 </tr>

						  <tr>
						 	<th width="80%" class="mdl-data-table__cell--non-numeric">acima de R$ 2.500,00</th>
							<th width="20%">'.$dados->row()->va_f6.'</th>
						 </tr>

						 <tr>
						 	<th width="80%" class="mdl-data-table__cell--non-numeric">Total de vendas</th>
							<th width="20%">'.($dados->row()->va_f1 + $dados->row()->va_f2 + $dados->row()->va_f3 + $dados->row()->va_f4 + $dados->row()->va_f5 + $dados->row()->va_f6).'</th>
						 </tr>

					  </tbody>
					</table>

					<script type="text/javascript">
				  		$(document).ready(function(){
				  			$("#valorTotal").text("R$ '.$dados->row()->faturamento.'");
				  			$("#curvaText").show();
				  			$("#curva").text("'.$dados->row()->curva.'");
				  		});
				  	</script>

					';		

				$teste = '<div class="mdl-grid">

				  		<div class="mdl-cell mdl-cell--12-col">
				  			<strong>O FATURAMENTO MÉDIO DO PDV '.$dados->row()->rede.' É R$ '.$dados->row()->faturamento.' E SUA CURVA É '.$dados->row()->curva.'</strong>
				  		</div>

				  	</div>

				  	<div class="mdl-grid">

				  		<div class="mdl-cell mdl-cell--2-col">
				  			até R$ 500,00 
				  		</div>
				  		<div class="mdl-cell mdl-cell--2-col">
				  			de R$ 501,00 a R$ 999,00
				  		</div>
				  		<div class="mdl-cell mdl-cell--2-col">
				  			de R$ 1.000,00 a R$ 1.499,00
				  		</div>
				  		<div class="mdl-cell mdl-cell--2-col">
				  			de R$ 1.500,00 a R$ 1.999,00
				  		</div>
				  		<div class="mdl-cell mdl-cell--2-col">
				  			de R$ 2.000,00 a R$ 2.499,00
				  		</div>
				  		<div class="mdl-cell mdl-cell--2-col">
				  			acima de R$ 2.500,00
				  		</div>

				  	</div>

				  	<div class="mdl-grid">

				  		<div class="mdl-cell mdl-cell--2-col resultadosDestaque">
				  			'.$dados->row()->va_f1.'
				  		</div>
				  		<div class="mdl-cell mdl-cell--2-col resultadosDestaque">
				  			'.$dados->row()->va_f2.'
				  		</div>
				  		<div class="mdl-cell mdl-cell--2-col resultadosDestaque">
				  			'.$dados->row()->va_f3.'
				  		</div>
				  		<div class="mdl-cell mdl-cell--2-col resultadosDestaque">
				  			'.$dados->row()->va_f4.'
				  		</div>
				  		<div class="mdl-cell mdl-cell--2-col resultadosDestaque">
				  			'.$dados->row()->va_f5.'
				  		</div>
				  		<div class="mdl-cell mdl-cell--2-col resultadosDestaque">
				  			'.$dados->row()->va_f6.'
				  		</div>

				  	</div>';
			}

		}

		################################# view_promotor_relatorios #################################
		public function ajax_promotor(){

			$de = $this->input->post('de');
			$ate = $this->input->post('ate');
			$filial = $this->input->post('filial');
			$fk_usuario = $this->input->post('usuario');
			$curva = $this->input->post('curva');

			$resultado = $this->model_relatorios->promotor_ajax($de,$ate,$filial,$fk_usuario,$curva);

			$dados = array();

			$dados['claro_pt'] = 0;
			$dados['claro_free'] = 0;
			$dados['claro_fixo'] = 0;
			
			$dados['oi_pt'] = 0;
			$dados['oi_free'] = 0;
			$dados['oi_fixo'] = 0;
			
			$dados['vivo_pt'] = 0;
			$dados['vivo_free'] = 0;
			$dados['vivo_fixo'] = 0;
			
			$dados['tim_pt'] = 0;
			$dados['tim_free'] = 0;
			$dados['tim_fixo'] = 0;

			header('Content-Type: application/json');


			foreach ($resultado as $operadora) {
				
				if ($operadora->operadora == 'claro') {
					$dados['claro_pt'] = $operadora->pt;
					$dados['claro_free'] = $operadora->free;
					$dados['claro_fixo'] = $operadora->fixo;

					$totalTemp = $operadora->pt + $operadora->free + $operadora->fixo;

					$dados['claro_perc_pt'] = $this->percentual_Grafico($totalTemp, $operadora->pt);
					$dados['claro_perc_free'] = $this->percentual_Grafico($totalTemp, $operadora->free);
					$dados['claro_perc_fixo'] = $this->percentual_Grafico($totalTemp, $operadora->fixo);

				}

				if ($operadora->operadora == 'oi') {
					$dados['oi_pt'] = $operadora->pt;
					$dados['oi_free'] = $operadora->free;
					$dados['oi_fixo'] = $operadora->fixo;

					$totalTemp = $operadora->pt + $operadora->free + $operadora->fixo;

					$dados['oi_perc_pt'] = $this->percentual_Grafico($totalTemp, $operadora->pt);
					$dados['oi_perc_free'] = $this->percentual_Grafico($totalTemp, $operadora->free);
					$dados['oi_perc_fixo'] = $this->percentual_Grafico($totalTemp, $operadora->fixo);

				}

				if ($operadora->operadora == 'vivo') {
					$dados['vivo_pt'] = $operadora->pt;
					$dados['vivo_free'] = $operadora->free;
					$dados['vivo_fixo'] = $operadora->fixo;

					$totalTemp = $operadora->pt + $operadora->free + $operadora->fixo;

					$dados['vivo_perc_pt'] = $this->percentual_Grafico($totalTemp, $operadora->pt);
					$dados['vivo_perc_free'] = $this->percentual_Grafico($totalTemp, $operadora->free);
					$dados['vivo_perc_fixo'] = $this->percentual_Grafico($totalTemp, $operadora->fixo);

				}

				if ($operadora->operadora == 'tim') {
					$dados['tim_pt'] = $operadora->pt;
					$dados['tim_free'] = $operadora->free;
					$dados['tim_fixo'] = $operadora->fixo;

					$totalTemp = $operadora->pt + $operadora->free + $operadora->fixo;

					$dados['tim_perc_pt'] = $this->percentual_Grafico($totalTemp, $operadora->pt);
					$dados['tim_perc_free'] = $this->percentual_Grafico($totalTemp, $operadora->free);
					$dados['tim_perc_fixo'] = $this->percentual_Grafico($totalTemp, $operadora->fixo);

				}

			}

			echo json_encode($dados);	

		}

		################################# view_campanha_relatorios #################################
		public function ajax_campanha(){

			$cnpj = str_replace(array('.','/','-'),'', $this->input->post('cnpj'));
			$de = $this->input->post('de');
			$ate = $this->input->post('ate');
			$filial = $this->input->post('filial');

			$dados = $this->model_relatorios->campanha_ajax($cnpj,$de,$ate,$filial);

			$this->echo_campanha($dados['campanhac'],'<img src="'.base_url().'style/imagens/relatorios/logo_claro.png"  width="40px"> <br> CAMPANHA');
			$this->echo_campanha($dados['produtoc'],'<img src="'.base_url().'style/imagens/relatorios/logo_claro.png"  width="40px"> <br> PRODUTOS');
			$this->echo_campanha($dados['campanhao'],'<img src="'.base_url().'style/imagens/relatorios/logo_oi.png"  width="40px"> <br> CAMPANHA');
			$this->echo_campanha($dados['produtoo'],'<img src="'.base_url().'style/imagens/relatorios/logo_oi.png"  width="40px"> <br> PRODUTOS');
			$this->echo_campanha($dados['campanhav'],'<img src="'.base_url().'style/imagens/relatorios/logo_vivo.png"  width="40px"> <br> CAMPANHA');
			$this->echo_campanha($dados['produtov'],'<img src="'.base_url().'style/imagens/relatorios/logo_vivo.png"  width="40px"> <br> PRODUTOS');
			$this->echo_campanha($dados['campanhat'],'<img src="'.base_url().'style/imagens/relatorios/logo_tim.png"  width="40px"> <br> CAMPANHA');
			$this->echo_campanha($dados['produtot'],'<img src="'.base_url().'style/imagens/relatorios/logo_tim.png"  width="40px"> <br> PRODUTOS');

			

		}

		public function echo_campanha($dados = null, $titulo = null){

			echo '<div class="mdl-grid">
  					<div class="mdl-cell mdl-cell--2-col" align="center">'.$titulo.'</div>';

			foreach ($dados->result() as $value) {

				echo '<div class="mdl-cell mdl-cell--2-col"><strong>'.$value->palavra.'</strong> <br> com ('.$value->quantidade.') Repetições</div>';
				
			}

			echo '</div>
			</div>
			<hr>';

		}

	}