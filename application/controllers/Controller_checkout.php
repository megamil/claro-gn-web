<?php defined('BASEPATH') OR exit('No direct script access allowed');

	class controller_checkout extends CI_Controller {

	public function exportar_pdvs_nlocalizados(){

		// Definimos o nome do arquivo que será exportado
		$arquivo = 'exportar_pdvs_nlocalizados.xls';


		$dados = $this->model_checkout->pdvs_nlocalizados();


		$html = '<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" />
		<table class="mdl-data-table mdl-js-data-table mdl-shadow--8dp">
		  <thead>
		    <tr>
		      <th>Checkout</th>
		      <th>COD Agente</th>
		      <th>Data criação</th>
			  <th>Usuário</th>
		      <th>Rede</th>
		      <th>Filial</th>
		      <th>CNPJ</th>
		      <th>Lougradouro</th>
		      <th>Bairro</th>
		      <th>CEP</th>
		      <th>Cidade</th>
		    </tr>

		    <tbody>';


		foreach ($dados as $revisar) {
			$html .= '<tr>';
			$html .= '<td>'.$revisar->fk_checkout.'</td>';
			$html .= '<td>'.$revisar->revisar_cod_agente.'</td>';
			$html .= '<td>'.$revisar->data_checkout.'</td>';
			$html .= '<td>'.$revisar->nome.'</td>';
			$html .= '<td>'.$revisar->revisar_rede.'</td>';
			$html .= '<td>'.$revisar->revisar_filial.'</td>';
			$html .= '<td>'.$revisar->revisar_cnpj.'</td>';
			$html .= '<td>'.$revisar->revisar_lougradouro.'</td>';
			$html .= '<td>'.$revisar->revisar_bairro.'</td>';
			$html .= '<td>'.$revisar->revisar_cep.'</td>';
			$html .= '<td>'.$revisar->revisar_cidade.'</td>';
			$html .= '</tr>';
		}

		$html .= '</tbody></table>';

		// Configurações header para forçar o download
		header ("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
		header ("Cache-Control: must-revalidate, post-check=0, pre-check=0"); 
		header ("Content-Transfer-Encoding: binary"); 
		header ("Content-Type: application/vnd.ms-excel"); 
		header ("Expires: 0"); 
		header ("Content-Disposition: attachment; filename=\"{$arquivo}\"");
		header ("Content-Description: PHP Generated Data");

		// Envia o conteúdo do arquivo
		chr(255).chr(254).iconv("UTF-8", "UTF-16LE//IGNORE", $html); 
		echo $html;
		exit;

	}

	//Função inicial.
	public function formulario () {
		
		//Trocar por chave no APP.
		if(empty($this->input->get('fk_pdv')) ||
			empty($this->input->get('fk_usuario')) ||
			empty($this->input->get('nome_usuario')) ||
			empty($this->input->get('rede')) ||
			empty($this->input->get('endereco_exato'))){

			echo '<h1 align="center">USO EXCLUSIVO DO APLICATIVO PARA IOS E ANDROID.</h1>';
			exit;

		}


		$enviar = $this->model_checkout->enviar_diretamente($this->input->get('fk_pdv'));
		$enviar_diretamente = $enviar->row()->envio_direto;

		
		//Valida se é um PDV que não precisa de formulário
		if($enviar_diretamente == 1) {

			$endereco_exato = $this->nulo_vazio($this->input->get('endereco_exato'));
			$nome_usuario = $this->nulo_vazio($this->input->get('nome_usuario'));

			//Campos Checkout
			$fk_pdv = $this->nulo_se_vazio($this->input->get('fk_pdv'));
			$fk_usuario = $this->nulo_se_vazio($this->input->get('fk_usuario'));
			$data = null;// Sim, com NULL ele grava a data atual...
			
			$checkout = array (

				'fk_pdv' => $fk_pdv,
				'fk_usuario' => $fk_usuario,
				'data_checkout' => $data,
				'endereco_exato' => $endereco_exato

			);

			$id_checkout = $this->model_checkout->checkout($checkout);

			$body = '<hr> 
			<h2>Rede: </h2> '.$enviar->row()->rede.'
			<br> 
			<h1>Localização exata do envio do check-out:</h1> '.$endereco_exato.'
			<br> 
			<h1>Usuário: </h1> '.$nome_usuario.'';
			//echo $body;
			// Detalhes do Email.
			
			$this->email->from('clarogn@megamil.net', 'Checkout CLAROGN'); 
			$this->email->to("comunicado.varejosp@claro.com.br"); 

			$list = array("megamil3d@gmail.com", 'guilherme@naville.marketing');
			$this->email->bcc($list); 
		 
			$this->email->subject('Checkout Enviado pelo Aplicativo ClaroGN'); 
			$this->email->message($body); 
			$this->email->set_mailtype("html");

			// Enviar... 
			if ($this->email->send()) { 
				$this->load->view('formulario', array('envio_direto' => 1));
			} else { //caso de erro no envio
				$this->load->view('formulario', array('envio_direto' => 2));
			}

		} else { // caso não seja de envio direto
			//OBS Os campos que são passados pela URL do aplicativo vão diretamente para a tela.

			$salvo = $this->model_checkout->continuar($this->input->get('fk_pdv'),$this->input->get('fk_usuario'));

			//Se tiver resultado, deve continuar o formulário
			if($salvo->num_rows() == 1) {

				$dados = array (
					'marcas' => $this->model_checkout->listarMarcas(),
					'redes' => $this->model_checkout->listarRedes(),
					'checkoutSalvo' => $salvo,
					'atendimentoSalvo' => $this->model_checkout->atendimentoSalvo($salvo->row()->fk_atendimento),
					'faturamentoSalvo' => $this->model_checkout->faturamentoSalvo($salvo->row()->fk_faturamento),
					'promotorSalvo' => $this->model_checkout->promotorSalvo($salvo->row()->fk_promotor),
					'atendepnSalvo' => $this->model_checkout->atendepnSalvo($salvo->row()->fk_atendepn),
					'marketingSalvo' => $this->model_checkout->marketingSalvo($salvo->row()->id_checkout),
					'campanhaSalvo' => $this->model_checkout->campanhaSalvo($salvo->row()->id_checkout),
					'informacaovendasSalvo' => $this->model_checkout->informacaovendasSalvo($salvo->row()->id_checkout),
					'parttimeSalvo' => $this->model_checkout->parttimeSalvo($salvo->row()->id_checkout),
					'maisvendidosSalvo' => $this->model_checkout->maisvendidosSalvo($salvo->row()->id_checkout),
					'dadosEsteMes' => array ('dados' => null,'registros' => 0), //Deve usar das alterações que estava fazendo
					'dadosEstaSemana' => array ('dados' => null,'registros' => 0)//Deve usar das alterações que estava fazendo

				);
				$this->load->view('formulario',$dados);	

			} else {

				if($this->input->get('fk_pdv') == 1) { // Se estiver adicionando um novo PDV
					$dadosEsteMes = array ('dados' => null,'registros' => 0);
					$dadosEstaSemana = array ('dados' => null,'registros' => 0);
				} else {
					$dadosEsteMes = $this->model_checkout->dadosEsteMes($this->input->get('fk_pdv'),$this->input->get('fk_usuario'));
					$dadosEstaSemana = $this->model_checkout->dadosEstaSemana($this->input->get('fk_pdv'),$this->input->get('fk_usuario'));
				}

				$dados = array (
					'marcas' => $this->model_checkout->listarMarcas(),
					'redes' => $this->model_checkout->listarRedes(),
					'dadosEsteMes' => $dadosEsteMes, 
					'dadosEstaSemana' => $dadosEstaSemana
					);
				$this->load->view('formulario',$dados);	
			}


		}

		

	}

	public function filtroAjax() {

		$usuario = $this->input->post('usuario');
		$de = $this->input->post('de');
		$ate = $this->input->post('ate');

		$resultado =  $this->model_checkout->filtrarPor($usuario,$de,$ate);

		echo '

				<table class="mdl-data-table mdl-js-data-table mdl-shadow--8dp">
			  <thead>
			    <tr>
			      <th class="mdl-data-table__cell--non-numeric">ID</th>
			      <th class="mdl-data-table__cell--non-numeric">Data</th>
			      <th class="mdl-data-table__cell--non-numeric">Usuário</th>
			      <th class="mdl-data-table__cell--non-numeric">Rede</th>
			      <th class="mdl-data-table__cell--non-numeric">Endereço de origem</th>
			    </tr>
			  </thead>
			  <tbody>';
			     foreach ($resultado as $checkout) {
			    	echo '<tr>';
			    	echo '<td class="mdl-data-table__cell--non-numeric">'.$checkout->id_checkout.'</td>';
					echo '<td class="mdl-data-table__cell--non-numeric">'.$checkout->data_checkout.'</td>';
					echo '<td class="mdl-data-table__cell--non-numeric">'.$checkout->usuario.'</td>';
					echo '<td class="mdl-data-table__cell--non-numeric">'.$checkout->rede.'</td>';
					echo '<td class="mdl-data-table__cell--non-numeric">'.$checkout->endereco_exato.'</td>';
					echo '</tr>';
				} 
			  echo '</tbody>
			</table>

		';


	}

	public function ajax_Listar_modelos(){

		if($this->input->post('fk_marca') > 0) {

			$modelos = $this->model_checkout->ajax_Modelos($this->input->post('fk_marca'));
			$indice = $this->input->post('indice');

			echo '<select class="form-control" type="" name="nome_aparelho'.$indice.'" id="aparelho'.$indice.'">';

			if($this->input->post('modelo') != "") {
				foreach ($modelos as $modelo) {
					//Segundo parametro para enviar o texto por e-mail, primeiro parametro ID para o banco.
					if($this->input->post('modelo') == $modelo->id_modelo){
						echo '<option value="'.$modelo->id_modelo.'-'.$modelo->modelo.'" selected>'.$modelo->modelo.'</option>';	
					} else {
						echo '<option value="'.$modelo->id_modelo.'-'.$modelo->modelo.'">'.$modelo->modelo.'</option>';
					}
		    		
				}
			} else {
				foreach ($modelos as $modelo) {
					//Segundo parametro para enviar o texto por e-mail, primeiro parametro ID para o banco.
	    			echo '<option value="'.$modelo->id_modelo.'-'.$modelo->modelo.'">'.$modelo->modelo.'</option>';
				}
			}

			echo '</select>';

		} else {

			echo '<select class="form-control" type="" name="nome_aparelho'.$this->input->post('indice').'" id="aparelho'.$this->input->post('indice').'">
				<option value="0">Selecione uma marca acima...</option>
			</select>';

		}

		

	}

	public function salvar_dados() {

		//Campos checkout_atendimento
		$atendimento_telefonia = $this->true_se_on($this->input->post('atendimento_telefonia'));
		$atendimento_aberto = $this->true_se_on($this->input->post('atendimento_aberto'));
		$atendimento_mobile = $this->true_se_on($this->input->post('atendimento_mobile'));
		$qtd_vendedor = $this->nulo_se_vazio($this->input->post('qtd_vendedor'));
		$loja = $this->nulo_se_vazio($this->input->post('loja'));
		$setor = $this->nulo_se_vazio($this->input->post('setor'));
		$qts_assistentes = $this->nulo_se_vazio($this->input->post('qts_assistentes'));
		$qts_caixas = $this->nulo_se_vazio($this->input->post('qts_caixas'));
		$balcao_vitrine = $this->true_se_on($this->input->post('balcao_vitrine'));
		$balcao_degustador = $this->true_se_on($this->input->post('balcao_degustador'));

		$checkout_atendimento = array (

			'atendimento_telefonia' => $atendimento_telefonia,
			'atendimento_aberto' => $atendimento_aberto,
			'atendimento_mobile' => $atendimento_mobile,
			'qtd_vendedor' => $qtd_vendedor,
			'loja' => $loja,
			'setor' => $setor,
			'qts_assistentes' => $qts_assistentes,
			'qts_caixas' => $qts_caixas,
			'balcao_vitrine' => $balcao_vitrine,
			'balcao_degustador' => $balcao_degustador 

		);

		//Campo checkout_faturamento 
		$faturamento_pdv = $this->nulo_se_vazio($this->input->post('faturamento_pdv'));
		$faturamento_setor = $this->nulo_se_vazio($this->input->post('faturamento_setor'));
		$va_f1 = $this->nulo_se_vazio($this->input->post('va_f1'));
		$va_f2 = $this->nulo_se_vazio($this->input->post('va_f2'));
		$va_f3 = $this->nulo_se_vazio($this->input->post('va_f3'));
		$va_f4 = $this->nulo_se_vazio($this->input->post('va_f4'));
		$va_f5 = $this->nulo_se_vazio($this->input->post('va_f5'));
		$va_f6 = $this->nulo_se_vazio($this->input->post('va_f6'));

		$checkout_faturamento = array (

			'faturamento_pdv' => str_replace('.','',$faturamento_pdv),
			'faturamento_setor' => str_replace('.','',$faturamento_setor),
			'va_f1' => $va_f1,
			'va_f2' => $va_f2,
			'va_f3' => $va_f3,
			'va_f4' => $va_f4,
			'va_f5' => $va_f5,
			'va_f6' => $va_f6 

		);

		//Campo checkout_promotor
		$pc_agencia = $this->nulo_se_vazio($this->input->post('pc_agencia'));
		$pc_supervisor = $this->nulo_se_vazio($this->input->post('pc_supervisor'));
		$pc_promotor = $this->nulo_se_vazio($this->input->post('pc_promotor'));
		$pc_tempo_pdv = $this->nulo_se_vazio($this->input->post('pc_tempo_pdv'));

		$checkout_promotor = array (

			'promotor' => $pc_promotor,
			'tempo_pdv' => $pc_tempo_pdv,
			'agencia' => $pc_agencia,
			'supervisor' => $pc_supervisor 

		);

		//Campo checkout_atendepn
		$apresentacao = $this->nulo_se_vazio($this->input->post('apresentacao'));
		$conhecimento = $this->nulo_se_vazio($this->input->post('conhecimento'));
		$book = $this->nulo_se_vazio($this->input->post('book'));

		$checkout_atendepn = array (

			'apresentacao' => $apresentacao,
			'conhecimento' => $conhecimento,
			'book' => $book 

		);

		//Campos Checkout
		$fk_pdv = $this->nulo_se_vazio($this->input->post('fk_pdv'));
		$fk_usuario = $this->nulo_se_vazio($this->input->post('fk_usuario'));
		$endereco_exato = $this->nulo_se_vazio($this->input->post('endereco_exato'));

		$fk_atendimento = $this->model_checkout->checkout_atendimento($checkout_atendimento);
		$fk_faturamento = $this->model_checkout->checkout_faturamento($checkout_faturamento);
		$fk_promotor = $this->model_checkout->checkout_promotor($checkout_promotor);
		$fk_atendepn = $this->model_checkout->checkout_atendepn($checkout_atendepn);

		if($this->input->post('checkpoint') == 1){
			$data = 'current_timestamp';
		} else {
			$data = null;
		}

		$obs = $this->nulo_se_vazio($this->input->post('obs'));

		//Valida se estamos editando um checkout
		if(!empty($this->input->post('id_checkout'))) {

			$id_checkout = $this->input->post('id_checkout');

			//Deleta os ultimos registros deste checkout.
			$this->model_checkout->deletarSalvos($id_checkout);

			$checkout = array (

				'id_checkout' => $id_checkout,
				'fk_pdv' => $fk_pdv,
				'fk_usuario' => $fk_usuario,
				'fk_atendimento' => $fk_atendimento,
				'fk_faturamento' => $fk_faturamento,
				'fk_promotor' => $fk_promotor,
				'fk_atendepn' => $fk_atendepn,
				'data_checkout' => $data,
				'endereco_exato' => $endereco_exato,
				'observacoes' => $obs 

			);

			$id_checkout = $this->model_checkout->checkout($checkout);
			

		} else {

			$checkout = array (

				'fk_pdv' => $fk_pdv,
				'fk_usuario' => $fk_usuario,
				'fk_atendimento' => $fk_atendimento,
				'fk_faturamento' => $fk_faturamento,
				'fk_promotor' => $fk_promotor,
				'fk_atendepn' => $fk_atendepn,
				'data_checkout' => $data,
				'endereco_exato' => $endereco_exato,
				'observacoes' => $obs 

			);

			$id_checkout = $this->model_checkout->checkout($checkout);
			
		} 


		if($fk_pdv == 1){ //Caso seja o PDV usado em casos de PDVs não localizados.

  			//Pega o texto entre temp e temp1
			$temp = explode('Cidade: ', $endereco_exato);
			$temp1 = explode(' CEP: ', $temp[1]);
			$cidade = $temp1[0];

			$temp = explode(' CEP: ', $endereco_exato);
			$temp1 = explode(' UF: ', $temp[1]);
			$cep = $temp1[0];

			$temp = explode(' UF: ', $endereco_exato);
			$temp1 = explode(' Bairro: ', $temp[1]);
			$UF = $temp1[0];

			$temp = explode(' Bairro: ', $endereco_exato);
			$temp1 = explode(' RUA: ', $temp[1]);
			$bairro = $temp1[0];

			$temp = explode(' RUA: ', $endereco_exato);
			$rua = $temp[1];

			$pdv_revisar = array (

				'fk_checkout' => $id_checkout,
				'revisar_cod_agente' => $this->input->post('codmod'),
				'revisar_rede' => $this->input->post('rede'),
				'revisar_filial' => $this->input->post('filial'),
				'revisar_cnpj' => $this->input->post('cnpj'),
				'revisar_lougradouro' => $rua,
				'revisar_bairro' => $bairro,
				'revisar_cep' => $cep,
				'revisar_cidade' => $cidade 

			);

			$this->model_checkout->pdv_revisar($pdv_revisar);
		}

		//Campo checkout_informacao_vendas (tipo 1,2 'Tipo 1 Pré pago, 2 Controle' Venda 1,2,3,4 'Cada número uma operadora')
		$t1venda1 = $this->nulo_se_vazio($this->input->post('t1venda1'));
		$t1venda2 = $this->nulo_se_vazio($this->input->post('t1venda2'));
		$t1venda3 = $this->nulo_se_vazio($this->input->post('t1venda3'));
		$t1venda4 = $this->nulo_se_vazio($this->input->post('t1venda4'));
		$t2venda1 = $this->nulo_se_vazio($this->input->post('t2venda1'));
		$t2venda2 = $this->nulo_se_vazio($this->input->post('t2venda2'));
		$t2venda3 = $this->nulo_se_vazio($this->input->post('t2venda3'));
		$t2venda4 = $this->nulo_se_vazio($this->input->post('t2venda4'));

		$checkout_informacao_vendas = array (

			 array ('fk_operadora' => 1, 'fk_checkout' => $id_checkout, 'tipo' => 'p', 'venda' => $t1venda1),
			 array ('fk_operadora' => 2, 'fk_checkout' => $id_checkout, 'tipo' => 'p', 'venda' => $t1venda2),
			 array ('fk_operadora' => 3, 'fk_checkout' => $id_checkout, 'tipo' => 'p', 'venda' => $t1venda3),
			 array ('fk_operadora' => 4, 'fk_checkout' => $id_checkout, 'tipo' => 'p', 'venda' => $t1venda4),
			 array ('fk_operadora' => 1, 'fk_checkout' => $id_checkout, 'tipo' => 'c', 'venda' => $t2venda1),
			 array ('fk_operadora' => 2, 'fk_checkout' => $id_checkout, 'tipo' => 'c', 'venda' => $t2venda2),
			 array ('fk_operadora' => 3, 'fk_checkout' => $id_checkout, 'tipo' => 'c', 'venda' => $t2venda3),
			 array ('fk_operadora' => 4, 'fk_checkout' => $id_checkout, 'tipo' => 'c', 'venda' => $t2venda4)

		);

		$this->model_checkout->checkout_informacao_vendas($checkout_informacao_vendas);

		//Campo checkout_mais_vendidos
		if($this->input->post('marcas1') == 0) {$marcas1 = null;} else {$marcas1 = $this->input->post('marcas1');}
		if($this->input->post('marcas2') == 0) {$marcas2 = null;} else {$marcas2 = $this->input->post('marcas2');}
		if($this->input->post('marcas3') == 0) {$marcas3 = null;} else {$marcas3 = $this->input->post('marcas3');}
		if($this->input->post('marcas4') == 0) {$marcas4 = null;} else {$marcas4 = $this->input->post('marcas4');}

		if($this->input->post('nome_aparelho1') == 0) {
			$modelo1 = null;
			$aparelho1 = '<div style="background-color: #D3D3D3;"> Em Branco </div>';
		} else {
			$modelo = explode('-',$this->input->post('nome_aparelho1'));
			$modelo1 = $modelo[0]; //ID
			$aparelho1 = $modelo[1]; // Texto para o e-mail
		}

		if($this->input->post('nome_aparelho2') == 0) {
			$modelo2 = null;
			$aparelho2 = '<div style="background-color: #D3D3D3;"> Em Branco </div>';
		} else {
			$modelo = explode('-',$this->input->post('nome_aparelho2'));
			$modelo2 = $modelo[0]; //ID
			$aparelho2 = $modelo[1]; // Texto para o e-mail
		}

		if($this->input->post('nome_aparelho3') == 0) {
			$modelo3 = null;
			$aparelho3 = '<div style="background-color: #D3D3D3;"> Em Branco </div>';
		} else {
			$modelo = explode('-',$this->input->post('nome_aparelho3'));
			$modelo3 = $modelo[0]; //ID
			$aparelho3 = $modelo[1]; // Texto para o e-mail
		}

		if($this->input->post('nome_aparelho4') == 0) {
			$modelo4 = null;
			$aparelho4 = '<div style="background-color: #D3D3D3;"> Em Branco </div>';
		} else {
			$modelo = explode('-',$this->input->post('nome_aparelho4'));
			$modelo4 = $modelo[0]; //ID
			$aparelho4 = $modelo[1]; // Texto para o e-mail
		}

		$valor1 = $this->nulo_se_vazio($this->input->post('valor1'));
		$media1 = $this->nulo_se_vazio($this->input->post('media1'));

		$valor2 = $this->nulo_se_vazio($this->input->post('valor2'));
		$media2 = $this->nulo_se_vazio($this->input->post('media2'));

		$valor3 = $this->nulo_se_vazio($this->input->post('valor3'));
		$media3 = $this->nulo_se_vazio($this->input->post('media3'));

		$valor4 = $this->nulo_se_vazio($this->input->post('valor4'));
		$media4 = $this->nulo_se_vazio($this->input->post('media4'));

		$checkout_mais_vendidos = array (
			array ('fk_marca' => $marcas1,'fk_modelo' => $modelo1,'fk_checkout' => $id_checkout,'valor' => $valor1,'media' => $media1),
			array ('fk_marca' => $marcas2,'fk_modelo' => $modelo2,'fk_checkout' => $id_checkout,'valor' => $valor2,'media' => $media2),
			array ('fk_marca' => $marcas3,'fk_modelo' => $modelo3,'fk_checkout' => $id_checkout,'valor' => $valor3,'media' => $media3),
			array ('fk_marca' => $marcas4,'fk_modelo' => $modelo4,'fk_checkout' => $id_checkout,'valor' => $valor4,'media' => $media4)
		);

		$this->model_checkout->checkout_mais_vendidos($checkout_mais_vendidos);

		//Campo checkout_part_time
		$claro_fixo = $this->nulo_se_vazio($this->input->post('claro_fixo'));
		$claro_pt = $this->nulo_se_vazio($this->input->post('claro_pt'));
		$claro_free = $this->nulo_se_vazio($this->input->post('claro_free'));
		$oi_fixo = $this->nulo_se_vazio($this->input->post('oi_fixo'));
		$oi_pt = $this->nulo_se_vazio($this->input->post('oi_pt'));
		$oi_free = $this->nulo_se_vazio($this->input->post('oi_free'));
		$vivo_fixo = $this->nulo_se_vazio($this->input->post('vivo_fixo'));
		$vivo_pt = $this->nulo_se_vazio($this->input->post('vivo_pt'));
		$vivo_free = $this->nulo_se_vazio($this->input->post('vivo_free'));
		$tim_fixo = $this->nulo_se_vazio($this->input->post('tim_fixo'));
		$tim_pt = $this->nulo_se_vazio($this->input->post('tim_pt'));
		$tim_free = $this->nulo_se_vazio($this->input->post('tim_free'));

		$checkout_part_time = array (
			array ('fk_operadora' => 1, 'fk_checkout' => $id_checkout, 'fixo' => $claro_fixo, 'part_time' => $claro_pt, 'free' => $claro_free),
			array ('fk_operadora' => 2, 'fk_checkout' => $id_checkout, 'fixo' => $oi_fixo, 'part_time' => $oi_pt, 'free' => $oi_free),
			array ('fk_operadora' => 3, 'fk_checkout' => $id_checkout, 'fixo' => $vivo_fixo, 'part_time' => $vivo_pt, 'free' => $vivo_free),
			array ('fk_operadora' => 4, 'fk_checkout' => $id_checkout, 'fixo' => $tim_fixo, 'part_time' => $tim_pt, 'free' => $tim_free)
		);

		$this->model_checkout->checkout_part_time($checkout_part_time);

		//Campo checkout_campanha
		$con_claro_campanha = $this->nulo_se_vazio($this->input->post('con_claro_campanha'));
		$con_claro_produto = $this->nulo_se_vazio($this->input->post('con_claro_produto'));
		$con_claro_periodo = $this->nulo_se_vazio($this->input->post('con_claro_periodo'));
		$con_oi_campanha = $this->nulo_se_vazio($this->input->post('con_oi_campanha'));
		$con_oi_produto = $this->nulo_se_vazio($this->input->post('con_oi_produto'));
		$con_oi_periodo = $this->nulo_se_vazio($this->input->post('con_oi_periodo'));
		$con_vivo_campanha = $this->nulo_se_vazio($this->input->post('con_vivo_campanha'));
		$con_vivo_produto = $this->nulo_se_vazio($this->input->post('con_vivo_produto'));
		$con_vivo_periodo = $this->nulo_se_vazio($this->input->post('con_vivo_periodo'));
		$con_tim_campanha = $this->nulo_se_vazio($this->input->post('con_tim_campanha'));
		$con_tim_produto = $this->nulo_se_vazio($this->input->post('con_tim_produto'));
		$con_tim_periodo = $this->nulo_se_vazio($this->input->post('con_tim_periodo'));

		$checkout_campanha = array (
			array ('fk_operadora' => 1, 'fk_checkout' => $id_checkout, 'campanha' => $con_claro_campanha, 'produto' => $con_claro_produto, 'periodo' => $con_claro_periodo),
		 	array ('fk_operadora' => 2, 'fk_checkout' => $id_checkout, 'campanha' => $con_oi_campanha, 'produto' => $con_oi_produto, 'periodo' => $con_oi_periodo),
		 	array ('fk_operadora' => 3, 'fk_checkout' => $id_checkout, 'campanha' => $con_vivo_campanha, 'produto' => $con_vivo_produto, 'periodo' => $con_vivo_periodo),
		 	array ('fk_operadora' => 4, 'fk_checkout' => $id_checkout, 'campanha' => $con_tim_campanha, 'produto' => $con_tim_produto, 'periodo' => $con_tim_periodo)
		 );

		$this->model_checkout->checkout_campanha($checkout_campanha);

		//Campo checkout_marketing
		$mm_claro = $this->nulo_se_vazio($this->input->post('mm_claro'));
		$mm_oi = $this->nulo_se_vazio($this->input->post('mm_oi'));
		$mm_vivo = $this->nulo_se_vazio($this->input->post('mm_vivo'));
		$mm_tim = $this->nulo_se_vazio($this->input->post('mm_tim'));

		$checkout_marketing = array (
			array ('fk_operadora' => 1, 'fk_checkout' => $id_checkout, 'material' => $mm_claro),
			array ('fk_operadora' => 2, 'fk_checkout' => $id_checkout, 'material' => $mm_oi),
			array ('fk_operadora' => 3, 'fk_checkout' => $id_checkout, 'material' => $mm_vivo),
			array ('fk_operadora' => 4, 'fk_checkout' => $id_checkout, 'material' => $mm_tim)
		);

		$this->model_checkout->checkout_marketing($checkout_marketing);










		//Envio de E-mail

		$fk_usuario = $this->nulo_vazio($this->input->post('fk_usuario'));
		$fk_pdv = $this->nulo_vazio($this->input->post('fk_pdv'));

		$endereco_exato = $this->nulo_vazio($this->input->post('endereco_exato'));
		$nome_usuario = $this->nulo_vazio($this->input->post('nome_usuario'));
		$rede = $this->nulo_vazio($this->input->post('rede'));
		$filial = $this->nulo_vazio($this->input->post('filial'));
		$pdv = $this->nulo_vazio($this->input->post('pdv'));
		$codmod = $this->nulo_vazio($this->input->post('codmod'));

		$atendimento_telefonia = $this->marcado($this->input->post('atendimento_telefonia'));
		$atendimento_aberto = $this->marcado($this->input->post('atendimento_aberto'));
		$atendimento_mobile = $this->marcado($this->input->post('atendimento_mobile'));

		$qtd_vendedor = $this->nulo_vazio($this->input->post('qtd_vendedor'));
		$loja = $this->nulo_vazio($this->input->post('loja'));
		$setor = $this->nulo_vazio($this->input->post('setor'));
		$qts_assistentes = $this->nulo_vazio($this->input->post('qts_assistentes'));
		$qts_caixas = $this->nulo_vazio($this->input->post('qts_caixas'));

		$balcao_vitrine = $this->marcado($this->input->post('balcao_vitrine'));
		$balcao_degustador = $this->marcado($this->input->post('balcao_degustador'));

		$faturamento_pdv = $this->nulo_vazio($this->input->post('faturamento_pdv'));
		$faturamento_setor = $this->nulo_vazio($this->input->post('faturamento_setor'));
		$va_f1 = $this->nulo_vazio($this->input->post('va_f1'));
		$va_f2 = $this->nulo_vazio($this->input->post('va_f2'));
		$va_f3 = $this->nulo_vazio($this->input->post('va_f3'));
		$va_f4 = $this->nulo_vazio($this->input->post('va_f4'));
		$va_f5 = $this->nulo_vazio($this->input->post('va_f5'));
		$va_f6 = $this->nulo_vazio($this->input->post('va_f6'));
		$t1venda1 = $this->nulo_vazio($this->input->post('t1venda1'));
		$t1venda3 = $this->nulo_vazio($this->input->post('t1venda3'));
		$t1venda2 = $this->nulo_vazio($this->input->post('t1venda2'));
		$t1venda4 = $this->nulo_vazio($this->input->post('t1venda4'));

		$t2venda1 = $this->nulo_vazio($this->input->post('t2venda1'));
		$t2venda3 = $this->nulo_vazio($this->input->post('t2venda3'));
		$t2venda2 = $this->nulo_vazio($this->input->post('t2venda2'));
		$t2venda4 = $this->nulo_vazio($this->input->post('t2venda4'));

		//$nome_aparelho1 = $this->nulo_vazio($this->input->post('nome_aparelho1'));
		$valor1 = $this->nulo_vazio($this->input->post('valor1'));
		$media1 = $this->nulo_vazio($this->input->post('media1'));
		//$nome_aparelho2 = $this->nulo_vazio($this->input->post('nome_aparelho2'));
		$valor2 = $this->nulo_vazio($this->input->post('valor2'));
		$media2 = $this->nulo_vazio($this->input->post('media2'));
		//$nome_aparelho3 = $this->nulo_vazio($this->input->post('nome_aparelho3'));
		$valor3 = $this->nulo_vazio($this->input->post('valor3'));
		$media3 = $this->nulo_vazio($this->input->post('media3'));
		//$nome_aparelho4 = $this->nulo_vazio($this->input->post('nome_aparelho4'));
		$valor4 = $this->nulo_vazio($this->input->post('valor4'));
		$media4 = $this->nulo_vazio($this->input->post('media4'));

		// $claro_fixo = $this->nulo_vazio($this->input->post('claro_fixo'));
		// $claro_pt = $this->nulo_vazio($this->input->post('claro_pt'));
		// $claro_free = $this->nulo_vazio($this->input->post('claro_free'));

		// $vivo_fixo = $this->nulo_vazio($this->input->post('vivo_fixo'));
		// $vivo_pt = $this->nulo_vazio($this->input->post('vivo_pt'));
		// $vivo_free = $this->nulo_vazio($this->input->post('vivo_free'));

		// $oi_fixo = $this->nulo_vazio($this->input->post('oi_fixo'));
		// $oi_pt = $this->nulo_vazio($this->input->post('oi_pt'));
		// $oi_free = $this->nulo_vazio($this->input->post('oi_free'));

		// $tim_fixo = $this->nulo_vazio($this->input->post('tim_fixo'));
		// $tim_pt = $this->nulo_vazio($this->input->post('tim_pt'));
		// $tim_free = $this->nulo_vazio($this->input->post('tim_free'));

		$pc_agencia = $this->nulo_vazio($this->input->post('pc_agencia'));
		$pc_supervisor = $this->nulo_vazio($this->input->post('pc_supervisor'));
		$pc_promotor = $this->nulo_vazio($this->input->post('pc_promotor'));
		$pc_tempo_pdv = $this->nulo_vazio($this->input->post('pc_tempo_pdv'));

		$apresentacao = $this->nulo_vazio($this->input->post('apresentacao'));
		$conhecimento = $this->nulo_vazio($this->input->post('conhecimento'));
		$book = $this->nulo_vazio($this->input->post('book'));


		$con_claro_campanha = $this->nulo_vazio($this->input->post('con_claro_campanha'));
		$con_claro_produto = $this->nulo_vazio($this->input->post('con_claro_produto'));
		$con_claro_periodo = $this->nulo_vazio($this->input->post('con_claro_periodo'));
		$con_vivo_campanha = $this->nulo_vazio($this->input->post('con_vivo_campanha'));
		$con_vivo_produto = $this->nulo_vazio($this->input->post('con_vivo_produto'));
		$con_vivo_periodo = $this->nulo_vazio($this->input->post('con_vivo_periodo'));
		$con_oi_campanha = $this->nulo_vazio($this->input->post('con_oi_campanha'));
		$con_oi_produto = $this->nulo_vazio($this->input->post('con_oi_produto'));
		$con_oi_periodo = $this->nulo_vazio($this->input->post('con_oi_periodo'));
		$con_tim_campanha = $this->nulo_vazio($this->input->post('con_tim_campanha'));
		$con_tim_produto = $this->nulo_vazio($this->input->post('con_tim_produto'));
		$con_tim_periodo = $this->nulo_vazio($this->input->post('con_tim_periodo'));

		$mm_claro = $this->nulo_vazio($this->input->post('mm_claro'));
		$mm_vivo = $this->nulo_vazio($this->input->post('mm_vivo'));
		$mm_tim = $this->nulo_vazio($this->input->post('mm_tim'));
		$mm_oi = $this->nulo_vazio($this->input->post('mm_oi'));

		$obs = $this->nulo_vazio($this->input->post('obs'));

		$body = 
'
<h1>Checklist:</h1>
<hr><br>

<strong>Rede:</strong> '.$rede.'
<br>
<strong>Filial:</strong> '.$filial.'
<br>
<strong>PDV:</strong>  '.$pdv.'
<br>
<strong>Cod Mod:</strong>  '.$codmod.'
<br>

<hr> 
<h1>INFORMAÇÃO DO PDV:</h1> 
<h2>ATENDIMENTO</h2> 
<br>
<strong> Telefonia: </strong> '.$atendimento_telefonia.'
<br>
<strong> Aberto: </strong> '.$atendimento_aberto.'
<br>
<strong> Mobile: </strong> '.$atendimento_mobile.'
<br>

<br>
<strong>Qtd VENDEDOR:</strong>  '.$qtd_vendedor.'
<br>
<strong>Qtd Assistentes:</strong>  '.$qts_assistentes.'
<br>
<strong>Qtd Caixas:</strong>  '.$qts_caixas.'
<br>
<strong>Loja:</strong>  '.$loja.'
<br>
<strong>Setor:</strong>  '.$setor.'
<br>

<hr> 
<h2>BALCÃO & IMOBILIÁRIO</h2> 

<br>
<strong> Vitrine: </strong> '.$balcao_vitrine.'
<br>
<strong> Degustador: </strong> '.$balcao_degustador.'
<br>

<hr> 
<h1>FATURAMENTO:</h1> 

<br>
<strong>PDV Geral:</strong> R$ '.$faturamento_pdv.'
<br>
<strong>PDV Telefonia:</strong> R$ '.$faturamento_setor.'
<br>

<hr> 
<h1>Qtd. VENDAS DE APARELHOS:</h1> 
<br>
<strong>Até R$ 500,00:</strong> '.$va_f1.' 
<br>
<strong>R$ 501,00 a R$ 999,00:</strong>  '.$va_f2.' 
<br>
<strong>R$ 1.000,00 a R$ 1.499,00:</strong>  '.$va_f3.' 
<br>
<strong>R$ 1.500,00 a R$ 1.999,00:</strong>  '.$va_f4.' 
<br>
<strong>R$ 2.000,00 a R$ 2.499,00:</strong>  '.$va_f5.' 
<br>
<strong>Acima de R$ 2500,00:</strong>  '.$va_f6.' 
<br>

<hr> 
<h1>VENDA SEMANAL PRÉ-PAGO - PDV:</h1> 
<br>
<strong>CHIP Claro:</strong>  '.$t1venda1.'
<br>
<strong>CHIP Vivo:</strong>  '.$t1venda2.'
<br>
<strong>Chip TIM:</strong>  '.$t1venda3.'
<br>
<strong>Chip OI:</strong>  '.$t1venda4.'
<br>

<hr> 
<h1>VENDA SEMANAL CONTROLE - PDV:</h1> 
<br>
<strong>CHIP Claro:</strong>  '.$t2venda1.'
<br>
<strong>CHIP Vivo:</strong>  '.$t2venda2.'
<br>
<strong>Chip TIM:</strong>  '.$t2venda3.'
<br>
<strong>Chip OI:</strong>  '.$t2venda4.'
<br>

<hr> 
<h1>Aparelhos mais vendidos:</h1> 
<br>
<strong>Nome do aparelho:</strong> '.$aparelho1.' R$: '.$valor1.' Média: '.$media1.' 
<br>
<strong>Nome do aparelho:</strong> '.$aparelho2.' R$: '.$valor2.' Média: '.$media2.' 
<br>
<strong>Nome do aparelho:</strong> '.$aparelho3.' R$: '.$valor3.' Média: '.$media3.' 
<br>
<strong>Nome do aparelho:</strong> '.$aparelho4.' R$: '.$valor4.' Média: '.$media4.'
<br>

<hr> 
<h1>Claro:</h1> 
<br>
<strong> Fixo: </strong> '.$claro_fixo.'
<br>
<strong> PART TIME: </strong> '.$claro_pt.'
<br>
<strong> Free: </strong> '.$claro_free.'
<br>

<hr> 
<h1>Oi:</h1> 
<br>
<strong> Fixo: </strong> '.$oi_fixo.'
<br>
<strong> PART TIME: </strong> '.$oi_pt.'
<br>
<strong> Free: </strong> '.$oi_free.'
<br>

<hr> 
<h1>Vivo:</h1> 
<br>
<strong> Fixo: </strong> '.$vivo_fixo.'
<br>
<strong> PART TIME: </strong> '.$vivo_pt.'
<br>
<strong> Free: </strong> '.$vivo_free.'
<br>

<hr> 
<h1>Tim:</h1> 
<br>
<strong> Fixo: </strong> '.$tim_fixo.'
<br>
<strong> PART TIME: </strong> '.$tim_pt.'
<br>
<strong> Free: </strong> '.$tim_free.'
<br>

<hr> 
<h1>INFORMAÇÕES PROMOTOR:</h1> 
<br>
<strong>Promotor:</strong>  '.$pc_promotor.'
<br>
<strong>Tempo de PDV:</strong>  '.$pc_tempo_pdv.'
<br>
<strong>Agência:</strong>  '.$pc_agencia.'
<br>
<strong>Supervisor:</strong>  '.$pc_supervisor.'
<br>


<hr> 
<h1>Apresentação (Uniforme & Crachá):</h1> 
<br>
<strong> MARCADO: </strong> '.$apresentacao.'
<br>

<hr> 
<h1>Conhecimento (Ofertas Claro):</h1> 
<br>
<strong> MARCADO: </strong> '.$conhecimento.'
<br>


<hr> 
<h1>Book (Preenchimento):</h1> 
<br>
<strong> MARCADO: </strong> '.$book.'
<br> 

<h1>CAMPANHAS</h1> 

<hr> 
<h1>Checklist CLARO:</h1> 
<br>
<strong>Campanha:</strong>  '.$con_claro_campanha.'
<br>
<strong>Produto:</strong>  '.$con_claro_produto.'
<br>
<strong>Período:</strong>  '.$con_claro_periodo.'
<br>

<h1>Checklist Oi:</h1> 
<br>
<strong>Campanha:</strong>  '.$con_oi_campanha.'
<br>
<strong>Produto:</strong>  '.$con_oi_produto.'
<br>
<strong>Período:</strong>  '.$con_oi_periodo.'
<br>

<h1>Checklist Vivo:</h1> 
<br>
<strong>Campanha:</strong>  '.$con_vivo_campanha.'
<br>
<strong>Produto:</strong>  '.$con_vivo_produto.'
<br>
<strong>Período:</strong>  '.$con_vivo_periodo.'
<br>

<h1>Checklist Tim:</h1> 
<br>
<strong>Campanha:</strong>  '.$con_tim_campanha.'
<br>
<strong>Produto:</strong>  '.$con_tim_produto.'
<br>
<strong>Período:</strong>  '.$con_tim_periodo.'
<br>

<hr>

<h1>MATERIAIS MERCHANDISING:</h1> 
<br>
<strong>Claro:</strong>  '.$mm_claro.'
<br>
<strong>Oi:</strong>  '.$mm_oi.'
<br>
<strong>Vivo:</strong>  '.$mm_vivo.'
<br>
<strong>Tim:</strong>  '.$mm_tim.'
<br>

<hr> 
<h1>Observação PDV:</h1> '.$obs.'
<br> 

<hr> 
<h1>Localização exata do envio do check-out:</h1> '.$endereco_exato.'
<br> 

<h1>Usuário: </h1> '.$nome_usuario.'
';
		//echo $body;
		// Detalhes do Email.
		
		$this->email->from('clarogn@megamil.net', 'Checkout CLAROGN'); 
		$this->email->to("comunicado.varejosp@claro.com.br"); 

		$list = array('guilherme@naville.marketing');
		$this->email->bcc($list); 

		$this->email->subject('Checkout Enviado pelo Aplicativo ClaroGN'); 
		$this->email->message($body); 
		$this->email->set_mailtype("html");

		// Enviar... 
		if($this->input->post('checkpoint') != 1){

				if($fk_pdv == 1) { // Se estiver adicionando um novo PDV
					$dadosEsteMes = $this->model_checkout->dadosEsteMes(0,0);
					$dadosEstaSemana = $this->model_checkout->dadosEstaSemana(0,0);
				} else {
					$dadosEsteMes = $this->model_checkout->dadosEsteMes($fk_pdv,$fk_usuario);
					$dadosEstaSemana = $this->model_checkout->dadosEstaSemana($fk_pdv,$fk_usuario);
				}

			$dados = array(

				'sucesso' => 1,// Encerrado
				'endereco_exato' => $endereco_exato,
				'fk_usuario' => $fk_usuario,
				'fk_pdv' => $fk_pdv,
				'nome_usuario' => $nome_usuario,
				'rede' => $rede,
				'redes' => $this->model_checkout->listarRedes(),
				'cod_agente' => $codmod,
				'filial' => $filial,
				'marcas' => $this->model_checkout->listarMarcas(),
				'dadosEsteMes' => $dadosEsteMes, 
				'dadosEstaSemana' => $dadosEstaSemana

			);
			$this->load->view('formulario',$dados);


	} else {

		redirect('controller_checkout/formulario?fk_pdv='.$fk_pdv.'&fk_usuario='.$fk_usuario.'&rede='.$rede.'&filial='.$filial.'&cod_agente='.$codmod.'&endereco_exato='.$endereco_exato.'&nome_usuario='.$nome_usuario.'&sucesso=2');

		//$this->load->view('formulario',$dados);
	}
		

	}

	public function excel_export() {	

		// Definimos o nome do arquivo que será exportado
		$arquivo = 'checkout.xls';
		
		// Criamos uma tabela HTML com o formato da planilha
		$html = "<meta http-equiv=\"content-type\" content=\"application/xhtml+xml; charset=UTF-8\" />
		<table class=\"mdl-data-table mdl-js-data-table mdl-shadow--8dp\">
		  <thead>
		    <tr>
		      <th>ID checkout</th>
		      <th>Data envio</th>
		      <th>Usuário</th>
		      <th>Endereço de origem</th>

		      <th>Rede</th>
		      <th>Filial</th>
		      <th>PDV</th>
		      <th>COD. MOD.</th>

		      <th>Telefonia</th>
		      <th>Aberto</th>
		      <th>Mobile</th>
		      <th>Qtd Vendedor</th>
		      <th>Loja</th>
		      <th>Qts Assistentes</th>
		      <th>Qts Caixas</th>
		      <th>setor</th>
		      <th>Balcão Vitrine</th>
		      <th>Balcão Degustador</th>

		      <th>Faturamento Pdv</th>
		      <th>Faturamento Setor</th>
		      <th>Até 500,00</th>
		      <th>501 a 999</th>
		      <th>1000 a 1499</th>
		      <th>1500 a 1999</th>
		      <th>2000 a 2499</th>
		      <th>Acima de 2500</th>

		      <th>PRE Chip Claro</th>
		      <th>PRE Chip Oi</th>
		      <th>PRE Chip Vivo</th>
		      <th>PRE Chip Tim</th>
		      <th>Controle Chip Claro</th>
		      <th>Controle Chip OI</th>
		      <th>Controle Chip Vivo</th>
		      <th>Controle Chip Tim</th>

		      <th>Marca</th>
		      <th>Aparelho</th>
		      <th>Valor</th>
		      <th>Média</th>
		      <th>Marca</th>
		      <th>Aparelho</th>
		      <th>Valor</th>
		      <th>Média</th>
		      <th>Marca</th>
		      <th>Aparelho</th>
		      <th>Valor</th>
		      <th>Média</th>
		      <th>Marca</th>
		      <th>Aparelho</th>
		      <th>Valor</th>
		      <th>Média</th>

		      <th>Claro Fixo</th>
		      <th>Claro Part Ttime</th>
		      <th>Claro Free</th>
		      <th>Oi Fixo</th>
		      <th>Oi Part Ttime</th>
		      <th>Oi Free</th>
		      <th>Vivo Fixo</th>
		      <th>Vivo Part Ttime</th>
		      <th>Vivo Free</th>
		      <th>Tim Fixo</th>
		      <th>Tim Part Ttime</th>
		      <th>Tim Free</th>


		      <th>Promotor</th>
		      <th>Tempo</th>
		      <th>Agência</th>
		      <th>Supervisor</th>

		      <th>Apresentação</th>
		      <th>Conhecimento</th>
		      <th>Book</th>

		      <th>Claro campanha</th>
		      <th>Claro produto</th>
		      <th>Claro Período</th>
		      <th>Oi campanha</th>
		      <th>Oi produto</th>
		      <th>Oi Período</th>
		      <th>Vivo campanha</th>
		      <th>Vivo produto</th>
		      <th>Vivo Período</th>
		      <th>Tim campanha</th>
		      <th>Tim produto</th>
		      <th>Tim Período</th>

		      <th>Claro Material</th>
		      <th>Oi Material</th>
		      <th>Vivo Material</th>
		      <th>Tim Material</th>

		      <th>Observações</th>
		    </tr>
		  </thead>
		  <tbody>";

		  $usuario = $this->input->post('fk_usuario');
		  $de = $this->input->post('de');
		  $ate = $this->input->post('ate');

		  $checkouts = $this->model_checkout->getCheckout($usuario,$de,$ate);

		  	$colorir = false;

		    foreach ($checkouts as $checkout) {

		    	if($colorir) {
		    		$html .= '<tr style="background-color: #EEE5DE;">';	
		    		$colorir = !$colorir;
		    	} else {
		    		$html .= '<tr>';
		    		$colorir = !$colorir;
		    	}
		    	
		    	//Checkout
				$html .= '<td>'.$this->nulo_vazio($checkout->id_checkout).'</td>';
				$html .= '<td>'.$this->nulo_vazio($checkout->data_checkout).'</td>';
				$html .= '<td>'.$this->nulo_vazio($checkout->usuario).'</td>';
				$html .= '<td>'.$this->nulo_vazio($checkout->endereco_exato).'</td>';
				//PDV - Checkout
				$html .= '<td>'.$this->nulo_vazio($checkout->rede).'</td>';
				$html .= '<td>'.$this->nulo_vazio($checkout->filial).'</td>';
				$html .= '<td>'.$this->nulo_vazio($checkout->rede).'</td>';
				$html .= '<td>'.$this->nulo_vazio($checkout->cod_agente).'</td>';
				//Atendimento
				$html .= '<td>'.$this->marcado($checkout->atendimento_telefonia).'</td>';
				$html .= '<td>'.$this->marcado($checkout->atendimento_aberto).'</td>';
				$html .= '<td>'.$this->marcado($checkout->atendimento_mobile).'</td>';
				$html .= '<td>'.$this->nulo_vazio($checkout->qtd_vendedor).'</td>';
				$html .= '<td>'.$this->nulo_vazio($checkout->loja).'</td>';
				$html .= '<td>'.$this->nulo_vazio($checkout->qts_assistentes).'</td>';
				$html .= '<td>'.$this->nulo_vazio($checkout->qts_caixas).'</td>';
				$html .= '<td>'.$this->nulo_vazio($checkout->setor).'</td>';
				$html .= '<td>'.$this->marcado($checkout->balcao_vitrine).'</td>';
				$html .= '<td>'.$this->marcado($checkout->balcao_degustador).'</td>';
				//Faturamento
				$html .= '<td>'.$this->nulo_vazio($checkout->faturamento_pdv).'</td>';
				$html .= '<td>'.$this->nulo_vazio($checkout->faturamento_setor).'</td>';
				$html .= '<td>'.$this->nulo_vazio($checkout->va_f1).'</td>';
				$html .= '<td>'.$this->nulo_vazio($checkout->va_f2).'</td>';
				$html .= '<td>'.$this->nulo_vazio($checkout->va_f3).'</td>';
				$html .= '<td>'.$this->nulo_vazio($checkout->va_f4).'</td>';
				$html .= '<td>'.$this->nulo_vazio($checkout->va_f5).'</td>';
				$html .= '<td>'.$this->nulo_vazio($checkout->va_f6).'</td>';
				//Informações de vendas
				$informacao_vendas = $this->model_checkout->get_informacao_vendas($checkout->id_checkout);

				foreach ($informacao_vendas as $iv) {

					$html .= '<td>'.$this->nulo_vazio($iv->venda).'</td>';

				}
				//Mais vendidos
				$mais_vendidos = $this->model_checkout->get_mais_vendidos($checkout->id_checkout);

				foreach ($mais_vendidos as $mv) {

					$html .= '<td>'.$this->nulo_vazio($mv->marca).'</td>';
					$html .= '<td>'.$this->nulo_vazio($mv->modelo).'</td>';
					$html .= '<td>'.$this->nulo_vazio($mv->valor).'</td>';
					$html .= '<td>'.$this->nulo_vazio($mv->media).'</td>';

				}
				//Part time
				$part_time = $this->model_checkout->get_part_time($checkout->id_checkout);

				foreach ($part_time as $ptime) {

					$html .= '<td>'.$this->nulo_vazio($ptime->fixo).'</td>';
					$html .= '<td>'.$this->nulo_vazio($ptime->part_time).'</td>';
					$html .= '<td>'.$this->nulo_vazio($ptime->free).'</td>';

				}

				//Promotor
				$html .= '<td>'.$this->nulo_vazio($checkout->promotor).'</td>';
				$html .= '<td>'.$this->nulo_vazio($checkout->tempo_pdv).'</td>';
				$html .= '<td>'.$this->nulo_vazio($checkout->agencia).'</td>';
				$html .= '<td>'.$this->nulo_vazio($checkout->supervisor).'</td>';
				//AtentePN
				if($checkout->apresentacao == 'a'){
					$html .= '<td>Atende</td>';
				} else if ($checkout->apresentacao == 'p') {
					$html .= '<td>Atende Parcial</td>';
				} else {
					$html .= '<td>Não atende</td>';
				}

				if($checkout->conhecimento == 'a'){
					$html .= '<td>Atende</td>';
				} else if ($checkout->conhecimento == 'p') {
					$html .= '<td>Atende Parcial</td>';
				} else {
					$html .= '<td>Não atende</td>';
				}

				if($checkout->book == 'a'){
					$html .= '<td>Atende</td>';
				} else if ($checkout->book == 'p') {
					$html .= '<td>Atende Parcial</td>';
				} else {
					$html .= '<td>Não atende</td>';
				}

				//Campanha
				$campanhas = $this->model_checkout->get_campanha($checkout->id_checkout);

				foreach ($campanhas as $campanha) {

					$html .= '<td>'.$this->nulo_vazio($campanha->campanha).'</td>';
					$html .= '<td>'.$this->nulo_vazio($campanha->produto).'</td>';
					$html .= '<td>'.$this->nulo_vazio($campanha->periodo).'</td>';

				}
				//Marketing
				$marketing = $this->model_checkout->get_marketing($checkout->id_checkout);
				foreach ($marketing as $m) {

					$html .= '<td>'.$this->nulo_vazio($m->material).'</td>';
				}

				$html .= '<td>'.$this->nulo_vazio($checkout->observacoes).'</td>';

			} 

		  $html .= "</tbody>
		</table>";

		// Configurações header para forçar o download
		header ("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
		header ("Cache-Control: must-revalidate, post-check=0, pre-check=0"); 
		header ("Content-Transfer-Encoding: binary"); 
		header ("Content-Type: application/vnd.ms-excel"); 
		header ("Expires: 0"); 
		header ("Content-Disposition: attachment; filename=\"{$arquivo}\"");
		header ("Content-Description: PHP Generated Data");

		// Envia o conteúdo do arquivo
		chr(255).chr(254).iconv("UTF-8", "UTF-16LE//IGNORE", $html); 
		echo $html;
		exit;



	}

	public function marcado($campo = null) {

		if(is_null($campo)  || $campo == '') {
			return '<div style="background-color: #D3D3D3;"> Não Marcado </div>';
		} else {
			return "Marcado";
		}

	}

	public function nulo_vazio($campo = null) {

		if(is_null($campo)  || $campo == '') {
			return '<div style="background-color: #D3D3D3;"> Em Branco </div>';
		} else {
			return $campo;
		}

	}

	//Usado para gravar null no banco.
	public function nulo_se_vazio($campo = null) {

		if(is_null($campo) || $campo == '') {
			return null;
		} else {
			return $campo;
		}

	}

	//Usado para evitar o erro de tentar gravar ON no banco
	public function true_se_on($campo = null) {

		if(is_null($campo)  || $campo == '') {
			return null;
		} else {
			return true;
		}

	}

}