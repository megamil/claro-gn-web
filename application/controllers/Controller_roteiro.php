<?php defined('BASEPATH') OR exit('No direct script access allowed');

	class Controller_roteiro extends CI_Controller {


		public function aprovacao(){

			$roteiro = $this->uri->segment(3);
			$status = $this->uri->segment(4);

			$this->model_roteiro->aprovar($roteiro,$status);

			redirect('main/redirecionar/editar-view_aprovacao_roteiro/'.$roteiro);


		}

		
	}