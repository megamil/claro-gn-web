<?php defined('BASEPATH') OR exit('No direct script access allowed');

	class controller_inputvendas extends CI_Controller {


	public function formulario () {

		//lógica para listar campos já existentes.

		$filial = $this->model_inputvendas->listar_filiais();

		$this->load->view('formulario_iv',array('filial' => $filial));

	}

	public function editar_input(){

		$dados = explode('-', $this->input->post('filial'));

		$filial = $dados[0];
		$fk_usuario = $dados[2];

		$data = explode('/', $this->input->post('data_input'));

		$data_ = $data[2].'-'.$data[1].'-'.$data[0];

		$dados = array(

			'fk_usuario' => $fk_usuario,
			'filial' => $filial,

			'data_input'    => $data_,
			'pre'            => $this->input->post('pre'),
			'controle_facil' => $this->input->post('controle_facil'),
			'controle_giga'  => $this->input->post('controle_giga'),
			'recarga'        => $this->input->post('recarga'),
			'migracao'       => $this->input->post('migracao'),
			'hc_oficial'     => $this->input->post('hc_oficial'),
			'hc_campo'       => $this->input->post('hc_campo'),
			'aspectos'       => $this->input->post('aspectos')


		);

		$this->model_inputvendas->update($dados, $this->input->post('id_input_vendas'));

		$this->session->set_flashdata('tipo','sucesso');
		$this->session->set_flashdata('titulo','Editado com sucesso!');
		$this->session->set_flashdata('mensagem','input editado com sucesso.');

		redirect('main/redirecionar/editar-view_editar_inputvendas/'.$this->input->post('id_input_vendas'));


	}

	public function ajax_geral(){

		$dados = $this->model_inputvendas->ajax_geral_inputvendas(
			$this->input->post('ano'),
			$this->input->post('mes'),
			$this->input->post('habitado'));


		echo '<div class="mdl-grid contorno novo-background"  align="left">

                <div class="mdl-cell mdl-cell--5-col novo-background">

                    <table class="semDataTable cor">
				<thead>
					<tr>
						<th></th>
						<th>Pré Pago</th>
						<th>Controle</th>
						<th>Recarga</th>
						<th>HC Oficial</th>
						<th>HC em Campo</th>
						<th>% Campo</th>
						<th>PHC Pré</th>
						<th>PHC Controle</th>
						<th>PHC Recarga</th>
					</tr>
				</thead>
				<tbody style="color: white">';

					foreach ($dados['geral'] as $value) {
						echo '<tr>';

						echo '<td style="background-color: #b5c0d4; color: #000; font-weight: 600">'.$value->filial.'</td>';
						echo '<td>'.$value->pre.'</td>';
						echo '<td>'.$value->controle.'</td>';
						echo '<td>'.$value->recarga.'</td>';
						echo '<td>'.$value->hc_oficial.'</td>';
						echo '<td>'.$value->hc_campo.'</td>';
						echo '<td>'.round(($this->divisao(($value->hc_campo * 100 ),$value->hc_oficial))).' %</td>';
						echo '<td>'.round(($this->divisao($value->pre,$value->hc_campo))).'</td>';
						echo '<td>'.round(($this->divisao($value->controle,$value->hc_campo))).'</td>';
						echo '<td>'.round(($this->divisao($value->recarga,$value->hc_campo))).'</td>';

						echo '</tr>';
					} 

				echo'</tbody>
			</table>

			</div>

			<div class="mdl-cell mdl-cell--5-col" align="left">

                    <table class="semDataTable" align="left">
				<thead>
					<tr>
						<th></th>
						<th>Controle Total</th>
						<th>Controle Fácil</th>
						<th>%</th>
						<th>Controle Boleto</th>
						<th>%</th>
					</tr>
				</thead>
				<tbody style="color: white">';
					foreach ($dados['geral_controle'] as $value) {
						echo '<tr>';

						echo '<td style="background-color: #b5c0d4; color: #000; font-weight: 600">'.$value->filial.'</td>';
						echo '<td>'.$value->controle_total.'</td>';
						echo '<td>'.$value->controle_facil.'</td>';
						echo '<td>'.round(($this->divisao(($value->controle_facil * 100 ),($value->controle_giga+$value->controle_facil)))).' %</td>';
						echo '<td>'.$value->controle_giga.'</td>';
						echo '<td>'.round(($this->divisao(($value->controle_giga * 100 ),($value->controle_giga+$value->controle_facil)))).' %</td>';

						echo '</tr>';

					}
				
				echo'</tbody>
			</table>

			</div>	
		</div>

		 <div class="mdl-grid contorno novo-background"  align="left">
                <div class="mdl-cell mdl-cell--10-col novo-background" align="left">

                    <h4 style="color: white">PRÉ PAGO</h4>

                    <table class="semDataTable" style="width: 100%">
				<thead>
					<tr>
						<th></th>';
						for ($i=1; $i < 32; $i++) { 
							echo '<th>'.$i.'</th>';
						}
					
						echo '<th>TOTAL</th>
					</tr>
				</thead>
				<tbody style="color: white">';
					
					

						$array_f1 = array();
						$array_f2 = array();

						echo '<tr>';
						echo '<td style="background-color: #b5c0d4; color: #000; font-weight: 600">F1</td>';

						for ($i=1; $i < 32; $i++) { 
							$valor = 0;

							foreach ($dados['pre_f1'] as $f1) {
								if ($f1->data == $i) {
									$valor = $f1->pre;
								}
							}

							array_push($array_f1, $valor);
							echo '<td>'.$valor.'</td>';
						}

						echo '<td>'.array_sum($array_f1).'</td>';
						echo '</tr>';

						//F2
						echo '<tr>';
						echo '<td style="background-color: #b5c0d4; color: #000; font-weight: 600">F2</td>';

						for ($i=1; $i < 32; $i++) { 
							$valor = 0;

							foreach ($dados['pre_f2'] as $f2) {
								if ($f2->data == $i) {
									$valor = $f2->pre;
								}
							}

							array_push($array_f2, $valor);
							echo '<td>'.$valor.'</td>';
						}

						echo '<td>'.array_sum($array_f2).'</td>';
						echo '</tr>';

						//CANAL
						echo '<tr>';
						echo '<td style="background-color: #b5c0d4; color: #000; font-weight: 600">CANAL</td>';

						for ($i=0; $i < 31; $i++) { 
							
							echo '<td>'.($array_f1[$i] + $array_f2[$i]).'</td>';

						}

						echo '<td>'.(array_sum($array_f1) + array_sum($array_f2)).'</td>';
						echo '</tr>';

				echo '</tbody>
				</table>

			</div>
		</div>

		<div class="mdl-grid contorno novo-background"  align="left">
                <div class="mdl-cell mdl-cell--10-col " align="left">
                    <h4 style="color: white">CONTROLE TOTAL</h4>

                    <table class="semDataTable" style="width: 100%">
				<thead>
					<tr>
						<th></th>';

						for ($i=1; $i < 32; $i++) { 
							echo '<th>'.$i.'</th>';
						}
					
						echo '<th>TOTAL</th>
					</tr>
				</thead>
				<tbody style="color: white">';
					
					

						$array_f1 = array();
						$array_f2 = array();

						echo '<tr>';
						echo '<td style="background-color: #b5c0d4; color: #000; font-weight: 600">F1</td>';

						for ($i=1; $i < 32; $i++) { 
							$valor = 0;

							foreach ($dados['controle_f1'] as $f1) {
								if ($f1->data == $i) {
									$valor = $f1->controle;
								}
							}

							array_push($array_f1, $valor);
							echo '<td>'.$valor.'</td>';
						}

						echo '<td>'.array_sum($array_f1).'</td>';
						echo '</tr>';

						//F2
						echo '<tr>';
						echo '<td style="background-color: #b5c0d4; color: #000; font-weight: 600">F2</td>';

						for ($i=1; $i < 32; $i++) { 
							$valor = 0;

							foreach ($dados['controle_f2'] as $f2) {
								if ($f2->data == $i) {
									$valor = $f2->controle;
								}
							}

							array_push($array_f2, $valor);
							echo '<td>'.$valor.'</td>';
						}

						echo '<td>'.array_sum($array_f2).'</td>';
						echo '</tr>';

						//CANAL
						echo '<tr>';
						echo '<td style="background-color: #b5c0d4; color: #000; font-weight: 600">CANAL</td>';

						for ($i=0; $i < 31; $i++) { 
							
							echo '<td>'.($array_f1[$i] + $array_f2[$i]).'</td>';

						}

						echo '<td>'.(array_sum($array_f1) + array_sum($array_f2)).'</td>';
						echo '</tr>';




						echo'</tbody>
						</table>

					</div>
				</div>';

	}

	public function ajax_f1_1(){

		$dados = $this->model_inputvendas->ajaxF1_1(
			$this->input->post('ano'),
			$this->input->post('mes'),
			$this->input->post('habitado'));


		echo '<div class="mdl-grid contorno novo-background"  align="left">

                <h4 style="color:#fff;">Filial 1</h4>

                <div class="mdl-cell mdl-cell--12-col">

                    <table class="semDataTable" width="100%">
			<thead>
				<tr>
					<th></th>
					<th>Pré Pago</th>
					<th>Controle</th>
					<th>Recarga</th>
					<th>HC Oficial</th>
					<th>HC em Campo</th>
					<th>% Campo</th>
					<th>PHC Pré</th>
					<th>PHC Controle</th>
					<th>PHC Recarga</th>
				</tr>
			</thead>
			<tbody>';

				$controle_prepago_count = array();
				$controle_controle_count = array();
				$controle_recarga_count = array();
				$controle_hc_oficial_count = array();
				$controle_hc_campo_count = array();

				foreach ($dados['geral_usuarios_f1'] as $value) {
					echo '<tr>';

					echo '<td style="background-color: #b5c0d4; color: #000; font-weight: 600">'.$value->usuario.'</td>';
					echo '<td>'.$value->prepago.'</td>';
					echo '<td>'.$value->controle.'</td>';
					echo '<td>'.$value->recarga.'</td>';
					echo '<td>'.$value->hc_oficial.'</td>';
					echo '<td>'.$value->hc_campo.'</td>';
					echo '<td>'.round($this->divisao(($value->hc_campo * 100 ),$value->hc_oficial)).'%</td>';

					echo '<td>'.round($this->divisao(($value->prepago * 100),$value->hc_campo)).'%</td>';
					echo '<td>'.round($this->divisao(($value->controle * 100),$value->hc_campo)).'%</td>';
					echo '<td>'.round($this->divisao(($value->recarga * 100),$value->hc_campo)).'%</td>';

					echo '</tr>';

					array_push($controle_prepago_count,$value->prepago);
					array_push($controle_controle_count,$value->controle);
					array_push($controle_recarga_count,$value->recarga);
					array_push($controle_hc_oficial_count,$value->hc_oficial);
					array_push($controle_hc_campo_count,$value->hc_campo);


				} 

				$prepago_tbl1 = array_sum($controle_prepago_count);
				$controle_tbl1 = array_sum($controle_controle_count);
				$recarga_tbl1 = array_sum($controle_recarga_count);
				$hc_oficial_tbl1 = array_sum($controle_hc_oficial_count);
				$hc_campo_tbl1 = array_sum($controle_hc_campo_count);

				echo '<tr><td style="background-color: #000; color: #000; font-weight: 600" >Total Filial:</td>
						<td>'.$prepago_tbl1.'</td>
						<td>'.$controle_tbl1.'</td>
						<td>'.$recarga_tbl1.'</td>
						<td>'.$hc_oficial_tbl1.'</td>
						<td>'.$hc_campo_tbl1.'</td>
						<td>'.round($this->divisao(($hc_campo_tbl1 * 100 ),$hc_oficial_tbl1)).'%</td>
						<td>'.round($this->divisao(($prepago_tbl1 * 100),$hc_campo_tbl1)).'%</td>
						<td>'.round($this->divisao(($controle_tbl1 * 100),$hc_campo_tbl1)).'%</td>
						<td>'.round($this->divisao(($recarga_tbl1 * 100),$hc_campo_tbl1)).'%</td>
				</tr>';


				echo '
				</tbody>
			</table>

			</div>

		</div>';
	}

	public function ajax_f1_2(){

		$dados = $this->model_inputvendas->ajaxF1_2(
			$this->input->post('ano'),
			$this->input->post('mes'),
			$this->input->post('habitado'));

		echo '<div align="left">

                    <table class="semDataTable">
				<thead>
					<tr>
						<th></th>
						<th>Controle Total</th>
						<th>Controle Fácil</th>
						<th>%</th>
						<th>Controle Boleto</th>
						<th>%</th>
					</tr>
				</thead>
				<tbody class="novo-background">';
					
					
					$controle_facil_count = array();
					$controle_giga_count = array();

					foreach ($dados['geral_controle_usuario_f1'] as $value) {
						echo '<tr>';

						echo '<td>'.$value->usuario.'</td>';
						echo '<td>'.$value->controle_total.'</td>';
						echo '<td>'.$value->controle_facil.'</td>';
						echo '<td>'.round(($this->divisao(($value->controle_facil * 100 ),($value->controle_giga+$value->controle_facil)))).' %</td>';
						echo '<td>'.$value->controle_giga.'</td>';
						echo '<td>'.round(($this->divisao(($value->controle_giga * 100 ),($value->controle_giga+$value->controle_facil)))).' %</td>';

						echo '</tr>';

						array_push($controle_facil_count,$value->controle_facil);
						array_push($controle_giga_count,$value->controle_giga);
					} 

					$total_tbl2 = (array_sum($controle_facil_count) + array_sum($controle_giga_count));
					$total_facil_tbl2 = array_sum($controle_facil_count);
					$total_boleto_tbl2 = array_sum($controle_giga_count);

					echo '<tr><td>Total Filial:</td>
					<td>'.$total_tbl2.'</td>
					<td>'.$total_facil_tbl2.'</td>
					<td>'.($this->divisao(($total_facil_tbl2  * 100),$total_tbl2)).' %</td>
					<td>'.$total_boleto_tbl2.'</td>
					<td>'.($this->divisao(($total_boleto_tbl2  * 100),$total_tbl2)).' %</td></tr>';


					
				echo '</tbody>
			</table>

			</div>';	


	}

	public function grafico_controles(){
			
		$dados = $this->model_inputvendas->graficosControles(
			$this->input->post('ano'),
			$this->input->post('mes'),
			$this->input->post('habitado'),
			$this->input->post('filial'));

		$resultado['boleto'] = $dados->boleto;
		$resultado['facil'] = $dados->facil;


		header('Content-Type: application/json');
		echo json_encode($resultado);	


	}


	public function ajax_f1_3(){

		$dados = $this->model_inputvendas->ajaxF1_3(
			$this->input->post('ano'),
			$this->input->post('mes'),
			$this->input->post('habitado'));


		$this->print_lista('Filial 1 Pré Pago',$dados['usuarios_iv'],$dados['detalhado_usuario_pre_f1'],false);
		$this->print_lista('Filial 1 Controle Total',$dados['usuarios_iv'],$dados['detalhado_usuario_total_f1'],false);
		$this->print_lista('Filial 1 Controle Fácil',$dados['usuarios_iv'],$dados['detalhado_usuario_facil_f1'],false);
		$this->print_lista('Filial 1 Controle Boleto',$dados['usuarios_iv'],$dados['detalhado_usuario_giga_f1'],false);

		$this->print_lista('REPRESENTATIVIDADE % - CONTROLE FÁCIL',$dados['usuarios_iv'],$dados['perc_facil_f1'],true);
		$this->print_lista('REPRESENTATIVIDADE % - CONTROLE BOLETO',$dados['usuarios_iv'],$dados['perc_giga_f1'],true);

		$this->print_lista('Filial 1 Recarga',$dados['usuarios_iv'],$dados['detalhado_usuario_recarga_f1'],false);
		$this->print_lista('Filial 1 Migração',$dados['usuarios_iv'],$dados['detalhado_usuario_migracao_f1'],false);
		$this->print_lista('Filial 1 HC Oficial',$dados['usuarios_iv'],$dados['detalhado_usuario_hc_oficial_f1'],false);
		$this->print_lista('Filial 1 HC Em Campo',$dados['usuarios_iv'],$dados['detalhado_usuario_hc_campo_f1'],false);

		$this->print_lista('REPRESENTATIVIDADE % - HCs EM CAMPO',$dados['usuarios_iv'],$dados['perc_campo_f1'],true);
		$this->print_lista('PHC - PRÉ PAGO',$dados['usuarios_iv'],$dados['perc_pre_f1'],true);
		$this->print_lista('PHC - CONTROLE',$dados['usuarios_iv'],$dados['perc_controle_f1'],true);
		$this->print_lista('PHC - RECARGA',$dados['usuarios_iv'],$dados['perc_recarga_f1'],true);



	}

	public function ajax_f2_1(){

		$dados = $this->model_inputvendas->ajaxF2_1(
			$this->input->post('ano'),
			$this->input->post('mes'),
			$this->input->post('habitado'));


		echo '<div class="mdl-grid contorno novo-background"  align="left">

                <h4 style="color: white">Filial 2</h4>

                <div class="mdl-cell mdl-cell--12-col">

                    <table class="semDataTable" width="100%">
			<thead>
				<tr>
					<th></th>
					<th>Pré Pago</th>
					<th>Controle</th>
					<th>Recarga</th>
					<th>HC Oficial</th>
					<th>HC em Campo</th>
					<th>% Campo</th>
					<th>PHC Pré</th>
					<th>PHC Controle</th>
					<th>PHC Recarga</th>
				</tr>
			</thead>
			<tbody>';

				$controle_prepago_count = array();
				$controle_controle_count = array();
				$controle_recarga_count = array();
				$controle_hc_oficial_count = array();
				$controle_hc_campo_count = array();

				foreach ($dados['geral_usuarios_f2'] as $value) {
					echo '<tr>';

					echo '<td style="background-color: #b5c0d4; color: #000; font-weight: 600">'.$value->usuario.'</td>';
					echo '<td>'.$value->prepago.'</td>';
					echo '<td>'.$value->controle.'</td>';
					echo '<td>'.$value->recarga.'</td>';
					echo '<td>'.$value->hc_oficial.'</td>';
					echo '<td>'.$value->hc_campo.'</td>';
					echo '<td>'.round($this->divisao(($value->hc_campo * 100 ),$value->hc_oficial)).'%</td>';

					echo '<td>'.round($this->divisao(($value->prepago * 100),$value->hc_campo)).'%</td>';
					echo '<td>'.round($this->divisao(($value->controle * 100),$value->hc_campo)).'%</td>';
					echo '<td>'.round($this->divisao(($value->recarga * 100),$value->hc_campo)).'%</td>';

					echo '</tr>';

					array_push($controle_prepago_count,$value->prepago);
					array_push($controle_controle_count,$value->controle);
					array_push($controle_recarga_count,$value->recarga);
					array_push($controle_hc_oficial_count,$value->hc_oficial);
					array_push($controle_hc_campo_count,$value->hc_campo);


				} 

				$prepago_tbl1 = array_sum($controle_prepago_count);
				$controle_tbl1 = array_sum($controle_controle_count);
				$recarga_tbl1 = array_sum($controle_recarga_count);
				$hc_oficial_tbl1 = array_sum($controle_hc_oficial_count);
				$hc_campo_tbl1 = array_sum($controle_hc_campo_count);

				echo '<tr><td style="background-color: #000; color: #fff; font-weight: 600">Total Filial:</td>
						<td>'.$prepago_tbl1.'</td>
						<td>'.$controle_tbl1.'</td>
						<td>'.$recarga_tbl1.'</td>
						<td>'.$hc_oficial_tbl1.'</td>
						<td>'.$hc_campo_tbl1.'</td>
						<td>'.round($this->divisao(($hc_campo_tbl1 * 100 ),$hc_oficial_tbl1)).'%</td>
						<td>'.round($this->divisao(($prepago_tbl1 * 100),$hc_campo_tbl1)).'%</td>
						<td>'.round($this->divisao(($controle_tbl1 * 100),$hc_campo_tbl1)).'%</td>
						<td>'.round($this->divisao(($recarga_tbl1 * 100),$hc_campo_tbl1)).'%</td>
				</tr>';


				echo '
				</tbody>
			</table>

			</div>

		</div>';
	}

	public function ajax_f2_2(){

		$dados = $this->model_inputvendas->ajaxF2_2(
			$this->input->post('ano'),
			$this->input->post('mes'),
			$this->input->post('habitado'));

		echo '<div  align="left">

			<table class="semDataTable">
				<thead>
					<tr>
						<th></th>
						<th>Controle Total</th>
						<th>Controle Fácil</th>
						<th>%</th>
						<th>Controle Boleto</th>
						<th>%</th>
					</tr>
				</thead>
				<tbody>';
					
					
					$controle_facil_count = array();
					$controle_giga_count = array();

					foreach ($dados['geral_controle_usuario_f2'] as $value) {
						echo '<tr>';

						echo '<td>'.$value->usuario.'</td>';
						echo '<td>'.$value->controle_total.'</td>';
						echo '<td>'.$value->controle_facil.'</td>';
						echo '<td>'.round(($this->divisao(($value->controle_facil * 100 ),($value->controle_giga+$value->controle_facil)))).' %</td>';
						echo '<td>'.$value->controle_giga.'</td>';
						echo '<td>'.round(($this->divisao(($value->controle_giga * 100 ),($value->controle_giga+$value->controle_facil)))).' %</td>';

						echo '</tr>';

						array_push($controle_facil_count,$value->controle_facil);
						array_push($controle_giga_count,$value->controle_giga);
					} 

					$total_tbl2 = (array_sum($controle_facil_count) + array_sum($controle_giga_count));
					$total_facil_tbl2 = array_sum($controle_facil_count);
					$total_boleto_tbl2 = array_sum($controle_giga_count);

					echo '<tr><td>Total Filial:</td>
					<td>'.$total_tbl2.'</td>
					<td>'.$total_facil_tbl2.'</td>
					<td>'.($this->divisao(($total_facil_tbl2  * 100),$total_tbl2)).' %</td>
					<td>'.$total_boleto_tbl2.'</td>
					<td>'.($this->divisao(($total_boleto_tbl2  * 100),$total_tbl2)).' %</td></tr>';


					
				echo '</tbody>
			</table>

			</div>';	


	}

	public function ajax_f2_3(){

		$dados = $this->model_inputvendas->ajaxF2_3(
			$this->input->post('ano'),
			$this->input->post('mes'),
			$this->input->post('habitado'));


		$this->print_lista('Filial 2 Pré Pago',$dados['usuarios_iv'],$dados['detalhado_usuario_pre_f2'],false);
		$this->print_lista('Filial 2 Controle Total',$dados['usuarios_iv'],$dados['detalhado_usuario_total_f2'],false);
		$this->print_lista('Filial 2 Controle Fácil',$dados['usuarios_iv'],$dados['detalhado_usuario_facil_f2'],false);
		$this->print_lista('Filial 2 Controle Boleto',$dados['usuarios_iv'],$dados['detalhado_usuario_giga_f2'],false);

		$this->print_lista('REPRESENTATIVIDADE % - CONTROLE FÁCIL',$dados['usuarios_iv'],$dados['perc_facil_f2'],true);
		$this->print_lista('REPRESENTATIVIDADE % - CONTROLE BOLETO',$dados['usuarios_iv'],$dados['perc_giga_f2'],true);

		$this->print_lista('Filial 2 Recarga',$dados['usuarios_iv'],$dados['detalhado_usuario_recarga_f2'],false);
		$this->print_lista('Filial 2 Migração',$dados['usuarios_iv'],$dados['detalhado_usuario_migracao_f2'],false);
		$this->print_lista('Filial 2 HC Oficial',$dados['usuarios_iv'],$dados['detalhado_usuario_hc_oficial_f2'],false);
		$this->print_lista('Filial 2 HC Em Campo',$dados['usuarios_iv'],$dados['detalhado_usuario_hc_campo_f2'],false);

		$this->print_lista('REPRESENTATIVIDADE % - HCs EM CAMPO',$dados['usuarios_iv'],$dados['perc_campo_f2'],true);
		$this->print_lista('PHC - PRÉ PAGO',$dados['usuarios_iv'],$dados['perc_pre_f2'],true);
		$this->print_lista('PHC - CONTROLE',$dados['usuarios_iv'],$dados['perc_controle_f2'],true);
		$this->print_lista('PHC - RECARGA',$dados['usuarios_iv'],$dados['perc_recarga_f2'],true);



	}

	function print_lista($titulo = null, $usuarios = null,$lista = null,$perc = null) {

	echo '<div class="mdl-grid contorno novo-background">
		<h4 style="color:#fff;">'.$titulo.'</h4>
		<table class="semDataTable" style="width: 100%">
			<thead>
				<tr>
					<th></th>';

					 for ($i=1; $i < 32; $i++) { 
						echo '<th>'.$i.'</th>';
					} 
					
					echo '<th>TOTAL</th>
				</tr>
			</thead>
			<tbody>';
				
				$array_totalizador = array();
				//Inicia
				for ($i=0; $i < 31; $i++) { 
					$array_totalizador[$i] = 0;
				}


				foreach ($usuarios as $usuario) {
			
					$array = array();

					echo '<tr>';
					echo '<td style="background-color: #b5c0d4; color: #000; font-weight: 600">'.$usuario->usuario.'</td>';

					for ($i=1; $i < 32; $i++) { 
						$valor = 0;

						foreach ($lista as $f1) {
							if ($f1->data == $i && $usuario->id_usuario == $f1->id_usuario) {
								$valor = $f1->valor;
							}
						}

						array_push($array, $valor);
						if ($perc) {
							echo '<td>'.$valor.'%</td>';
						} else {
							echo '<td>'.$valor.'</td>';
						}
						
					}

					//Descobrindo a quantidade de valores acima de 0 estão no array, para poder dividir e extrair uma %
					if ($perc) {

						$count = 0;
						foreach ($array as $value) {
							if ($value > 0) {
								$count += 1;
							}
						}


						echo '<td>'.$this->divisao(array_sum($array),$count).'%</td>';
					} else {
						echo '<td style="background-color: #b5c0d4; color: #000; font-weight: 600">'.array_sum($array).'</td>';
					}
					
					echo '</tr>';

					for ($i=0; $i < 31; $i++) { 
						if ($array[$i] > 0) {
							$array_totalizador[$i] += $array[$i];
						}
						
					}


				}
					//Linhas em branco
					for ($j=0; $j < 3; $j++) { 
						echo '<tr>';
						echo '<td></td>';
						for ($i=0; $i < 31; $i++) { 
							echo '<td></td>';
						}
						echo '<td>0</td>';
						echo '</tr>';
					}

					//Linha com o total
					echo '<tr>';
					echo '<td style="background-color: #000; color: #fff; font-weight: 600">Total Filial</td>';
					for ($i=0; $i < 31; $i++) { 
						if ($perc) {
							echo '<td>'.$array_totalizador[$i].'%</td>';
						} else {
							echo '<td>'.$array_totalizador[$i].'</td>';
						}
					}

					if ($perc) {

						$count = 0;
						foreach ($array_totalizador as $value) {
							if ($value > 0) {
								$count += 1;
							}
						}

						echo '<td style="background-color: #000; color: #fff;">'.$this->divisao(array_sum($array_totalizador),$count).'%</td>';
					} else {
						echo '<td style="background-color: #000; color: #fff;">'.array_sum($array_totalizador).'</td>';
					}
					echo '</tr>';

			echo '</tbody>
			</table>
	</div>';

}


	//Histórico
	public function filtroAjax() {

		$usuario = $this->input->post('usuario');
		$de = $this->input->post('de');
		$ate = $this->input->post('ate');

		$resultado =  $this->model_inputvendas->filtrarPor($usuario,$de,$ate);

		echo '<table class="mdl-data-table mdl-js-data-table mdl-shadow--8dp">
	  <thead>
	    <tr>
	      <th></th>
	      <th>Usuário</th>
	      <th>Filial</th>
	      <th>Data</th>
	      <th>Pré</th>
	      <th>Controle fácil</th>
	      <th>Controle Giga</th>
	      <th>Recarga</th>
	      <th>Banda Pré</th>
	      <th>Migracao</th>
	      <th>Chip Vendedor</th>
	      <th>HC Oficial</th>
	      <th>HC Campo</th>
	      <th>Data Envio</th>
	      <th>Aspectos</th>
	    </tr>
	  </thead>
	  <tbody>';
	    foreach ($resultado as $input_vendas) {
	    	echo '<tr>';
			echo '<td>'.anchor('main/redirecionar/editar-view_editar_inputvendas/'.$input_vendas->id_input_vendas, '<i class="material-icons">mode_edit</i>Editar', array('class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent', 'title' => 'Editar', 'alt' => 'Editar', 'style' => 'margin-top: -7px;','target' => '_blank')).'</td>';
			echo '<td>'.$input_vendas->usuario.'</td>';
			echo '<td>'.$input_vendas->filial.'</td>';
			echo '<td>'.$input_vendas->data_input.'</td>';
			echo '<td>'.$input_vendas->pre.'</td>';
			echo '<td>'.$input_vendas->controle_facil.'</td>';
			echo '<td>'.$input_vendas->controle_giga.'</td>';
			echo '<td>'.$input_vendas->recarga.'</td>';
			echo '<td>'.$input_vendas->banda_pre.'</td>';
			echo '<td>'.$input_vendas->migracao.'</td>';
			echo '<td>'.$input_vendas->chip_vendedor.'</td>';
			echo '<td>'.$input_vendas->hc_oficial.'</td>';
			echo '<td>'.$input_vendas->hc_campo.'</td>';
			echo '<td>'.$input_vendas->data_envio.'</td>';
			echo '<td>'.$input_vendas->aspectos.'</td>';
			echo '</tr>';
		} 
			  echo '</tbody>
			</table>';


	}

	public function excel_export(){

		// Definimos o nome do arquivo que será exportado
		$arquivo = 'input_vendas.xls';
		
		// Criamos uma tabela HTML com o formato da planilha
		$html = "<meta http-equiv=\"content-type\" content=\"application/xhtml+xml; charset=UTF-8\" />
		<table class=\"mdl-data-table mdl-js-data-table mdl-shadow--8dp\">
		  <thead>
		    <tr>
		      <th>ID</th>
		      <th>Usuário</th>
		      <th>Filial</th>
		      <th>Data</th>
		      <th>Pré</th>
		      <th>Controle fácil</th>
		      <th>Controle Giga</th>
		      <th>Recarga</th>
		      <th>Migracao</th>
		      <th>HC Oficial</th>
		      <th>HC Campo</th>
		      <th>Habitado</th>
		      <th>Data Envio</th>
		      <th>Aspectos</th>
		    </tr>
		  </thead>
		  <tbody>";

		  $usuario = $this->input->post('fk_usuario');
		  $de = $this->input->post('de');
		  $ate = $this->input->post('ate');

		  $dados = $this->model_inputvendas->filtrarPor($usuario,$de,$ate);

		    foreach ($dados as $input_vendas) {
		    	$html .= '<tr>';
				$html .= '<td>'.$input_vendas->id_input_vendas.'</td>';
				$html .= '<td>'.$input_vendas->usuario.'</td>';
				$html .= '<td>'.$input_vendas->filial.'</td>';
				$html .= '<td>'.$input_vendas->data_input.'</td>';
				$html .= '<td>'.$input_vendas->pre.'</td>';
				$html .= '<td>'.$input_vendas->controle_facil.'</td>';
				$html .= '<td>'.$input_vendas->controle_giga.'</td>';
				$html .= '<td>'.$input_vendas->recarga.'</td>';
				$html .= '<td>'.$input_vendas->migracao.'</td>';
				$html .= '<td>'.$input_vendas->hc_oficial.'</td>';
				$html .= '<td>'.$input_vendas->hc_campo.'</td>';
				if ($input_vendas->habitado) {
					$html .= '<td>Habitado</td>';
				} else {
					$html .= '<td>Desabitado</td>';
				}
				$html .= '<td>'.$input_vendas->data_envio.'</td>';
				$html .= '<td>'.$input_vendas->aspectos.'</td>';
				$html .= '</tr>';
			} 

		  $html .= "</tbody>
		</table>";

		// Configurações header para forçar o download
		header ("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
		header ("Cache-Control: must-revalidate, post-check=0, pre-check=0"); 
		header ("Content-Transfer-Encoding: binary"); 
		header ("Content-Type: application/vnd.ms-excel"); 
		header ("Expires: 0"); 
		header ("Content-Disposition: attachment; filename=\"{$arquivo}\"");
		header ("Content-Description: PHP Generated Data");

		// Envia o conteúdo do arquivo
		chr(255).chr(254).iconv("UTF-8", "UTF-16LE//IGNORE", $html); 
		echo $html;
		exit;

	}

	public function persistencia() {

		$dados = explode('-', $this->input->post('filial'));

		$filial = $dados[0];
		$usuario_selecionado = $dados[1];
		$fk_usuario = $dados[2];

		//Dados a serem inseridos, os não preenchidos viram NULL
		//$fk_usuario = $this->nulo_se_vazio($this->input->post('fk_usuario'));
		$fk_usuario_criou = $this->nulo_se_vazio($this->input->post('fk_usuario_criou'));
		//$filial = $this->nulo_se_vazio($this->input->post('filial'));
		$data_input = $this->nulo_se_vazio($this->input->post('data_input'));
		$pre = $this->nulo_se_vazio($this->input->post('pre'));
		$controle_facil = $this->nulo_se_vazio($this->input->post('controle_facil'));
		$controle_giga = $this->nulo_se_vazio($this->input->post('controle_giga'));
		$recarga = $this->nulo_se_vazio($this->input->post('recarga'));
		//$banda_pre = $this->nulo_se_vazio($this->input->post('banda_pre'));
		$migracao = $this->nulo_se_vazio($this->input->post('migracao'));
		//$chip_vendedor = $this->nulo_se_vazio($this->input->post('chip_vendedor'));
		$hc_oficial = $this->nulo_se_vazio($this->input->post('hc_oficial'));
		$hc_campo = $this->nulo_se_vazio($this->input->post('hc_campo'));
		$aspectos = $this->nulo_se_vazio($this->input->post('aspectos'));

		$inputVendas = array (

			'fk_usuario' => $fk_usuario,
			'fk_usuario_criou' => $fk_usuario_criou,
			'filial' => $filial,
			'data_input' => $data_input,
			'pre' => $pre,
			'controle_facil' => $controle_facil,
			'controle_giga' => $controle_giga,
			'recarga' => $recarga,
			'migracao' => $migracao,
			'hc_oficial' => $hc_oficial,
			'hc_campo' => $hc_campo,
			'aspectos' => $aspectos,
			'habitado' => true

		);

		//Armazena
		$this->model_inputvendas->insert($inputVendas);

		//Desabitado
		if ($this->input->post('pre_de') != '' ||
			$this->input->post('controle_facil_de') != '' ||
			$this->input->post('controle_giga_de') != '' ||
			$this->input->post('recarga_de') != '' ||
			$this->input->post('migracao_de') != '') {

			$inputVendas = array (

				'fk_usuario' => $fk_usuario,
				'fk_usuario_criou' => $fk_usuario_criou,
				'filial' => $filial,
				'data_input' => $data_input,
				'pre' => $this->input->post('pre_de'),
				'controle_facil' => $this->input->post('controle_facil_de'),
				'controle_giga' => $this->input->post('controle_giga_de'),
				'recarga' => $this->input->post('recarga_de'),
				'migracao' => $this->input->post('migracao_de'),
				'hc_oficial' => 0,
				'hc_campo' => 0,
				'aspectos' => $aspectos,
				'habitado' => false

			);

			//Armazena
			$this->model_inputvendas->insert($inputVendas);


		}


		//Usado para preencher o novo formulário.
		$usuario = $this->nulo_vazio($this->input->post('usuario'));
		//Somente insere sem envio do e-mail.
		if(true) {
			$dados = array('aviso' => "Formulário enviado com sucesso!", 'usuario' => $usuario, 'fk_usuario' => $fk_usuario, 'filial' => $this->model_inputvendas->listar_filiais());

			$email = $this->model_inputvendas->pegar_email($fk_usuario);

			$input = "<!DOCTYPE html>
			<html>
			<head>
				<title>Input de vendas</title>
			</head>
			<body>

			GN, Filial: {$usuario_selecionado} {$filial}<br>
			Enviado por: {$usuario}
			Data: {$data_input}<br>
			<br>

			<strong>Habitado:</strong><br>
			<br>
			Pré Pago: {$pre} <br>
			Controle Fácil: {$controle_facil} <br>
			Controle Boleto: {$controle_giga} <br>
			Recarga: {$recarga} <br>
			Migração: {$migracao} <br>
			<br>

			<strong>Desabitado</strong>
			<br>
			Pré Pago: {$this->input->post('pre_de')} <br>
			Controle Fácil: {$this->input->post('controle_facil_de')} <br>
			Controle Boleto: {$this->input->post('controle_giga_de')} <br>
			Recarga: {$this->input->post('recarga_de')} <br>
			Migração: {$this->input->post('migracao_de')} <br>

			<br>

			HC Oficial {$hc_oficial} <br>
			HC Campo {$hc_campo} <br>
			Aspectos {$aspectos} <br>
			<br>
			<br>
			DATA DO ENVIO: ".date('d/m/Y')." às ".date('H:i:s')."
			</body>
			</html>";

			$this->email->from('clarogn@megamil.net', 'Input de Vendas'); 
			$this->email->to($email); 
			$this->email->subject('Input de vendas'); 
			$this->email->message($input); 

			// Enviar... 
			if ($this->email->send()) { 
				$this->load->view('formulario_iv',$dados);
			} else {
				$this->load->view('formulario_iv',$dados);
			}


			
		} else {
			$dados = array('aviso' => "Formulário enviado com sucesso!", 'usuario' => $usuario, 'fk_usuario' => $fk_usuario, 'filial' => $this->model_inputvendas->listar_filiais());
			$this->load->view('formulario_iv',$dados);
		}



	}

	//(Atualização 02/10/2017)
	//View input editar
	public function input_editar(){

		$id = $this->input->get('id_usuario');
		$data = $this->input->get('data_input');

		$dados = $this->model_inputvendas->editarInput($id,$data);
		
		if (is_null($dados['cabecalho']) || $dados['cabecalho']->id_input_vendas == "") {
			echo "<h1>Nenhum input encontrado</h1>";
		} else {
			$this->load->view('formulario_iv_editar',array('dados' => $dados));
		}

	}

	//Editar Input 
	public function persistencia_editar(){

		$dados = explode('-', $this->input->post('filial'));

		$filial = $dados[0];
		$usuario_selecionado = $dados[1];
		$fk_usuario = $dados[2];

		//Dados a serem inseridos, os não preenchidos viram NULL
		//$fk_usuario = $this->nulo_se_vazio($this->input->post('fk_usuario'));
		$fk_usuario_criou = $this->nulo_se_vazio($this->input->post('fk_usuario_criou'));
		//$filial = $this->nulo_se_vazio($this->input->post('filial'));
		$data_input = $this->nulo_se_vazio($this->input->post('data_input'));
		$pre = $this->nulo_se_vazio($this->input->post('pre'));
		$controle_facil = $this->nulo_se_vazio($this->input->post('controle_facil'));
		$controle_giga = $this->nulo_se_vazio($this->input->post('controle_giga'));
		$recarga = $this->nulo_se_vazio($this->input->post('recarga'));
		//$banda_pre = $this->nulo_se_vazio($this->input->post('banda_pre'));
		$migracao = $this->nulo_se_vazio($this->input->post('migracao'));
		//$chip_vendedor = $this->nulo_se_vazio($this->input->post('chip_vendedor'));
		$hc_oficial = $this->nulo_se_vazio($this->input->post('hc_oficial'));
		$hc_campo = $this->nulo_se_vazio($this->input->post('hc_campo'));
		$aspectos = $this->nulo_se_vazio($this->input->post('aspectos'));

		$inputVendas = array (

			'id_input_vendas' => $this->input->post('id_habitado'),
			'fk_usuario' => $fk_usuario,
			'fk_usuario_criou' => $fk_usuario_criou,
			'filial' => $filial,
			'data_input' => $data_input,
			'pre' => $pre,
			'controle_facil' => $controle_facil,
			'controle_giga' => $controle_giga,
			'recarga' => $recarga,
			'migracao' => $migracao,
			'hc_oficial' => $hc_oficial,
			'hc_campo' => $hc_campo,
			'aspectos' => $aspectos,
			'habitado' => true

		);

		//Armazena
		$this->model_inputvendas->persistenciaEditar($inputVendas);

		//Desabitado
		if ($this->input->post('pre_de') != '' ||
			$this->input->post('controle_facil_de') != '' ||
			$this->input->post('controle_giga_de') != '' ||
			$this->input->post('recarga_de') != '' ||
			$this->input->post('migracao_de') != '') {

			$inputVendas = array (

				'id_input_vendas' => $this->input->post('id_desabitado'),
				'fk_usuario' => $fk_usuario,
				'fk_usuario_criou' => $fk_usuario_criou,
				'filial' => $filial,
				'data_input' => $data_input,
				'pre' => $this->input->post('pre_de'),
				'controle_facil' => $this->input->post('controle_facil_de'),
				'controle_giga' => $this->input->post('controle_giga_de'),
				'recarga' => $this->input->post('recarga_de'),
				'migracao' => $this->input->post('migracao_de'),
				'hc_oficial' => $hc_oficial,
				'hc_campo' => $hc_campo,
				'aspectos' => $aspectos,
				'habitado' => false

			);

			//Armazena
			$this->model_inputvendas->persistenciaEditar($inputVendas);


		}


		//Usado para preencher o novo formulário.
		$usuario = $this->input->post('usuario');
		$data_input_get = $this->input->post('data_input_get');

		$dados_ = $this->model_inputvendas->editarInput($fk_usuario,$data_input_get);

		if (is_null($dados_['cabecalho']) || $dados_['cabecalho']->id_input_vendas == "") {
			echo "<h1>Nenhum input encontrado <br> Usuário: {$fk_usuario} - Data: {$data_input_get}</h1>";
		} else {
			$this->load->view('formulario_iv_editar',array('dados' => $dados_,'aviso' => "Formulário enviado com sucesso!", 'usuario' => $usuario, 'fk_usuario' => $fk_usuario, 'data_input_get' => $this->input->post('data_input_get')));
		}


	}
	//(Atualização 02/10/2017)


	//troca campos não preenchidos por um texto.
	public function nulo_vazio($campo = null) {

		if(is_null($campo)) {
			return '<div style="background-color: #D3D3D3;"> Em Branco </div>';
		} else {
			return $campo;
		}

	}

	//troca campos não preenchidos por NULL.
	public function nulo_se_vazio($campo = null) {

		if(is_null($campo)) {
			return null;
		} else {
			return $campo;
		}

	}

	function divisao($v1,$v2){
		if ($v1 > 0 && $v2 > 0) {
			return round(($v1/$v2));
		} else {
			return 0;
		}
	}

}