<?php defined('BASEPATH') OR exit('No direct script access allowed');

	class controller_relatorio extends CI_Controller {


		public function novo() {

			if($_FILES['imagemdescricao']['name'] != ''){ //Caso seja somente um upload para descrição

				$config['upload_path'] = $_SERVER['DOCUMENT_ROOT'].'/'.base_url().'upload/relatorio';
				$config['allowed_types'] = 'gif|jpg|png';

				//Tira os ascentos e espaços etc..
				$nome = strtolower(strtr(utf8_decode(trim($_FILES['imagemdescricao']['name'])), utf8_decode("áàãâéêíóôõúüñçÁÀÃÂÉÊÍÓÔÕÚÜÑÇ"),"aaaaeeiooouuncAAAAEEIOOOUUNC-"));

				$nome = str_replace(array('.','?',' '), '-', $nome);

				//Quando tirar o ponto da extenção.
				$nome = str_replace('-png', '.png', $nome);
				$nome = str_replace('-gif', '.gif', $nome);
				$nome = str_replace('-jpg', '.jpg', $nome);

				//Gera um nome único para o arquivo
				$arquivo = basename(time().uniqid(md5($nome)).$nome);

				$config['file_name'] = $arquivo;
				
				$this->load->library('upload', $config);
			
				$this->upload->do_upload('imagemdescricao');

				//set_flashdata
				$campos = array (

					'titulo_relatorio' => $this->input->post('titulo_relatorio'),
					'data_limite' => $this->input->post('data_limite'),
					'descricao_relatorio' => $this->input->post('descricao_relatorio').'<img src="'.base_url().'upload/relatorio/'.$arquivo.'">'

				);

				$this->session->set_flashdata($campos);

				redirect('main/redirecionar/cadastro-view_novo_relatorio');

			} else {

				$data = explode('/',$this->input->post('data_limite'));
				$data_limite = $data[2].'-'.$data[1].'-'.$data[0];

				$dados = array (
					//replace pq o tinymce troca o local por ../
					'descricao' => str_replace('../../', base_url(), $this->input->post('descricao_relatorio')),
					'data_limite' => $data_limite,
					'titulo' => $this->input->post('titulo_relatorio')
				);

				$id = $this->model_relatorio->novo_Registro($dados);

				$config['upload_path'] = $_SERVER['DOCUMENT_ROOT'].'/'.base_url().'upload/relatorio';
				$config['allowed_types'] = 'gif|jpg|png';
				$config['file_name'] = 'relatorio'.$id;
				
				$this->load->library('upload', $config);
			
				if (!$this->upload->do_upload('imagem')) {

					$campos = array (

						'titulo_relatorio' => $this->input->post('titulo_relatorio'),
						'data_limite' => $this->input->post('data_limite'),
						'descricao_relatorio' => $this->input->post('descricao_relatorio')

					);

					$this->session->set_flashdata($campos);

					$this->model_relatorio->deletar_Registro($id);

					$this->session->set_flashdata('tipo','erro');
					$this->session->set_flashdata('titulo','Erro no upload.');
					$this->session->set_flashdata('mensagem',$this->upload->display_errors());

					if(is_null($this->input->post('mobile'))){
						redirect('main/redirecionar/cadastro-view_novo_relatorio/');
					} else {
						$array = array ("code" => "0", "message" => "Falha");
						echo json_encode ( $array );  
					}


				} else {

					$arquivo = $this->upload->data();

					$dados = array (

						'url_imagem' => $arquivo['file_name'],
						'id_relatorio' => $id
						
					);

					$this->model_relatorio->atualizar_Registro($dados);

					$this->session->set_flashdata('tipo','sucesso');
					$this->session->set_flashdata('titulo','Sucesso.');
					$this->session->set_flashdata('mensagem',"Upload realizado com sucesso!");

					if(is_null($this->input->post('mobile'))){

						$title = "Novo relatorio";
						$message = $this->input->post('titulo_relatorio'); // Usa o título da publicação como mensagem no PUSH
						$retornar = "cadastro-view_novo_relatorio";

						redirect('controller_notificacoes/notificando?title='.$title.'&message='.$message.'&retornar='.$retornar);

					} else {
						$array = array ("code" => "1", "message" => "Sucesso");
						echo json_encode ( $array );  
					}
					
					
				}

			} //else upload de imagem para descrição


		}

	public function editar() {
		if($_FILES['imagemdescricao']['name'] != ''){ //Caso seja somente um upload para descrição

				$config['upload_path'] = $_SERVER['DOCUMENT_ROOT'].'/'.base_url().'upload/relatorio';
				$config['allowed_types'] = 'gif|jpg|png';

				//Tira os ascentos e espaços etc..
				$nome = strtolower(strtr(utf8_decode(trim($_FILES['imagemdescricao']['name'])), utf8_decode("áàãâéêíóôõúüñçÁÀÃÂÉÊÍÓÔÕÚÜÑÇ"),"aaaaeeiooouuncAAAAEEIOOOUUNC-"));

				$nome = str_replace(array('.','?',' '), '-', $nome);

				//Quando tirar o ponto da extenção.
				$nome = str_replace('-png', '.png', $nome);
				$nome = str_replace('-gif', '.gif', $nome);
				$nome = str_replace('-jpg', '.jpg', $nome);

				//Gera um nome único para o arquivo
				$arquivo = basename(time().uniqid(md5($nome)).$nome);

				$config['file_name'] = $arquivo;
				
				$this->load->library('upload', $config);
			
				$this->upload->do_upload('imagemdescricao');

				$campos = array (

					'titulo_relatorio' => $this->input->post('titulo_relatorio'),
					'data_limite' => $this->input->post('data_limite'),
					'descricao_relatorio' => $this->input->post('descricao_relatorio').'<img src="'.base_url().'upload/relatorio/'.$arquivo.'">'

				);

				$this->session->set_flashdata($campos);

				redirect('main/redirecionar/editar-view_editar_relatorio/'.$this->input->post('id_relatorio'));

			} else {

				$data = explode('/',$this->input->post('data_limite'));
				$data_limite = $data[2].'-'.$data[1].'-'.$data[0];

				if($_FILES['imagem']['name'] == "") {

					$dados = array (
						'id_relatorio' => $this->input->post('id_relatorio'),
						'data_limite' => $data_limite,
						//replace pq o tinymce troca o local por ../, só que 3vezes
						'descricao' => str_replace('../../../', base_url(), $this->input->post('descricao_relatorio')),
						'titulo' => $this->input->post('titulo_relatorio')
					);

				} else {

					$arquivoAtual = "upload/relatorio/".$this->input->post('imagemAtual');
					if (file_exists($arquivoAtual)) {
						unlink($arquivoAtual);
					}

					$config['upload_path'] = $_SERVER['DOCUMENT_ROOT'].'/'.base_url().'upload/relatorio';
					$config['allowed_types'] = 'gif|jpg|png';
					$config['file_name'] = 'relatorio'.$this->input->post('id_relatorio');
					
					$this->load->library('upload', $config);
				
					if (!$this->upload->do_upload('imagem')) {

						$campos = array (

							'titulo_relatorio' => $this->input->post('titulo_relatorio'),
							'data_limite' => $this->input->post('data_limite'),
							'descricao_relatorio' => $this->input->post('descricao_relatorio')

						);

						$this->session->set_flashdata($campos);

						$this->session->set_flashdata('tipo','erro');
						$this->session->set_flashdata('titulo','Erro no upload, imagem anterior deletada.');
						$this->session->set_flashdata('mensagem',$this->upload->display_errors());

						redirect('main/redirecionar/editar-view_editar_relatorio/'.$this->input->post('id_relatorio'));

					} else {

						$arquivo = $this->upload->data();
						$data = explode('/',$this->input->post('data_limite'));
						$data_limite = $data[2].'-'.$data[1].'-'.$data[0];

						$dados = array (
							'id_relatorio' => $this->input->post('id_relatorio'),
							'descricao' => $this->input->post('descricao_relatorio'),
							'titulo' => $this->input->post('titulo_relatorio'),
							'data_limite' => $data_limite,
							'url_imagem' => $arquivo['file_name']
						);

					}

				}

				if($this->model_relatorio->update($dados)) {

					$this->session->set_flashdata('tipo','sucesso');
					$this->session->set_flashdata('titulo','Sucesso.');
					$this->session->set_flashdata('mensagem',"Registro atualizado com sucesso!");

					redirect('main/redirecionar/editar-view_editar_relatorio/'.$this->input->post('id_relatorio'));

				} else {

					$this->session->set_flashdata('tipo','erro');
					$this->session->set_flashdata('titulo','Falha ao atualizar.');
					$this->session->set_flashdata('mensagem',"Erro ao atualizar.");

					redirect('main/redirecionar/editar-view_editar_relatorio/'.$this->input->post('id_relatorio'));

				}

			} //else upload de imagem para descrição


		}


		public function deletar() {

			if(!$this->model_relatorio->del($this->uri->segment(3))) {

				$this->session->set_flashdata('tipo','erro');
				$this->session->set_flashdata('titulo','Erro.');
				$this->session->set_flashdata('mensagem',"Falha ao deletar!");
				redirect('main/redirecionar/listar-view_listar_relatorio/');

			} else {

				$arquivoAtual = "upload/relatorio/".$this->uri->segment(4);
				if (file_exists($arquivoAtual)) {
					unlink($arquivoAtual);
				}

				$this->session->set_flashdata('tipo','sucesso');
				$this->session->set_flashdata('titulo','Sucesso.');
				$this->session->set_flashdata('mensagem',"Registro deletado com sucesso!");
				redirect('main/redirecionar/listar-view_listar_relatorio/');

			}

		}


	}