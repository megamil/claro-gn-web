<?php defined('BASEPATH') OR exit('No direct script access allowed');

class controller_webservice extends CI_Controller {

	public function validar_login() {

		$login = $_GET['login'];
		$senha = $_GET['senha'];

		$dados = $this->model_webservice->login($login,$senha);

		header('Content-Type: application/json; charset=utf-8');
		header("access-control-allow-origin: *");

		if($dados) {

			foreach ($dados as $key => $linha) {

				$linhas = array(

					'ativo' => $linha->ativo,
					'fk_grupo' => $linha->fk_grupo,
					'id_usuario' => $linha->id_usuario,
					'usuario' => $linha->usuario

				);

			}

		} else {

			$linhas = array(

				'ativo' => 0,
				'fk_grupo' => "",
				'id_usuario' => "",
				'usuario' => ""

			);


		}		


		echo json_encode($linhas);

	}

	public function esqueci_Senha() {

		$caracteres = 'a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,1,2,3,4,5,6,7,8,9,0';
		$caractereVetor = explode(',',$caracteres);
		$senha = '';

		while(strlen($senha) < 10) { //Cria uma nova senha com 10 caracteres.
			
			$indice = mt_rand(0, count($caractereVetor));
			$senha .= $caractereVetor[$indice];

		}

		$email = $this->model_webservice->senha_Email(md5($senha),$_GET['login']);

		//Usuário inativo ou desativado
		if($email == ""){

			//echo 'Usuário está desabilitado ou não existe, entre em contato com o administrador!';
			$json = array(
				'resultado' => "Dados incorretos ou perfil inativo"
			);

		} else { //Senha enviada para o E-mail.

			// Detalhes do Email. 
			$this->email->from('clarogn@megamil.net', 'Troca de senha Claro GN'); 
			$this->email->to($email); 
			$this->email->subject('Troca de senha'); 
			$this->email->message('Sua nova senha é: '.$senha); 

			// Enviar... 
			if ($this->email->send()) { 
				//echo 'Nova Senha enviada para o E-mail: "'.$_GET['email'].'"';
				$json = array(
					'resultado' => "Nova Senha enviada para o E-mail!"
				);
			} else {
				//echo $this->email->print_debugger(); 
				$json = array(
					'resultado' => "Erro ao enviar senha."
				);
			}

		}

		echo json_encode($json);


	}

	public function detalhesLista() {

		$categoria = $_GET['categoria'];
		$id = $_GET['id'];

		$dados = $this->model_webservice->detalhesDasListas($id,$categoria);

		echo "<html>
		<meta charset='UTF-8'>
		<meta name=\"viewport\" content=\"width=device-width, user-scalable=0\" />
		<body>
		<div align=\"center\">
			<strong>{$dados->row()->titulo}</strong>
			<br>
			<small>{$dados->row()->data}<small>
		</div>
		<a href=\"".base_url()."upload/{$categoria}/{$dados->row()->url_imagem}\" download><img src=\"".base_url()."upload/{$categoria}/{$dados->row()->url_imagem}\" style=\"width: 100%;\"></a>
		<br>
		<strong>{$dados->row()->descricao}</strong>";



	}

	public function atualizar_dados() {

		$id_usuario = $_GET['id_usuario'];
		$usuario = $_GET['nome'];
		$senha = $_GET['senha'];
		$nova_senha = $_GET['nova_senha'];	

		$json = array(
			'resultado' => $this->model_webservice->atualizar($id_usuario,$usuario,$senha,$nova_senha)
		);

		echo json_encode($json);

	}

	//Usado no inicio do carteira
	public function usuarios() {

		$usuarios = $this->model_webservice->todosUsuarios();
		echo json_encode($usuarios, JSON_UNESCAPED_UNICODE);

	}

	public function porCnpj() {

		$gn = $this->model_webservice->buscarPorCnpj($this->input->get('cnpj'));
		echo json_encode($gn, JSON_UNESCAPED_UNICODE);

	}

	public function porUsuario() {

		$usuarios = $this->model_webservice->buscarPorUsuario($this->input->get('id_usuario'));
		echo json_encode($usuarios, JSON_UNESCAPED_UNICODE);

	}

	public function listarPelaOperadora() {

		$registros = $this->model_webservice->buscarPelaOperadora($this->input->get('operadora'));

		$var = json_encode($registros, JSON_UNESCAPED_UNICODE);

		$tirando_tags = strip_tags($var);
		$tirando_tags = html_entity_decode($tirando_tags);
		$descricao = str_replace(array('\r','\n'), '', $tirando_tags);

		echo $descricao;

	}

	public function listarPelaRede() {

		$rede = $this->input->get('fk_rede');

		if($rede > 5){
			$rede = $rede+1;
		}

		if($rede > 13){
			$rede = $rede+1;
		}

		$registros = $this->model_webservice->buscarPelaRede($rede);

		$var = json_encode($registros, JSON_UNESCAPED_UNICODE);

		$tirando_tags = strip_tags($var);
		$tirando_tags = html_entity_decode($tirando_tags);
		$descricao = str_replace(array('\r','\n'), '', $tirando_tags);

		echo $descricao;

	}

	public function listar() {


		$registros = $this->model_webservice->buscarRegistros($this->input->get('categoria'));

		$var = json_encode($registros, JSON_UNESCAPED_UNICODE);

		$tirando_tags = strip_tags($var);
		$tirando_tags = html_entity_decode($tirando_tags);
		$descricao = str_replace(array('\r','\n'), '', $tirando_tags);

		echo $descricao;

	}

	public function geolocalizacao(){

		$dados = array (

			'raio' => 0.3,
			'lat_pdv' => $this->input->get('lat_pdv'),
			'lng_pdv' => $this->input->get('lng_pdv')

		);

		$registros = $this->model_webservice->geolocalizar($dados);

		echo json_encode($registros, JSON_UNESCAPED_UNICODE);

		

	}

	public function geo() {

		$dados = array (

			'cep' => $this->input->get('cep'),
			'bairro' => $this->input->get('bairro'),
			'cidade' => $this->input->get('cidade')

		);

		$registros = $this->model_webservice->localizar($dados);

		echo json_encode($registros, JSON_UNESCAPED_UNICODE);

	}
	//Adicionar ouu atualizar token
	public function token() {

		$dados = array (

			'token' => $this->input->get('token'),
			'fk_usuario' => $this->input->get('fk_usuario'),
			'aparelho' => $this->input->get('aparelho')

		);

		$resultado = $this->model_webservice->gravar_token($dados);

		$json = array(
			'resultado' => $resultado
		);

		echo json_encode($json, JSON_UNESCAPED_UNICODE);

	}

	//Usado pela carteira no APP
	public function carteiraListaUsuarios() {

		$json =  $this->model_webservice->todosUsuarios();

		echo json_encode($json, JSON_UNESCAPED_UNICODE);

	}

	public function carteiraBuscarPorCnpj() {

		$json = $this->model_webservice->buscarPorCnpj($this->input->get('cnpj'));

		echo json_encode($json, JSON_UNESCAPED_UNICODE);

	}

	public function carteiraBuscarPorFilial() {

		$json = $this->model_webservice->buscarPorFilial($this->input->get('filial'));

		echo json_encode($json, JSON_UNESCAPED_UNICODE);

	}

	public function carteiraBuscarPorUsuario() {

		$json = $this->model_webservice->buscarPorUsuario($this->input->get('id_usuario'));

		echo json_encode($json, JSON_UNESCAPED_UNICODE);

	}

	public function main_function(){

		$q1 = $this->input->get('query');
		$q2 = $this->input->post('query');

		$s2 = $this->input->get('s');
		$s1 = $this->input->post('s');

		if (isset($q1)) {
			$q = $q1;
		} else {
			$q = $q2;
		}

		if (isset($s1)) {
			$s = true;
		} else if (isset($s2)) {
			$s = true;
		} else  {
			$s = false;
		}

		header('Content-Type: application/json; charset=utf-8');
		header("access-control-allow-origin: *");

		echo json_encode($this->model_webservice->mainFunction($q,$s));

	}


	//redireciona o webview
	public function webview_faq(){
		$dados = array ('faq' => $this->model_webservice->faq_ios());
		$this->load->view('webview_faq',$dados);
	}

	//Exibe os detalhes do FAQ
	public function detalhes(){
		echo $this->model_webservice->faq_detalhes($this->uri->segment(3));
	}

	//redireciona o webview
	public function webview_faq_android(){
		$dados = array ('faq' => $this->model_webservice->faq_android());
		$this->load->view('webview_faq_android',$dados);
	}

	public function webview_roteiro(){
		
		$id = $this->input->get('id_usuario');

		$grupo = $this->model_webservice->descobrirGrupo($id);

		if ($grupo != 2) { //Não é um GN
			//Não é GN mas quer criar um roteiro
			$criar_msm_assim = $this->input->get('novo');

			if ($criar_msm_assim != '') {
				$lista = $this->model_webservice->buscar_lista($id);

				if ($lista) {
					$this->load->view('webview_lista_roteiro',array('id' => $id, 'lista' => $lista));
				} else {
					$this->load->view('webview_lista_roteiro',array('id' => $id));
				}
			} else {
				$dados = $this->model_webservice->roteiroAdm($id);
				$dados['id'] = $id;
				$this->load->view('listar/view_roteiro_adm',$dados);
			}


		} else { //GN

			$lista = $this->model_webservice->buscar_lista($id);

			if ($lista) {
				$this->load->view('webview_lista_roteiro',array('id' => $id, 'lista' => $lista));
			} else {
				$this->load->view('webview_lista_roteiro',array('id' => $id));
			}

		}

	}

	public function roteiro_pendentes(){

		$id = $this->input->get('id_usuario');

		$dados = $this->model_webservice->listar_roteiros_where($id);	
		$this->load->view('listar/view_listar_roteirospendentes_adm',$dados);
	}

	public function novo_roteiro(){

		$id = $this->input->get('id_usuario');
		$aviso = $this->input->get('aviso');

		if ($id == '')  {
			$id = 1; //Administrador como padrão
		}

		if (!isset($aviso))  {
			$aviso = '';
		}

		$this->load->view('webview_roteiro',array('id' => $id, 'aviso' => $aviso));

	}

	public function aprovarRoteiro_view(){

		$dados = $this->model_webservice->roteiro_detalhes($this->input->get('id_roteiro'));

		$this->load->view('editar/view_roteiro_aprovar_adm',$dados);

	}

	public function rejeitar(){

		$roteiro = $this->uri->segment(3);

		$this->model_roteiro->aprovar($roteiro,2);

		$dados = $this->model_webservice->roteiro_detalhes($roteiro);

		$this->load->view('editar/view_roteiro_aprovar_adm',$dados);


	}

	public function editar_roteiro(){

		$roteiro = $this->input->post('id_roteiro');

		$data = explode('/',$this->input->post('inicio'));
		$inicio = $data[2].'-'.$data[1].'-'.$data[0];

		$data = explode('/',$this->input->post('fim'));
		$fim = $data[2].'-'.$data[1].'-'.$data[0];

		$dados = array(

			'horas_total' => $this->input->post('horas_total'),
			'km_total' => $this->input->post('km_total'),
			'inicio_roteiro' => $inicio,
			'fim_roteiro' => $fim,
			'status' => 1,
			'obs_roteiro' => $this->input->post('obs')

		);

		$id = $this->model_webservice->editarRoteiro($dados,$roteiro);


		$arrays = array(

			'fk_roteiro' => $id,
			'endereco_google' => $this->input->post('address'),
			'km_google' => $this->input->post('km_google'),
			'tempo_google' => $this->input->post('tempoGoogle'),
			'tempo_adicional' => $this->input->post('tempo')

		);

		$this->model_webservice->addEnderecosRoteiro($arrays);



		$dados = $this->model_webservice->roteiro_detalhes($roteiro);
		$this->load->view('editar/view_roteiro_aprovar_adm',$dados);


	}

	public function gravar_roteiro(){

		$data = explode('/',$this->input->post('inicio'));
		$inicio = $data[2].'-'.$data[1].'-'.$data[0];

		$data = explode('/',$this->input->post('fim'));
		$fim = $data[2].'-'.$data[1].'-'.$data[0];

		$dados = array(

			'fk_usuario' => $this->input->post('id_usuario'),
			'horas_total' => $this->input->post('horas_total'),
			'km_total' => $this->input->post('km_total'),
			'inicio_roteiro' => $inicio,
			'fim_roteiro' => $fim,
			'status' => false,
			'obs_roteiro' => $this->input->post('obs')

		);

		$id = $this->model_webservice->addRoteiro($dados);


		$arrays = array(

			'fk_roteiro' => $id,
			'endereco_google' => $this->input->post('address'),
			'km_google' => $this->input->post('km_google'),
			'tempo_google' => $this->input->post('tempoGoogle'),
			'tempo_adicional' => $this->input->post('tempo')

		);

		$this->model_webservice->addEnderecosRoteiro($arrays);

		redirect('controller_webservice/novo_roteiro?id_usuario='.$this->input->post('id_usuario').'&aviso=Roteiro Adicionado!');

	}
	//Exibe os detalhes dos atendimentos dos GNs para os Gerentes de filiais, atraves do APP (Acompanhamento de GN - Roteiro)
	public function detalhesCheckouts(){

		$dados['id_usuario'] = $this->input->get('id_usuario');//Usuário usando o app
		$dados['fk_usuario'] = $this->input->get('fk_usuario');//usuário a ser consultado
		$dados['nome'] = $this->input->get('nome');//usuário a ser consultado

		$dados['detalhes'] = $this->model_webservice->detalhesCheckout($dados['fk_usuario']);

		$this->load->view('listar/detalhesCheckouts',$dados);

	}

	//Usado pela carteira no APP FIM

	public function badge(){
		$json = $this->model_webservice->view_badge();

		//echo json_encode($json, JSON_UNESCAPED_UNICODE);
		echo json_encode(0);
	}

	public function badge_rede(){

		$json = $this->model_webservice->badgeRede();

		echo json_encode($json, JSON_UNESCAPED_UNICODE);

	}

	//Funções abaixo ainda não estão em funcionamento

	public function notificar_usuarios() {
		$this->load->helper('Notificar');

		$tokens = $this->model_webservice->tokens_usuarios();

		notificando("AIzaSyCEmNvxkH0FQ2DB-lSxgm-SDh-duu_OcaM",$tokens,"Teste WEb service Usuarios");

	}

	public function notificar_master() {
		$this->load->helper('Notificar');

		$tokens = $this->model_webservice->token_master();

		notificando("AIzaSyCEmNvxkH0FQ2DB-lSxgm-SDh-duu_OcaM",$tokens,"Teste WEb service Master");

	}

	public function ajax_pdvs_busca(){

		$nome = $this->input->get('rede');

		$pdvs = $this->model_webservice->ajax_buscar_pdvs($nome);

		echo json_encode($pdvs);

	}

	/*Atualização 02/10/2017*/
	//Usado para trazer as informações de metas / projeções do GN.
	public function hist_input_gn() {

		$id = $this->input->post('id_usuario');

		$hist_input_gn = $this->model_webservice->histInputGn($id);

		$resultado = array (

			'meta_pre' 			=> $hist_input_gn['metas']->meta_pre,
			'realizado_pre' 	=> $hist_input_gn['medias']->pre,
			'projecao_pre' 		=> $this->projecao(
									$hist_input_gn['medias']->pre,
									$hist_input_gn['medias']->inputs),

			'meta_facil' 		=> $hist_input_gn['metas']->meta_controle,
			'realizado_facil' 	=> $hist_input_gn['medias']->controle_facil,
			'projecao_facil' 	=> $this->projecao(
									$hist_input_gn['medias']->controle_facil,
									$hist_input_gn['medias']->inputs),

			'meta_boleto' 		=> $hist_input_gn['metas']->meta_boleto,
			'realizado_boleto' 	=> $hist_input_gn['medias']->controle_giga,
			'projecao_boleto' 	=> $this->projecao(
									$hist_input_gn['medias']->controle_giga,
									$hist_input_gn['medias']->inputs),

			'meta_total'	 	=> $hist_input_gn['metas']->meta_total,
			'realizado_total' 	=> $hist_input_gn['medias']->controle_total,
			'projecao_total' 	=> $this->projecao(
									$hist_input_gn['medias']->controle_total,
									$hist_input_gn['medias']->inputs),

			'meta_recarga' 		=> $hist_input_gn['metas']->meta_recarga,
			'realizado_recarga' => $hist_input_gn['medias']->recarga,
			'projecao_recarga' 	=> $this->projecao(
									$hist_input_gn['medias']->recarga,
									$hist_input_gn['medias']->inputs),

			'meta_migracao' 	=> $hist_input_gn['metas']->meta_migracao,
			'realizado_migracao'=> $hist_input_gn['medias']->migracao,
			'projecao_migracao' => $this->projecao(
									$hist_input_gn['medias']->migracao,
									$hist_input_gn['medias']->inputs),

			'projecao_geral' 	=> round($this->projecao_geral(
									$hist_input_gn['metas'],
									$hist_input_gn['medias'],
									$hist_input_gn['medias']->inputs)),
			'data_atualizacao' 	=> $hist_input_gn['medias']->data_input

		);

		echo json_encode($resultado,JSON_PRETTY_PRINT);

	}

	//Exibe as metas dos usuários para o gerente de filial.
	public function view_metas_app() {

		header('Content-Type: application/json; charset=utf-8');
		header("access-control-allow-origin: *");

		$id = $this->input->post('id_usuario');
		$selecionado = $this->input->post('selecionado');

		if (!isset($id) || !isset($selecionado)) {
			echo json_encode(array('erro'=>"Envie o ID do usuário + a lista que deseja. dados enviados ID={$id},Selecionado='{$selecionado}'"));
			die();
		}

		$view_metas_app = $this->model_webservice->viewMetasApp($id,$selecionado);

		$pre = array();
		$facil = array();
		$boleto = array();
		$total = array();
		$recarga = array();
		$migracao = array();

		$pre_metas = 0;
		$pre_realizado = 0;
		$facil_metas = 0;
		$facil_realizado = 0;
		$boleto_metas = 0;
		$boleto_realizado = 0;
		$total_metas = 0;
		$total_realizado = 0;
		$recarga_metas = 0;
		$recarga_realizado = 0;
		$migracao_metas = 0;
		$migracao_realizado = 0;

		$quantidade_dias = 0; //Se baseando no usuário que fez maior número de inputs encontramos o número de dias para tirar a projeção geral.

		$filial = "";

		foreach ($view_metas_app as $detalhes) {

			if ($detalhes->inputs > $quantidade_dias) {
				$quantidade_dias = $detalhes->inputs;
			}

			$filial = "F".$detalhes->filial;

			//Exibir resultado
			switch ($selecionado) {
				case 'Pré Pago':
					$pre_metas += $this->valida_numero($detalhes->meta_pre);
					$pre_realizado += $this->valida_numero($detalhes->pre);
					$pre["array"][] = array(
						'id_usuario' => $detalhes->id_usuario,
						'nome_gn' => $detalhes->usuario,
						'meta_usuario' => $detalhes->meta_pre,
						'realizado_usuario' => $detalhes->pre,
						'projecao_usuario' => $detalhes->projecao,
						'atingimento_usuario' => $this->atingimento($detalhes->meta_pre,$detalhes->pre)
					);
					break;
				case 'Controle Fácil':
					$facil_metas += $this->valida_numero($detalhes->meta_controle);
					$facil_realizado += $this->valida_numero($detalhes->controle_facil);
					$facil["array"][] = array(
						'id_usuario' => $detalhes->id_usuario,
						'nome_gn' => $detalhes->usuario,
						'meta_usuario' => $detalhes->meta_controle,
						'realizado_usuario' => $detalhes->controle_facil,
						'projecao_usuario' => $detalhes->projecao,
						'atingimento_usuario' => $this->atingimento($detalhes->meta_controle,$detalhes->controle_facil)
					); 
					
					break;
				case 'Controle Boleto':
					$boleto_metas += $this->valida_numero($detalhes->meta_boleto);
					$boleto_realizado += $this->valida_numero($detalhes->controle_giga);
					$boleto["array"][] = array(
						'id_usuario' => $detalhes->id_usuario,
						'nome_gn' => $detalhes->usuario,
						'meta_usuario' => $detalhes->meta_boleto,
						'realizado_usuario' => $detalhes->controle_giga,
						'projecao_usuario' => $detalhes->projecao,
						'atingimento_usuario' => $this->atingimento($detalhes->meta_boleto,$detalhes->controle_giga)
					); 
					
					break;
				case 'Controle Total':
					$total_metas += $this->valida_numero($detalhes->meta_total);
					$total_realizado += $this->valida_numero($detalhes->controle_total);
					$total["array"][] = array(
						'id_usuario' => $detalhes->id_usuario,
						'nome_gn' => $detalhes->usuario,
						'meta_usuario' => $detalhes->meta_total,
						'realizado_usuario' => $detalhes->controle_total,
						'projecao_usuario' => $detalhes->projecao,
						'atingimento_usuario' => $this->atingimento($detalhes->meta_total,$detalhes->controle_total)
					); 
					
					break;
				case 'Recarga':
					$recarga_metas += $this->valida_numero($detalhes->meta_recarga);
					$recarga_realizado += $this->valida_numero($detalhes->recarga);
					$recarga["array"][] = array(
						'id_usuario' => $detalhes->id_usuario,
						'nome_gn' => $detalhes->usuario,
						'meta_usuario' => $detalhes->meta_recarga,
						'realizado_usuario' => $detalhes->recarga,
						'projecao_usuario' => $detalhes->projecao,
						'atingimento_usuario' => $this->atingimento($detalhes->meta_recarga,$detalhes->recarga)
					); 
					
					break;
				case 'Migração':
					$migracao_metas += $this->valida_numero($detalhes->meta_migracao);
					$migracao_realizado += $this->valida_numero($detalhes->migracao);
					$migracao["array"][] = array(
						'id_usuario' => $detalhes->id_usuario,
						'nome_gn' => $detalhes->usuario,
						'meta_usuario' => $detalhes->meta_migracao,
						'realizado_usuario' => $detalhes->migracao,
						'projecao_usuario' => $detalhes->projecao,
						'atingimento_usuario' => $this->atingimento($detalhes->meta_migracao,$detalhes->migracao)
					); 
					
					break;
				
				default:
					echo "Falha switch {$selecionado}";
					break;
			}

		}


		//Exibir resultado
		switch ($selecionado) {
			case 'Pré Pago':
				$pre['projecao_geral'] = $this->projecao_geral_todos($pre_metas,$pre_realizado,$quantidade_dias);
				$pre['filial'] = $filial;
				echo json_encode($pre,JSON_PRETTY_PRINT);
				break;
			case 'Controle Fácil':
				$facil['projecao_geral'] = $this->projecao_geral_todos($facil_metas,$facil_realizado,$quantidade_dias);
				$facil['filial'] = $filial;
				echo json_encode($facil,JSON_PRETTY_PRINT);
				break;
			case 'Controle Boleto':
				$boleto['projecao_geral'] = $this->projecao_geral_todos($boleto_metas,$boleto_realizado,$quantidade_dias);
				$boleto['filial'] = $filial;
				echo json_encode($boleto,JSON_PRETTY_PRINT);
				break;
			case 'Controle Total':
				$total['projecao_geral'] = $this->projecao_geral_todos($total_metas,$total_realizado,$quantidade_dias);
				$total['filial'] = $filial;
				echo json_encode($total,JSON_PRETTY_PRINT);
				break;
			case 'Recarga':
				$recarga['projecao_geral'] = $this->projecao_geral_todos($recarga_metas,$recarga_realizado,$quantidade_dias);
				$recarga['filial'] = $filial;
				echo json_encode($recarga,JSON_PRETTY_PRINT);
				break;
			case 'Migração':
				$migracao['projecao_geral'] = $this->projecao_geral_todos($migracao_metas,$migracao_realizado,$quantidade_dias);
				$migracao['filial'] = $filial;
				echo json_encode($migracao,JSON_PRETTY_PRINT);
				break;
			
			default:
				echo "Falha switch {$selecionado}";
				break;
		}

	}

	public function atingimento($meta,$atingido){

		if($this->valida_numero($meta) > 0 && $this->valida_numero($atingido) > 0) {

			return round((($atingido * 100 ) / $meta));

		} else {
			return 0;
		}

	}

	//Usado no relatório individual, onde todos os campos vem de uma vez.
	public function projecao_geral($meta = null, $realizado = null,$dias = null) {

		$metas = 0;
		$metas += $this->valida_numero($meta->meta_pre);
		$metas += $this->valida_numero($meta->meta_controle);
		$metas += $this->valida_numero($meta->meta_boleto);
		$metas += $this->valida_numero($meta->meta_total);
		$metas += $this->valida_numero($meta->meta_recarga);
		$metas += $this->valida_numero($meta->meta_migracao);

		$realizados = 0;
		$realizados += $this->valida_numero($realizado->pre);
		$realizados += $this->valida_numero($realizado->controle_facil);
		$realizados += $this->valida_numero($realizado->controle_giga);
		$realizados += $this->valida_numero($realizado->controle_total);
		$realizados += $this->valida_numero($realizado->recarga);
		$realizados += $this->valida_numero($realizado->migracao);

		if($this->valida_numero($metas) > 0
			&& $this->valida_numero($realizados) > 0
				&& $this->valida_numero($dias) > 0) {

			$dia = $realizados / $dias; //Média de quanto o GN realiza por dia
			$projecao = round($dia * 25); //Projeção é o valor por dia * 25
			return round(($projecao * 100 ) / $metas);

		} else {

			return 0;

		}

	}

	public function projecao_geral_todos($metas = null, $realizados = null,$dias = null) {

		if($this->valida_numero($metas) > 0
			&& $this->valida_numero($realizados) > 0
				&& $this->valida_numero($dias) > 0) {

			$dia = $realizados / $dias; //Média de quanto o GN realiza por dia
			$projecao = round($dia * 25); //Projeção é o valor por dia * 25
			return round(($projecao * 100 ) / $metas);

		} else {

			return 0;

		}

	}

	public function projecao($realizado = null,$dias = null) {

		if($this->valida_numero($realizado) > 0
				&& $this->valida_numero($dias) > 0) {

			$dia = $realizado / $dias; //Média de quanto o GN realiza por dia
			return round($dia * 25); //Projeção é o valor por dia * 25

		} else {

			return 0;

		}

	}

	public function valida_numero($num = null){

		if(is_null($num) || $num == ""){
			return 0;
		} else {
			return $num;
		}

	}
	/*Atualização 02/10/2017*/

}