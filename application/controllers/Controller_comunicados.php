<?php defined('BASEPATH') OR exit('No direct script access allowed');

	class controller_comunicados extends CI_Controller {


		public function novo() {

			if($_FILES['imagemdescricao']['name'] != ''){ //Caso seja somente um upload para descrição

				$config['upload_path'] = $_SERVER['DOCUMENT_ROOT'].'/'.base_url().'upload/comunicados';
				$config['allowed_types'] = 'gif|jpg|png';

				//Tira os ascentos e espaços etc..
				$nome = strtolower(strtr(utf8_decode(trim($_FILES['imagemdescricao']['name'])), utf8_decode("áàãâéêíóôõúüñçÁÀÃÂÉÊÍÓÔÕÚÜÑÇ"),"aaaaeeiooouuncAAAAEEIOOOUUNC-"));

				$nome = str_replace(array('.','?',' '), '-', $nome);

				//Quando tirar o ponto da extenção.
				$nome = str_replace('-png', '.png', $nome);
				$nome = str_replace('-gif', '.gif', $nome);
				$nome = str_replace('-jpg', '.jpg', $nome);

				//Gera um nome único para o arquivo
				$arquivo = basename(time().uniqid(md5($nome)).$nome);

				$config['file_name'] = $arquivo;
				
				$this->load->library('upload', $config);
			
				$this->upload->do_upload('imagemdescricao');

				//set_flashdata
				$campos = array (

					'titulo_comunicados' => $this->input->post('titulo_comunicados'),
					'data_limite' => $this->input->post('data_limite'),
					'fk_rede' => $this->input->post('fk_rede'),
					'descricao_comunicados' => $this->input->post('descricao_comunicados').'<img src="'.base_url().'upload/comunicados/'.$arquivo.'">'

				);

				$this->session->set_flashdata($campos);

				redirect('main/redirecionar/cadastro-view_novo_comunicados');

			} else {

				$data = explode('/',$this->input->post('data_limite'));
				$data_limite = $data[2].'-'.$data[1].'-'.$data[0];

				$dados = array (
					//replace pq o tinymce troca o local por ../
					'descricao' => str_replace('../../', base_url(), $this->input->post('descricao_comunicados')),
					'data_limite' => $data_limite,
					'fk_rede' => $this->input->post('fk_rede'),
					'titulo' => $this->input->post('titulo_comunicados')
				);

				$id = $this->model_comunicados->novo_Registro($dados);

				$config['upload_path'] = $_SERVER['DOCUMENT_ROOT'].'/'.base_url().'upload/comunicados';
				$config['allowed_types'] = 'gif|jpg|png';
				$config['file_name'] = 'comunicados'.$id;
				
				$this->load->library('upload', $config);
			
				if (!$this->upload->do_upload('imagem')) {

					$campos = array (

						'titulo_comunicados' => $this->input->post('titulo_comunicados'),
						'data_limite' => $this->input->post('data_limite'),
						'fk_rede' => $this->input->post('fk_rede'),
						'descricao_comunicados' => $this->input->post('descricao_comunicados')

					);

					$this->session->set_flashdata($campos);

					$this->model_comunicados->deletar_Registro($id);

					$this->session->set_flashdata('tipo','erro');
					$this->session->set_flashdata('titulo','Erro no upload.');
					$this->session->set_flashdata('mensagem',$this->upload->display_errors());

					if(is_null($this->input->post('mobile'))){
						redirect('main/redirecionar/cadastro-view_novo_comunicados/');
					} else {
						$array = array ("code" => "0", "message" => "Falha");
						echo json_encode ( $array );  
					}


				} else {

					$arquivo = $this->upload->data();

					$dados = array (

						'url_imagem' => $arquivo['file_name'],
						'id_comunicado' => $id
						
					);

					$this->model_comunicados->atualizar_Registro($dados);

					$this->session->set_flashdata('tipo','sucesso');
					$this->session->set_flashdata('titulo','Sucesso.');
					$this->session->set_flashdata('mensagem',"Upload realizado com sucesso!");

					if(is_null($this->input->post('mobile'))){

						$title = "Novo comunicado";
						$message = $this->input->post('titulo_comunicados'); // Usa o título da publicação como mensagem no PUSH
						$retornar = "cadastro-view_novo_comunicados";

						redirect('controller_notificacoes/notificando?title='.$title.'&message='.$message.'&retornar='.$retornar);

					} else {
						$array = array ("code" => "1", "message" => "Sucesso");
						echo json_encode ( $array );  
					}
					
					
				}

			} //else upload de imagem para descrição


		}

	public function editar() {
		if($_FILES['imagemdescricao']['name'] != ''){ //Caso seja somente um upload para descrição

				$config['upload_path'] = $_SERVER['DOCUMENT_ROOT'].'/'.base_url().'upload/comunicados';
				$config['allowed_types'] = 'gif|jpg|png';

				//Tira os ascentos e espaços etc..
				$nome = strtolower(strtr(utf8_decode(trim($_FILES['imagemdescricao']['name'])), utf8_decode("áàãâéêíóôõúüñçÁÀÃÂÉÊÍÓÔÕÚÜÑÇ"),"aaaaeeiooouuncAAAAEEIOOOUUNC-"));

				$nome = str_replace(array('.','?',' '), '-', $nome);

				//Quando tirar o ponto da extenção.
				$nome = str_replace('-png', '.png', $nome);
				$nome = str_replace('-gif', '.gif', $nome);
				$nome = str_replace('-jpg', '.jpg', $nome);

				//Gera um nome único para o arquivo
				$arquivo = basename(time().uniqid(md5($nome)).$nome);

				$config['file_name'] = $arquivo;
				
				$this->load->library('upload', $config);
			
				$this->upload->do_upload('imagemdescricao');

				$campos = array (

					'titulo_comunicados' => $this->input->post('titulo_comunicados'),
					'data_limite' => $this->input->post('data_limite'),
					'fk_rede' => $this->input->post('fk_rede'),
					'descricao_comunicados' => $this->input->post('descricao_comunicados').'<img src="'.base_url().'upload/comunicados/'.$arquivo.'">'

				);

				$this->session->set_flashdata($campos);

				redirect('main/redirecionar/editar-view_editar_comunicados/'.$this->input->post('id_comunicado'));

			} else {

				$data = explode('/',$this->input->post('data_limite'));
				$data_limite = $data[2].'-'.$data[1].'-'.$data[0];

				if($_FILES['imagem']['name'] == "") {

					$dados = array (
						'id_comunicado' => $this->input->post('id_comunicado'),
						'data_limite' => $data_limite,
						'fk_rede' => $this->input->post('fk_rede'),
						//replace pq o tinymce troca o local por ../, só que 3vezes
						'descricao' => str_replace('../../../', base_url(), $this->input->post('descricao_comunicados')),
						'titulo' => $this->input->post('titulo_comunicados')
					);

				} else {

					$arquivoAtual = "upload/comunicados/".$this->input->post('imagemAtual');
					if (file_exists($arquivoAtual)) {
						unlink($arquivoAtual);
					}

					$config['upload_path'] = $_SERVER['DOCUMENT_ROOT'].'/'.base_url().'upload/comunicados';
					$config['allowed_types'] = 'gif|jpg|png';
					$config['file_name'] = 'comunicados'.$this->input->post('id_comunicado');
					
					$this->load->library('upload', $config);
				
					if (!$this->upload->do_upload('imagem')) {

						$campos = array (

							'titulo_comunicados' => $this->input->post('titulo_comunicados'),
							'data_limite' => $this->input->post('data_limite'),
							'descricao_comunicados' => $this->input->post('descricao_comunicados')

						);

						$this->session->set_flashdata($campos);

						$this->session->set_flashdata('tipo','erro');
						$this->session->set_flashdata('titulo','Erro no upload, imagem anterior deletada.');
						$this->session->set_flashdata('mensagem',$this->upload->display_errors());

						redirect('main/redirecionar/editar-view_editar_comunicados/'.$this->input->post('id_comunicado'));

					} else {

						$arquivo = $this->upload->data();
						$data = explode('/',$this->input->post('data_limite'));
						$data_limite = $data[2].'-'.$data[1].'-'.$data[0];

						$dados = array (
							'id_comunicado' => $this->input->post('id_comunicado'),
							'descricao' => $this->input->post('descricao_comunicados'),
							'titulo' => $this->input->post('titulo_comunicados'),
							'fk_rede' => $this->input->post('fk_rede'),
							'data_limite' => $data_limite,
							'url_imagem' => $arquivo['file_name']
						);

					}

				}

				if($this->model_comunicados->update($dados)) {

					$this->session->set_flashdata('tipo','sucesso');
					$this->session->set_flashdata('titulo','Sucesso.');
					$this->session->set_flashdata('mensagem',"Registro atualizado com sucesso!");

					redirect('main/redirecionar/editar-view_editar_comunicados/'.$this->input->post('id_comunicado'));

				} else {

					$this->session->set_flashdata('tipo','erro');
					$this->session->set_flashdata('titulo','Falha ao atualizar.');
					$this->session->set_flashdata('mensagem',"Erro ao atualizar.");

					redirect('main/redirecionar/editar-view_editar_comunicados/'.$this->input->post('id_comunicado'));

				}

			} //else upload de imagem para descrição


		}


		public function deletar() {

			if(!$this->model_comunicados->del($this->uri->segment(3))) {

				$this->session->set_flashdata('tipo','erro');
				$this->session->set_flashdata('titulo','Erro.');
				$this->session->set_flashdata('mensagem',"Falha ao deletar!");
				redirect('main/redirecionar/listar-view_listar_comunicados/');

			} else {

				$arquivoAtual = "upload/comunicados/".$this->uri->segment(4);
				if (file_exists($arquivoAtual)) {
					unlink($arquivoAtual);
				}

				$this->session->set_flashdata('tipo','sucesso');
				$this->session->set_flashdata('titulo','Sucesso.');
				$this->session->set_flashdata('mensagem',"Registro deletado com sucesso!");
				redirect('main/redirecionar/listar-view_listar_comunicados/');

			}

		}


	}