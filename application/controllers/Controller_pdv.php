<?php defined('BASEPATH') OR exit('No direct script access allowed');

	class controller_pdv extends CI_Controller {

		public function criar_pdv() {

			$this->form_validation->set_rules('cod_agente','Cód agente','required');
			$this->form_validation->set_rules('cnpj','CNPJ','required');
			$this->form_validation->set_rules('rede','Rede','required');
			$this->form_validation->set_rules('razao_social','Razão social','required');
			$this->form_validation->set_rules('cep','Cep','required');
			$this->form_validation->set_rules('lougradouro','Lougradouro','required');
			$this->form_validation->set_rules('numero_local','Número','required');
			$this->form_validation->set_rules('cidade','Cidade','required');
			// $this->form_validation->set_rules('complemento','Complemento','required');
			$this->form_validation->set_rules('bairro','Bairro','required');
			$this->form_validation->set_rules('filial','Fílial','required');
			$this->form_validation->set_rules('cod_interno_rede','Código interno','required');
			$this->form_validation->set_rules('varejo','Varejo','required');
			$this->form_validation->set_rules('key_account','Key account','required');


			if($this->form_validation->run()) {

				$campos['cod_agente'] = $this->input->post('cod_agente');
				$campos['cnpj'] = $this->input->post('cnpj');
				$campos['rede'] = $this->input->post('rede');
				$campos['razao_social'] = $this->input->post('razao_social');
				$campos['cep'] = $this->input->post('cep');
				$campos['lougradouro'] = $this->input->post('lougradouro');
				$campos['numero_local'] = $this->input->post('numero_local');
				$campos['cidade'] = $this->input->post('cidade');
				$campos['complemento'] = $this->input->post('complemento');
				$campos['bairro'] = $this->input->post('bairro');
				$campos['curva'] = $this->input->post('curva');
				$campos['filial'] = $this->input->post('filial');
				$campos['cod_interno_rede'] = $this->input->post('cod_interno_rede');
				$campos['varejo'] = $this->input->post('varejo');
				$campos['lat_pdv'] = $this->input->post('lat_pdv');
				$campos['lng_pdv'] = $this->input->post('lng_pdv');
				$campos['key_account'] = $this->input->post('key_account');

				$campos['envio_direto'] = 0; //Fixo, pois somente a sede claro e compart são de envio direto.
				$campos['data_criacao'] = "''";
				$campos['fk_usuario_criou'] = $this->session->userdata('id_usuario');

				$id = $this->model_pdv->add_pdv($campos);

				$this->session->set_flashdata('tipo','sucesso');
				$this->session->set_flashdata('titulo','PDV Criado');
				$this->session->set_flashdata('mensagem','PDV: '.$this->input->post('Rede').' criado com sucesso!');

				redirect('main/redirecionar/editar-view_editar_pdv/'.$id);

			} else {

				$this->session->set_flashdata('tipo','erro');
				$this->session->set_flashdata('titulo','Erro ao criar PDV.');
				$this->session->set_flashdata('mensagem',validation_errors());

				$this->session->set_flashdata($campos);

				redirect('main/redirecionar/cadastro-view_novo_pdv/');

			}


		}

		public function filtroAjax(){
			echo '<table class="mdl-data-table mdl-js-data-table mdl-shadow--8dp" style="width: 100%;">
				  <thead>
				    <tr>
				      <th>Editar</th>
				      <th>Rede</th>
				      <th>CNPJ</th>
				      <th>Lougradouro</th>
				      <th>Filial</th>
				      <th>GN</th>
				    </tr>
				  </thead>
				  <tbody>';

				  $dados['usuario'] = $this->input->post('usuario');
				  $dados['rede'] = $this->input->post('rede');
				  $dados['filial'] = $this->input->post('filial');
				  $dados['cnpj'] = str_replace(array('.','/','-'),'', $this->input->post('cnpj'));

				    foreach ($this->model_pdv->filtro($dados) as $pdv) {
						    	echo '<tr>';
						    	echo '<td>'.anchor('main/redirecionar/editar-view_editar_pdv/'.$pdv->id_pdv, '<i class="material-icons">mode_edit</i>Editar', array('class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent', 'title' => 'Editar PDV.', 'alt' => 'Editar PDV.', 'style' => 'margin-top: -7px;')).'</td>';
								echo '<td>'.$pdv->rede.'</td>';
								echo '<td>'.$pdv->cnpj.'</td>';
								echo '<td>'.$pdv->lougradouro.'</td>';

								if ($pdv->filial == 1) {
									echo '<td>Filial 1</td>';
								} else {
									echo '<td>Filial 2</td>';
								}

								echo '<td>'.$pdv->nome.'</td>';

								echo '</tr>';
							}
					  echo '</tbody>
					</table>';
		}

		public function excel_export(){

			// Definimos o nome do arquivo que será exportado
			$arquivo = 'lista_pdvs.xls';

			$html = "";

			$html .= '<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" />
			<table class="mdl-data-table mdl-js-data-table mdl-shadow--8dp" style="width: 100%;">
				  <thead>
				    <tr>
				      <th>ID</th>
				      <th>Cod Agente</th>
				      <th>CNPJ</th>
				      <th>Rede</th>
				      <th>Razão Social</th>
				      <th>Lougradouro</th>
				      <th>Número</th>
				      <th>Complemento</th>
				      <th>Bairro</th>
				      <th>Cep</th>
				      <th>Cidade</th>
				      <th>Filial</th>
				      <th>Cod interno</th>
				      <th>Varejo</th>
				      <th>Curva</th>
				      <th>key account</th>
				      <th>Data criação</th>
				      <th>Data edicao</th>
				      <th>Usuario que criou</th>
				    </tr>
				  </thead>
				  <tbody>';

				  $dados['usuario'] = $this->input->post('usuario');
				  $dados['rede'] = $this->input->post('rede');
				  $dados['filial'] = $this->input->post('filial');
				  $dados['cnpj'] = $this->input->post('cnpj');

				    foreach ($this->model_pdv->filtroExcel($dados) as $pdv) {
						    	$html .= '<tr>';

									$html .= '<td>'.$pdv->id_pdv.'</td>';
									$html .= '<td>'.$pdv->cod_agente.'</td>';
									$html .= '<td>'.$pdv->cnpj.'</td>';
									$html .= '<td>'.$pdv->rede.'</td>';
									$html .= '<td>'.$pdv->razao_social.'</td>';
									$html .= '<td>'.$pdv->lougradouro.'</td>';
									$html .= '<td>'.$pdv->numero_local.'</td>';
									$html .= '<td>'.$pdv->complemento.'</td>';
									$html .= '<td>'.$pdv->bairro.'</td>';
									$html .= '<td>'.$pdv->cep.'</td>';
									$html .= '<td>'.$pdv->cidade.'</td>';

									if ($pdv->filial == 1) {
										$html .= '<td>Filial 1</td>';
									} else {
										$html .= '<td>Filial 2</td>';
									}

									$html .= '<td>'.$pdv->cod_interno_rede.'</td>';
									$html .= '<td>'.$pdv->varejo.'</td>';
									$html .= '<td>'.$pdv->curva.'</td>';
									$html .= '<td>'.$pdv->key_account.'</td>';
									$html .= '<td>'.$pdv->data_criacao.'</td>';
									$html .= '<td>'.$pdv->data_edicao.'</td>';
									$html .= '<td>'.$pdv->fk_usuario_criou.'</td>';

								$html .= '</tr>';
							}
					 $html .= '</tbody>
					 </table>';

				// Configurações header para forçar o download
				header ("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
				header ("Cache-Control: must-revalidate, post-check=0, pre-check=0"); 
				header ("Content-Transfer-Encoding: binary"); 
				header ("Content-Type: application/vnd.ms-excel"); 
				header ("Expires: 0"); 
				header ("Content-Disposition: attachment; filename=\"{$arquivo}\"");
				header ("Content-Description: PHP Generated Data");

				// Envia o conteúdo do arquivo
				chr(255).chr(254).iconv("UTF-8", "UTF-16LE//IGNORE", $html); 
				echo $html;
				exit;
		}

		public function editar_pdv() {

			$this->form_validation->set_rules('cod_agente','Cód agente','required');
			$this->form_validation->set_rules('cnpj','CNPJ','required');
			$this->form_validation->set_rules('rede','Rede','required');
			$this->form_validation->set_rules('razao_social','Razão social','required');
			$this->form_validation->set_rules('cep','Cep','required');
			$this->form_validation->set_rules('lougradouro','Lougradouro','required');
			$this->form_validation->set_rules('numero_local','Número','required');
			$this->form_validation->set_rules('cidade','Cidade','required');
			// $this->form_validation->set_rules('complemento','Complemento','required');
			$this->form_validation->set_rules('bairro','Bairro','required');
			$this->form_validation->set_rules('filial','Fílial','required');
			$this->form_validation->set_rules('cod_interno_rede','Código interno','required');
			$this->form_validation->set_rules('varejo','Varejo','required');
			$this->form_validation->set_rules('key_account','Key account','required');


			if($this->form_validation->run()) {

				$campos['cod_agente'] = $this->input->post('cod_agente');
				$campos['cnpj'] = $this->input->post('cnpj');
				$campos['rede'] = $this->input->post('rede');
				$campos['razao_social'] = $this->input->post('razao_social');
				$campos['cep'] = $this->input->post('cep');
				$campos['lougradouro'] = $this->input->post('lougradouro');
				$campos['numero_local'] = $this->input->post('numero_local');
				$campos['cidade'] = $this->input->post('cidade');
				$campos['complemento'] = $this->input->post('complemento');
				$campos['bairro'] = $this->input->post('bairro');
				$campos['curva'] = $this->input->post('curva');
				$campos['filial'] = $this->input->post('filial');
				$campos['cod_interno_rede'] = $this->input->post('cod_interno_rede');
				$campos['varejo'] = $this->input->post('varejo');
				$campos['lat_pdv'] = $this->input->post('lat_pdv');
				$campos['lng_pdv'] = $this->input->post('lng_pdv');
				$campos['key_account'] = $this->input->post('key_account');

				$this->model_pdv->update_pdv($campos,$this->input->post('id_pdv'));

				$this->session->set_flashdata('tipo','sucesso');
				$this->session->set_flashdata('titulo','PDV Editado');
				$this->session->set_flashdata('mensagem','PDV: '.$this->input->post('Rede').' editado com sucesso!');

				redirect('main/redirecionar/editar-view_editar_pdv/'.$this->input->post('id_pdv'));

			} else {

				$this->session->set_flashdata('tipo','erro');
				$this->session->set_flashdata('titulo','Erro ao editar PDV.');
				$this->session->set_flashdata('mensagem',validation_errors());

				$this->session->set_flashdata($campos);

				redirect('main/redirecionar/editar-view_editar_pdv/'.$this->input->post('id_pdv'));

			}


		}



	}