<?php defined('BASEPATH') OR exit('No direct script access allowed');

	class controller_excel_input extends CI_Controller {


	public function formulario () {

		//lógica para listar campos já existentes.

		$filial = $this->model_inputvendas->listar_filiais();

		$this->load->view('formulario_iv',array('filial' => $filial));

	}

	public function ajax_geral(){

		$dados = $this->model_inputvendas->ajax_geral_inputvendas(
			$this->input->get('ano'),
			$this->input->get('mes'),
			$this->input->get('habitado'));

		$excel = '<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" />';


		$excel .= '<div class="mdl-grid contorno" style="background-color: white;" align="left">

			<div class="mdl-cell mdl-cell--7-col">

			<table>
				<thead>
					<tr>
						<th></th>
						<th>Pré Pago</th>
						<th>Controle</th>
						<th>Recarga</th>
						<th>HC Oficial</th>
						<th>HC em Campo</th>
						<th>% Campo</th>
						<th>PHC Pré</th>
						<th>PHC Controle</th>
						<th>PHC Recarga</th>
					</tr>
				</thead>
				<tbody>';

					foreach ($dados['geral'] as $value) {
						$excel .=  '<tr>';

						$excel .=  '<td align="left">'.$value->filial.'</td>';
						$excel .=  '<td align="left">'.$value->pre.'</td>';
						$excel .=  '<td align="left">'.$value->controle.'</td>';
						$excel .=  '<td align="left">'.$value->recarga.'</td>';
						$excel .=  '<td align="left">'.$value->hc_oficial.'</td>';
						$excel .=  '<td align="left">'.$value->hc_campo.'</td>';
						$excel .=  '<td align="left">'.round(($this->divisao(($value->hc_campo * 100 ),$value->hc_oficial)),2).' %</td>';
						$excel .=  '<td align="left">'.round(($this->divisao($value->pre,$value->hc_campo)),2).'</td>';
						$excel .=  '<td align="left">'.round(($this->divisao($value->controle,$value->hc_campo)),2).'</td>';
						$excel .=  '<td align="left">'.round(($this->divisao($value->recarga,$value->hc_campo)),2).'</td>';

						$excel .=  '</tr>';
					} 

				$excel .= '</tbody>
			</table>

			</div>

			<div class="mdl-cell mdl-cell--5-col" align="left">

			<table align="left">
				<thead>
					<tr>
						<th></th>
						<th>Controle Total</th>
						<th>Controle Fácil</th>
						<th>%</th>
						<th>Controle Boleto</th>
						<th>%</th>
					</tr>
				</thead>
				<tbody>';
					foreach ($dados['geral_controle'] as $value) {
						$excel .=  '<tr>';

						$excel .=  '<td align="left">'.$value->filial.'</td>';
						$excel .=  '<td align="left">'.$value->controle_total.'</td>';
						$excel .=  '<td align="left">'.$value->controle_facil.'</td>';
						$excel .=  '<td align="left">'.round(($this->divisao(($value->controle_facil * 100 ),($value->controle_giga+$value->controle_facil))),2).' %</td>';
						$excel .=  '<td align="left">'.$value->controle_giga.'</td>';
						$excel .=  '<td align="left">'.round(($this->divisao(($value->controle_giga * 100 ),($value->controle_giga+$value->controle_facil))),2).' %</td>';

						$excel .=  '</tr>';

					}
				
				$excel .= '</tbody>
			</table>

			</div>	
		</div>

		<div class="mdl-grid contorno" style="background-color: white;" align="left">
			<div class="mdl-cell mdl-cell--12-col" align="left">

			<h4>PRÉ PAGO</h4>

				<table style="width: 100%">
				<thead>
					<tr>
						<th></th>';
						for ($i=1; $i < 32; $i++) { 
							$excel .=  '<th>'.$i.'</th>';
						}
					
						$excel .=  '<th>TOTAL</th>
					</tr>
				</thead>
				<tbody>';
					
					

						$array_f1 = array();
						$array_f2 = array();

						$excel .=  '<tr>';
						$excel .=  '<td align="left">F1</td>';

						for ($i=1; $i < 32; $i++) { 
							$valor = 0;

							foreach ($dados['pre_f1'] as $f1) {
								if ($f1->data == $i) {
									$valor = $f1->pre;
								}
							}

							array_push($array_f1, $valor);
							$excel .=  '<td align="left">'.$valor.'</td>';
						}

						$excel .=  '<td align="left">'.array_sum($array_f1).'</td>';
						$excel .=  '</tr>';

						//F2
						$excel .=  '<tr>';
						$excel .=  '<td align="left">F2</td>';

						for ($i=1; $i < 32; $i++) { 
							$valor = 0;

							foreach ($dados['pre_f2'] as $f2) {
								if ($f2->data == $i) {
									$valor = $f2->pre;
								}
							}

							array_push($array_f2, $valor);
							$excel .=  '<td align="left">'.$valor.'</td>';
						}

						$excel .=  '<td align="left">'.array_sum($array_f2).'</td>';
						$excel .=  '</tr>';

						//CANAL
						$excel .=  '<tr>';
						$excel .=  '<td align="left">CANAL</td>';

						for ($i=0; $i < 31; $i++) { 
							
							$excel .=  '<td align="left">'.($array_f1[$i] + $array_f2[$i]).'</td>';

						}

						$excel .=  '<td align="left">'.(array_sum($array_f1) + array_sum($array_f2)).'</td>';
						$excel .=  '</tr>';

				$excel .=  '</tbody>
				</table>

			</div>
		</div>

		<div class="mdl-grid contorno" style="background-color: white;" align="left">
			<div class="mdl-cell mdl-cell--12-col" align="left">
				<h4>CONTROLE TOTAL</h4>

				<table style="width: 100%">
				<thead>
					<tr>
						<th></th>';

						for ($i=1; $i < 32; $i++) { 
							$excel .=  '<th>'.$i.'</th>';
						}
					
						$excel .=  '<th>TOTAL</th>
					</tr>
				</thead>
				<tbody>';
					
					

						$array_f1 = array();
						$array_f2 = array();

						$excel .=  '<tr>';
						$excel .=  '<td align="left">F1</td>';

						for ($i=1; $i < 32; $i++) { 
							$valor = 0;

							foreach ($dados['controle_f1'] as $f1) {
								if ($f1->data == $i) {
									$valor = $f1->controle;
								}
							}

							array_push($array_f1, $valor);
							$excel .=  '<td align="left">'.$valor.'</td>';
						}

						$excel .=  '<td align="left">'.array_sum($array_f1).'</td>';
						$excel .=  '</tr>';

						//F2
						$excel .=  '<tr>';
						$excel .=  '<td align="left">F2</td>';

						for ($i=1; $i < 32; $i++) { 
							$valor = 0;

							foreach ($dados['controle_f2'] as $f2) {
								if ($f2->data == $i) {
									$valor = $f2->controle;
								}
							}

							array_push($array_f2, $valor);
							$excel .=  '<td align="left">'.$valor.'</td>';
						}

						$excel .=  '<td align="left">'.array_sum($array_f2).'</td>';
						$excel .=  '</tr>';

						//CANAL
						$excel .=  '<tr>';
						$excel .=  '<td align="left">CANAL</td>';

						for ($i=0; $i < 31; $i++) { 
							
							$excel .=  '<td align="left">'.($array_f1[$i] + $array_f2[$i]).'</td>';

						}

						$excel .=  '<td align="left">'.(array_sum($array_f1) + array_sum($array_f2)).'</td>';
						$excel .=  '</tr>';




						$excel .= '</tbody>
						</table>

					</div>
				</div>';

		$arquivo = 'input_geral.xls';

		// Configurações header para forçar o download
		header ("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
		header ("Cache-Control: must-revalidate, post-check=0, pre-check=0"); 
		header ("Content-Transfer-Encoding: binary"); 
		header ("Content-Type: application/vnd.ms-excel"); 
		header ("Expires: 0"); 
		header ("Content-Disposition: attachment; filename=\"{$arquivo}\"");
		header ("Content-Description: PHP Generated Data");

		// Envia o conteúdo do arquivo
		chr(255).chr(254).iconv("UTF-8", "UTF-16LE//IGNORE", $excel); 
		echo $excel;
		exit;


	}

	public function ajax_f1(){

		$dados = $this->model_inputvendas->ajaxF1_1(
			$this->input->get('ano'),
			$this->input->get('mes'),
			$this->input->get('habitado'));

		$excel = '<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" />';


		$excel .= '<div class="mdl-grid contorno" style="background-color: white;" align="left">

	<h4>Filial 1</h4>

		<div class="mdl-cell mdl-cell--12-col">

		<table width="100%">
			<thead>
				<tr>
					<th></th>
					<th>Pré Pago</th>
					<th>Controle</th>
					<th>Recarga</th>
					<th>HC Oficial</th>
					<th>HC em Campo</th>
					<th>% Campo</th>
					<th>PHC Pré</th>
					<th>PHC Controle</th>
					<th>PHC Recarga</th>
				</tr>
			</thead>
			<tbody>';

				$controle_prepago_count = array();
				$controle_controle_count = array();
				$controle_recarga_count = array();
				$controle_hc_oficial_count = array();
				$controle_hc_campo_count = array();

				foreach ($dados['geral_usuarios_f1'] as $value) {
					$excel .= '<tr>';

					$excel .= '<td align="left">'.$value->usuario.'</td>';
					$excel .= '<td align="left">'.$value->prepago.'</td>';
					$excel .= '<td align="left">'.$value->controle.'</td>';
					$excel .= '<td align="left">'.$value->recarga.'</td>';
					$excel .= '<td align="left">'.$value->hc_oficial.'</td>';
					$excel .= '<td align="left">'.$value->hc_campo.'</td>';
					$excel .= '<td align="left">'.round($this->divisao(($value->hc_campo * 100 ),$value->hc_oficial),2).'%</td>';

					$excel .= '<td align="left">'.round($this->divisao(($value->prepago * 100),$value->hc_campo),2).'%</td>';
					$excel .= '<td align="left">'.round($this->divisao(($value->controle * 100),$value->hc_campo),2).'%</td>';
					$excel .= '<td align="left">'.round($this->divisao(($value->recarga * 100),$value->hc_campo),2).'%</td>';

					$excel .= '</tr>';

					array_push($controle_prepago_count,$value->prepago);
					array_push($controle_controle_count,$value->controle);
					array_push($controle_recarga_count,$value->recarga);
					array_push($controle_hc_oficial_count,$value->hc_oficial);
					array_push($controle_hc_campo_count,$value->hc_campo);


				} 

				$prepago_tbl1 = array_sum($controle_prepago_count);
				$controle_tbl1 = array_sum($controle_controle_count);
				$recarga_tbl1 = array_sum($controle_recarga_count);
				$hc_oficial_tbl1 = array_sum($controle_hc_oficial_count);
				$hc_campo_tbl1 = array_sum($controle_hc_campo_count);

				$excel .= '<tr><td align="left">Total Filial:</td>
						<td align="left">'.$prepago_tbl1.'</td>
						<td align="left">'.$controle_tbl1.'</td>
						<td align="left">'.$recarga_tbl1.'</td>
						<td align="left">'.$hc_oficial_tbl1.'</td>
						<td align="left">'.$hc_campo_tbl1.'</td>
						<td align="left">'.round($this->divisao(($hc_campo_tbl1 * 100 ),$hc_oficial_tbl1),2).'%</td>
						<td align="left">'.round($this->divisao(($prepago_tbl1 * 100),$hc_campo_tbl1),2).'%</td>
						<td align="left">'.round($this->divisao(($controle_tbl1 * 100),$hc_campo_tbl1),2).'%</td>
						<td align="left">'.round($this->divisao(($recarga_tbl1 * 100),$hc_campo_tbl1),2).'%</td>
				</tr>';


				$excel .= '
				</tbody>
			</table>

			</div>

		</div>';

		//2

		$dados = $this->model_inputvendas->ajaxF1_2(
			$this->input->get('ano'),
			$this->input->get('mes'),
			$this->input->get('habitado'));

		$excel .= '<div  align="left">

			<table>
				<thead>
					<tr>
						<th></th>
						<th>Controle Total</th>
						<th>Controle Fácil</th>
						<th>%</th>
						<th>Controle Boleto</th>
						<th>%</th>
					</tr>
				</thead>
				<tbody>';
					
					
					$controle_facil_count = array();
					$controle_giga_count = array();

					foreach ($dados['geral_controle_usuario_f1'] as $value) {
						$excel .= '<tr>';

						$excel .= '<td align="left">'.$value->usuario.'</td>';
						$excel .= '<td align="left">'.$value->controle_total.'</td>';
						$excel .= '<td align="left">'.$value->controle_facil.'</td>';
						$excel .= '<td align="left">'.round(($this->divisao(($value->controle_facil * 100 ),($value->controle_giga+$value->controle_facil))),2).' %</td>';
						$excel .= '<td align="left">'.$value->controle_giga.'</td>';
						$excel .= '<td align="left">'.round(($this->divisao(($value->controle_giga * 100 ),($value->controle_giga+$value->controle_facil))),2).' %</td>';

						$excel .= '</tr>';

						array_push($controle_facil_count,$value->controle_facil);
						array_push($controle_giga_count,$value->controle_giga);
					} 

					$total_tbl2 = (array_sum($controle_facil_count) + array_sum($controle_giga_count));
					$total_facil_tbl2 = array_sum($controle_facil_count);
					$total_boleto_tbl2 = array_sum($controle_giga_count);

					$excel .= '<tr><td align="left">Total Filial:</td>
					<td align="left">'.$total_tbl2.'</td>
					<td align="left">'.$total_facil_tbl2.'</td>
					<td align="left">'.($this->divisao(($total_facil_tbl2  * 100),$total_tbl2)).' %</td>
					<td align="left">'.$total_boleto_tbl2.'</td>
					<td align="left">'.($this->divisao(($total_boleto_tbl2  * 100),$total_tbl2)).' %</td></tr>';


					
				$excel .= '</tbody>
			</table>

			</div>';	

		//3

		$dados = $this->model_inputvendas->ajaxF1_3(
			$this->input->get('ano'),
			$this->input->get('mes'),
			$this->input->get('habitado'));


		$excel .=  $this->print_lista('Filial 1 Pré Pago',$dados['usuarios_iv'],$dados['detalhado_usuario_pre_f1'],false);
		$excel .=  $this->print_lista('Filial 1 Controle Total',$dados['usuarios_iv'],$dados['detalhado_usuario_total_f1'],false);
		$excel .=  $this->print_lista('Filial 1 Controle Fácil',$dados['usuarios_iv'],$dados['detalhado_usuario_facil_f1'],false);
		$excel .=  $this->print_lista('Filial 1 Controle Boleto',$dados['usuarios_iv'],$dados['detalhado_usuario_giga_f1'],false);

		$excel .=  $this->print_lista('REPRESENTATIVIDADE % - CONTROLE FÁCIL',$dados['usuarios_iv'],$dados['perc_facil_f1'],true);
		$excel .=  $this->print_lista('REPRESENTATIVIDADE % - CONTROLE BOLETO',$dados['usuarios_iv'],$dados['perc_giga_f1'],true);

		$excel .=  $this->print_lista('Filial 1 Recarga',$dados['usuarios_iv'],$dados['detalhado_usuario_recarga_f1'],false);
		$excel .=  $this->print_lista('Filial 1 Migração',$dados['usuarios_iv'],$dados['detalhado_usuario_migracao_f1'],false);
		$excel .=  $this->print_lista('Filial 1 HC Oficial',$dados['usuarios_iv'],$dados['detalhado_usuario_hc_oficial_f1'],false);
		$excel .=  $this->print_lista('Filial 1 HC Em Campo',$dados['usuarios_iv'],$dados['detalhado_usuario_hc_campo_f1'],false);

		$excel .=  $this->print_lista('REPRESENTATIVIDADE % - HCs EM CAMPO',$dados['usuarios_iv'],$dados['perc_campo_f1'],true);
		$excel .=  $this->print_lista('PHC - PRÉ PAGO',$dados['usuarios_iv'],$dados['perc_pre_f1'],true);
		$excel .=  $this->print_lista('PHC - CONTROLE',$dados['usuarios_iv'],$dados['perc_controle_f1'],true);
		$excel .=  $this->print_lista('PHC - RECARGA',$dados['usuarios_iv'],$dados['perc_recarga_f1'],true);

		$arquivo = 'input_filiais_1.xls';

		// Configurações header para forçar o download
		header ("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
		header ("Cache-Control: must-revalidate, post-check=0, pre-check=0"); 
		header ("Content-Transfer-Encoding: binary"); 
		header ("Content-Type: application/vnd.ms-excel"); 
		header ("Expires: 0"); 
		header ("Content-Disposition: attachment; filename=\"{$arquivo}\"");
		header ("Content-Description: PHP Generated Data");

		// Envia o conteúdo do arquivo
		chr(255).chr(254).iconv("UTF-8", "UTF-16LE//IGNORE", $excel); 
		echo $excel;
		exit;



	}

	public function ajax_f2(){

		$dados = $this->model_inputvendas->ajaxF2_1(
			$this->input->get('ano'),
			$this->input->get('mes'),
			$this->input->get('habitado'));

		$excel = '<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" />';


		$excel .= '<div class="mdl-grid contorno" style="background-color: white;" align="left">

	<h4>Filial 2</h4>

		<div class="mdl-cell mdl-cell--12-col">

		<table width="100%">
			<thead>
				<tr>
					<th></th>
					<th>Pré Pago</th>
					<th>Controle</th>
					<th>Recarga</th>
					<th>HC Oficial</th>
					<th>HC em Campo</th>
					<th>% Campo</th>
					<th>PHC Pré</th>
					<th>PHC Controle</th>
					<th>PHC Recarga</th>
				</tr>
			</thead>
			<tbody>';

				$controle_prepago_count = array();
				$controle_controle_count = array();
				$controle_recarga_count = array();
				$controle_hc_oficial_count = array();
				$controle_hc_campo_count = array();

				foreach ($dados['geral_usuarios_f2'] as $value) {
					$excel .= '<tr>';

					$excel .= '<td align="left">'.$value->usuario.'</td>';
					$excel .= '<td align="left">'.$value->prepago.'</td>';
					$excel .= '<td align="left">'.$value->controle.'</td>';
					$excel .= '<td align="left">'.$value->recarga.'</td>';
					$excel .= '<td align="left">'.$value->hc_oficial.'</td>';
					$excel .= '<td align="left">'.$value->hc_campo.'</td>';
					$excel .= '<td align="left">'.round($this->divisao(($value->hc_campo * 100 ),$value->hc_oficial),2).'%</td>';

					$excel .= '<td align="left">'.round($this->divisao(($value->prepago * 100),$value->hc_campo),2).'%</td>';
					$excel .= '<td align="left">'.round($this->divisao(($value->controle * 100),$value->hc_campo),2).'%</td>';
					$excel .= '<td align="left">'.round($this->divisao(($value->recarga * 100),$value->hc_campo),2).'%</td>';

					$excel .= '</tr>';

					array_push($controle_prepago_count,$value->prepago);
					array_push($controle_controle_count,$value->controle);
					array_push($controle_recarga_count,$value->recarga);
					array_push($controle_hc_oficial_count,$value->hc_oficial);
					array_push($controle_hc_campo_count,$value->hc_campo);


				} 

				$prepago_tbl1 = array_sum($controle_prepago_count);
				$controle_tbl1 = array_sum($controle_controle_count);
				$recarga_tbl1 = array_sum($controle_recarga_count);
				$hc_oficial_tbl1 = array_sum($controle_hc_oficial_count);
				$hc_campo_tbl1 = array_sum($controle_hc_campo_count);

				$excel .= '<tr><td align="left">Total Filial:</td>
						<td align="left">'.$prepago_tbl1.'</td>
						<td align="left">'.$controle_tbl1.'</td>
						<td align="left">'.$recarga_tbl1.'</td>
						<td align="left">'.$hc_oficial_tbl1.'</td>
						<td align="left">'.$hc_campo_tbl1.'</td>
						<td align="left">'.round($this->divisao(($hc_campo_tbl1 * 100 ),$hc_oficial_tbl1),2).'%</td>
						<td align="left">'.round($this->divisao(($prepago_tbl1 * 100),$hc_campo_tbl1),2).'%</td>
						<td align="left">'.round($this->divisao(($controle_tbl1 * 100),$hc_campo_tbl1),2).'%</td>
						<td align="left">'.round($this->divisao(($recarga_tbl1 * 100),$hc_campo_tbl1),2).'%</td>
				</tr>';


				$excel .= '
				</tbody>
			</table>

			</div>

		</div>';

		//2

		$dados = $this->model_inputvendas->ajaxF2_2(
			$this->input->get('ano'),
			$this->input->get('mes'),
			$this->input->get('habitado'));

		$excel .= '<div  align="left">

			<table>
				<thead>
					<tr>
						<th></th>
						<th>Controle Total</th>
						<th>Controle Fácil</th>
						<th>%</th>
						<th>Controle Boleto</th>
						<th>%</th>
					</tr>
				</thead>
				<tbody>';
					
					
					$controle_facil_count = array();
					$controle_giga_count = array();

					foreach ($dados['geral_controle_usuario_f2'] as $value) {
						$excel .= '<tr>';

						$excel .= '<td align="left">'.$value->usuario.'</td>';
						$excel .= '<td align="left">'.$value->controle_total.'</td>';
						$excel .= '<td align="left">'.$value->controle_facil.'</td>';
						$excel .= '<td align="left">'.round(($this->divisao(($value->controle_facil * 100 ),($value->controle_giga+$value->controle_facil))),2).' %</td>';
						$excel .= '<td align="left">'.$value->controle_giga.'</td>';
						$excel .= '<td align="left">'.round(($this->divisao(($value->controle_giga * 100 ),($value->controle_giga+$value->controle_facil))),2).' %</td>';

						$excel .= '</tr>';

						array_push($controle_facil_count,$value->controle_facil);
						array_push($controle_giga_count,$value->controle_giga);
					} 

					$total_tbl2 = (array_sum($controle_facil_count) + array_sum($controle_giga_count));
					$total_facil_tbl2 = array_sum($controle_facil_count);
					$total_boleto_tbl2 = array_sum($controle_giga_count);

					$excel .= '<tr><td align="left">Total Filial:</td>
					<td align="left">'.$total_tbl2.'</td>
					<td align="left">'.$total_facil_tbl2.'</td>
					<td align="left">'.($this->divisao(($total_facil_tbl2  * 100),$total_tbl2)).' %</td>
					<td align="left">'.$total_boleto_tbl2.'</td>
					<td align="left">'.($this->divisao(($total_boleto_tbl2  * 100),$total_tbl2)).' %</td></tr>';


					
				$excel .= '</tbody>
			</table>

			</div>';	

			//3

		$dados = $this->model_inputvendas->ajaxF2_3(
			$this->input->get('ano'),
			$this->input->get('mes'),
			$this->input->get('habitado'));


		$excel .= $this->print_lista('Filial 2 Pré Pago',$dados['usuarios_iv'],$dados['detalhado_usuario_pre_f2'],false);
		$excel .= $this->print_lista('Filial 2 Controle Total',$dados['usuarios_iv'],$dados['detalhado_usuario_total_f2'],false);
		$excel .= $this->print_lista('Filial 2 Controle Fácil',$dados['usuarios_iv'],$dados['detalhado_usuario_facil_f2'],false);
		$excel .= $this->print_lista('Filial 2 Controle Boleto',$dados['usuarios_iv'],$dados['detalhado_usuario_giga_f2'],false);

		$excel .= $this->print_lista('REPRESENTATIVIDADE % - CONTROLE FÁCIL',$dados['usuarios_iv'],$dados['perc_facil_f2'],true);
		$excel .= $this->print_lista('REPRESENTATIVIDADE % - CONTROLE BOLETO',$dados['usuarios_iv'],$dados['perc_giga_f2'],true);

		$excel .= $this->print_lista('Filial 2 Recarga',$dados['usuarios_iv'],$dados['detalhado_usuario_recarga_f2'],false);
		$excel .= $this->print_lista('Filial 2 Migração',$dados['usuarios_iv'],$dados['detalhado_usuario_migracao_f2'],false);
		$excel .= $this->print_lista('Filial 2 HC Oficial',$dados['usuarios_iv'],$dados['detalhado_usuario_hc_oficial_f2'],false);
		$excel .= $this->print_lista('Filial 2 HC Em Campo',$dados['usuarios_iv'],$dados['detalhado_usuario_hc_campo_f2'],false);

		$excel .= $this->print_lista('REPRESENTATIVIDADE % - HCs EM CAMPO',$dados['usuarios_iv'],$dados['perc_campo_f2'],true);
		$excel .= $this->print_lista('PHC - PRÉ PAGO',$dados['usuarios_iv'],$dados['perc_pre_f2'],true);
		$excel .= $this->print_lista('PHC - CONTROLE',$dados['usuarios_iv'],$dados['perc_controle_f2'],true);
		$excel .= $this->print_lista('PHC - RECARGA',$dados['usuarios_iv'],$dados['perc_recarga_f2'],true);

		$arquivo = 'input_filiais_2.xls';

		// Configurações header para forçar o download
		header ("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
		header ("Cache-Control: must-revalidate, post-check=0, pre-check=0"); 
		header ("Content-Transfer-Encoding: binary"); 
		header ("Content-Type: application/vnd.ms-excel"); 
		header ("Expires: 0"); 
		header ("Content-Disposition: attachment; filename=\"{$arquivo}\"");
		header ("Content-Description: PHP Generated Data");

		// Envia o conteúdo do arquivo
		chr(255).chr(254).iconv("UTF-8", "UTF-16LE//IGNORE", $excel); 
		echo $excel;
		exit;

	}

	function print_lista($titulo = null, $usuarios = null,$lista = null,$perc = null) {

		$retornar = "";

	$retornar .= '<div class="mdl-grid contorno" style="background-color: white;">
		<h4>'.$titulo.'</h4>
		<table style="width: 100%">
			<thead>
				<tr>
					<th></th>';

					 for ($i=1; $i < 32; $i++) { 
						$retornar .= '<th>'.$i.'</th>';
					} 
					
					$retornar .= '<th>TOTAL</th>
				</tr>
			</thead>
			<tbody>';
				
				$array_totalizador = array();
				//Inicia
				for ($i=0; $i < 31; $i++) { 
					$array_totalizador[$i] = 0;
				}


				foreach ($usuarios as $usuario) {
			
					$array = array();

					$retornar .= '<tr>';
					$retornar .= '<td align="left">'.$usuario->usuario.'</td>';

					for ($i=1; $i < 32; $i++) { 
						$valor = 0;

						foreach ($lista as $f1) {
							if ($f1->data == $i && $usuario->id_usuario == $f1->id_usuario) {
								$valor = $f1->valor;
							}
						}

						array_push($array, $valor);
						if ($perc) {
							$retornar .= '<td align="left">'.$valor.'%</td>';
						} else {
							$retornar .= '<td align="left">'.$valor.'</td>';
						}
						
					}

					//Descobrindo a quantidade de valores acima de 0 estão no array, para poder dividir e extrair uma %
					if ($perc) {

						$count = 0;
						foreach ($array as $value) {
							if ($value > 0) {
								$count += 1;
							}
						}


						$retornar .= '<td align="left">'.$this->divisao(array_sum($array),$count).'%</td>';
					} else {
						$retornar .= '<td align="left">'.array_sum($array).'</td>';
					}
					
					$retornar .= '</tr>';

					for ($i=0; $i < 31; $i++) { 
						if ($array[$i] > 0) {
							$array_totalizador[$i] += $array[$i];
						}
						
					}


				}
					//Linhas em branco
					for ($j=0; $j < 3; $j++) { 
						$retornar .= '<tr>';
						$retornar .= '<td align="left"></td>';
						for ($i=0; $i < 31; $i++) { 
							$retornar .= '<td align="left"></td>';
						}
						$retornar .= '<td align="left">0</td>';
						$retornar .= '</tr>';
					}

					//Linha com o total
					$retornar .= '<tr>';
					$retornar .= '<td align="left">Total Filial</td>';
					for ($i=0; $i < 31; $i++) { 
						if ($perc) {
							$retornar .= '<td align="left">'.$array_totalizador[$i].'%</td>';
						} else {
							$retornar .= '<td align="left">'.$array_totalizador[$i].'</td>';
						}
					}

					if ($perc) {

						$count = 0;
						foreach ($array_totalizador as $value) {
							if ($value > 0) {
								$count += 1;
							}
						}

						$retornar .= '<td align="left">'.$this->divisao(array_sum($array_totalizador),$count).'%</td>';
					} else {
						$retornar .= '<td align="left">'.array_sum($array_totalizador).'</td>';
					}
					$retornar .= '</tr>';

			$retornar .= '</tbody>
			</table>
	</div>';

	return $retornar;

}


	//Histórico
	public function filtroAjax() {

		$usuario = $this->input->get('usuario');
		$de = $this->input->get('de');
		$ate = $this->input->get('ate');

		$resultado =  $this->model_inputvendas->filtrarPor($usuario,$de,$ate);

		echo '<table class="mdl-data-table mdl-js-data-table mdl-shadow--8dp">
	  <thead>
	    <tr>
	      <th>ID</th>
	      <th>Usuário</th>
	      <th>Filial</th>
	      <th>Data</th>
	      <th>Pré</th>
	      <th>Controle fácil</th>
	      <th>Controle Giga</th>
	      <th>Recarga</th>
	      <th>Banda Pré</th>
	      <th>Migracao</th>
	      <th>Chip Vendedor</th>
	      <th>HC Oficial</th>
	      <th>HC Campo</th>
	      <th>Data Envio</th>
	      <th>Aspectos</th>
	    </tr>
	  </thead>
	  <tbody>';
	    foreach ($resultado as $input_vendas) {
	    	echo '<tr>';
			echo '<td align="left">'.$input_vendas->id_input_vendas.'</td>';
			echo '<td align="left">'.$input_vendas->usuario.'</td>';
			echo '<td align="left">'.$input_vendas->filial.'</td>';
			echo '<td align="left">'.$input_vendas->data_input.'</td>';
			echo '<td align="left">'.$input_vendas->pre.'</td>';
			echo '<td align="left">'.$input_vendas->controle_facil.'</td>';
			echo '<td align="left">'.$input_vendas->controle_giga.'</td>';
			echo '<td align="left">'.$input_vendas->recarga.'</td>';
			echo '<td align="left">'.$input_vendas->banda_pre.'</td>';
			echo '<td align="left">'.$input_vendas->migracao.'</td>';
			echo '<td align="left">'.$input_vendas->chip_vendedor.'</td>';
			echo '<td align="left">'.$input_vendas->hc_oficial.'</td>';
			echo '<td align="left">'.$input_vendas->hc_campo.'</td>';
			echo '<td align="left">'.$input_vendas->data_envio.'</td>';
			echo '<td align="left">'.$input_vendas->aspectos.'</td>';
			echo '</tr>';
		} 
			  echo '</tbody>
			</table>';


	}

	public function excel_export(){

		// Definimos o nome do arquivo que será exportado
		$arquivo = 'input_vendas.xls';
		
		// Criamos uma tabela HTML com o formato da planilha
		$html = "<meta http-equiv=\"content-type\" content=\"application/xhtml+xml; charset=UTF-8\" />
		<table class=\"mdl-data-table mdl-js-data-table mdl-shadow--8dp\">
		  <thead>
		    <tr>
		      <th>ID</th>
		      <th>Usuário</th>
		      <th>Filial</th>
		      <th>Data</th>
		      <th>Pré</th>
		      <th>Controle fácil</th>
		      <th>Controle Giga</th>
		      <th>Recarga</th>
		      <th>Migracao</th>
		      <th>HC Oficial</th>
		      <th>HC Campo</th>
		      <th>Habitado</th>
		      <th>Data Envio</th>
		      <th>Aspectos</th>
		    </tr>
		  </thead>
		  <tbody>";

		  $usuario = $this->input->get('fk_usuario');
		  $de = $this->input->get('de');
		  $ate = $this->input->get('ate');

		  $dados = $this->model_inputvendas->filtrarPor($usuario,$de,$ate);

		    foreach ($dados as $input_vendas) {
		    	$html .= '<tr>';
				$html .= '<td align="left">'.$input_vendas->id_input_vendas.'</td>';
				$html .= '<td align="left">'.$input_vendas->usuario.'</td>';
				$html .= '<td align="left">'.$input_vendas->filial.'</td>';
				$html .= '<td align="left">'.$input_vendas->data_input.'</td>';
				$html .= '<td align="left">'.$input_vendas->pre.'</td>';
				$html .= '<td align="left">'.$input_vendas->controle_facil.'</td>';
				$html .= '<td align="left">'.$input_vendas->controle_giga.'</td>';
				$html .= '<td align="left">'.$input_vendas->recarga.'</td>';
				$html .= '<td align="left">'.$input_vendas->migracao.'</td>';
				$html .= '<td align="left">'.$input_vendas->hc_oficial.'</td>';
				$html .= '<td align="left">'.$input_vendas->hc_campo.'</td>';
				if ($input_vendas->habitado) {
					$html .= '<td align="left">Habitado</td>';
				} else {
					$html .= '<td align="left">Desabitado</td>';
				}
				$html .= '<td align="left">'.$input_vendas->data_envio.'</td>';
				$html .= '<td align="left">'.$input_vendas->aspectos.'</td>';
				$html .= '</tr>';
			} 

		  $html .= "</tbody>
		</table>";

		// Configurações header para forçar o download
		header ("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
		header ("Cache-Control: must-revalidate, post-check=0, pre-check=0"); 
		header ("Content-Transfer-Encoding: binary"); 
		header ("Content-Type: application/vnd.ms-excel"); 
		header ("Expires: 0"); 
		header ("Content-Disposition: attachment; filename=\"{$arquivo}\"");
		header ("Content-Description: PHP Generated Data");

		// Envia o conteúdo do arquivo
		chr(255).chr(254).iconv("UTF-8", "UTF-16LE//IGNORE", $html); 
		echo $html;
		exit;

	}

	public function persistencia() {

		//Dados a serem inseridos, os não preenchidos viram NULL
		$fk_usuario = $this->nulo_se_vazio($this->input->get('fk_usuario'));
		$filial = $this->nulo_se_vazio($this->input->get('filial'));
		$data_input = $this->nulo_se_vazio($this->input->get('data_input'));
		$pre = $this->nulo_se_vazio($this->input->get('pre'));
		$controle_facil = $this->nulo_se_vazio($this->input->get('controle_facil'));
		$controle_giga = $this->nulo_se_vazio($this->input->get('controle_giga'));
		$recarga = $this->nulo_se_vazio($this->input->get('recarga'));
		//$banda_pre = $this->nulo_se_vazio($this->input->get('banda_pre'));
		$migracao = $this->nulo_se_vazio($this->input->get('migracao'));
		//$chip_vendedor = $this->nulo_se_vazio($this->input->get('chip_vendedor'));
		$hc_oficial = $this->nulo_se_vazio($this->input->get('hc_oficial'));
		$hc_campo = $this->nulo_se_vazio($this->input->get('hc_campo'));
		$aspectos = $this->nulo_se_vazio($this->input->get('aspectos'));

		$inputVendas = array (

			'fk_usuario' => $fk_usuario,
			'filial' => $filial,
			'data_input' => $data_input,
			'pre' => $pre,
			'controle_facil' => $controle_facil,
			'controle_giga' => $controle_giga,
			'recarga' => $recarga,
			'migracao' => $migracao,
			'hc_oficial' => $hc_oficial,
			'hc_campo' => $hc_campo,
			'aspectos' => $aspectos,
			'habitado' => true

		);

		//Armazena
		$inserido = $this->model_inputvendas->insert($inputVendas);

		//Desabitado
		if ($this->input->get('pre_de') != '' ||
			$this->input->get('controle_facil_de') != '' ||
			$this->input->get('controle_giga_de') != '' ||
			$this->input->get('recarga_de') != '' ||
			$this->input->get('migracao_de') != '') {

			$inputVendas = array (

				'fk_usuario' => $fk_usuario,
				'filial' => $filial,
				'data_input' => $data_input,
				'pre' => $this->input->get('pre_de'),
				'controle_facil' => $this->input->get('controle_facil_de'),
				'controle_giga' => $this->input->get('controle_giga_de'),
				'recarga' => $this->input->get('recarga_de'),
				'migracao' => $this->input->get('migracao_de'),
				'hc_oficial' => $hc_oficial,
				'hc_campo' => $hc_campo,
				'aspectos' => $aspectos,
				'habitado' => false

			);

			//Armazena
			$inserido = $this->model_inputvendas->insert($inputVendas);


		}


		//Usado para preencher o novo formulário.
		$usuario = $this->nulo_vazio($this->input->get('usuario'));

		//Somente insere sem envio do e-mail.
		if($inserido) {
			$dados = array('aviso' => "Formulário enviado com sucesso!", 'usuario' => $usuario, 'fk_usuario' => $fk_usuario, 'filial' => $this->model_inputvendas->listar_filiais());
			$this->load->view('formulario_iv',$dados);
		} else {
			$dados = array('aviso' => "Formulário enviado com sucesso!", 'usuario' => $usuario, 'fk_usuario' => $fk_usuario, 'filial' => $this->model_inputvendas->listar_filiais());
			$this->load->view('formulario_iv',$dados);
		}



	}

	public function grafico_controles(){
			
		$dados = $this->model_inputvendas->graficosControles(
			$this->input->get('ano'),
			$this->input->get('mes'),
			$this->input->get('habitado'),
			$this->input->get('filial'));

		$resultado['boleto'] = $dados->boleto;
		$resultado['facil'] = $dados->facil;


		header('Content-Type: application/json');
		echo json_encode($resultado);	


	}

	//troca campos não preenchidos por um texto.
	public function nulo_vazio($campo = null) {

		if(is_null($campo)) {
			return '<div style="background-color: #D3D3D3;"> Em Branco </div>';
		} else {
			return $campo;
		}

	}

	//troca campos não preenchidos por NULL.
	public function nulo_se_vazio($campo = null) {

		if(is_null($campo)) {
			return null;
		} else {
			return $campo;
		}

	}

	function divisao($v1,$v2){
		if ($v1 > 0 && $v2 > 0) {
			return round(($v1/$v2),2);
		} else {
			return 0;
		}
	}

}