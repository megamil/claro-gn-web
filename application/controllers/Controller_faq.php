<?php defined('BASEPATH') OR exit('No direct script access allowed');

	class controller_faq extends CI_Controller {

		public function novo_faq(){

			if($_FILES['imagem']['name'] != ''){

				$config['upload_path'] = $_SERVER['DOCUMENT_ROOT'].'/'.base_url().'upload/faq';
				$config['allowed_types'] = 'gif|jpg|png';

				//Tira os ascentos e espaços etc..
				$nome = strtolower(strtr(utf8_decode(trim($_FILES['imagem']['name'])), utf8_decode("áàãâéêíóôõúüñçÁÀÃÂÉÊÍÓÔÕÚÜÑÇ"),"aaaaeeiooouuncAAAAEEIOOOUUNC-"));

				$nome = str_replace(array('.','?',' '), '-', $nome);

				//Quando tirar o ponto da extenção.
				$nome = str_replace('-png', '.png', $nome);
				$nome = str_replace('-gif', '.gif', $nome);
				$nome = str_replace('-jpg', '.jpg', $nome);

				//Gera um nome único para o arquivo
				$arquivo = basename(time().uniqid(md5($nome)).$nome);

				$config['file_name'] = $arquivo;
				
				$this->load->library('upload', $config);
			
				$this->upload->do_upload('imagem');

				$campos = array (

					'titulo_faq' => $this->input->post('titulo_faq'),
					'descricao_faq' => $this->input->post('descricao_faq').'<img src="'.base_url().'upload/faq/'.$arquivo.'">',
					'tipo_faq' => $this->input->post('tipo_faq')

				);

				$this->session->set_flashdata($campos);

				redirect('main/redirecionar/cadastro-view_novo_faq');

			} else {		

				$this->form_validation->set_rules('titulo_faq','Título','required|is_unique[grupos.nome_grupo]');
				$this->form_validation->set_rules('descricao_faq','Descrição','required');

				$campos = array (

					'titulo_faq' => $this->input->post('titulo_faq'),
					'descricao_faq' => $this->input->post('descricao_faq'),
					'tipo_faq' => $this->input->post('tipo_faq')

				);

				if($this->form_validation->run()) {

					$id = $this->model_faq->criarFaq($campos);

					$this->session->set_flashdata('tipo','sucesso');
					$this->session->set_flashdata('titulo','FAQ criado!');
					$this->session->set_flashdata('mensagem','Faq: '.$this->input->post('titulo').' criado com sucesso!');

					redirect('main/redirecionar/editar-view_editar_faq/'.$id);


				} else {

					$this->session->set_flashdata('tipo','erro');
					$this->session->set_flashdata('titulo','Erro ao criar faq.');
					$this->session->set_flashdata('mensagem',validation_errors());

					$this->session->set_flashdata($campos);

					redirect('main/redirecionar/cadastro-view_novo_faq');
				}

			}

		}

		
		public function editar_faq(){

			$id = $this->input->post('id_faq');

			if($_FILES['imagem']['name'] != ''){

				$config['upload_path'] = $_SERVER['DOCUMENT_ROOT'].'/'.base_url().'upload/faq';
				$config['allowed_types'] = 'gif|jpg|png';

				//Tira os ascentos e espaços etc..
				$nome = strtolower(strtr(utf8_decode(trim($_FILES['imagem']['name'])), utf8_decode("áàãâéêíóôõúüñçÁÀÃÂÉÊÍÓÔÕÚÜÑÇ"),"aaaaeeiooouuncAAAAEEIOOOUUNC-"));

				$nome = str_replace(array('.','?',' '), '-', $nome);

				//Quando tirar o ponto da extenção.
				$nome = str_replace('-png', '.png', $nome);
				$nome = str_replace('-gif', '.gif', $nome);
				$nome = str_replace('-jpg', '.jpg', $nome);

				//Gera um nome único para o arquivo
				$arquivo = basename(time().uniqid(md5($nome)).$nome);

				$config['file_name'] = $arquivo;
				
				$this->load->library('upload', $config);
			
				$this->upload->do_upload('imagem');

				$campos = array (

					'titulo_faq' => $this->input->post('titulo_faq'),
					'descricao_faq' => $this->input->post('descricao_faq').'<img src="'.base_url().'upload/faq/'.$arquivo.'">',
					'tipo_faq' => $this->input->post('tipo_faq')

				);

				$this->session->set_flashdata($campos);

				redirect('main/redirecionar/editar-view_editar_faq/'.$id);

			} else {

				$this->form_validation->set_rules('titulo_faq','Título','required|is_unique[grupos.nome_grupo]');
				$this->form_validation->set_rules('descricao_faq','Descrição','required');

				$campos = array (

					'titulo_faq' => $this->input->post('titulo_faq'),
					'descricao_faq' => $this->input->post('descricao_faq'),
					'tipo_faq' => $this->input->post('tipo_faq')

				);

				if($this->form_validation->run()) {

					$this->model_faq->editarFaq($campos,$id);

					$this->session->set_flashdata('tipo','sucesso');
					$this->session->set_flashdata('titulo','FAQ editado!');
					$this->session->set_flashdata('mensagem','Faq: '.$this->input->post('titulo').' editado com sucesso!');

					redirect('main/redirecionar/editar-view_editar_faq/'.$id);


				} else {

					$this->session->set_flashdata('tipo','erro');
					$this->session->set_flashdata('titulo','Erro ao editar faq.');
					$this->session->set_flashdata('mensagem',validation_errors());

					$this->session->set_flashdata($campos);

					redirect('main/redirecionar/editar-view_editar_faq/'.$id);
				}

			}

		}


	}