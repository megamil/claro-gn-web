<?php defined('BASEPATH') OR exit('No direct script access allowed');

	class controller_pdf extends CI_Controller {


		public function analise_promotor_pdv(){

			function resultado($valor = null,$total = null){

				if($total > 0 && $valor > 0){
					$perc = (($valor * 100) / $total);
					return number_format($perc, 2, '.', '');
				} else {
					return 0;
				}

			}

			$de = str_replace('-', '/', $this->input->get('de'));
			$ate = str_replace('-', '/', $this->input->get('ate'));

			$dados['de'] = $de;
			$dados['ate'] = $ate;

			$resultado = $this->model_relatorios->atende_ajax(1,$de,$ate);


			$dados['qts_apresentacao_a1'] = 
				$resultado->qts_apresentacao_a 
					.' ('.
						resultado($resultado->qts_apresentacao_a,$resultado->total).'%)';
			$dados['qts_apresentacao_b1'] = 
				$resultado->qts_apresentacao_b 
					.' ('.
						resultado($resultado->qts_apresentacao_b,$resultado->total).'%)';
			$dados['qts_apresentacao_c1'] = 
				$resultado->qts_apresentacao_c 
					.' ('.
						resultado($resultado->qts_apresentacao_c,$resultado->total).'%)';
			$dados['qts_apresentacao_a_p1'] = 
				$resultado->qts_apresentacao_a_p 
					.' ('.
						resultado($resultado->qts_apresentacao_a_p,$resultado->total).'%)';
			$dados['qts_apresentacao_b_p1'] = 
				$resultado->qts_apresentacao_b_p 
					.' ('.
						resultado($resultado->qts_apresentacao_b_p,$resultado->total).'%)';
			$dados['qts_apresentacao_c_p1'] = 
				$resultado->qts_apresentacao_c_p 
					.' ('.
						resultado($resultado->qts_apresentacao_c_p,$resultado->total).'%)';
			$dados['qts_apresentacao_a_n1'] = 
				$resultado->qts_apresentacao_a_n 
					.' ('.
						resultado($resultado->qts_apresentacao_a_n,$resultado->total).'%)';
			$dados['qts_apresentacao_b_n1'] = 
				$resultado->qts_apresentacao_b_n 
					.' ('.
						resultado($resultado->qts_apresentacao_b_n,$resultado->total).'%)';
			$dados['qts_apresentacao_c_n1'] = 
				$resultado->qts_apresentacao_c_n 
					.' ('.
						resultado($resultado->qts_apresentacao_c_n,$resultado->total).'%)';

			//

			$dados['qts_book_a1'] = 
				$resultado->qts_book_a 
					.' ('.
						resultado($resultado->qts_book_a,$resultado->total).'%)';
			$dados['qts_book_b1'] = 
				$resultado->qts_book_b 
					.' ('.
						resultado($resultado->qts_book_b,$resultado->total).'%)';
			$dados['qts_book_c1'] = 
				$resultado->qts_book_c 
					.' ('.
						resultado($resultado->qts_book_c,$resultado->total).'%)';
			$dados['qts_book_a_p1'] = 
				$resultado->qts_book_a_p 
					.' ('.
						resultado($resultado->qts_book_a_p,$resultado->total).'%)';
			$dados['qts_book_b_p1'] = 
				$resultado->qts_book_b_p 
					.' ('.
						resultado($resultado->qts_book_b_p,$resultado->total).'%)';
			$dados['qts_book_c_p1'] = 
				$resultado->qts_book_c_p 
					.' ('.
						resultado($resultado->qts_book_c_p,$resultado->total).'%)';
			$dados['qts_book_a_n1'] = 
				$resultado->qts_book_a_n 
					.' ('.
						resultado($resultado->qts_book_a_n,$resultado->total).'%)';
			$dados['qts_book_b_n1'] = 
				$resultado->qts_book_b_n 
					.' ('.
						resultado($resultado->qts_book_b_n,$resultado->total).'%)';
			$dados['qts_book_c_n1'] = 
				$resultado->qts_book_c_n 
					.' ('.
						resultado($resultado->qts_book_c_n,$resultado->total).'%)';

			//

			$dados['qts_conhecimento_a1'] = 
				$resultado->qts_conhecimento_a 
					.' ('.
						resultado($resultado->qts_conhecimento_a,$resultado->total).'%)';
			$dados['qts_conhecimento_b1'] = 
				$resultado->qts_conhecimento_b 
					.' ('.
						resultado($resultado->qts_conhecimento_b,$resultado->total).'%)';
			$dados['qts_conhecimento_c1'] = 
				$resultado->qts_conhecimento_c 
					.' ('.
						resultado($resultado->qts_conhecimento_c,$resultado->total).'%)';
			$dados['qts_conhecimento_a_p1'] = 
				$resultado->qts_conhecimento_a_p 
					.' ('.
						resultado($resultado->qts_conhecimento_a_p,$resultado->total).'%)';
			$dados['qts_conhecimento_b_p1'] = 
				$resultado->qts_conhecimento_b_p 
					.' ('.
						resultado($resultado->qts_conhecimento_b_p,$resultado->total).'%)';
			$dados['qts_conhecimento_c_p1'] = 
				$resultado->qts_conhecimento_c_p 
					.' ('.
						resultado($resultado->qts_conhecimento_c_p,$resultado->total).'%)';
			$dados['qts_conhecimento_a_n1'] = 
				$resultado->qts_conhecimento_a_n 
					.' ('.
						resultado($resultado->qts_conhecimento_a_n,$resultado->total).'%)';
			$dados['qts_conhecimento_b_n1'] = 
				$resultado->qts_conhecimento_b_n 
					.' ('.
						resultado($resultado->qts_conhecimento_b_n,$resultado->total).'%)';
			$dados['qts_conhecimento_c_n1'] = 
				$resultado->qts_conhecimento_c_n 
					.' ('.
						resultado($resultado->qts_conhecimento_c_n,$resultado->total).'%)';

			$resultado2 = $this->model_relatorios->atende_ajax(2,$de,$ate);

			$dados['qts_apresentacao_a2'] = 
				$resultado2->qts_apresentacao_a 
					.' ('.
						resultado($resultado2->qts_apresentacao_a,$resultado2->total).'%)';
			$dados['qts_apresentacao_b2'] = 
				$resultado2->qts_apresentacao_b 
					.' ('.
						resultado($resultado2->qts_apresentacao_b,$resultado2->total).'%)';
			$dados['qts_apresentacao_c2'] = 
				$resultado2->qts_apresentacao_c 
					.' ('.
						resultado($resultado2->qts_apresentacao_c,$resultado2->total).'%)';
			$dados['qts_apresentacao_a_p2'] = 
				$resultado2->qts_apresentacao_a_p 
					.' ('.
						resultado($resultado2->qts_apresentacao_a_p,$resultado2->total).'%)';
			$dados['qts_apresentacao_b_p2'] = 
				$resultado2->qts_apresentacao_b_p 
					.' ('.
						resultado($resultado2->qts_apresentacao_b_p,$resultado2->total).'%)';
			$dados['qts_apresentacao_c_p2'] = 
				$resultado2->qts_apresentacao_c_p 
					.' ('.
						resultado($resultado2->qts_apresentacao_c_p,$resultado2->total).'%)';
			$dados['qts_apresentacao_a_n2'] = 
				$resultado2->qts_apresentacao_a_n 
					.' ('.
						resultado($resultado2->qts_apresentacao_a_n,$resultado2->total).'%)';
			$dados['qts_apresentacao_b_n2'] = 
				$resultado2->qts_apresentacao_b_n 
					.' ('.
						resultado($resultado2->qts_apresentacao_b_n,$resultado2->total).'%)';
			$dados['qts_apresentacao_c_n2'] = 
				$resultado2->qts_apresentacao_c_n 
					.' ('.
						resultado($resultado2->qts_apresentacao_c_n,$resultado2->total).'%)';

			//

			$dados['qts_book_a2'] = 
				$resultado2->qts_book_a 
					.' ('.
						resultado($resultado2->qts_book_a,$resultado2->total).'%)';
			$dados['qts_book_b2'] = 
				$resultado2->qts_book_b 
					.' ('.
						resultado($resultado2->qts_book_b,$resultado2->total).'%)';
			$dados['qts_book_c2'] = 
				$resultado2->qts_book_c 
					.' ('.
						resultado($resultado2->qts_book_c,$resultado2->total).'%)';
			$dados['qts_book_a_p2'] = 
				$resultado2->qts_book_a_p 
					.' ('.
						resultado($resultado2->qts_book_a_p,$resultado2->total).'%)';
			$dados['qts_book_b_p2'] = 
				$resultado2->qts_book_b_p 
					.' ('.
						resultado($resultado2->qts_book_b_p,$resultado2->total).'%)';
			$dados['qts_book_c_p2'] = 
				$resultado2->qts_book_c_p 
					.' ('.
						resultado($resultado2->qts_book_c_p,$resultado2->total).'%)';
			$dados['qts_book_a_n2'] = 
				$resultado2->qts_book_a_n 
					.' ('.
						resultado($resultado2->qts_book_a_n,$resultado2->total).'%)';
			$dados['qts_book_b_n2'] = 
				$resultado2->qts_book_b_n 
					.' ('.
						resultado($resultado2->qts_book_b_n,$resultado2->total).'%)';
			$dados['qts_book_c_n2'] = 
				$resultado2->qts_book_c_n 
					.' ('.
						resultado($resultado2->qts_book_c_n,$resultado2->total).'%)';

			//

			$dados['qts_conhecimento_a2'] = 
				$resultado2->qts_conhecimento_a 
					.' ('.
						resultado($resultado2->qts_conhecimento_a,$resultado2->total).'%)';
			$dados['qts_conhecimento_b2'] = 
				$resultado2->qts_conhecimento_b 
					.' ('.
						resultado($resultado2->qts_conhecimento_b,$resultado2->total).'%)';
			$dados['qts_conhecimento_c2'] = 
				$resultado2->qts_conhecimento_c 
					.' ('.
						resultado($resultado2->qts_conhecimento_c,$resultado2->total).'%)';
			$dados['qts_conhecimento_a_p2'] = 
				$resultado2->qts_conhecimento_a_p 
					.' ('.
						resultado($resultado2->qts_conhecimento_a_p,$resultado2->total).'%)';
			$dados['qts_conhecimento_b_p2'] = 
				$resultado2->qts_conhecimento_b_p 
					.' ('.
						resultado($resultado2->qts_conhecimento_b_p,$resultado2->total).'%)';
			$dados['qts_conhecimento_c_p2'] = 
				$resultado2->qts_conhecimento_c_p 
					.' ('.
						resultado($resultado2->qts_conhecimento_c_p,$resultado2->total).'%)';
			$dados['qts_conhecimento_a_n2'] = 
				$resultado2->qts_conhecimento_a_n 
					.' ('.
						resultado($resultado2->qts_conhecimento_a_n,$resultado2->total).'%)';
			$dados['qts_conhecimento_b_n2'] = 
				$resultado2->qts_conhecimento_b_n 
					.' ('.
						resultado($resultado2->qts_conhecimento_b_n,$resultado2->total).'%)';
			$dados['qts_conhecimento_c_n2'] = 
				$resultado2->qts_conhecimento_c_n 
					.' ('.
						resultado($resultado2->qts_conhecimento_c_n,$resultado2->total).'%)';

			$dados['total_atende'] = ($resultado->qts_apresentacao_a + 
										$resultado->qts_apresentacao_b + 
										$resultado->qts_apresentacao_c + 
										$resultado->qts_book_a + 
										$resultado->qts_book_b + 
										$resultado->qts_book_c + 
										$resultado->qts_conhecimento_a + 
										$resultado->qts_conhecimento_b + 
										$resultado->qts_conhecimento_c + 
										
										$resultado2->qts_apresentacao_a + 
										$resultado2->qts_apresentacao_b + 
										$resultado2->qts_apresentacao_c + 
										$resultado2->qts_book_a + 
										$resultado2->qts_book_b + 
										$resultado2->qts_book_c + 
										$resultado2->qts_conhecimento_a + 
										$resultado2->qts_conhecimento_b + 
										$resultado2->qts_conhecimento_c); 

			$dados['total_atendep'] = ($resultado->qts_apresentacao_a_p + 
										$resultado->qts_apresentacao_b_p + 
										$resultado->qts_apresentacao_c_p + 
										$resultado->qts_book_a_p + 
										$resultado->qts_book_b_p + 
										$resultado->qts_book_c_p + 
										$resultado->qts_conhecimento_a_p + 
										$resultado->qts_conhecimento_b_p + 
										$resultado->qts_conhecimento_c_p + 

										$resultado2->qts_apresentacao_a_p + 
										$resultado2->qts_apresentacao_b_p + 
										$resultado2->qts_apresentacao_c_p + 
										$resultado2->qts_book_a_p + 
										$resultado2->qts_book_b_p + 
										$resultado2->qts_book_c_p + 
										$resultado2->qts_conhecimento_a_p + 
										$resultado2->qts_conhecimento_b_p + 
										$resultado2->qts_conhecimento_c_p); 	

			$dados['total_natende'] = ($resultado->qts_apresentacao_a_n + 
										$resultado->qts_apresentacao_b_n + 
										$resultado->qts_apresentacao_c_n + 
										$resultado->qts_book_a_n + 
										$resultado->qts_book_b_n + 
										$resultado->qts_book_c_n + 
										$resultado->qts_conhecimento_a_n + 
										$resultado->qts_conhecimento_b_n + 
										$resultado->qts_conhecimento_c_n + 

										$resultado2->qts_apresentacao_a_n + 
										$resultado2->qts_apresentacao_b_n + 
										$resultado2->qts_apresentacao_c_n + 
										$resultado2->qts_book_a_n + 
										$resultado2->qts_book_b_n + 
										$resultado2->qts_book_c_n + 
										$resultado2->qts_conhecimento_a_n + 
										$resultado2->qts_conhecimento_b_n + 
										$resultado2->qts_conhecimento_c_n); 							

			$this->load->view('pdf/analise_promotor_pdv',$dados);

		}

		public function recorrencia(){

			function resultado($valor = null,$total = null){

				if($total > 0 && $valor > 0){
					$perc = (($valor * 100) / $total);
					return number_format($perc, 2, '.', '');
				} else {
					return 0;
				}

			}

			$cnpj = str_replace(array('.','/','-'),'', $this->input->get('cnpj'));
			$de = $this->input->get('de');
			$ate = $this->input->get('ate');
			$filial_filtro = $this->input->get('filial_filtro');
			$gn_filtro = $this->input->get('gn_filtro');

			$nomeGN = strtoupper($this->input->get('nomeGN')); //Usado somente para leitura

			$dados['de'] = $de;
			$dados['ate'] = $ate;

			$dados['registros'] = $this->model_relatorios->recorrencia_ajax_pdf($cnpj,$de,$ate,$filial_filtro,$gn_filtro);

			$this->load->view('pdf/recorrencia',$dados);


		}

		public function pdf_mediatam(){

			$nomeGN = strtoupper($this->input->get('nomeGN')); //Usado somente para leitura
			$cnpj = str_replace(array('.','/','-'),'', $this->input->get('cnpj'));
			$de = $this->input->get('de');
			$ate = $this->input->get('ate');
			$gn_filtro = $this->input->get('gn_filtro');

			$total = $this->model_relatorios->mediatam_ajax($gn_filtro,"",$de,$ate,$cnpj);
			$filial1 = $this->model_relatorios->mediatam_ajax($gn_filtro," and filial = 1",$de,$ate,$cnpj);
			$filial2 = $this->model_relatorios->mediatam_ajax($gn_filtro," and filial = 2",$de,$ate,$cnpj);

			$dados['de'] = $de;
			$dados['ate'] = $ate;

			$dados['total_mobile'] = $total->row()->mobile;
			$dados['total_telefonia'] = $total->row()->telefonia;
			$dados['total_aberto'] = $total->row()->aberto;

			$totalTemp = $total->row()->telefonia + $total->row()->aberto + $total->row()->mobile;

			$dados['total_geral'] = $totalTemp;

			$dados['total_perc_t'] = $this->percentual_Grafico($totalTemp, $total->row()->telefonia);
			$dados['total_perc_a'] = $this->percentual_Grafico($totalTemp, $total->row()->aberto);
			$dados['total_perc_m'] = $this->percentual_Grafico($totalTemp, $total->row()->mobile);

			$dados['filial1_mobile'] = $filial1->row()->mobile;
			$dados['filial1_telefonia'] = $filial1->row()->telefonia;
			$dados['filial1_aberto'] = $filial1->row()->aberto;

			$totalTemp = $filial1->row()->telefonia + $filial1->row()->aberto + $filial1->row()->mobile;

			$dados['f1_perc_t'] = $this->percentual_Grafico($totalTemp, $filial1->row()->telefonia);
			$dados['f1_perc_a'] = $this->percentual_Grafico($totalTemp, $filial1->row()->aberto);
			$dados['f1_perc_m'] = $this->percentual_Grafico($totalTemp, $filial1->row()->mobile);

			$dados['filial2_mobile'] = $filial2->row()->mobile;
			$dados['filial2_telefonia'] = $filial2->row()->telefonia;
			$dados['filial2_aberto'] = $filial2->row()->aberto;

			$totalTemp = $filial2->row()->telefonia + $filial2->row()->aberto + $filial2->row()->mobile;

			$dados['f2_perc_t'] = $this->percentual_Grafico($totalTemp, $filial2->row()->telefonia);
			$dados['f2_perc_a'] = $this->percentual_Grafico($totalTemp, $filial2->row()->aberto);
			$dados['f2_perc_m'] = $this->percentual_Grafico($totalTemp, $filial2->row()->mobile);

			$this->load->view('pdf/tipos_pdv',$dados);

			// $cnpj = str_replace(array('.','/','-'),'', $this->input->get('cnpj'));
			// $de = $this->input->get('de');
			// $ate = $this->input->get('ate');
			// $gn_filtro = $this->input->get('gn_filtro');
			// $nomeGN = strtoupper($this->input->get('nomeGN')); //Usado somente para leitura

			// $dados['de'] = $de;
			// $dados['ate'] = $ate;

			// $dados['registros'] = $this->model_relatorios->recorrencia_ajax_pdf($cnpj,$de,$ate,$filial_filtro,$gn_filtro);

			// $this->load->view('pdf/recorrencia',$dados);


		}

		public function horario_pdf(){

			$de = $this->input->get('de');
			$ate = $this->input->get('ate');

			$dados = $this->model_relatorios->horas_pdf($de,$ate);

			$dados['graficof1'] = $this->convert_horas_numeros($dados['entrou_mediaf1']);
			$dados['graficof2'] = $this->convert_horas_numeros($dados['entrou_mediaf2']);

			$dados['grafico2f1'] = $this->convert_horas_numeros($dados['saiu_mediaf1']);
			$dados['grafico2f2'] = $this->convert_horas_numeros($dados['saiu_mediaf2']);


			$this->load->view('pdf/media_horas',$dados);

		}

		public function pdf_visitas(){

			$de = $this->input->get('de');
			$ate = $this->input->get('ate');
			$dados = $this->model_relatorios->visitas_pdf($de,$ate);

			$this->load->view('pdf/media_visitas',$dados);

		}

		public function percentual_Grafico($total = null, $quantidade = null){
			
			if ($quantidade > 0 && $total > 0) {
				return round((($quantidade * 100) / $total));
			} else {
				return 0;
			}


		}

		//Usado para que o gráfico saiba dividir os valores na pizza.
		public function convert_horas_numeros($horas = null){

			if($horas != ''){

				$temp = explode(':',$horas);
				return $temp[0].'.'.$temp[1];

			} else {

				return 1;

			}

		}


	}