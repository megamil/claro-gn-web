<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
 function notificando($apiKey = null, $tokens = null, $mensagem = null) {
            
            $devicesToken = array ();

            $resultadoHTML = "";

            $prontos = 0; //Garante que passou por todos token

            while ($prontos < $tokens->num_rows()) {

                $devicesToken[] = $tokens->row($prontos)->token;
                    
                $prontos += 1;

                if(count($devicesToken) == 999) { //Garante que não passará o limite de envio
                    
                    $gcpm = new FCMPushMessage($apiKey);
                    $gcpm->setDevices($devicesToken);

                    $titulo = "Claro GN";
                    $mensagem = $mensagem;
                    $response = $gcpm->send(array('mensagem' => $mensagem,'titulo' => $titulo));
                    //Atualizar token ou remover, caso necessário.
                    $resultadoHTML .= $this->atualizarBD($response, $devicesToken);

                    $resultadoHTML .= "<br><hr><br>";

                    $devicesToken = array(); //zera o array, e fica pronto para mais 999 tokens

                }


            }
            
            if(count($devicesToken) > 0) { //Garante que se não entrar no if do while, enviara os tokens existentes no array.

                $gcpm = new FCMPushMessage($apiKey);
                $gcpm->setDevices($devicesToken);

                $titulo = "Claro GN";
                $mensagem = $mensagem;
                $response = $gcpm->send(array('mensagem' => $mensagem,'titulo' => $titulo));

                $resultadoHTML .= atualizarBD($response, $devicesToken);

                $resultadoHTML .= "<br><hr><br>";

            }
            //Conferir envio das notificações.
            echo $resultadoHTML;

            /*$this->session->set_userdata('aviso','Notificação enviada!');
            $this->session->set_userdata('tipo','success');

            $content = array(
                'notificacao' => $_POST["message"],
                'fk_aplicacao_notificada' => $_POST["id"],
                'aparelhos' => $prontos
            );

            $this->armazenar->historico_notificacoes($content);
            redirect('editar/historico_notificacoes/'.$_POST["id"]);*/
      

    }

     function atualizarBD($response = null, $devicesToken = null) {
        // RESULT JSON
                $html = '';
                $ci =& get_instance();
                $resultJson = json_decode($response);
                foreach($resultJson as $key=>$value){
                    if(is_array($value)){
                        $html .= $key.'=>{<br />';
                        $i = 0;
                        
                        foreach($value as $k=>$v){
                            $html .= '&nbsp;&nbsp;&nbsp;&nbsp;{&nbsp;';
                            foreach($v as $kObj=>$vObj){
                                $html .= $kObj.'=>'.$vObj;
                                
                                // UPDATE REG ID
                                    if(strcasecmp($kObj, 'registration_id') == 0 && strlen( trim($vObj) ) > 0){

                                        $novo = trim($vObj);

                                        if($ci->model_webservice->atualizarToken($novo,$devicesToken[$i])) {
                                        $html .= " Token : ".$devicesToken[$i]." Atualizado com sucesso para: ".$novo;
                                        } else {
                                            $html .= " Falha na atualização.";
                                        }

                                    }
                                // DELETE REG ID
                                    else if(strcasecmp($kObj, 'error') == 0 && strcasecmp($vObj, 'NotRegistered') == 0){

                                        if($ci->model_webservice->deletarToken($devicesToken[$i])) {
                                            $html .= " Token: ".$devicesToken[$i]." Deletado com sucesso.";
                                        } else {
                                            $html .= " Token: ".$devicesToken[$i]." Falhou ao deletar.";
                                        }

                                    }
                                    else if(strcasecmp($kObj, 'error') == 0 && strcasecmp($vObj, 'MismatchSenderId') == 0){

                                        if($ci->model_webservice->deletarToken($devicesToken[$i])) {
                                            $html .= " Token: ".$devicesToken[$i]." Deletado com sucesso.";
                                        } else {
                                            $html .= " Token: ".$devicesToken[$i]." Falhou ao deletar.";
                                        }

                                    } else if(strcasecmp($kObj, 'error') == 0 && strcasecmp($vObj, 'InvalidRegistration') == 0){
                                        
                                        if($ci->model_webservice->deletarToken($devicesToken[$i])) {
                                            $html .= " Token: ".$devicesToken[$i]." Deletado com sucesso.";
                                        } else {
                                            $html .= " Token: ".$devicesToken[$i]." Falhou ao deletar.";
                                        }

                                    } else {

                                        $html .= " Token ".$devicesToken[$i]." OK";

                                    }
                                    
                                $html .= '<br />';
                            }
                            $html = rtrim($html, '<br />');
                            $html .= '&nbsp;}<br />';
                            $i++;
                        }
                        $html .= '}<br />';
                    }
                    else{
                        $html .= $key.'=>'.$value.'<br />';
                    }
                }

                return $html; // PRINT RESULT

    }


class FCMPushMessage extends CI_Controller {

    var $url = 'https://fcm.googleapis.com/fcm/send';
    
    var $serverApiKey = "";
    var $devices = array();
    
    function FCMPushMessage($apiKeyIn){
        $this->serverApiKey = $apiKeyIn;
    }
    function setDevices($deviceIds){
    
        if(is_array($deviceIds)){
            $this->devices = $deviceIds;
        } else {
            $this->devices = array($deviceIds);
        }
    
    }
    function send($data = null){
        
        if(!is_array($this->devices) || count($this->devices) == 0){
            $this->error("No devices set");
        }
        
        if(strlen($this->serverApiKey) < 8){
            $this->error("Server API Key not set");
        }

        $notification = array
        (
            "body" => $data['mensagem'],
            "title" => $data['titulo'],
            "badge" => 1,
            'vibrate'   => 1,
            'sound'     => 1,
        );
        
        $fields = array(
            'registration_ids'  => $this->devices,
            'priority'              => 'high',
            'content_available'     => true,
            'notification'              => $notification,
            'data'              => $data,
        );
        

        $headers = array( 
            'Authorization: key=' . $this->serverApiKey,
            'Content-Type: application/json'
        );
        $ch = curl_init();
        
        curl_setopt( $ch, CURLOPT_URL, $this->url );
        
        curl_setopt( $ch, CURLOPT_POST, true );
        curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        
        curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $fields ) );
        
        curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false);
        
        $result = curl_exec($ch);
        
        curl_close($ch);
        
        return $result;
    }
    
    function error($msg){
        echo "Falha ao enviar:";
        echo "\t" . $msg;
        exit(1);
    }

}