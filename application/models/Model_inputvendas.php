<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class model_inputvendas extends CI_Model {

		public function listar_filiais($usuario = null){

			return $this->db->query('select filial_responsavel filial, concat(usuario,\' Filial: \',filial_responsavel) usuario_, usuario, id_usuario from usuarios inner join grupo_usuarios on id_usuario = fk_usuario where filial_responsavel is not null and ativo = 1 and id_usuario > 1 and fk_grupo = 2 order by usuario')->result();

		}

		public function pegar_email($fk_usuario = null){

			return $this->db->query('select email from usuarios where id_usuario = '.$fk_usuario)->row()->email;

		}

		public function editar_inputvendas($where = null){

			$this->db->query('SET lc_time_names = \'pt_BR\'');


			return array (

				'usuarios' => $this->db->query('select filial_responsavel filial, concat(usuario,\' Filial: \',filial_responsavel) usuario_, usuario, id_usuario from usuarios inner join grupo_usuarios on id_usuario = fk_usuario where filial_responsavel is not null and ativo = 1 and id_usuario > 1 and fk_grupo = 2 order by usuario')->result(),

				'input' => $this->db->query('select *, date_format(data_input, \'%d/%m/%Y\') as data_input from input_vendas where id_input_vendas = '.$where[0])

				);
			

		}

		public function update($dados = null, $id = null){

			$this->db->where('id_input_vendas', $id);
			return $this->db->update('input_vendas',$dados);

		}

		public function geral_inputvendas(){



			$geral = $this->db->query('SELECT 
										\'F1\' as filial,
										sum(pre) as pre,
										sum((controle_facil + controle_giga)) controle,
										sum(recarga) as recarga,
										sum(hc_oficial) as hc_oficial,
										sum(hc_campo) as hc_campo
										FROM input_vendas
										where filial = 1 and month(data_input) = month(current_date) and year(data_input) = year(current_date)
										union 
										SELECT 
										\'F2\' as filial,
										sum(pre) as pre,
										sum((controle_facil + controle_giga)) controle,
										sum(recarga) as recarga,
										sum(hc_oficial) as hc_oficial,
										sum(hc_campo) as hc_campo
										FROM input_vendas
										where filial = 2 and month(data_input) = month(current_date) and year(data_input) = year(current_date)
										union 
										SELECT 
										\'canal\' as filial,
										sum(pre) as pre,
										sum((controle_facil + controle_giga)) controle,
										sum(recarga) as recarga,
										sum(hc_oficial) as hc_oficial,
										sum(hc_campo) as hc_campo
										FROM input_vendas where month(data_input) = month(current_date) and year(data_input) = year(current_date);')->result();

			$geral_controle = $this->db->query('SELECT 
												\'F1\' as filial,
												sum((controle_facil + controle_giga)) controle_total,
												sum(controle_facil) as controle_facil,
												sum(controle_giga) as controle_giga
												FROM input_vendas
												where filial = 1 and month(data_input) = month(current_date) and year(data_input) = year(current_date)
												union 
												SELECT 
												\'F2\' as filial,
												sum((controle_facil + controle_giga)) controle_total,
												sum(controle_facil) as controle_facil,
												sum(controle_giga) as controle_giga
												FROM input_vendas
												where filial = 2 and month(data_input) = month(current_date) and year(data_input) = year(current_date)
												union
												SELECT 
												\'canal\' as filial,
												sum((controle_facil + controle_giga)) controle_total,
												sum(controle_facil) as controle_facil,
												sum(controle_giga) as controle_giga
												FROM input_vendas where month(data_input) = month(current_date) and year(data_input) = year(current_date)')->result();

			$pre_f1 =$this->db->query('select 
										distinct(day(data_input)) as data,
											(select sum(pre) from input_vendas iv1 
											where iv1.data_input = iv2.data_input and filial = 1) pre
											from input_vendas iv2
											where filial = 1 
										    and data_input > 0 
										    and month(data_input) = month(current_date) 
										    and year(data_input) = year(current_date)')->result();

			$pre_f2 =$this->db->query('select 
										distinct(day(data_input)) as data,
											(select sum(pre) from input_vendas iv1 
											where iv1.data_input = iv2.data_input and filial = 2) pre
											from input_vendas iv2
											where filial = 2
										    and data_input > 0 
										    and month(data_input) = month(current_date) 
										    and year(data_input) = year(current_date)')->result();

			$controle_f1 =$this->db->query('select 
											distinct(day(data_input)) as data,
											(select sum(controle_giga + controle_facil) 
										    from input_vendas iv1 
										    where iv1.data_input = iv2.data_input
										    and filial = 1) controle

											from input_vendas iv2
											where filial = 1 
										    and data_input > 0 
										    and month(data_input) = month(current_date) 
										    and year(data_input) = year(current_date)')->result();

			$controle_f2 =$this->db->query('select 
												distinct(day(data_input)) as data,
												(select sum(controle_giga + controle_facil) 
											    from input_vendas iv1 
											    where iv1.data_input = iv2.data_input
											    and filial = 2) controle

												from input_vendas iv2
												where filial = 2 
											    and data_input > 0 
											    and month(data_input) = month(current_date) 
											    and year(data_input) = year(current_date)')->result();

			return array (

					'geral' => $geral, 
					'geral_controle' => $geral_controle, 
					'pre_f1' => $pre_f1, 
					'pre_f2' => $pre_f2, 
					'controle_f1' => $controle_f1, 
					'controle_f2' => $controle_f2

				);


		}

		public function ajax_geral_inputvendas($ano = null, $mes = null,$habitado = null){

			if ($ano == "") {
				$ano = 'year(current_date)';
			}

			if ($mes == "") {
				$mes = 'month(current_date)';
			}

			if ($habitado != "") {
				$habitado = ' and habitado = '.$habitado;
			}

			$geral = $this->db->query('SELECT 
										\'F1\' as filial,
										sum(pre) as pre,
										sum((controle_facil + controle_giga)) controle,
										sum(recarga) as recarga,
										sum(hc_oficial) as hc_oficial,
										sum(hc_campo) as hc_campo
										FROM input_vendas
										where filial = 1 and month(data_input) = '.$mes.' and year(data_input) = '.$ano.' '.$habitado.'
										union 
										SELECT 
										\'F2\' as filial,
										sum(pre) as pre,
										sum((controle_facil + controle_giga)) controle,
										sum(recarga) as recarga,
										sum(hc_oficial) as hc_oficial,
										sum(hc_campo) as hc_campo
										FROM input_vendas
										where filial = 2 and month(data_input) = '.$mes.' and year(data_input) = '.$ano.' '.$habitado.'
										union 
										SELECT 
										\'canal\' as filial,
										sum(pre) as pre,
										sum((controle_facil + controle_giga)) controle,
										sum(recarga) as recarga,
										sum(hc_oficial) as hc_oficial,
										sum(hc_campo) as hc_campo
										FROM input_vendas where month(data_input) = '.$mes.' and year(data_input) = '.$ano.' '.$habitado.';')->result();

			$geral_controle = $this->db->query('SELECT 
												\'F1\' as filial,
												sum((controle_facil + controle_giga)) controle_total,
												sum(controle_facil) as controle_facil,
												sum(controle_giga) as controle_giga
												FROM input_vendas
												where filial = 1 and month(data_input) = '.$mes.' and year(data_input) = '.$ano.' '.$habitado.'
												union 
												SELECT 
												\'F2\' as filial,
												sum((controle_facil + controle_giga)) controle_total,
												sum(controle_facil) as controle_facil,
												sum(controle_giga) as controle_giga
												FROM input_vendas
												where filial = 2 and month(data_input) = '.$mes.' and year(data_input) = '.$ano.' '.$habitado.'
												union
												SELECT 
												\'canal\' as filial,
												sum((controle_facil + controle_giga)) controle_total,
												sum(controle_facil) as controle_facil,
												sum(controle_giga) as controle_giga
												FROM input_vendas where month(data_input) = '.$mes.' and year(data_input) = '.$ano.' '.$habitado.'')->result();

			$pre_f1 =$this->db->query('select 
										distinct(day(data_input)) as data,
											(select sum(pre) from input_vendas iv1 
											where iv1.data_input = iv2.data_input and filial = 1) pre
											from input_vendas iv2
											where filial = 1 
										    and data_input > 0 
										    and month(data_input) = '.$mes.' 
										    and year(data_input) = '.$ano.' '.$habitado.'')->result();

			$pre_f2 =$this->db->query('select 
										distinct(day(data_input)) as data,
											(select sum(pre) from input_vendas iv1 
											where iv1.data_input = iv2.data_input and filial = 2) pre
											from input_vendas iv2
											where filial = 2
										    and data_input > 0 
										    and month(data_input) = '.$mes.' 
										    and year(data_input) = '.$ano.' '.$habitado.'')->result();

			$controle_f1 =$this->db->query('select 
											distinct(day(data_input)) as data,
											(select sum(controle_giga + controle_facil) 
										    from input_vendas iv1 
										    where iv1.data_input = iv2.data_input
										    and filial = 1) controle

											from input_vendas iv2
											where filial = 1 
										    and data_input > 0 
										    and month(data_input) = '.$mes.' 
										    and year(data_input) = '.$ano.' '.$habitado.'')->result();

			$controle_f2 =$this->db->query('select 
												distinct(day(data_input)) as data,
												(select sum(controle_giga + controle_facil) 
											    from input_vendas iv1 
											    where iv1.data_input = iv2.data_input
											    and filial = 2) controle

												from input_vendas iv2
												where filial = 2 
											    and data_input > 0 
											    and month(data_input) = '.$mes.' 
											    and year(data_input) = '.$ano.' '.$habitado.'')->result();

			return array (

					'geral' => $geral, 
					'geral_controle' => $geral_controle, 
					'pre_f1' => $pre_f1, 
					'pre_f2' => $pre_f2, 
					'controle_f1' => $controle_f1, 
					'controle_f2' => $controle_f2

				);


		}

		public function filial1_inputvendas(){

			$usuarios_iv = $this->db->query('select id_usuario, usuario from usuarios inner join grupo_usuarios on id_usuario = fk_usuario where fk_grupo = 2 and ativo = 1 and filial_responsavel = 1
								ORDER BY usuario')->result();

			$detalhado_usuario_pre_f1 = $this->db->query('select 
							distinct(day(data_input)) as data,
						    id_usuario,
							(select sum(pre) from input_vendas iv1 where iv1.data_input = iv2.data_input and id_usuario = fk_usuario and month(data_input) = month(current_date) and year(data_input) = year(current_date)) valor
							from input_vendas iv2
						    inner join usuarios on id_usuario = fk_usuario
							where filial = 1 and data_input > 0 and month(data_input) = month(current_date) and year(data_input) = year(current_date)')->result();

			$detalhado_usuario_total_f1 = $this->db->query('select 
						distinct(day(data_input)) as data,
						id_usuario,
						(select sum(controle_facil + controle_giga) from input_vendas iv1 where iv1.data_input = iv2.data_input and id_usuario = fk_usuario and month(data_input) = month(current_date) and year(data_input) = year(current_date)) valor
						from input_vendas iv2
						inner join usuarios on id_usuario = fk_usuario
						where filial = 1 and data_input > 0 and month(data_input) = month(current_date) and year(data_input) = year(current_date)')->result();

			$detalhado_usuario_facil_f1 = $this->db->query('select 
						distinct(day(data_input)) as data,
						id_usuario,
						(select sum(controle_facil) from input_vendas iv1 where iv1.data_input = iv2.data_input and id_usuario = fk_usuario and month(data_input) = month(current_date) and year(data_input) = year(current_date)) valor
						from input_vendas iv2
						inner join usuarios on id_usuario = fk_usuario
						where filial = 1 and data_input > 0 and month(data_input) = month(current_date) and year(data_input) = year(current_date)')->result();

			$detalhado_usuario_giga_f1 = $this->db->query('select 
						distinct(day(data_input)) as data,
						id_usuario,
						(select sum(controle_giga) from input_vendas iv1 where iv1.data_input = iv2.data_input and id_usuario = fk_usuario and month(data_input) = month(current_date) and year(data_input) = year(current_date)) valor
						from input_vendas iv2
						inner join usuarios on id_usuario = fk_usuario
						where filial = 1 and data_input > 0 and month(data_input) = month(current_date) and year(data_input) = year(current_date)')->result();

			$detalhado_usuario_recarga_f1 = $this->db->query('select 
						distinct(day(data_input)) as data,
						id_usuario,
						(select sum(recarga) from input_vendas iv1 where iv1.data_input = iv2.data_input and id_usuario = fk_usuario and month(data_input) = month(current_date) and year(data_input) = year(current_date)) valor
						from input_vendas iv2
						inner join usuarios on id_usuario = fk_usuario
						where filial = 1 and data_input > 0 and month(data_input) = month(current_date) and year(data_input) = year(current_date)')->result();

			$detalhado_usuario_migracao_f1 = $this->db->query('select 
						distinct(day(data_input)) as data,
						id_usuario,
						(select sum(migracao) from input_vendas iv1 where iv1.data_input = iv2.data_input and id_usuario = fk_usuario and month(data_input) = month(current_date) and year(data_input) = year(current_date)) valor
						from input_vendas iv2
						inner join usuarios on id_usuario = fk_usuario
						where filial = 1 and data_input > 0 and month(data_input) = month(current_date) and year(data_input) = year(current_date)')->result();

			$detalhado_usuario_hc_oficial_f1 = $this->db->query('select 
						distinct(day(data_input)) as data,
						id_usuario,
						(select sum(hc_oficial) from input_vendas iv1 where iv1.data_input = iv2.data_input and id_usuario = fk_usuario and month(data_input) = month(current_date) and year(data_input) = year(current_date)) valor
						from input_vendas iv2
						inner join usuarios on id_usuario = fk_usuario
						where filial = 1 and data_input > 0 and month(data_input) = month(current_date) and year(data_input) = year(current_date)')->result();

			$detalhado_usuario_hc_campo_f1 = $this->db->query('select 
						distinct(day(data_input)) as data,
						id_usuario,
						(select sum(hc_campo) from input_vendas iv1 where iv1.data_input = iv2.data_input and id_usuario = fk_usuario and month(data_input) = month(current_date) and year(data_input) = year(current_date)) valor
						from input_vendas iv2
						inner join usuarios on id_usuario = fk_usuario
						where filial = 1 and data_input > 0 and month(data_input) = month(current_date) and year(data_input) = year(current_date)')->result();

			$geral_usuarios_f1 = $this->db->query('
					SELECT
						distinct
					    (usuario) usuario,
						(select sum(pre) from input_vendas where fk_usuario = iv.fk_usuario and filial = 1 and month(data_input) = month(current_date) and year(data_input) = year(current_date)) as prepago,
					    (select sum((controle_facil + controle_giga)) from input_vendas where fk_usuario = iv.fk_usuario and filial = 1 and month(data_input) = month(current_date) and year(data_input) = year(current_date)) as controle,
					    (select sum(recarga) from input_vendas where fk_usuario = iv.fk_usuario and filial = 1 and month(data_input) = month(current_date) and year(data_input) = year(current_date)) as recarga,
					    (select sum(hc_oficial) from input_vendas where fk_usuario = iv.fk_usuario and filial = 1 and month(data_input) = month(current_date) and year(data_input) = year(current_date)) as hc_oficial,
					    (select sum(hc_campo) from input_vendas where fk_usuario = iv.fk_usuario and filial = 1 and month(data_input) = month(current_date) and year(data_input) = year(current_date)) as hc_campo
						from input_vendas iv
					    inner join usuarios on id_usuario = fk_usuario
						where filial = 1 and month(data_input) = month(current_date) and year(data_input) = year(current_date) and filial_responsavel = 1 ORDER BY usuario')->result();

			$geral_controle_usuario_f1 = $this->db->query('  
				SELECT 
					distinct
					(usuario) usuario,
			        (select sum((controle_facil + controle_giga)) from input_vendas where fk_usuario = iv.fk_usuario and filial = 1 and month(data_input) = month(current_date) and year(data_input) = year(current_date)) as controle_total,
			        (select sum(controle_facil) from input_vendas where fk_usuario = iv.fk_usuario and filial = 1 and month(data_input) = month(current_date) and year(data_input) = year(current_date)) as controle_facil,
			        (select sum(controle_giga) from input_vendas where fk_usuario = iv.fk_usuario and filial = 1 and month(data_input) = month(current_date) and year(data_input) = year(current_date)) as controle_giga
					FROM input_vendas iv
					inner join usuarios on id_usuario = fk_usuario
					where filial = 1 and month(data_input) = month(current_date) and year(data_input) = year(current_date) and filial_responsavel = 1 ORDER BY usuario')->result();

			$perc_facil_f1 = $this->db->query('select 
				distinct(day(data_input)) as data,
				id_usuario,
				(select round(((sum(controle_facil) * 100 ) / (sum(controle_facil) + sum(controle_giga))),0)

				from input_vendas iv1 where iv1.data_input = iv2.data_input and id_usuario = fk_usuario and month(data_input) = month(current_date) and year(data_input) = year(current_date)) valor
				from input_vendas iv2
				inner join usuarios on id_usuario = fk_usuario
				where filial = 1 and data_input > 0 and month(data_input) = month(current_date) and year(data_input) = year(current_date)')->result();

			$perc_giga_f1 = $this->db->query('select 
				distinct(day(data_input)) as data,
				id_usuario,
				(select round(((sum(controle_giga) * 100 ) / (sum(controle_facil) + sum(controle_giga))),0)

				from input_vendas iv1 where iv1.data_input = iv2.data_input and id_usuario = fk_usuario and month(data_input) = month(current_date) and year(data_input) = year(current_date)) valor
				from input_vendas iv2
				inner join usuarios on id_usuario = fk_usuario
				where filial = 1 and data_input > 0 and month(data_input) = month(current_date) and year(data_input) = year(current_date)')->result();

			$perc_campo_f1 = $this->db->query('select 

				distinct(day(data_input)) as data,
				id_usuario,
				(select round(((sum(hc_campo) * 100 ) / sum(hc_oficial)),0)

				from input_vendas iv1 where iv1.data_input = iv2.data_input and id_usuario = fk_usuario and month(data_input) = month(current_date) and year(data_input) = year(current_date)) valor
				from input_vendas iv2
				inner join usuarios on id_usuario = fk_usuario
				where filial = 1 and data_input > 0 and month(data_input) = month(current_date) and year(data_input) = year(current_date)')->result();

			$perc_pre_f1 = $this->db->query('select 

				distinct(day(data_input)) as data,
				id_usuario,
				(select round(((sum(pre) * 100 ) / sum(hc_campo)),0)

				from input_vendas iv1 where iv1.data_input = iv2.data_input and id_usuario = fk_usuario and month(data_input) = month(current_date) and year(data_input) = year(current_date)) valor
				from input_vendas iv2
				inner join usuarios on id_usuario = fk_usuario
				where filial = 1 and data_input > 0 and month(data_input) = month(current_date) and year(data_input) = year(current_date)')->result();

			$perc_controle_f1 = $this->db->query('select 

				distinct(day(data_input)) as data,
				id_usuario,
				(select round((((sum(controle_facil)+ sum(controle_giga)) * 100 ) / sum(hc_campo)),0)

				from input_vendas iv1 where iv1.data_input = iv2.data_input and id_usuario = fk_usuario and month(data_input) = month(current_date) and year(data_input) = year(current_date)) valor
				from input_vendas iv2
				inner join usuarios on id_usuario = fk_usuario
				where filial = 1 and data_input > 0 and month(data_input) = month(current_date) and year(data_input) = year(current_date)')->result();

			$perc_recarga_f1 = $this->db->query('select 

				distinct(day(data_input)) as data,
				id_usuario,
				(select round(((sum(recarga) * 100 ) / sum(hc_campo)),0)

				from input_vendas iv1 where iv1.data_input = iv2.data_input and id_usuario = fk_usuario and month(data_input) = month(current_date) and year(data_input) = year(current_date)) valor
				from input_vendas iv2
				inner join usuarios on id_usuario = fk_usuario
				where filial = 1 and data_input > 0 and month(data_input) = month(current_date) and year(data_input) = year(current_date)')->result();

			return array (

					'detalhado_usuario_pre_f1' => $detalhado_usuario_pre_f1,
					'detalhado_usuario_total_f1' => $detalhado_usuario_total_f1,
					'detalhado_usuario_facil_f1' => $detalhado_usuario_facil_f1,
					'detalhado_usuario_giga_f1' => $detalhado_usuario_giga_f1,
					'detalhado_usuario_recarga_f1' => $detalhado_usuario_recarga_f1,
					'detalhado_usuario_migracao_f1' => $detalhado_usuario_migracao_f1,
					'detalhado_usuario_hc_oficial_f1' => $detalhado_usuario_hc_oficial_f1,
					'detalhado_usuario_hc_campo_f1' => $detalhado_usuario_hc_campo_f1,
					'geral_controle_usuario_f1' => $geral_controle_usuario_f1,
					'geral_usuarios_f1' => $geral_usuarios_f1,
					'usuarios_iv' => $usuarios_iv,
					'perc_facil_f1' => $perc_facil_f1,
					'perc_giga_f1' => $perc_giga_f1,
					'perc_campo_f1' => $perc_campo_f1,
					'perc_pre_f1' => $perc_pre_f1,
					'perc_controle_f1' => $perc_controle_f1,
					'perc_recarga_f1' => $perc_recarga_f1

				);

		}

		public function ajaxF1_1($ano = null, $mes = null,$habitado = null){

			if ($ano == "") {
				$ano = 'year(current_date)';
			}

			if ($mes == "") {
				$mes = 'month(current_date)';
			}

			if ($habitado != "") {
				$habitado = ' and habitado = '.$habitado;
			}

			return array( 'geral_usuarios_f1' => $this->db->query('
					SELECT
						distinct
					    (usuario) usuario,
						(select sum(pre) from input_vendas where fk_usuario = iv.fk_usuario and filial = 1 and month(data_input) = '.$mes.' and year(data_input) = '.$ano.' '.$habitado.') as prepago,
					    (select sum((controle_facil + controle_giga)) from input_vendas where fk_usuario = iv.fk_usuario and filial = 1 and month(data_input) = '.$mes.' and year(data_input) = '.$ano.' '.$habitado.') as controle,
					    (select sum(recarga) from input_vendas where fk_usuario = iv.fk_usuario and filial = 1 and month(data_input) = '.$mes.' and year(data_input) = '.$ano.' '.$habitado.') as recarga,
					    (select sum(hc_oficial) from input_vendas where fk_usuario = iv.fk_usuario and filial = 1 and month(data_input) = '.$mes.' and year(data_input) = '.$ano.' '.$habitado.') as hc_oficial,
					    (select sum(hc_campo) from input_vendas where fk_usuario = iv.fk_usuario and filial = 1 and month(data_input) = '.$mes.' and year(data_input) = '.$ano.' '.$habitado.') as hc_campo
						from input_vendas iv
					    inner join usuarios on id_usuario = fk_usuario
						where filial = 1 and filial_responsavel = 1
						and month(data_input) = '.$mes.' and year(data_input) = '.$ano.' '.$habitado.' ORDER BY usuario')->result());


		}

		public function ajaxF1_2($ano = null, $mes = null,$habitado = null){

			if ($ano == "") {
				$ano = 'year(current_date)';
			}

			if ($mes == "") {
				$mes = 'month(current_date)';
			}

			if ($habitado != "") {
				$habitado = ' and habitado = '.$habitado;
			}

			return array( 'geral_controle_usuario_f1' => $this->db->query('  
				SELECT 
					distinct
					(usuario) usuario,
			        (select sum((controle_facil + controle_giga)) from input_vendas where fk_usuario = iv.fk_usuario and filial = 1 and month(data_input) = '.$mes.' and year(data_input) = '.$ano.' '.$habitado.') as controle_total,
			        (select sum(controle_facil) from input_vendas where fk_usuario = iv.fk_usuario and filial = 1 and month(data_input) = '.$mes.' and year(data_input) = '.$ano.' '.$habitado.') as controle_facil,
			        (select sum(controle_giga) from input_vendas where fk_usuario = iv.fk_usuario and filial = 1 and month(data_input) = '.$mes.' and year(data_input) = '.$ano.' '.$habitado.') as controle_giga
					FROM input_vendas iv
					inner join usuarios on id_usuario = fk_usuario
					where filial = 1 and filial_responsavel = 1 and month(data_input) = '.$mes.' and year(data_input) = '.$ano.' '.$habitado.' ORDER BY usuario')->result());


		}


		public function ajaxF1_3($ano = null, $mes = null,$habitado = null){

			if ($ano == "") {
				$ano = 'year(current_date)';
			}

			if ($mes == "") {
				$mes = 'month(current_date)';
			}

			if ($habitado != "") {
				$habitado = ' and habitado = '.$habitado;
			}

			$usuarios_iv = $this->db->query('select id_usuario, usuario from usuarios inner join grupo_usuarios on id_usuario = fk_usuario where fk_grupo = 2 and ativo = 1 and filial_responsavel = 1
								ORDER BY usuario')->result();

			$detalhado_usuario_pre_f1 = $this->db->query('select 
							distinct(day(data_input)) as data,
						    id_usuario,
							(select sum(pre) from input_vendas iv1 where iv1.data_input = iv2.data_input and id_usuario = fk_usuario and month(data_input) = '.$mes.' and year(data_input) = '.$ano.' '.$habitado.') valor
							from input_vendas iv2
						    inner join usuarios on id_usuario = fk_usuario
							where filial = 1 and data_input > 0 and month(data_input) = '.$mes.' and year(data_input) = '.$ano.' '.$habitado.'')->result();

			$detalhado_usuario_total_f1 = $this->db->query('select 
						distinct(day(data_input)) as data,
						id_usuario,
						(select sum(controle_facil + controle_giga) from input_vendas iv1 where iv1.data_input = iv2.data_input and id_usuario = fk_usuario and month(data_input) = '.$mes.' and year(data_input) = '.$ano.' '.$habitado.') valor
						from input_vendas iv2
						inner join usuarios on id_usuario = fk_usuario
						where filial = 1 and data_input > 0 and month(data_input) = '.$mes.' and year(data_input) = '.$ano.' '.$habitado.'')->result();

			$detalhado_usuario_facil_f1 = $this->db->query('select 
						distinct(day(data_input)) as data,
						id_usuario,
						(select sum(controle_facil) from input_vendas iv1 where iv1.data_input = iv2.data_input and id_usuario = fk_usuario and month(data_input) = '.$mes.' and year(data_input) = '.$ano.' '.$habitado.') valor
						from input_vendas iv2
						inner join usuarios on id_usuario = fk_usuario
						where filial = 1 and data_input > 0 and month(data_input) = '.$mes.' and year(data_input) = '.$ano.' '.$habitado.'')->result();

			$detalhado_usuario_giga_f1 = $this->db->query('select 
						distinct(day(data_input)) as data,
						id_usuario,
						(select sum(controle_giga) from input_vendas iv1 where iv1.data_input = iv2.data_input and id_usuario = fk_usuario and month(data_input) = '.$mes.' and year(data_input) = '.$ano.' '.$habitado.') valor
						from input_vendas iv2
						inner join usuarios on id_usuario = fk_usuario
						where filial = 1 and data_input > 0 and month(data_input) = '.$mes.' and year(data_input) = '.$ano.' '.$habitado.'')->result();

			$detalhado_usuario_recarga_f1 = $this->db->query('select 
						distinct(day(data_input)) as data,
						id_usuario,
						(select sum(recarga) from input_vendas iv1 where iv1.data_input = iv2.data_input and id_usuario = fk_usuario and month(data_input) = '.$mes.' and year(data_input) = '.$ano.' '.$habitado.') valor
						from input_vendas iv2
						inner join usuarios on id_usuario = fk_usuario
						where filial = 1 and data_input > 0 and month(data_input) = '.$mes.' and year(data_input) = '.$ano.' '.$habitado.'')->result();

			$detalhado_usuario_migracao_f1 = $this->db->query('select 
						distinct(day(data_input)) as data,
						id_usuario,
						(select sum(migracao) from input_vendas iv1 where iv1.data_input = iv2.data_input and id_usuario = fk_usuario and month(data_input) = '.$mes.' and year(data_input) = '.$ano.' '.$habitado.') valor
						from input_vendas iv2
						inner join usuarios on id_usuario = fk_usuario
						where filial = 1 and data_input > 0 and month(data_input) = '.$mes.' and year(data_input) = '.$ano.' '.$habitado.'')->result();

			$detalhado_usuario_hc_oficial_f1 = $this->db->query('select 
						distinct(day(data_input)) as data,
						id_usuario,
						(select sum(hc_oficial) from input_vendas iv1 where iv1.data_input = iv2.data_input and id_usuario = fk_usuario and month(data_input) = '.$mes.' and year(data_input) = '.$ano.' '.$habitado.') valor
						from input_vendas iv2
						inner join usuarios on id_usuario = fk_usuario
						where filial = 1 and data_input > 0 and month(data_input) = '.$mes.' and year(data_input) = '.$ano.' '.$habitado.'')->result();

			$detalhado_usuario_hc_campo_f1 = $this->db->query('select 
						distinct(day(data_input)) as data,
						id_usuario,
						(select sum(hc_campo) from input_vendas iv1 where iv1.data_input = iv2.data_input and id_usuario = fk_usuario and month(data_input) = '.$mes.' and year(data_input) = '.$ano.' '.$habitado.') valor
						from input_vendas iv2
						inner join usuarios on id_usuario = fk_usuario
						where filial = 1 and data_input > 0 and month(data_input) = '.$mes.' and year(data_input) = '.$ano.' '.$habitado.'')->result();


			$perc_facil_f1 = $this->db->query('select 
				distinct(day(data_input)) as data,
				id_usuario,
				(select round(((sum(controle_facil) * 100 ) / (sum(controle_facil) + sum(controle_giga))),0)

				from input_vendas iv1 where iv1.data_input = iv2.data_input and id_usuario = fk_usuario and month(data_input) = '.$mes.' and year(data_input) = '.$ano.' '.$habitado.') valor
				from input_vendas iv2
				inner join usuarios on id_usuario = fk_usuario
				where filial = 1 and data_input > 0 and month(data_input) = '.$mes.' and year(data_input) = '.$ano.' '.$habitado.'')->result();

			$perc_giga_f1 = $this->db->query('select 
				distinct(day(data_input)) as data,
				id_usuario,
				(select round(((sum(controle_giga) * 100 ) / (sum(controle_facil) + sum(controle_giga))),0)

				from input_vendas iv1 where iv1.data_input = iv2.data_input and id_usuario = fk_usuario and month(data_input) = '.$mes.' and year(data_input) = '.$ano.' '.$habitado.') valor
				from input_vendas iv2
				inner join usuarios on id_usuario = fk_usuario
				where filial = 1 and data_input > 0 and month(data_input) = '.$mes.' and year(data_input) = '.$ano.' '.$habitado.'')->result();

			$perc_campo_f1 = $this->db->query('select 

				distinct(day(data_input)) as data,
				id_usuario,
				(select round(((sum(hc_campo) * 100 ) / sum(hc_oficial)),0)

				from input_vendas iv1 where iv1.data_input = iv2.data_input and id_usuario = fk_usuario and month(data_input) = '.$mes.' and year(data_input) = '.$ano.' '.$habitado.') valor
				from input_vendas iv2
				inner join usuarios on id_usuario = fk_usuario
				where filial = 1 and data_input > 0 and month(data_input) = '.$mes.' and year(data_input) = '.$ano.' '.$habitado.'')->result();

			$perc_pre_f1 = $this->db->query('select 

				distinct(day(data_input)) as data,
				id_usuario,
				(select round(((sum(pre) * 100 ) / sum(hc_campo)),0)

				from input_vendas iv1 where iv1.data_input = iv2.data_input and id_usuario = fk_usuario and month(data_input) = '.$mes.' and year(data_input) = '.$ano.' '.$habitado.') valor
				from input_vendas iv2
				inner join usuarios on id_usuario = fk_usuario
				where filial = 1 and data_input > 0 and month(data_input) = '.$mes.' and year(data_input) = '.$ano.' '.$habitado.'')->result();

			$perc_controle_f1 = $this->db->query('select 

				distinct(day(data_input)) as data,
				id_usuario,
				(select round((((sum(controle_facil)+ sum(controle_giga)) * 100 ) / sum(hc_campo)),0)

				from input_vendas iv1 where iv1.data_input = iv2.data_input and id_usuario = fk_usuario and month(data_input) = '.$mes.' and year(data_input) = '.$ano.' '.$habitado.') valor
				from input_vendas iv2
				inner join usuarios on id_usuario = fk_usuario
				where filial = 1 and data_input > 0 and month(data_input) = '.$mes.' and year(data_input) = '.$ano.' '.$habitado.'')->result();

			$perc_recarga_f1 = $this->db->query('select 

				distinct(day(data_input)) as data,
				id_usuario,
				(select round(((sum(recarga) * 100 ) / sum(hc_campo)),0)

				from input_vendas iv1 where iv1.data_input = iv2.data_input and id_usuario = fk_usuario and month(data_input) = '.$mes.' and year(data_input) = '.$ano.' '.$habitado.') valor
				from input_vendas iv2
				inner join usuarios on id_usuario = fk_usuario
				where filial = 1 and data_input > 0 and month(data_input) = '.$mes.' and year(data_input) = '.$ano.' '.$habitado.'')->result();


			return array (

					'detalhado_usuario_pre_f1' => $detalhado_usuario_pre_f1,
					'detalhado_usuario_total_f1' => $detalhado_usuario_total_f1,
					'detalhado_usuario_facil_f1' => $detalhado_usuario_facil_f1,
					'detalhado_usuario_giga_f1' => $detalhado_usuario_giga_f1,
					'detalhado_usuario_recarga_f1' => $detalhado_usuario_recarga_f1,
					'detalhado_usuario_migracao_f1' => $detalhado_usuario_migracao_f1,
					'detalhado_usuario_hc_oficial_f1' => $detalhado_usuario_hc_oficial_f1,
					'detalhado_usuario_hc_campo_f1' => $detalhado_usuario_hc_campo_f1,
					'usuarios_iv' => $usuarios_iv,
					'perc_facil_f1' => $perc_facil_f1,
					'perc_giga_f1' => $perc_giga_f1,
					'perc_campo_f1' => $perc_campo_f1,
					'perc_pre_f1' => $perc_pre_f1,
					'perc_controle_f1' => $perc_controle_f1,
					'perc_recarga_f1' => $perc_recarga_f1

				);

		}

		//FILIAL 2

		public function filial2_inputvendas(){

			$usuarios_iv = $this->db->query('select id_usuario, usuario from usuarios inner join grupo_usuarios on id_usuario = fk_usuario where fk_grupo = 2 and ativo = 1 and filial_responsavel = 2
								ORDER BY usuario')->result();

			$detalhado_usuario_pre_f2 = $this->db->query('select 
							distinct(day(data_input)) as data,
						    id_usuario,
							(select sum(pre) from input_vendas iv1 where iv1.data_input = iv2.data_input and id_usuario = fk_usuario and month(data_input) = month(current_date) and year(data_input) = year(current_date)) valor
							from input_vendas iv2
						    inner join usuarios on id_usuario = fk_usuario
							where filial = 2 and data_input > 0 and month(data_input) = month(current_date) and year(data_input) = year(current_date)')->result();

			$detalhado_usuario_total_f2 = $this->db->query('select 
						distinct(day(data_input)) as data,
						id_usuario,
						(select sum(controle_facil + controle_giga) from input_vendas iv1 where iv1.data_input = iv2.data_input and id_usuario = fk_usuario and month(data_input) = month(current_date) and year(data_input) = year(current_date)) valor
						from input_vendas iv2
						inner join usuarios on id_usuario = fk_usuario
						where filial = 2 and data_input > 0 and month(data_input) = month(current_date) and year(data_input) = year(current_date)')->result();

			$detalhado_usuario_facil_f2 = $this->db->query('select 
						distinct(day(data_input)) as data,
						id_usuario,
						(select sum(controle_facil) from input_vendas iv1 where iv1.data_input = iv2.data_input and id_usuario = fk_usuario and month(data_input) = month(current_date) and year(data_input) = year(current_date)) valor
						from input_vendas iv2
						inner join usuarios on id_usuario = fk_usuario
						where filial = 2 and data_input > 0 and month(data_input) = month(current_date) and year(data_input) = year(current_date)')->result();

			$detalhado_usuario_giga_f2 = $this->db->query('select 
						distinct(day(data_input)) as data,
						id_usuario,
						(select sum(controle_giga) from input_vendas iv1 where iv1.data_input = iv2.data_input and id_usuario = fk_usuario and month(data_input) = month(current_date) and year(data_input) = year(current_date)) valor
						from input_vendas iv2
						inner join usuarios on id_usuario = fk_usuario
						where filial = 2 and data_input > 0 and month(data_input) = month(current_date) and year(data_input) = year(current_date)')->result();

			$detalhado_usuario_recarga_f2 = $this->db->query('select 
						distinct(day(data_input)) as data,
						id_usuario,
						(select sum(recarga) from input_vendas iv1 where iv1.data_input = iv2.data_input and id_usuario = fk_usuario and month(data_input) = month(current_date) and year(data_input) = year(current_date)) valor
						from input_vendas iv2
						inner join usuarios on id_usuario = fk_usuario
						where filial = 2 and data_input > 0 and month(data_input) = month(current_date) and year(data_input) = year(current_date)')->result();

			$detalhado_usuario_migracao_f2 = $this->db->query('select 
						distinct(day(data_input)) as data,
						id_usuario,
						(select sum(migracao) from input_vendas iv1 where iv1.data_input = iv2.data_input and id_usuario = fk_usuario and month(data_input) = month(current_date) and year(data_input) = year(current_date)) valor
						from input_vendas iv2
						inner join usuarios on id_usuario = fk_usuario
						where filial = 2 and data_input > 0 and month(data_input) = month(current_date) and year(data_input) = year(current_date)')->result();

			$detalhado_usuario_hc_oficial_f2 = $this->db->query('select 
						distinct(day(data_input)) as data,
						id_usuario,
						(select sum(hc_oficial) from input_vendas iv1 where iv1.data_input = iv2.data_input and id_usuario = fk_usuario and month(data_input) = month(current_date) and year(data_input) = year(current_date)) valor
						from input_vendas iv2
						inner join usuarios on id_usuario = fk_usuario
						where filial = 2 and data_input > 0 and month(data_input) = month(current_date) and year(data_input) = year(current_date)')->result();

			$detalhado_usuario_hc_campo_f2 = $this->db->query('select 
						distinct(day(data_input)) as data,
						id_usuario,
						(select sum(hc_campo) from input_vendas iv1 where iv1.data_input = iv2.data_input and id_usuario = fk_usuario and month(data_input) = month(current_date) and year(data_input) = year(current_date)) valor
						from input_vendas iv2
						inner join usuarios on id_usuario = fk_usuario
						where filial = 2 and data_input > 0 and month(data_input) = month(current_date) and year(data_input) = year(current_date)')->result();

			$geral_usuarios_f2 = $this->db->query('
					SELECT
						distinct
					    (usuario) usuario,
						(select sum(pre) from input_vendas where fk_usuario = iv.fk_usuario and filial = 2 and month(data_input) = month(current_date) and year(data_input) = year(current_date)) as prepago,
					    (select sum((controle_facil + controle_giga)) from input_vendas where fk_usuario = iv.fk_usuario and filial = 2 and month(data_input) = month(current_date) and year(data_input) = year(current_date)) as controle,
					    (select sum(recarga) from input_vendas where fk_usuario = iv.fk_usuario and filial = 2 and month(data_input) = month(current_date) and year(data_input) = year(current_date)) as recarga,
					    (select sum(hc_oficial) from input_vendas where fk_usuario = iv.fk_usuario and filial = 2 and month(data_input) = month(current_date) and year(data_input) = year(current_date)) as hc_oficial,
					    (select sum(hc_campo) from input_vendas where fk_usuario = iv.fk_usuario and filial = 2 and month(data_input) = month(current_date) and year(data_input) = year(current_date)) as hc_campo
						from input_vendas iv
					    inner join usuarios on id_usuario = fk_usuario
						where filial = 2 and month(data_input) = month(current_date) and year(data_input) = year(current_date) and filial_responsavel = 2 ORDER BY usuario')->result();

			$geral_controle_usuario_f2 = $this->db->query('  
				SELECT 
					distinct
					(usuario) usuario,
			        (select sum((controle_facil + controle_giga)) from input_vendas where fk_usuario = iv.fk_usuario and filial = 2 and month(data_input) = month(current_date) and year(data_input) = year(current_date)) as controle_total,
			        (select sum(controle_facil) from input_vendas where fk_usuario = iv.fk_usuario and filial = 2 and month(data_input) = month(current_date) and year(data_input) = year(current_date)) as controle_facil,
			        (select sum(controle_giga) from input_vendas where fk_usuario = iv.fk_usuario and filial = 2 and month(data_input) = month(current_date) and year(data_input) = year(current_date)) as controle_giga
					FROM input_vendas iv
					inner join usuarios on id_usuario = fk_usuario
					where filial = 2 and month(data_input) = month(current_date) and year(data_input) = year(current_date) and filial_responsavel = 2 ORDER BY usuario')->result();

			$perc_facil_f2 = $this->db->query('select 
				distinct(day(data_input)) as data,
				id_usuario,
				(select round(((sum(controle_facil) * 100 ) / (sum(controle_facil) + sum(controle_giga))),0)

				from input_vendas iv1 where iv1.data_input = iv2.data_input and id_usuario = fk_usuario and month(data_input) = month(current_date) and year(data_input) = year(current_date)) valor
				from input_vendas iv2
				inner join usuarios on id_usuario = fk_usuario
				where filial = 2 and data_input > 0 and month(data_input) = month(current_date) and year(data_input) = year(current_date)')->result();

			$perc_giga_f2 = $this->db->query('select 
				distinct(day(data_input)) as data,
				id_usuario,
				(select round(((sum(controle_giga) * 100 ) / (sum(controle_facil) + sum(controle_giga))),0)

				from input_vendas iv1 where iv1.data_input = iv2.data_input and id_usuario = fk_usuario and month(data_input) = month(current_date) and year(data_input) = year(current_date)) valor
				from input_vendas iv2
				inner join usuarios on id_usuario = fk_usuario
				where filial = 2 and data_input > 0 and month(data_input) = month(current_date) and year(data_input) = year(current_date)')->result();

			$perc_campo_f2 = $this->db->query('select 

				distinct(day(data_input)) as data,
				id_usuario,
				(select round(((sum(hc_campo) * 100 ) / sum(hc_oficial)),0)

				from input_vendas iv1 where iv1.data_input = iv2.data_input and id_usuario = fk_usuario and month(data_input) = month(current_date) and year(data_input) = year(current_date)) valor
				from input_vendas iv2
				inner join usuarios on id_usuario = fk_usuario
				where filial = 2 and data_input > 0 and month(data_input) = month(current_date) and year(data_input) = year(current_date)')->result();

			$perc_pre_f2 = $this->db->query('select 

				distinct(day(data_input)) as data,
				id_usuario,
				(select round(((sum(pre) * 100 ) / sum(hc_campo)),0)

				from input_vendas iv1 where iv1.data_input = iv2.data_input and id_usuario = fk_usuario and month(data_input) = month(current_date) and year(data_input) = year(current_date)) valor
				from input_vendas iv2
				inner join usuarios on id_usuario = fk_usuario
				where filial = 2 and data_input > 0 and month(data_input) = month(current_date) and year(data_input) = year(current_date)')->result();

			$perc_controle_f2 = $this->db->query('select 

				distinct(day(data_input)) as data,
				id_usuario,
				(select round((((sum(controle_facil)+ sum(controle_giga)) * 100 ) / sum(hc_campo)),0)

				from input_vendas iv1 where iv1.data_input = iv2.data_input and id_usuario = fk_usuario and month(data_input) = month(current_date) and year(data_input) = year(current_date)) valor
				from input_vendas iv2
				inner join usuarios on id_usuario = fk_usuario
				where filial = 2 and data_input > 0 and month(data_input) = month(current_date) and year(data_input) = year(current_date)')->result();

			$perc_recarga_f2 = $this->db->query('select 

				distinct(day(data_input)) as data,
				id_usuario,
				(select round(((sum(recarga) * 100 ) / sum(hc_campo)),0)

				from input_vendas iv1 where iv1.data_input = iv2.data_input and id_usuario = fk_usuario and month(data_input) = month(current_date) and year(data_input) = year(current_date)) valor
				from input_vendas iv2
				inner join usuarios on id_usuario = fk_usuario
				where filial = 2 and data_input > 0 and month(data_input) = month(current_date) and year(data_input) = year(current_date)')->result();

			return array (

					'detalhado_usuario_pre_f2' => $detalhado_usuario_pre_f2,
					'detalhado_usuario_total_f2' => $detalhado_usuario_total_f2,
					'detalhado_usuario_facil_f2' => $detalhado_usuario_facil_f2,
					'detalhado_usuario_giga_f2' => $detalhado_usuario_giga_f2,
					'detalhado_usuario_recarga_f2' => $detalhado_usuario_recarga_f2,
					'detalhado_usuario_migracao_f2' => $detalhado_usuario_migracao_f2,
					'detalhado_usuario_hc_oficial_f2' => $detalhado_usuario_hc_oficial_f2,
					'detalhado_usuario_hc_campo_f2' => $detalhado_usuario_hc_campo_f2,
					'geral_controle_usuario_f2' => $geral_controle_usuario_f2,
					'geral_usuarios_f2' => $geral_usuarios_f2,
					'usuarios_iv' => $usuarios_iv,
					'perc_facil_f2' => $perc_facil_f2,
					'perc_giga_f2' => $perc_giga_f2,
					'perc_campo_f2' => $perc_campo_f2,
					'perc_pre_f2' => $perc_pre_f2,
					'perc_controle_f2' => $perc_controle_f2,
					'perc_recarga_f2' => $perc_recarga_f2

				);

		}

		public function ajaxF2_1($ano = null, $mes = null,$habitado = null){

			if ($ano == "") {
				$ano = 'year(current_date)';
			}

			if ($mes == "") {
				$mes = 'month(current_date)';
			}

			if ($habitado != "") {
				$habitado = ' and habitado = '.$habitado;
			}

			return array( 'geral_usuarios_f2' => $this->db->query('
					SELECT
						distinct
					    (usuario) usuario,
						(select sum(pre) from input_vendas where fk_usuario = iv.fk_usuario and filial = 2 and month(data_input) = '.$mes.' and year(data_input) = '.$ano.' '.$habitado.') as prepago,
					    (select sum((controle_facil + controle_giga)) from input_vendas where fk_usuario = iv.fk_usuario and filial = 2 and month(data_input) = '.$mes.' and year(data_input) = '.$ano.' '.$habitado.') as controle,
					    (select sum(recarga) from input_vendas where fk_usuario = iv.fk_usuario and filial = 2 and month(data_input) = '.$mes.' and year(data_input) = '.$ano.' '.$habitado.') as recarga,
					    (select sum(hc_oficial) from input_vendas where fk_usuario = iv.fk_usuario and filial = 2 and month(data_input) = '.$mes.' and year(data_input) = '.$ano.' '.$habitado.') as hc_oficial,
					    (select sum(hc_campo) from input_vendas where fk_usuario = iv.fk_usuario and filial = 2 and month(data_input) = '.$mes.' and year(data_input) = '.$ano.' '.$habitado.') as hc_campo
						from input_vendas iv
					    inner join usuarios on id_usuario = fk_usuario
						where filial = 2 and filial_responsavel = 2
						and month(data_input) = '.$mes.' and year(data_input) = '.$ano.' '.$habitado.' ORDER BY usuario')->result());


		}

		public function ajaxF2_2($ano = null, $mes = null,$habitado = null){

			if ($ano == "") {
				$ano = 'year(current_date)';
			}

			if ($mes == "") {
				$mes = 'month(current_date)';
			}

			if ($habitado != "") {
				$habitado = ' and habitado = '.$habitado;
			}

			return array( 'geral_controle_usuario_f2' => $this->db->query('  
				SELECT 
					distinct
					(usuario) usuario,
			        (select sum((controle_facil + controle_giga)) from input_vendas where fk_usuario = iv.fk_usuario and filial = 2 and month(data_input) = '.$mes.' and year(data_input) = '.$ano.' '.$habitado.') as controle_total,
			        (select sum(controle_facil) from input_vendas where fk_usuario = iv.fk_usuario and filial = 2 and month(data_input) = '.$mes.' and year(data_input) = '.$ano.' '.$habitado.') as controle_facil,
			        (select sum(controle_giga) from input_vendas where fk_usuario = iv.fk_usuario and filial = 2 and month(data_input) = '.$mes.' and year(data_input) = '.$ano.' '.$habitado.') as controle_giga
					FROM input_vendas iv
					inner join usuarios on id_usuario = fk_usuario
					where filial = 2 and filial_responsavel = 2 and month(data_input) = '.$mes.' and year(data_input) = '.$ano.' '.$habitado.' ORDER BY usuario')->result());


		}


		public function ajaxF2_3($ano = null, $mes = null,$habitado = null){

			if ($ano == "") {
				$ano = 'year(current_date)';
			}

			if ($mes == "") {
				$mes = 'month(current_date)';
			}

			if ($habitado != "") {
				$habitado = ' and habitado = '.$habitado;
			}

			$usuarios_iv = $this->db->query('select id_usuario, usuario from usuarios inner join grupo_usuarios on id_usuario = fk_usuario where fk_grupo = 2 and ativo = 1 and filial_responsavel = 2
								ORDER BY usuario')->result();

			$detalhado_usuario_pre_f2 = $this->db->query('select 
							distinct(day(data_input)) as data,
						    id_usuario,
							(select sum(pre) from input_vendas iv1 where iv1.data_input = iv2.data_input and id_usuario = fk_usuario and month(data_input) = '.$mes.' and year(data_input) = '.$ano.' '.$habitado.') valor
							from input_vendas iv2
						    inner join usuarios on id_usuario = fk_usuario
							where filial = 2 and data_input > 0 and month(data_input) = '.$mes.' and year(data_input) = '.$ano.' '.$habitado.'')->result();

			$detalhado_usuario_total_f2 = $this->db->query('select 
						distinct(day(data_input)) as data,
						id_usuario,
						(select sum(controle_facil + controle_giga) from input_vendas iv1 where iv1.data_input = iv2.data_input and id_usuario = fk_usuario and month(data_input) = '.$mes.' and year(data_input) = '.$ano.' '.$habitado.') valor
						from input_vendas iv2
						inner join usuarios on id_usuario = fk_usuario
						where filial = 2 and data_input > 0 and month(data_input) = '.$mes.' and year(data_input) = '.$ano.' '.$habitado.'')->result();

			$detalhado_usuario_facil_f2 = $this->db->query('select 
						distinct(day(data_input)) as data,
						id_usuario,
						(select sum(controle_facil) from input_vendas iv1 where iv1.data_input = iv2.data_input and id_usuario = fk_usuario and month(data_input) = '.$mes.' and year(data_input) = '.$ano.' '.$habitado.') valor
						from input_vendas iv2
						inner join usuarios on id_usuario = fk_usuario
						where filial = 2 and data_input > 0 and month(data_input) = '.$mes.' and year(data_input) = '.$ano.' '.$habitado.'')->result();

			$detalhado_usuario_giga_f2 = $this->db->query('select 
						distinct(day(data_input)) as data,
						id_usuario,
						(select sum(controle_giga) from input_vendas iv1 where iv1.data_input = iv2.data_input and id_usuario = fk_usuario and month(data_input) = '.$mes.' and year(data_input) = '.$ano.' '.$habitado.') valor
						from input_vendas iv2
						inner join usuarios on id_usuario = fk_usuario
						where filial = 2 and data_input > 0 and month(data_input) = '.$mes.' and year(data_input) = '.$ano.' '.$habitado.'')->result();

			$detalhado_usuario_recarga_f2 = $this->db->query('select 
						distinct(day(data_input)) as data,
						id_usuario,
						(select sum(recarga) from input_vendas iv1 where iv1.data_input = iv2.data_input and id_usuario = fk_usuario and month(data_input) = '.$mes.' and year(data_input) = '.$ano.' '.$habitado.') valor
						from input_vendas iv2
						inner join usuarios on id_usuario = fk_usuario
						where filial = 2 and data_input > 0 and month(data_input) = '.$mes.' and year(data_input) = '.$ano.' '.$habitado.'')->result();

			$detalhado_usuario_migracao_f2 = $this->db->query('select 
						distinct(day(data_input)) as data,
						id_usuario,
						(select sum(migracao) from input_vendas iv1 where iv1.data_input = iv2.data_input and id_usuario = fk_usuario and month(data_input) = '.$mes.' and year(data_input) = '.$ano.' '.$habitado.') valor
						from input_vendas iv2
						inner join usuarios on id_usuario = fk_usuario
						where filial = 2 and data_input > 0 and month(data_input) = '.$mes.' and year(data_input) = '.$ano.' '.$habitado.'')->result();

			$detalhado_usuario_hc_oficial_f2 = $this->db->query('select 
						distinct(day(data_input)) as data,
						id_usuario,
						(select sum(hc_oficial) from input_vendas iv1 where iv1.data_input = iv2.data_input and id_usuario = fk_usuario and month(data_input) = '.$mes.' and year(data_input) = '.$ano.' '.$habitado.') valor
						from input_vendas iv2
						inner join usuarios on id_usuario = fk_usuario
						where filial = 2 and data_input > 0 and month(data_input) = '.$mes.' and year(data_input) = '.$ano.' '.$habitado.'')->result();

			$detalhado_usuario_hc_campo_f2 = $this->db->query('select 
						distinct(day(data_input)) as data,
						id_usuario,
						(select sum(hc_campo) from input_vendas iv1 where iv1.data_input = iv2.data_input and id_usuario = fk_usuario and month(data_input) = '.$mes.' and year(data_input) = '.$ano.' '.$habitado.') valor
						from input_vendas iv2
						inner join usuarios on id_usuario = fk_usuario
						where filial = 2 and data_input > 0 and month(data_input) = '.$mes.' and year(data_input) = '.$ano.' '.$habitado.'')->result();


			$perc_facil_f2 = $this->db->query('select 
				distinct(day(data_input)) as data,
				id_usuario,
				(select round(((sum(controle_facil) * 100 ) / (sum(controle_facil) + sum(controle_giga))),0)

				from input_vendas iv1 where iv1.data_input = iv2.data_input and id_usuario = fk_usuario and month(data_input) = '.$mes.' and year(data_input) = '.$ano.' '.$habitado.') valor
				from input_vendas iv2
				inner join usuarios on id_usuario = fk_usuario
				where filial = 2 and data_input > 0 and month(data_input) = '.$mes.' and year(data_input) = '.$ano.' '.$habitado.'')->result();

			$perc_giga_f2 = $this->db->query('select 
				distinct(day(data_input)) as data,
				id_usuario,
				(select round(((sum(controle_giga) * 100 ) / (sum(controle_facil) + sum(controle_giga))),0)

				from input_vendas iv1 where iv1.data_input = iv2.data_input and id_usuario = fk_usuario and month(data_input) = '.$mes.' and year(data_input) = '.$ano.' '.$habitado.') valor
				from input_vendas iv2
				inner join usuarios on id_usuario = fk_usuario
				where filial = 2 and data_input > 0 and month(data_input) = '.$mes.' and year(data_input) = '.$ano.' '.$habitado.'')->result();

			$perc_campo_f2 = $this->db->query('select 

				distinct(day(data_input)) as data,
				id_usuario,
				(select round(((sum(hc_campo) * 100 ) / sum(hc_oficial)),0)

				from input_vendas iv1 where iv1.data_input = iv2.data_input and id_usuario = fk_usuario and month(data_input) = '.$mes.' and year(data_input) = '.$ano.' '.$habitado.') valor
				from input_vendas iv2
				inner join usuarios on id_usuario = fk_usuario
				where filial = 2 and data_input > 0 and month(data_input) = '.$mes.' and year(data_input) = '.$ano.' '.$habitado.'')->result();

			$perc_pre_f2 = $this->db->query('select 

				distinct(day(data_input)) as data,
				id_usuario,
				(select round(((sum(pre) * 100 ) / sum(hc_campo)),0)

				from input_vendas iv1 where iv1.data_input = iv2.data_input and id_usuario = fk_usuario and month(data_input) = '.$mes.' and year(data_input) = '.$ano.' '.$habitado.') valor
				from input_vendas iv2
				inner join usuarios on id_usuario = fk_usuario
				where filial = 2 and data_input > 0 and month(data_input) = '.$mes.' and year(data_input) = '.$ano.' '.$habitado.'')->result();

			$perc_controle_f2 = $this->db->query('select 

				distinct(day(data_input)) as data,
				id_usuario,
				(select round((((sum(controle_facil)+ sum(controle_giga)) * 100 ) / sum(hc_campo)),0)

				from input_vendas iv1 where iv1.data_input = iv2.data_input and id_usuario = fk_usuario and month(data_input) = '.$mes.' and year(data_input) = '.$ano.' '.$habitado.') valor
				from input_vendas iv2
				inner join usuarios on id_usuario = fk_usuario
				where filial = 2 and data_input > 0 and month(data_input) = '.$mes.' and year(data_input) = '.$ano.' '.$habitado.'')->result();

			$perc_recarga_f2 = $this->db->query('select 

				distinct(day(data_input)) as data,
				id_usuario,
				(select round(((sum(recarga) * 100 ) / sum(hc_campo)),0)

				from input_vendas iv1 where iv1.data_input = iv2.data_input and id_usuario = fk_usuario and month(data_input) = '.$mes.' and year(data_input) = '.$ano.' '.$habitado.') valor
				from input_vendas iv2
				inner join usuarios on id_usuario = fk_usuario
				where filial = 2 and data_input > 0 and month(data_input) = '.$mes.' and year(data_input) = '.$ano.' '.$habitado.'')->result();


			return array (

					'detalhado_usuario_pre_f2' => $detalhado_usuario_pre_f2,
					'detalhado_usuario_total_f2' => $detalhado_usuario_total_f2,
					'detalhado_usuario_facil_f2' => $detalhado_usuario_facil_f2,
					'detalhado_usuario_giga_f2' => $detalhado_usuario_giga_f2,
					'detalhado_usuario_recarga_f2' => $detalhado_usuario_recarga_f2,
					'detalhado_usuario_migracao_f2' => $detalhado_usuario_migracao_f2,
					'detalhado_usuario_hc_oficial_f2' => $detalhado_usuario_hc_oficial_f2,
					'detalhado_usuario_hc_campo_f2' => $detalhado_usuario_hc_campo_f2,
					'usuarios_iv' => $usuarios_iv,
					'perc_facil_f2' => $perc_facil_f2,
					'perc_giga_f2' => $perc_giga_f2,
					'perc_campo_f2' => $perc_campo_f2,
					'perc_pre_f2' => $perc_pre_f2,
					'perc_controle_f2' => $perc_controle_f2,
					'perc_recarga_f2' => $perc_recarga_f2

				);

		}

		public function graficosControles($ano = null, $mes = null,$habitado = null,$filial = null){

			if ($ano == "") {
				$ano = 'year(current_date)';
			}

			if ($mes == "") {
				$mes = 'month(current_date)';
			}

			if ($habitado != "") {
				$habitado = ' and habitado = '.$habitado;
			}

			return $this->db->query('select 
									(select sum(controle_facil) from input_vendas where filial = '.$filial.' and month(data_input) = '.$mes.' and year(data_input) = '.$ano.' '.$habitado.') facil,
								    (select sum(controle_giga) from input_vendas where filial = '.$filial.' and month(data_input) = '.$mes.' and year(data_input) = '.$ano.' '.$habitado.')boleto')->row();
		}

		public function historico_inputvendas() {

			$this->db->query('SET lc_time_names = \'pt_BR\'');

			try {

				return array ('input_vendas' => $this->db->query('select iv.*, date_format(data_input, \'%d/%m/%Y\') data_input, date_format(data_envio, \'%d/%m/%Y %H:%i\') data_envio, usuario from input_vendas iv left join usuarios on id_usuario = fk_usuario')->result(),
					'usuarios' => $this->db->get('usuarios')->result());
				
			} catch (Exception $e) {

				echo 'Falha ao listar os inputs: '.$e;
				
			}


		}

		public function filtrarPor($usuario = null, $de = null, $ate = null) {

			$sql = 'select iv.*, date_format(data_input, \'%d/%m/%Y\') data_input, date_format(data_envio, \'%d/%m/%Y %H:%i\') data_envio, usuario from input_vendas iv left join usuarios on id_usuario = fk_usuario';

			$where = "where data_envio > 0";

			if($usuario != '') {
				$where .= ' and fk_usuario = '.$usuario;
			}

			if($de != '') {
				$data = explode('/', $de);
				$dia = $data [0];
				$mes = $data [1];
				$ano = $data [2];
				$de = $ano.'-'.$mes.'-'.$dia.' 00:00:00';
				$where .= ' and data_envio > \''.$de.'\'';
			}

			if($ate != '') {
				$data = explode('/', $ate);
				$dia = $data [0];
				$mes = $data [1];
				$ano = $data [2];
				$ate = $ano.'-'.$mes.'-'.$dia.' 23:59:59';
				$where .= ' and data_envio < \''.$ate.'\'';
			}

			//echo $sql.' '.$where;

			return $this->db->query($sql.' '.$where)->result();

		}

		public function insert($dados = null) {

			//$dados['fk_usuario'] = $this->db->query("select id_usuario as id from usuarios where usuario = '".$usuario."'")->row()->id;

			return $this->db->insert('input_vendas',$dados);

		}

		//(Atualização 02/10/2017)
		public function editarInput($id = null, $data = null) {

			// echo "SELECT 
			// 							id_input_vendas,
			// 							pre,
			// 						    controle_facil,
			// 						    controle_giga,
			// 						    recarga,
			// 						    migracao
			// 								FROM 
			// 									input_vendas
			// 								WHERE 
			// 									date(data_input) = '{$this->fDatas($data)}'
			// 								AND 
			// 									fk_usuario = {$id}
			// 								AND habitado = 1
			// 								limit 1;";
			// 								die();

			return array('habitado' =>$this->db->query("SELECT 
										id_input_vendas,
										pre,
									    controle_facil,
									    controle_giga,
									    recarga,
									    migracao
											FROM 
												input_vendas
											WHERE 
												date(data_input) = '{$this->fDatas($data)}'
											AND 
												fk_usuario = {$id}
											AND habitado = 1
											limit 1;")->row(),

						'desabitado' =>$this->db->query("SELECT 
										id_input_vendas,
										pre,
									    controle_facil,
									    controle_giga,
									    recarga,
									    migracao
											FROM 
												input_vendas
											WHERE 
												date(data_input) = '{$this->fDatas($data)}'
											AND 
												fk_usuario = {$id}
											AND habitado = 0
											limit 1;")->row(),

						'cabecalho' =>$this->db->query("SELECT 
									    id_input_vendas,
									    aspectos,
									    data_input,
									    hc_oficial,
									    hc_campo,
									    fk_usuario
											FROM 
												input_vendas
											WHERE 
												date(data_input) = '{$this->fDatas($data)}'
											AND 
												fk_usuario = {$id}
											limit 1;")->row(),

						'filial' => $this->db->query('select filial_responsavel filial, concat(usuario,\' Filial: \',filial_responsavel) usuario_, usuario, id_usuario from usuarios inner join grupo_usuarios on id_usuario = fk_usuario where filial_responsavel is not null and ativo = 1 and id_usuario > 1 and fk_grupo = 2 order by usuario')->result()
			);

		}

		//Atualizar Persistencia
		public function persistenciaEditar($valores = null){

			$this->db->where('id_input_vendas',$valores['id_input_vendas']);
			$this->db->update('input_vendas',$valores);


		}

		//(Atualização 02/10/2017)

		################################# FORMATAR DATAS #################################

		public function fDatas($data = null){
			if($data != ""){
				$data2 = explode('-', $data);
				$dia = $data2 [0];
				$mes = $data2 [1];
				$ano = $data2 [2];
				$nova = $ano.'-'.$mes.'-'.$dia;
				return $nova;
			} else {
				return "";
			}
			
		}

	}