<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class model_roteiro extends CI_Model {

		public function lista_roteiro(){

			$this->db->query('SET lc_time_names = \'pt_BR\'');

			return $this->db->query('Select 
										id_roteiro as id,
										nome,
										date_format(inicio_roteiro, \'%d de %M de %Y\') as inicio, 
										date_format(fim_roteiro, \'%d de %M de %Y\') as fim, 
										date_format(data_solicitacao, \'%d de %M de %Y\') as solicitacao,
										 (CASE status
										      WHEN 2 THEN \'Não Aprovado\'
										      WHEN 1 THEN \'Aprovado\'
										      WHEN 0 THEN \'Aguardando\'
										      end) as status,
										km_total km,
										obs_roteiro as obs
										from roteiro 
										left join usuarios on id_usuario = fk_usuario')->result();

		}

		public function aprovacao_roteiro($where = null){

			$this->db->query('SET lc_time_names = \'pt_BR\'');

			$this->db->select('date_format(data_solicitacao, \'%d de %M de %Y\') as data_solicitacao,
				date_format(inicio_roteiro, \'%d de %M de %Y\') as inicio_roteiro,
				date_format(fim_roteiro, \'%d de %M de %Y\') as fim_roteiro,
				date_format(data_aprovacao, \'%d de %M de %Y\') as data_aprovacao,
				(select sum(tempo_google) from roteiro_enderecos where fk_roteiro = '.$where[0].') totalPercurso,
				(select sum(tempo_adicional) from roteiro_enderecos where fk_roteiro = '.$where[0].') totalPdv,
				obs_roteiro,
				nome,
				fk_usuario,
				km_total,
				horas_total,
				id_roteiro,
				status');
			$this->db->join('usuarios','id_usuario = fk_usuario');
			$roteiro = $this->db->get_where('roteiro', array('id_roteiro' => $where[0]))->row();

			return array ('enderecos' => $this->db->query('select endereco_google from roteiro_enderecos 
									inner join roteiro 
									on id_roteiro = fk_roteiro
									where id_roteiro = '.$roteiro->id_roteiro.';')->result(),

							'roteiro' => $roteiro);

		}

		public function aprovar($roteiro = null, $status = null){

			return $this->db->query('update roteiro set status = '.$status.', data_aprovacao = curdate() where id_roteiro = '.$roteiro);

		}


	}
