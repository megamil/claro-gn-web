<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class model_usuarios extends CI_Model {

		public function listar_usuarios() {

			try {

				return $this->db->get('usuarios')->result();

			} catch (Exception $e) {

				echo 'Falha ao listar usuários: '.$e;

			}

			

		}

		public function criar_usuarios($dados = null) {

			try {

				$this->db->insert('usuarios',$dados);
				return $this->db->insert_id(); //Retorna o id do novo usuário.

			} catch (Exception $e) {

				echo 'Falha ao criar usuário: '.$e;

			}

		}

		public function update_usuarios($dados = null) {

			try {
				
				$this->db->where('id_usuario', $dados['id_usuario']);
				return $this->db->update('usuarios',$dados);

			} catch (Exception $e) {

				echo 'Falha ao editar usuário: '.$e;
				
			}

			

		}	

		public function editar_usuarios($where = null) { //Array

			try {

				$pack = array (

				'usuario' => $this->db->query('select *, (fk_grupo = 2) as gn from usuarios left join grupo_usuarios on id_usuario = fk_usuario where id_usuario = '.$where[0]),
				'grupos' => $this->db->query('select G.*, GU.fk_usuario > 0 pertence
				from grupos G LEFT OUTER join grupo_usuarios GU on GU.fk_usuario = '.$where[0].'
				and GU.fk_grupo = G.id_grupo
				order by G.id_grupo;')->result()

				);

				return $pack;
				
			} catch (Exception $e) {

				echo 'Falha ao carregar usuário: '.$e;
				
			}

		}

		public function proprios_usuarios() {

			try {

				return $this->db->get_where('usuarios', array ('usuario' => $this->session->userdata('usuario')));
				
			} catch (Exception $e) {

				echo 'Falha ao carregar usuário: '.$e;
				
			}

		}

		public function rem_add_grupo($dados = null, $condicao = null) {

			try {

				if($condicao == 'remover') {

					$this->db->where($dados);
					return $this->db->delete('grupo_usuarios'); 

				} else {

					$this->db->where('fk_usuario',$dados['fk_usuario']);
					$this->db->delete('grupo_usuarios'); 

					return $this->db->insert('grupo_usuarios',$dados);

				}
				
			} catch (Exception $e) {

				echo 'Falha ao Adicionar/Remover do grupo: '.$e;
				
			}

		}

		//criar / editar metas
		public function metas_editar_usuarios(){

			return $this->db->query('select id_usuario, usuario , ifnull(meta_pre,0) as meta_pre,ifnull(meta_controle,0) as meta_controle,ifnull(meta_boleto,0) as meta_boleto,
				ifnull(meta_total,0) as meta_total,ifnull(meta_recarga,0) as meta_recarga,ifnull(meta_migracao,0) as meta_migracao from usuarios where ativo = 1 and filial_responsavel > 0')->result();

		}

		public function editarMeta($dados){

			$this->db->where('id_usuario',$dados['id_usuario']);
			return $this->db->update('usuarios',$dados);

		}

	}

?>