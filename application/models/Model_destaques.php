<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class model_destaques extends CI_Model {


		public function editar_destaques($where = null) {

			$this->db->query('SET lc_time_names = \'pt_BR\'');

			return $this->db->query('select *, ((data_limite >= curdate() or data_limite is null)) valido, date_format(data_limite, \'%d/%m/%Y\') as limite,
					date_format(data_upload, \'%d de %M de %Y as %H:%i\') as upload from destaques where id_destaque = '.$where[0]);

		}

		public function update($dados = null) {

			try {
				
				$this->db->where('id_destaque', $dados['id_destaque']);
				return $this->db->update('destaques',$dados);

			} catch (Exception $e) {

				echo 'Falha ao editar '.$e;
				return false;
				
			}


		}	

		public function del($id = null) {
			$this->db->where("id_destaque",$id);
			return $this->db->delete("destaques");
		}

		public function listar_destaques() {

			try {
				
				$this->db->query('SET lc_time_names = \'pt_BR\'');

				return $this->db->query('select *, ((data_limite >= curdate() or data_limite is null)) valido, date_format(data_limite, \'%d de %M de %Y\') as limite,
					date_format(data_upload, \'%d de %M de %Y as %H:%i\') as upload from destaques')->result();
				
			} catch (Exception $e) {

				echo 'Falha ao listar: '.$e;
				
			}


		}

		public function novo_Registro($dados = null) {
			try {

				$this->db->insert('destaques',$dados);
				return $this->db->insert_id(); //Retorna o id 

			} catch (Exception $e) {

				echo 'Falha ao gravar '.$e;

			}
		}

		public function deletar_Registro ($id = null) {

			return $this->db->query('delete from destaques where id_destaque = '.$id.';');

		}

		public function atualizar_Registro ($dados = null) {

			return $this->db->query('update destaques set url_imagem = \''.$dados['url_imagem'].'\' where id_destaque = '.$dados['id_destaque'].';');

		}

	}