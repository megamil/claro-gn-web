<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class model_pdv extends CI_Model {

		public function listar_pdv() {

			return array ('pdvs' => $this->db->query("SELECT 
									id_pdv,rede, cnpj, lougradouro, filial, nome
									FROM pdv 
									inner join usuarios on key_account = id_usuario
									where filial != 0 limit 10;")->result(),

						'redes' => $this->db->get('lista_redes')->result(),

						'usuarios' => $this->db->query('select distinct(id_usuario), usuario from pdv
				left join usuarios on id_usuario = key_account where ativo = 1 and id_usuario != 1')->result()

						);

		}

		public function novo_pdv(){

			return array (
							'usuarios' => $this->db->query('select id_usuario, usuario from usuarios inner join grupo_usuarios on id_usuario = fk_usuario where fk_grupo = 2 and ativo = 1;')->result(),
							'rede' => $this->db->query('select * from lista_redes')->result(),
							'razao_social' => $this->db->query('select * from lista_razao_social')->result()
						);

		}

		public function filtro($dados = null){

			$where = "";

			if ($dados['usuario'] != "") {
				$where .= " and id_usuario = ".$dados['usuario'];
			}

			if ($dados['rede'] != "") {
				$where .= " and rede = '".$dados['rede']."'";	
			}

			if ($dados['filial'] != "") {
				$where .= " and filial = ".$dados['filial'];
			}

			if ($dados['cnpj'] != "") {
				$where .= " and cnpj = ".$dados['cnpj'];
			}

			return $this->db->query("SELECT 
									id_pdv,rede, cnpj, lougradouro, filial, nome
									FROM pdv 
									inner join usuarios on key_account = id_usuario
									where filial != 0 {$where};")->result();


		}

		public function filtroExcel($dados = null){

			$where = "";

			if ($dados['usuario'] != "") {
				$where .= " and id_usuario = ".$dados['usuario'];
			}

			if ($dados['rede'] != "") {
				$where .= " and rede = '".$dados['rede']."'";	
			}

			if ($dados['filial'] != "") {
				$where .= " and filial = ".$dados['filial'];
			}

			if ($dados['cnpj'] != "") {

				$cnpj = str_replace('.', '', $dados['cnpj']);
				$cnpj = str_replace('-', '', $cnpj);
				$cnpj = str_replace('/', '', $cnpj);

				$where .= " and cnpj = '".$cnpj."'";
			}

			return $this->db->query("SELECT 

									id_pdv,cod_agente,cnpj,rede,razao_social,lougradouro,numero_local,complemento,
									bairro,cep,cidade,filial,cod_interno_rede,varejo,curva,
									(select nome from usuarios where id_usuario = key_account) as key_account,
									data_criacao,data_edicao,(select nome from usuarios where id_usuario = fk_usuario_criou) as fk_usuario_criou

									FROM pdv 
									where filial != 0 {$where};")->result();


		}

		public function editar_pdv($where = null){

			return array (
							'usuarios' => $this->db->query('select id_usuario, usuario from usuarios where ativo = 1;')->result(),
							'rede' => $this->db->query('select * from lista_redes')->result(),
							'pdv' => $this->db->query('select * from pdv where id_pdv = '.$where[0]),
							'razao_social' => $this->db->query('select * from lista_razao_social')->result()
						);


		}

		public function update_pdv($dados = null,$id = null) {
			$this->db->where('id_pdv',$id);
			$this->db->update('pdv',$dados);

			return $this->db->query('update pdv set data_edicao = curdate() where id_pdv = '.$id);
		}

		public function add_pdv($dados = null){
			$this->db->insert('pdv',$dados);
			$id = $this->db->insert_id(); //Retorna o id 

			$this->db->query('update pdv set data_criacao = curdate() where id_pdv = '.$id);

			return $id;
		}


	}