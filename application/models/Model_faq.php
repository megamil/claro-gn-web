<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class model_faq extends CI_Model {

		public function listar_faq(){

			return $this->db->query('select id_faq, titulo_faq, date_format(data_faq, \'%d/%m/%Y %l:%i %p\') as data_faq, tipo_faq from faq')->result();


		}

		public function criarFaq($faq = null){
			$this->db->insert('faq',$faq);
			return $this->db->insert_id(); //Retorna o id do novo usuário.
		}

		public function editar_faq($where = null) {
			return $this->db->get_where('faq',array ('id_faq' => $where[0]));
		}

		public function editarFaq($faq = null, $id = null){

			return $this->db->query('update faq set titulo_faq = \''.$faq['titulo_faq'].'\', descricao_faq = \''.$faq['descricao_faq'].'\', tipo_faq = '.$faq['tipo_faq'].' where id_faq = '.$id);


		}


	}