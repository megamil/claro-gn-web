<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class model_atento extends CI_Model {

		public function editar_atento($where = null) {

			$this->db->query('SET lc_time_names = \'pt_BR\'');

			return $this->db->query('select *, ((data_limite >= curdate() or data_limite is null)) valido, date_format(data_limite, \'%d/%m/%Y\') as limite,
					date_format(data_upload, \'%d de %M de %Y as %H:%i\') as upload from atento where id_atento = '.$where[0]);

		}

		public function update($dados = null) {

			try {
				
				$this->db->where('id_atento', $dados['id_atento']);
				return $this->db->update('atento',$dados);

			} catch (Exception $e) {

				echo 'Falha ao editar '.$e;
				return false;
				
			}

			

		}	

		public function del($id = null) {
			$this->db->where("id_atento",$id);
			return $this->db->delete("atento");
		}

		public function listar_atento() {

			try {

				$this->db->query('SET lc_time_names = \'pt_BR\'');

				return $this->db->query('select *, ((data_limite >= curdate() or data_limite is null)) valido, date_format(data_limite, \'%d de %M de %Y\') as limite,
					date_format(data_upload, \'%d de %M de %Y %H:%i\') as upload from atento')->result();
				
			} catch (Exception $e) {

				echo 'Falha ao listar: '.$e;
				
			}


		}

		public function novo_Registro($dados = null) {
			try {

				$this->db->insert('atento',$dados);
				return $this->db->insert_id(); //Retorna o id 

			} catch (Exception $e) {

				echo 'Falha ao gravar '.$e;

			}
		}

		public function deletar_Registro ($id = null) {

			return $this->db->query('delete from atento where id_atento = '.$id.';');

		}

		public function atualizar_Registro ($dados = null) {

			return $this->db->query('update atento set url_imagem = \''.$dados['url_imagem'].'\' where id_atento = '.$dados['id_atento'].';');

		}

	}