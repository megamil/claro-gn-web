<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class model_relatorios extends CI_Model {

		public function listar_relatorios(){

			return $this->db->query('select * from aplicacoes where id_aplicacao > 40')->result();

		}

		//Inicio da tela
		public function horario_relatorios(){

			$usuarios = $this->db->query('SELECT id_usuario, nome FROM usuarios inner join grupo_usuarios on fk_usuario = id_usuario and fk_grupo = 2 and ativo = 1;')->result();

			// $lista = "";


			// foreach ($usuarios as $usuario) {
				
			// 	$dados = $this->db->query('select 
			// 		distinct(date(data_checkout)) as dia,
			// 		usuario,
			// 		(select min(time(data_checkout)) as entrou 
			// 		from checkout ch where date(co.data_checkout) = date(ch.data_checkout)) as entrou,
			// 		(select max(time(data_checkout)) as saiu 
			// 		from checkout ch where date(co.data_checkout) = date(ch.data_checkout)) as saiu,

			// 		(select TIMEDIFF(

			// 					(select max(time(data_checkout)) as saiu 
			// 					from checkout ch where date(co.data_checkout) = date(ch.data_checkout)),
								
			// 					(select min(time(data_checkout)) as entrou 
			// 					from checkout ch where date(co.data_checkout) = date(ch.data_checkout))
								
			// 				)
			// 		) as tempo

			// 		from checkout as co
			// 		inner join usuarios u on id_usuario = co.fk_usuario
			// 		inner join grupo_usuarios gs on u.id_usuario = gs.fk_usuario
			// 		inner join pdv on id_pdv = fk_pdv
			// 		where co.fk_usuario = '.$usuario->id_usuario. ' and fk_grupo = 2')->result();

			// 	$horas = Array();

			// 	foreach ($dados as $key => $entrou) {
			// 		$horas[$key] = strtotime($entrou->entrou);
			// 	}

			// 	$entrou_media = date("H:i:s", $this->zero_visitaspdvc(array_sum($horas),count($horas)));

			// 	$horas = Array();

			// 	foreach ($dados as $key => $saiu) {
			// 		$horas[$key] = strtotime($saiu->saiu);
			// 	}

			// 	$saiu_media = date("H:i:s", $this->zero_visitaspdvc(array_sum($horas),count($horas)));

			// 	$horas = Array();

			// 	foreach ($dados as $key => $tempo) {
			// 		$horas[$key] = strtotime($tempo->tempo);
			// 	}

			// 	$tempo_media = date("H:i:s", $this->zero_visitaspdvc(array_sum($horas),count($horas)));

			// 	$f1 = $this->db->query('select count(*) f1 from checkout inner join pdv on id_pdv = fk_pdv where fk_usuario = '.$usuario->id_usuario.' and filial = 1')->row();

			// 	$f2 = $this->db->query('select count(*) f2 from checkout inner join pdv on id_pdv = fk_pdv where fk_usuario = '.$usuario->id_usuario.' and filial = 2')->row();

			// 	$lista .= '<tr>
			// 				<td>'.$usuario->nome.'</td>
			// 				<td>'.$entrou_media.'</td>
			// 				<td>'.$saiu_media.'</td>
			// 				<td>'.$tempo_media.'</td>
			// 				<td>'.$f1->f1.'</td>
			// 				<td>'.$f2->f2.'</td>
			// 				<td>'.($f2->f2 + $f1->f1).'</td>
			// 			</tr>';


			// }

				$lista = '<tr>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>';


			//Lista dos usuarios que não são administradores
			return array( 'usuarios' => $usuarios,

				'lista' => $lista);

		}

		public function lista_horas($de = null,$ate = null){

			$where = "";

			if($de != '') {
				$where .= ' and date(data_checkout) >= \''.$this->fDatas($de).'\'';
			}

			if($ate != '') {
				$where .= ' and date(data_checkout) <= \''.$this->fDatas($ate).'\'';
			}

			$usuarios = $this->db->query('SELECT distinct(id_usuario), nome 
											FROM usuarios
											 inner join checkout on id_usuario = fk_usuario where id_usuario > 0 '.$where)->result();


			$lista = "";


			foreach ($usuarios as $usuario) {
				
				$dados = $this->db->query('select 
					distinct(date(data_checkout)) as dia,
					usuario,
					(select min(time(data_checkout)) as entrou 
					from checkout ch where fk_usuario = '.$usuario->id_usuario.' '.$where.') as entrou,
					(select max(time(data_checkout)) as saiu 
					from checkout ch where fk_usuario = '.$usuario->id_usuario.' '.$where.' and fk_usuario = '.$usuario->id_usuario.') as saiu,

					(select TIMEDIFF(

								(select max(time(data_checkout)) as saiu 
								from checkout ch where fk_usuario = '.$usuario->id_usuario.' '.$where.'),
								
								(select min(time(data_checkout)) as entrou 
								from checkout ch where fk_usuario = '.$usuario->id_usuario.' '.$where.')
								
							)
					) as tempo

					from checkout as co
					inner join usuarios u on id_usuario = co.fk_usuario
					inner join grupo_usuarios gs on u.id_usuario = gs.fk_usuario
					inner join pdv on id_pdv = fk_pdv
					where co.fk_usuario = '.$usuario->id_usuario.' '.$where.' and fk_grupo = 2')->result();

				$horas = Array();

				foreach ($dados as $key => $entrou) {
					$horas[$key] = strtotime($entrou->entrou);
				}

				$entrou_media = date("H:i:s", $this->zero_visitaspdvc(array_sum($horas),count($horas)));

				$horas = Array();

				foreach ($dados as $key => $saiu) {
					$horas[$key] = strtotime($saiu->saiu);
				}

				$saiu_media = date("H:i:s", $this->zero_visitaspdvc(array_sum($horas),count($horas)));

				$horas = Array();

				foreach ($dados as $key => $tempo) {
					$horas[$key] = strtotime($tempo->tempo);
				}

				$tempo_media = date("H:i:s", $this->zero_visitaspdvc(array_sum($horas),count($horas)));

				$f1 = $this->db->query('select count(*) f1 from checkout inner join pdv on id_pdv = fk_pdv where fk_usuario = '.$usuario->id_usuario.' and filial = 1 '.$where)->row();

				$f2 = $this->db->query('select count(*) f2 from checkout inner join pdv on id_pdv = fk_pdv where fk_usuario = '.$usuario->id_usuario.' and filial = 2 '.$where)->row();

				$lista .= '<tr>
							<td>'.$usuario->nome.'</td>
							<td>'.$entrou_media.'</td>
							<td>'.$saiu_media.'</td>
							<td>'.$tempo_media.'</td>
							<td>'.$f1->f1.'</td>
							<td>'.$f2->f2.'</td>
							<td>'.($f2->f2 + $f1->f1).'</td>
						</tr>';




			}

			return $lista;

		}

		public function ajaxHorarioE($id_usuario = null,$de = null, $ate = null) {

			$where = ' where co.fk_usuario = '.$id_usuario;

			if($de != '') {
				$where .= ' and date(data_checkout) >= \''.$this->fDatas($de).'\'';
			}

			if($ate != '') {
				$where .= ' and date(data_checkout) <= \''.$this->fDatas($ate).'\'';
			}

			//Dados Gerais,
			$dados = $this->db->query('select 
					distinct(date(data_checkout)) as dia,
					usuario,
					(select min(time(data_checkout)) as entrou 
					from checkout ch where date(co.data_checkout) = date(ch.data_checkout)) as entrou,
					(select max(time(data_checkout)) as saiu 
					from checkout ch where date(co.data_checkout) = date(ch.data_checkout)) as saiu,

					(select TIMEDIFF(

								(select max(time(data_checkout)) as saiu 
								from checkout ch where date(co.data_checkout) = date(ch.data_checkout)),
								
								(select min(time(data_checkout)) as entrou 
								from checkout ch where date(co.data_checkout) = date(ch.data_checkout))
								
							)
					) as tempo

					from checkout as co
					inner join usuarios on id_usuario = fk_usuario

					inner join pdv on id_pdv = fk_pdv
					'.$where)->result();

				$horas = Array();

				foreach ($dados as $key => $entrou) {
					$horas[$key] = strtotime($entrou->entrou);
				}

				if (count($horas) > 0) {
					$entrou_media = date("H:i:s", $this->zero_visitaspdvc(array_sum($horas),count($horas)));
				} else {
					$entrou_media = 0;
				}

				$horas = Array();

				foreach ($dados as $key => $saiu) {
					$horas[$key] = strtotime($saiu->saiu);
				}

				if (count($horas) > 0) {
					$saiu_media = date("H:i:s", $this->zero_visitaspdvc(array_sum($horas),count($horas)));
				} else {
					$saiu_media = 0;
				}

				$horas = Array();

				foreach ($dados as $key => $tempo) {
					$horas[$key] = strtotime($tempo->tempo);
				}

				if (count($horas) > 0) {
					$tempo_medio = date("H:i:s", $this->zero_visitaspdvc(array_sum($horas),count($horas)));
				} else {
					$tempo_medio = 0;
				}

				//Média Filial 1
				$dados = $this->db->query('select 
					distinct(date(data_checkout)) as dia,
					usuario,
					(select TIMEDIFF(

								(select max(time(data_checkout)) as saiu 
								from checkout ch where date(co.data_checkout) = date(ch.data_checkout)),
								
								(select min(time(data_checkout)) as entrou 
								from checkout ch where date(co.data_checkout) = date(ch.data_checkout))
								
							)
					) as tempo

					from checkout as co
					inner join usuarios u on id_usuario = co.fk_usuario
					inner join grupo_usuarios gs on u.id_usuario = gs.fk_usuario
					inner join pdv on id_pdv = fk_pdv
					'.$where.' and filial = 1 and fk_grupo = 2')->result();

				$horas = Array();

				foreach ($dados as $key => $tempo) {
					$horas[$key] = strtotime($tempo->tempo);
				}

				if (count($horas) > 0) {
					$tempo_medio_f1 = date("H:i:s", $this->zero_visitaspdvc(array_sum($horas),count($horas)));
				} else {
					$tempo_medio_f1 = 0;
				}


				//Média Filial 2
				$dados = $this->db->query('select 
					distinct(date(data_checkout)) as dia,
					usuario,
					(select TIMEDIFF(

								(select max(time(data_checkout)) as saiu 
								from checkout ch where date(co.data_checkout) = date(ch.data_checkout)),
								
								(select min(time(data_checkout)) as entrou 
								from checkout ch where date(co.data_checkout) = date(ch.data_checkout))
								
							)
					) as tempo

					from checkout as co
					inner join usuarios u on id_usuario = co.fk_usuario
					inner join grupo_usuarios gs on u.id_usuario = gs.fk_usuario
					inner join pdv on id_pdv = fk_pdv
					'.$where.' and filial = 2 and fk_grupo = 2')->result();

				$horas = Array();

				foreach ($dados as $key => $tempo) {
					$horas[$key] = strtotime($tempo->tempo);
				}

				if (count($horas) > 0) {
					$tempo_medio_f2 = date("H:i:s", $this->zero_visitaspdvc(array_sum($horas),count($horas)));
				} else {
					$tempo_medio_f2 = 0;
				}

				return array(

						'entrou_media' => $entrou_media,
						'saiu_media' => $saiu_media,
						'tempo_medio' => $tempo_medio,
						'tempo_medio_f1' => $tempo_medio_f1,
						'tempo_medio_f12' => $tempo_medio_f2

					);



		}


		################################# view_visitaspdvc_relatorios #################################
		public function visitaspdvc_relatorios(){
			//Lista dos usuarios que não são administradores
			return array(

				'usuarios' => $this->db->query('SELECT * FROM usuarios inner join grupo_usuarios on fk_usuario = id_usuario and fk_grupo = 2 and ativo = 1;')->result(),
				
				'dados' =>$this->db->query('select 
									round((count(*) / datediff(curdate(),min(data_checkout))),2) media_checkout,
									count(*) total_checkout,

									(select count(*) from checkout 
									inner join pdv on id_pdv = fk_pdv where filial = 1) filiais1,
								    (select count(*) from checkout 
									inner join pdv on id_pdv = fk_pdv where filial = 2) filiais2,

								    (select count(*)
									from checkout c
									inner join pdv on id_pdv = fk_pdv 
								    inner join usuarios u on id_usuario = c.fk_usuario
								    inner join grupo_usuarios gs on u.id_usuario = gs.fk_usuario
									where fk_pdv not in (select fk_pdv from carteira_gn cg
																where cg.fk_usuario = c.fk_usuario) and fk_grupo = 2) as fora_carteira,

									datediff(curdate(),min(data_checkout)) as dias/* Total em dias*/ 
									from checkout 
									inner join pdv on id_pdv = fk_pdv
									inner join usuarios u on id_usuario = checkout.fk_usuario
									inner join grupo_usuarios gs on u.id_usuario = gs.fk_usuario
									where data_checkout > 0 and fk_grupo = 2')->row(),

				'visitar_gn' => $this->db->query('select
													round((count(*) / datediff(curdate(),min(data_checkout))),2) media, 
													count(*) total,
												    usuario, 
												    id_usuario, 
													(select count(*)
														from checkout c0
														inner join pdv on id_pdv = fk_pdv 
														inner join usuarios u on u.id_usuario = c0.fk_usuario
														inner join grupo_usuarios gs on u.id_usuario = gs.fk_usuario 
														where  fk_pdv not in (select fk_pdv from carteira_gn cg
																	where cg.fk_usuario = c0.fk_usuario) and u.id_usuario = us.id_usuario and fk_grupo = 2) 
													fora
													from checkout c1
													inner join pdv on id_pdv = fk_pdv
													inner join usuarios us on us.id_usuario = c1.fk_usuario
													inner join grupo_usuarios gs on us.id_usuario = gs.fk_usuario 
													where data_checkout > 0 and fk_grupo = 2 group by usuario')->result());

		}

		// public function ajax_visitaspdvc($id_usuario = null, $where = null){

		// 	if($where == 1){ // quando for usado esse where o resultado vai ficar diferente, pois existem filiais 0, as que foram adicionadas por fora.
		// 		$where = " and filial = 1";
		// 	} else if($where == 2) {
		// 		$where = " and filial = 2";
		// 	}

		// 	$dados = $this->db->query('select (
		// 								select 
		// 								count(*)
		// 								from checkout 
		// 								inner join pdv on id_pdv = fk_pdv
		// 								inner join usuario on id_usuario = fk_usuario
		// 								where fk_usuario = '.$id_usuario.' and filial = filial_responsavel '.$where.') carteira,
		// 								(select 
		// 								count(*)
		// 								from checkout 
		// 								inner join pdv on id_pdv = fk_pdv
		// 								inner join usuario on id_usuario = fk_usuario
		// 								where fk_usuario = '.$id_usuario.' and filial <> filial_responsavel '.$where.') outros,
		// 								(select count(distinct(date(data_checkout)))
		// 								from checkout 
		// 								inner join pdv on id_pdv = fk_pdv 
		// 								inner join usuario on id_usuario = fk_usuario
		// 								where fk_usuario = '.$id_usuario.' and filial = filial_responsavel '.$where.') diascarteira,
		// 							    (select count(distinct(date(data_checkout)))
		// 								from checkout 
		// 								inner join pdv on id_pdv = fk_pdv 
		// 								inner join usuario on id_usuario = fk_usuario
		// 								where fk_usuario = '.$id_usuario.' and filial <> filial_responsavel '.$where.') diasoutros');

		// 	$totalCarteira = $dados->row()->carteira;
		// 	$totalOutros = $dados->row()->outros;
		// 	$diasCarteira = $dados->row()->diascarteira;
		// 	$diasOutros = $dados->row()->diasoutros;

		// 	return array (

		// 		'diaria' => array('carteira' => $this->zero_visitaspdvc($totalCarteira,$diasCarteira),'outros' => $this->zero_visitaspdvc($totalOutros,$diasOutros)),

		// 		'semanal' => array('carteira' => $this->zero_visitaspdvc($totalCarteira,7),'outros' => $this->zero_visitaspdvc($totalOutros,7)),

		// 		// 'quinzenal' => array('carteira' => $this->zero_visitaspdvc($totalCarteira,15),'outros' => $this->zero_visitaspdvc($totalOutros,15)),

		// 		'mensal' => array('carteira' => $this->zero_visitaspdvc($totalCarteira,30),'outros' => $this->zero_visitaspdvc($totalOutros,30)),

		// 		// 'trimestral' => array('carteira' => $this->zero_visitaspdvc($totalCarteira,90),'outros' => $this->zero_visitaspdvc($totalOutros,90)),

		// 		'semestral' => array('carteira' => $this->zero_visitaspdvc($totalCarteira,180),'outros' => $this->zero_visitaspdvc($totalOutros,180)),

		// 		'anual' => array('carteira' => $this->zero_visitaspdvc($totalCarteira,365),'outros' => $this->zero_visitaspdvc($totalOutros,365))


		// 	);

		// }

		######################################## MÉDIA GERAL DAS VISITAS ########################################

		public function ajax_visitaspdvc_filtro($data_filtro = null, $de = null, $ate = null, $filial_filtro = null, $gn_filtro = null){


			$where = "";
			if($filial_filtro != ""){
				$where .= " and filial = ".$filial_filtro;
			}

			if($de != '') {
				$de = '\''.$this->fDatas($de).'\'';
				$where .= ' and date(data_checkout) >= '.$de;
			} else {
				$de = 'min(data_checkout)';
			}

			if($ate != '') {
				$where .= ' and date(data_checkout) <= \''.$this->fDatas($ate).'\'';
				$ate = '\''.$this->fDatas($ate).'\'';
			} else {
				$ate = ' curdate() ';
			}

			if($gn_filtro != '') {
				$where .= ' and checkout.fk_usuario = \''.$gn_filtro.'\'';
			}

			return $this->db->query('select 
									round(((count(*) / (datediff('.$ate.','.$de.')+ 1)) / (select count(*) from usuarios inner join grupo_usuarios on id_usuario = fk_usuario where fk_grupo = 2 and ativo = 1) ),2) media_checkout,
									count(*) total_checkout,

									(select round((count(*) / (select count(*) from usuarios inner join grupo_usuarios on id_usuario = fk_usuario where fk_grupo = 2 and filial_responsavel = 1 and ativo = 1)),2)  from checkout c0
									inner join pdv on id_pdv = fk_pdv 
									inner join usuarios u on id_usuario = c0.fk_usuario
								    inner join grupo_usuarios gs on u.id_usuario = gs.fk_usuario
									where filial = 1 '.$where.' and fk_grupo = 2) filiais1,


								    (select round((count(*) / (select count(*) from usuarios inner join grupo_usuarios on id_usuario = fk_usuario where fk_grupo = 2 and filial_responsavel = 2 and ativo = 1)),2)  from checkout c0
									inner join pdv on id_pdv = fk_pdv 
									inner join usuarios u on id_usuario = c0.fk_usuario
								    inner join grupo_usuarios gs on u.id_usuario = gs.fk_usuario
									where filial = 2 '.$where.' and fk_grupo = 2) filiais2,

								    (select round((count(*) / (select count(*) from usuarios inner join grupo_usuarios on id_usuario = fk_usuario where fk_grupo = 2 and ativo = 1)),2) 
									from checkout c0
									inner join pdv fcpdv on fcpdv.id_pdv = c0.fk_pdv 
								    inner join usuarios u on id_usuario = c0.fk_usuario
								    inner join grupo_usuarios gs on u.id_usuario = gs.fk_usuario
									where c0.fk_pdv not in (select fk_pdv from carteira_gn cg
																where cg.fk_usuario = c0.fk_usuario) and fk_grupo = 2 '.$where.') as fora_carteira,

									datediff('.$ate.',min(data_checkout)) as dias/* Total em dias*/ 
									from checkout 
									inner join pdv on id_pdv = fk_pdv
									inner join usuarios u on id_usuario = checkout.fk_usuario
									inner join grupo_usuarios gs on u.id_usuario = gs.fk_usuario
									where data_checkout > 0 and fk_grupo = 2 '.$where)->result();
		}

		public function ajax_visitaspdvc_filtro_lista($data_filtro = null, $de = null, $ate = null, $filial_filtro = null, $gn_filtro = null){

			$where = "";
			if($filial_filtro != ""){
				$where .= " and filial = ".$filial_filtro;
			}

			if($de != '') {
				$de = '\''.$this->fDatas($de).'\'';
				$where .= ' and date(data_checkout) >= '.$de;
			} else {
				$de = 'min(data_checkout)';
			}

			if($ate != '') {
				$where .= ' and date(data_checkout) <= \''.$this->fDatas($ate).'\'';
				$ate = '\''.$this->fDatas($ate).'\'';
			} else {
				$ate = ' curdate() ';
			}

			if($gn_filtro != '') {
				$where .= ' and c1.fk_usuario = \''.$gn_filtro.'\'';
			}

			return $this->db->query('select 
										round((count(*) / datediff('.$ate.','.$de.')+ 1),2) media,
										count(*) total, usuario, id_usuario, 
										(select count(*)
											from checkout c0
											inner join pdv on id_pdv = fk_pdv 
										    inner join usuarios u on u.id_usuario = c0.fk_usuario
										    inner join grupo_usuarios gs on u.id_usuario = gs.fk_usuario
											where  fk_pdv not in (select fk_pdv from carteira_gn cg
																where cg.fk_usuario = c0.fk_usuario) and fk_grupo = 2 and u.id_usuario = us.id_usuario '.$where.') fora
										from checkout c1
										inner join pdv on id_pdv = fk_pdv
									    inner join usuarios us on us.id_usuario = c1.fk_usuario
									    inner join grupo_usuarios gs on us.id_usuario = gs.fk_usuario
									    where data_checkout > 0 and fk_grupo = 2 '.$where.' group by usuario')->result();

		}

		public function ajax_visitaspdvc_detalhes($data_filtro = null, $de = null, $ate = null, $gn_filtro = null,$fora = null){

			if ($gn_filtro == "") {//no caso do Excel
				$where = ' where fk_pdv > 1';
			} else {
				$where = ' where fk_usuario = \''.$gn_filtro.'\' and fk_pdv > 1';
			}

			if($de != '') {
				$where .= ' and date(data_checkout) >= \''.$this->fDatas($de).'\'';
			}

			if($ate != '') {
				$where .= ' and date(data_checkout) <= \''.$this->fDatas($ate).'\'';
			}

			if ($fora) {
				$where .= ' and fk_pdv not in (select fk_pdv from carteira_gn cg
																where cg.fk_usuario = fk_usuario)';
			} else {
				$where .= '';
			}

			return $this->db->query('select usuario,cnpj, rede, lougradouro, curva 
									from pdv
									inner join checkout on fk_pdv = id_pdv
									inner join usuarios on fk_usuario = id_usuario
									'.$where)->result();

		}

		public function ajax_visitaspdvc_detalhes_excel($data_filtro = null, $de = null, $ate = null){

			$where = ' where fk_pdv > 1';

			if($de != '') {
				$where .= ' and date(data_checkout) >= \''.$this->fDatas($de).'\'';
			}

			if($ate != '') {
				$where .= ' and date(data_checkout) <= \''.$this->fDatas($ate).'\'';
			}

			return $this->db->query('select usuario,cnpj, rede, lougradouro, curva, (filial_responsavel = filial) 						fora from pdv inner join checkout on fk_pdv = id_pdv inner join usuarios on 							fk_usuario = id_usuario	'.$where)->result();

		}

		

		//Garante que não será feita divisão por 0
		public function zero_visitaspdvc($valor = null, $dividir = null){

			if($valor > 0 && $dividir > 0){
				return ($valor / $dividir);
			} else {
				return $valor;
			}

		}


		################################# view_visitasgerais_relatorios #################################
		public function visitargerais_relatorios(){

			return array (
				'atual' => $this->db->query('select 
										count(distinct(fk_pdv)) as distintos
										from checkout
										where month(data_checkout) = month(current_date());'),
				'ultimo' => $this->db->query('select 
										count(distinct(fk_pdv)) as distintos
										from checkout where
										EXTRACT(YEAR_MONTH from data_checkout) = EXTRACT(YEAR_MONTH from current_date() - INTERVAL 1 MONTH);'),
				'trimestre' => $this->db->query('select 
										count(distinct(fk_pdv)) as distintos
										from checkout where
										EXTRACT(YEAR_MONTH from data_checkout) >= EXTRACT(YEAR_MONTH from current_date() - INTERVAL 3 MONTH);'),
				'semestre' => $this->db->query('select 
										count(distinct(fk_pdv)) as distintos
										from checkout where
										EXTRACT(YEAR_MONTH from data_checkout) >= EXTRACT(YEAR_MONTH from current_date() - INTERVAL 6 MONTH);'),
				'ano' => $this->db->query('select 
										count(distinct(fk_pdv)) as distintos
										from checkout where
										EXTRACT(YEAR_MONTH from data_checkout) >= EXTRACT(YEAR_MONTH from current_date() - INTERVAL 12 MONTH);'),


				'totalpdvs' => $this->db->query('select count(*) as totalpdvs from pdv'),



				'atualDetalhes' => $this->db->query('(select fk_pdv, rede, count(fk_pdv) as valor from checkout
								inner join pdv on fk_pdv = id_pdv
								where month(data_checkout) = month(current_date()) and filial > 0
								group by fk_pdv order by valor desc limit 1)

								union 

								(select fk_pdv, rede, count(fk_pdv) as valor from checkout
								inner join pdv on fk_pdv = id_pdv
								where month(data_checkout) = month(current_date()) and filial > 0
								group by fk_pdv order by valor asc limit 1)'),

				'ultimoDetalhes' => $this->db->query('(select fk_pdv, rede, count(fk_pdv) as valor from checkout
								inner join pdv on fk_pdv = id_pdv
								where EXTRACT(YEAR_MONTH from data_checkout) = EXTRACT(YEAR_MONTH from current_date() - INTERVAL 1 MONTH) and filial > 0
								group by fk_pdv order by valor desc limit 1)

								union 

								(select fk_pdv, rede, count(fk_pdv) as valor from checkout
								inner join pdv on fk_pdv = id_pdv
								where EXTRACT(YEAR_MONTH from data_checkout) = EXTRACT(YEAR_MONTH from current_date() - INTERVAL 1 MONTH) and filial > 0
								group by fk_pdv order by valor asc limit 1)'),

				'trimestreDetalhes' => $this->db->query('(select fk_pdv, rede, count(fk_pdv) as valor from checkout
								inner join pdv on fk_pdv = id_pdv
								where EXTRACT(YEAR_MONTH from data_checkout) >= EXTRACT(YEAR_MONTH from current_date() - INTERVAL 3 MONTH) and filial > 0
								group by fk_pdv order by valor desc limit 1)

								union 

								(select fk_pdv, rede, count(fk_pdv) as valor from checkout
								inner join pdv on fk_pdv = id_pdv
								where EXTRACT(YEAR_MONTH from data_checkout) >= EXTRACT(YEAR_MONTH from current_date() - INTERVAL 3 MONTH) and filial > 0
								group by fk_pdv order by valor asc limit 1)'),

				'semestreDetalhes' => $this->db->query('(select fk_pdv, rede, count(fk_pdv) as valor from checkout
								inner join pdv on fk_pdv = id_pdv
								where EXTRACT(YEAR_MONTH from data_checkout) >= EXTRACT(YEAR_MONTH from current_date() - INTERVAL 6 MONTH) and filial > 0
								group by fk_pdv order by valor desc limit 1)

								union 

								(select fk_pdv, rede, count(fk_pdv) as valor from checkout
								inner join pdv on fk_pdv = id_pdv
								where EXTRACT(YEAR_MONTH from data_checkout) >= EXTRACT(YEAR_MONTH from current_date() - INTERVAL 6 MONTH) and filial > 0
								group by fk_pdv order by valor asc limit 1)'),

				'anoDetalhes' => $this->db->query('(select fk_pdv, rede, count(fk_pdv) as valor from checkout
								inner join pdv on fk_pdv = id_pdv
								where EXTRACT(YEAR_MONTH from data_checkout) >= EXTRACT(YEAR_MONTH from current_date() - INTERVAL 12 MONTH) and filial > 0
								group by fk_pdv order by valor desc limit 1)

								union 

								(select fk_pdv, rede, count(fk_pdv) as valor from checkout
								inner join pdv on fk_pdv = id_pdv
								where EXTRACT(YEAR_MONTH from data_checkout) >= EXTRACT(YEAR_MONTH from current_date() - INTERVAL 12 MONTH) and filial > 0
								group by fk_pdv order by valor asc limit 1)'),


				);

		}


		################################# view_mediatam_relatorios #################################

		public function mediatam_relatorios(){
			//Lista dos usuarios que não são administradores
			return array( 'usuarios' => $this->db->query('SELECT * FROM usuarios inner join grupo_usuarios on fk_usuario = id_usuario and fk_grupo = 2 and ativo = 1;')->result());
		}


		public function mediatam_ajax($id = null, $where = null,$de = null, $ate = null, $cnpj = null){

			if($id != '') {
				$where .= ' and fk_usuario > '.$id;
			}

			if($de != '') {
				$where .= ' and date(data_checkout) > \''.$this->fDatas($de).'\'';
			}

			if($ate != '') {
				$where .= ' and date(data_checkout) < \''.$this->fDatas($ate).'\'';
			}

			if($cnpj != ""){ $where .= " and cnpj = '".$cnpj."'";}

			return $this->db->query('select
				count(id_checkout) as total,
				count(atendimento_telefonia) as telefonia,
				count(atendimento_aberto) as aberto,
				count(atendimento_mobile) as mobile
				from checkout
				left join checkout_atendimento on fk_atendimento = id_atendimento
				left join pdv on fk_pdv = id_pdv
				where fk_usuario > 0 '.$where);

		}

		################################# view_recorrencia_relatorios #################################

		public function recorrencia_ajax_pdf($cnpj = null,$de = null,$ate = null,
$filial_filtro = null, $gn_filtro = null){

			$where = "";
			$where_com_checkout = "";

			if($cnpj != ""){ $where .= " and cnpj = ".$cnpj."";}
			if($filial_filtro != ""){ $where .= " and filial = ".$filial_filtro;}

			$where_com_checkout = $where; 

			if($de != ""){ $where_com_checkout .= " and date(data_checkout) >= '".$this->fDatas($de)."'";}
			if($ate != ""){ $where_com_checkout .= " and date(data_checkout) <= '".$this->fDatas($ate)."'";}
			if($gn_filtro != ""){ $where_com_checkout .= " and fk_usuario = ".$gn_filtro;}


			$where1 = "select

						distinct(rede) as rede,
					    
						(select count(rede) from pdv p0 where p0.rede = p2.rede and curva = 'A' ".$where." and id_pdv not in (1,991,990,1864,1865)) lojas_ativas_a,
						(select count(rede) from pdv p0 where p0.rede = p2.rede and curva = 'B' ".$where." and id_pdv not in (1,991,990,1864,1865)) lojas_ativas_b,
						(select count(rede) from pdv p0 where p0.rede = p2.rede and curva = 'C' ".$where." and id_pdv not in (1,991,990,1864,1865)) lojas_ativas_c			

						from pdv p2
						where id_pdv not in (1,991,990,1864,1865) ".$where."
						group by rede
						order by rede;";

			$where2 = "select
				distinct(rede) as rede,
			    
				(select count(distinct(fk_pdv)) from checkout inner join pdv p on fk_pdv = id_pdv where p.rede = p2.rede and curva = 'A' ".$where_com_checkout." and fk_pdv not in (1,991,990,1864,1865)) lojas_atendidas_a,
			    (select count(distinct(fk_pdv)) from checkout inner join pdv p on fk_pdv = id_pdv where p.rede = p2.rede and curva = 'B' ".$where_com_checkout." and fk_pdv not in (1,991,990,1864,1865)) lojas_atendidas_b,
			    (select count(distinct(fk_pdv)) from checkout inner join pdv p on fk_pdv = id_pdv where p.rede = p2.rede and curva = 'C' ".$where_com_checkout." and fk_pdv not in (1,991,990,1864,1865)) lojas_atendidas_c,
			    
				(select count(fk_pdv) from checkout inner join pdv p on fk_pdv = id_pdv where p.rede = p2.rede and curva = 'A' ".$where_com_checkout." and fk_pdv not in (1,991,990,1864,1865)) visitas_realizadas_a,
			    (select count(fk_pdv) from checkout inner join pdv p on fk_pdv = id_pdv where p.rede = p2.rede and curva = 'B' ".$where_com_checkout." and fk_pdv not in (1,991,990,1864,1865)) visitas_realizadas_b,
			    (select count(fk_pdv) from checkout inner join pdv p on fk_pdv = id_pdv where p.rede = p2.rede and curva = 'C' ".$where_com_checkout." and fk_pdv not in (1,991,990,1864,1865)) visitas_realizadas_c
			    
				from pdv p2
				where id_pdv not in (1,991,990,1864,1865) ".$where."
				group by rede
				order by rede;";

			return array('ativos' => $this->db->query($where1)->result(),
						'dados' => $this->db->query($where2)->result());

		}

		public function recorrencia_relatorios(){

			return array ('usuarios' => $this->db->query('SELECT * FROM usuarios inner join grupo_usuarios on fk_usuario = id_usuario and fk_grupo = 2 and ativo = 1;')->result());

		}

		public function recorrencia_ajax($cnpj = null,$de = null,$ate = null,$curva_filtro = null,
$filial_filtro = null, $gn_filtro = null){

			$where = "";
			$where_com_checkout = "";

			if($cnpj != ""){ $where .= " and cnpj = ".$cnpj."";}
			if($curva_filtro != ""){ $where .= " and lower(curva) = lower('".$curva_filtro."')";}
			if($filial_filtro != ""){ $where .= " and filial = ".$filial_filtro;}

			$where_com_checkout = $where; 

			if($de != ""){ $where_com_checkout .= " and date(data_checkout) >= '".$this->fDatas($de)."'";}
			if($ate != ""){ $where_com_checkout .= " and date(data_checkout) <= '".$this->fDatas($ate)."'";}
			if($gn_filtro != ""){ $where_com_checkout .= " and fk_usuario = ".$gn_filtro;}

			return $this->db->query('select
										distinct(rede) as rede,
										count(rede) lojas_ativas,
										(select count(distinct(fk_pdv)) from checkout inner join pdv p on fk_pdv = id_pdv where p.rede = p2.rede '.$where_com_checkout.' and fk_pdv not in (1,991,990,1864,1865)) lojas_atendidas,
										(select count(fk_pdv) from checkout inner join pdv p on fk_pdv = id_pdv where p.rede = p2.rede '.$where_com_checkout.' and fk_pdv not in (1,991,990,1864,1865)) visitas_realizadas
										from pdv p2
										where id_pdv not in (1,991,990,1864,1865) '.$where.'
										group by rede
										order by rede')->result();

		}

		public function recorrencia_atendidos($rede = null,$cnpj = null,$de = null,$ate = null,$curva_filtro = null,
$filial_filtro = null, $gn_filtro = null){

			$where = "";

			if($rede != ""){ $where .= " and lower(rede) = lower('".$rede."')";}
			if($cnpj != ""){ $where .= " and cnpj = ".$cnpj."";}
			if($curva_filtro != ""){ $where .= " and lower(curva) = lower('".$curva_filtro."')";}
			if($filial_filtro != ""){ $where .= " and filial = ".$filial_filtro;}
			if($de != ""){ $where .= " and date(data_checkout) >= '".$this->fDatas($de)."'";}
			if($ate != ""){ $where .= " and date(data_checkout) <= '".$this->fDatas($ate)."'";}
			if($gn_filtro != ""){ $where .= " and fk_usuario = ".$gn_filtro;}


			return $this->db->query("select 
									rede,
									concat(cidade,' ',bairro,' ',lougradouro,' ',cep,' ',numero_local) endereco,
									usuario,
									curva
									from checkout c
									inner join pdv on id_pdv = fk_pdv
									inner join usuarios u on id_usuario = c.fk_usuario
									inner join grupo_usuarios gs on u.id_usuario = gs.fk_usuario
									where id_pdv not in (1,991,990,1864,1865) and fk_grupo = 2 ".$where)->result();

		}

		public function recorrencia_ativos($rede = null,$cnpj = null,$curva_filtro = null,$filial_filtro = null){

			$where = "";
			if($rede != ""){ $where .= " and lower(rede) = lower('".$rede."')";}
			if($cnpj != ""){ $where .= " and cnpj = ".$cnpj."";}
			if($curva_filtro != ""){ $where .= " and lower(curva) = lower('".$curva_filtro."')";}
			if($filial_filtro != ""){ $where .= " and filial = ".$filial_filtro;}

			return $this->db->query("select 
									rede,
									concat(cidade,' ',bairro,' ',lougradouro,' ',cep,' ',numero_local) endereco,
									curva
									from pdv 
									where id_pdv > 0 ".$where)->result();

		}

		################################# view_faturamento_relatorios #################################

		public function faturamento_ajax($de = null, $ate = null, $cnpj = null){

			$where = "";

			if($de != ""){ $where .= " and date(data_checkout) >= '".$this->fDatas($de)."'";}
			if($ate != ""){ $where .= " and date(data_checkout) <= '".$this->fDatas($ate)."'";}

			return $this->db->query('select
										REPLACE(REPLACE(REPLACE(FORMAT(avg(faturamento_pdv), 2),\'.\',\';\'),\',\',\'.\'),\';\',\',\') as faturamento,
										ROUND(avg(va_f1)) va_f1,
										ROUND(avg(va_f2)) va_f2,
										ROUND(avg(va_f3)) va_f3,
										ROUND(avg(va_f4)) va_f4,
										ROUND(avg(va_f5)) va_f5,
										ROUND(avg(va_f6)) va_f6,
										curva,
										rede
										from checkout_faturamento
										inner join checkout on id_faturamento = fk_faturamento
										inner join pdv on id_pdv = fk_pdv
										where cnpj = \''.$cnpj.'\' '.$where);

		}

		################################# view_promotor_relatorios #################################

		public function promotor_relatorios(){

			//Lista dos usuarios que não são administradores
			return array('usuarios' => $this->db->query('SELECT * FROM usuarios inner join grupo_usuarios on fk_usuario = id_usuario and fk_grupo = 2 and ativo = 1;')->result());

		}

		public function promotor_ajax($de = null,$ate = null,$filial = null,$fk_usuario = null,$curva = null){

			$where = "";

			if($de != ""){ $where .= " and date(data_checkout) >= '".$this->fDatas($de)."'";}
			if($ate != ""){ $where .= " and date(data_checkout) <= '".$this->fDatas($ate)."'";}
			if($filial != ""){ $where .= " and filial = '".$filial."'";}
			if($curva != ""){ $where .= " and curva = '".$curva."'";}
			if($fk_usuario != ""){ $where .= " and fk_usuario = '".$fk_usuario."'";}

			return $this->db->query('select 
										lower(operadora) operadora,
										sum(fixo) fixo,
										sum(part_time) pt,
										sum(free) free
										from checkout
										inner join pdv on id_pdv = fk_pdv
										inner join checkout_part_time on id_checkout = fk_checkout
										inner join operadoras on fk_operadora = id_operadora
										where id_pdv > 0 '.$where.'
										group by operadora')->result();

		}

		################################# view_gnatente_relatorios #################################

		public function gnatente_relatorios(){

			return array ('apresentacao' => $this->db->query("select
										count(apresentacao) quantidade,
										apresentacao
										from checkout
										inner join pdv on id_pdv = fk_pdv
										inner join checkout_atendepn on id_atendepn = fk_atendepn
										group by apresentacao")->result(),
						'conhecimento' => $this->db->query("select
										count(conhecimento) quantidade,
										conhecimento
										from checkout
										inner join pdv on id_pdv = fk_pdv
										inner join checkout_atendepn on id_atendepn = fk_atendepn
										group by conhecimento")->result(),
						'book' => $this->db->query("select
										count(book) quantidade,
										book
										from checkout
										inner join pdv on id_pdv = fk_pdv
										inner join checkout_atendepn on id_atendepn = fk_atendepn
										group by book")->result(),
						'total' => $this->db->query("select count(*) as total from checkout")->row()->total
						);

										

		}

		public function atende_ajax($filial = null,$de = null, $ate = null){

			$where = "";

			if($de != ""){ $where .= " and date(data_checkout) >= '".$this->fDatas($de)."'";}
			if($ate != ""){ $where .= " and date(data_checkout) <= '".$this->fDatas($ate)."'";}
			if($filial != ""){ $where .= " and filial = '".$filial."'";}

			return $this->db->query("select 

									(select
									count(apresentacao) quantidade
									from checkout
									inner join pdv on id_pdv = fk_pdv
									inner join checkout_atendepn on id_atendepn = fk_atendepn
									where id_pdv > 0 and curva = 'A' and apresentacao = 'a' ".$where.") as qts_apresentacao_a,

									(select
									count(apresentacao) quantidade
									from checkout
									inner join pdv on id_pdv = fk_pdv
									inner join checkout_atendepn on id_atendepn = fk_atendepn
									where id_pdv > 0 and curva = 'B' and apresentacao = 'a' ".$where.") as qts_apresentacao_b,

									(select
									count(apresentacao) quantidade
									from checkout
									inner join pdv on id_pdv = fk_pdv
									inner join checkout_atendepn on id_atendepn = fk_atendepn
									where id_pdv > 0 and curva = 'C' and apresentacao = 'a' ".$where.") as qts_apresentacao_c,

									(select
									count(conhecimento) quantidade
									from checkout
									inner join pdv on id_pdv = fk_pdv
									inner join checkout_atendepn on id_atendepn = fk_atendepn
									where id_pdv > 0 and curva = 'A'  and conhecimento = 'a' ".$where.") as qts_conhecimento_a,

									(select
									count(conhecimento) quantidade
									from checkout
									inner join pdv on id_pdv = fk_pdv
									inner join checkout_atendepn on id_atendepn = fk_atendepn
									where id_pdv > 0 and curva = 'B' and conhecimento = 'a' ".$where.") as qts_conhecimento_b,

									(select
									count(conhecimento) quantidade
									from checkout
									inner join pdv on id_pdv = fk_pdv
									inner join checkout_atendepn on id_atendepn = fk_atendepn
									where id_pdv > 0 and curva = 'C' and conhecimento = 'a' ".$where.") as qts_conhecimento_c,

									(select
									count(book) quantidade
									from checkout
									inner join pdv on id_pdv = fk_pdv
									inner join checkout_atendepn on id_atendepn = fk_atendepn
									where id_pdv > 0 and curva = 'A' and book = 'a' ".$where.") as qts_book_a,

									(select
									count(book) quantidade
									from checkout
									inner join pdv on id_pdv = fk_pdv
									inner join checkout_atendepn on id_atendepn = fk_atendepn
									where id_pdv > 0 and curva = 'B' and book = 'a'  ".$where.") as qts_book_b,

									(select
									count(book) quantidade
									from checkout
									inner join pdv on id_pdv = fk_pdv
									inner join checkout_atendepn on id_atendepn = fk_atendepn
									where id_pdv > 0 and curva = 'C' and book = 'a'  ".$where.") as qts_book_c,







									(select
									count(apresentacao) quantidade
									from checkout
									inner join pdv on id_pdv = fk_pdv
									inner join checkout_atendepn on id_atendepn = fk_atendepn
									where id_pdv > 0 and curva = 'A' and apresentacao = 'p' ".$where.") as qts_apresentacao_a_p,

									(select
									count(apresentacao) quantidade
									from checkout
									inner join pdv on id_pdv = fk_pdv
									inner join checkout_atendepn on id_atendepn = fk_atendepn
									where id_pdv > 0 and curva = 'B' and apresentacao = 'p' ".$where.") as qts_apresentacao_b_p,

									(select
									count(apresentacao) quantidade
									from checkout
									inner join pdv on id_pdv = fk_pdv
									inner join checkout_atendepn on id_atendepn = fk_atendepn
									where id_pdv > 0 and curva = 'C' and apresentacao = 'p' ".$where.") as qts_apresentacao_c_p,

									(select
									count(conhecimento) quantidade
									from checkout
									inner join pdv on id_pdv = fk_pdv
									inner join checkout_atendepn on id_atendepn = fk_atendepn
									where id_pdv > 0 and curva = 'A'  and conhecimento = 'p' ".$where.") as qts_conhecimento_a_p,

									(select
									count(conhecimento) quantidade
									from checkout
									inner join pdv on id_pdv = fk_pdv
									inner join checkout_atendepn on id_atendepn = fk_atendepn
									where id_pdv > 0 and curva = 'B' and conhecimento = 'p' ".$where.") as qts_conhecimento_b_p,

									(select
									count(conhecimento) quantidade
									from checkout
									inner join pdv on id_pdv = fk_pdv
									inner join checkout_atendepn on id_atendepn = fk_atendepn
									where id_pdv > 0 and curva = 'C' and conhecimento = 'p' ".$where.") as qts_conhecimento_c_p,

									(select
									count(book) quantidade
									from checkout
									inner join pdv on id_pdv = fk_pdv
									inner join checkout_atendepn on id_atendepn = fk_atendepn
									where id_pdv > 0 and curva = 'A' and book = 'p' ".$where.") as qts_book_a_p,

									(select
									count(book) quantidade
									from checkout
									inner join pdv on id_pdv = fk_pdv
									inner join checkout_atendepn on id_atendepn = fk_atendepn
									where id_pdv > 0 and curva = 'B' and book = 'p' ".$where.") as qts_book_b_p,

									(select
									count(book) quantidade
									from checkout
									inner join pdv on id_pdv = fk_pdv
									inner join checkout_atendepn on id_atendepn = fk_atendepn
									where id_pdv > 0 and curva = 'C' and book = 'p' ".$where.") as qts_book_c_p,








									(select
									count(apresentacao) quantidade
									from checkout
									inner join pdv on id_pdv = fk_pdv
									inner join checkout_atendepn on id_atendepn = fk_atendepn
									where id_pdv > 0 and curva = 'A' and apresentacao = 'n' ".$where.") as qts_apresentacao_a_n,

									(select
									count(apresentacao) quantidade
									from checkout
									inner join pdv on id_pdv = fk_pdv
									inner join checkout_atendepn on id_atendepn = fk_atendepn
									where id_pdv > 0 and curva = 'B' and apresentacao = 'n' ".$where.") as qts_apresentacao_b_n,

									(select
									count(apresentacao) quantidade
									from checkout
									inner join pdv on id_pdv = fk_pdv
									inner join checkout_atendepn on id_atendepn = fk_atendepn
									where id_pdv > 0 and curva = 'C' and apresentacao = 'n' ".$where.") as qts_apresentacao_c_n,

									(select
									count(conhecimento) quantidade
									from checkout
									inner join pdv on id_pdv = fk_pdv
									inner join checkout_atendepn on id_atendepn = fk_atendepn
									where id_pdv > 0 and curva = 'A'  and conhecimento = 'n' ".$where.") as qts_conhecimento_a_n,

									(select
									count(conhecimento) quantidade
									from checkout
									inner join pdv on id_pdv = fk_pdv
									inner join checkout_atendepn on id_atendepn = fk_atendepn
									where id_pdv > 0 and curva = 'B' and conhecimento = 'n' ".$where.") as qts_conhecimento_b_n,

									(select
									count(conhecimento) quantidade
									from checkout
									inner join pdv on id_pdv = fk_pdv
									inner join checkout_atendepn on id_atendepn = fk_atendepn
									where id_pdv > 0 and curva = 'C' and conhecimento = 'n' ".$where.") as qts_conhecimento_c_n,

									(select
									count(book) quantidade
									from checkout
									inner join pdv on id_pdv = fk_pdv
									inner join checkout_atendepn on id_atendepn = fk_atendepn
									where id_pdv > 0 and curva = 'A' and book = 'n' ".$where.") as qts_book_a_n,

									(select
									count(book) quantidade
									from checkout
									inner join pdv on id_pdv = fk_pdv
									inner join checkout_atendepn on id_atendepn = fk_atendepn
									where id_pdv > 0 and curva = 'B' and book = 'n' ".$where.") as qts_book_b_n,

									(select
									count(book) quantidade
									from checkout
									inner join pdv on id_pdv = fk_pdv
									inner join checkout_atendepn on id_atendepn = fk_atendepn
									where id_pdv > 0 and curva = 'C' and book = 'n' ".$where.") as qts_book_c_n,

									(select count(*) as total from checkout inner join pdv on id_pdv = fk_pdv where id_pdv > 0 ".$where.") as total")->row();

		}

		################################# view_campanhas_relatorios #################################
		//Atualiza a tabela no momento que o relatório é aberto
		public function campanhas_relatorios(){

			$ultimacampanha = $this->db->query('select max(fk_campanha) ultimacampanha from dicionario_campanha')->row()->ultimacampanha;

			if($ultimacampanha == ""){
				$ultimacampanha = 0;
			}

			$campanha = $this->db->query('select id_campanha, campanha 
										from checkout_campanha
										where campanha is not null and id_campanha > '.$ultimacampanha)->result();

			$produto = $this->db->query('select id_campanha, produto 
										from checkout_campanha
										where produto is not null and id_campanha > '.$ultimacampanha)->result();

			foreach ($campanha as $value) {

				$palavras = explode(' ', str_replace('\\', '', $value->campanha));

				foreach ($palavras as $palavra) {
					$this->db->query('insert into dicionario_campanha (fk_campanha,tipo,palavra) values 
						('.$value->id_campanha.',\'C\',\''.$palavra.'\')');
				}

			}

			foreach ($produto as $value) {

				$palavras = explode(' ', str_replace('\\', '', $value->produto));

				foreach ($palavras as $palavra) {
					$this->db->query('insert into dicionario_campanha (fk_campanha,tipo,palavra) values 
						('.$value->id_campanha.',\'P\',\''.$palavra.'\')');
				}

			}

			return array(
					'campanhac' => $this->otmizarcodigo_campanha("where tipo = 'C' and id_operadora = 1"),
					'campanhao' => $this->otmizarcodigo_campanha("where tipo = 'C' and id_operadora = 2"),
					'campanhav' => $this->otmizarcodigo_campanha("where tipo = 'C' and id_operadora = 3"),
					'campanhat' => $this->otmizarcodigo_campanha("where tipo = 'C' and id_operadora = 4"),
					'produtoc' => $this->otmizarcodigo_campanha("where tipo = 'P' and id_operadora = 1"),
					'produtoo' => $this->otmizarcodigo_campanha("where tipo = 'P' and id_operadora = 2"),
					'produtov' => $this->otmizarcodigo_campanha("where tipo = 'P' and id_operadora = 3"),
					'produtot' => $this->otmizarcodigo_campanha("where tipo = 'P' and id_operadora = 4")
				);

		}

		public function campanha_ajax($cnpj = null,$de = null, $ate = null,$filial = null){

			$where = "";

			if($de != ""){ $where .= " and date(data_checkout) >= '".$this->fDatas($de)."'";}
			if($ate != ""){ $where .= " and date(data_checkout) <= '".$this->fDatas($ate)."'";}
			if($filial != ""){ $where .= " and filial = '".$filial."'";}
			if($cnpj != ""){ $where .= " and cnpj = '".$cnpj."'";}

			return array(
					'campanhac' => $this->otmizarcodigo_campanha("where tipo = 'C' and id_operadora = 1".$where),
					'campanhao' => $this->otmizarcodigo_campanha("where tipo = 'C' and id_operadora = 2".$where),
					'campanhav' => $this->otmizarcodigo_campanha("where tipo = 'C' and id_operadora = 3".$where),
					'campanhat' => $this->otmizarcodigo_campanha("where tipo = 'C' and id_operadora = 4".$where),
					'produtoc' => $this->otmizarcodigo_campanha("where tipo = 'P' and id_operadora = 1".$where),
					'produtoo' => $this->otmizarcodigo_campanha("where tipo = 'P' and id_operadora = 2".$where),
					'produtov' => $this->otmizarcodigo_campanha("where tipo = 'P' and id_operadora = 3".$where),
					'produtot' => $this->otmizarcodigo_campanha("where tipo = 'P' and id_operadora = 4".$where)
				);

		}

		public function otmizarcodigo_campanha($where = null){

			return $this->db->query("SELECT 
										count(palavra) quantidade,
										palavra
										FROM dicionario_campanha
										inner join checkout_campanha on id_campanha = fk_campanha
										inner join checkout on id_checkout = fk_checkout
										inner join operadoras on fk_operadora = id_operadora
										inner join pdv on id_pdv = fk_pdv
										{$where}
										and lower(palavra) not in 
										('de','para',' ','sem','e','não','tem','suas','por')
										group by palavra
										order by quantidade desc limit 5");

		}

		public function visitas_pdf ($de = null, $ate = null){

			$where = "";

			if($de != '') {
				$where .= ' and date(data_checkout) > \''.$this->fDatas($de).'\'';
			}

			if($ate != '') {
				$where .= ' and date(data_checkout) < \''.$this->fDatas($ate).'\'';
			}
			$pack = array();

			$pack['de'] = $de;
			$pack['ate'] = $ate;

			$pack['cinco_menores'] = $this->db->query('SELECT 
														distinct(rede) rede,
														count(*) visitas
														FROM checkout
														inner join pdv on id_pdv = fk_pdv
														where id_pdv > 0 '.$where.'
														group by rede
														order by visitas asc
														limit 5')->result();

			$pack['media_f1'] = $this->db->query('SELECT 
											round((count(*) / datediff(curdate(),min(data_checkout))),2) media_checkout,
											count(*) visitas,

											(select count(*)
													from checkout c0
													inner join pdv fcpdv on fcpdv.id_pdv = c0.fk_pdv 
													inner join usuarios u on id_usuario = c0.fk_usuario
													inner join grupo_usuarios gs on u.id_usuario = gs.fk_usuario
													where fk_pdv not in (select fk_pdv from carteira_gn cg
																where cg.fk_usuario = c0.fk_usuario) and fk_grupo = 2 and filial = 1 '.$where.') as fora_carteira

											from checkout 
											inner join pdv on id_pdv = fk_pdv
											inner join usuarios u on id_usuario = checkout.fk_usuario
											inner join grupo_usuarios gs on u.id_usuario = gs.fk_usuario
											where filial = 1 '.$where.' and fk_grupo = 2')->row();

			$pack['media_f2'] = $this->db->query('SELECT 
											round((count(*) / datediff(curdate(),min(data_checkout))),2) media_checkout,
											count(*) visitas,

											(select count(*)
													from checkout c0
													inner join pdv fcpdv on fcpdv.id_pdv = c0.fk_pdv 
													inner join usuarios u on id_usuario = c0.fk_usuario
													inner join grupo_usuarios gs on u.id_usuario = gs.fk_usuario
													where fk_pdv not in (select fk_pdv from carteira_gn cg
																where cg.fk_usuario = c0.fk_usuario) and fk_grupo = 2 and filial = 2 '.$where.') as fora_carteira

											from checkout 
											inner join pdv on id_pdv = fk_pdv
											inner join usuarios u on id_usuario = checkout.fk_usuario
											inner join grupo_usuarios gs on u.id_usuario = gs.fk_usuario
											where filial = 2 '.$where.' and fk_grupo = 2')->row();

			$pack['curvas_filiais'] = $this->db->query('select
								(select count(*)
										from checkout c0
										inner join pdv fcpdv on fcpdv.id_pdv = c0.fk_pdv 
										inner join usuarios u on id_usuario = c0.fk_usuario
										inner join grupo_usuarios gs on u.id_usuario = gs.fk_usuario
										where fk_pdv not in (select fk_pdv from carteira_gn cg
																where cg.fk_usuario = c0.fk_usuario) and fk_grupo = 2 and filial = 1 and curva = \'A\' '.$where.') as fora_carteira_A1,
								        
								(select count(*)
										from checkout c0
										inner join pdv fcpdv on fcpdv.id_pdv = c0.fk_pdv 
										inner join usuarios u on id_usuario = c0.fk_usuario
										inner join grupo_usuarios gs on u.id_usuario = gs.fk_usuario
										where fk_pdv not in (select fk_pdv from carteira_gn cg
																where cg.fk_usuario = c0.fk_usuario) and fk_grupo = 2 and filial = 2 and curva = \'A\' '.$where.') as fora_carteira_A2,
								        
								(select count(*)
										from checkout c0
										inner join pdv fcpdv on fcpdv.id_pdv = c0.fk_pdv 
										inner join usuarios u on id_usuario = c0.fk_usuario
										inner join grupo_usuarios gs on u.id_usuario = gs.fk_usuario
										where fk_pdv not in (select fk_pdv from carteira_gn cg
																where cg.fk_usuario = c0.fk_usuario) and fk_grupo = 2 and filial = 1 and curva = \'B\' '.$where.') as fora_carteira_B1,
								        
								(select count(*)
										from checkout c0
										inner join pdv fcpdv on fcpdv.id_pdv = c0.fk_pdv 
										inner join usuarios u on id_usuario = c0.fk_usuario
										inner join grupo_usuarios gs on u.id_usuario = gs.fk_usuario
										where fk_pdv not in (select fk_pdv from carteira_gn cg
																where cg.fk_usuario = c0.fk_usuario) and fk_grupo = 2 and filial = 2 and curva = \'B\' '.$where.') as fora_carteira_B2,
								        
								(select count(*)
										from checkout c0
										inner join pdv fcpdv on fcpdv.id_pdv = c0.fk_pdv 
										inner join usuarios u on id_usuario = c0.fk_usuario
										inner join grupo_usuarios gs on u.id_usuario = gs.fk_usuario
										where fk_pdv not in (select fk_pdv from carteira_gn cg
																where cg.fk_usuario = c0.fk_usuario) and fk_grupo = 2 and filial = 1 and curva = \'C\' '.$where.') as fora_carteira_C1,
								        
								(select count(*)
										from checkout c0
										inner join pdv fcpdv on fcpdv.id_pdv = c0.fk_pdv 
										inner join usuarios u on id_usuario = c0.fk_usuario
										inner join grupo_usuarios gs on u.id_usuario = gs.fk_usuario
										where fk_pdv not in (select fk_pdv from carteira_gn cg
																where cg.fk_usuario = c0.fk_usuario) and fk_grupo = 2 and filial = 2 and curva = \'C\' '.$where.') as fora_carteira_C2

								')->row();

		$pack['lista_f1'] = $this->db->query('select

						distinct(usuario) usuario,

						(select count(*) from carteira_gn where fk_usuario = id_usuario) total,

						count(*) visitas,



						((select count(*) from carteira_gn where fk_usuario = id_usuario) 
								-
						 (select count(distinct(fk_pdv)) from checkout c0 where fk_pdv in 
														(select fk_pdv from carteira_gn cg
																where cg.fk_usuario = c0.fk_usuario) and fk_usuario = id_usuario)
						) n_visitados,



						
						((select count(*) from carteira_gn
						inner join pdv on id_pdv = fk_pdv
						where fk_usuario = id_usuario and curva = \'A\') 
								-
						 (select count(distinct(fk_pdv)) from 
						 checkout c0 
						 inner join pdv on id_pdv = fk_pdv
						 where fk_pdv in 
							(select fk_pdv from carteira_gn cg
									inner join pdv on id_pdv = fk_pdv
									where cg.fk_usuario = c0.fk_usuario) and fk_usuario = id_usuario and curva = \'A\')
							and curva = \'A\'
						) n_curva_a, 

						((select count(*) from carteira_gn
						inner join pdv on id_pdv = fk_pdv
						where fk_usuario = id_usuario and curva = \'B\') 
								-
						 (select count(distinct(fk_pdv)) from 
						 checkout c0 
						 inner join pdv on id_pdv = fk_pdv
						 where fk_pdv in 
							(select fk_pdv from carteira_gn cg
									inner join pdv on id_pdv = fk_pdv
									where cg.fk_usuario = c0.fk_usuario) and fk_usuario = id_usuario and curva = \'B\')
							and curva = \'B\'
						)n_curva_b, 

						((select count(*) from carteira_gn
						inner join pdv on id_pdv = fk_pdv
						where fk_usuario = id_usuario and curva = \'C\') 
								-
						 (select count(distinct(fk_pdv)) from 
						 checkout c0 
						 inner join pdv on id_pdv = fk_pdv
						 where fk_pdv in 
							(select fk_pdv from carteira_gn cg
									inner join pdv on id_pdv = fk_pdv
									where cg.fk_usuario = c0.fk_usuario) and fk_usuario = id_usuario and curva = \'C\')
							and curva = \'C\'
						)n_curva_c, 

						round((count(*) / datediff(curdate(),min(data_checkout))),2) media_checkout,

						(select count(distinct(fk_pdv))
							from checkout c0
							inner join pdv fcpdv on fcpdv.id_pdv = c0.fk_pdv 
							inner join usuarios u on id_usuario = c0.fk_usuario
							inner join grupo_usuarios gs on u.id_usuario = gs.fk_usuario
							where fk_pdv not in (select fk_pdv from carteira_gn cg
														where cg.fk_usuario = c0.fk_usuario) and fk_grupo = 2 and c0.fk_usuario = cout.fk_usuario '.$where.') as fora_carteira

						from checkout cout
						inner join usuarios us on id_usuario = fk_usuario
						inner join pdv on id_pdv = fk_pdv
						where filial = 1 and filial_responsavel = 1 '.$where.'
						group by usuario;')->result();

		$pack['lista_f2'] = $this->db->query('select

						distinct(usuario) usuario,

						(select count(*) from carteira_gn where fk_usuario = id_usuario) total,

						count(*) visitas,



						((select count(*) from carteira_gn where fk_usuario = id_usuario) 
								-
						 (select count(distinct(fk_pdv)) from checkout c0 where fk_pdv in 
														(select fk_pdv from carteira_gn cg
																where cg.fk_usuario = c0.fk_usuario) and fk_usuario = id_usuario)
						) n_visitados,



						
						((select count(*) from carteira_gn
						inner join pdv on id_pdv = fk_pdv
						where fk_usuario = id_usuario and curva = \'A\') 
								-
						 (select count(distinct(fk_pdv)) from 
						 checkout c0 
						 inner join pdv on id_pdv = fk_pdv
						 where fk_pdv in 
							(select fk_pdv from carteira_gn cg
									inner join pdv on id_pdv = fk_pdv
									where cg.fk_usuario = c0.fk_usuario) and fk_usuario = id_usuario and curva = \'A\')
							and curva = \'A\'
						) n_curva_a, 

						((select count(*) from carteira_gn
						inner join pdv on id_pdv = fk_pdv
						where fk_usuario = id_usuario and curva = \'B\') 
								-
						 (select count(distinct(fk_pdv)) from 
						 checkout c0 
						 inner join pdv on id_pdv = fk_pdv
						 where fk_pdv in 
							(select fk_pdv from carteira_gn cg
									inner join pdv on id_pdv = fk_pdv
									where cg.fk_usuario = c0.fk_usuario) and fk_usuario = id_usuario and curva = \'B\')
							and curva = \'B\'
						)n_curva_b, 

						((select count(*) from carteira_gn
						inner join pdv on id_pdv = fk_pdv
						where fk_usuario = id_usuario and curva = \'C\') 
								-
						 (select count(distinct(fk_pdv)) from 
						 checkout c0 
						 inner join pdv on id_pdv = fk_pdv
						 where fk_pdv in 
							(select fk_pdv from carteira_gn cg
									inner join pdv on id_pdv = fk_pdv
									where cg.fk_usuario = c0.fk_usuario) and fk_usuario = id_usuario and curva = \'C\')
							and curva = \'C\'
						)n_curva_c, 

						round((count(*) / datediff(curdate(),min(data_checkout))),2) media_checkout,

						(select count(distinct(fk_pdv))
							from checkout c0
							inner join pdv fcpdv on fcpdv.id_pdv = c0.fk_pdv 
							inner join usuarios u on id_usuario = c0.fk_usuario
							inner join grupo_usuarios gs on u.id_usuario = gs.fk_usuario
							where fk_pdv not in (select fk_pdv from carteira_gn cg
														where cg.fk_usuario = c0.fk_usuario) and fk_grupo = 2 and c0.fk_usuario = cout.fk_usuario '.$where.') as fora_carteira

						from checkout cout
						inner join usuarios us on id_usuario = fk_usuario
						inner join pdv on id_pdv = fk_pdv
						where filial = 2 and filial_responsavel = 2 '.$where.'
						group by usuario;')->result();

			$pack['analitico_pdvs_visitados'] = $this->db->query('
									SELECT 
									rede,cnpj, curva, lougradouro, bairro, usuario, id_usuario, filial_responsavel
									from checkout 
									left join pdv on fk_pdv = id_pdv
									left join usuarios on id_usuario = checkout.fk_usuario
									inner join grupo_usuarios on id_usuario = grupo_usuarios.fk_usuario
									where fk_grupo = 2 '.$where.' order by id_usuario ')->result();



			return $pack;


		}

		public function horas_pdf($de = null, $ate = null){

			$where = " where id_checkout > 0";

			if($de != '') {
				$where .= ' and date(data_checkout) >= \''.$this->fDatas($de).'\'';
			}

			if($ate != '') {
				$where .= ' and date(data_checkout) <= \''.$this->fDatas($ate).'\'';
			}

			//Dados Gerais,
			$dados = $this->db->query('select 
					distinct(date(data_checkout)) as dia,
					usuario,
					(select min(time(data_checkout)) as entrou 
					from checkout ch '.$where.'  and fk_usuario = id_usuario) as entrou,
					(select max(time(data_checkout)) as saiu 
					from checkout ch '.$where.'  and fk_usuario = id_usuario) as saiu,

					(select TIMEDIFF(

								(select max(time(data_checkout)) as saiu 
								from checkout ch '.$where.'  and fk_usuario = id_usuario),
								
								(select min(time(data_checkout)) as entrou 
								from checkout ch '.$where.'  and fk_usuario = id_usuario)
								
							)
					) as tempo

					from checkout as co
					inner join usuarios on id_usuario = fk_usuario

					inner join pdv on id_pdv = fk_pdv
					'.$where)->result();

				$horas = Array();

				foreach ($dados as $key => $entrou) {
					$horas[$key] = strtotime($entrou->entrou);
				}

				if (count($horas) > 0) {
					$entrou_media = date("H:i:s", $this->zero_visitaspdvc(array_sum($horas),count($horas)));
				} else {
					$entrou_media = 0;
				}

				$horas = Array();

				foreach ($dados as $key => $saiu) {
					$horas[$key] = strtotime($saiu->saiu);
				}

				if (count($horas) > 0) {
					$saiu_media = date("H:i:s", $this->zero_visitaspdvc(array_sum($horas),count($horas)));
				} else {
					$saiu_media = 0;
				}

				$horas = Array();

				foreach ($dados as $key => $tempo) {
					$horas[$key] = strtotime($tempo->tempo);
				}

				if (count($horas) > 0) {
					$tempo_medio = date("H:i:s", $this->zero_visitaspdvc(array_sum($horas),count($horas)));
				} else {
					$tempo_medio = 0;
				}

				//Média Filial 1
				$dados = $this->db->query('select 
					distinct(date(data_checkout)) as dia,
					usuario,
					count(*) checkouts_f1,
					(select min(time(data_checkout)) as entrou 
					from checkout ch '.$where.' and fk_usuario = id_usuario) as entrou,
					(select max(time(data_checkout)) as saiu 
					from checkout ch '.$where.'  and fk_usuario = id_usuario) as saiu,

					timediff((select TIMEDIFF(

								(select max(time(data_checkout)) as saiu 
								from checkout ch '.$where.' and fk_usuario = id_usuario),
								
								(select min(time(data_checkout)) as entrou 
								from checkout ch '.$where.' and fk_usuario = id_usuario)
								
							)
					),\'08:00:00\') def_f1,

					(select TIMEDIFF(

								(select max(time(data_checkout)) as saiu 
								from checkout ch '.$where.' and fk_usuario = id_usuario),
								
								(select min(time(data_checkout)) as entrou 
								from checkout ch '.$where.' and fk_usuario = id_usuario)
								
							)
					) as tempo

					from checkout as co
					inner join usuarios u on id_usuario = co.fk_usuario
					inner join grupo_usuarios gs on u.id_usuario = gs.fk_usuario
					inner join pdv on id_pdv = fk_pdv
					'.$where.' and filial = 1 and fk_grupo = 2')->result();

				$horas = Array();

				foreach ($dados as $key => $entrou) {
					$horas[$key] = strtotime($entrou->entrou);
					$checkouts_f1 = $entrou->checkouts_f1;
				}

				if (count($horas) > 0) {
					$entrou_mediaf1 = date("H:i:s", $this->zero_visitaspdvc(array_sum($horas),count($horas)));
				} else {
					$entrou_mediaf1 = 0;
				}

				$horas = Array();

				foreach ($dados as $key => $saiu) {
					$horas[$key] = strtotime($saiu->saiu);
				}

				if (count($horas) > 0) {
					$saiu_mediaf1 = date("H:i:s", $this->zero_visitaspdvc(array_sum($horas),count($horas)));
				} else {
					$saiu_mediaf1 = 0;
				}

				$horas = Array();

				foreach ($dados as $key => $tempo) {
					$horas[$key] = strtotime($tempo->tempo);
				}

				if (count($horas) > 0) {
					$tempo_medio_f1 = date("H:i:s", $this->zero_visitaspdvc(array_sum($horas),count($horas)));
				} else {
					$tempo_medio_f1 = 0;
				}

				$def_f1 = $this->db->query('select TIMEDIFF(\'08:00:00\',\''.$tempo_medio_f1.'\') as tempo')->row()->tempo;


				//Média Filial 2
				$dados = $this->db->query('select 
					distinct(date(data_checkout)) as dia,
					usuario,
					count(*) checkouts_f2,
					(select min(time(data_checkout)) as entrou 
					from checkout ch '.$where.'  and fk_usuario = id_usuario) as entrou,
					(select max(time(data_checkout)) as saiu 
					from checkout ch '.$where.'  and fk_usuario = id_usuario) as saiu,

					timediff((select TIMEDIFF(

								(select max(time(data_checkout)) as saiu 
								from checkout ch '.$where.'  and fk_usuario = id_usuario),
								
								(select min(time(data_checkout)) as entrou 
								from checkout ch '.$where.'  and fk_usuario = id_usuario)
								
							)
					),\'08:00:00\') def_f2,

					(select TIMEDIFF(

								(select max(time(data_checkout)) as saiu 
								from checkout ch '.$where.'  and fk_usuario = id_usuario),
								
								(select min(time(data_checkout)) as entrou 
								from checkout ch '.$where.'  and fk_usuario = id_usuario)
								
							)
					) as tempo

					from checkout as co
					inner join usuarios u on id_usuario = co.fk_usuario
					inner join grupo_usuarios gs on u.id_usuario = gs.fk_usuario
					inner join pdv on id_pdv = fk_pdv
					'.$where.' and filial = 2 and fk_grupo = 2')->result();

				$horas = Array();

				

				foreach ($dados as $key => $entrou) {
					$horas[$key] = strtotime($entrou->entrou);
					$checkouts_f2 = $entrou->checkouts_f2;
				}

				if (count($horas) > 0) {
					$entrou_mediaf2 = date("H:i:s", $this->zero_visitaspdvc(array_sum($horas),count($horas)));
				} else {
					$entrou_mediaf2 = 0;
				}

				$horas = Array();

				foreach ($dados as $key => $saiu) {
					$horas[$key] = strtotime($saiu->saiu);
				}

				if (count($horas) > 0) {
					$saiu_mediaf2 = date("H:i:s", $this->zero_visitaspdvc(array_sum($horas),count($horas)));
				} else {
					$saiu_mediaf2 = 0;
				}

				$horas = Array();

				foreach ($dados as $key => $tempo) {
					$horas[$key] = strtotime($tempo->tempo);
				}

				if (count($horas) > 0) {
					$tempo_medio_f2 = date("H:i:s", $this->zero_visitaspdvc(array_sum($horas),count($horas)));
				} else {
					$tempo_medio_f2 = 0;
				}

				$def_f2 = $this->db->query('select TIMEDIFF(\'08:00:00\',\''.$tempo_medio_f2.'\') as tempo')->row()->tempo;

				$def = array ('1' => strtotime($def_f1), '2' => strtotime($def_f2));

				$def_media = date("H:i:s", array_sum($def)/2);



				$gns_f1 = $this->db->query('select 

										distinct(usuario) usuario,

										(select min(time(data_checkout)) as entrou 
										from checkout ch '.$where.' and fk_usuario = id_usuario) as entrou,

										(select max(time(data_checkout)) as saiu 
										from checkout ch '.$where.' and fk_usuario = id_usuario) as saiu,


										(select TIMEDIFF(

												(select max(time(data_checkout)) as saiu 
												from checkout ch '.$where.' and fk_usuario = id_usuario),
												
												(select min(time(data_checkout)) as entrou 
												from checkout ch '.$where.' and fk_usuario = id_usuario)
												
											)
										) as tempo_trab ,

										(select TIMEDIFF(\'08:00:00\',
											(select TIMEDIFF(

													(select max(time(data_checkout)) as saiu 
													from checkout ch '.$where.' and fk_usuario = id_usuario),
													
													(select min(time(data_checkout)) as entrou 
													from checkout ch '.$where.' and fk_usuario = id_usuario)
													
												)
											)
										)) as tempo_def,


										count(*) checkouts 
										from checkout cout
										inner join pdv on id_pdv = cout.fk_pdv
										inner join usuarios u on id_usuario = fk_usuario
										inner join grupo_usuarios gu on gu.fk_usuario = cout.fk_usuario
										'.$where.'
										and filial = 1 
										and filial_responsavel = 1 
										and ativo = 1
										and fk_grupo = 2
										group by usuario')->result();

				$gns_f2 = $this->db->query('select 

										distinct(usuario) usuario,

										(select min(time(data_checkout)) as entrou 
										from checkout ch '.$where.' and fk_usuario = id_usuario) as entrou,

										(select max(time(data_checkout)) as saiu 
										from checkout ch '.$where.' and fk_usuario = id_usuario) as saiu,


										(select TIMEDIFF(

												(select max(time(data_checkout)) as saiu 
												from checkout ch '.$where.'  and fk_usuario = id_usuario),
												
												(select min(time(data_checkout)) as entrou 
												from checkout ch '.$where.'  and fk_usuario = id_usuario)
												
											)
										) as tempo_trab,


										(select TIMEDIFF(\'08:00:00\',
											(select TIMEDIFF(

													(select max(time(data_checkout)) as saiu 
													from checkout ch '.$where.' and fk_usuario = id_usuario),
													
													(select min(time(data_checkout)) as entrou 
													from checkout ch '.$where.' and fk_usuario = id_usuario)
													
												)
											)
										)) as tempo_def ,
										

										count(*) checkouts
										from checkout cout
										inner join pdv on id_pdv = cout.fk_pdv
										inner join usuarios u on id_usuario = fk_usuario
										inner join grupo_usuarios gu on gu.fk_usuario = cout.fk_usuario
										'.$where.'
										and filial = 2 
										and filial_responsavel = 2 
										and ativo = 1
										and fk_grupo = 2 
										group by usuario')->result();

				return array(

					'entrou_media' => $entrou_media,
					'saiu_media' => $saiu_media,
					'tempo_medio' => $tempo_medio,
					'tempo_medio_f1' => $tempo_medio_f1,
					'tempo_medio_f2' => $tempo_medio_f2,
					'de' => $de,
					'ate' => $ate,
					'def_f1' => $def_f1,
					'def_f2' => $def_f2,
					'def_media' => $def_media,
					'entrou_mediaf1' => $entrou_mediaf1,
					'saiu_mediaf1' => $saiu_mediaf1,
					'entrou_mediaf2' => $entrou_mediaf2,
					'saiu_mediaf2' =>$saiu_mediaf2 ,
					'checkouts_f1' => $checkouts_f1,
					'checkouts_f2' => $checkouts_f2,
					'gns_f1' => $gns_f1,
					'gns_f2' => $gns_f2

				);

		}


		################################# FORMATAR DATAS #################################

		public function fDatas($data = null){
			if($data != ""){
				$data2 = explode('/', $data);
				$dia = $data2 [0];
				$mes = $data2 [1];
				$ano = $data2 [2];
				$nova = $ano.'-'.$mes.'-'.$dia;
				return $nova;
			} else {
				return "";
			}
			
		}


	}