<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class model_checkout extends CI_Model {

		public function listarMarcas(){
			return $this->db->get('marcas_aparelhos')->result();
		}

		public function listarRedes(){
			return $this->db->get('lista_redes')->result();
		}

		public function ajax_Modelos($fk_marca) {
			return $this->db->query('SELECT id_modelo, modelo FROM modelo_aparelhos where fk_marca = '.$fk_marca.';')->result();
		}

		//Traz o último registro do mês vigente, para o PDV pelo usuário.
		public function dadosEsteMes($pdv = null, $usuario = null){
			
			$resultado = $this->db->query("select ca.*, cf.faturamento_pdv, cf.faturamento_setor  from checkout
				inner join checkout_atendimento ca on id_atendimento = fk_atendimento
				inner join checkout_faturamento cf on id_faturamento = fk_faturamento
				where MONTH(data_checkout) = MONTH(now()) and
				fk_pdv = ".$pdv." and fk_usuario = ".$usuario."
				order by data_checkout desc limit 1;");

			return array (
				'dados' => $resultado,
				'registros' => $resultado->num_rows()
				);

		}

		//Traz o último registro da semana vigente, para o PDV pelo usuário.
		public function dadosEstaSemana($pdv = null, $usuario = null){

			$resultado = $this->db->query("select cf.va_f1,cf.va_f2,cf.va_f3,cf.va_f4,cf.va_f5,cf.va_f6, cp.promotor,
					cp.tempo_pdv,cp.agencia,cp.supervisor  from checkout
					inner join checkout_promotor cp on id_promotor = fk_promotor
					inner join checkout_faturamento cf on id_faturamento = fk_faturamento
					where week(data_checkout) = week(now()) and
					fk_pdv = ".$pdv." and fk_usuario = ".$usuario."
					order by data_checkout desc limit 1;");

			return array (
				'dados' => $resultado,
				'registros' => $resultado->num_rows()
				);

		}

		public function enviar_diretamente($fk_pdv = null) {

			return $this->db->query("select envio_direto, rede from pdv where id_pdv = ".$fk_pdv);

		}

		//Se a data do checkout for 0 ele não foi finalizado
		public function continuar($fk_pdv = null,$fk_usuario = null) {
			$salvo =  $this->db->query('SELECT * FROM checkout 
									where fk_pdv = '.$fk_pdv.' 
									and fk_usuario = '.$fk_usuario.'
									and data_checkout = 0;');

			return $salvo;
		}

		public function pdvs_nlocalizados() {
			return $this->db->query('SELECT pdv_revisar.*, data_checkout, nome FROM pdv_revisar inner join checkout on fk_checkout = id_checkout
				inner join usuarios on fk_usuario = id_usuario;')->result();
		}

		public function pdv_revisar($dados = null) {
			return $this->db->insert('pdv_revisar',$dados);
		}

		public function atendimentoSalvo($fk_atendimento = null) {
			return $this->db->get_where('checkout_atendimento', array ('id_atendimento' => $fk_atendimento));
		}

		public function faturamentoSalvo($fk_faturamento = null) {
			return $this->db->get_where('checkout_faturamento', array ('id_faturamento' => $fk_faturamento));
		}

		public function promotorSalvo($fk_promotor = null) {
			return $this->db->get_where('checkout_promotor', array ('id_promotor' => $fk_promotor));
		}

		public function atendepnSalvo($fk_atendepn = null) {
			return $this->db->get_where('checkout_atendepn', array ('id_atendepn' => $fk_atendepn));
		}

		public function informacaovendasSalvo($fk_checkout = null) {
			return $this->db->get_where('checkout_informacao_vendas', array ('fk_checkout' => $fk_checkout));
		}

		public function maisvendidosSalvo($fk_checkout = null) {
			$this->db->order_by("id_mais_vendidos", "asc"); 
			return $this->db->get_where('checkout_mais_vendidos', array ('fk_checkout' => $fk_checkout));
		}

		public function parttimeSalvo($fk_checkout = null) {
			$this->db->order_by("fk_operadora", "asc"); 
			return $this->db->get_where('checkout_part_time', array ('fk_checkout' => $fk_checkout));
		}

		public function campanhaSalvo($fk_checkout = null) {
			$this->db->order_by("fk_operadora", "asc"); 
			return $this->db->get_where('checkout_campanha', array ('fk_checkout' => $fk_checkout));
		}

		public function marketingSalvo($fk_checkout = null) {
			$this->db->order_by("fk_operadora", "asc"); 
			return $this->db->get_where('checkout_marketing', array ('fk_checkout' => $fk_checkout));
		}

		public function filtrarPor($usuario = null, $de = null, $ate = null) {

			$sql = 'SELECT id_checkout,rede,usuario,data_checkout,endereco_exato 
					FROM checkout left join pdv on id_pdv = fk_pdv
					left join usuarios on id_usuario = fk_usuario';

			$where = "where data_checkout > 0";

			if($usuario != '') {
				$where .= ' and fk_usuario = '.$usuario;
			}

			if($de != '') {
				$data = explode('/', $de);
				$dia = $data [0];
				$mes = $data [1];
				$ano = $data [2];
				$de = $ano.'-'.$mes.'-'.$dia.' 00:00:00';
				$where .= ' and data_checkout > \''.$de.'\'';
			}

			if($ate != '') {
				$data = explode('/', $ate);
				$dia = $data [0];
				$mes = $data [1];
				$ano = $data [2];
				$ate = $ano.'-'.$mes.'-'.$dia.' 23:59:59';
				$where .= ' and data_checkout < \''.$ate.'\'';
			}

			//echo $sql.' '.$where;

			return $this->db->query($sql.' '.$where)->result();

		}

		//GRID relatório
		public function relatorio_checkout() {

			try {

				return array('checkout' => $this->db->query('SELECT id_checkout,rede,usuario,data_checkout,endereco_exato 
					FROM checkout left join pdv on id_pdv = fk_pdv
					left join usuarios on id_usuario = fk_usuario
					where data_checkout > 0;')->result(),

					'usuarios' => $this->db->query('select usuario from usuarios inner join grupo_usuarios on id_usuario = fk_usuario where fk_grupo = 2 order by usuario asc')->result()
				);
				
			} catch (Exception $e) {

				echo 'Falha ao listar os inputs: '.$e;
				
			}


		}

		//Selects
		public function getCheckout($usuario = null, $de = null, $ate = null){
			$sql = '
					select id_checkout, rede, usuario, atendimento.*, faturamento.*, promotor.*, atendepn.*, data_checkout, endereco_exato, filial, cod_agente, observacoes from checkout
					inner join pdv on id_pdv = fk_pdv
					inner join usuarios on id_usuario = fk_usuario
					left join checkout_atendimento atendimento on id_atendimento = fk_atendimento
					left join checkout_faturamento faturamento on id_faturamento = fk_faturamento
					left join checkout_promotor promotor on id_promotor = fk_promotor
					left join checkout_atendepn atendepn on id_atendepn = fk_atendepn
					';

			$where = "where data_checkout > 0";

			if($usuario != '') {
				$where .= ' and fk_usuario = '.$usuario;
			}

			if($de != '') {
				$data = explode('/', $de);
				$dia = $data [0];
				$mes = $data [1];
				$ano = $data [2];
				$de = $ano.'-'.$mes.'-'.$dia.' 00:00:00';
				$where .= ' and data_checkout > \''.$de.'\'';
			}

			if($ate != '') {
				$data = explode('/', $ate);
				$dia = $data [0];
				$mes = $data [1];
				$ano = $data [2];
				$ate = $ano.'-'.$mes.'-'.$dia.' 23:59:59';
				$where .= ' and data_checkout < \''.$ate.'\'';
			}

			//echo $sql.' '.$where;

			return $this->db->query($sql.' '.$where)->result();


		}

		public function getCheckoutDireto(){
			return $this->db->query('
					select id_checkout, rede, usuario,data_checkout, endereco_exato from checkout
					inner join pdv on id_pdv = fk_pdv
					inner join usuarios on id_usuario = fk_usuario
					where envio_direto = 1;
					')->result();
		}

		public function get_informacao_vendas($id = null) {
			return $this->db->query("SELECT * FROM checkout_informacao_vendas
			where fk_checkout = {$id} order by tipo desc, fk_operadora asc")->result();
		}

		public function get_mais_vendidos($id = null){
			return $this->db->query("SELECT marca, modelo, valor, media FROM checkout_mais_vendidos 
				left join marcas_aparelhos on fk_marca = id_marca
				left join modelo_aparelhos on fk_modelo = id_modelo
				where fk_checkout = {$id};")->result();
		}

		public function get_part_time($id = null) {
			$this->db->order_by("fk_operadora", "asc"); 
			return $this->db->query("SELECT * FROM checkout_part_time where fk_checkout = {$id} order by fk_operadora;")->result();
		}

		public function get_campanha($id = null) {
			$this->db->order_by("fk_operadora", "asc"); 
			return $this->db->query("SELECT * FROM checkout_campanha where fk_checkout = {$id} order by fk_operadora;")->result();
		}

		public function get_marketing($id = null) {
			$this->db->order_by("fk_operadora", "asc"); 
			return $this->db->query("SELECT * FROM checkout_marketing where fk_checkout = {$id} order by fk_operadora;")->result();
		}

		//Inserts
		public function checkout_atendimento($dados = null) {

			$this->db->insert('checkout_atendimento',$dados);
			return $this->db->insert_id();

		}


		public function checkout_faturamento($dados = null) {

			$this->db->insert('checkout_faturamento',$dados);
			return $this->db->insert_id();

		}


		public function checkout_promotor($dados = null) {

			$this->db->insert('checkout_promotor',$dados);
			return $this->db->insert_id();

		}


		public function checkout_atendepn($dados = null) {

			$this->db->insert('checkout_atendepn',$dados);
			return $this->db->insert_id();

		}
		
		public function checkout($dados = null) {

			$this->db->insert('checkout',$dados);
			return $this->db->insert_id();

		}

		public function checkout_informacao_vendas($dados = null) {

			$this->db->insert_batch('checkout_informacao_vendas',$dados);
			return $this->db->insert_id();

		}

		public function checkout_mais_vendidos($dados = null) {

			$this->db->insert_batch('checkout_mais_vendidos',$dados);
			return $this->db->insert_id();

		}

		public function checkout_part_time($dados = null) {

			$this->db->insert_batch('checkout_part_time',$dados);
			return $this->db->insert_id();

		}

		public function checkout_campanha($dados = null) {

			$this->db->insert_batch('checkout_campanha',$dados);
			return $this->db->insert_id();

		}

		public function checkout_marketing($dados = null) {

			$this->db->insert_batch('checkout_marketing',$dados);
			return $this->db->insert_id();

		}

		public function deletarSalvos($id_checkout = null) {

			$checkout = $this->db->get('checkout',array ('id_checkout' => $id_checkout));

			$this->db->delete('checkout_informacao_vendas',array('fk_checkout' => $id_checkout));
			$this->db->delete('checkout_mais_vendidos',array('fk_checkout' => $id_checkout));
			$this->db->delete('checkout_part_time',array('fk_checkout' => $id_checkout));
			$this->db->delete('checkout_campanha',array('fk_checkout' => $id_checkout));
			$this->db->delete('checkout_marketing',array('fk_checkout' => $id_checkout));
			$this->db->delete('checkout',array('id_checkout' => $id_checkout));

			$this->db->delete('checkout_atendimento',array('id_atendimento' => $checkout->row()->fk_atendimento));
			$this->db->delete('checkout_faturamento',array('id_faturamento' => $checkout->row()->fk_faturamento));
			$this->db->delete('checkout_promotor',array('id_promotor' => $checkout->row()->fk_promotor));
			$this->db->delete('checkout_atendepn',array('id_atendepn' => $checkout->row()->fk_atendepn));



		}

		
	}