<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class model_carteira extends CI_Model {

		public function listar_carteira() {

			return $this->db->query('
				SELECT id_usuario, 
				nome, 
		        filial_responsavel,
		        (select count(*) from carteira_gn where fk_usuario = id_usuario) total
		        from usuarios 
		        inner join grupo_usuarios on id_usuario = fk_usuario
		        where filial_responsavel > 0 and ativo = 1 and fk_grupo = 2
		        order by filial_responsavel')->result();

		}

		public function associar($dados = null) {
	
			return $this->db->query("insert into carteira_gn (fk_usuario,fk_pdv) values(".$dados['fk_usuario'].",".$dados['fk_pdv'].");");	
			

		}

		public function listar_por_cnpj($cnpj = null) {

			return $this->db->query("SELECT id_usuario, 
										nome, 
								        filial_responsavel,
								        (select count(*) from carteira_gn where fk_usuario = id_usuario) total
								        from usuarios 
								        inner join grupo_usuarios gs on id_usuario = gs.fk_usuario
								        inner join carteira_gn cg on id_usuario = cg.fk_usuario
								        inner join pdv on id_pdv = fk_pdv
								        where filial_responsavel > 0 and ativo = 1 and fk_grupo = 2
								        and cnpj = '".$cnpj."'
								        order by filial_responsavel")->result();


		}

		public function crud_carteira($where = null) { //Array

			try {

				$pack = array (

					'usuario' => $this->db->query('select id_usuario, usuario from usuarios 
										inner join grupo_usuarios on id_usuario = fk_usuario 
										where id_usuario = '.$where[0].''),

					'cnpj' => $this->db->query('select 
							id_pdv, 
							cnpj, 
							rede 
							from pdv 
							where id_pdv not in (select fk_pdv from carteira_gn 
											where fk_usuario = '.$where[0].')')->result(),

					'pdvs' => $this->db->query("select rede, INSERT(INSERT(INSERT(INSERT(cnpj,13,0,'-'),9,0,'/'),6,0,'.'),3,0,'.') as cnpj, id_pdv from pdv 
						where id_pdv in (select fk_pdv from carteira_gn 
											where fk_usuario = ".$where[0].")")->result()

				);

				return $pack;
				
			} catch (Exception $e) {

				echo 'Falha ao carregar usuário: '.$e;
				
			}

		}

		public function deletar_vinculo($id_pdv = null,$id_usuario = null) {

			return $this->db->query('DELETE from carteira_gn
										where fk_usuario = '.$id_usuario.' 
												and fk_pdv = '.$id_pdv);

		}


	}