<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class model_seguranca extends CI_Model {

		public function validar($login = null) { //Valida o login no inicio.

			try {

				$ativo = $this->db->query("select * from usuarios inner join grupo_usuarios 
				on fk_usuario = id_usuario
				where ativo = 1 and fk_grupo = 1 and usuario = '".$login['usuario']."' and senha = '".$login['senha']."';");

				/*Se existir um usuário com essa senha*/
				if($ativo->num_rows() == 1) { return $ativo; } else { return false; }
				
			} catch (Exception $e) {

				echo 'Falha ao validar login: '.$e;
				
			}

		}


		public function acesso($tela = null) { //Valida se o usuário tem permissão de acesso a tela.

			try {

				return $this->db->query('select count(U.usuario) permissao from usuarios U inner join grupo_usuarios GU on U.id_usuario = GU.fk_usuario
				inner join grupo_aplicacoes GA on GA.fk_grupo = GU.fk_grupo
				inner join aplicacoes A on A.id_aplicacao = GA.fk_aplicacao
				where A.aplicacao = \''.$tela.'\' and U.id_usuario = '.$this->session->userdata('id_usuario'))->row()->permissao;
				
			} catch (Exception $e) {

				echo 'Falha ao validar acesso: '.$e;
				
			}

		}

		public function titulo($tela = null) { //Carrega o título da aplicação.

			try {
			
				return $this->db->query('select titulo_aplicacao from aplicacoes where aplicacao = \''.$tela.'\';')->row();

			} catch (Exception $e) {
				
				echo 'Falha ao consultar título: '.$e;

			}	

		}

		public function senha_Email($senha = null,$usuario = null) {

			$email = $this->db->query('select email from usuarios where usuario = \''.$usuario.'\' and ativo = 1;');

			try {

				if($email->num_rows() > 0) {

				$this->db->query('update usuarios set senha = \''.$senha.'\' where usuario = \''.$usuario.'\';');

				return $email->row()->email;

				} 
				
			} catch (Exception $e) {

				return "";
				
			}

		}

		public function senha_Usuario($senha = null,$usuario = null) {

			$email = $this->db->query('select * from usuarios where usuario = \''.$usuario.'\' and ativo = 1;');

			try {

				if($email->num_rows() > 0) {

				 $this->db->query('update usuarios set senha = \''.$senha.'\' where usuario = \''.$usuario.'\';');

				return $email->row()->email;

				} else {

					return false;

				}
				
			} catch (Exception $e) {

				echo "Erro: ".$e;
				return false;
				
			}

		}

	}

?>