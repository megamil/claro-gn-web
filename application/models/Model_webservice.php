<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class model_webservice extends CI_Model {

		public function login($login = null, $senha = null) {

			$dados = $this->db->query("select id_usuario,ativo, fk_grupo,usuario from 
			usuarios inner join grupo_usuarios on id_usuario = fk_usuario
			where login = '".$login."'
			and senha = '".md5($senha)."';");

			if($dados->num_rows() == 1) {
				return $dados->result();
			} else {
				return false;
			}

		}

		public function senha_Email($senha = null,$login = null) {

			$dados = $this->db->query('select * from usuarios where login = \''.$login.'\' and ativo = 1;');

			try {

				if($dados->num_rows() > 0) {

					$this->db->query('update usuarios set senha = \''.$senha.'\' where login = \''.$login.'\';');
					return $dados->row()->email;

				} else {
					return "";
				}
				
			} catch (Exception $e) {

				echo $e;
				return "";
				
			}

		}

		public function atualizar($id_usuario = null,$usuario = null,$senha = null,$nova_senha = null){

			$dados = $this->db->query('select * from usuarios where id_usuario = '.$id_usuario.' and senha = \''.md5($senha).'\' and ativo = 1;');

			try {

				if($dados->num_rows() > 0) {

					if($nova_senha != "") {
						$this->db->query('update usuarios set senha = \''.md5($nova_senha).'\', usuario = \''.$usuario.'\' where id_usuario = '.$id_usuario.';');
					} else {
						$this->db->query('update usuarios set usuario = \''.$usuario.'\' where id_usuario = '.$id_usuario.';');
					}

					return true;

				} else {
					return false;
				}
				
			} catch (Exception $e) {

				return false;
				
			}


		}

		public function geolocalizar($dados = null){

			return $this->db->query('SELECT 
										distinct(id_pdv) id_pdv,
										lat_pdv,
										lng_pdv,
										filial,
										rede,
										cod_agente,
										lougradouro,
										numero_local,
										bairro,
										cidade,
										cep ,
										geo('.$dados['lat_pdv'].','.$dados['lng_pdv'].', lat_pdv, lng_pdv) as distancia
										from pdv 
										left join endereco_adicional_pdv on id_pdv = fk_pdv
										where lat_pdv is not null and
										lng_pdv is not null and
										geo('.$dados['lat_pdv'].','.$dados['lng_pdv'].', lat_pdv, lng_pdv) <= '.$dados['raio'])->result();

		}

		public function localizar($dados = null) {

			$testes = false;

			if($testes){
				return $this->db->query('select * from pdv where id_pdv in (840,380,116,569,988,989,172,767,632,9,769,485,729,178,272,719,991,990,741,856) order by rede;')->result();
			} else {
				return $this->db->query('
				select distinct(id_pdv) id_pdv,filial,rede,cod_agente,lougradouro,numero_local,bairro,cidade,cep from pdv left join endereco_adicional_pdv on id_pdv = fk_pdv 
				where cep like \''.$dados['cep'].'%\' or adicional_cep like \''.$dados['cep'].'%\'

				union

				select distinct(id_pdv) id_pdv,filial,rede,cod_agente,lougradouro,numero_local,bairro,cidade,cep from pdv left join endereco_adicional_pdv on id_pdv = fk_pdv 
				 where ((lower(cidade) = lower(\''.$dados['cidade'].'\') and lower(bairro) like lower(\''.$dados['bairro'].'%\') and cep not like \''.$dados['cep'].'%\') or (lower(adicional_cidade) = lower(\''.$dados['cidade'].'\')
				and lower(adicional_bairro) like lower(\''.$dados['bairro'].'%\') 
				and adicional_cep not like \''.$dados['cep'].'%\'))
				
				union

				select id_pdv,filial,rede,cod_agente,lougradouro,numero_local,bairro,cidade,cep from pdv where id_pdv = 1;')->result();

			}

		}

		public function view_badge(){
			//return $this->db->get("view_badge_hoje")->result();

			return $this->db->query("SELECT 0 as atento, 0 as comunicados, 0 as concorrencia, 0 as destaques, 0 as relatorio")->result();
		}

		public function buscarRegistros($categoria = null) {

			$this->db->query('SET lc_time_names = \'pt_BR\'');

			return $this->db->query('select *, date_format(data_upload, \'%d de %M de %Y as %H:%i\') as data
				 from '.$categoria.' where (data_limite >= curdate() or data_limite is null)  order by data_upload desc limit 100;')->result();
			

		}

		public function badgeRede(){

			return $this->db->query('select 
										distinct(rede)
										from comunicados
										inner join lista_redes on fk_rede = id_rede
										where date(data_upload) = current_date')->result();

		}

		public function faq_ios(){
			return $this->db->query('select id_faq,titulo_faq from faq where tipo_faq = 2')->result();
		}

		public function faq_android(){
			return $this->db->query('select id_faq,titulo_faq from faq where tipo_faq = 1')->result();	
		}

		public function faq_detalhes($id = null) {
			return $this->db->query('SELECT descricao_faq FROM faq where id_faq = '.$id)->row()->descricao_faq;		
		}

		public function badges(){
			return array ('revisar' => $this->db->query('SELECT count(*) as badge FROM pdv_revisar 
				inner join checkout on fk_checkout = id_checkout
				where date_format(data_checkout, \'%d/%m/%Y\') = date_format(current_date(), \'%d/%m/%Y\');')->row()->badge,
						'roteiro' => $this->db->query('select count(*) aguardando from roteiro where status = 0')->row()->aguardando);
		}

		public function mainFunction($q, $s) {

			if ($s) {
				return $this->db->query("{$q}")->result();
			} else {
				return $this->db->query("{$q}");
			}

			
		}

		//Usado no inicio do carteira
		public function todosUsuarios(){
			return $this->db->query('
				select distinct(id_usuario), 
				upper(usuario) usuario
				from usuarios 
				inner join grupo_usuarios on id_usuario = fk_usuario
				where filial_responsavel is not null and ativo = 1 and fk_grupo = 2
				order by usuario')->result();
		}

		public function buscarPorCnpj($cnpj = null){
			return $this->db->query("SELECT usuarios.*
								        from usuarios 
								        inner join grupo_usuarios gs on id_usuario = gs.fk_usuario
								        inner join carteira_gn cg on id_usuario = cg.fk_usuario
								        inner join pdv on id_pdv = fk_pdv
								        where filial_responsavel > 0 and ativo = 1 and fk_grupo = 2
								        and cnpj = '".$cnpj."'
								        order by filial_responsavel")->result();
		}

		public function buscarPorUsuario($id_usuario = null){
			return $this->db->query("select 
										rede, 
										id_pdv, 
										lougradouro 
										from pdv 
										where id_pdv in (select fk_pdv from carteira_gn 
											where fk_usuario = ".$id_usuario.");")->result();
		}
		//Usado no inicio do carteira FIM

		public function buscarPelaOperadora($operadora = null) {

			$this->db->query('SET lc_time_names = \'pt_BR\'');

			return $this->db->query('select *, date_format(data_upload, \'%d de %M de %Y as %H:%i\') as data
				 from concorrencia where operadora = '.$operadora.' and (data_limite >= curdate() or data_limite is null) order by data_upload desc limit 100;')->result();

		}

		public function buscarPelaRede($fk_rede = null) {

			$this->db->query('SET lc_time_names = \'pt_BR\'');

			return $this->db->query('select *, date_format(data_upload, \'%d de %M de %Y as %H:%i\') as data
				 from comunicados where fk_rede = '.$fk_rede.' and (data_limite >= curdate() or data_limite is null) order by data_upload desc limit 100;')->result();

		}

		public function buscarPorFilial($filial = null) {

			return $this->db->query("select rede, id_pdv, lougradouro from pdv where filial = ".$filial)->result();

		}

		public function gravar_token($dados = null) {

			$existe = $this->db->query("select * from tokens where fk_usuario = ".$dados['fk_usuario'].";");

			if($existe->num_rows() > 0) { // Já existe

				if($dados['token'] == $existe->row()->token) {
					return true;
				} else { //Token alterado
					return $this->db->query('update tokens set token = \''.$dados['token'].'\', aparelho = '.$dados['aparelho'].' where fk_usuario = '.$dados['fk_usuario'].';');
				}

			} else { //Novo Token

				return $this->db->insert('tokens',$dados);

			}

		}

		public function detalhesDasListas($id = null, $categoria = null){

			switch($categoria) {
                case "destaques":
                    $pk = 'id_destaque';
                    break;
                
                case "comunicados":
                    $pk = 'id_comunicado';
                    break;
                
                case "relatorio":
                    $pk = 'id_relatorio';
                    break;
                
                case "concorrencia":
                    $pk = 'id_concorrencia';
                    break;
                
                case "atento":
                    $pk = 'id_atento';
                    break;
                
                default:
                    break;
                
            }

            $this->db->query('SET lc_time_names = \'pt_BR\'');

            return $this->db->query('select *, date_format(data_upload, \'%d de %M de %Y as %H:%i\') as data from '.$categoria.' where '.$pk.' = '.$id);


		}

		public function tokens_usuarios(){
			return $this->db->query('select token from tokens t 
									inner join grupo_usuarios gs on t.fk_usuario = gs.fk_usuario
									where fk_grupo != 1;');
		}

		public function token_master(){
			return $this->db->query('select token from tokens t 
									inner join grupo_usuarios gs on t.fk_usuario = gs.fk_usuario
									where fk_grupo = 1;');
		}

		public function deletarToken($token = null) {

			$this->db->where('token',$token);
			return $this->db->delete("tokens");	
		
		}

		public function atualizarToken($novo = null, $antigo = null) {

			$this->db->where('token',$novo);
			$this->db->delete("tokens");	

			return $this->db->query("update tokens set token = '".$novo."' where token = '".$antigo."';");

		}

		public function ajax_buscar_pdvs($nome = null){
			return $this->db->query('select id_pdv as id, rede as text from pdv where lower(rede) like lower(\''.$nome.'%\');')->result();
		}

		/*ROTEIRO*/
		public function addRoteiro($valores = null){

			$this->db->insert('roteiro',$valores);
			return $this->db->insert_id();

		}

		public function editarRoteiro($valores = null,$id_roteiro = null){
			
			$this->db->where(array('id_roteiro' => $id_roteiro));
			return $this->db->update('roteiro',$valores);

		}

		public function descobrirGrupo($id_usuario = null) {
			return $this->db->query('select fk_grupo from grupo_usuarios where fk_usuario = '.$id_usuario)->row()->fk_grupo;
		}

		public function roteiroAdm($id = null){

			$filial = $this->db->query('select filial_responsavel from usuarios where id_usuario = '.$id)->row()->filial_responsavel;

			if (is_null($filial)) {
				$sql = 'Select id_usuario,usuario, (select count(*) from checkout c where c.fk_usuario = id_usuario and date(data_checkout) = curdate()) checkouts 
													from usuarios
													inner join grupo_usuarios gu on gu.fk_usuario = id_usuario
													where fk_grupo = 2 order by checkouts asc';
			} else {
				$sql = 'Select id_usuario,usuario, (select count(*) from checkout c where c.fk_usuario = id_usuario and date(data_checkout) = curdate()) checkouts 
													from usuarios
													inner join grupo_usuarios gu on gu.fk_usuario = id_usuario
													where fk_grupo = 2 and filial_responsavel = '.$filial.' order by checkouts asc';
			}

			return array('gns' => $this->db->query($sql)->result());

		}

		public function addEnderecosRoteiro($valores = null){

			$this->db->query('delete from roteiro_enderecos where fk_roteiro = '.$valores['fk_roteiro']);

			for ($i=0; $i < count($valores['endereco_google']); $i++) { 

				$dados = array (

					'fk_roteiro' => $valores['fk_roteiro'],
					'endereco_google' => $valores['endereco_google'][$i],
					'km_google' => $valores['km_google'][$i],
					'tempo_google' => $valores['tempo_google'][$i],
					'tempo_adicional' => $valores['tempo_adicional'][$i],

				);

				$this->db->insert('roteiro_enderecos',$dados);
			}

			return true;

		}

		public function listar_roteiros_where($id = null){

			$this->db->query('SET lc_time_names = \'pt_BR\'');

			$filial = $this->db->query('select filial_responsavel from usuarios where id_usuario = '.$id)->row()->filial_responsavel;

			if (is_null($filial)) {
				$sql = 'Select 
						id_roteiro,
						nome,
						date_format(data_solicitacao, \'%d de %M de %Y\') as solicitacao
						from roteiro 
						left join usuarios on id_usuario = fk_usuario
						where status = 0';
			} else {
				$sql = 'Select 
						id_roteiro,
						nome,
						date_format(data_solicitacao, \'%d de %M de %Y\') as solicitacao
						from roteiro 
						left join usuarios on id_usuario = fk_usuario
						where filial_responsavel = '.$filial.' and status = 0';
			}

			return array('pendentes' => $this->db->query($sql)->result());
		
		}

		public function roteiro_detalhes($id_roteiro = null){

			$this->db->query('SET lc_time_names = \'pt_BR\'');

			$this->db->select('date_format(data_solicitacao, \'%d de %M de %Y\') as data_solicitacao,
				date_format(inicio_roteiro, \'%d de %M de %Y\') as inicio_roteiro,
				date_format(fim_roteiro, \'%d de %M de %Y\') as fim_roteiro,
				date_format(data_aprovacao, \'%d de %M de %Y\') as data_aprovacao,
				(select sum(tempo_google) from roteiro_enderecos where fk_roteiro = '.$id_roteiro.') totalPercurso,
				(select sum(tempo_adicional) from roteiro_enderecos where fk_roteiro = '.$id_roteiro.') totalPdv,
				date_format(inicio_roteiro, \'%d/%m/%Y\') as inicio_roteiro_original,
				date_format(fim_roteiro, \'%d/%m/%Y\') as fim_roteiro_original,
				obs_roteiro,
				nome,
				fk_usuario,
				id_roteiro,
				km_total,
				horas_total,
				status');
			$this->db->join('usuarios','id_usuario = fk_usuario');
			$roteiro = $this->db->get_where('roteiro', array('id_roteiro' => $id_roteiro))->row();

			return array ('enderecos' => $this->db->query('select endereco_google,tempo_adicional from roteiro_enderecos 
									inner join roteiro 
									on id_roteiro = fk_roteiro
									where id_roteiro = '.$roteiro->id_roteiro.';')->result(),

							'roteiro' => $roteiro);

		}

		public function buscar_lista($id = null){

			return $this->db->query('select endereco_google from roteiro_enderecos 
									inner join roteiro 
									on id_roteiro = fk_roteiro
									where inicio_roteiro <= curdate() and fim_roteiro >= curdate() and status = 1 and fk_usuario = '.$id)->result();

		}

		//Exibe os detalhes dos atendimentos dos GNs para os Gerentes de filiais, atraves do APP (Acompanhamento de GN - Roteiro)
		public function detalhesCheckout($id_usuario = null){

			$this->db->query('SET lc_time_names = \'pt_BR\'');

			return $this->db->query('select upper(usuario) as usuario, upper(rede) as rede, upper(endereco_exato) as endereco_exato, date_format(data_checkout, \'%d de %M de %Y as %H:%i:%s\') as data_checkout, upper(curva) as curva
							from checkout 
							inner join pdv on fk_pdv = id_pdv
							inner join usuarios on id_usuario = fk_usuario
							where date(data_checkout) = curdate() and id_usuario = '.$id_usuario)->result();

		}

		/*Atualização 02/10/2017*/
		//Usado para trazer as informações de metas / projeções do GN.
		public function histInputGn($id = null){


			return array (
				
				'medias' => $this->db->query("SELECT 
										round(count(*) /2) as inputs,
										ifnull(sum(pre),0) as pre,
									    ifnull(sum(controle_facil),0) as controle_facil,
									    ifnull(sum(controle_giga),0) as controle_giga,
									    ifnull((sum(controle_facil) + sum(controle_giga)),0) as controle_total,
									    ifnull(sum(recarga),0) as recarga,
									    ifnull(sum(migracao),0) as migracao,
									    (select date_format(data_input,'%d/%m/%Y') 
									    	from input_vendas 
									    	where fk_usuario = {$id} 
									    	order by data_input desc 
									    	limit 1) data_input
										FROM input_vendas 
										WHERE 
											MONTH(data_input) = MONTH(NOW()) AND YEAR(data_input) = YEAR(NOW())
									        AND fk_usuario = {$id}")->row(),

				'metas' => $this->db->query("SELECT 
										meta_pre,
									    meta_controle,
									    meta_boleto,
									    meta_total,
									    meta_recarga,
									    meta_migracao
										 FROM usuarios
										 where id_usuario = {$id}")->row()
			);

		}

		//Exibe as metas dos usuários para o gerente de filial.
		public function viewMetasApp($id = null,$selecionado = null){

			$filial_responsavel = $this->db->query("select filial_responsavel from usuarios where id_usuario = {$id}")->row()->filial_responsavel;

			switch ($selecionado) {
				case 'Pré Pago':
					$campo = "ifnull(round(((((
                            	sum(pre) / round(count(*) /2) *25
                             )))*100)/meta_pre),0) as projecao,";
				break;

				case 'Controle Fácil':
					$campo = "ifnull(round(((((
								sum(controle_facil) / round(count(*) /2) *25
								)))*100)/meta_controle),0) as projecao,";
				break;

				case 'Controle Boleto':
					$campo = "ifnull(round(((((
								sum(controle_giga) / round(count(*) /2) *25
								)))*100)/meta_boleto),0) as projecao,";
				break;

				case 'Controle Total':
					$campo = "ifnull(round(((((
								(sum(controle_facil) + sum(controle_giga)) / round(count(*) /2) *25
								)))*100)/meta_total),0) as projecao,";
				break;

				case 'Recarga':
					$campo = "ifnull(round(((((
								sum(recarga) / round(count(*) /2) *25
								)))*100)/meta_recarga),0) as projecao,";
				break;

				case 'Migração':
					$campo = "ifnull(round(((((
								sum(migracao) / round(count(*) /2) *25
								)))*100)/meta_migracao),0) as projecao,";
				break;

				default:
					$campo = "0 as projecao,";
				break;
			}


			if ($filial_responsavel == 0) {
				return $this->db->query("SELECT 
										id_usuario,
										usuario,
										'1 e F2' as filial,
                                        round(count(*) /2) as inputs,
										ifnull(sum(pre),0) as pre,
									    ifnull(sum(controle_facil),0) as controle_facil,
									    ifnull(sum(controle_giga),0) as controle_giga,
									    ifnull((sum(controle_facil) + sum(controle_giga)),0) as controle_total,
									    ifnull(sum(recarga),0) as recarga,
									    ifnull(sum(migracao),0) as migracao,

									    {$campo}


									    meta_pre,
									    meta_controle,
									    meta_boleto,
									    meta_total,
									    meta_recarga,
									    meta_migracao
                                        
										FROM input_vendas 
                                        inner join usuarios on id_usuario = fk_usuario
										WHERE 
											MONTH(data_input) = MONTH(NOW()) 
                                            AND YEAR(data_input) = YEAR(NOW())
										GROUP BY id_usuario,usuario
										order by projecao desc")->result();
			} else {
				return $this->db->query("SELECT 
										id_usuario,
										usuario,
										filial,
                                        round(count(*) /2) as inputs,
										sum(pre) as pre,
									    sum(controle_facil) as controle_facil,
									    sum(controle_giga) as controle_giga,
									    (sum(controle_facil) + sum(controle_giga)) as controle_total,
									    sum(recarga) as recarga,
									    sum(migracao) as migracao,

									    {$campo}

									    meta_pre,
									    meta_controle,
									    meta_boleto,
									    meta_total,
									    meta_recarga,
									    meta_migracao
                                        
										FROM input_vendas 
                                        inner join usuarios on id_usuario = fk_usuario
										WHERE 
											MONTH(data_input) = MONTH(NOW()) 
                                            AND YEAR(data_input) = YEAR(NOW())
                                            AND filial = {$filial_responsavel}
										GROUP BY id_usuario,usuario
										order by projecao desc")->result();
			}

			

		}
		/*Atualização 02/10/2017*/


	}