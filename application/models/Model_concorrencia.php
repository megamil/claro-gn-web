<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class model_concorrencia extends CI_Model {

		public function editar_concorrencia($where = null) {

			$this->db->query('SET lc_time_names = \'pt_BR\'');

			return $this->db->query('select *, ((data_limite >= curdate() or data_limite is null)) valido, date_format(data_limite, \'%d/%m/%Y\') as limite,
					date_format(data_upload, \'%d de %M de %Y %H:%i\') as upload from concorrencia where id_concorrencia = '.$where[0]);

		}

		public function update($dados = null) {

			try {
				
				$this->db->where('id_concorrencia', $dados['id_concorrencia']);
				return $this->db->update('concorrencia',$dados);

			} catch (Exception $e) {

				echo 'Falha ao editar '.$e;
				return false;
				
			}

			

		}	

		public function del($id = null) {
			$this->db->where("id_concorrencia",$id);
			return $this->db->delete("concorrencia");
		}

		public function listar_concorrencia() {

			try {

				$this->db->query('SET lc_time_names = \'pt_BR\'');

				return $this->db->query('select *, ((data_limite >= curdate() or data_limite is null)) valido, date_format(data_limite, \'%d de %M de %Y\') as limite,
					date_format(data_upload, \'%d de %M de %Y %H:%i\') as upload from concorrencia')->result();
				
			} catch (Exception $e) {

				echo 'Falha ao listar: '.$e;
				
			}


		}

		public function novo_Registro($dados = null) {
			try {

				$this->db->insert('concorrencia',$dados);
				return $this->db->insert_id(); //Retorna o id 

			} catch (Exception $e) {

				echo 'Falha ao gravar '.$e;

			}
		}

		public function deletar_Registro ($id = null) {

			return $this->db->query('delete from concorrencia where id_concorrencia = '.$id.';');

		}

		public function atualizar_Registro ($dados = null) {

			return $this->db->query('update concorrencia set url_imagem = \''.$dados['url_imagem'].'\' where id_concorrencia = '.$dados['id_concorrencia'].';');

		}

	}