<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class model_comunicados extends CI_Model {

		public function editar_comunicados($where = null) {

			return array (
				'comunicados' => $this->db->query("select c.*, lr.rede, ((data_limite >= curdate() or data_limite is null)) valido, date_format(data_limite, '%d/%m/%Y') as limite,
					date_format(data_upload, '%d de %M de %Y %H:%i') as upload from comunicados c left join 
					lista_redes lr on id_rede = fk_rede where id_comunicado = ".$where[0]),
				'rede' => $this->db->get('lista_redes')->result()
				);

		}

		public function novo_comunicados($where = null) {

			return  $this->db->get('lista_redes')->result();

		}

		public function update($dados = null) {

			try {
				
				$this->db->where('id_comunicado', $dados['id_comunicado']);
				return $this->db->update('comunicados',$dados);

			} catch (Exception $e) {

				echo 'Falha ao editar '.$e;
				return false;
				
			}

			

		}	

		public function del($id = null) {
			$this->db->where("id_comunicado",$id);
			return $this->db->delete("comunicados");
		}

		public function listar_comunicados() {

			try {

				$this->db->query('SET lc_time_names = \'pt_BR\'');

				return $this->db->query('select c.*, lr.rede, ((data_limite >= curdate() or data_limite is null)) valido, date_format(data_limite, \'%d de %M de %Y\') as limite,
					date_format(data_upload, \'%d de %M de %Y %H:%i\') as upload from comunicados c left join 
					lista_redes lr on id_rede = fk_rede')->result();
				
			} catch (Exception $e) {

				echo 'Falha ao listar: '.$e;
				
			}


		}

		public function novo_Registro($dados = null) {
			try {

				$this->db->insert('comunicados',$dados);
				return $this->db->insert_id(); //Retorna o id 

			} catch (Exception $e) {

				echo 'Falha ao gravar '.$e;

			}
		}

		public function deletar_Registro ($id = null) {

			return $this->db->query('delete from comunicados where id_comunicado = '.$id.';');

		}

		public function atualizar_Registro ($dados = null) {

			return $this->db->query('update comunicados set url_imagem = \''.$dados['url_imagem'].'\' where id_comunicado = '.$dados['id_comunicado'].';');

		}

	}