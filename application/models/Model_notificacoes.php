<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class model_notificacoes extends CI_Model {


	public function carregarFirebaseTokens() {

		return $this->db->query('SELECT distinct(token) FROM tokens;');

	}

	public function novo_Token($token = null){
		return $this->db->insert('tokens',$token);
	}

	public function deletarToken($token = null) {

		$this->db->where('token',$token);
		return $this->db->delete("tokens");	
	
	}

	public function atualizarToken($novo = null, $antigo = null) {	

		return $this->db->query("update tokens set token = '".$novo."' where token = '".$antigo."';");

	}

}

?>