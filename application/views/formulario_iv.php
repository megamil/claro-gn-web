<?php 

$mes = date("m"); // Mês Atual
$ano = date("Y"); // Ano atual
$ultimo_dia = date("t", mktime(0,0,0,$mes,'01',$ano)); // último dia do mês
$ultimoDiaMes = $ano.'-'.$mes.'-'.$ultimo_dia;
$primeiroDiaMes = $ano.'-'.$mes.'-01';
$hoje = date('Y-m-d');

 ?>

<!DOCTYPE html>
<html>
<head>
	<title>Input de vendas</title>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- 	<meta http-equiv="refresh" content=1;url="https://docs.google.com/forms/d/1oTw-ziAS72vvsQxpdEbCzo1DnE17OxIxN9VBZK_7LV4/edit"> -->

	<link rel="stylesheet" href="<?php echo base_url(); ?>stylebootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>stylebootstrap/css/style.css">
	<script src="<?php echo base_url(); ?>stylebootstrap/js/jquery.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>style/js/maskedinput.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>style/jquery-ui/jquery-ui.js"></script>
	<script src="<?php echo base_url(); ?>style/js/script.js"></script>
	<script src="<?php echo base_url(); ?>stylebootstrap/js/bootstrap.min.js"></script>

</head>

<script type="text/javascript">

	$(document).ready(function(){

		<?php if(!empty($aviso)) {
			echo "var sucesso = true;
			";
			echo "var aviso = '".$aviso."';";
		} else {
			echo "var sucesso = false;";
			echo "var aviso = ''";
		} ?>

		 if(sucesso) {
		 	alert(aviso);
		 }

		 $('form').on('keyup keypress', function(e) {
			  var keyCode = e.keyCode || e.which;
			  if (keyCode === 13) { 
			  	console.log("Envio bloqueado.");
			    e.preventDefault();
			    return false;
			  }
		});

		 $('#filial').change(function(){
		 	$('#labelFilial').text($(this).val());
		 });

		 var ultimoDiaMes = new Date('<?php echo $ultimoDiaMes; ?>');
		 var primeiroDiaMes = new Date('<?php echo $primeiroDiaMes; ?>');
		 var hoje = new Date('<?php echo $hoje; ?>');

		 // $("#data_input").change(function(){

		 // 	var data = new Date(''+$(this).val()+'');

		 // 	if (data > ultimoDiaMes || data < primeiroDiaMes) {
		 // 		alert('Por favor, selecione uma data dentro do mês atual.');
		 // 		$(this).val(hoje);
		 // 	} else {
		 // 		console.log('Data OK');
		 // 	}

		 // });
		
		 $('#validar_Enviar').click(function(){

		  	var campo = $(this);

		 	campo.attr('disabled',true);
		 	campo.text('Aguarde...');

		 	setTimeout(function(){
			    campo.text('Verifique campos em branco');
			}, 5000);

			setTimeout(function(){
			    campo.text('ENVIAR');
			    campo.attr('disabled',false);
			}, 7000);

		 });


	});

</script>

<body aling="center">

<?php echo form_open('controller_inputvendas/persistencia'); ?>

<div class="row">
	<div class="col-xs-12">
		GN:
		<select name="filial" id="filial" class="form-control obrigatorio"  aviso="Selecone um GN" style="width: 100%">
		<option>Selecione...</option>
		<?php 
		$filial_label = "";
		foreach ($filial as $value) {
			if ($filial_label == "") {
				$filial_label = $value->filial;
			}
			echo '<option value="'.$value->filial.'-'.$value->usuario_.'-'.$value->id_usuario.'">'.$value->usuario.'</option>';
		}?>
		</select>
		Filial: <span id="labelFilial"><?php echo $filial_label; ?></span>
	</div>
</div>

<?php 

	if(!empty($_GET['usuario'])) { // se veio pela URL
		$usuario = $_GET['usuario'];
		echo '<input type="hidden" name="usuario" value="'.$usuario.'">';
	} else if (!empty($usuario)) { // se não veio pelo controller.
		echo '<input type="hidden" name="usuario" value="'.$usuario.'">';
	}

	if(!empty($_GET['fk_usuario'])) { // se veio pela URL
		$fk_usuario = $_GET['fk_usuario'];
		echo '<input type="hidden" name="fk_usuario_criou" value="'.$fk_usuario.'">';
	} else if (!empty($fk_usuario)) { // se não veio pelo controller.
		echo '<input type="hidden" name="fk_usuario_criou" value="'.$fk_usuario.'">';
	}

	// $hoje = time(); 

	// $ontem = $hoje - (24*3600); 
	
 ?>

<h4>Dados</h4>
<hr>

<h5>Data</h5>

<input class="form-control" type="date" min="<?php echo $primeiroDiaMes; ?>" max="<?php echo $ultimoDiaMes; ?>" name="data_input" id="data_input" style="width: 100%" value="<?php echo $hoje; ?>">
<br>

<h5 align="left">HABITADO</h5>
<br>

<h5>Pré pago</h5>
<input class="form-control obrigatorio" type="tel" name="pre" id="pre">
<br>

<h5>Controle Fácil</h5>
<input class="form-control obrigatorio" type="tel" name="controle_facil" id="controle_facil">
<br>

<h5>Controle Boleto</h5>
<input class="form-control obrigatorio" type="tel" name="controle_giga" id="controle_giga">
<br>

<h5>Recarga</h5>
<input class="form-control obrigatorio" type="tel" name="recarga" id="recarga">
<br>

<h5>Migração</h5>
<input class="form-control obrigatorio" type="tel" name="migracao" id="migracao">
<br>
<!-- <h5>Banda Larga - Pré Pago</h5>
<input class="form-control obrigatorio" type="tel" name="banda_pre" id="banda_pre">
<br> -->

<h5 align="left">DESABITADO</h5>
<br>

<h5>Pré pago</h5>
<input class="form-control obrigatorio" type="tel" name="pre_de" id="pre_de">
<br>

<h5>Controle Fácil</h5>
<input class="form-control obrigatorio" type="tel" name="controle_facil_de" id="controle_facil_de">
<br>

<h5>Controle Boleto</h5>
<input class="form-control obrigatorio" type="tel" name="controle_giga_de" id="controle_giga_de">
<br>

<h5>Recarga</h5>
<input class="form-control obrigatorio" type="tel" name="recarga_de" id="recarga_de">
<br>

<h5>Migração</h5>
<input class="form-control obrigatorio" type="tel" name="migracao_de" id="migracao_de">
<br>

<hr>

<!-- <h5>Chip Vendedor</h5>
<input class="form-control obrigatorio" type="tel" name="chip_vendedor" id="chip_vendedor">
<br> -->

<h5>HC Oficial</h5>
<input class="form-control obrigatorio" type="tel" name="hc_oficial" id="hc_oficial">
<br>

<h5>HC em Campo</h5>
<input class="form-control obrigatorio" type="tel" name="hc_campo" id="hc_campo">
<br>

<h5>Aspectos relevantes relacionados a rotina de vendas</h5>
<textarea class="form-control obrigatorio" type="text" rows="15" name="aspectos" id="aspectos"></textarea>
<br>

<div class="row" style="margin-bottom: 35px;">
	<div class="col-xs-12">
		<button type="submit" class="col-xs-12 btn btn-danger" height="100px" id="validar_Enviar">ENVIAR</button>
	</div>
</div>

<?php echo form_close(); ?>

</body>
</html>