<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Claro GN | Log in</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.4 -->
    <link href="<?php echo base_url() ?>stylebootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="<?php echo base_url() ?>stylebootstrap/fonts/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="<?php echo base_url() ?>stylebootstrap/css/bootsz.css" rel="stylesheet" type="text/css" />
    <!-- iCheck -->
    <link href="<?php echo base_url() ?>stylebootstrap/css/blue.css" rel="stylesheet" type="text/css" />

    <!--ICON-->
		<link rel="shortcut icon" href="<?php echo base_url(); ?>stylebootstrap/img/favicon.jpg" type="image/x-icon">


	<noscript>
		<div style="background-color: red; height: 100%; width: 100%; position: absolute; z-index: 200000;" align="center">
		HABILITE O JAVASCRIPT PARA USAR O SISTEMA!.
			<iframe src="http://enable-javascript.com/pt/" width="100%" height="100%"></iframe>

		</div>
	</noscript> 

	<script type="text/javascript">
		history.replaceState({pagina: "index"}, "index", "<?php echo base_url() ?>");
	</script>

	<?php if($aviso['tipo'] != ''){ /*Confirma a existencia de aviso para esta tela.*/ 

			switch ($aviso['tipo']) { 
        case 'erro':
          $recado = '<p style="color: red;" align="center">'.$aviso['mensagem'].'</p>';
          break;

        case 'aviso':
          $recado = '<p style="color: yellow;" align="center">'.$aviso['mensagem'].'</p>';
          break;
              
        default:
          $recado = '<p style="color: #e2e2e2;" align="center">' .$aviso['mensagem'].'</p>';;
          break;
      }

		} else {

			$recado = "";

		} ?>

   <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="tela-login">

    <!-- div formulario -->
    <div class="col-lg-6 col-md-6 col-sm-12 div-login">
        <div class="col-lg-2 col-md-1 col-sm-3"></div>
        <div class="col-lg-8 col-md-10 col-sm-6">
            <div class="login-logo col-lg-12  col-sm-12 col-xs-12">
                <a href="#"><img src="<?php echo base_url() ?>stylebootstrap/img/claro-gn.png" ></a>
            </div><!-- /.login-logo -->
            <div class="login-box-body col-lg-12  col-sm-12 col-xs-12" style="background-color: transparent">
                <p class="login-box-msg">Entre para ter acesso ao painel</p>
                <?php echo $recado; ?>
                <form action="main/login" method="post" accept-charset="utf-8">
                    <div class="form-group has-feedback">
                        <input type="text" class="form-control" name="user" placeholder="Dígite seu usuário" />
                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="password" class="form-control" name="pass" placeholder="Dígite sua senha"/>
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4"></div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4"></div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                            <button type="submit" style='background: #d83844; border: 0px;' class="btn btn-primary btn-block btn-flat">Entrar</button>
                        </div><!-- /.col -->
                    </div>
                    <?php echo form_close(); ?>

            </div><!-- /.login-box-body -->
        </div><!-- /.login-box -->

        </div>
        <div class="col-lg-2 col-md-1 col-sm-3"></div>



    <!-- div imagem -->
    <div class="col-lg-6 col-md-6 col-sm-12 div-imagem">
        <img src="<?php echo base_url(); ?>stylebootstrap/img/imagem3.jpg" alt="">
    </div>

    <!-- jQuery 2.1.4 -->
    <script src="<?php echo base_url() ?>stylebootstrap/js/jquery.js"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="<?php echo base_url() ?>stylebootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- iCheck -->
    <script src="<?php echo base_url() ?>stylebootstrap/js/icheck.min.js" type="text/javascript"></script>
    <script>
      $(function () {
        $('input').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_square-blue',
          increaseArea: '20%' // optional
        });
      });
    </script>
  </body>
</html>