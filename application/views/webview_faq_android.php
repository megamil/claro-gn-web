<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Claro GN | FAQ Android</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.4 -->
    <link href="<?php echo base_url() ?>stylebootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="<?php echo base_url() ?>stylebootstrap/fonts/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="<?php echo base_url() ?>stylebootstrap/css/bootsz.css" rel="stylesheet" type="text/css" />
    <!-- iCheck -->
    <link href="<?php echo base_url() ?>stylebootstrap/css/blue.css" rel="stylesheet" type="text/css" />

    <!--ICON-->
    <link rel="shortcut icon" href="<?php echo base_url(); ?>stylebootstrap/img/favicon.jpg" type="image/x-icon">
  <body>

  <noscript>
    <div style="background-color: red; height: 100%; width: 100%; position: absolute; z-index: 200000;" align="center">
    HABILITE O JAVASCRIPT PARA USAR O SISTEMA!.
      <iframe src="http://enable-javascript.com/pt/" width="100%" height="100%"></iframe>

    </div>
  </noscript> 


  <div align="center">
    <?php 

      foreach ($faq as $value) {
        echo '<a href="detalhes/'.$value->id_faq.'"><div style="height: 70px; padding-top:25px; border-bottom-width: 1px; border-bottom-color: black; border-bottom-style: solid;">'.strtoupper($value->titulo_faq).'</div></a>';
      }

    ?>
  </div>


  </body>
</html>