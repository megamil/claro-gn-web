<script type="text/javascript">
	history.replaceState({pagina: "lista_usuarios"}, "Listar usuarios", "<?php echo base_url() ?>main/redirecionar/seguranca-view_listar_usuarios");
</script>

<?php echo form_open('controller_seguranca/editar_usuario'); 
echo form_fieldset('Editar Usuário: '.$dados['usuario']->row()->usuario.' ID '.$dados['usuario']->row()->id_usuario);
echo '<div class="mdl-grid">
	<div class="mdl-cell mdl-cell--3-col">
		<a href="" id="apagar"><i class="material-icons">clear</i>Limpar campos</a>
	</div>
	<div class="mdl-cell mdl-cell--3-col">
		<a href="" id="voltar" class=""><i class="material-icons">reply</i>Voltar</a>
	</div>
	<div class="mdl-cell mdl-cell--3-col">
		<a href="" id="recarregar" url="'.$_SERVER ['REQUEST_URI'].'"><i class="material-icons">cached</i>Recarregar</a>
	</div>
	<div class="mdl-cell mdl-cell--3-col">
		'.anchor('main/redirecionar/seguranca-view_novos_usuarios', '<i class="material-icons">add</i>', array('class' => 'mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored', 'title' => 'Editar Usuário', 'alt' => 'Editar Usuário')).'
	</div>

</div>';
?>

<hr> 

<input type="hidden" name="id_usuario" value="<?php echo $dados['usuario']->row()->id_usuario; ?>" size="50" />
<input type="hidden" name="usuario_anterior" value="<?php echo $dados['usuario']->row()->usuario; ?>" size="50" />
<input type="hidden" name="email_anterior" value="<?php echo $dados['usuario']->row()->email; ?>" size="50" />

<div class="mdl-grid">

 <div class="mdl-cell mdl-cell--3-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
    <input type="text" class="mdl-textfield__input obrigatorio" name="usuario" aviso="Usuário" id="usuario" value="<?php echo $dados['usuario']->row()->usuario; ?>" size="50" maxlength="15"/>
    <label class="mdl-textfield__label" for="usuario">Usuário</label>
 </div>

 <div class="mdl-cell mdl-cell--3-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
    <input type="text" class="mdl-textfield__input validar_email obrigatorio" aviso="E-mail" name="email" id="email" value="<?php echo $dados['usuario']->row()->email; ?>" size="50" maxlength="40" />
    <label class="mdl-textfield__label" for="email">E-mail</label>
 </div>

<div class="mdl-cell mdl-cell--3-col">
 <label class="label" for="telefone">Telefone</label>
    <input type="tel" class="mdl-textfield__input mascara_cel obrigatorio" aviso="Telefone" name="telefone" id="telefone" value="<?php echo $dados['usuario']->row()->telefone; ?>" size="50" maxlength="11" />
 </div>

 <div class="mdl-cell mdl-cell--3-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
    <input type="text" class="mdl-textfield__input obrigatorio" aviso="Nome" name="nome" id="nome" value="<?php echo $dados['usuario']->row()->nome; ?>" size="50" maxlength="40" />
    <label class="mdl-textfield__label" for="nome">Nome</label>
 </div>

</div> <!-- Fecha mdl-grid Linha 1 -->

<div class="mdl-grid">
<div class="mdl-cell mdl-cell--3-col">
 <label class="label" for="login">Login</label>
    <input type="tel" class="mdl-textfield__input validar_numeros obrigatorio" aviso="Login" name="login" id="login" value="<?php echo $dados['usuario']->row()->login; ?>" size="50" maxlength="8" />
 </div>

 <div class="mdl-cell mdl-cell--3-col">
 <label class="label" for="matricula">Matrícula</label>
    <input type="tel" class="mdl-textfield__input validar_numeros obrigatorio" aviso="Matrícula" name="matricula" id="matricula" value="<?php echo $dados['usuario']->row()->matricula; ?>" size="50" maxlength="5" />
 </div>

<div class="mdl-cell mdl-cell--3-col">
 <label class="label" for="filial_responsavel">Filial</label>
    
    <select class="mdl-select" name="filial_responsavel" id="filial_responsavel">

    	<?php if ($dados['usuario']->row()->filial_responsavel == 1) {
    		$selected1 ='selected';
    		$selected2 ='';
    	} else if($dados['usuario']->row()->filial_responsavel == 2) {
    		$selected2 ='selected';
    		$selected1 ='';
    	} else {
			$selected1 ='';
    		$selected2 ='';
    	}?>

    	<option value="0">Sem Filial</option>
    	<option value="1" <?php echo $selected1; ?>>Filial 1</option>
    	<option value="2" <?php echo $selected2; ?>>Filial 2</option>
    </select>

 </div>
</div>

<?php if ($dados['usuario']->row()->gn): ?>

<div class="mdl-grid">

	<div class="mdl-cell mdl-cell--2-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
		<input class="mdl-textfield__input validar_numeros" aviso="Meta Pré Pago" name="meta_pre" id="meta_pre" value="<?php echo $dados['usuario']->row()->meta_pre; ?>"/>
		<label class="mdl-textfield__label" for="meta_pre">Meta Pré Pago</label>
	</div>
	<div class="mdl-cell mdl-cell--2-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
		<input class="mdl-textfield__input validar_numeros" aviso="Meta Controle Fácil" name="meta_controle" id="meta_controle" value="<?php echo $dados['usuario']->row()->meta_controle; ?>"/>
		<label class="mdl-textfield__label" for="meta_controle">Meta Controle Fácil</label>
	</div>
	<div class="mdl-cell mdl-cell--2-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
		<input class="mdl-textfield__input validar_numeros" aviso="Meta Controle Boleto" name="meta_boleto" id="meta_boleto" value="<?php echo $dados['usuario']->row()->meta_boleto; ?>"/>
		<label class="mdl-textfield__label" for="meta_boleto">Meta Controle Boleto</label>
	</div>
	<div class="mdl-cell mdl-cell--2-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
		<input class="mdl-textfield__input validar_numeros" aviso="Meta Controle Total" name="meta_total" id="meta_total" value="<?php echo $dados['usuario']->row()->meta_total; ?>"/>
		<label class="mdl-textfield__label" for="meta_total">Meta Controle Total</label>
	</div>
	<div class="mdl-cell mdl-cell--2-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
		<input class="mdl-textfield__input validar_numeros" aviso="Meta Recarga" name="meta_recarga" id="meta_recarga" value="<?php echo $dados['usuario']->row()->meta_recarga; ?>"/>
		<label class="mdl-textfield__label" for="meta_recarga">Meta Recarga</label>
	</div>
	<div class="mdl-cell mdl-cell--2-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
		<input class="mdl-textfield__input validar_numeros" aviso="Meta Migração" name="meta_migracao" id="meta_migracao" value="<?php echo $dados['usuario']->row()->meta_migracao; ?>"/>
		<label class="mdl-textfield__label" for="meta_migracao">Meta Migração</label>
	</div>

</div>

<?php endif; ?>

<div class="mdl-grid">

	 <div class="mdl-cell mdl-cell--3-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
	    <input type="password" class="mdl-textfield__input" aviso="Senha" name="senha" id="senha" value="" size="50" maxlength="32" />
	    <label class="mdl-textfield__label" for="senha">Nova Senha</label>
	 </div>

	 <div class="mdl-cell mdl-cell--3-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
	    <input type="password" class="mdl-textfield__input" aviso="Confirmação de senha" name="confirmacao" id="confirmacaoSenha" value="" size="50" maxlength="32" />
	    <label class="mdl-textfield__label" for="confirmacao">Confirme a nova Senha</label>
	 </div>

	<?php if($dados['usuario']->row()->ativo == 1) { $checked = 'checked';} else {$checked = '';} ?>

	<?php if($dados['usuario']->row()->id_usuario > 1) { //caso não seja o usuário de ID 1?>

	<label class="mdl-cell mdl-cell--1-col mdl-switch mdl-js-switch mdl-js-ripple-effect" for="switch-1" style="margin-top: 20px;">
		  <span class="mdl-switch__label">Ativo</span>	
		  <input type="checkbox" name="ativo" id="switch-1" class="mdl-switch__input" <?php echo $checked ?> />
	</label>

	<?php } else { ?>

		<input type="hidden" name="ativo" value="on">

	<?php } ?>
	
	<div class="mdl-cell mdl-cell--2">
		<button class="-col mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" id="validar_Enviar"><i class="material-icons">done</i>Confirmar Edição</button>	
	</div>

</div> <!-- Fecha mdl-grid Linha 2 -->

	<?php echo form_fieldset('Defina a qual grupo o usuário pertence') ?>

	<hr>

		<div align="center">
		<table class="mdl-data-table mdl-js-data-table mdl-shadow--8dp">
		  <thead>
		    <tr>
		      <th>ID</th>
		      <th>Grupo</th>
		      <th>Descrição</th>
		      <th>Adicionar ou Remover</th>
		    </tr>
		  </thead>
		  <tbody>
		    <?php foreach ($dados['grupos'] as $grupo) {
		    	echo '<tr>';
				echo '<td>'.$grupo->id_grupo.'</td>';
				echo '<td>'.$grupo->nome_grupo.'</td>';
				echo '<td>'.$grupo->descricao_grupo.'</td>';
				if($grupo->id_grupo == 1 && $dados['usuario']->row()->id_usuario == 1){
					echo '<td class="sucesso">Pertence ao Grupo.<td>';
				} else if ($dados['usuario']->row()->id_usuario == 1){
					echo '<td class="desabilitado">---<td>'; 
				} else {

					if($grupo->pertence == 1){

						echo '<td>'.anchor('controller_seguranca/add_rem_grupo/remover/'.$grupo->id_grupo.'/'.$dados['usuario']->row()->id_usuario, 'Remover do grupo', array('class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect alerta', 'title' => 'Remover do Grupo.', 'alt' => 'Remover do Grupo.', 'style' => 'margin-top: -7px;')).'</td>';

					} else {

						echo '<td>'.anchor('controller_seguranca/add_rem_grupo/adicionar/'.$grupo->id_grupo.'/'.$dados['usuario']->row()->id_usuario, 'Adicionar ao grupo', array('class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect sucesso', 'title' => 'Adicionar ao Grupo.', 'alt' => 'Adicionar ao Grupo.', 'style' => 'margin-top: -7px;')).'</td>';

					}

				}
				echo '</tr>';
			} ?>
		  </tbody>
		</table>
		</div>

	<?php echo form_fieldset_close();

echo form_close(); ?>