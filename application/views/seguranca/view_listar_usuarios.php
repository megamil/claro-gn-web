<div align="center">
	<?php echo anchor('main/redirecionar/seguranca-view_novos_usuarios', '<i class="material-icons">assignment</i>Novo Usuário', array('class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent', 'title' => 'Adicionar novo Usuário.', 'alt' => 'Novo Usuário.')); ?>
</div>

<hr>

<div align="center">

	<table class="mdl-data-table mdl-js-data-table mdl-shadow--8dp">
	  <thead>
	    <tr>
	      <th>ID</th>
	      <th>Usuários</th>
	      <th>Login Mobile</th>
	      <th>Telefone</th>
	      <th>Nome</th>
	      <th>Status</th>
	      <th>Editar</th>
	    </tr>
	  </thead>
	  <tbody>
	    <?php foreach ($dados as $usuario) {
	    	echo '<tr>';
			echo '<td>'.$usuario->id_usuario.'</td>';
			echo '<td>'.$usuario->usuario.'</td>';
			echo '<td>'.$usuario->login.'</td>';
			echo '<td><input value="'.$usuario->telefone.'" class="label_mascara_input mascara_telefone"/></td>';
			echo '<td>'.$usuario->nome.'</td>';
			if($usuario->ativo == 1) {echo '<td>Ativo</td>';}else{echo '<td>Inativo</td>';}
			echo '<td>'.anchor('main/redirecionar/seguranca-view_editar_usuarios/'.$usuario->id_usuario, '<i class="material-icons">mode_edit</i>Editar', array('class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent', 'title' => 'Editar Usuário.', 'alt' => 'Editar Usuário.', 'style' => 'margin-top: -7px;')).'</td>';
			echo '</tr>';
		} ?>
	  </tbody>
	</table>
</div>

