<script type="text/javascript">
	history.replaceState({pagina: "lista_usuarios"}, "Listar usuarios", "<?php echo base_url() ?>main/redirecionar/seguranca-view_listar_usuarios");
</script>
<?php echo form_open('controller_seguranca/editar_proprio_usuario'); 
echo form_fieldset('Editar Usuário: '.$dados->row()->usuario.' ID '.$dados->row()->id_usuario);
echo '<div class="mdl-grid">
	<div class="mdl-cell mdl-cell--3-col">
		<a href="" id="apagar"><i class="material-icons">clear</i>Limpar campos</a>
	</div>
	<div class="mdl-cell mdl-cell--3-col">
		<a href="" id="voltar" class=""><i class="material-icons">reply</i>Voltar</a>
	</div>
	<div class="mdl-cell mdl-cell--3-col">
		<a href="" id="recarregar" url="'.$_SERVER ['REQUEST_URI'].'"><i class="material-icons">cached</i>Recarregar</a>
	</div>
	<div class="mdl-cell mdl-cell--3-col">
		'.anchor('main/redirecionar/seguranca-view_novos_usuarios', '<i class="material-icons">add</i>', array('class' => 'mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored', 'title' => 'Criar Usuário', 'alt' => 'Criar Usuário')).'
	</div>

</div>';
?>

<hr> 

<input type="hidden" name="id_usuario" value="<?php echo $dados->row()->id_usuario; ?>" size="50" />
<input type="hidden" name="usuario_anterior" value="<?php echo $dados->row()->usuario; ?>" size="50" />
<input type="hidden" name="email_anterior" value="<?php echo $dados->row()->email; ?>" size="50" />

<div class="mdl-grid">

 <div class="mdl-cell mdl-cell--3-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
    <input type="text" class="mdl-textfield__input obrigatorio" name="usuario" aviso="Usuário" id="usuario" value="<?php echo $dados->row()->usuario; ?>" size="50" maxlength="15"/>
    <label class="mdl-textfield__label" for="usuario">Usuário</label>
 </div>

 <div class="mdl-cell mdl-cell--3-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
    <input type="text" class="mdl-textfield__input validar_email obrigatorio" aviso="E-mail" name="email" id="email" value="<?php echo $dados->row()->email; ?>" size="50" maxlength="40"/>
    <label class="mdl-textfield__label" for="email">E-mail</label>
 </div>

 <div class="mdl-cell mdl-cell--3-col">
 	<label class="label" for="telefone">Telefone</label>
    <input type="tel" class="mdl-textfield__input mascara_cel obrigatorio" aviso="Telefone" name="telefone" id="telefone" value="<?php echo $dados->row()->telefone; ?>" size="50" maxlength="11" />
 </div>

 <div class="mdl-cell mdl-cell--3-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
    <input type="text" class="mdl-textfield__input obrigatorio" aviso="Nome" name="nome" id="nome" value="<?php echo $dados->row()->nome; ?>" size="50" maxlength="40"/>
    <label class="mdl-textfield__label" for="nome">Nome</label>
 </div>

</div> <!-- Fecha mdl-grid Linha 1 -->

<div class="mdl-grid">
<div class="mdl-cell mdl-cell--3-col">
 <label class="label" for="telefone">Login</label>
    <input type="tel" class="mdl-textfield__input validar_numeros obrigatorio" aviso="Login" name="login" id="login" value="<?php echo $dados->row()->login; ?>" size="50" maxlength="8" />
 </div>

 <div class="mdl-cell mdl-cell--3-col">
 <label class="label" for="telefone">Matrícula</label>
    <input type="tel" class="mdl-textfield__input validar_numeros obrigatorio" aviso="Matrícula" name="matricula" id="matricula" value="<?php echo $dados->row()->matricula; ?>" size="50" maxlength="5" />
 </div>
</div>

<div class="mdl-grid">

	 <div class="mdl-cell mdl-cell--3-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
	    <input type="password" class="mdl-textfield__input" aviso="Senha" name="senha" id="senha" value="" size="50" maxlength="32"/>
	    <label class="mdl-textfield__label" for="senha">Nova Senha</label>
	 </div>

	 <div class="mdl-cell mdl-cell--3-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
	    <input type="password" class="mdl-textfield__input" aviso="Confirmação de senha" name="confirmacao" id="confirmacaoSenha" value="" size="50" maxlength="32"/>
	    <label class="mdl-textfield__label" for="confirmacao">Confirme a nova Senha</label>
	 </div>

	<div class="mdl-cell mdl-cell--2">
		<button class="-col mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" id="validar_Enviar"><i class="material-icons">done</i>Confirmar Edição</button>	
	</div>

</div> <!-- Fecha mdl-grid Linha 2 -->

<?php echo form_fieldset_close();
echo form_close(); ?>