<div align="center">
	<?php echo anchor('main/redirecionar/seguranca-view_novos_grupos', '<i class="material-icons">assignment</i>Novo Grupo', array('class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent', 'title' => 'Adicionar novo Grupo.', 'alt' => 'Novo Grupo.')); ?>
</div>

<hr>

<div align="center">

	<table class="mdl-data-table mdl-js-data-table mdl-shadow--8dp">
	  <thead>
	    <tr>
	      <th>ID</th>
	      <th>Grupo</th>
	      <th>Descrição</th>
	      <th>Editar</th>
	    </tr>
	  </thead>
	  <tbody>
	    <?php foreach ($dados as $grupo) {
	    	echo '<tr>';
			echo '<td>'.$grupo->id_grupo.'</td>';
			echo '<td>'.$grupo->nome_grupo.'</td>';
			echo '<td>'.$grupo->descricao_grupo.'</td>';
			echo '<td>'.anchor('main/redirecionar/seguranca-view_editar_grupos/'.$grupo->id_grupo, '<i class="material-icons">mode_edit</i>Editar', array('class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent', 'title' => 'Editar Grupo.', 'alt' => 'Editar Grupo.', 'style' => 'margin-top: -7px;')).'</td>';
			echo '</tr>';
		} ?>
	  </tbody>
	</table>
</div>

