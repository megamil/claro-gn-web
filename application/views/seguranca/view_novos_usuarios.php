<script type="text/javascript">
	history.replaceState({pagina: "listar_usuarios"}, "Novo Usuário", "<?php echo base_url() ?>main/redirecionar/seguranca-view_listar_usuarios");
</script>

<?php echo form_open('controller_seguranca/criar_usuario'); 
echo form_fieldset('Criar Usuário');
echo '<div class="mdl-grid">
	<div class="mdl-cell mdl-cell--3-col">
		<a href="" id="apagar"><i class="material-icons">clear</i>Limpar campos</a>
	</div>
	<div class="mdl-cell mdl-cell--3-col">
		<a href="" id="voltar" class=""><i class="material-icons">reply</i>Voltar</a>
	</div>
	<div class="mdl-cell mdl-cell--3-col">
		<a href="" id="recarregar" url="'.$_SERVER ['REQUEST_URI'].'"><i class="material-icons">cached</i>Recarregar</a>
	</div>
</div>';
?>

<hr>

<div class="mdl-grid">

 <div class="mdl-cell mdl-cell--3-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
    <input type="text" class="mdl-textfield__input obrigatorio" aviso="Usuário" name="usuario" id="usuario" value="<?php echo $this->session->flashdata('usuario'); ?>" size="50" maxlength="15"/>
    <label class="mdl-textfield__label" for="usuario">Usuário</label>
 </div>

 <div class="mdl-cell mdl-cell--3-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
    <input type="text" class="mdl-textfield__input obrigatorio validar_email" aviso="E-mail" name="email" id="email" value="<?php echo $this->session->flashdata('email'); ?>" size="50" maxlength="40"/>
    <label class="mdl-textfield__label" for="email">E-mail</label>
 </div>
 <div class="mdl-cell mdl-cell--3-col">
 <label class="label" for="telefone">Telefone</label>
    <input type="tel" class="mdl-textfield__input obrigatorio mascara_cel" aviso="Telefone" name="telefone" id="telefone" value="<?php echo $this->session->flashdata('telefone'); ?>" size="50" maxlength="11"/>
 </div>

 <div class="mdl-cell mdl-cell--3-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
    <input type="text" class="mdl-textfield__input obrigatorio" name="nome" aviso="Nome" id="nome" value="<?php echo $this->session->flashdata('nome'); ?>" size="50" maxlength="40" />
    <label class="mdl-textfield__label" for="nome">Nome</label>
 </div>

</div> <!-- Fecha mdl-grid Linha 1 -->

<div class="mdl-grid">
<div class="mdl-cell mdl-cell--3-col">
 <label class="label" for="telefone">Login</label>
    <input type="tel" class="mdl-textfield__input validar_numeros obrigatorio" aviso="Login" name="login" id="login" value="" size="50" maxlength="8" />
 </div>

 <div class="mdl-cell mdl-cell--3-col">
 <label class="label" for="telefone">Matrícula</label>
    <input type="tel" class="mdl-textfield__input validar_numeros obrigatorio" aviso="Matrícula" name="matricula" id="matricula" value="" size="50" maxlength="5" />
 </div>

<div class="mdl-cell mdl-cell--3-col">
 <label class="label" for="filial_responsavel">Filial</label>
    
    <select class="mdl-select" name="filial_responsavel" id="filial_responsavel">

    	<?php if ($this->session->flashdata('filial_responsavel') == 1) {
    		$selected1 ='selected';
    		$selected2 ='';
    	} else if($this->session->flashdata('filial_responsavel') == 2) {
    		$selected2 ='selected';
    		$selected1 ='';
    	} else {
			$selected1 ='';
    		$selected2 ='';
    	}?>

    	<option value="0">Sem Filial</option>
    	<option value="1" <?php echo $selected1; ?>>Filial 1</option>
    	<option value="2" <?php echo $selected2; ?>>Filial 2</option>
    </select>

 </div>
</div>

<div class="mdl-grid">

	 <div class="mdl-cell mdl-cell--3-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
	    <input type="password" class="mdl-textfield__input obrigatorio" aviso="Senha" name="senha" id="senha" value="" size="50" maxlength="32"/>
	    <label class="mdl-textfield__label" for="senha">Nova Senha</label>
	 </div>

	 <div class="mdl-cell mdl-cell--3-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
	    <input type="password" class="mdl-textfield__input obrigatorio" aviso="confirmação de senha" name="confirmacao" id="confirmacaoSenha" value="" size="50" maxlength="32"/>
	    <label class="mdl-textfield__label" for="confirmacao">Confirme a nova Senha</label>
	 </div>

	<?php if($this->session->flashdata('ativo') != 'off') { $checked = 'checked';} else {$checked = '';} ?>

		<label class="mdl-cell mdl-cell--1-col mdl-switch mdl-js-switch mdl-js-ripple-effect" for="switch-1" style="margin-top: 20px;">
		  <span class="mdl-switch__label">Ativo</span>	
		  <input type="checkbox" name="ativo" id="switch-1" class="mdl-switch__input" <?php echo $checked ?> />
		</label>

	<div class="mdl-cell mdl-cell--2">
		<button class="-col mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" id="validar_Enviar"><i class="material-icons">done</i>Criar Usuário</button>	
	</div>

</div> <!-- Fecha mdl-grid Linha 2 -->

<?php echo form_fieldset_close();
echo form_close(); ?>