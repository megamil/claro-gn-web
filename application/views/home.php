<!-- div principal -->
<div class="mdl-grid div-home">

    <div class="mdl-cell--1-col"></div>
    <div class="mdl-cell--10-col">

        <div class="col-lg-12 mdl-grid">
            <!-- novo destaque -->
            <div class="mdl-cell--3-col div-primeira-linha-home">
                <a href="<?php echo base_url() ?>main/redirecionar/cadastro-view_novo_destaques">
                    <img src="<?php echo base_url() ?>stylebootstrap/img/novo_destaque.jpg" width="95%">
                    <div class="div-h4-home-destaque">
                        <h4 class="text-center h4-primeira-linha">NOVO DESTAQUE</h4>
                    </div>
                </a>
            </div>


            <!-- novo comunicado -->
            <div class="mdl-cell--3-col div-primeira-linha-home">
                <a href="<?php echo base_url() ?>main/redirecionar/cadastro-view_novo_comunicados">
                    <img src="<?php echo base_url() ?>stylebootstrap/img/novo_comunicado.jpg" width="95%">
                    <div class="div-h4-home-comunicado">
                        <h4 class="text-center h4-primeira-linha">NOVO COMUNICADO</h4>
                    </div>
                </a>
            </div>

            <!-- relatórios -->
            <div class="mdl-cell--3-col div-primeira-linha-home">
                <a href="<?php echo base_url() ?>main/redirecionar/relatorios-view_listar_relatorios">
                    <img src="<?php echo base_url() ?>stylebootstrap/img/Relatorios.jpg" width="95%">
                    <div class="div-h4-home">
                        <h4 class="text-center h4-primeira-linha">RELATÓRIOS</h4>
                    </div>
                </a>
            </div>

            <!-- novo usuario -->
            <div class="mdl-cell--3-col div-primeira-linha-home">
                <a href="<?php echo base_url() ?>main/redirecionar/seguranca-view_novos_usuarios">
                    <img src="<?php echo base_url() ?>stylebootstrap/img/novo_usuario.jpg" width="95%">
                    <div class="div-h4-home">
                        <h4 class="text-center h4-primeira-linha">NOVO USUÁRIO</h4>
                    </div>
                </a>
            </div>
        </div>

    </div>
    <div class="mdl-cell--1-col"></div>

    <div class="mdl-cell--1-col"></div>
    <div class="mdl-cell--10-col">


        <div class="col-lg-12 mdl-grid">
            <!-- roteiro -->
            <div class="mdl-cell--3-col div-primeira-linha-home">
                <a href="<?php echo base_url() ?>main/redirecionar/listar-view_lista_roteiro">
                    <img src="<?php echo base_url() ?>stylebootstrap/img/Roteiro.jpg" width="95%">
                    <div class="div-h4-home-roteiro">
                        <h4 class="text-center h4-primeira-linha">ROTEIRO</h4>
                    </div>
                </a>
            </div>

            <!-- input de vendas -->
            <div class="mdl-cell--3-col div-primeira-linha-home">
                <a href="<?php echo base_url() ?>main/redirecionar/relatorios-view_geral_inputvendas">
                    <img src="<?php echo base_url() ?>stylebootstrap/img/input_de_vendas.jpg" width="95%">
                    <div class="div-h4-home-input">
                        <h4 class="text-center h4-primeira-linha">INPUT DE VENDAS</h4>
                    </div>
                </a>
            </div>

            <!-- divs relatorios -->
            <div class="mdl-cell--3-col div-primeira-linha-home">

                <div class="mdl-grid">

                    <!-- media de visitas -->
                    <div class="mdl-cell--6-col">
                        <a href="<?php echo base_url() ?>main/redirecionar/relatorios-view_visitaspdvc_relatorios">
                            <img src="<?php echo base_url() ?>stylebootstrap/img/media_de_visitas.jpg" width="95%" alt="" >
                            <div class="div-h4-home-roteiro">
                                <p class="text-center p-divs">MÉDIA DE VISITAS</p>
                            </div>
                        </a>
                    </div>

                    <!-- analise de horas trabalhadas -->
                    <div class="mdl-cell--6-col" style="margin-left: 9%">
                        <a href="<?php echo base_url() ?>main/redirecionar/relatorios-view_horario_relatorios">
                            <img src="<?php echo base_url() ?>stylebootstrap/img/analise_de_horas_trabalhadas.jpg" width="95%" alt="">
                            <div class="div-h4-home-roteiro">
                                <p class="text-center p-divs">ANÁLISE DE HORAS TRABALHADAS </p>
                            </div>
                        </a>
                    </div>

                    <!-- media de faturamento -->
                    <div class="mdl-cell--6-col">
                        <a href="<?php echo base_url() ?>main/redirecionar/relatorios-view_faturamento_relatorios">
                            <img src="<?php echo base_url() ?>stylebootstrap/img/media_de_faturamento.jpg" width="95%" alt="" >
                            <div class="div-h4-home-roteiro">
                                <p class="text-center p-divs">MÉDIA DE FATURAMENTO </p>
                            </div>
                        </a>
                    </div>

                    <!-- promotor claro x concorrencia -->
                    <div class="mdl-cell--6-col" style="margin-left: 9%">
                        <a href="<?php echo base_url() ?>main/redirecionar/relatorios-view_promotor_relatorios">
                            <img src="<?php echo base_url() ?>stylebootstrap/img/promotor_claro_x_concorrencia.jpg" width="95%" alt="">
                            <div class="div-h4-home-roteiro">
                                <p class="text-center p-divs">PROMOTOR CLARO X CONCORRÊNCIA </p>
                            </div>
                        </a>
                    </div>

                </div>

            </div>

            <div class="mdl-cell--3-col div-primeira-linha-home">

                <div class="mdl-grid">

                    <!-- analise de pdvs -->
                    <div class="mdl-cell--6-col">
                        <a href="<?php echo base_url() ?>main/redirecionar/relatorios-view_mediatam_relatorios">
                            <img src="<?php echo base_url() ?>stylebootstrap/img/analise_de_pdvs.jpg" width="95%" alt="" >
                            <div class="div-h4-home-roteiro">
                                <p class="text-center p-divs">ANÁLISE DE PDVs (TIPOS DE PDVs)</p>
                            </div>
                        </a>
                    </div>

                    <!-- ranking de visitas -->
                    <div class="mdl-cell--6-col" style="margin-left: 9%">
                        <a href="<?php echo base_url() ?>main/redirecionar/relatorios-view_recorrencia_relatorios">
                            <img src="<?php echo base_url() ?>stylebootstrap/img/ranking_de_visitas_a_pdvs.jpg" width="95%" alt="">
                            <div class="div-h4-home-roteiro">
                                <p class="text-center p-divs">RANKING DE VISITAS A PDVs</p>
                            </div>
                        </a>
                    </div>

                    <!-- analise promotor no pdv -->
                    <div class="mdl-cell--6-col">
                        <a href="<?php echo base_url() ?>main/redirecionar/relatorios-view_gnatente_relatorios">
                            <img src="<?php echo base_url() ?>stylebootstrap/img/analise_promotor_no_pdv.jpg" width="95%" alt="" >
                            <div class="div-h4-home-roteiro">
                                <p class="text-center p-divs">ANÁLISE PROMOTOR NO PDV </p>
                            </div>
                        </a>
                    </div>

                    <!-- campanhas -->
                    <div class="mdl-cell--6-col" style="margin-left: 9%">
                        <a href="<?php echo base_url() ?>main/redirecionar/relatorios-view_campanhas_relatorios">
                            <img src="<?php echo base_url() ?>stylebootstrap/img/campanhas.jpg" width="95%" alt="">
                            <div class="div-h4-home-roteiro">
                                <p class="text-center p-divs">CAMPANHAS </p>
                            </div>
                        </a>
                    </div>

                </div>

            </div>
        </div>

    </div>
    <div class="mdl-cell--1-col"></div>

</div>

<!-- div naville -->
<div class="mdl-grid">

    <div class="mdl-cell--8-col">

        <div class="mdl-grid" style="padding-top: .5%; padding-bottom: .5%">

            <div class="mdl-cell--9-col " style="margin-left: -15%">
                Key Account: <b>Tharik Sorbini</b><br>
                &nbsp;&nbsp; &nbsp; &nbsp; &nbsp;<a href="mailto:tharik.sorbini@navillebrasil.com" style="color: #7a0b09">tharik.sorbini@navillebrasil.com</a>
            </div>
            <div class="mdl-cell--3-col" style="border-left: 1px solid #b3b3b3">
                Tel.: <span style="font-size: 15px"><b>+55 (11) 2788-9195</b></span>
            </div>

        </div>

    </div>
    <div class="mdl-cell--1-col"></div>
    <div class="mdl-cell--3-col" style="padding-top: 1%;">
        <a href="http://www.navillebrasil.com">
            <img src="<?php echo base_url() ?>stylebootstrap/img/logo_naville.png" width="70%" alt="" >
        </a>
    </div>

</div>

<style>

    @import url('https://fonts.googleapis.com/css?family=Open+Sans:300');

    .div-home{
        background-color: #7a0b09;
        padding-top: 3%;
    }

    .div-primeira-linha-home{
        display: inline-block;
    }

    .div-h4-home-destaque{
        background-color: #1a801d;
        width: 95%;
        padding-top: 1%;
        padding-bottom: 1%;
    }

    .div-h4-home-comunicado{
        background-color: #b58f06;
        width: 95%;
        padding-top: 1%;
        padding-bottom: 1%;
    }

    .div-h4-home{
        background-color: #801008;
        width: 95%;
        padding-top: 1%;
        padding-bottom: 1%;
    }

    .div-h4-home-roteiro{
        background-color: #801008;
        width: 95%;
        padding-top: 1%;
        padding-bottom: 1%;
    }

    .div-h4-home-input{
        background-color: #055673;
        width: 95%;
        padding-top: 1%;
        padding-bottom: 1%;
    }

    .p-divs{
        background-color: #801008;
        width: 95%;
        padding-top: 1%;
        font-size: 10px;
        color: white;
    }



    .h4-primeira-linha{
        color: white;
        font-family: 'Open Sans', sans-serif;
    }

</style>
