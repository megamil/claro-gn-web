<span align="center"><h2>Atualizar Comunicados</h2></span>

<hr>

<?php echo form_open_multipart('controller_comunicados/editar'); ?>

<div class="mdl-grid">
<div class="mdl-cell mdl-cell--3-col">
  <input type="hidden" name="id_comunicado" value="<?php echo $dados['comunicados']->row()->id_comunicado; ?>"/>
  <input type="hidden" name="imagemAtual" value="<?php echo $dados['comunicados']->row()->url_imagem; ?>"/>
  <small>Criado em: <?php echo $dados['comunicados']->row()->upload; ?></small>
</div>
</div>

<div class="mdl-grid">
<div class="mdl-cell mdl-cell--1-col"></div>
<div class="mdl-cell mdl-cell--3-col" style="margin-top: 25px;">
  <input type="hidden" name="id_comunicado" value="<?php echo $dados['comunicados']->row()->id_comunicado; ?>"/>
  <input type="hidden" name="imagemAtual" value="<?php echo $dados['comunicados']->row()->url_imagem; ?>"/>

      REDE: <select name="fk_rede" id="fk_rede" style="width: 200px;">
      <?php 

        foreach ($dados['rede'] as $rede) {
          if($rede->id_rede == $dados['comunicados']->row()->fk_rede) {
            echo '<option value="'.$rede->id_rede.'" selected>'.$rede->rede.'</option>';
          } else if($this->session->flashdata('fk_rede') != "") {
            echo '<option value="'.$rede->id_rede.'" selected>'.$rede->rede.'</option>';
          } else {
            echo '<option value="'.$rede->id_rede.'">'.$rede->rede.'</option>';
          }
        }

       ?>
    </select>
</div>

  <div class="mdl-cell mdl-cell--4-col">
    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
    <input class="mdl-textfield__input obrigatorio" type="text" id="titulo_comunicados" name="titulo_comunicados" aviso="Título" maxlength="140" value="<?php if($this->session->flashdata('titulo_comunicados') != "" ) {
              echo $this->session->flashdata('titulo_comunicados'); 
            } else {
              echo $dados['comunicados']->row()->titulo; 
            } ?> ">
    <label class="mdl-textfield__label" for="titulo_comunicados">Título</label>
  </div>
  </div>

  <div class="mdl-cell mdl-cell--3-col">
    <label class="label" for="data_limite">Data Limite</label>
    <input class="mdl-textfield__input mascara_data obrigatorio" type="text" id="data_limite" name="data_limite" aviso="Data Limite" value="<?php if($this->session->flashdata('data_limite') != "" ) {
              echo $this->session->flashdata('data_limite'); 
            } else {
              echo $dados['comunicados']->row()->limite; 
            } ?>" >
  </div>
  
</div>


<div class="mdl-grid">
  <div class="mdl-cell mdl-cell--10-col mdl-textfield mdl-js-textfield">
    Descrição: <textarea class="mdl-textfield__input" type="text" rows= "5"  Aviso="Descrição" id="descricao_comunicados" name="descricao_comunicados" maxlength="500">
    <?php if($this->session->flashdata('descricao_comunicados') != "" ) {
              echo $this->session->flashdata('descricao_comunicados'); 
            } else {
              echo $dados['comunicados']->row()->descricao; 
            } ?>          
    </textarea>
   </div>

  <div class="mdl-cell mdl-cell--2-col" align="center">
    <label class="label" for="imagemdescricao">UPLOAD IMAGEM PARA DESCRIÇÃO</label>
    <input type="file" name="imagemdescricao" id="arquivo">
  </div>
    
  </div>

<div class="mdl-grid">
  <div class="mdl-cell mdl-cell--4-col" style="margin-top: 25px;"></div>
  <div class="mdl-cell mdl-cell--4-col" style="margin-top: 25px;">
      <a href="<?php echo base_url();?>/upload/comunicados/<?php echo $dados['comunicados']->row()->url_imagem ?>" target="_blank" title="Click para abrir">
    <img src="<?php echo base_url();?>/upload/comunicados/<?php echo $dados['comunicados']->row()->url_imagem ?>" width="200px">
  </a>
  <br>
    <label class="label" for="imagem">Imagem Referência. (Usada nos Aplicativos)</label>
    <input class="mdl-textfield__input" type="file" aviso="Imagem" name="imagem" maxlength="200">
  </div>
</div>

<div class="mdl-grid">
  <div class="mdl-cell mdl-cell--4-col"></div>
  <div class="mdl-cell mdl-cell--4-col">
    <button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" id="validar_Enviar">
        Atualizar
    </button>
  </div>
</div>

<?php echo form_close(); ?>

<script type="text/javascript">

    (function () {
      var container = document.getElementById("descricao_comunicados").parentNode;
      container.addEventListener('mdl-componentupgraded', function(){
        tinymce.init({
          selector: 'textarea',
      height: 500,
      theme: 'modern',
      plugins: [
        'advlist autolink lists link image charmap print preview hr anchor pagebreak',
        'searchreplace wordcount visualblocks visualchars code fullscreen',
        'insertdatetime media nonbreaking save table contextmenu directionality',
        'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
      ],
      toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
      toolbar2: 'print preview media | forecolor backcolor emoticons | codesample',
      image_advtab: true,
      templates: [
        { title: 'Test template 1', content: 'Test 1' },
        { title: 'Test template 2', content: 'Test 2' }
      ],
      content_css: [
        '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i'
      ]
        });
      }, false);
    })();

    $('#arquivo').change(function(){
      $('form').submit();
    });

</script>