
<?php echo form_open('controller_inputvendas/editar_input'); 

echo '<div class="mdl-grid">
	<div class="mdl-cell mdl-cell--3-col">
		<a href="" id="apagar"><i class="material-icons">clear</i>Limpar campos</a>
	</div>
	<div class="mdl-cell mdl-cell--3-col">
		<a href="" id="voltar" class=""><i class="material-icons">reply</i>Voltar</a>
	</div>
	<div class="mdl-cell mdl-cell--3-col">
		<a href="" id="recarregar" url="'.$_SERVER ['REQUEST_URI'].'"><i class="material-icons">cached</i>Recarregar</a>
	</div>
	<div class="mdl-cell mdl-cell--3-col">
		'.anchor('main/redirecionar/seguranca-view_novos_usuarios', '<i class="material-icons">add</i>', array('class' => 'mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored', 'title' => 'Editar Usuário', 'alt' => 'Editar Usuário')).'
	</div>

</div>';
?>

<hr> 

<input type="hidden" name="id_input_vendas" value="<?php echo $dados['input']->row()->id_input_vendas; ?>" size="50" />


<div class="mdl-grid">

	 <div class="mdl-cell mdl-cell--3-col">
	 	<label class="label" for="filial">GN</label>
	    <select name="filial" id="filial" class="form-control obrigatorio"  aviso="Selecone um GN" style="width: 100%">
		<option>Selecione...</option>
		<?php 
		foreach ($dados['usuarios'] as $value) {
			if ($value->id_usuario == $dados['input']->row()->fk_usuario) {
				echo '<option value="'.$value->filial.'-'.$value->usuario_.'-'.$value->id_usuario.'" selected>'.$value->usuario.'</option>';
			} else {
				echo '<option value="'.$value->filial.'-'.$value->usuario_.'-'.$value->id_usuario.'">'.$value->usuario.'</option>';
			}
		}?>
		</select>
	 </div>


	 <div class="mdl-cell mdl-cell--3-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
	    <input type="text" class="mdl-textfield__input obrigatorio mascara_data" aviso="Data input" name="data_input" id="data_input" value="<?php echo $dados['input']->row()->data_input; ?>" size="50" maxlength="4"/>
	    <label class="mdl-textfield__label" for="data_input">Data input</label>
	 </div>

	 <div class="mdl-cell mdl-cell--3-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
	 	<?php 

	 		if($dados['input']->row()->habitado){
	 			echo '<h3>HABITADO</h3>';
	 		} else {
	 			echo '<h3>DESABITADO</h3>';
	 		}

	 	 ?>
	   
	 </div>

</div>


<div class="mdl-grid">

	<div class="mdl-cell mdl-cell--2-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
	    <input type="text" class="mdl-textfield__input obrigatorio validar_numeros" aviso="Pré pago" name="pre" id="pre" value="<?php echo $dados['input']->row()->pre; ?>" size="50" maxlength="4"/>
	    <label class="mdl-textfield__label" for="pre">Pré pago</label>
	 </div>

	<div class="mdl-cell mdl-cell--2-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
	    <input type="text" class="mdl-textfield__input obrigatorio validar_numeros" aviso="Controle Fácil" name="controle_facil" id="controle_facil" value="<?php echo $dados['input']->row()->controle_facil; ?>" size="50" maxlength="4"/>
	    <label class="mdl-textfield__label" for="controle_facil">Controle Fácil</label>
	 </div>

	 <div class="mdl-cell mdl-cell--2-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
	    <input type="text" class="mdl-textfield__input obrigatorio validar_numeros" aviso="Controle Boleto" name="controle_giga" id="controle_giga" value="<?php echo $dados['input']->row()->controle_giga; ?>" size="50" maxlength="4"/>
	    <label class="mdl-textfield__label" for="controle_giga">Controle Boleto</label>
	 </div>

	 <div class="mdl-cell mdl-cell--2-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
	    <input type="text" class="mdl-textfield__input obrigatorio validar_numeros" aviso="Recarga" name="recarga" id="recarga" value="<?php echo $dados['input']->row()->recarga; ?>" size="50" maxlength="4"/>
	    <label class="mdl-textfield__label" for="recarga">Recarga</label>
	 </div>

	 <div class="mdl-cell mdl-cell--2-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
	    <input type="text" class="mdl-textfield__input obrigatorio validar_numeros" aviso="Migração" name="migracao" id="migracao" value="<?php echo $dados['input']->row()->migracao; ?>" size="50" maxlength="4"/>
	    <label class="mdl-textfield__label" for="migracao">Migração</label>
	 </div>

	 <div class="mdl-cell mdl-cell--2-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
	    <input type="text" class="mdl-textfield__input obrigatorio validar_numeros" aviso="HC Oficial" name="hc_oficial" id="hc_oficial" value="<?php echo $dados['input']->row()->hc_oficial; ?>" size="50" maxlength="4"/>
	    <label class="mdl-textfield__label" for="hc_oficial">HC Oficial</label>
	 </div>

	 <div class="mdl-cell mdl-cell--2-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
	    <input type="text" class="mdl-textfield__input obrigatorio validar_numeros" aviso="HC Campo" name="hc_campo" id="hc_campo" value="<?php echo $dados['input']->row()->hc_campo; ?>" size="50" maxlength="4"/>
	    <label class="mdl-textfield__label" for="hc_campo">HC Campo</label>
	 </div>


</div>

<div class="mdl-grid">
	<div class="mdl-cell mdl-cell--12-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
		<label class="mdl-textfield__label" for="aspectos">Aspectos</label>
		<textarea class="mdl-textfield__input obrigatorio" type="text" rows="15" aviso="Aspectos" name="aspectos" id="aspectos"><?php echo $dados['input']->row()->aspectos; ?></textarea>
	</div>

</div>
	
	<div class="mdl-cell mdl-cell--2">
		<button class="-col mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" id="validar_Enviar"><i class="material-icons">done</i>Confirmar Edição</button>	
	</div>


<?php echo form_close(); ?>