		<div class="mdl-grid" align="center">
			<h3>APROVAR ROTEIRO</h3>
		</div>
		<div class="mdl-grid">

			<div class="mdl-cell mdl-cell--3-col" align="left">

				 <strong>Roteiro criado pelo GN : <?php echo $dados['roteiro']->nome; ?></strong>
			<br> <strong>Em : </strong><?php echo $dados['roteiro']->data_solicitacao; ?>
			<br> <strong>Inicio em : </strong><?php echo $dados['roteiro']->inicio_roteiro; ?> 
			<br> <strong>Termino em : </strong><?php echo $dados['roteiro']->fim_roteiro; ?>
			
			<?php if($dados['roteiro']->status == 1){ ?>
				<br> <strong>Aprovado em : </strong><?php echo $dados['roteiro']->data_aprovacao; ?>
			<?php } else if($dados['roteiro']->status == 2) { ?>
				<br> <strong>Rejeitado em : </strong><?php echo $dados['roteiro']->data_aprovacao; ?>
			<?php } ?>

			<br> <strong>Total de KM : </strong> <?php echo $dados['roteiro']->km_total; ?>
			<br> <strong>Total em Horas : </strong>	<?php echo $dados['roteiro']->horas_total; ?>
			<br> <strong>Total em PDVs : </strong>	 <span id="totalPdv"></span>
			<br> <strong>Tempo no Trajero : </strong>	<span id="totalPercurso"></span>
			<br> <strong>Observação : </strong><?php echo $dados['roteiro']->obs_roteiro; ?>

			</div>

			<div class="mdl-cell mdl-cell--6-col">
			<?php 

			echo '<ol> ';	

			foreach ($dados['enderecos'] as $key => $value) {
				echo '<li>'.$value->endereco_google.'</li><hr>';
			}

			echo '</ol>';

	 ?>
	 		</div>
		</div>


		<div class="mdl-grid">
			<div class="mdl-cell mdl-cell--3-col"></div>
			<div class="mdl-cell mdl-cell--6-col">

				<?php if($dados['roteiro']->status == 0){ ?>
				
				<a href="<?php echo base_url(); ?>controller_roteiro/aprovacao/<?php echo $dados['roteiro']->id_roteiro ?>/2" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" style="background-color: #d52b1e; color: white;">Rejeitar</a>
				<a href="<?php echo base_url(); ?>controller_roteiro/aprovacao/<?php echo $dados['roteiro']->id_roteiro ?>/1" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored">Aprovar</a>

				<?php } ?>

			</div>

		</div>

<script type="text/javascript">

	if((Math.floor(<?php echo $roteiro->totalPdv; ?>/60)) > 0) { // Mais de uma hora
		
		if ((Math.floor(<?php echo $roteiro->totalPdv; ?>%60)) > 0) { // Mais de um minuto

			$('#totalPdv').text(Math.floor(<?php echo $roteiro->totalPdv; ?>/60) + " Horas e " + Math.floor(<?php echo $roteiro->totalPdv; ?>%60) + " Minutos");

		} else {

			$('#totalPdv').text(Math.floor(<?php echo $roteiro->totalPdv; ?>/60) + " Horas");

		}
      
    } else {
      
      $('#totalPdv').text(Math.floor(<?php echo $roteiro->totalPdv; ?>%60) + " Minutos");

    }

    //***************

    if((Math.floor(<?php echo $roteiro->totalPercurso; ?>/60)) > 0) { // Mais de uma hora
		
		if ((Math.floor(<?php echo $roteiro->totalPercurso; ?>%60)) > 0) { // Mais de um minuto

			$('#totalPercurso').text(Math.floor(<?php echo $roteiro->totalPercurso; ?>/60) + " Horas e " + Math.floor(<?php echo $roteiro->totalPercurso; ?>%60) + " Minutos");

		} else {

			$('#totalPercurso').text(Math.floor(<?php echo $roteiro->totalPercurso; ?>/60) + " Horas");

		}
      
    } else {
      
      $('#totalPercurso').text(Math.floor(<?php echo $roteiro->totalPercurso; ?>%60) + " Minutos");

    }

</script>