<?php echo form_open('controller_carteira/novo');  ?>
<div class="mdl-grid" align="center">
	
	 <div class="mdl-cell mdl-cell--1-col"></div> 
	 <div class="mdl-cell mdl-cell--3-col"> Selecione um  CNPJ para associá-lo ao GN <br> <small>(Só serão listados PDVs sem um GN vinculado atualmente.)</small></div> 
	 
	 <div class="mdl-cell mdl-cell--4-col">
		<select class="obrigatorio" name="id_pdv" id="id_pdv" style="width: 300px;">
			<option value="">SELECIONE UM CNPJ</option>
			<?php foreach ($dados['cnpj'] as $cnpj) {

		    	echo '<option value="'.$cnpj->id_pdv.'">'.$cnpj->cnpj.' - '.$cnpj->rede.'</option>';

			} ?>
		</select>
	</div>

     <div class="mdl-cell mdl-cell--3-col">
		<button class="-col mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" id="validar_enviar" type="submit"><i class="material-icons">done</i>ASSOCIAR</button>	
	 </div>
</div>

<input type="hidden" name="id_usuario" value="<?php echo $dados['usuario']->row()->id_usuario; ?>">

 <?php echo form_close(); ?>

<div class="mdl-grid" align="center">
	 <div class="mdl-cell mdl-cell--2-col">
	 	<?php echo anchor('main/redirecionar/listar-view_listar_carteira', '< VOLTAR', array('class' => 'mdl-navigation__link')); ?>	 	
	 </div> 
</div>

<div align="center">
	<h4>PDV's Vinculados ao usuário(a): <?php echo $dados['usuario']->row()->usuario; ?></h4>
</div>
<hr>

<div align="center" id="load">

	<table class="mdl-data-table mdl-js-data-table mdl-shadow--8dp">
	  <thead>
	    <tr>
	      <th>REDE</th>
	      <th>CNPJ</th>
	      <th>Excluir</th>
	    </tr>
	  </thead>
	  <tbody>
	    <?php foreach ($dados['pdvs'] as $pdv) {
	    	echo '<tr>';
			echo '<td>'.$pdv->rede.'</td>';
			echo '<td>'.$pdv->cnpj.'</td>';
			echo '<td>'.anchor('controller_carteira/remover/'.$pdv->id_pdv.'/'.$dados['usuario']->row()->id_usuario, 'Remover', array('class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect alerta', 'title' => 'Remover', 'alt' => 'Remover', 'style' => 'margin-top: -7px;')).'</td>';
			echo '</tr>';
		} ?>
	  </tbody>
	</table>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$("#id_pdv").select2();
	});
</script>