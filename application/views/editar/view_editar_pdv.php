<script type="text/javascript">
	//history.replaceState({pagina: "listar_usuarios"}, "Novo Usuário", "<?php echo base_url() ?>main/redirecionar/listar-view_listar_pdv");
</script>

<?php echo form_open('controller_pdv/editar_pdv'); 
echo form_fieldset('Editar PDV ID '.$dados['pdv']->row()->id_pdv);
echo '<div class="mdl-grid">
	<div class="mdl-cell mdl-cell--3-col">
		<a href="" id="apagar"><i class="material-icons">clear</i>Limpar campos</a>
	</div>
	<div class="mdl-cell mdl-cell--3-col">
		<a href="" id="voltar" class=""><i class="material-icons">reply</i>Voltar</a>
	</div>
	<div class="mdl-cell mdl-cell--3-col">
		<a href="" id="recarregar" url="'.$_SERVER ['REQUEST_URI'].'"><i class="material-icons">cached</i>Recarregar</a>
	</div>
</div>';
?>

<hr>

<input type="hidden" name="id_pdv" value="<?php echo $dados['pdv']->row()->id_pdv; ?>">

<div class="mdl-grid">

 <div class="mdl-cell mdl-cell--3-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
    <input type="text" class="mdl-textfield__input obrigatorio" aviso="Cód Agente" name="cod_agente" id="cod_agente" value="<?php echo $dados['pdv']->row()->cod_agente; ?>" size="50" maxlength="4"/>
    <label class="mdl-textfield__label" for="cod_agente">Cód Agente</label>
 </div>

 <div class="mdl-cell mdl-cell--3-col">
    <label class="label" for="cnpj">CNPJ</label>
    <input type="text" class="mdl-textfield__input obrigatorio mascara_cnpj" aviso="CNPJ" name="cnpj" id="cnpj" value="<?php echo $dados['pdv']->row()->cnpj; ?>" size="50" maxlength="20"/>
 </div>

   <div class="mdl-cell mdl-cell--2-col">
    
    <label class="label" for="filial">Rede</label>
    <select style="width: 100%" aviso="Rede" name="rede" id="rede">
    	<?php 

    		foreach ($dados['rede'] as $rede) {
    			if ($dados['pdv']->row()->rede == $rede->rede) {
	    			echo '<option value="'.$rede->rede.'" selected>'.$rede->rede.'</option>';
	    		} else {
	    			echo '<option value="'.$rede->rede.'">'.$rede->rede.'</option>';
	    		}
    		}

    	 ?>
    	
    </select>

 </div>

  <div class="mdl-cell mdl-cell--2-col">
    
    <label class="label" for="filial">Razão Social</label>
    <select style="width: 100%" aviso="Razão Social" name="razao_social" id="razao_social">
    	<?php 

    		foreach ($dados['razao_social'] as $razao_social) {
    			if ($dados['pdv']->row()->razao_social == $razao_social->razao_social) {
	    			echo '<option value="'.$razao_social->razao_social.'" selected>'.$razao_social->razao_social.'</option>';
	    		} else {
	    			echo '<option value="'.$razao_social->razao_social.'">'.$razao_social->razao_social.'</option>';
	    		}
    		}

    	 ?>
    	
    </select>

 </div>

</div> <!-- Fecha mdl-grid Linha 1 -->

<div class="mdl-grid">

 <div class="mdl-cell mdl-cell--2-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
    <input type="text" class="mdl-textfield__input obrigatorio" aviso="Cep" name="cep" id="cep" value="<?php echo $dados['pdv']->row()->cep; ?>" size="50" maxlength="9"/>
    <label class="mdl-textfield__label" for="cep">Cep</label>
 </div>

 <div class="mdl-cell mdl-cell--4-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
    <input type="text" class="mdl-textfield__input obrigatorio" aviso="Lougradouro" name="lougradouro" id="lougradouro" value="<?php echo $dados['pdv']->row()->lougradouro; ?>" size="50" maxlength="200"/>
    <label class="mdl-textfield__label" for="lougradouro">Lougradouro</label>
 </div>

 <div class="mdl-cell mdl-cell--2-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
    <input type="text" class="mdl-textfield__input obrigatorio" aviso="Número" name="numero_local" id="numero_local" value="<?php echo $dados['pdv']->row()->numero_local; ?>" size="50" maxlength="10"/>
    <label class="mdl-textfield__label" for="numero_local">Número</label>
 </div>

 <div class="mdl-cell mdl-cell--3-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
    <input type="text" class="mdl-textfield__input obrigatorio" aviso="Cidade" name="cidade" id="cidade" value="<?php echo $dados['pdv']->row()->cidade; ?>" size="50" maxlength="20"/>
    <label class="mdl-textfield__label" for="cidade">Cidade</label>
 </div>

</div> <!-- Fecha mdl-grid Linha 2 -->

<div class="mdl-grid">

 <div class="mdl-cell mdl-cell--2-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
    <input type="text" class="mdl-textfield__input" aviso="Complemento" name="complemento" id="complemento" value="<?php echo $dados['pdv']->row()->complemento; ?>" size="50" maxlength="50"/>
    <label class="mdl-textfield__label" for="complemento">Complemento</label>
 </div>

 <div class="mdl-cell mdl-cell--4-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
    <input type="text" class="mdl-textfield__input obrigatorio" aviso="Bairro" name="bairro" id="bairro" value="<?php echo $dados['pdv']->row()->bairro; ?>" size="50" maxlength="30"/>
    <label class="mdl-textfield__label" for="bairro">Bairro</label>
 </div>

 <div class="mdl-cell mdl-cell--3-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
    <input type="text" class="mdl-textfield__input validar_decimais" aviso="Latitude" name="lat_pdv" id="lat_pdv" value="<?php echo $dados['pdv']->row()->lat_pdv; ?>" size="50" maxlength="30"/>
    <label class="mdl-textfield__label" for="lat_pdv">Latitude</label>
 </div>

  <div class="mdl-cell mdl-cell--3-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
    <input type="text" class="mdl-textfield__input validar_decimais" aviso="Longitude" name="lng_pdv" id="lng_pdv" value="<?php echo $dados['pdv']->row()->lng_pdv; ?>" size="50" maxlength="30"/>
    <label class="mdl-textfield__label" for="lng_pdv">Longitude</label>
 </div>

</div> <!-- Fecha mdl-grid Linha 3 -->

<div class="mdl-grid">

 <div class="mdl-cell mdl-cell--2-col">
    
    <label class="label" for="filial">Fílial</label>
    <select style="width: 100%" aviso="Fílial" name="filial" id="filial">
    	<?php 

    		if ($dados['pdv']->row()->filial == 2) {
    			echo '<option value="1">Filial 1</option>
    				<option value="2" selected>Filial 2</option>';
    		} else {
    			echo '<option value="1" selected>Filial 1</option>
    				<option value="2">Filial 2</option>';
    		}

    	 ?>
    	
    </select>

 </div>

 <div class="mdl-cell mdl-cell--2-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
    <input type="text" class="mdl-textfield__input obrigatorio" aviso="Código interno" name="cod_interno_rede" id="cod_interno_rede" value="<?php echo $dados['pdv']->row()->cod_interno_rede; ?>" size="50" maxlength="5"/>
    <label class="mdl-textfield__label" for="cod_interno_rede">Código interno</label>
 </div>

  <div class="mdl-cell mdl-cell--2-col">
    
    <label class="label" for="curva">Curva</label>
    <select style="width: 100%" aviso="Curva" name="curva" id="curva">
        <option value="">Selecione...</option>
        <?php 

            if ($dados['pdv']->row()->curva == 'A') {
                echo '<option value="A" selected>A</option>
                    <option value="B">B</option>
                    <option value="C">C</option>';

            } else if ($dados['pdv']->row()->curva == 'B') {
               echo '<option value="A">A</option>
                    <option value="B" selected>B</option>
                    <option value="C">C</option>';
            } else if ($dados['pdv']->row()->curva == 'C') {
                echo '<option value="A">A</option>
                    <option value="B">B</option>
                    <option value="C" selected>C</option>';
            } else {
                echo '<option value="A">A</option>
                    <option value="B">B</option>
                    <option value="C">C</option>';
            }

         ?>
        
    </select>

 </div>

 <div class="mdl-cell mdl-cell--2-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
    <input type="text" class="mdl-textfield__input obrigatorio" aviso="Varejo" name="varejo" id="varejo" value="<?php echo $dados['pdv']->row()->varejo; ?>" size="50" maxlength="60"/>
    <label class="mdl-textfield__label" for="varejo">Varejo</label>
 </div>

 <div class="mdl-cell mdl-cell--2-col">
    
    <label class="label" for="filial">GN</label>
    <select style="width: 100%" aviso="GN" name="key_account" id="key_account">
    	<?php 

    		foreach ($dados['usuarios'] as $usuario) {
    			if ($dados['pdv']->row()->key_account == $usuario->id_usuario) {
	    			echo '<option value="'.$usuario->id_usuario.'" selected>'.$usuario->usuario.'</option>';
	    		} else {
	    			echo '<option value="'.$usuario->id_usuario.'">'.$usuario->usuario.'</option>';
	    		}
    		}

    		

    	 ?>
    	
    </select>

 </div>

</div> <!-- Fecha mdl-grid Linha 2 -->

<div class="mdl-grid">

	<div class="mdl-cell mdl-cell--2-col">
		<button class="-col mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" id="validar_Enviar"><i class="material-icons">done</i>Editar PDV</button>	
	</div>

</div> <!-- Fecha mdl-grid Linha 2 -->

<?php echo form_fieldset_close();
echo form_close(); ?>