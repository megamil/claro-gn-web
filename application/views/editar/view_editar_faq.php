<span align="center"><h2>Editar FAQ</h2></span>

<hr>

<?php echo form_open_multipart('controller_faq/editar_faq'); ?>

  <div class="mdl-cell mdl-cell--3-col">

  </div>

<div class="mdl-grid">

<div class="mdl-cell mdl-cell--3-col">
	<input type="hidden" name="id_faq" value="<?php echo $dados->row()->id_faq; ?>">
</div>

<div class="mdl-cell mdl-cell--3-col" style="margin-top: 25px;">
  
    Sistema:
    <select name="tipo_faq" id="tipo_faq" style="width: 100px;">
    <?php if ($dados->row()->tipo_faq == 2){

    		echo '<option value="1">Android</option>
      			<option value="2" selected>iOS</option>';

    	} else {

    		echo '<option value="1" selected>Android</option>
      			<option value="2">iOS</option>';

    	} ?>
    	
      
    </select>

</div>
  <div class="mdl-cell mdl-cell--3-col">
    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
    <input class="mdl-textfield__input obrigatorio" type="text" id="titulo_faq" name="titulo_faq" aviso="Título" maxlength="140" value="<?php echo $dados->row()->titulo_faq; ?>">
    <label class="mdl-textfield__label" for="titulo_faq">Título</label>
  </div>
  </div>
</div>

<div class="mdl-grid">

<div class="mdl-cell mdl-cell--4-col"></div>

  <div class="mdl-cell mdl-cell--4-col" align="center">
    <label class="label" for="imagem">UPLOAD IMAGEM</label>
    <input type="file" name="imagem" id="arquivo">
  </div>

</div>


<div class="mdl-grid">
  <div class="mdl-cell mdl-cell--3-col"></div>

	  <div class="mdl-cell mdl-cell--6-col mdl-textfield mdl-js-textfield is-invalid mdl-textfield--floating-label">
	    
	    <textarea class="mdl-textfield__input" type="text" Aviso="Descrição" id="descricao_faq" name="descricao_faq">

      <?php if($this->session->flashdata('descricao_faq') != "" ) {
              echo $this->session->flashdata('descricao_faq'); 
            } else {
              echo $dados->row()->descricao_faq; 
            } ?>
        
      </textarea>

	  </div>

</div>
<div class="mdl-grid">
	<div class="mdl-cell mdl-cell--4-col"></div>
	<div class="mdl-cell mdl-cell--4-col">
		<button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" id="validar_Enviar">
  			Editar FAQ
		</button>
	</div>
</div>


<script type="text/javascript">

    (function () {
      var container = document.getElementById("descricao_faq").parentNode;
      container.addEventListener('mdl-componentupgraded', function(){
        tinymce.init({
          selector: 'textarea',
		  height: 500,
		  theme: 'modern',
		  plugins: [
		    'advlist autolink lists link image charmap print preview hr anchor pagebreak',
		    'searchreplace wordcount visualblocks visualchars code fullscreen',
		    'insertdatetime media nonbreaking save table contextmenu directionality',
		    'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
		  ],
		  toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
		  toolbar2: 'print preview media | forecolor backcolor emoticons | codesample',
		  image_advtab: true,
		  templates: [
		    { title: 'Test template 1', content: 'Test 1' },
		    { title: 'Test template 2', content: 'Test 2' }
		  ],
		  content_css: [
		    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i'
		  ]
        });
      }, false);
    })();

    $('#arquivo').change(function(){
      $('form').submit();
    });
</script>
