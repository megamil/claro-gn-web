<style type="text/css">
	.tela {
		padding: 50px;
	}
</style>

<div class="tela">
	<h3>Editar Metas</h3>
	<?php echo form_open('controller_seguranca/editar_meta');  ?>
	<table class="mdl-data-table mdl-js-data-table mdl-shadow--8dp semDataTable">
	  <thead>
	    <tr>
	      <th>GN</th>
	      <th>Pré Pago</th>
	      <th>Controle</th>
	      <th>Boleto</th>
	      <th>Total</th>
	      <th>Recarga</th>
	      <th>Migração</th>
	    </tr>
	  </thead>
	  <tbody>
	    <?php foreach ($dados as $meta) {
	    	echo '<tr>';
	    	echo '<td>'.strtoupper($meta->usuario).'</td>';
	    	
	    	$id = $meta->id_usuario;

			echo '<td> <input name="pre'.$id.'" class="mdl-textfield__input validar_numeros" value="'.$meta->meta_pre.'"> 
				  </td>';

			echo '<td> <input name="controle'.$id.'" class="mdl-textfield__input validar_numeros" value="'.$meta->meta_controle.'"> 
				  </td>';

			echo '<td> <input name="boleto'.$id.'" class="mdl-textfield__input validar_numeros" value="'.$meta->meta_boleto.'"> 
				  </td>';

			echo '<td> <input name="total'.$id.'" class="mdl-textfield__input validar_numeros" value="'.$meta->meta_total.'"> 
				  </td>';

			echo '<td> <input name="recarga'.$id.'" class="mdl-textfield__input validar_numeros" value="'.$meta->meta_recarga.'"> 
				  </td>';

			echo '<td> <input name="migracao'.$id.'" class="mdl-textfield__input validar_numeros" value="'.$meta->meta_migracao.'"> 
				  </td>';

		} ?>
	  </tbody>
	</table>

	<div>
		<button class="-col mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent"> 
				  <i class="material-icons">done</i>Registrar Metas</button>	

	</div>

	<?php echo form_close(); ?>
</div>	

