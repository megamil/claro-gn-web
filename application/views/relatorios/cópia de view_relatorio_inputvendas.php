<style type="text/css">
	.destaque {
		font-size: 46px;
	}

	.adicional{
		font-size: 46px;
	}

	.contorno{
		border-style: solid;
		border-width: 1px;
		border-color: #dde;
	}

	#corpo {
		width: 90%;
	}

</style>

<div class="mdl-grid" style="margin-left: -15px;">

	<div class="mdl-cell mdl-cell--2-col">
	    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
	    	<label class="label" for="data_filtro">Período</label>
	    	<select class="select" name="data_filtro" id="data_filtro" style="width: 100%;">
	    		<!-- <option value="0">Todo Período</option>
	    		<option value="1">Hoje</option>
	    		<option value="2">Última Semana</option>
	    		<option value="3">Última Quinzena</option>
	    		<option value="4">Último Mês</option>
	    		<option value="5">Último Bimestre</option>
	    		<option value="6">Último Trimestre</option>
	    		<option value="7">Último Semestre</option>
	    		<option value="8">Último Ano</option> -->
	    		<option value="9">Personalizado</option>
	    	</select>
	    </div>
	</div>

	<div class="mdl-cell mdl-cell--2-col" id="de_filtro" style="margin-top: 25px">
		<label class="label" for="de">De</label>
		<input class="mdl-textfield__input mascara_data obrigatorio" type="text" id="de" name="de" aviso="Data Limite">
	</div>

	<div class="mdl-cell mdl-cell--2-col" id="ate_filtro" style="margin-top: 25px">
		<label class="label" for="ate">Até</label>
		<input class="mdl-textfield__input mascara_data obrigatorio" type="text" id="ate" name="ate" aviso="Data Limite">
	</div>

	<!-- <div class="mdl-cell mdl-cell--2-col">
	    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
	    	<label class="label" for="habitado_filtro">Habitado</label>
	    	<select class="select" name="habitado_filtro" id="habitado_filtro" style="width: 100%;">
	    		<option value="">Todos</option>
	    		<option value="1">Habitados</option>
	    		<option value="0">Desabitados</option>
	    	</select>
	    </div>
	</div> -->

	<div class="mdl-cell mdl-cell--3-col" style="margin-top: 30px;">
	<a type="button" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" id="filtrar">Aplicar Filtro</a>
	</div>

</div>

<div class="mdl-grid contorno" style="background-color: white;" align="left">
		
	<b>INPUT DE VENDAS</b>

</div>

<div class="mdl-grid contorno" style="background-color: white;" align="left">

	<div class="mdl-cell mdl-cell--12-col">

	<table class="mdl-data-table" width="100%">
		<thead>
			<tr>
				<th></th>
				<th>Pré Pago</th>
				<th>Controle</th>
				<th>Recarga</th>
				<th>Banda Larga - Pré Pago</th>
				<th>HC Oficial</th>
				<th>HC em Campo</th>
				<th>% Campo</th>
				<th>PHC Pré</th>
				<th>PHC Controle</th>
				<th>PHC Recarga</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($dados['geral'] as $value) {
				echo '<tr>';

				echo '<td>'.$value->filial.'</td>';
				echo '<td>'.$value->pre.'</td>';
				echo '<td>'.$value->controle.'</td>';
				echo '<td>'.$value->recarga.'</td>';
				echo '<td>'.$value->banda_pre.'</td>';
				echo '<td>'.$value->hc_oficial.'</td>';
				echo '<td>'.$value->hc_campo.'</td>';
				echo '<td>'.round((divisao(($value->hc_campo * 100 ),$value->hc_oficial)),2).' %</td>';
				echo '<td>'.round((divisao($value->pre,$value->hc_campo)),2).'</td>';
				echo '<td>'.round((divisao($value->controle,$value->hc_campo)),2).'</td>';
				echo '<td>'.round((divisao($value->recarga,$value->hc_campo)),2).'</td>';

				echo '</tr>';
			} 

			function divisao($v1,$v2){
				if ($v1 > 0 && $v2 > 0) {
					return ($v1/$v2);
				} else {
					return 0;
				}
			}

			?>
		</tbody>
	</table>

	</div>

</div>

<div class="mdl-grid contorno" style="background-color: white;" align="left">
	<div class="mdl-cell mdl-cell--5-col" align="left">

	<table class="mdl-data-table">
		<thead>
			<tr>
				<th></th>
				<th>Controle Total</th>
				<th>Controle Fácil</th>
				<th>%</th>
				<th>Controle Giga</th>
				<th>%</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($dados['geral_controle'] as $value) {
				echo '<tr>';

				echo '<td>'.$value->filial.'</td>';
				echo '<td>'.$value->controle_total.'</td>';
				echo '<td>'.$value->controle_facil.'</td>';
				echo '<td>'.round((divisao(($value->controle_facil * 100 ),($value->controle_giga+$value->controle_facil))),2).' %</td>';
				echo '<td>'.$value->controle_giga.'</td>';
				echo '<td>'.round((divisao(($value->controle_giga * 100 ),($value->controle_giga+$value->controle_facil))),2).' %</td>';

				echo '</tr>';
			} ?>
		</tbody>
	</table>

	</div>	
</div>

<div class="mdl-grid contorno" style="background-color: white;" align="left">
	<div class="mdl-cell mdl-cell--12-col" align="left">

	<h4>PRÉ PAGO</h4>
	<!-- <select class="select" id="ano_pre">
		<?php 

			// $ano = date('Y');
			// $ano_count = 2016;

			// while ($ano >= $ano_count) {
			// 	echo '<option selected>'.$ano_count.'</option>';
			// 	$ano_count++;
			// }

		 ?>
	</select>

	<select class="select" id="mes_pre">
		<option>Janeiro</option>
		<option>Fevereiro</option>
		<option>Março</option>
		<option>Abril</option>
		<option>Maio</option>
		<option>Junho</option>
		<option>Julho</option>
		<option>Agosto</option>
		<option>Setembro</option>
		<option>Outubro</option>
		<option>Novembro</option>
		<option>Dezembro</option>
	</select> -->

		<table class="mdl-data-table" style="width: 100%">
		<thead>
			<tr>
				<th></th>
				<?php for ($i=1; $i < 32; $i++) { 
					echo '<th>'.$i.'</th>';
				} ?>
				<th>TOTAL</th>
			</tr>
		</thead>
		<tbody>
			
			<?php 

				$array_f1 = array();
				$array_f2 = array();

				echo '<tr>';
				echo '<td>F1</td>';

				for ($i=1; $i < 32; $i++) { 
					$valor = 0;

					foreach ($dados['pre_f1'] as $f1) {
						if ($f1->data == $i) {
							$valor = $f1->pre;
						}
					}

					array_push($array_f1, $valor);
					echo '<td>'.$valor.'</td>';
				}

				echo '<td>'.array_sum($array_f1).'</td>';
				echo '</tr>';

				//F2
				echo '<tr>';
				echo '<td>F2</td>';

				for ($i=1; $i < 32; $i++) { 
					$valor = 0;

					foreach ($dados['pre_f2'] as $f2) {
						if ($f2->data == $i) {
							$valor = $f2->pre;
						}
					}

					array_push($array_f2, $valor);
					echo '<td>'.$valor.'</td>';
				}

				echo '<td>'.array_sum($array_f2).'</td>';
				echo '</tr>';

				//CANAL
				echo '<tr>';
				echo '<td>CANAL</td>';

				for ($i=0; $i < 31; $i++) { 
					
					echo '<td>'.($array_f1[$i] + $array_f2[$i]).'</td>';

				}

				echo '<td>'.(array_sum($array_f1) + array_sum($array_f2)).'</td>';
				echo '</tr>';

			 ?>



		</tbody>
		</table>

	</div>
</div>

<div class="mdl-grid contorno" style="background-color: white;" align="left">
	<div class="mdl-cell mdl-cell--12-col" align="left">
		<h4>CONTROLE TOTAL</h4>
		<!-- <select class="select" id="ano_controle">
			<?php 

				// $ano = date('Y');
				// $ano_count = 2016;

				// while ($ano >= $ano_count) {
				// 	echo '<option selected>'.$ano_count.'</option>';
				// 	$ano_count++;
				// }

			 ?>
		</select>

		<select class="select" id="mes_controle">
			<option>Janeiro</option>
			<option>Fevereiro</option>
			<option>Março</option>
			<option>Abril</option>
			<option>Maio</option>
			<option>Junho</option>
			<option>Julho</option>
			<option>Agosto</option>
			<option>Setembro</option>
			<option>Outubro</option>
			<option>Novembro</option>
			<option>Dezembro</option>
		</select> -->

		<table class="mdl-data-table" style="width: 100%">
		<thead>
			<tr>
				<th></th>
				<?php for ($i=1; $i < 32; $i++) { 
					echo '<th>'.$i.'</th>';
				} ?>
				<th>TOTAL</th>
			</tr>
		</thead>
		<tbody>
			
			<?php 

				$array_f1 = array();
				$array_f2 = array();

				echo '<tr>';
				echo '<td>F1</td>';

				for ($i=1; $i < 32; $i++) { 
					$valor = 0;

					foreach ($dados['controle_f1'] as $f1) {
						if ($f1->data == $i) {
							$valor = $f1->controle;
						}
					}

					array_push($array_f1, $valor);
					echo '<td>'.$valor.'</td>';
				}

				echo '<td>'.array_sum($array_f1).'</td>';
				echo '</tr>';

				//F2
				echo '<tr>';
				echo '<td>F2</td>';

				for ($i=1; $i < 32; $i++) { 
					$valor = 0;

					foreach ($dados['controle_f2'] as $f2) {
						if ($f2->data == $i) {
							$valor = $f2->controle;
						}
					}

					array_push($array_f2, $valor);
					echo '<td>'.$valor.'</td>';
				}

				echo '<td>'.array_sum($array_f2).'</td>';
				echo '</tr>';

				//CANAL
				echo '<tr>';
				echo '<td>CANAL</td>';

				for ($i=0; $i < 31; $i++) { 
					
					echo '<td>'.($array_f1[$i] + $array_f2[$i]).'</td>';

				}

				echo '<td>'.(array_sum($array_f1) + array_sum($array_f2)).'</td>';
				echo '</tr>';

			 ?>



		</tbody>
		</table>

	</div>
</div>

<!-- FILIAL 1 ********** -->

<div class="mdl-grid contorno" style="background-color: white;" align="left">

<h4>Filial 1</h4>

	<div class="mdl-cell mdl-cell--12-col">

	<table class="mdl-data-table" width="100%">
		<thead>
			<tr>
				<th></th>
				<th>Pré Pago</th>
				<th>Controle</th>
				<th>Recarga</th>
				<th>Banda Larga - Pré Pago</th>
				<th>HC Oficial</th>
				<th>HC em Campo</th>
				<th>% Campo</th>
				<th>PHC Pré</th>
				<th>PHC Controle</th>
				<th>PHC Recarga</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($dados['geral_usuarios_f1'] as $value) {
				echo '<tr>';

				echo '<td>'.$value->usuario.'</td>';
				echo '<td>'.$value->pre.'</td>';
				echo '<td>'.$value->controle.'</td>';
				echo '<td>'.$value->recarga.'</td>';
				echo '<td>'.$value->banda_pre.'</td>';
				echo '<td>'.$value->hc_oficial.'</td>';
				echo '<td>'.$value->hc_campo.'</td>';
				echo '<td>'.round(divisao(($value->hc_campo * 100 ),$value->hc_oficial),2).' %</td>';

				echo '<td>'.round(divisao($value->pre,$value->hc_campo),2).'</td>';
				echo '<td>'.round(divisao($value->controle,$value->hc_campo),2).'</td>';
				echo '<td>'.round(divisao($value->recarga,$value->hc_campo),2).'</td>';

				echo '</tr>';
			} ?>
		</tbody>
	</table>

	</div>

</div>

<div class="mdl-grid contorno" style="background-color: white;" align="left">
	<div class="mdl-cell mdl-cell--5-col" align="left">

	<table class="mdl-data-table">
		<thead>
			<tr>
				<th></th>
				<th>Controle Total</th>
				<th>Controle Fácil</th>
				<th>%</th>
				<th>Controle Giga</th>
				<th>%</th>
			</tr>
		</thead>
		<tbody>
			<?php 
			
			$controle_facil_count = array();
			$controle_giga_count = array();

			foreach ($dados['geral_controle_usuario_f1'] as $value) {
				echo '<tr>';

				echo '<td>'.$value->usuario.'</td>';
				echo '<td>'.$value->controle_total.'</td>';
				echo '<td>'.$value->controle_facil.'</td>';
				echo '<td>'.round((divisao(($value->controle_facil * 100 ),($value->controle_giga+$value->controle_facil))),2).' %</td>';
				echo '<td>'.$value->controle_giga.'</td>';
				echo '<td>'.round((divisao(($value->controle_giga * 100 ),($value->controle_giga+$value->controle_facil))),2).' %</td>';

				echo '</tr>';

				array_push($controle_facil_count,$value->controle_facil);
				array_push($controle_giga_count,$value->controle_giga);
			} ?>
		</tbody>
	</table>

	</div>	

	<div class="mdl-cell mdl-cell--2-col"></div>

	<div class="mdl-cell mdl-cell--5-col contorno" style="">
		<div id="grafico_pizza" style="width:100%; height: 90%;"></div>
	</div>

</div>

<div class="mdl-grid contorno">
	<h4>Filial 1 Controle Pré</h4>
	<table class="mdl-data-table" style="width: 100%">
		<thead>
			<tr>
				<th></th>
				<?php for ($i=1; $i < 32; $i++) { 
					echo '<th>'.$i.'</th>';
				} ?>
				<th>TOTAL</th>
			</tr>
		</thead>
		<tbody>
			
			<?php 


			foreach ($dados['usuarios_iv'] as $usuario) {
		
				$array_f1 = array();

				echo '<tr>';
				echo '<td>'.$usuario->usuario.'</td>';

				for ($i=1; $i < 32; $i++) { 
					$valor = 0;

					foreach ($dados['detalhado_usuario_pre_f1'] as $f1) {
						if ($f1->data == $i && $usuario->fk_usuario == $f1->id_usuario) {
							$valor = $f1->pre;
						}
					}

					array_push($array_f1, $valor);
					echo '<td>'.$valor.'</td>';
				}

				echo '<td>'.array_sum($array_f1).'</td>';
				echo '</tr>';

			}


			 ?>



		</tbody>
		</table>
</div>


<div class="mdl-grid contorno" style="background-color: white;" align="left">

<h4>Filial 2</h4>

	<div class="mdl-cell mdl-cell--12-col">

	<table class="mdl-data-table" width="100%">
		<thead>
			<tr>
				<th></th>
				<th>Pré Pago</th>
				<th>Controle</th>
				<th>Recarga</th>
				<th>Banda Larga - Pré Pago</th>
				<th>HC Oficial</th>
				<th>HC em Campo</th>
				<th>% Campo</th>
				<th>PHC Pré</th>
				<th>PHC Controle</th>
				<th>PHC Recarga</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($dados['geral_usuarios_f2'] as $value) {
				echo '<tr>';

				echo '<td>'.$value->usuario.'</td>';
				echo '<td>'.$value->pre.'</td>';
				echo '<td>'.$value->controle.'</td>';
				echo '<td>'.$value->recarga.'</td>';
				echo '<td>'.$value->banda_pre.'</td>';
				echo '<td>'.$value->hc_oficial.'</td>';
				echo '<td>'.$value->hc_campo.'</td>';
				echo '<td>'.round(divisao(($value->hc_campo * 100 ),$value->hc_oficial),2).' %</td>';

				echo '<td>'.round((divisao($value->pre,$value->hc_campo)),2).'</td>';
				echo '<td>'.round((divisao($value->controle,$value->hc_campo)),2).'</td>';
				echo '<td>'.round((divisao($value->recarga,$value->hc_campo)),2).'</td>';

				echo '</tr>';
			} ?>
		</tbody>
	</table>

	</div>

</div>

<div class="mdl-grid contorno" style="background-color: white;" align="left">
	<div class="mdl-cell mdl-cell--5-col" align="left">

	<table class="mdl-data-table">
		<thead>
			<tr>
				<th></th>
				<th>Controle Total</th>
				<th>Controle Fácil</th>
				<th>%</th>
				<th>Controle Giga</th>
				<th>%</th>
			</tr>
		</thead>
		<tbody>
			<?php 

			$controle_facil_count2 = array();
			$controle_giga_count2 = array();


			foreach ($dados['geral_controle_usuario_f2'] as $value) {
				echo '<tr>';

				echo '<td>'.$value->usuario.'</td>';
				echo '<td>'.$value->controle_total.'</td>';
				echo '<td>'.$value->controle_facil.'</td>';
				echo '<td>'.round((divisao(($value->controle_facil * 100 ),($value->controle_giga+$value->controle_facil))),2).' %</td>';
				echo '<td>'.$value->controle_giga.'</td>';
				echo '<td>'.round((divisao(($value->controle_giga * 100 ),($value->controle_giga+$value->controle_facil))),2).' %</td>';

				echo '</tr>';

				array_push($controle_facil_count2,$value->controle_facil);
				array_push($controle_giga_count2,$value->controle_giga);

			} 

			?>
		</tbody>
	</table> 

	</div>	

	<div class="mdl-cell mdl-cell--2-col"></div>

	<div class="mdl-cell mdl-cell--5-col contorno" style=" padding: 5px;">
		<div id="grafico_pizza2" style="width:100%; height: 90%;"></div>
	</div> 

</div>

<div class="mdl-grid" style="margin-left: -15px">
	<div class="mdl-cell mdl-cell--3-col" align="left">
	<a type="button" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" target="_blank" style="color: white;" href="<?php echo base_url() ?>main/redirecionar/relatorios-view_historico_inputvendas">Histórico de Inputs</a>
	</div>
</div>


<script type="text/javascript">
	$(document).ready(function(){

		var grafico_pizza;
		var grafico_pizza2;

		window.onload = function () {
			grafico_pizza = new CanvasJS.Chart("grafico_pizza",{

				data: [
				{
					type: "pie",
					indexLabelFontColor: "black",    
					indexLabelLineColor: "darkgrey",
					indexLabelFontWeight: "bold",
					indexLabelFontSize: 15,
					indexLabelFontFamily: "Garamond",
					toolTipContent: "{name} #percent% ({y})", 
					indexLabel: "{name} #percent% ({y})", 
					showInLegend: true,
					dataPoints: [
						{ y: <?php echo array_sum($controle_facil_count); ?> , name: "Controle Giga", color: "#5cb85c", indexLabel: "Controle Giga #percent% ({y})"},
						{ y: <?php echo array_sum($controle_giga_count); ?> , name: "Controle Fácil", color: "#d9534f", indexLabel: "Controle Fácil #percent% ({y})"}
						
					]
				}
				]
			});

			grafico_pizza.render();
		 
			grafico_pizza2 = new CanvasJS.Chart("grafico_pizza2",{

				data: [
				{
					type: "pie",
					indexLabelFontColor: "black",    
					indexLabelLineColor: "darkgrey",
					indexLabelFontWeight: "bold",
					indexLabelFontSize: 15,
					indexLabelFontFamily: "Garamond",
					toolTipContent: "{name} #percent% ({y})", 
					indexLabel: "{name} #percent% ({y})", 
					showInLegend: true,
					dataPoints: [
						{ y: <?php echo array_sum($controle_facil_count2); ?> , name: "Controle Giga", color: "#5cb85c", indexLabel: "Controle Giga #percent% ({y})"},
						{ y: <?php echo array_sum($controle_giga_count2); ?> , name: "Controle Fácil", color: "#d9534f", indexLabel: "Controle Fácil #percent% ({y})"}
						
					]
				}
				]
			});

			grafico_pizza2.render();
		}

		$('#filtrar').click(function(){
			
			var de = $('#de').val().replace('/','-');
			de = de.replace('/','-');

			var ate = $('#ate').val().replace('/','-');
			ate = ate.replace('/','-');

			window.location.href = "<?php echo base_url() ?>main/redirecionar/relatorios-view_relatorio_inputvendas/"+de+"/"+ate+"/";//+$('#habitado_filtro').val();
		});

	});
</script>