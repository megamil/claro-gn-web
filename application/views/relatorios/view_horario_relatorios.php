
<script type="text/javascript">
$(document).ready(function(){

	$('#print').click(function(){
		print();
	});

	$('select').select2();

	// $('#de_filtro').hide();
	// $('#ate_filtro').hide();

	$('#data_filtro').change(function(){

		if ($(this).val() == 9) {
			$('#de_filtro').show();
			$('#ate_filtro').show();
		} else {
			$('#de_filtro').hide();
			$('#ate_filtro').hide();

		}

	});

	//Caso vá específicar uma Filial o gráfico deixa de fazer sentido.
	$('#filial_filtro').change(function(){
		
		if($(this).val() > 0){
			$('#piechart_3d').hide();
		} else {
			$('#piechart_3d').show();
		}


	});

	$('#de, #ate, #gn_filtro').change(function(){
		ajax();
	});

	$('#load').hide();

	function ajax(){

		$('#load').show();

		$.ajax({
	      url: "<?php echo base_url(); ?>controller_relatorios/ajax_horario_especifico",
	      type: "post",
	      data: {data_filtro: $("#data_filtro").val(),
					de: $("#de").val(),
					ate: $("#ate").val(),
					filial_filtro: $("#filial_filtro").val(),
					gn_filtro: $("#gn_filtro").val()},
	  	datatype: 'json',
	      success: function(data){

	            $('#f1').text(data['filial1']);
	            $('#f2').text(data['filial2']);

	            $('#primeiroCh').text(data['min']);
	            $('#ultimoCh').text(data['max']);
	            $('#geralCh').text(data['media']);
	            
	            grafico_pizza.options.data[0].dataPoints[0].y = parseInt(data['graficof1']);
				grafico_pizza.options.data[0].dataPoints[1].y = parseInt(data['graficof2']);
				grafico_pizza.render();

				$('#loadDetalhes').load('<?php echo base_url() ?>controller_relatorios/ajax_detalhes_horarios',{de: $("#de").val(),
					ate: $("#ate").val()},function(){

						$('#load').hide();

				});


	      },
	      error:function(){
	          
	      }   
	    });

	 //    $(".semDataTable").load("<?php //echo base_url(); ?>controller_relatorios/ajax_visitaspdvc_filtro_lista",{data_filtro: $("#data_filtro").val(),
		// 	de: $("#de").val(),
		// 	ate: $("#ate").val(),
		// 	filial_filtro: $("#filial_filtro").val(),
		// 	gn_filtro: $("#gn_filtro").val()}, function (){

					

		// });

	}

	$('#pdf_geral').click(function(){

			var data_filtro = $("#data_filtro").val()
			var de = $("#de").val()
			var ate = $("#ate").val()
			var gn_filtro = $("#gn_filtro").val()

			var url = "<?php echo base_url(); ?>controller_pdf/horario_pdf?de="+de+"&ate="+ate+"&gn_filtro="+gn_filtro+"&=data_filtro"+data_filtro+"&nomeGN="+$("#gn_filtro :selected").text();

			window.open(url, '_blank');

		});

	$('#excel_geral').click(function(){

			var data_filtro = $("#data_filtro").val()
			var de = $("#de").val()
			var ate = $("#ate").val()
			var gn_filtro = $("#gn_filtro").val()

			var url = "<?php echo base_url(); ?>controller_excel/horario_excel?de="+de+"&ate="+ate+"&gn_filtro="+gn_filtro+"&=data_filtro"+data_filtro+"&nomeGN="+$("#gn_filtro :selected").text();

			$.ajax({
		      url: url,
		      type: "post", 
		  	datatype: 'json',
		      success: function(data){
		      	window.location = url;
		      },
		      error:function(){
		          
		      }   
		    });

		});


	var grafico_pizza;

	window.onload = function () {
		grafico_pizza = new CanvasJS.Chart("grafico_pizza",{

			data: [
			{
				type: "pie",
				indexLabelFontColor: "black",    
				indexLabelLineColor: "darkgrey",
				indexLabelFontWeight: "bold",
				indexLabelFontSize: 15,
				indexLabelFontFamily: "Garamond",
				toolTipContent: "{name} #percent% ({y})", 
				indexLabel: "{name} #percent% ({y})", 
				showInLegend: true,
				dataPoints: [
					{ y: 0 , name: "Filial 2", color: "#5cb85c", indexLabel: "Filial 2 #percent% ({y})"},
					{ y: 0 , name: "Filial 1", color: "#d9534f", indexLabel: "Filial 1 #percent% ({y})"}
					
				]
			}
			]
		});

		grafico_pizza.render();
	}


});

</script>

<style type="text/css">
	.destaque {
		font-size: 46px;
	}

	.adicional{
		font-size: 46px;
	}

	.contorno{
		border-style: solid;
		border-width: 1px;
		border-color: #dde;
	}

</style>


<div class="mdl-grid" style="margin-left: -15px;">

	<div class="mdl-cell mdl-cell--3-col">
	    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
	    	<label class="label" for="data_filtro">Período</label>
	    	<select class="select" name="data_filtro" id="data_filtro" style="width: 100%;">
<!-- 	    		<option value="0">Todo Período</option>
	    		<option value="1">Hoje</option>
	    		<option value="2">Última Semana</option>
	    		<option value="3">Última Quinzena</option>
	    		<option value="4">Último Mês</option>
	    		<option value="5">Último Bimestre</option>
	    		<option value="6">Último Trimestre</option>
	    		<option value="7">Último Semestre</option>
	    		<option value="8">Último Ano</option> -->
	    		<option value="9">Personalizado</option>
	    	</select>
	    </div>
	</div>

	<div class="mdl-cell mdl-cell--2-col" id="de_filtro" style="margin-top: 25px">
		<label class="label" for="de">De</label>
		<input class="mdl-textfield__input mascara_data obrigatorio" type="text" id="de" name="de" aviso="Data Limite">
	</div>

	<div class="mdl-cell mdl-cell--2-col" id="ate_filtro" style="margin-top: 25px">
		<label class="label" for="ate">Até</label>
		<input class="mdl-textfield__input mascara_data obrigatorio" type="text" id="ate" name="ate" aviso="Data Limite">
	</div>

	<div class="mdl-cell mdl-cell--2-col">
	    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
	    	<label class="label" for="gn_filtro">GNs</label>
	    	<select class="select" name="gn_filtro" id="gn_filtro" style="width: 100%;">	
    		<?php 

	  			foreach ($dados['usuarios'] as $usuarios) {
	  				echo '<option value="'.$usuarios->id_usuario.'">'.$usuarios->nome.'</option>';
	  			}

	  		 ?>
	    	</select>
	    </div>
	</div>

</div>

<div class="mdl-grid contorno" style="background-color: white;" align="left">
		
	<b>ANALÍSE de HORAS TRABALHADAS</b>

</div>

<div class="mdl-grid" style="background-color: white; padding: 0px;">
	<div class="mdl-cell mdl-cell--3-col contorno" align="center" style="margin: 0px; padding-top: 5px;">
	RESULTADO <br>
	<br>
	<br>
	<br>
	<br>

	<strong>FILIAL 1</strong><br>
	<span class="" id="f1"></span> <br>
	<br><br><br>

	<strong>FILIAL 2</strong><br>
	<span class="" id="f2"></span> <br>
	<br>
	<br>


	</div>

	<div class="mdl-cell mdl-cell--5-col contorno" style="margin: 0px; border-right-style: none;">
		<div id="grafico_pizza" style="width:100%; height: 90%;"></div>
	</div>

	<div class="mdl-cell mdl-cell--4-col contorno" style="margin: 0px; border-right-style: none; padding-top: 60px; font-size: 17">
		<strong>MÉDIA PRIMEIRO CHECK-IN</strong>  <br> <span id="primeiroCh"></span> <br>  
		<br>
		<strong>MÉDIA ÚLTIMO CHECK-OUT</strong> <br> <span id="ultimoCh"></span> <br>
		<br>
		<strong>MÉDIA DE HORAS TRABALHADAS</strong> <br> <span id="geralCh"></span>
	</div>
</div>

<!-- Lista dos gns -->

<div class="mdl-grid contorno" style="background-color: white; margin-top: 10px;">

	<div class="mdl-cell mdl-cell--12-col">
	<div class="mdl-spinner mdl-js-spinner is-active" id="load"></div>
		<table class="mdl-data-table mdl-js-data-table mdl-shadow--8dp semDataTable" style="width: 100%">
			<thead>
				<tr>
					<th width="40%" class="mdl-data-table__cell--non-numeric">GN</th>
					<th width="20%">MÉDIA 1° CHECK-IN </th>
					<th width="20%">MÉDIA ÚLTIMO CHECK-OUT</th>
					<th width="20%">MÉDIA DE HORAS TRABALHADAS</th>
					<th width="20%">CHECKOUT FILIAL 1</th>
					<th width="20%">CHECKOUT FILIAL 2</th>
					<th width="20%">CHECKOUT TOTAL</th>
				</tr>
			</thead>
		  <tbody id="loadDetalhes">

		 <?php echo $dados['lista']; ?>

		  </tbody>
		</table>

	</div>

</div>

<div class="mdl-grid" style="margin-left: -30px;">

	<div class="mdl-cell mdl-cell--1-col">
		<a id="excel_geral" target="_blank" style="cursor: pointer;">
			<img src="<?php echo base_url() ?>style/imagens/excel.jpg">
		</a>
	</div>
	<div class="mdl-cell mdl-cell--1-col" style="margin-left: -20px;">
		<a id="pdf_geral" target="_blank" style="cursor: pointer;">
			<img src="<?php echo base_url() ?>style/imagens/pdf.jpg">
		</a>
	</div>

</div>

<script type="text/javascript" src="http://www.chartjs.org/assets/Chart.js">
</script>