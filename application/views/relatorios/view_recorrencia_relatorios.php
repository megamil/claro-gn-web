<style type="text/css">
	.destaque {
		font-size: 46px;
	}

	.adicional{
		font-size: 46px;
	}

	.contorno{
		border-style: solid;
		border-width: 1px;
		border-color: #dde;
	}

</style>

<div class="mdl-grid" style="margin-left: -15px;">

	<div class="mdl-cell mdl-cell--2-col">
	    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
	    	<label class="label" for="data_filtro">Período</label>
	    	<select class="select filtrar" name="data_filtro" id="data_filtro" style="width: 100%;">
	    		<!-- <option value="0">Todo Período</option> -->
	    		<!-- <option value="1">Hoje</option>
	    		<option value="2">Última Semana</option>
	    		<option value="3">Última Quinzena</option>
	    		<option value="4">Último Mês</option>
	    		<option value="5">Último Bimestre</option>
	    		<option value="6">Último Trimestre</option>
	    		<option value="7">Último Semestre</option>
	    		<option value="8">Último Ano</option> -->
	    		<option value="9">Personalizado</option>
	    	</select>
	    </div>
	</div>

	<div class="mdl-cell mdl-cell--2-col" align="center" style="margin-top: 27px;">
	  	<label for="de" class="label">DE</label>
	  	<input type="text" class="mdl-textfield__input filtrar mascara_data" name="de" id="de">
	  </div>

	  <div class="mdl-cell mdl-cell--2-col" align="center" style="margin-top: 27px;">
	  	<label for="ate" class="label">ATÉ</label>
	  	<input type="text" class="mdl-textfield__input filtrar mascara_data" name="ate" id="ate">
	  </div>

	<div class="mdl-cell mdl-cell--1-col">
	    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
	    	<label class="label" for="curva_filtro">Curva</label>
	    	<select class="select filtrar" name="curva_filtro" id="curva_filtro" style="width: 100%;">
	    		<option value="">Todas</option>
	    		<option>A</option>
	    		<option>B</option>
	    		<option>C</option>
	    	</select>
	    </div>
	</div>

	<div class="mdl-cell mdl-cell--1-col">
	    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
	    	<label class="label" for="filial_filtro">Filial</label>
	    	<select class="select filtrar" name="filial_filtro" id="filial_filtro" style="width: 100%;">
	    		<option value="">Filiais</option>
	    		<option value="1">Filial 1</option>
	    		<option value="2">Filial 2</option>
	    	</select>
	    </div>
	</div>

	<div class="mdl-cell mdl-cell--2-col">
	    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
	    	<label class="label" for="gn_filtro">GNs</label>
	    	<select class="select filtrar" name="gn_filtro" id="gn_filtro" style="width: 100%;">
	    	<option value="">TODOS</option>
    		<?php 

	  			foreach ($dados['usuarios'] as $usuarios) {
	  				echo '<option value="'.$usuarios->id_usuario.'">'.$usuarios->nome.'</option>';
	  			}

	  		 ?>
	    	</select>
	    </div>
	</div>

 	<div class="mdl-cell mdl-cell--2-col" style="margin-top: 27px;">
	    <label class="label" for="cnpj">CNPJ</label>
	    <input type="text" class="mdl-textfield__input mascara_cnpj filtrar"  name="cnpj" id="cnpj"/>
   </div>

</div>

<div class="mdl-grid contorno" style="background-color: white;" align="left">
		
	<b>RANKING de VISITAS a PDVs</b>

</div>

<div class="mdl-grid contorno" style="background-color: white; margin-top: 10px;">

	<div class="mdl-cell mdl-cell--12-col" id="loadrpp">

		<table class="mdl-data-table mdl-js-data-table mdl-shadow--8dp semDataTable" style="width: 100%;">
			<thead>
				<tr style="background-color: #dedede">
					<th width="20%" class="mdl-data-table__cell--non-numeric">REDE</th>
					<th width="20%" class="mdl-data-table__cell--non-numeric">LOJAS ATIVAS</th>
					<th width="15%" class="mdl-data-table__cell--non-numeric">LOJAS ATENDIDAS</th>
					<th width="15%" class="mdl-data-table__cell--non-numeric">VISITAS REALIZADAS</th>
					<th width="15%" class="mdl-data-table__cell--non-numeric">% REDE ATENDIDA</th>
					<th width="15%" class="mdl-data-table__cell--non-numeric">% REDE NÃO ATENDIDA</th>
				</tr>
			</thead>
		  <tbody>

			 <tr>
			 	<td width="20%" class="mdl-data-table__cell--non-numeric"></td>
			 	<td width="20%" class="mdl-data-table__cell--non-numeric"></td>
			 	<td width="15%" class="mdl-data-table__cell--non-numeric"></td>
			 	<td width="15%" class="mdl-data-table__cell--non-numeric"></td>
			 	<td width="15%" class="mdl-data-table__cell--non-numeric"></td>
			 	<td width="15%" class="mdl-data-table__cell--non-numeric"></td>
			 </tr>

		  </tbody>
		</table>

	</div>

</div>

<div class="mdl-grid contorno" style="background-color: white; margin-top: 10px;">

	<div class="mdl-cell mdl-cell--12-col" id="loadDetalhes">

	</div>

</div>


<div class="mdl-grid" style="margin-left: -30px;">

	<div class="mdl-cell mdl-cell--1-col">
		<a id="excel_geral" target="_blank" style="cursor: pointer;">
			<img src="<?php echo base_url() ?>style/imagens/excel.jpg">
		</a>
	</div>
	<div class="mdl-cell mdl-cell--1-col" style="margin-left: -20px;">
		<a id="pdf_geral" target="_blank" style="cursor: pointer;">
			<img src="<?php echo base_url() ?>style/imagens/pdf.jpg">
		</a>
	</div>

</div>

<script type="text/javascript">
	$(document).ready(function(){

		$('select').select2();

		$("#progress1").hide();

		$('.filtrar').change(function(){

				$("#progress1").show();

				$("#loadrpp").load("<?php echo base_url(); ?>controller_relatorios/ajax_recorrencia",{cnpj: $("#cnpj").val(),de: $("#de").val(),ate: $("#ate").val(),curva_filtro: $("#curva_filtro").val(),filial_filtro: $("#filial_filtro").val(),gn_filtro: $("#gn_filtro").val()}, function (){


					$("#progress1").hide();

				});

		});

		$('#excel_geral').click(function(){

			var cnpj = $("#cnpj").val()
			var de = $("#de").val()
			var ate = $("#ate").val()
			var curva_filtro = $("#curva_filtro").val()
			var filial_filtro = $("#filial_filtro").val()
			var gn_filtro = $("#gn_filtro").val()

			var url = "<?php echo base_url(); ?>controller_excel/excel_recorrencia?de="+de+"&ate="+ate+"&filial_filtro="+filial_filtro+"&gn_filtro="+gn_filtro+"&=data_filtro"+data_filtro+"&nomeGN="+$("#gn_filtro :selected").text()+"&filial_filtro="+filial_filtro+"&curva_filtro="+curva_filtro;

			$.ajax({
		      url: url,
		      type: "post", 
		  	datatype: 'json',
		      success: function(data){
		      	window.location = url;
		      },
		      error:function(){
		          
		      }   
		    });

		});

		$('#pdf_geral').click(function(){

			var cnpj = $("#cnpj").val()
			var de = $("#de").val()
			var ate = $("#ate").val()
			var filial_filtro = $("#filial_filtro").val()
			var gn_filtro = $("#gn_filtro").val()

			var url = "<?php echo base_url(); ?>controller_pdf/recorrencia?de="+de+"&ate="+ate+"&filial_filtro="+filial_filtro+"&gn_filtro="+gn_filtro+"&=data_filtro"+data_filtro+"&nomeGN="+$("#gn_filtro :selected").text()+"&filial_filtro="+filial_filtro;

			window.open(url, '_blank');

		});

			var receberFoco = document.getElementById("loadDetalhes");


		$(document).on('click','.listarAtivas',function(){

			$("#loadDetalhes").load("<?php echo base_url(); ?>controller_relatorios/ajax_recorrencia_ativos",{rede: $(this).attr('rede'),cnpj: $("#cnpj").val(),curva_filtro: $("#curva_filtro").val(),filial_filtro: $("#filial_filtro").val()}, function (){

				receberFoco.scrollIntoView();	

				});

		});

		$(document).on('click','.listarAtendidas',function(){

			$("#loadDetalhes").load("<?php echo base_url(); ?>controller_relatorios/ajax_recorrencia_atendidos",{rede: $(this).attr('rede'),cnpj: $("#cnpj").val(),de: $("#de").val(),ate: $("#ate").val(),curva_filtro: $("#curva_filtro").val(),filial_filtro: $("#filial_filtro").val(),gn_filtro: $("#gn_filtro").val()}, function (){

				receberFoco.scrollIntoView();	

				});

		});


	});
</script>
