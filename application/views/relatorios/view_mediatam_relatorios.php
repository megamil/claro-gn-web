<script type="text/javascript" src="http://www.chartjs.org/assets/Chart.js"></script>
<script type="text/javascript">
$(document).ready(function(){

	$('#print').click(function(){
		print();
	});

	$('select').select2();

	// $('#de_filtro').hide();
	// $('#ate_filtro').hide();

	$('#data_filtro').change(function(){

		if ($(this).val() == 9) {
			$('#de_filtro').show();
			$('#ate_filtro').show();
		} else {
			$('#de_filtro').hide();
			$('#ate_filtro').hide();

			ajax();

		}

	});

	//Caso vá específicar uma Filial o gráfico deixa de fazer sentido.
	$('#filial_filtro').change(function(){
		
		if($(this).val() > 0){
			$('#piechart_3d').hide();
		} else {
			$('#piechart_3d').show();
		}

		ajax();

	});

	$('.filtro').change(function(){
		ajax();
	});


	$('#pdf_geral').click(function(){

		var data_filtro = $("#data_filtro").val();
		var de = $("#de").val();
		var ate = $("#ate").val();
		var cnpj = $("#cnpj").val();
		var gn_filtro = $("#gn_filtro").val();

		var url = "<?php echo base_url(); ?>controller_pdf/pdf_mediatam?de="+de+"&ate="+ate+"&gn_filtro="+gn_filtro+"&=data_filtro"+data_filtro+"&cnpj="+cnpj+"&nomeGN="+$("#gn_filtro :selected").text();

		window.open(url, '_blank');

	});

	$('#excel_geral').click(function(){

		var data_filtro = $("#data_filtro").val();
		var de = $("#de").val();
		var ate = $("#ate").val();
		var cnpj = $("#cnpj").val();
		var gn_filtro = $("#gn_filtro").val();

		var url = "<?php echo base_url(); ?>controller_excel/excel_mediatam?de="+de+"&ate="+ate+"&gn_filtro="+gn_filtro+"&=data_filtro"+data_filtro+"&cnpj="+cnpj+"&nomeGN="+$("#gn_filtro :selected").text();

		$.ajax({
	      url: url,
	      type: "post",
	  	datatype: 'json',
	      success: function(data){
 			window.location = url;

 			},
	      error:function(){
	          
	      }   
	    });

	});

	function ajax(){

		console.log("CNPJ")

		$.ajax({
	      url: "<?php echo base_url(); ?>controller_relatorios/ajax_mediatam",
	      type: "post",
	      data: {id_usuario: $('#gn_filtro').val(),de: $('#de').val(),ate: $('#ate').val(),cnpj: $('#cnpj').val()},
	  	datatype: 'json',
	      success: function(data){

	      	//Títulos: 
	      	$('#legenda_graf1').text('(Filial 1 + Filial 2 = '+data['total_couts']+' Checkouts)');	   
			$('#legenda_graf2').text('('+data['f1_couts']+' Checkouts)');	   
			$('#legenda_graf3').text('('+data['f2_couts']+' Checkouts)');

			//Gráfico Totalizador:
			graficoTotal.options.data[0].dataPoints[0].y = parseInt(data['total_mobile']);
			graficoTotal.options.data[0].dataPoints[1].y = parseInt(data['total_telefonia']);
			graficoTotal.options.data[0].dataPoints[2].y = parseInt(data['total_aberto']);

			graficoTotal.options.data[0].dataPoints[0].percentual = parseInt(data['total_perc_m']);
			graficoTotal.options.data[0].dataPoints[1].percentual = parseInt(data['total_perc_t']);
			graficoTotal.options.data[0].dataPoints[2].percentual = parseInt(data['total_perc_a']);

			graficoTotal.render();	

			graficoFilial1.options.data[0].dataPoints[0].y = parseInt(data['f1_mobile']);
			graficoFilial1.options.data[0].dataPoints[1].y = parseInt(data['f1_telefonia']);
			graficoFilial1.options.data[0].dataPoints[2].y = parseInt(data['f1_aberto']);

			graficoFilial1.options.data[0].dataPoints[0].percentual = parseInt(data['f1_perc_m']);
			graficoFilial1.options.data[0].dataPoints[1].percentual = parseInt(data['f1_perc_t']);
			graficoFilial1.options.data[0].dataPoints[2].percentual = parseInt(data['f1_perc_a']);

			graficoFilial1.render();	

			graficoFilial2.options.data[0].dataPoints[0].y = parseInt(data['f2_mobile']);
			graficoFilial2.options.data[0].dataPoints[1].y = parseInt(data['f2_telefonia']);
			graficoFilial2.options.data[0].dataPoints[2].y = parseInt(data['f2_aberto']);

			graficoFilial2.options.data[0].dataPoints[0].percentual = parseInt(data['f2_perc_m']);
			graficoFilial2.options.data[0].dataPoints[1].percentual = parseInt(data['f2_perc_t']);
			graficoFilial2.options.data[0].dataPoints[2].percentual = parseInt(data['f2_perc_a']);

			graficoFilial2.render();                 

	      },
	      error:function(){
	          
	      }   
	    });


	}

	var graficoTotal;
	var graficoFilial1;
	var graficoFilial2;

	window.onload = function () {
		//Total
		graficoTotal = new CanvasJS.Chart("graficoTotal", {

			      data: [

			      {
			      	indexLabel: "{y} ({percentual} %)", 
			      	indexLabelFontColor: "black",    
					indexLabelLineColor: "darkgrey",
					indexLabelFontWeight: "bold",
					showInLegend: false,
			        dataPoints: [
						{y: 0, label: "Mobile", color : "#38597A", percentual: "0"},
						{y: 0, label: "Telefonia", color : "#D9A300", percentual: "0"},
						{y: 0, label: "Aberto", color : "#FF8000", percentual: "0"}
			        ]
			      }
			      ]
			    });

		graficoTotal.render();

		//Filial 1
		graficoFilial1 = new CanvasJS.Chart("graficoFilial1", {

			      data: [

			      {
			      	indexLabel: "{y} ({percentual} %)", 
			      	indexLabelFontColor: "black",    
					indexLabelLineColor: "darkgrey",
					indexLabelFontWeight: "bold",
					showInLegend: false,
			        dataPoints: [
						{y: 0, label: "Mobile", color : "#38597A", percentual: "0"},
						{y: 0, label: "Telefonia", color : "#D9A300", percentual: "0"},
						{y: 0, label: "Aberto", color : "#FF8000", percentual: "0"}
			        ]
			      }
			      ]
			    });

		graficoFilial1.render();

	 	//Filial 2
		graficoFilial2 = new CanvasJS.Chart("graficoFilial2", {

			      data: [

			      {
			      	indexLabel: "{y} ({percentual} %)", 
			      	indexLabelFontColor: "black",    
					indexLabelLineColor: "darkgrey",
					indexLabelFontWeight: "bold",
					showInLegend: false,
			        dataPoints: [
						{y: 0, label: "Mobile", color : "#38597A", percentual: "0"},
						{y: 0, label: "Telefonia", color : "#D9A300", percentual: "0"},
						{y: 0, label: "Aberto", color : "#FF8000", percentual: "0"}
			        ]
			      }
			      ]
			    });

		graficoFilial2.render();

	 }


});

</script>



<style type="text/css">
	.destaque {
		font-size: 46px;
	}

	.adicional{
		font-size: 46px;
	}

	.contorno{
		border-style: solid;
		border-width: 1px;
		border-color: #dde;
	}

</style>


<div class="mdl-grid" style="margin-left: -15px;">

	<div class="mdl-cell mdl-cell--2-col">
	    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
	    	<label class="label" for="data_filtro">Período</label>
	    	<select class="select" name="data_filtro" id="data_filtro" style="width: 100%;">
	    		<!-- <option value="0">Todo Período</option>
	    		<option value="1">Hoje</option>
	    		<option value="2">Última Semana</option>
	    		<option value="3">Última Quinzena</option>
	    		<option value="4">Último Mês</option>
	    		<option value="5">Último Bimestre</option>
	    		<option value="6">Último Trimestre</option>
	    		<option value="7">Último Semestre</option>
	    		<option value="8">Último Ano</option> -->
	    		<option value="9">Personalizado</option>
	    	</select>
	    </div>
	</div>

	<div class="mdl-cell mdl-cell--2-col" id="de_filtro" style="margin-top: 25px">
		<label class="label" for="de">De</label>
		<input class="mdl-textfield__input filtro mascara_data obrigatorio" type="text" id="de" name="de" aviso="Data Limite">
	</div>

	<div class="mdl-cell mdl-cell--2-col" id="ate_filtro" style="margin-top: 25px">
		<label class="label" for="ate">Até</label>
		<input class="mdl-textfield__input filtro mascara_data obrigatorio" type="text" id="ate" name="ate" aviso="Data Limite">
	</div>

	<div class="mdl-cell mdl-cell--2-col" id="cnpj" style="margin-top: 25px">
		<label class="label" for="cnpj">CNPJ</label>
		<input class="mdl-textfield__input filtro mascara_cnpj obrigatorio" type="text" id="cnpj" name="cnpj" aviso="CNPJ">
	</div>

	<div class="mdl-cell mdl-cell--2-col">
	    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
	    	<label class="label" for="gn_filtro">GNs</label>
	    	<select class="select" name="gn_filtro" id="gn_filtro" style="width: 100%;">
	    	<option value="">TODOS</option>
    		<?php 

	  			foreach ($dados['usuarios'] as $usuarios) {
	  				echo '<option value="'.$usuarios->id_usuario.'">'.$usuarios->nome.'</option>';
	  			}

	  		 ?>
	    	</select>
	    </div>
	</div>

</div>

<div class="mdl-grid contorno" style="background-color: white;" align="left">
		
	<b>ANÁLISE de PDVs (Tipo de PDVs)</b>

</div>

<div class="mdl-grid" style="background-color: white; padding: 0px;">
	
	<div class="mdl-cell mdl-cell--3-col contorno" style="margin: 0px; border-right-style: none;">
		<h5>TOTAL</h5> <br>
		<span id="legenda_graf1"></span>
		<div id="graficoTotal" style="height: 75%; width: 90%;"></div>
	</div>

	<div class="mdl-cell mdl-cell--3-col contorno" style="margin: 0px; border-right-style: none;">
		<h5>FILIAL 1</h5> <br>
		<span id="legenda_graf2"></span>
		<div id="graficoFilial1" style="height: 75%; width: 90%;"></div>
	</div>

	<div class="mdl-cell mdl-cell--3-col contorno" style="margin: 0px; border-right-style: none;">
		<h5>FILIAL 2</h5> <br>
		<span id="legenda_graf3"></span>
		<div id="graficoFilial2" style="height: 300px; width: 90%;"></div>
	</div>

	<div class="mdl-cell mdl-cell--3-col contorno" style="margin: 0px; border-right-style: none;">
		
	</div>
</div>

<div class="mdl-grid contorno" style="background-color: white; margin-top: 10px" align="left">
	
	<div class="mdl-cell mdl-cell--1-col">

		<div style="width: 35px; height: 30px; background-color: #FF8000;"></div>
		
	</div>

	<div class="mdl-cell mdl-cell--3-col" align="left" style="margin-top: 10px;"> <strong>ABERTO</strong></div>

	<div class="mdl-cell mdl-cell--1-col">
		<div style="width: 35px; height: 30px; background-color: #38597A;"></div>
	</div>

	<div class="mdl-cell mdl-cell--3-col" align="left" style="margin-top: 10px;"><strong>MOBILE</strong></div>

	<div class="mdl-cell mdl-cell--1-col">
		<div style="width: 35px; height: 30px; background-color: #D9A300;"></div>
	</div>

	<div class="mdl-cell mdl-cell--3-col" align="left" style="margin-top: 10px;"><strong>TELEFONIA</strong></div>


	<div class="mdl-cell mdl-cell--4-col">
		PDVs sem espaço dedicado para telefonia
	</div>

	<div class="mdl-cell mdl-cell--4-col">
		PDVs com layout de ilha no setor de telefonia
	</div>

	<div class="mdl-cell mdl-cell--4-col">
		PDVs com balcão
	</div>


</div>


<div class="mdl-grid" style="margin-left: -30px;">

	<div class="mdl-cell mdl-cell--1-col">
		<a id="excel_geral" target="_blank" style="cursor: pointer;">
			<img src="<?php echo base_url() ?>style/imagens/excel.jpg">
		</a>
	</div>
	<div class="mdl-cell mdl-cell--1-col" style="margin-left: -20px;">
		<a id="pdf_geral" target="_blank" style="cursor: pointer;">
	      <img src="<?php echo base_url() ?>style/imagens/pdf.jpg">
	    </a>
	</div>

</div>