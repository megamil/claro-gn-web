<div class="mdl-grid" style="margin-top: -50px;">
  <h3>Relatórios Input de Vendas</h3>
</div>
<div class="mdl-grid" style="margin-top: -30px;">
  <small>Selecione a opção desejada</small>
</div>

<div class="mdl-grid" style="margin-top: 30px;">

 <div class="mdl-cell mdl-cell--3-col">

    <a href="<?php echo base_url() ?>main/redirecionar/relatorios-view_historico_inputvendas">
      <img src="<?php echo base_url() ?>style/imagens/relatorios/campanhas.png" width="150px">
    </a>
    <br>
    HISTÓRICO

  </div>

  <div class="mdl-cell mdl-cell--3-col">

    <a href="<?php echo base_url() ?>main/redirecionar/relatorios-view_geral_inputvendas">
      <img src="<?php echo base_url() ?>style/imagens/relatorios/analisep.png" width="150px">
    </a>
    <br>
    GERAL INPUT DE VENDAS

  </div>

 <div class="mdl-cell mdl-cell--3-col">

    <a href="<?php echo base_url() ?>main/redirecionar/relatorios-view_filial1_inputvendas">
      <img src="<?php echo base_url() ?>style/imagens/relatorios/analise.png" width="150px">
    </a>
    <br>
    POR FILIAL 1

  </div>

 <div class="mdl-cell mdl-cell--3-col">

    <a href="<?php echo base_url() ?>main/redirecionar/relatorios-view_filial2_inputvendas">
      <img src="<?php echo base_url() ?>style/imagens/relatorios/analise.png" width="150px">
    </a>
    <br>
    POR FILIAL 2

  </div>

</div>

</div>