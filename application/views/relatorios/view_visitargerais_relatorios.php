<style type="text/css">
	.resultados {
		border-bottom-style: solid;
		border-bottom-width: 2px;
		border-bottom-color: green;
		font-size: 18px;
		padding-bottom: 5px;
	}

	.resultadosDestaque {
		border-bottom-style: solid;
		border-bottom-width: 2px;
		border-bottom-color: #D52B1E;
		font-size: 20px;
		padding-bottom: 10px;
	}
	small {
		font-size: 10px;
		font-weight: bold;
	}
</style>

<div class="mdl-grid">

<div class="mdl-cell mdl-cell--12-col tituloRelatorio" align="center">

  <h3>TOTAL DE VISITAS DISTINTAS</h3>
  <hr>

  </div>

</div>
<div class="mdl-grid">

  <div class="mdl-cell mdl-cell--1-col filtroRelatorio" align="center">

<!--   FILTRO <i class="material-icons">search</i> -->

  </div>

</div>
<div class="mdl-grid">

  <div class="mdl-cell mdl-cell--2-col" align="left">
  	MÉDIA DO MÊS ATUAL:
  </div>

  <div class="mdl-cell mdl-cell--10-col resultadosDestaque" align="left">

  	<?php $porc = (($dados['atual']->row()->distintos * 100) / $dados['totalpdvs']->row()->totalpdvs); ?>

  	FORAM REALIZADAS <?php echo $dados['atual']->row()->distintos; ?> VISITAS A PDVS DISTINTOS NESTE PERÍODO, REPRESENTANDO <?php echo number_format($porc, 2, '.', ''); ?> % DO TOTAL
  	<li><small>PDV com Maior número de vísitas no período: <?php echo $dados['atualDetalhes']->row(0)->rede; ?>, <?php echo $dados['atualDetalhes']->row(0)->valor; ?>  Visitas.</small></li>
  	<li><small>PDV com Menor número de vísitas no período: <?php echo $dados['atualDetalhes']->row(1)->rede; ?>, <?php echo $dados['atualDetalhes']->row(1)->valor; ?>  Visitas.</small></li>
  </div>

</div>

<div class="mdl-grid">

  <div class="mdl-cell mdl-cell--2-col" align="left">
  	MÉDIA DO ÚLTIMO MÊS:
  </div>

  <div class="mdl-cell mdl-cell--10-col resultados" align="left">

  	<?php $porc = (($dados['ultimo']->row()->distintos * 100) / $dados['totalpdvs']->row()->totalpdvs); ?>

  	FORAM REALIZADAS <?php echo $dados['ultimo']->row()->distintos; ?> VISITAS A PDVS DISTINTOS NESTE PERÍODO, REPRESENTANDO <?php echo number_format($porc, 2, '.', ''); ?> % DO TOTAL
  	<li><small>PDV com Maior número de vísitas no período: <?php echo $dados['ultimoDetalhes']->row(0)->rede; ?>, <?php echo $dados['ultimoDetalhes']->row(0)->valor; ?>  Visitas.</small></li>
  	<li><small>PDV com Menor número de vísitas no período: <?php echo $dados['ultimoDetalhes']->row(1)->rede; ?>, <?php echo $dados['ultimoDetalhes']->row(1)->valor; ?>  Visitas.</small></li>
  </div>

</div>

<div class="mdl-grid">

  <div class="mdl-cell mdl-cell--2-col" align="left">
  	MÉDIA DO ÚLTIMO TRIMESTRE:
  </div>

  <div class="mdl-cell mdl-cell--10-col resultados" align="left">

  	<?php $porc = (($dados['trimestre']->row()->distintos * 100) / $dados['totalpdvs']->row()->totalpdvs); ?>

  	FORAM REALIZADAS <?php echo $dados['trimestre']->row()->distintos; ?> VISITAS A PDVS DISTINTOS NESTE PERÍODO, REPRESENTANDO <?php echo number_format($porc, 2, '.', ''); ?> % DO TOTAL
  	<li><small>PDV com Maior número de vísitas no período: <?php echo $dados['trimestreDetalhes']->row(0)->rede; ?>, <?php echo $dados['trimestreDetalhes']->row(0)->valor; ?>  Visitas.</small></li>
  	<li><small>PDV com Menor número de vísitas no período: <?php echo $dados['trimestreDetalhes']->row(1)->rede; ?>, <?php echo $dados['trimestreDetalhes']->row(1)->valor; ?>  Visitas.</small></li>
  </div>

</div>

<div class="mdl-grid">

  <div class="mdl-cell mdl-cell--2-col" align="left">
  	MÉDIA DO ÚLTIMO SEMESTRE:
  </div>

  <div class="mdl-cell mdl-cell--10-col resultados" align="left">

  	<?php $porc = (($dados['semestre']->row()->distintos * 100) / $dados['totalpdvs']->row()->totalpdvs); ?>

  	FORAM REALIZADAS <?php echo $dados['semestre']->row()->distintos; ?> VISITAS A PDVS DISTINTOS NESTE PERÍODO, REPRESENTANDO <?php echo number_format($porc, 2, '.', ''); ?> % DO TOTAL
  	<li><small>PDV com Maior número de vísitas no período: <?php echo $dados['semestreDetalhes']->row(0)->rede; ?>, <?php echo $dados['semestreDetalhes']->row(0)->valor; ?>  Visitas.</small></li>
  	<li><small>PDV com Menor número de vísitas no período: <?php echo $dados['semestreDetalhes']->row(1)->rede; ?>, <?php echo $dados['semestreDetalhes']->row(1)->valor; ?>  Visitas.</small></li>
  </div>

</div>

<div class="mdl-grid">

  <div class="mdl-cell mdl-cell--2-col" align="left">
  	MÉDIA DO ÚLTIMO ANO:
  </div>

  <div class="mdl-cell mdl-cell--10-col resultados" align="left">

  <?php $porc = (($dados['ano']->row()->distintos * 100) / $dados['totalpdvs']->row()->totalpdvs); ?>

  	FORAM REALIZADAS <?php echo $dados['ano']->row()->distintos; ?> VISITAS A PDVS DISTINTOS NESTE PERÍODO, REPRESENTANDO <?php echo number_format($porc, 2, '.', ''); ?> % DO TOTAL
  	<li><small>PDV com Maior número de vísitas no período: <?php echo $dados['anoDetalhes']->row(0)->rede; ?>, <?php echo $dados['anoDetalhes']->row(0)->valor; ?>  Visitas.</small></li>
  	<li><small>PDV com Menor número de vísitas no período: <?php echo $dados['anoDetalhes']->row(1)->rede; ?>, <?php echo $dados['anoDetalhes']->row(1)->valor; ?>  Visitas.</small></li>
  </div>

</div>

<!-- RODAPÉ -->

<div class="mdl-grid">

  <div class="mdl-cell mdl-cell--12-col" align="right" style="margin-top: 30px;">
  	<small>Total de PDVs Existentes: <?php echo $dados['totalpdvs']->row()->totalpdvs; ?></small>
  </div>

</div>