<script type="text/javascript" src="http://www.chartjs.org/assets/Chart.js">
</script>
<script type="text/javascript">
$(document).ready(function(){

  $('#print').click(function(){
    print();
  });

  $('select').select2();

  // $('#de_filtro').hide();
  // $('#ate_filtro').hide();

  $('#data_filtro').change(function(){

    if ($(this).val() == 9) {
      $('#de_filtro').show();
      $('#ate_filtro').show();
    } else {
      $('#de_filtro').hide();
      $('#ate_filtro').hide();

      ajax();

    }

  });

  $("#exibir_vivo").change(function(){

    if (!$(this).is(':checked')) {
      $('#graficoVivo').hide();
    } else {
      $('#graficoVivo').show();
    }

  });

    $("#exibir_oi").change(function(){

    if (!$(this).is(':checked')) {
      $('#graficoOi').hide();
    } else {
      $('#graficoOi').show();
    }

  });

  $("#exibir_tim").change(function(){

    if (!$(this).is(':checked')) {
      $('#graficoTim').hide();
    } else {
      $('#graficoTim').show();
    }

  });



  //Caso vá específicar uma Filial o gráfico deixa de fazer sentido.
  $('#filial_filtro').change(function(){
    
    if($(this).val() > 0){
      $('#piechart_3d').hide();
    } else {
      $('#piechart_3d').show();
    }

    ajax();

  });

  $('#de, #ate, #curva_filtro').change(function(){
    ajax();
  });


  $('#gn_filtro').change(function(){
    
    // if($(this).val() > 0){
    //  $('.semDataTable').hide();
    // } else {
    //  $('.semDataTable').show();
    // }

    ajax();
    
  });

  // $('#graficoClaro').hide();
  // $('#graficoVivo').hide();
  // $('#graficoOi').hide();
  // $('#graficoTim').hide();

  $('#excel_geral').click(function(){

    var data_filtro = $("#data_filtro").val();
    var de = $("#de").val();
    var ate = $("#ate").val();
    var filial_filtro = $("#filial_filtro").val();
    var gn_filtro = $("#gn_filtro").val();

    var url = "<?php echo base_url(); ?>controller_excel/excel_promotor?de="+de+"&ate="+ate+"&gn_filtro="+gn_filtro+"&=data_filtro"+data_filtro+"$filial_filtro="+filial_filtro+"&nomeGN="+$("#gn_filtro :selected").text()+"&curva="+$('#curva_filtro').val();

    $.ajax({
        url: url,
        type: "post",
      datatype: 'json',
        success: function(data){
      window.location = url;

      },
        error:function(){
            
        }   
      });

  });

  function ajax(){

    if ($("#exibir_vivo").is(':checked')) {
      $('#graficoVivo').show();
    }

    if ($("#exibir_oi").is(':checked')) {
      $('#graficoOi').show();
    }

    if ($("#exibir_tim").is(':checked')) {
      $('#graficoTim').show();
    }

    $('#graficoClaro').show();

    $.ajax({
        url: "<?php echo base_url(); ?>controller_relatorios/ajax_promotor",
        type: "post",
        data: {data_filtro: $("#data_filtro").val(),
          de: $("#de").val(),
          ate: $("#ate").val(),
          filial_filtro: $("#filial_filtro").val(),
          curva: $("#curva_filtro").val(),
          gn_filtro: $("#gn_filtro").val()},
      datatype: 'json',
        success: function(data){

          graficoClaro.options.data[0].dataPoints[0].y = parseInt(data['claro_pt']);
          graficoClaro.options.data[0].dataPoints[1].y = parseInt(data['claro_free']);
          graficoClaro.options.data[0].dataPoints[2].y = parseInt(data['claro_fixo']);

          graficoClaro.options.data[0].dataPoints[0].percentual = parseInt(data['claro_perc_pt']);
          graficoClaro.options.data[0].dataPoints[1].percentual = parseInt(data['claro_perc_free']);
          graficoClaro.options.data[0].dataPoints[2].percentual = parseInt(data['claro_perc_fixo']);

          graficoClaro.render();    

          $('#totalClaro').text("Total de Promotores: "+(parseInt(data['claro_pt']) + parseInt(data['claro_free']) + parseInt(data['claro_fixo'])));

          graficoOi.options.data[0].dataPoints[0].y = parseInt(data['oi_pt']);
          graficoOi.options.data[0].dataPoints[1].y = parseInt(data['oi_free']);
          graficoOi.options.data[0].dataPoints[2].y = parseInt(data['oi_fixo']);

          graficoOi.options.data[0].dataPoints[0].percentual = parseInt(data['oi_perc_pt']);
          graficoOi.options.data[0].dataPoints[1].percentual = parseInt(data['oi_perc_free']);
          graficoOi.options.data[0].dataPoints[2].percentual = parseInt(data['oi_perc_fixo']);

          graficoOi.render();    

          $('#totalOi').text("Total de Promotores: "+(parseInt(data['oi_pt']) + parseInt(data['oi_free']) + parseInt(data['oi_fixo'])));

          graficoVivo.options.data[0].dataPoints[0].y = parseInt(data['vivo_pt']);
          graficoVivo.options.data[0].dataPoints[1].y = parseInt(data['vivo_free']);
          graficoVivo.options.data[0].dataPoints[2].y = parseInt(data['vivo_fixo']);

          graficoVivo.options.data[0].dataPoints[0].percentual = parseInt(data['vivo_perc_pt']);
          graficoVivo.options.data[0].dataPoints[1].percentual = parseInt(data['vivo_perc_free']);
          graficoVivo.options.data[0].dataPoints[2].percentual = parseInt(data['vivo_perc_fixo']);

          graficoVivo.render();    

          $('#totalVivo').text("Total de Promotores: "+(parseInt(data['vivo_pt']) + parseInt(data['vivo_free']) + parseInt(data['vivo_fixo'])));

          graficoTim.options.data[0].dataPoints[0].y = parseInt(data['tim_pt']);
          graficoTim.options.data[0].dataPoints[1].y = parseInt(data['tim_free']);
          graficoTim.options.data[0].dataPoints[2].y = parseInt(data['tim_fixo']);

          graficoTim.options.data[0].dataPoints[0].percentual = parseInt(data['tim_perc_pt']);
          graficoTim.options.data[0].dataPoints[1].percentual = parseInt(data['tim_perc_free']);
          graficoTim.options.data[0].dataPoints[2].percentual = parseInt(data['tim_perc_fixo']);

          graficoTim.render();     

          $('#totalTim').text("Total de Promotores: "+(parseInt(data['tim_pt']) + parseInt(data['tim_free']) + parseInt(data['tim_fixo'])));

        },
        error:function(){
            
        }   
      });

  }

  var graficoClaro;
  var graficoOi;
  var graficoVivo;
  var graficoTim;

  window.onload = function () {
      
      graficoClaro = new CanvasJS.Chart("graficoClaro", {

              data: [

              {
                indexLabel: "{y} ({percentual} %)", 
                indexLabelFontColor: "black",    
                indexLabelLineColor: "darkgrey",
                indexLabelFontWeight: "bold",
                showInLegend: false,
                dataPoints: [
              {y: 0, label: "PART TIME", color : "#38597A", percentual: "0"},
              {y: 0, label: "FREE", color : "#D9A300", percentual: "0"},
              {y: 0, label: "FIXO", color : "#FF8000", percentual: "0"}
                ]
              }
              ]
            });

      graficoClaro.render();

      
      graficoOi = new CanvasJS.Chart("graficoOi", {

              data: [

              {
                indexLabel: "{y} ({percentual} %)", 
                indexLabelFontColor: "black",    
                indexLabelLineColor: "darkgrey",
                indexLabelFontWeight: "bold",
                showInLegend: false,
                dataPoints: [
              {y: 0, label: "PART TIME", color : "#38597A", percentual: "0"},
              {y: 0, label: "FREE", color : "#D9A300", percentual: "0"},
              {y: 0, label: "FIXO", color : "#FF8000", percentual: "0"}
                ]
              }
              ]
            });

      graficoOi.render();

      
      graficoVivo = new CanvasJS.Chart("graficoVivo", {

              data: [

              {
                indexLabel: "{y} ({percentual} %)", 
                indexLabelFontColor: "black",    
                indexLabelLineColor: "darkgrey",
                indexLabelFontWeight: "bold",
                showInLegend: false,
                dataPoints: [
              {y: 0, label: "PART TIME", color : "#38597A", percentual: "0"},
              {y: 0, label: "FREE", color : "#D9A300", percentual: "0"},
              {y: 0, label: "FIXO", color : "#FF8000", percentual: "0"}
                ]
              }
              ]
            });

      graficoVivo.render();

      graficoTim = new CanvasJS.Chart("graficoTim", {

              data: [

              {
                indexLabel: "{y} ({percentual} %)", 
                indexLabelFontColor: "black",    
                indexLabelLineColor: "darkgrey",
                indexLabelFontWeight: "bold",
                showInLegend: false,
                dataPoints: [
              {y: 0, label: "PART TIME", color : "#38597A", percentual: "0"},
              {y: 0, label: "FREE", color : "#D9A300", percentual: "0"},
              {y: 0, label: "FIXO", color : "#FF8000", percentual: "0"}
                ]
              }
              ]
            });

      graficoTim.render();

   }

 });

</script>



<style type="text/css">
  .destaque {
    font-size: 46px;
  }

  .adicional{
    font-size: 46px;
  }

  .contorno{
    border-style: solid;
    border-width: 1px;
    border-color: #dde;
  }

</style>


<div class="mdl-grid" style="margin-left: -15px;">

  <div class="mdl-cell mdl-cell--2-col">
      <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
        <label class="label" for="data_filtro">Período</label>
        <select class="select" name="data_filtro" id="data_filtro" style="width: 100%;">
          <option value="9">Personalizado</option>
        </select>
      </div>
  </div>

  <div class="mdl-cell mdl-cell--2-col" id="de_filtro" style="margin-top: 25px">
    <label class="label" for="de">De</label>
    <input class="mdl-textfield__input mascara_data obrigatorio" type="text" id="de" name="de" aviso="Data Limite">
  </div>

  <div class="mdl-cell mdl-cell--2-col" id="ate_filtro" style="margin-top: 25px">
    <label class="label" for="ate">Até</label>
    <input class="mdl-textfield__input mascara_data obrigatorio" type="text" id="ate" name="ate" aviso="Data Limite">
  </div>

  <div class="mdl-cell mdl-cell--2-col">
      <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
        <label class="label" for="filial_filtro">Filial</label>
        <select class="select" name="filial_filtro" id="filial_filtro" style="width: 100%;">
          <option value="">Filiais</option>
          <option value="1">Filial 1</option>
          <option value="2">Filial 2</option>
        </select>
      </div>
  </div>

    <div class="mdl-cell mdl-cell--1-col">
        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
          <label class="label" for="curva_filtro">Curva</label>
          <select class="select filtrar" name="curva_filtro" id="curva_filtro" style="width: 100%;">
            <option value="">Todas</option>
            <option>A</option>
            <option>B</option>
            <option>C</option>
          </select>
        </div>
    </div>

  <div class="mdl-cell mdl-cell--2-col">
      <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
        <label class="label" for="gn_filtro">GNs</label>
        <select class="select" name="gn_filtro" id="gn_filtro" style="width: 100%;">
        <option value="">Selecione</option>
        <?php 

          foreach ($dados['usuarios'] as $usuarios) {
            echo '<option value="'.$usuarios->id_usuario.'">'.$usuarios->nome.'</option>';
          }

         ?>
        </select>
      </div>
  </div>

</div>

<div class="mdl-grid contorno" style="background-color: white;" align="left">
    
  <b>PROMOTORES CLARO em PDVs X CONCORRÊNCIA (VIVO, TIM E OI)</b>

</div>

<div class="mdl-grid" style="background-color: white; padding: 0px;">

  <div class="mdl-cell mdl-cell--3-col contorno" style="margin: 0px; border-right-style: none; padding-top: 20px; padding-bottom: 20px;">
    <img src="<?php echo base_url() ?>style/imagens/relatorios/logo_claro.png" style="height: 60px"> <br>
    <span id="totalClaro"></span>
    <div id="graficoClaro" style="height: 300px; width: 90%;"></div>
  </div>

  <div class="mdl-cell mdl-cell--3-col contorno" style="margin: 0px; border-right-style: none; padding-top: 20px; padding-bottom: 20px;">
    <img src="<?php echo base_url() ?>style/imagens/relatorios/logo_vivo.png" style="height: 60px"> <br>
    <span id="totalVivo"></span>
    <div id="graficoVivo" style="height: 300px; width: 90%;"></div>
    Exibir <input type="checkbox" id="exibir_vivo" checked="" style="bottom: 0px;">
  </div>

    <div class="mdl-cell mdl-cell--3-col contorno" style="margin: 0px; border-right-style: none; padding-top: 20px; padding-bottom: 20px;">
      <img src="<?php echo base_url() ?>style/imagens/relatorios/logo_tim.png" style="height: 60px"> <br>
      <span id="totalTim"></span>
     <div id="graficoTim" style="height: 300px; width: 90%;"></div>
     Exibir <input type="checkbox" id="exibir_tim" checked="" style="bottom: 0px;">
    </div>

    <div class="mdl-cell mdl-cell--3-col contorno" style="margin: 0px; border-right-style: none; padding-top: 20px; padding-bottom: 20px;">
      <img src="<?php echo base_url() ?>style/imagens/relatorios/logo_oi.png" style="height: 60px"> <br>
      <span id="totalOi"></span>
      <div id="graficoOi" style="height: 300px; width: 90%;"></div>
      Exibir <input type="checkbox" id="exibir_oi" checked="" style="bottom: 0px;">
    </div>
</div>

<div class="mdl-grid contorno" style="background-color: white; margin-top: 10px" align="left">
  
  <div class="mdl-cell mdl-cell--1-col">

    <div style="width: 35px; height: 30px; background-color: #FF8000;"></div>
    
  </div>

  <div class="mdl-cell mdl-cell--3-col" align="left" style="margin-top: 10px;"> <strong>FIXO</strong></div>

  <div class="mdl-cell mdl-cell--1-col">
    <div style="width: 35px; height: 30px; background-color: #38597A;"></div>
  </div>

  <div class="mdl-cell mdl-cell--3-col" align="left" style="margin-top: 10px;"><strong>PART TIME</strong></div>

  <div class="mdl-cell mdl-cell--1-col">
    <div style="width: 35px; height: 30px; background-color: #D9A300;"></div>
  </div>

  <div class="mdl-cell mdl-cell--3-col" align="left" style="margin-top: 10px;"><strong>FREE</strong></div>

</div>


<div class="mdl-grid" style="margin-left: -30px;">

  <div class="mdl-cell mdl-cell--1-col">
    <a id="excel_geral" target="_blank" style="cursor: pointer;">
      <img src="<?php echo base_url() ?>style/imagens/excel.jpg">
    </a>
  </div>
  <div class="mdl-cell mdl-cell--1-col" style="margin-left: -20px;">
    <img src="<?php echo base_url() ?>style/imagens/pdf.jpg">
  </div>

</div>

<script type="text/javascript">
	$(document).ready(function(){

		$("#progress1").hide();

		$('#buscarf').click(function(){

			$("#progress1").show();

			$("#loadfaturamento").load("<?php echo base_url(); ?>controller_relatorios/ajax_promotor",{de: $("#de").val(),ate: $("#ate").val(),cnpj: $("#cnpj").val(),usuario: $("#usuario").val(),filial: $("#filial").val()}, function (){

				$("#progress1").hide();

			});


		});

	});
</script>
