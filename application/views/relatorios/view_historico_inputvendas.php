<script type="text/javascript" src="<?php echo base_url(); ?>style/js/datatable.js"></script>
<?php echo form_open('controller_inputvendas/excel_export'); ?>
<div class="mdl-grid">

	<div class="mdl-cell mdl-cell--3-col">

	<label class="label" for="fk_usuario">Filtrar Por Usuário</label>
	<select name="fk_usuario" id="fk_usuario" style="width: 100%">
		<option value="">Todos</option>
		<?php foreach ($dados['usuarios'] as $usuario) {

	    	echo '<option value="'.$usuario->id_usuario.'">'.$usuario->usuario.'</option>';

		} ?>
	</select>

	</div>

	<div class="mdl-cell mdl-cell--2-col">

		<label class="label" for="ate">De</label>
	    <input class="mdl-textfield__input" type="text" id="de" name="de">

	</div>

	<div class="mdl-cell mdl-cell--2-col">

		<label class="label" for="ate">Até</label>
	    <input class="mdl-textfield__input" type="text" id="ate" name="ate">
	    
	</div>

	<div class="mdl-cell mdl-cell--2-col">
	    <div class="">
	    	<label class="label" for="habitado_filtro">Habitado</label>
	    	<select name="habitado_filtro" id="habitado_filtro" style="width: 100%;">
	    		<option value="">Todos</option>
	    		<option value="">Habitados</option>
	    		<option value="">Desabitados</option>
	    	</select>
	    </div>
	</div>

	<div class="mdl-cell mdl-cell--3-col">
	<a type="button" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" id="filtrar" style="color: white;">Aplicar Filtro</a>
	</div>

</div>

<div class="mdl-spinner mdl-js-spinner" id="loadSpinner"></div>
<div id="load" align="center">

	<table class="mdl-data-table mdl-js-data-table mdl-shadow--8dp">
	  <thead>
	    <tr>
	      <th></th>
	      <th>Usuário</th>
	      <th>Filial</th>
	      <th>Data</th>
	      <th>Pré</th>
	      <th>Controle fácil</th>
	      <th>Controle Giga</th>
	      <th>Recarga</th>
	      <th>Migracao</th>
	      <th>HC Oficial</th>
	      <th>HC Campo</th>
	      <th>Habitado</th>
	      <th>Data Envio</th>
	      <th>Aspectos</th>
	    </tr>
	  </thead>
	  <tbody>
	    <?php foreach ($dados['input_vendas'] as $input_vendas) {
	    	echo '<tr>';
	    	echo '<td>'.anchor('main/redirecionar/editar-view_editar_inputvendas/'.$input_vendas->id_input_vendas, '<i class="material-icons">mode_edit</i>Editar', array('class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent', 'title' => 'Editar', 'alt' => 'Editar', 'style' => 'margin-top: -7px;','target' => '_blank')).'</td>';
			echo '<td>'.$input_vendas->usuario.'</td>';
			echo '<td>'.$input_vendas->filial.'</td>';
			echo '<td>'.$input_vendas->data_input.'</td>';
			echo '<td>'.$input_vendas->pre.'</td>';
			echo '<td>'.$input_vendas->controle_facil.'</td>';
			echo '<td>'.$input_vendas->controle_giga.'</td>';
			echo '<td>'.$input_vendas->recarga.'</td>';
			echo '<td>'.$input_vendas->migracao.'</td>';
			echo '<td>'.$input_vendas->hc_oficial.'</td>';
			echo '<td>'.$input_vendas->hc_campo.'</td>';
			if ($input_vendas->habitado) {
				echo '<td>Habitado</td>';
			} else {
				echo '<td>Desabitado</td>';
			}
			echo '<td>'.$input_vendas->data_envio.'</td>';
			echo '<td>'.$input_vendas->aspectos.'</td>';
			echo '</tr>';
		} ?>
	  </tbody>
	</table>
</div>

<script type="text/javascript">
	$(document).ready(function(){

		$("#fk_usuario").select2();
		$("#habitado_filtro").select2();

		$("#de, #ate").datepicker({
		    dateFormat: 'dd/mm/yy',
		    dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
		    dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
		    dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
		    monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		    monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
		    nextText: 'Próximo',
		    prevText: 'Anterior'
		});

		$('#filtrar').click(function(){

			$('#loadSpinner').addClass('is-active');

			var usuario = $('#fk_usuario').val();
			var de = $('#de').val();
			var ate = $('#ate').val();

			$('#load').load(<?php echo "'".base_url()."controller_inputvendas/filtroAjax'"; ?>, {"usuario" : usuario, "de" : de, "ate" : ate}, function(){

				$('#loadSpinner').removeClass('is-active');

				if(!$('table').hasClass('semDataTable')){

					// Adicionando um campos com input para as buscas indivíduais.
				    $('table thead th').each( function () {
				        var title = $(this).text();
				        if (title != '') {
				       		$(this).html('<input type="text" class="mdl-textfield__input" placeholder="'+title+'" alt="'+title+'" title="'+title+'"/>' ); 	
				        }
				    } );

				    var table = $('table').DataTable({
				    	"oLanguage": {
				            "sLengthMenu": "Mostrar _MENU_ registros por página",
				            "sZeroRecords": "Nenhum registro encontrado",
				            "sInfo": "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
				            "sInfoEmpty": "Mostrando 0 / 0 de 0 registros",
				            "sInfoFiltered": "(filtrado de _MAX_ registros)",
				            "sSearch": "Pesquisa Geral: ",
				            "oPaginate": {
				                "sFirst": "Início",
				                "sPrevious": "Anterior",
				                "sNext": "Próximo",
				                "sLast": "Último"
				            }
				        }
				    });
							 
				 
					    //Aplicando a busca nos campos
					    table.columns().every( function () {
					        var that = this;
					 
					        $( 'input', this.header() ).on( 'keyup change', function () {
					            if ( that.search() !== this.value ) {
					                that
					                    .search( this.value )
					                    .draw();
					            }
					        } );
					    } );

				}

			});


		});

	});
</script>

<div class="mdl-grid" style="margin-left: -15px;">

	<div class="mdl-cell mdl-cell--1-col">
		<button type="submit" id="excel_geral" target="_blank" style="cursor: pointer;">
			<img src="<?php echo base_url() ?>style/imagens/excel.jpg">
		</button>
	</div>
	<!-- <div class="mdl-cell mdl-cell--1-col" style="margin-left: -20px;">
		<img src="<?php //echo base_url() ?>style/imagens/pdf.jpg">
	</div>
	<div class="mdl-cell mdl-cell--1-col" id="print" style="margin-left: -20px;">
		<img src="<?php //echo base_url() ?>style/imagens/imprimir.jpg">
	</div> -->

</div>
<? echo form_close(); ?>