<style type="text/css">
  .destaque {
    font-size: 46px;
  }

  .adicional{
    font-size: 46px;
  }

  .contorno{
    border-style: solid;
    border-width: 1px;
    border-color: #dde;
  }

  td {
    text-align: center;
  }

</style>


<div class="mdl-grid" style="margin-left: -15px;">

  <div class="mdl-cell mdl-cell--3-col">
      <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
        <label class="label" for="data_filtro">Período</label>
        <select class="select" name="data_filtro" id="data_filtro" style="width: 100%;">
          <option value="9">Personalizado</option>
        </select>
      </div>
  </div>

  <div class="mdl-cell mdl-cell--2-col" id="de_filtro" style="margin-top: 25px">
    <label class="label" for="de">De</label>
    <input class="mdl-textfield__input mascara_data obrigatorio" type="text" id="de" name="de" aviso="Data Limite">
  </div>

  <div class="mdl-cell mdl-cell--2-col" id="ate_filtro" style="margin-top: 25px">
    <label class="label" for="ate">Até</label>
    <input class="mdl-textfield__input mascara_data obrigatorio" type="text" id="ate" name="ate" aviso="Data Limite">
  </div>

  <div class="mdl-cell mdl-cell--2-col">
      <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
        <label class="label" for="filial_filtro">Filial</label>
        <select class="select" name="filial_filtro" id="filial_filtro" style="width: 100%;">
          <option value="">Filiais</option>
          <option value="1">Filial 1</option>
          <option value="2">Filial 2</option>
        </select>
      </div>
  </div>

</div>

<div class="mdl-grid contorno" style="background-color: white; margin-top: 10px" align="left">
  <strong>ANÁLISE Promotor no PDV </strong>
</div>


<!-- Lista dos gns -->

<div class="mdl-grid contorno" style="background-color: white; margin-top: 10px;">

  <div class="mdl-cell mdl-cell--12-col">

    <table class="semDataTable" style="width: 100%; background-color: #D6D6D6"" >
      <thead>
        <tr style="color: white; background-color: #D6D6D6">
          <th width="40%"  class="mdl-data-table__cell--non-numeric" colspan="1"></th>
          <th width="20%" colspan="3">Atende</th>
          <th width="20%" colspan="3">Atende Parcialmente</th>
          <th width="20%" colspan="3">Não Atende</th>
        </tr>

        <tr style="color: white; background-color: #D6D6D6; font-weight:normal;">
          <th width="40%" colspan="1"></th>

          <th width="">A</th>
          <th width="">B</th>
          <th width="">C</th>

          <th width="">A</th>
          <th width="">B</th>
          <th width="">C</th>

          <th width="">A</th>
          <th width="">B</th>
          <th width="">C</th>
        </tr>
      </thead>
      <tbody style="background-color: white" id="loadatende">

        <tr>
          <td>APRESENTAÇÃO</td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>

        <tr>
          <td>BOOK</td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>

        <tr>
          <td>CONHECIMENTO</td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>

      </tbody>
    </table>

  </div>

</div>

<div class="mdl-grid contorno" style="background-color: white; margin-top: 10px" align="left">
  
  <div class="mdl-cell mdl-cell--4-col" align="left" style="margin-top: 10px;"> <strong>ATENDE</strong></div>

  <div class="mdl-cell mdl-cell--4-col" align="left" style="margin-top: 10px;"><strong>ATENDE PARCIALMENTE</strong></div>

  <div class="mdl-cell mdl-cell--4-col" align="left" style="margin-top: 10px;"><strong>NÃO ATENDE</strong></div>


  <div class="mdl-cell mdl-cell--4-col">
   9 e 10
  </div>

  <div class="mdl-cell mdl-cell--4-col">
    8 à 6
  </div>

  <div class="mdl-cell mdl-cell--4-col">
    5 à 4
  </div>


</div>

<div class="mdl-grid" style="margin-left: -30px;">

  <div class="mdl-cell mdl-cell--1-col">
    <a id="excel_geral" target="_blank" style="cursor: pointer;">
      <img src="<?php echo base_url() ?>style/imagens/excel.jpg">
    </a>
  </div>
  <div class="mdl-cell mdl-cell--1-col" style="margin-left: -20px;">
    <a id="pdf_geral" target="_blank" style="cursor: pointer;">
      <img src="<?php echo base_url() ?>style/imagens/pdf.jpg">
    </a>
  </div>

</div>

<script type="text/javascript">
  $(document).ready(function(){

    $('#data_filtro').select2();

    $("#progress1").hide();

    $('#de, #ate, #filial_filtro').change(function(){

      $("#progress1").show();

      $("#loadatende").load("<?php echo base_url(); ?>controller_relatorios/ajax_atende",{filial: $("#filial_filtro").val(),de: $("#de").val(),ate: $("#ate").val()}, function (){

        $("#progress1").hide();

      });

    });

     $('#pdf_geral').click(function(){

      var de = $("#de").val();
      var ate = $("#ate").val();

      de = de.replace('/','-'); //Ele só troca um por vez.
      de = de.replace('/','-');

      ate = ate.replace('/','-'); //Ele só troca um por vez.
      ate = ate.replace('/','-');

      var url = "<?php echo base_url(); ?>controller_pdf/analise_promotor_pdv?de="+de+"&ate="+ate;

      window.open(url, '_blank');

    });


    $('#excel_geral').click(function(){

      var de = $("#de").val();
      var ate = $("#ate").val();
      var filial_filtro = $("#filial_filtro").val();

      de = de.replace('/','-'); //Ele só troca um por vez.
      de = de.replace('/','-');

      ate = ate.replace('/','-'); //Ele só troca um por vez.
      ate = ate.replace('/','-');

      var url = "<?php echo base_url(); ?>controller_excel/excel_gnatende?de="+de+"&ate="+ate+"&filial_filtro="+filial_filtro;

      $.ajax({
          url: url,
          type: "post",
        datatype: 'json',
          success: function(data){
        window.location = url;

        },
          error:function(){
              
          }   
        });

    });

  });
</script>