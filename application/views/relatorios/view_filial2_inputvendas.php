<style type="text/css">
    .destaque {
        font-size: 46px;
    }

    .adicional{
        font-size: 46px;
    }

    .contorno{
        border-style: solid;
        border-width: 1px;
        border-color: #dde;
    }

    #corpo {
        width: 90%;
    }

    table {
        border-collapse: collapse;
        text-align: center;
    }

    tr, td, th{
        border:1px solid black;
        padding: 2px;
        color: white;
    }

    th {
        background-color: #CCC;
    }



    th {
        background-color: #000;
        color: white;
    }

    tr:hover {
        background-color: #000000;
        color: black;
    }

    tr:hover, td:hover, th:hover{
        color: white;
    }

    .novo-background{
        background-color: #7a0b09;
        border:none;
    }

    .cor{
        white;
    }

    .label-titulo{
        float: left;
        font-size: 12px;
        color: white;
    }

    .inputs{
        padding-top: 20px;
        width: 100px;
    }

    .primeiraTd {
        background-color: #b5c0d4 !important; 
        color: #000 !important; 
        font-weight: 600;
    }

    .ultimaTr {
        background-color: #000 !important; 
        color: #fff !important; 
        font-weight: 600;
    }

</style>


<div class="mdl-grid novo-background">

    <div class="mdl-cell--10-col novo-background">

        <div class="mdl-grid novo-background" style="margin-left: 0px;">

            <div class="mdl-cell mdl-cell--2-col" id="de_filtro" style="margin-top: 25px">
                <label class="label-titulo" for="de">Mês</label>
                <select class="select" id="mes_controle">
                    <option value="1" <?php if(date('m') == 1) {echo 'selected';} ?>>Janeiro</option>
                    <option value="2" <?php if(date('m') == 2) {echo 'selected';} ?>>Fevereiro</option>
                    <option value="3" <?php if(date('m') == 3) {echo 'selected';} ?>>Março</option>
                    <option value="4" <?php if(date('m') == 4) {echo 'selected';} ?>>Abril</option>
                    <option value="5" <?php if(date('m') == 5) {echo 'selected';} ?>>Maio</option>
                    <option value="6" <?php if(date('m') == 6) {echo 'selected';} ?>>Junho</option>
                    <option value="7" <?php if(date('m') == 7) {echo 'selected';} ?>>Julho</option>
                    <option value="8" <?php if(date('m') == 8) {echo 'selected';} ?>>Agosto</option>
                    <option value="9" <?php if(date('m') == 9) {echo 'selected';} ?>>Setembro</option>
                    <option value="10" <?php if(date('m') == 10) {echo 'selected';} ?>>Outubro</option>
                    <option value="11" <?php if(date('m') == 11) {echo 'selected';} ?>>Novembro</option>
                    <option value="12" <?php if(date('m') == 12) {echo 'selected';} ?>>Dezembro</option>
                </select>
            </div>

            <div class="mdl-cell mdl-cell--2-col" id="ate_filtro" style="margin-top: 25px">
                <label class="label-titulo" for="ate">Ano</label>
                <select class="select" id="ano_controle">
                    <?php

                    $ano = date('Y');
                    $ano_count = 2016;

                    while ($ano >= $ano_count) {
                        echo '<option selected>'.$ano_count.'</option>';
                        $ano_count++;
                    }

                    ?>
                </select>
            </div>

            <div class="mdl-cell mdl-cell--2-col">
                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                    <label class="label-titulo" for="habitado_filtro">Habitado</label>
                    <select class="select" name="habitado_filtro" id="habitado_filtro" style="width: 100%;">
                        <option value="">Todos</option>
                        <option value="1">Habitados</option>
                        <option value="0">Desabitados</option>
                    </select>
                </div>
            </div>

            <div class="mdl-cell mdl-cell--3-col" style="margin-top: 30px;">
                <a type="button" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" id="filtrar">Aplicar Filtro</a>
            </div>

        </div>

        <div id="load_1">
            <div class="mdl-grid contorno novo-background"  align="left">

                <h4 style="color: white">Filial 2</h4>

                <div class="mdl-cell mdl-cell--12-col">

                    <table class="semDataTable" width="100%">
                        <thead>
                        <tr>
                            <th></th>
                            <th>Pré Pago</th>
                            <th>Controle</th>
                            <th>Recarga</th>
                            <th>HC Oficial</th>
                            <th>HC em Campo</th>
                            <th>% Campo</th>
                            <th>PHC Pré</th>
                            <th>PHC Controle</th>
                            <th>PHC Recarga</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php

                        $controle_prepago_count = array();
                        $controle_controle_count = array();
                        $controle_recarga_count = array();
                        $controle_hc_oficial_count = array();
                        $controle_hc_campo_count = array();

                        foreach ($dados['geral_usuarios_f2'] as $value) {
                            echo '<tr>';

                            echo '<td class="primeiraTd">'.$value->usuario.'</td>';
                            echo '<td>'.$value->prepago.'</td>';
                            echo '<td>'.$value->controle.'</td>';
                            echo '<td>'.$value->recarga.'</td>';
                            echo '<td>'.$value->hc_oficial.'</td>';
                            echo '<td>'.$value->hc_campo.'</td>';
                            echo '<td>'.round(divisao(($value->hc_campo * 100 ),$value->hc_oficial),2).'%</td>';

                            echo '<td>'.round(divisao(($value->prepago * 100),$value->hc_campo),2).'%</td>';
                            echo '<td>'.round(divisao(($value->controle * 100),$value->hc_campo),2).'%</td>';
                            echo '<td>'.round(divisao(($value->recarga * 100),$value->hc_campo),2).'%</td>';

                            echo '</tr>';

                            array_push($controle_prepago_count,$value->prepago);
                            array_push($controle_controle_count,$value->controle);
                            array_push($controle_recarga_count,$value->recarga);
                            array_push($controle_hc_oficial_count,$value->hc_oficial);
                            array_push($controle_hc_campo_count,$value->hc_campo);


                        }

                        $prepago_tbl1 = array_sum($controle_prepago_count);
                        $controle_tbl1 = array_sum($controle_controle_count);
                        $recarga_tbl1 = array_sum($controle_recarga_count);
                        $hc_oficial_tbl1 = array_sum($controle_hc_oficial_count);
                        $hc_campo_tbl1 = array_sum($controle_hc_campo_count);

                        echo '<tr>
                                <td class="ultimaTr">Total Filial:</td>
        						<td class="ultimaTr">'.$prepago_tbl1.'</td>
        						<td class="ultimaTr">'.$controle_tbl1.'</td>
        						<td class="ultimaTr">'.$recarga_tbl1.'</td>
        						<td class="ultimaTr">'.$hc_oficial_tbl1.'</td>
        						<td class="ultimaTr">'.$hc_campo_tbl1.'</td>
        						<td class="ultimaTr">'.round(divisao(($hc_campo_tbl1 * 100 ),$hc_oficial_tbl1),2).'%</td>
        						<td class="ultimaTr">'.round(divisao(($prepago_tbl1 * 100),$hc_campo_tbl1),2).'%</td>
        						<td class="ultimaTr">'.round(divisao(($controle_tbl1 * 100),$hc_campo_tbl1),2).'%</td>
        						<td class="ultimaTr">'.round(divisao(($recarga_tbl1 * 100),$hc_campo_tbl1),2).'%</td>
            				</tr>';


                        ?>
                        </tbody>
                    </table>

                </div>

            </div>
        </div>

        <div class="mdl-grid contorno novo-background"  align="left">
            <div class="mdl-cell mdl-cell--5-col" id="load_2" width="100%">
                <div align="left">

                    <table class="semDataTable">
                        <thead>
                        <tr>
                            <th></th>
                            <th>Controle Total</th>
                            <th>Controle Fácil</th>
                            <th>%</th>
                            <th>Controle Boleto</th>
                            <th>%</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php

                        $controle_facil_count = array();
                        $controle_giga_count = array();

                        foreach ($dados['geral_controle_usuario_f2'] as $value) {
                            echo '<tr>';

                            echo '<td class="primeiraTd">'.$value->usuario.'</td>';
                            echo '<td>'.$value->controle_total.'</td>';
                            echo '<td>'.$value->controle_facil.'</td>';
                            echo '<td>'.round((divisao(($value->controle_facil * 100 ),($value->controle_giga+$value->controle_facil))),2).' %</td>';
                            echo '<td>'.$value->controle_giga.'</td>';
                            echo '<td>'.round((divisao(($value->controle_giga * 100 ),($value->controle_giga+$value->controle_facil))),2).' %</td>';

                            echo '</tr>';

                            array_push($controle_facil_count,$value->controle_facil);
                            array_push($controle_giga_count,$value->controle_giga);
                        }

                        $total_tbl2 = (array_sum($controle_facil_count) + array_sum($controle_giga_count));
                        $total_facil_tbl2 = array_sum($controle_facil_count);
                        $total_boleto_tbl2 = array_sum($controle_giga_count);

                        echo '<tr><td class="ultimaTr">Total Filial:</td>
					<td class="ultimaTr">'.$total_tbl2.'</td>
					<td class="ultimaTr">'.$total_facil_tbl2.'</td>
					<td class="ultimaTr">'.(divisao(($total_facil_tbl2  * 100),$total_tbl2)).' %</td>
					<td class="ultimaTr">'.$total_boleto_tbl2.'</td>
					<td class="ultimaTr">'.(divisao(($total_boleto_tbl2  * 100),$total_tbl2)).' %</td></tr>';


                        ?>
                        </tbody>
                    </table>

                </div>
            </div>

            <div class="mdl-cell mdl-cell--2-col"></div>

            <div class="mdl-cell mdl-cell--5-col contorno" style="border: none;">
                <div id="grafico_pizza" style="width:100%; height: 90%;"></div>
            </div>

        </div>
        <div id="load_3">

            <?php


            print_lista('Filial 2 Pré Pago',$dados['usuarios_iv'],$dados['detalhado_usuario_pre_f2'],false);
            print_lista('Filial 2 Controle Total',$dados['usuarios_iv'],$dados['detalhado_usuario_total_f2'],false);
            print_lista('Filial 2 Controle Fácil',$dados['usuarios_iv'],$dados['detalhado_usuario_facil_f2'],false);
            print_lista('Filial 2 Controle Boleto',$dados['usuarios_iv'],$dados['detalhado_usuario_giga_f2'],false);

            print_lista('REPRESENTATIVIDADE % - CONTROLE FÁCIL',$dados['usuarios_iv'],$dados['perc_facil_f2'],true);
            print_lista('REPRESENTATIVIDADE % - CONTROLE BOLETO',$dados['usuarios_iv'],$dados['perc_giga_f2'],true);

            print_lista('Filial 2 Recarga',$dados['usuarios_iv'],$dados['detalhado_usuario_recarga_f2'],false);
            print_lista('Filial 2 Migração',$dados['usuarios_iv'],$dados['detalhado_usuario_migracao_f2'],false);
            print_lista('Filial 2 HC Oficial',$dados['usuarios_iv'],$dados['detalhado_usuario_hc_oficial_f2'],false);
            print_lista('Filial 2 HC Em Campo',$dados['usuarios_iv'],$dados['detalhado_usuario_hc_campo_f2'],false);

            print_lista('REPRESENTATIVIDADE % - HCs EM CAMPO',$dados['usuarios_iv'],$dados['perc_campo_f2'],true);
            print_lista('PHC - PRÉ PAGO',$dados['usuarios_iv'],$dados['perc_pre_f2'],true);
            print_lista('PHC - CONTROLE',$dados['usuarios_iv'],$dados['perc_controle_f2'],true);
            print_lista('PHC - RECARGA',$dados['usuarios_iv'],$dados['perc_recarga_f2'],true);

            function print_lista($titulo = null, $usuarios = null,$lista = null,$perc = null) {

                echo '<div class="mdl-grid contorno novo-background" >
		<h4 style="color: #fff">'.$titulo.'</h4>
		<table class="semDataTable" style="width: 100%">
			<thead>
				<tr>
					<th></th>';

                for ($i=1; $i < 32; $i++) {
                    echo '<th>'.$i.'</th>';
                }

                echo '<th>TOTAL</th>
				</tr>
			</thead>
			<tbody>';

                $array_totalizador = array();
                //Inicia
                for ($i=0; $i < 31; $i++) {
                    $array_totalizador[$i] = 0;
                }


                foreach ($usuarios as $usuario) {

                    $array = array();

                    echo '<tr>';
                    echo '<td class="primeiraTd">'.$usuario->usuario.'</td>';

                    for ($i=1; $i < 32; $i++) {
                        $valor = 0;

                        foreach ($lista as $f1) {
                            if ($f1->data == $i && $usuario->id_usuario == $f1->id_usuario) {
                                $valor = $f1->valor;
                            }
                        }

                        array_push($array, $valor);
                        if ($perc) {
                            echo '<td>'.$valor.'%</td>';
                        } else {
                            echo '<td>'.$valor.'</td>';
                        }

                    }

                    //Descobrindo a quantidade de valores acima de 0 estão no array, para poder dividir e extrair uma %
                    if ($perc) {

                        $count = 0;
                        foreach ($array as $value) {
                            if ($value > 0) {
                                $count += 1;
                            }
                        }


                        echo '<td class="primeiraTd">'.divisao(array_sum($array),$count).'%</td>';
                    } else {
                        echo '<td class="primeiraTd">'.array_sum($array).'</td>';
                    }

                    echo '</tr>';

                    for ($i=0; $i < 31; $i++) {
                        if ($array[$i] > 0) {
                            $array_totalizador[$i] += $array[$i];
                        }

                    }


                }
                //Linhas em branco
                for ($j=0; $j < 3; $j++) {
                    echo '<tr>';
                    echo '<td></td>';
                    for ($i=0; $i < 31; $i++) {
                        echo '<td></td>';
                    }
                    echo '<td>0</td>';
                    echo '</tr>';
                }

                //Linha com o total
                echo '<tr>';
                echo '<td class="ultimaTr">Total Filial</td>';
                for ($i=0; $i < 31; $i++) {
                    if ($perc) {
                        echo '<td class="ultimaTr">'.$array_totalizador[$i].'%</td>';
                    } else {
                        echo '<td class="ultimaTr">'.$array_totalizador[$i].'</td>';
                    }
                }

                if ($perc) {

                    $count = 0;
                    foreach ($array_totalizador as $value) {
                        if ($value > 0) {
                            $count += 1;
                        }
                    }

                    echo '<td class="ultimaTr">'.divisao(array_sum($array_totalizador),$count).'%</td>';
                } else {
                    echo '<td class="ultimaTr">'.array_sum($array_totalizador).'</td>';
                }
                echo '</tr>';

                echo '</tbody>
			</table>
	</div>';

            }

            function divisao($v1,$v2){
                if ($v1 > 0 && $v2 > 0) {
                    return round(($v1/$v2),2);
                } else {
                    return 0;
                }
            }
            ?>
        </div>
        <div class="mdl-grid" style="margin-left: -30px;">

        </div></div>

    <div class="mdl-cell--2-col novo-background" style="padding: 0;  margin-top: 2%;">
            <div class="col-lg-2 div-input" style="background-color: black;">
                <div style=" border-bottom: 1px solid white;  padding-top: 5%; padding-bottom: 5%">
                    <span style="color: white; font-size: 14px;">INPUT DE VENDAS</span>
                </div>

                <!-- input geral de vendas -->
                <div>
                    <a href="<?php echo base_url() ?>main/redirecionar/relatorios-view_geral_inputvendas" style="color: white">
                        <img src="<?php echo base_url() ?>stylebootstrap/img/geral_input_de_vendas_input.png" class="inputs">
                        <div class="col-lg-12 inputs-texto" >
                            <p>GERAL INPUT DE VENDAS</p>
                        </div>
                    </a><br />
                </div>

                <!-- input por filial 1 -->
                <div>
                    <a href="<?php echo base_url() ?>main/redirecionar/relatorios-view_filial1_inputvendas" style="color: white">
                        <img src="<?php echo base_url() ?>stylebootstrap/img/filial_1_input.png" class="inputs">
                        <div class="col-lg-12 inputs-texto" >
                            <p>POR FILIAL 1</p>
                        </div>
                    </a><br />
                </div>

                <!-- input por filial 2 -->
                <div>
                    <a href="<?php echo base_url() ?>main/redirecionar/relatorios-view_filial2_inputvendas" style="color: white">
                        <img src="<?php echo base_url() ?>stylebootstrap/img/filial_2_input.png" class="inputs">
                        <div class="col-lg-12 inputs-texto" >
                            <p>POR FILIAL 2</p>
                        </div>
                    </a><br />
                </div>

                <!-- input historico -->
                <div>
                    <a href="<?php echo base_url() ?>main/redirecionar/relatorios-view_historico_inputvendas" style="color: white">
                        <img src="<?php echo base_url() ?>stylebootstrap/img/historico_input.png" class="inputs">
                        <div class="col-lg-12 inputs-texto" >
                            <p>HISTÓRICO</p>
                        </div>
                    </a><br />
                </div>


            </div>
        </div>

</div>

<div class="mdl-grid" style="margin-left: -30px;">

    <div class="mdl-cell mdl-cell--1-col ">
        <a id="excel_geral" target="_blank" style="cursor: pointer;">
            <img src="<?php echo base_url() ?>style/imagens/excel.jpg">
        </a>

    </div>

<script type="text/javascript">
	$(document).ready(function(){
		$('.select').select2({'width': '100%'});

		var grafico_pizza;

		window.onload = function () {
			grafico_pizza = new CanvasJS.Chart("grafico_pizza",{

				data: [
				{
					type: "pie",
					indexLabelFontColor: "black",    
					indexLabelLineColor: "darkgrey",
					indexLabelFontWeight: "bold",
					indexLabelFontSize: 15,
					indexLabelFontFamily: "Garamond",
					toolTipContent: "{name} #percent% ({y})", 
					indexLabel: "{name} #percent% ({y})", 
					showInLegend: true,
					dataPoints: [
						{ y: <?php echo array_sum($controle_giga_count); ?> , name: "Controle Boleto", color: "#5cb85c", indexLabel: "Controle Boleto #percent% ({y})"},
						{ y: <?php echo array_sum($controle_facil_count); ?> , name: "Controle Fácil", color: "#d9534f", indexLabel: "Controle Fácil #percent% ({y})"}
						
					]
				}
				]
			});

			grafico_pizza.render();

		}

		$('#excel_geral').click(function(){

			var ano  = $('#ano_controle').val();
			var mes = $('#mes_controle').val();
			var habitado = $('#habitado_filtro').val();

			var url = "<?php echo base_url(); ?>controller_excel_input/ajax_f2?ano="+ano+"&mes="+mes+"&habitado="+habitado;

		    $.ajax({
		        url: url,
		        type: "post",
		      datatype: 'json',
		        success: function(data){
		      window.location = url;

		      },
		        error:function(){
		            
		        }   
		      });

		  });

		$('#filtrar').click(function(){

			$('#load_1').load('<?php echo base_url() ?>controller_inputvendas/ajax_f2_1',{mes: $('#mes_controle').val(),
			ano: $('#ano_controle').val(),
			habitado: $('#habitado_filtro').val()},function(){

			});

			$('#load_2').load('<?php echo base_url() ?>controller_inputvendas/ajax_f2_2',{mes: $('#mes_controle').val(),
			ano: $('#ano_controle').val(),
			habitado: $('#habitado_filtro').val()},function(){

			});

			$('#load_3').load('<?php echo base_url() ?>controller_inputvendas/ajax_f2_3',{mes: $('#mes_controle').val(),
			ano: $('#ano_controle').val(),
			habitado: $('#habitado_filtro').val()},function(){

			});

			$.ajax({
		      url: "<?php echo base_url(); ?>controller_inputvendas/grafico_controles",
		      type: "post",

		      data: {mes: $('#mes_controle').val(),
					ano: $('#ano_controle').val(),
					habitado: $('#habitado_filtro').val(),
					filial: 2},

		  	datatype: 'json',
		      success: function(data){

		            grafico_pizza.options.data[0].dataPoints[0].y = parseInt(data['boleto']);
					grafico_pizza.options.data[0].dataPoints[1].y = parseInt(data['facil']);
					grafico_pizza.render();

		      },
		      error:function(){
		          
		      }   
		    });
			

		});

	});
</script>