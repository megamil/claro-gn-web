 <script src="https://rawgit.com/kimmobrunfeldt/progressbar.js/1.0.0/dist/progressbar.js"></script>

 <style type="text/css">
   #container {
    margin: 20px;
    width: 400px;
    height: 8px;
    position: relative;
  }
   .media-horas-tipos {
       display: inline-block;
       padding-right: 15px;
   }

   .ranking-media-promotor_div{
       display: inline-block;
       padding-right: 5px;
   }

   .media-horas-tipos_imagem{
       width: 320px;

   }

   .ranking-media-promotor_imagem{
       width: 190px;
   }

   .media-horas-tipos_texto{
       height: 50px;
       background-color: #700200;
       padding-top: 1px;
       padding-bottom: 10px;
   }

   .ranking-media-promotor_texto{
       height: 50px;
       background-color: #700200;
       padding-top: 1px;
       padding-bottom: 10px;
   }

   .media-horas-tipos_texto > p {
       padding-top: 20px;
   }

   .ranking-media-promotor_texto > p {
       padding-top: 20px;
       font-size: 10px;
   }

   .h3-relatorio{
       background-color: #7a0b09;
       padding-left: 4%;
       padding-top: 2%;
       padding-bottom: 2%;
       margin-top: -2%;
       margin-bottom: 0px;
       text-align: left;
       color: white;
   }

   .div-input{
      display: inline-block;
       float: right;
       width: 10%;
       background-color: #970401;
       margin-top: -6.5%;
       height: auto;
       padding-top: 4%;
   }

   .inputs{
       padding-top: 20px;
       width: 60px;
   }

   .inputs-texto > p {
       font-size: 10px;
   }

   @media only screen and (max-width: 1440px){

       .media-horas-tipos {
           padding-right: 10px;
       }

       .media-horas-tipos_imagem{
           width: 250px;

       }


       .div-input{
           width: 13%;
           margin-top: -7.5%;
           padding-top: 4%;
       }

   }

   @media only screen and (max-width: 1145px){

       .ranking-media-promotor_imagem{
           width: 180px;
       }

       .ranking-media-promotor_texto > p {
           padding-top: 20px;
           font-size: 8px;
       }

       .div-input{
           display: inline-block;
           float: right;
           width: 7%;
           background-color: #970401;
           margin-top: -6.5%;
           height: auto;
           padding-top: 4%;
       }
   }
 </style>

 <h3 class="h3-relatorio">Relatórios</h3>
<div class="col-lg-12" style=" background-color: #7a0b09">

    <div class="col-lg-10" style="display: inline-block;" >


        <!-- media de visitas -->
        <div class="col-lg-4 media-horas-tipos">
            <a href="<?php echo base_url() ?>main/redirecionar/relatorios-view_visitaspdvc_relatorios" style="color: white">
                <img src="<?php echo base_url() ?>stylebootstrap/img/media_de_visitas.jpg" class="media-horas-tipos_imagem">
                <div class="col-lg-12 media-horas-tipos_texto" >
                    <p>MÉDIA DE VISITAS</p>
                </div>
            </a><br />
        </div>

        <!-- analise de horas trabalhadas -->
        <div class="col-lg-4 media-horas-tipos">
            <a href="<?php echo base_url() ?>main/redirecionar/relatorios-view_horario_relatorios" style="color: white">
                <img src="<?php echo base_url() ?>stylebootstrap/img/analise_de_horas_trabalhadas.jpg" class="media-horas-tipos_imagem">
                <div class="col-lg-12 media-horas-tipos_texto"  >
                    <p>ANÁLISE DE HORAS TRABALHADAS</p>
                </div>
            </a>
            <br />
        </div>

        <!-- analise de pdv´s -->
        <div class="col-lg-4 media-horas-tipos">
            <a href="<?php echo base_url() ?>main/redirecionar/relatorios-view_mediatam_relatorios" style="color: white">
                <img src="<?php echo base_url() ?>stylebootstrap/img/analise_de_pdvs.jpg" class="media-horas-tipos_imagem">
                <div class="col-lg-12 media-horas-tipos_texto"  >
                    <p>ANÁLISE DE PDVs(TIPOS DE PDVs)</p>
                </div>
            </a>
            <br />
        </div>


    </div>

    <div class="col-lg-2 div-input">
        <div style=" border-bottom: 1px solid white">
            <span style="color: white; font-size: 14px">INPUT DE VENDAS</span>
        </div>

        <!-- input geral de vendas -->
        <div>
            <a href="<?php echo base_url() ?>main/redirecionar/relatorios-view_geral_inputvendas" style="color: white">
                <img src="<?php echo base_url() ?>stylebootstrap/img/geral_input_de_vendas.png" class="inputs">
                <div class="col-lg-12 inputs-texto" >
                    <p>GERAL INPUT DE VENDAS</p>
                </div>
            </a><br />
        </div>

        <!-- input por filial 1 -->
        <div>
            <a href="<?php echo base_url() ?>main/redirecionar/relatorios-view_filial1_inputvendas" style="color: white">
                <img src="<?php echo base_url() ?>stylebootstrap/img/filial_1.png" class="inputs">
                <div class="col-lg-12 inputs-texto" >
                    <p>POR FILIAL 1</p>
                </div>
            </a><br />
        </div>

        <!-- input por filial 2 -->
        <div>
            <a href="<?php echo base_url() ?>main/redirecionar/relatorios-view_filial2_inputvendas" style="color: white">
                <img src="<?php echo base_url() ?>stylebootstrap/img/filial_2.png" class="inputs">
                <div class="col-lg-12 inputs-texto" >
                    <p>POR FILIAL 2</p>
                </div>
            </a><br />
        </div>

        <!-- input historico -->
        <div>
            <a href="<?php echo base_url() ?>main/redirecionar/relatorios-view_historico_inputvendas" style="color: white">
                <img src="<?php echo base_url() ?>stylebootstrap/img/historico.png" class="inputs">
                <div class="col-lg-12 inputs-texto" >
                    <p>HISTÓRICO</p>
                </div>
            </a><br />
        </div>

    </div>
</div>

 <div class="col-lg-12" style=" background-color: #7a0b09; padding-bottom: 350px">

     <div class="col-lg-10" style="display: inline-block;" >


         <!-- ranking de visitas a pdv´s -->
         <div class="col-lg-4 ranking-media-promotor_div">
             <a href="<?php echo base_url() ?>main/redirecionar/relatorios-view_recorrencia_relatorios" style="color: white">
                 <img src="<?php echo base_url() ?>stylebootstrap/img/ranking_de_visitas_a_pdvs.jpg" class="ranking-media-promotor_imagem">
                 <div class="col-lg-12 ranking-media-promotor_texto" >
                     <p>RANKING DE VISITAS A PDVs</p>
                 </div>
             </a><br />
         </div>

         <!-- media de faturamento -->
         <div class="col-lg-4 ranking-media-promotor_div">
             <a href="<?php echo base_url() ?>main/redirecionar/relatorios-view_faturamento_relatorios" style="color: white">
                 <img src="<?php echo base_url() ?>stylebootstrap/img/media_de_faturamento.jpg" class="ranking-media-promotor_imagem">
                 <div class="col-lg-12 ranking-media-promotor_texto"  >
                     <p>MÉDIA DE FATURAMENTO</p>
                 </div>
             </a>
             <br />
         </div>

         <!-- média claro x concorrencia -->
         <div class="col-lg-4 ranking-media-promotor_div">
             <a href="<?php echo base_url() ?>main/redirecionar/relatorios-view_promotor_relatorios" style="color: white">
                 <img src="<?php echo base_url() ?>stylebootstrap/img/promotor_claro_x_concorrencia.jpg" class="ranking-media-promotor_imagem">
                 <div class="col-lg-12 ranking-media-promotor_texto"  >
                     <p>PROMOTOR CLARO X CONCORRÊNCIA</p>
                 </div>
             </a>
             <br />
         </div>

         <!-- analise promotor no pdv -->
         <div class="col-lg-4 ranking-media-promotor_div">
             <a href="<?php echo base_url() ?>main/redirecionar/relatorios-view_gnatente_relatorios" style="color: white">
                 <img src="<?php echo base_url() ?>stylebootstrap/img/analise_promotor_no_pdv.jpg" class="ranking-media-promotor_imagem">
                 <div class="col-lg-12 ranking-media-promotor_texto"  >
                     <p>ANÁLISE PROMOTOR NO PDV</p>
                 </div>
             </a>
             <br />
         </div>

         <!-- campanhas -->
         <div class="col-lg-4 ranking-media-promotor_div">
             <a href="<?php echo base_url() ?>main/redirecionar/relatorios-view_campanhas_relatorios" style="color: white">
                 <img src="<?php echo base_url() ?>stylebootstrap/img/campanhas.jpg" class="ranking-media-promotor_imagem">
                 <div class="col-lg-12 ranking-media-promotor_texto"  >
                     <p>CAMPANHAS</p>
                 </div>
             </a>
             <br />
         </div>
     </div></div>

         <script type="text/javascript">
  $(document).ready(function(){

    $('#load').hide();

    $('a').click(function(){

      $('#load').show();
      $('.relatorios').hide();

      var bar = new ProgressBar.Line(container, {
      strokeWidth: 4,
      easing: 'easeInOut',
      duration: 140000,
      color: '#FFEA82',
      trailColor: '#eee',
      trailWidth: 1,
      svgStyle: {width: '100%', height: '100%'},
      text: {
        style: {
          // Text color.
          // Default: same as stroke color (options.color)
          color: '#999',
          position: 'absolute',
          right: '0',
          top: '30px',
          padding: 0,
          margin: 0,
          transform: null
        },
        autoStyleContainer: false
      },
      from: {color: '#FFEA82'},
      to: {color: '#ED6A5A'},
      step: (state, bar) => {
        bar.setText(Math.round(bar.value() * 99) + ' %');
      }
    });

    bar.animate(1.0);  // Number from 0.0 to 1.0

    });

  });

</script>