<style type="text/css">
	.destaque {
		font-size: 46px;
	}

	.adicional{
		font-size: 46px;
	}

	.contorno{
		border-style: solid;
		border-width: 1px;
		border-color: #dde;
	}

	#corpo {
		width: 90%;
	}

	table {
		border-collapse: collapse; 
		text-align: center;
	}

	tr, td, th{
		border:1px solid black;
		padding: 2px;
	}

	th {
		background-color: #000;
        color: white;
	}

	tr:hover {
		background-color: #EEE;
        color: black;
	}

    .novo-background{
        background-color: #7a0b09;
        border:none;
    }

    .cor{
        white;
    }

    .label-titulo{
        float: left;
        font-size: 12px;
        color: white;
    }

    .inputs{
        padding-top: 20px;
        width: 100px;
    }
</style>

<script type="text/javascript">
	$(document).ready(function(){
		$('.select').select2({'width': '100%'});

		$('#filtrar').click(function(){

            console.log('Filtro');

			$('#load_ajax').load('<?php echo base_url() ?>controller_inputvendas/ajax_geral',{mes: $('#mes_controle').val(),
			ano: $('#ano_controle').val(),
			habitado: $('#habitado_filtro').val()},function(){

			});

		});

		$('#excel_geral').click(function(){

			var ano  = $('#ano_controle').val();
			var mes = $('#mes_controle').val();
			var habitado = $('#habitado_filtro').val();

			var url = "<?php echo base_url(); ?>controller_excel_input/ajax_geral?ano="+ano+"&mes="+mes+"&habitado="+habitado;

		    $.ajax({
		        url: url,
		        type: "post",
		      datatype: 'json',
		        success: function(data){
		      window.location = url;

		      },
		        error:function(){
		            
		        }   
		      });

		  });

	});
</script>

<div class="mdl-grid novo-background" style="padding: 0">
    <div class="mdl-cell--10-col" style="padding: 0">
        <div class="mdl-grid novo-background" style="margin-left:0px;">

            <div class="mdl-cell mdl-cell--2-col novo-background" id="de_filtro" style="margin-top: 25px">
                <label class="label-titulo" for="de" style="color white;">Mês</label>
                <select class="select" id="mes_controle">
                    <option value="1" <?php if(date('m') == 1) {echo 'selected';} ?>>Janeiro</option>
                    <option value="2" <?php if(date('m') == 2) {echo 'selected';} ?>>Fevereiro</option>
                    <option value="3" <?php if(date('m') == 3) {echo 'selected';} ?>>Março</option>
                    <option value="4" <?php if(date('m') == 4) {echo 'selected';} ?>>Abril</option>
                    <option value="5" <?php if(date('m') == 5) {echo 'selected';} ?>>Maio</option>
                    <option value="6" <?php if(date('m') == 6) {echo 'selected';} ?>>Junho</option>
                    <option value="7" <?php if(date('m') == 7) {echo 'selected';} ?>>Julho</option>
                    <option value="8" <?php if(date('m') == 8) {echo 'selected';} ?>>Agosto</option>
                    <option value="9" <?php if(date('m') == 9) {echo 'selected';} ?>>Setembro</option>
                    <option value="10" <?php if(date('m') == 10) {echo 'selected';} ?>>Outubro</option>
                    <option value="11" <?php if(date('m') == 11) {echo 'selected';} ?>>Novembro</option>
                    <option value="12" <?php if(date('m') == 12) {echo 'selected';} ?>>Dezembro</option>
                </select>
            </div>

            <div class="mdl-cell mdl-cell--2-col" id="ate_filtro" style="margin-top: 25px">
                <label class="label-titulo" for="ate">Ano</label>
                <select class="select" id="ano_controle">
                    <?php

                    $ano = date('Y');
                    $ano_count = 2016;

                    while ($ano >= $ano_count) {
                        echo '<option selected>'.$ano_count.'</option>';
                        $ano_count++;
                    }

                    ?>
                </select>
            </div>

            <div class="mdl-cell mdl-cell--2-col">
                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                    <label class="label-titulo " for="habitado_filtro">Habitado</label>
                    <select class="select" name="habitado_filtro" id="habitado_filtro" style="width: 100%;">
                        <option value="">Todos</option>
                        <option value="1">Habitados</option>
                        <option value="0">Desabitados</option>
                    </select>
                </div>
            </div>

            <div class="mdl-cell mdl-cell--3-col" style="margin-top: 30px;">
                <a type="button" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" id="filtrar">Aplicar Filtro</a>
            </div>

        </div>

        <div class="mdl-grid contorno novo-background"  align="left">

            <h4 style="color: white">INPUT DE VENDAS</h4>

        </div>

        <div id="load_ajax" class="novo-background">
            <div class="mdl-grid contorno novo-background">

                <div class="mdl-cell mdl-cell--5-col novo-background" style="margin-right: 20px;" align="left">

                    <table class="semDataTable cor">
                        <thead>
                        <tr>
                            <th></th>
                            <th>Pré Pago</th>
                            <th>Controle</th>
                            <th>Recarga</th>
                            <th>HC Oficial</th>
                            <th>HC em Campo</th>
                            <th>% Campo</th>
                            <th>PHC Pré</th>
                            <th>PHC Controle</th>
                            <th>PHC Recarga</th>
                        </tr>
                        </thead>
                        <tbody style="color:#fff;">
                        <?php foreach ($dados['geral'] as $chave => $value) {

                            echo '<tr>';
                            if($chave == count($dados['geral'])-1) {
                                echo '<td style="background-color: #000 !important; color: #fff !important; font-weight: 600">'.$value->filial.'</td>';
                                echo '<td style="background-color: #000 !important; color: #fff !important; font-weight: 600">'.$value->pre.'</td>';
                                echo '<td style="background-color: #000 !important; color: #fff !important; font-weight: 600">'.$value->controle.'</td>';
                                echo '<td style="background-color: #000 !important; color: #fff !important; font-weight: 600">'.$value->recarga.'</td>';
                                echo '<td style="background-color: #000 !important; color: #fff !important; font-weight: 600">'.$value->hc_oficial.'</td>';
                                echo '<td style="background-color: #000 !important; color: #fff !important; font-weight: 600">'.$value->hc_campo.'</td>';
                                echo '<td style="background-color: #000 !important; color: #fff !important; font-weight: 600">'.round((divisao(($value->hc_campo * 100 ),$value->hc_oficial)),2).' %</td>';
                                echo '<td style="background-color: #000 !important; color: #fff !important; font-weight: 600">'.round((divisao($value->pre,$value->hc_campo)),2).'</td>';
                                echo '<td style="background-color: #000 !important; color: #fff !important; font-weight: 600">'.round((divisao($value->controle,$value->hc_campo)),2).'</td>';
                                echo '<td style="background-color: #000 !important; color: #fff !important; font-weight: 600">'.round((divisao($value->recarga,$value->hc_campo)),2).'</td>';
                            } else {
                                echo '<tr>';
                                echo '<td style="background-color: #b5c0d4 !important; color: #000; font-weight: 600">'.$value->filial.'</td>';
                                echo '<td>'.$value->pre.'</td>';
                                echo '<td>'.$value->controle.'</td>';
                                echo '<td>'.$value->recarga.'</td>';
                                echo '<td>'.$value->hc_oficial.'</td>';
                                echo '<td>'.$value->hc_campo.'</td>';
                                echo '<td>'.round((divisao(($value->hc_campo * 100 ),$value->hc_oficial)),2).' %</td>';
                                echo '<td>'.round((divisao($value->pre,$value->hc_campo)),2).'</td>';
                                echo '<td>'.round((divisao($value->controle,$value->hc_campo)),2).'</td>';
                                echo '<td>'.round((divisao($value->recarga,$value->hc_campo)),2).'</td>';
                            }
                            echo '</tr>';
                        }

                        function divisao($v1,$v2){
                            if ($v1 > 0 && $v2 > 0) {
                                return round(($v1/$v2),2);
                            } else {
                                return 0;
                            }
                        }

                        ?>
                        </tbody>
                    </table>

                </div>

                <div class="mdl-cell mdl-cell--2-col"></div>

                <div class="mdl-cell mdl-cell--5-col" style="margin-left: 20px;" align="right">

                    <table class="semDataTable" align="left">
                        <thead>
                        <tr>
                            <th></th>
                            <th>Controle Total</th>
                            <th>Controle Fácil</th>
                            <th>%</th>
                            <th>Controle Boleto</th>
                            <th>%</th>
                        </tr>
                        </thead>
                        <tbody style="color: white">
                        <?php foreach ($dados['geral_controle'] as $chave => $value) {
                            echo '<tr>';

                            if($chave == count($dados['geral_controle'])-1) {

                            echo '<td style="background-color: #000 !important; color: #fff !important; font-weight: 600">'.$value->filial.'</td>';
                            echo '<td style="background-color: #000 !important; color: #fff !important; font-weight: 600">'.$value->controle_total.'</td>';
                            echo '<td style="background-color: #000 !important; color: #fff !important; font-weight: 600">'.$value->controle_facil.'</td>';
                            echo '<td style="background-color: #000 !important; color: #fff !important; font-weight: 600">'.round((divisao(($value->controle_facil * 100 ),($value->controle_giga+$value->controle_facil))),2).' %</td>';
                            echo '<td style="background-color: #000 !important; color: #fff !important; font-weight: 600">'.$value->controle_giga.'</td>';
                            echo '<td style="background-color: #000 !important; color: #fff !important; font-weight: 600">'.round((divisao(($value->controle_giga * 100 ),($value->controle_giga+$value->controle_facil))),2).' %</td>';
                            } else {
                                 echo '<td style="background-color: #b5c0d4 !important; color: #000; font-weight: 600">'.$value->filial.'</td>';
                                echo '<td>'.$value->controle_total.'</td>';
                                echo '<td>'.$value->controle_facil.'</td>';
                                echo '<td>'.round((divisao(($value->controle_facil * 100 ),($value->controle_giga+$value->controle_facil))),2).' %</td>';
                                echo '<td>'.$value->controle_giga.'</td>';
                                echo '<td>'.round((divisao(($value->controle_giga * 100 ),($value->controle_giga+$value->controle_facil))),2).' %</td>';
                            }

                            echo '</tr>';
                        } ?>
                        </tbody>
                    </table>

                </div>
            </div>

            <div class="mdl-grid contorno novo-background"  align="left">
                <div class="mdl-cell mdl-cell--10-col novo-background" align="left">

                    <h4 style="color: white">PRÉ PAGO</h4>

                    <table class="semDataTable" style="width: 100%">
                        <thead>
                        <tr>
                            <th></th>
                            <?php for ($i=1; $i < 32; $i++) {
                                echo '<th>'.$i.'</th>';
                            } ?>
                            <th>TOTAL</th>
                        </tr>
                        </thead>
                        <tbody style="color: #fff;">

                        <?php

                        $array_f1 = array();
                        $array_f2 = array();

                        echo '<tr>';
                        echo '<td style="background-color: #b5c0d4 !important; color: #000 !important; font-weight: 600">F1</td>';

                        for ($i=1; $i < 32; $i++) {
                            $valor = 0;

                            foreach ($dados['pre_f1'] as $f1) {
                                if ($f1->data == $i) {
                                    $valor = $f1->pre;
                                }
                            }

                            array_push($array_f1, $valor);
                            echo '<td>'.$valor.'</td>';
                        }

                        echo '<td style="background-color: #b5c0d4 !important; color: #000 !important;">'.array_sum($array_f1).'</td>';
                        echo '</tr>';

                        //F2
                        echo '<tr>';
                        echo '<td style="background-color: #b5c0d4 !important; color: #000; font-weight: 600">F2</td>';

                        for ($i=1; $i < 32; $i++) {
                            $valor = 0;

                            foreach ($dados['pre_f2'] as $f2) {
                                if ($f2->data == $i) {
                                    $valor = $f2->pre;
                                }
                            }

                            array_push($array_f2, $valor);
                            echo '<td>'.$valor.'</td>';
                        }

                        echo '<td style="background-color: #b5c0d4 !important; color: #000 !important; ">'.array_sum($array_f2).'</td>';
                        echo '</tr>';

                        //CANAL
                        echo '<tr>';
                        echo '<td style="background-color: #000 !important; color: #fff !important; font-weight: 600">CANAL</td>';

                        for ($i=0; $i < 31; $i++) {

                            echo '<td style="background-color: #000 !important; color: #fff !important">'.($array_f1[$i] + $array_f2[$i]).'</td>';

                        }

                        echo '<td style="background-color: #000 !important; color: #fff !important;">'.(array_sum($array_f1) + array_sum($array_f2)).'</td>';
                        echo '</tr>';

                        ?>



                        </tbody>
                    </table>

                </div>
            </div>

            <div class="mdl-grid contorno novo-background"  align="left">
                <div class="mdl-cell mdl-cell--10-col " align="left">
                    <h4 style="color: white">CONTROLE TOTAL</h4>

                    <table class="semDataTable" style="width: 100%">
                        <thead>
                        <tr>
                            <th></th>
                            <?php for ($i=1; $i < 32; $i++) {
                                echo '<th>'.$i.'</th>';
                            } ?>
                            <th>TOTAL</th>
                        </tr>
                        </thead>
                        <tbody style="color: white">

                        <?php

                        $array_f1 = array();
                        $array_f2 = array();

                        echo '<tr>';
                        echo '<td style="background-color: #b5c0d4 !important; color: #000 !important; font-weight: 600">F1</td>';

                        for ($i=1; $i < 32; $i++) {
                            $valor = 0;

                            foreach ($dados['controle_f1'] as $f1) {
                                if ($f1->data == $i) {
                                    $valor = $f1->controle;
                                }
                            }

                            array_push($array_f1, $valor);
                            echo '<td>'.$valor.'</td>';
                        }

                        echo '<td style="background-color: #b5c0d4 !important; color: #000 !important;">'.array_sum($array_f1).'</td>';
                        echo '</tr>';

                        //F2
                        echo '<tr>';
                        echo '<td style="background-color: #b5c0d4 !important; color: #000 !important; font-weight: 600">F2</td>';

                        for ($i=1; $i < 32; $i++) {
                            $valor = 0;

                            foreach ($dados['controle_f2'] as $f2) {
                                if ($f2->data == $i) {
                                    $valor = $f2->controle;
                                }
                            }

                            array_push($array_f2, $valor);
                            echo '<td>'.$valor.'</td>';
                        }

                        echo '<td style="background-color: #b5c0d4 !important; color: #000 !important; ">'.array_sum($array_f2).'</td>';
                        echo '</tr>';

                        //CANAL
                        echo '<tr>';
                        echo '<td style="background-color: #000 !important; color: #fff !important; font-weight: 600">CANAL</td>';

                        for ($i=0; $i < 31; $i++) {

                            echo '<td style="background-color: black !important; color: white !important">'.($array_f1[$i] + $array_f2[$i]).'</td>';

                        }

                        echo '<td style="background-color: #000 !important; color: #fff !important; ">'.(array_sum($array_f1) + array_sum($array_f2)).'</td>';
                        echo '</tr>';

                        ?>



                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>

    <div class="mdl-cell--2-col " style="padding: 0; background-color: black; margin-top: 2%">
        <div class="col-lg-2 div-input">
            <div style=" border-bottom: 1px solid white;  padding-top: 5%; padding-bottom: 5%">
                <span style="color: white; font-size: 14px;">INPUT DE VENDAS</span>
            </div>

            <!-- input geral de vendas -->
            <div>
                <a href="<?php echo base_url() ?>main/redirecionar/relatorios-view_geral_inputvendas" style="color: white">
                    <img src="<?php echo base_url() ?>stylebootstrap/img/geral_input_de_vendas_input.png" class="inputs">
                    <div class="col-lg-12 inputs-texto" >
                        <p>GERAL INPUT DE VENDAS</p>
                    </div>
                </a><br />
            </div>

            <!-- input por filial 1 -->
            <div>
                <a href="<?php echo base_url() ?>main/redirecionar/relatorios-view_filial1_inputvendas" style="color: white">
                    <img src="<?php echo base_url() ?>stylebootstrap/img/filial_1_input.png" class="inputs">
                    <div class="col-lg-12 inputs-texto" >
                        <p>POR FILIAL 1</p>
                    </div>
                </a><br />
            </div>

            <!-- input por filial 2 -->
            <div>
                <a href="<?php echo base_url() ?>main/redirecionar/relatorios-view_filial2_inputvendas" style="color: white">
                    <img src="<?php echo base_url() ?>stylebootstrap/img/filial_2_input.png" class="inputs">
                    <div class="col-lg-12 inputs-texto" >
                        <p>POR FILIAL 2</p>
                    </div>
                </a><br />
            </div>

            <!-- input historico -->
            <div>
                <a href="<?php echo base_url() ?>main/redirecionar/relatorios-view_historico_inputvendas" style="color: white">
                    <img src="<?php echo base_url() ?>stylebootstrap/img/historico_input.png" class="inputs">
                    <div class="col-lg-12 inputs-texto" >
                        <p>HISTÓRICO</p>
                    </div>
                </a><br />
            </div>

        </div>
    </div>
</div>


<div class="mdl-grid novo-background" style="margin-left: -30px;">
  <div class="mdl-cell mdl-cell--1-col" style="padding-bottom: 100px">
    <a id="excel_geral" target="_blank" style="cursor: pointer;">
      <img src="<?php echo base_url() ?>style/imagens/excel.jpg">
    </a>
  </div>
</div>