<style type="text/css">
	.resultados {
		border-bottom-style: solid;
		border-bottom-width: 2px;
		border-bottom-color: green;
		font-size: 15px;
		padding-bottom: 5px;
	}

	.resultadosDestaque {
		border-bottom-style: solid;
		border-bottom-width: 2px;
		border-bottom-color: #D52B1E;
		font-size: 20px;
		padding-bottom: 10px;
	}
	.small {
		font-size: 15px;
	}
</style>

<div class="mdl-grid">

 <div class="mdl-cell mdl-cell--12-col tituloRelatorio" align="center">

  <h3>HORAS TRABALHADAS</h3>
  <hr>

  </div>


  <div class="mdl-cell mdl-cell--1-col filtroRelatorio" align="center">

 	FILTRO <i class="material-icons">search</i>

  </div>

  <div class="mdl-cell mdl-cell--5-col" align="center">
  	<label for="usuario" class="label">Selecione um GN</label>
  	<select name="usuario" id="usuario" style="width: 100%">
  		<option value="">Selecione...</option>
  		<?php 

  			foreach ($dados as $usuarios) {
  				echo '<option value="'.$usuarios->id_usuario.'">'.$usuarios->nome.'</option>';
  			}

  		 ?>
  	</select>
  </div>

   <div class="mdl-cell mdl-cell--1-col" align="center">
  	<label for="de" class="label">DE</label>
  	<input type="text" class="mdl-textfield__input mascara_data" name="de" id="de">
  </div>

  <div class="mdl-cell mdl-cell--1-col" align="center">
  	<label for="ate" class="label">ATÉ</label>
  	<input type="text" class="mdl-textfield__input mascara_data" name="ate" id="ate">
  </div>

  <div class="mdl-cell mdl-cell--4-col">
  	
  	<div class="mdl-grid titulosResultado">

	  	<div class="mdl-cell mdl-cell--4-col"></div>
	  	<div class="mdl-cell mdl-cell--4-col">
	  		Média Personalizada
	  	</div>

	</div>

	<div id="loadEspecifico"></div>

  	<div class="mdl-spinner mdl-js-spinner is-active" id="progress1"></div>

  </div>

</div>

<div class="mdl-grid">

  <div class="mdl-cell mdl-cell--12-col">

  	<div id="loadDetalhes"></div>

  	<div class="mdl-spinner mdl-js-spinner is-active" id="progress2"></div>

  </div>

</div>

<script type="text/javascript">
	
	$(document).ready(function(){

		$("#progress1").hide();
		$("#progress2").hide();
		//$(".titulosResultado").hide();

		$("#usuario").change(function(){

			if($("#usuario").val() != "") {

				$("#progress1").show();
				$("#progress2").show();
				$(".titulosResultado").show();

				$("#loadEspecifico").load("<?php echo base_url(); ?>controller_relatorios/ajax_horario_especifico",{id_usuario: $(this).val(),de: $("#de").val(),ate: $("#ate").val()}, function (){

					$("#progress1").hide();

				});

				$("#loadDetalhes").load("<?php echo base_url(); ?>controller_relatorios/ajax_horario_geral",{id_usuario: $(this).val()}, function (){

					$("#progress2").hide();

				});

			} else {
				alert('Selecione um usuário');
			}


		});

		$("#de").change(function(){

			if($("#usuario").val() != "") {

				$("#progress1").show();
				$(".titulosResultado").show();

				$("#loadEspecifico").load("<?php echo base_url(); ?>controller_relatorios/ajax_horario_especifico",{id_usuario: $('#usuario').val(),de: $("#de").val(),ate: $("#ate").val()}, function (){

					$("#progress1").hide();

				});

			
			} else {
				alert('Selecione um usuário');
			}


		});

		$("#ate").change(function(){

			if($("#usuario").val() != "") {

				$("#progress1").show();
				$(".titulosResultado").show();

				$("#loadEspecifico").load("<?php echo base_url(); ?>controller_relatorios/ajax_horario_especifico",{id_usuario: $('#usuario').val(),de: $("#de").val(),ate: $("#ate").val()}, function (){

					$("#progress1").hide();

				});

			
			} else {
				alert('Selecione um usuário');
			}


		});


	});


</script>