<style type="text/css">
	.resultados {
		border-bottom-style: solid;
		border-bottom-width: 2px;
		border-bottom-color: green;
		font-size: 18px;
		padding-bottom: 5px;
	}

	.resultadosDestaque {
		border-bottom-style: solid;
		border-bottom-width: 2px;
		border-bottom-color: #D52B1E;
		font-size: 20px;
		padding-bottom: 10px;
	}
	small {
		font-size: 10px;
		font-weight: bold;
	}
</style>

<div class="mdl-grid">

  <div class="mdl-cell mdl-cell--12-col tituloRelatorio" align="center">

  <h3>MÉDIA MENSAL DE FATURAMENTO</h3>
  <hr>

  </div>

</div>

<div class="mdl-grid">

  <div class="mdl-cell mdl-cell--1-col filtroRelatorio" align="center">

 	FILTRO <i class="material-icons">search</i>

  </div>

  <div class="mdl-cell mdl-cell--3-col">
	    <label class="label" for="cnpj">CNPJ</label>
	    <input type="text" class="mdl-textfield__input mascara_cnpj"  name="cnpj" id="cnpj"/>
   </div>

  <div class="mdl-cell mdl-cell--2-col" align="center">
  	<label for="de" class="label">DE</label>
  	<input type="text" class="mdl-textfield__input mascara_data" name="de" id="de">
  </div>

  <div class="mdl-cell mdl-cell--2-col" align="center">
  	<label for="ate" class="label">ATÉ</label>
  	<input type="text" class="mdl-textfield__input mascara_data" name="ate" id="ate">
  </div>

  <div class="mdl-cell mdl-cell--2-col">
		<button class="-col mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" id="buscarf"><i class="material-icons">search</i>Buscar</button>	
	</div>

</div>

<div id="loadfaturamento">

</div>


<script type="text/javascript">
	$(document).ready(function(){

		$("#progress1").hide();

		$('#buscarf').click(function(){

			if($('#cnpj').val() != ""){

				$("#progress1").show();

				$("#loadfaturamento").load("<?php echo base_url(); ?>controller_relatorios/ajax_faturamento",{de: $("#de").val(),ate: $("#ate").val(),cnpj: $("#cnpj").val()}, function (){

					$("#progress1").hide();

				});

			} else {
				alert('Digite o CNPJ');
			}

		});

	});
</script>
