<h2>Relatórios</h2>

<table class="mdl-data-table mdl-js-data-table mdl-shadow--8dp">
  <thead>
    <tr>
      <th>Abrir</th>
      <th>Título</th>
      <th>Descrição</th>
    </tr>
  </thead>

  <tbody>
    <?php foreach ($dados as $aplicacao) {
    	echo '<tr>';

    		echo '<td style="width: 10%;">'.anchor('main/redirecionar/relatorios-'.$aplicacao->aplicacao, '<i class="material-icons">assessment</i> Abrir', array('class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent', 'title' => 'Editar PDV.', 'alt' => 'Abrir.', 'style' => 'margin-top: -7px;')).'</td>';

      		echo '<td style="width: 30%;">'.$aplicacao->titulo_aplicacao.'</td>';
      		echo '<td>'.$aplicacao->descricao_aplicacao.'</td>';
		echo '</tr>';
	} ?>
  </tbody>
 </table>