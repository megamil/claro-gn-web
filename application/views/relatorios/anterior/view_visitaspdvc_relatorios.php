<style type="text/css">
	.resultados {
		border-bottom-style: solid;
		border-bottom-width: 2px;
		border-bottom-color: green;
		font-size: 25px;
		padding-bottom: 5px;
	}

	.resultadosDestaque {
		border-bottom-style: solid;
		border-bottom-width: 2px;
		border-bottom-color: #D52B1E;
		font-size: 40px;
		padding-bottom: 10px;
	}
	.small {
		font-size: 15px;
	}

	.detailsc {
		font-size: 10px;	
		color: green;
	}

	.detailsnc {
		font-size: 10px;
		color: red;	
	}
</style>

<div class="mdl-grid">

<div class="mdl-cell mdl-cell--12-col tituloRelatorio" align="center">

  <h3>MÉDIA DE VISITAS</h3>
  <hr>

  </div>


  <div class="mdl-cell mdl-cell--1-col filtroRelatorio" align="center">

 	FILTRO <i class="material-icons">search</i>

  </div>

  <div class="mdl-cell mdl-cell--4-col" align="center">
  	<label for="usuario" class="label">Selecione um GN</label>
  	<select name="usuario" id="usuario" style="width: 100%">
  		<option value="">Selecione...</option>
  		<?php 

  			foreach ($dados as $usuarios) {
  				echo '<option value="'.$usuarios->id_usuario.'">'.$usuarios->nome.'</option>';
  			}

  		 ?>
  	</select>
  </div>

  <div class="mdl-cell mdl-cell--1-col" align="center">
  	<label for="filial" class="label">Filial</label>
  	<select name="filial" id="filial" style="width: 100%">
  		<option value="">Todas</option>
  		<option value="0">0</option>
  		<option value="1">1</option>
  		<option value="2">2</option>
  	</select>
  </div>

  <div class="mdl-cell mdl-cell--2-col" style="margin-top: -18px">
  	<div class="mdl-grid">
  		<div class="mdl-cell mdl-cell--12-col">
  			<div class="mdl-cell mdl-cell--2-col" align="center">
			  	<label for="de" class="label">De</label>
			  	<input type="text" class="mascara_data" name="de" id="de">
			  </div>
  		</div>

  		<div class="mdl-cell mdl-cell--12-col">
  			 <div class="mdl-cell mdl-cell--2-col" align="center">
			  	<label for="ate" class="label">Até</label>
			  	<input type="text" class="mascara_data" name="ate" id="ate">
			  </div>
  		</div>
  	</div>
  </div>

  <div class="mdl-cell mdl-cell--4-col" style="margin-top: -18px;">

  	<div class="mdl-grid titulosResultado">

  	<div class="mdl-cell mdl-cell--3-col"></div>
	  	<div class="mdl-cell mdl-cell--6-col" align="center">
	  		Média Personalizada
	  	</div>

	</div>

	<div id="loadEspecifico"></div>

  	<div class="mdl-spinner mdl-js-spinner is-active" id="progress1"></div>

  </div>

</div>

<div class="mdl-grid">

  <div class="mdl-cell mdl-cell--12-col">
  	
  	<div class="mdl-grid titulosResultado">

	  	<div class="mdl-cell mdl-cell--1-col"></div>

	  	<div class="mdl-cell mdl-cell--2-col">
	  		POR DIA
	  	</div>

	  	<div class="mdl-cell mdl-cell--2-col">
	  		POR SEMANA
	  	</div>

	  	<div class="mdl-cell mdl-cell--2-col">
	  		POR MÊS
	  	</div>

	  	<div class="mdl-cell mdl-cell--2-col">
	  		POR SEMESTRE
	  	</div>

	  	<div class="mdl-cell mdl-cell--2-col">
	  		POR ANO
	  	</div>

	</div>

  	<div id="loadDetalhes"></div>

  	<div class="mdl-spinner mdl-js-spinner is-active" id="progress2"></div>

  </div>

</div>

<script type="text/javascript">
	
	$(document).ready(function(){

		$("#progress1").hide();
		$("#progress2").hide();
		//$(".titulosResultado").hide();

		$("#usuario").change(function(){

			if($(this).val() != "") {

				$("#progress2").show();
				$(".titulosResultado").show();


				$("#loadDetalhes").load("<?php echo base_url(); ?>controller_relatorios/ajax_visitaspdvc_geral",{id_usuario: $(this).val()}, function (){

					$("#progress2").hide();

				});

			}


		});

		$('#filial').change(function() {

			if($("#usuario").val() != "") {

				$("#progress1").show();
				$(".titulosResultado").show();


				$("#loadEspecifico").load("<?php echo base_url(); ?>controller_relatorios/ajax_visitaspdvc_filtro",{id_usuario: $("#usuario").val(),
					filial: $(this).val(),
					de: $("#de").val(),
					ate: $("#ate").val()}, function (){

					$("#progress1").hide();

				});

			}

		});

		$('#de').change(function() {

			if($("#usuario").val() != "") {

				$("#progress1").show();
				$(".titulosResultado").show();

				$("#loadEspecifico").load("<?php echo base_url(); ?>controller_relatorios/ajax_visitaspdvc_filtro",{id_usuario: $("#usuario").val(),
						filial: $("#filial").val(),
						de: $(this).val(),
						ate: $("#ate").val()}, function (){

					$("#progress1").hide();

				});

			}

		});

		$('#ate').change(function() {

			if($("#usuario").val() != "") {

				$("#progress1").show();
				$(".titulosResultado").show();


				$("#loadEspecifico").load("<?php echo base_url(); ?>controller_relatorios/ajax_visitaspdvc_filtro",{id_usuario: $("#usuario").val(),
						filial: $("#filial").val(),
						de: $("#de").val(),
						ate: $(this).val()}, function (){

					$("#progress1").hide();

				});

			}

		});


	});


</script>