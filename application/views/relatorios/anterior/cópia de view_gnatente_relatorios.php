<style type="text/css">
  .gerarRelatorio {
    color: white !important;
  }
  .tituloRelatorio {
    background-color: white;
    text-align: left;
    height: 40px;
    padding: 10px;
  }
  th {
    background-color: #CCCCCC;
    color: white;
  }
</style>

<script type="text/javascript">
  $(document).ready(function(){
    $('select').select2();
  });
</script>

<div class="mdl-grid">

  <div class="mdl-cell mdl-cell--2-col">
    <select name="filial" id="filial" class="mdl-select">
      <option value="">Todo Período</option>
      <option value="0">Últimos 7 Dias</option>
      <option value="1">Últimos 30 Dias</option>
      <option value="2">último Trimestre</option>
      <option value="2">último Semestre</option>
      <option value="2">último Ano</option>
      <option value="2">Personalizado</option>
    </select>
  </div>

  <div class="mdl-cell mdl-cell--2-col">
    <select name="filial" id="filial" class="mdl-select">
      <option value="">Todas Filiais</option>
      <option value="0">0</option>
      <option value="1">1</option>
      <option value="2">2</option>
    </select>
  </div>

  <div class="mdl-cell mdl-cell--2-col">
    <button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent gerarRelatorio">
      Gerar Relatorio
    </button>
  </div>


</div>

<div class="mdl-grid">

<div class="mdl-cell mdl-cell--12-col tituloRelatorio">
  APURAÇÃO DE PDVs (ATENDE, ATENDE PARCIAL ou NÃO ATENDE)
</div>

</div>

<div class="mdl-grid">

  <div class="mdl-cell mdl-cell--12-col">

  <table style="width: 100%">
    <thead>
      <th></th>
      <th>ATENDE</th>
      <th>ATENDE PARCIALMENTE</th>
      <th>NÃO ATENDE</th>
    </thead>

    <tbody>
      <tr>
        <td width="60%">APRESENTAÇÃO</td>

        <?php 

          function resultado($valor = null,$total = null){
            $perc = (($valor * 100) / $total);
            echo $valor.' de '.$total.' ( '.number_format($perc, 2, '.', '').'%)';
          }

          $tipos = array ('a','n','p');

            for ($i=0; $i < count($tipos); $i++) { 

              echo '<td>';

              foreach ($dados['apresentacao'] as $valor) {

              if ($valor->apresentacao == $tipos[$i] && $valor->quantidade != ""){
                resultado($valor->quantidade,$dados['total']);
              } 

              }

              echo '</td>';

            }

          ?>
      </tr>

      <tr>
        <td width="60%">CONHECIMENTO</td>
        <?php 

         for ($i=0; $i < count($tipos); $i++) { 

              echo '<td>';

              foreach ($dados['conhecimento'] as $valor) {

              if ($valor->conhecimento == $tipos[$i] && $valor->quantidade != ""){
                resultado($valor->quantidade,$dados['total']);
              } 

              }

              echo '</td>';

            }

         ?>
      </tr>

      <tr>
        <td width="60%">BOOK</td>
         <?php 

         for ($i=0; $i < count($tipos); $i++) { 

              echo '<td>';

              foreach ($dados['book'] as $valor) {

              if ($valor->book == $tipos[$i] && $valor->quantidade != ""){
                resultado($valor->quantidade,$dados['total']);
              } 

              }

              echo '</td>';

            }

         ?>
      </tr>
      </tr>

    </tbody>

  </table>

</div>
</div>


<?php if(false){ ?>
<style type="text/css">
	.resultados {
		border-bottom-style: solid;
		border-bottom-width: 2px;
		border-bottom-color: green;
		font-size: 18px;
		padding-bottom: 5px;
	}

	.resultadosDestaque {
		border-bottom-style: solid;
		border-bottom-width: 2px;
		border-bottom-color: #D52B1E;
		font-size: 20px;
		padding-bottom: 10px;
	}
	small {
		font-size: 10px;
		font-weight: bold;
	}
</style>

<div class="mdl-grid">

  <div class="mdl-cell mdl-cell--12-col tituloRelatorio" align="center">

  <h3>GN ATENDE, PARCIAL OU NÃO ATENDE</h3>
  <hr>

  </div>

</div>

<div class="mdl-grid">

  <div class="mdl-cell mdl-cell--1-col filtroRelatorio" align="center">

 	FILTRO <i class="material-icons">search</i>

  </div>

  <div class="mdl-cell mdl-cell--2-col" align="center">
  	<label for="de" class="label">DE</label>
  	<input type="text" class="mdl-textfield__input mascara_data" name="de" id="de">
  </div>

  <div class="mdl-cell mdl-cell--2-col" align="center">
  	<label for="ate" class="label">ATÉ</label>
  	<input type="text" class="mdl-textfield__input mascara_data" name="ate" id="ate">
  </div>

    <div class="mdl-cell mdl-cell--1-col" align="center">
  	<label for="filial" class="label">Filial</label>
  	<select name="filial" id="filial">
  		<option value="">Todas</option>
  		<option value="0">0</option>
  		<option value="1">1</option>
  		<option value="2">2</option>
  	</select>
  </div>

  <div class="mdl-cell mdl-cell--2-col">
		<button class="-col mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" id="buscarrpp"><i class="material-icons">search</i>Buscar</button>	
	</div>

</div>


<div class="mdl-grid titulosResultado">

	<div class="mdl-spinner mdl-js-spinner is-active" id="progress1"></div>		  

  	<div class="mdl-cell mdl-cell--3-col">
  		
  	</div>

  	<div class="mdl-cell mdl-cell--3-col">
  		ATENDE
  	</div>

  	<div class="mdl-cell mdl-cell--3-col">
  		NÃO ATENDE
  	</div>

  	<div class="mdl-cell mdl-cell--3-col">
  		ATENDE PARCIALMENTE
  	</div>

</div>

<div id="loadatende">

<?php 

	function resultado($valor = null,$total = null){
		$perc = (($valor * 100) / $total);
		echo $valor.' de '.$total.' ( '.number_format($perc, 2, '.', '').'%)';
	}

	$tipos = array ('a','n','p');

	echo '<div class="mdl-grid resultadosDestaque">
	<div class="mdl-cell mdl-cell--3-col">
  		APRESENTAÇÃO
  	</div>';

  	for ($i=0; $i < count($tipos); $i++) { 

  		echo '<div class="mdl-cell mdl-cell--3-col">';

  		foreach ($dados['apresentacao'] as $valor) {

			if ($valor->apresentacao == $tipos[$i] && $valor->quantidade != ""){
				resultado($valor->quantidade,$dados['total']);
			} 

	  	}

	  	echo '</div>';

  	}

  	echo '</div>';

  	echo '<div class="mdl-grid resultadosDestaque">
	<div class="mdl-cell mdl-cell--3-col">
  		CONHECIMENTO
  	</div>';

  	for ($i=0; $i < count($tipos); $i++) { 

  		echo '<div class="mdl-cell mdl-cell--3-col">';

  		foreach ($dados['conhecimento'] as $valor) {

			if ($valor->conhecimento == $tipos[$i] && $valor->quantidade != ""){
				resultado($valor->quantidade,$dados['total']);
			} 

	  	}

	  	echo '</div>';

  	}

  	echo '</div>';

  	echo '<div class="mdl-grid resultadosDestaque">
	<div class="mdl-cell mdl-cell--3-col">
  		BOOK
  	</div>';

  	for ($i=0; $i < count($tipos); $i++) { 

  		echo '<div class="mdl-cell mdl-cell--3-col">';

  		foreach ($dados['book'] as $valor) {

			if ($valor->book == $tipos[$i] && $valor->quantidade != ""){
				resultado($valor->quantidade,$dados['total']);
			} 

	  	}

	  	echo '</div>';

  	}

  	echo '</div>';

 ?>
</div>
<script type="text/javascript">
	$(document).ready(function(){

		$("#progress1").hide();

		$('#buscarrpp').click(function(){

			$("#progress1").show();

			$("#loadatende").load("<?php echo base_url(); ?>controller_relatorios/ajax_atende",{filial: $("#filial").val(),de: $("#de").val(),ate: $("#ate").val()}, function (){

				$("#progress1").hide();

			});

		});

	});
</script>
<?php } ?>