<style type="text/css">
	.resultados {
		border-bottom-style: solid;
		border-bottom-width: 2px;
		border-bottom-color: green;
		font-size: 15px;
		padding-bottom: 5px;
	}

	.resultadosDestaque {
		border-bottom-style: solid;
		border-bottom-width: 2px;
		border-bottom-color: #D52B1E;
		font-size: 20px;
		padding-bottom: 10px;
	}
	.small {
		font-size: 15px;
	}
</style>

<div class="mdl-grid">

 <div class="mdl-cell mdl-cell--12-col tituloRelatorio" align="center">

  <h3>MÉDIA ABERTO / MOBILE / TELEFONIA</h3>
  <hr>

  </div>


  <div class="mdl-cell mdl-cell--1-col filtroRelatorio" align="center">

 	FILTRO <i class="material-icons">search</i>

  </div>

  <div class="mdl-cell mdl-cell--3-col" align="center">
  	<label for="usuario" class="label">Selecione um GN</label>
  	<select name="usuario" id="usuario" style="width: 100%;">
  		<option>Selecione...</option>
  		<?php 

  			foreach ($dados as $usuarios) {
  				echo '<option value="'.$usuarios->id_usuario.'">'.$usuarios->nome.'</option>';
  			}

  		 ?>
  	</select>
  </div>

  <div class="mdl-cell mdl-cell--2-col" align="center">
  	<label for="de" class="label">DE</label>
  	<input type="text" class="mdl-textfield__input mascara_data" name="de" id="de">
  </div>

  <div class="mdl-cell mdl-cell--2-col" align="center">
  	<label for="ate" class="label">ATÉ</label>
  	<input type="text" class="mdl-textfield__input mascara_data" name="ate" id="ate">
  </div>

  <div class="mdl-cell mdl-cell--2-col">
	    <label class="label" for="cnpj">CNPJ</label>
	    <input type="text" class="mdl-textfield__input mascara_cnpj"  name="cnpj" id="cnpj"/>
   </div>

   <div class="mdl-cell mdl-cell--2-col">
		<button class="-col mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" id="buscartam"><i class="material-icons">search</i>Buscar</button>	
	</div>

</div>

<div class="mdl-grid">

  <div class="mdl-cell mdl-cell--12-col">
  	
  	<div class="mdl-grid titulosResultado">

  		<div class="mdl-cell mdl-cell--3-col"></div>

	  	<div class="mdl-cell mdl-cell--1-col">
	  		
	  	</div>

	  	<div class="mdl-cell mdl-cell--2-col">
	  		Total atendimentos:
	  	</div>

	  	<div class="mdl-cell mdl-cell--1-col">
	  		Aberto
	  	</div>

	  	<div class="mdl-cell mdl-cell--1-col">
	  		Mobile
	  	</div>

	  	<div class="mdl-cell mdl-cell--1-col">
	  		Telefonia
	  	</div>

  	</div>

  	<div id="loadDetalhes"></div>

  	<div class="mdl-spinner mdl-js-spinner is-active" id="progress"></div>

  </div>

</div>


<script type="text/javascript">
	
	$(document).ready(function(){

		$("#progress").hide();

		$("#buscartam").click(function(){

			if($("#usuario").val() != "") {

				$("#progress").show();
				$(".titulosResultado").show();

				$("#loadDetalhes").load("<?php echo base_url(); ?>controller_relatorios/ajax_mediatam",{id_usuario: $('#usuario').val(),de: $('#de').val(),ate: $('#ate').val(),cnpj: $('#cnpj').val()}, function (){

					$("#progress").hide();

				});

			}


		});


	});


</script>