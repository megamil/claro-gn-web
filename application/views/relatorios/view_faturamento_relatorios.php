<style type="text/css">
	.destaque {
		font-size: 46px;
	}

	.adicional{
		font-size: 46px;
	}

	.contorno{
		border-style: solid;
		border-width: 1px;
		border-color: #dde;
	}

</style>

<div class="mdl-grid" style="margin-left: -15px;">

	<div class="mdl-cell mdl-cell--2-col">
	    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
	    	<label class="label" for="data_filtro">Período</label>
	    	<select class="select filtrar" name="data_filtro" id="data_filtro" style="width: 100%;">
	    		<!-- <option value="0">Todo Período</option> -->
	    		<!-- <option value="1">Hoje</option>
	    		<option value="2">Última Semana</option>
	    		<option value="3">Última Quinzena</option>
	    		<option value="4">Último Mês</option>
	    		<option value="5">Último Bimestre</option>
	    		<option value="6">Último Trimestre</option>
	    		<option value="7">Último Semestre</option>
	    		<option value="8">Último Ano</option> -->
	    		<option value="9">Personalizado</option>
	    	</select>
	    </div>
	</div>

	<div class="mdl-cell mdl-cell--2-col" align="center" style="margin-top: 27px;">
	  	<label for="de" class="label">DE</label>
	  	<input type="text" class="mdl-textfield__input filtrar mascara_data" name="de" id="de">
	  </div>

	  <div class="mdl-cell mdl-cell--2-col" align="center" style="margin-top: 27px;">
	  	<label for="ate" class="label">ATÉ</label>
	  	<input type="text" class="mdl-textfield__input filtrar mascara_data" name="ate" id="ate">
	  </div>

 	<div class="mdl-cell mdl-cell--2-col" style="margin-top: 27px;">
	    <label class="label" for="cnpj">CNPJ</label>
	    <input type="text" class="mdl-textfield__input mascara_cnpj filtrar"  name="cnpj" id="cnpj"/>
   </div>

</div>

<div class="mdl-grid contorno" style="background-color: white;" align="left">

	<div class="mdl-cell--10-col">

		MÉDIA DE FATURAMENTO
		<h3 align="left" id="valorTotal">R$ </h3>

	</div>

	<div class="mdl-cell--2-col" align="center" style="border-left-style: solid; border-left-color: black; border-left-width: 1px;">

		<h1  style="margin-bottom: -15px;" id="curva"></h1>
		<span id="curvaText">curva</span>

	</div>
	

</div>

<div class="mdl-grid contorno" style="background-color: white; margin-top: 10px;">

	<div class="mdl-cell mdl-cell--12-col" id="loadfaturamento">

		<table class="mdl-data-table mdl-js-data-table mdl-shadow--8dp semDataTable" style="width: 100%;">
			<thead>
				<tr style="background-color: #dedede">
					<th width="80%" class="mdl-data-table__cell--non-numeric">Venda de Aparelhos - Faixa</th>
					<th width="20%">Total</th>
				</tr>
			</thead>
		  <tbody>

			 <tr>
			 	<th width="80%" class="mdl-data-table__cell--non-numeric"></th>
				<th width="20%"></th>
			 </tr>

		  </tbody>
		</table>

	</div>

</div>

<div class="mdl-grid contorno" style="background-color: white; margin-top: 10px" align="left">
	

	<div class="mdl-cell mdl-cell--4-col" align="left" style="margin-top: 10px;"> <strong>CURVA A</strong></div>


	<div class="mdl-cell mdl-cell--4-col" align="left" style="margin-top: 10px;"><strong>CURVA B</strong></div>


	<div class="mdl-cell mdl-cell--4-col" align="left" style="margin-top: 10px;"><strong>CURVA C</strong></div>


	<div class="mdl-cell mdl-cell--4-col" align="left">
		> 700 K
	</div>

	<div class="mdl-cell mdl-cell--4-col" align="left">
		300K - 699K
	</div>

	<div class="mdl-cell mdl-cell--4-col" align="left">
		< 300K
	</div>


</div>


<div class="mdl-grid" style="margin-left: -30px;">

	<div class="mdl-cell mdl-cell--1-col">
		 <a id="excel_geral" target="_blank" style="cursor: pointer;">
	      <img src="<?php echo base_url() ?>style/imagens/excel.jpg">
	    </a>
	</div>
	<div class="mdl-cell mdl-cell--1-col" style="margin-left: -20px;" style="cursor: pointer;">
		<a href="<?php echo base_url() ?>faturamento.pdf">
			<img src="<?php echo base_url() ?>style/imagens/pdf.jpg">
		</a>
	</div>

</div>

<script type="text/javascript">
	$(document).ready(function(){

		$("#progress1").hide();

		$('#cnpj').change(function(){

			if($('#cnpj').val() != ""){

				$("#progress1").show();

				$("#loadfaturamento").load("<?php echo base_url(); ?>controller_relatorios/ajax_faturamento",{de: $("#de").val(),ate: $("#ate").val(),cnpj: $("#cnpj").val()}, function (){

					$("#progress1").hide();

				});

			} else {
				alert('Digite o CNPJ');
			}

		});

		$('#excel_geral').click(function(){

			if($('#cnpj').val() != ""){

		      var cnpj = $("#cnpj").val();
		      var de = $("#de").val();
		      var ate = $("#ate").val();
		      var filial_filtro = $("#filial_filtro").val();

		      var url = "<?php echo base_url(); ?>controller_excel/excel_faturamento?de="+de+"&ate="+ate+"&filial_filtro="+filial_filtro+"&cnpj="+cnpj;

		      $.ajax({
		          url: url,
		          type: "post",
		        datatype: 'json',
		          success: function(data){
		        window.location = url;

		        },
		          error:function(){
		              
		          }   
		        });

	      	} else {
				alert('Digite o CNPJ');
			}

	    });

	});
</script>

