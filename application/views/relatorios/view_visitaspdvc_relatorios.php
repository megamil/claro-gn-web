<script type="text/javascript">
$(document).ready(function(){

	$('#print').click(function(){
		print();
	});

	$('select').select2();

	//$('#de_filtro').hide();
	//$('#ate_filtro').hide();

	$('#data_filtro').change(function(){

		ajax();

		if ($(this).val() == 9) {
		//	$('#de_filtro').show();
		//	$('#ate_filtro').show();
		} else {
		//	$('#de_filtro').hide();
		//	$('#ate_filtro').hide();

			ajax();

		}

	});

	//Caso vá específicar uma Filial o gráfico deixa de fazer sentido.
	$('#filial_filtro').change(function(){
		
		if($(this).val() > 0){
			$('#piechart_3d').hide();
		} else {
			$('#piechart_3d').show();
		}

		ajax();

	});

	$('#de, #ate').change(function(){
		ajax();
	});


	$('#gn_filtro').change(function(){
		
		// if($(this).val() > 0){
		// 	$('.semDataTable').hide();
		// } else {
		// 	$('.semDataTable').show();
		// }

		ajax();
		
	});

	function ajax(){


		$.ajax({
	      url: "<?php echo base_url(); ?>controller_relatorios/ajax_visitaspdvc_filtro",
	      type: "post",
	      data: {data_filtro: $("#data_filtro").val(),
					de: $("#de").val(),
					ate: $("#ate").val(),
					filial_filtro: $("#filial_filtro").val(),
					gn_filtro: $("#gn_filtro").val()},
	  	datatype: 'json',
	      success: function(data){

	            $('#mediav').text(data[0].media_checkout);
	            $('#totalv').text(data[0].total_checkout);
	            $('#visitasfc').text(data[0].fora_carteira);

				chart.options.data[0].dataPoints[0].y = parseInt(data[0].filiais2);
				chart.options.data[0].dataPoints[1].y = parseInt(data[0].filiais1);
				chart.render();


	      },
	      error:function(){
	          
	      }   
	    });

	    $(".semDataTable").load("<?php echo base_url(); ?>controller_relatorios/ajax_visitaspdvc_filtro_lista",{data_filtro: $("#data_filtro").val(),
			de: $("#de").val(),
			ate: $("#ate").val(),
			filial_filtro: $("#filial_filtro").val(),
			gn_filtro: $("#gn_filtro").val()}, function (){					

		});

	}
		
	var receberFoco = document.getElementById("detaques");

	$(document).on('click','.dadosTotais',function(){
		
		$(".load").load("<?php echo base_url(); ?>controller_relatorios/ajax_visitaspdvc_detalhes",{data_filtro: $("#data_filtro").val(),
			de: $("#de").val(),
			ate: $("#ate").val(),
			gn_filtro: $(this).attr('cod'),
			fora: 0}, function (){		

			receberFoco.scrollIntoView();			

		});


	});

	$(document).on('click','.dadosFora',function(){

		$(".load").load("<?php echo base_url(); ?>controller_relatorios/ajax_visitaspdvc_detalhes",{data_filtro: $("#data_filtro").val(),
			de: $("#de").val(),
			ate: $("#ate").val(),
			gn_filtro: $(this).attr('cod'),
			fora: 1}, function (){	

			receberFoco.scrollIntoView();				

		});

	});

	$('#pdf_geral').click(function(){

		var data_filtro = $("#data_filtro").val();
		var de = $("#de").val();
		var ate = $("#ate").val();
		var filial_filtro = $("#filial_filtro").val();
		var gn_filtro = $("#gn_filtro").val();

		var url = "<?php echo base_url(); ?>controller_pdf/pdf_visitas?de="+de+"&ate="+ate+"&filial_filtro="+filial_filtro+"&gn_filtro="+gn_filtro+"&=data_filtro"+data_filtro+"&nomeGN="+$("#gn_filtro :selected").text();

		window.open(url, '_blank');

	});

	$('#excel_geral').click(function(){

		var data_filtro = $("#data_filtro").val();
		var de = $("#de").val();
		var ate = $("#ate").val();
		var filial_filtro = $("#filial_filtro").val();
		var gn_filtro = $("#gn_filtro").val();

		var url = "<?php echo base_url(); ?>controller_excel/excel_visitaspdvc?de="+de+"&ate="+ate+"&filial_filtro="+filial_filtro+"&gn_filtro="+gn_filtro+"&=data_filtro"+data_filtro+"&nomeGN="+$("#gn_filtro :selected").text();

		$.ajax({
	      url: url,
	      type: "post", 
	  	datatype: 'json',
	      success: function(data){
	      	window.location = url;
	      },
	      error:function(){
	          
	      }   
	    });

	});


	/*GRÁFICO - PIZZA*/

	//gerarGrafico(,<?php //echo $dados['dados']->filiais2; ?>);

	var chart;
	var filial1 = parseInt(<?php echo $dados['dados']->filiais1; ?>);
	var filial2 = parseInt(<?php echo $dados['dados']->filiais2; ?>);

	window.onload = function () {

			chart = new CanvasJS.Chart("grafico_visitas",{

				data: [
				{
					type: "pie",
					indexLabelFontColor: "black",    
					indexLabelLineColor: "darkgrey",
					indexLabelFontWeight: "bold",
					indexLabelFontSize: 15,
					indexLabelFontFamily: "Garamond",
					toolTipContent: "{name} #percent% ({y})", 
					indexLabel: "{name} #percent% ({y})", 
					showInLegend: true,
					dataPoints: [
						{ y: filial2 , name: "Filial 2", color: "#5cb85c", indexLabel: "Filial 2 #percent% ({y})"},
						{ y: filial1 , name: "Filial 1", color: "#d9534f", indexLabel: "Filial 1 #percent% ({y})"}
						
					]
				}
				]
			});

			chart.render();
	}




});

</script>


<style type="text/css">
	.destaque {
		font-size: 46px;
	}

	.adicional{
		font-size: 46px;
	}

	.contorno{
		border-style: solid;
		border-width: 1px;
		border-color: #dde;
	}

</style>


<div class="mdl-grid" style="margin-left: -15px;">

	<div class="mdl-cell mdl-cell--3-col">
	    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
	    	<label class="label" for="data_filtro">Período</label>
	    	<select class="select" name="data_filtro" id="data_filtro" style="width: 100%;">
	    		<!-- <option value="0">Todo Período</option> -->
	    		<!-- <option value="1">Hoje</option>
	    		<option value="2">Última Semana</option>
	    		<option value="3">Última Quinzena</option>
	    		<option value="4">Último Mês</option>
	    		<option value="5">Último Bimestre</option>
	    		<option value="6">Último Trimestre</option>
	    		<option value="7">Último Semestre</option>
	    		<option value="8">Último Ano</option> -->
	    		<option value="9">Personalizado</option>
	    	</select>
	    </div>
	</div>

	<div class="mdl-cell mdl-cell--2-col" id="de_filtro" style="margin-top: 25px">
		<label class="label" for="de">De</label>
		<input class="mdl-textfield__input mascara_data obrigatorio" type="text" id="de" name="de" aviso="Data Limite">
	</div>

	<div class="mdl-cell mdl-cell--2-col" id="ate_filtro" style="margin-top: 25px">
		<label class="label" for="ate">Até</label>
		<input class="mdl-textfield__input mascara_data obrigatorio" type="text" id="ate" name="ate" aviso="Data Limite">
	</div>

	<div class="mdl-cell mdl-cell--2-col">
	    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
	    	<label class="label" for="filial_filtro">Filial</label>
	    	<select class="select" name="filial_filtro" id="filial_filtro" style="width: 100%;">
	    		<option value="">Filiais</option>
	    		<option value="1">Filial 1</option>
	    		<option value="2">Filial 2</option>
	    	</select>
	    </div>
	</div>

	<div class="mdl-cell mdl-cell--2-col">
	    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
	    	<label class="label" for="gn_filtro">GNs</label>
	    	<select class="select" name="gn_filtro" id="gn_filtro" style="width: 100%;">
	    	<option value="">TODOS</option>
    		<?php 

	  			foreach ($dados['usuarios'] as $usuarios) {
	  				echo '<option value="'.$usuarios->id_usuario.'">'.$usuarios->nome.'</option>';
	  			}

	  		 ?>
	    	</select>
	    </div>
	</div>

</div>

<div class="mdl-grid contorno" style="background-color: white;" align="left">
		
	<b>MÉDIA DE VISITAS</b>

</div>

<div class="mdl-grid" style="background-color: white; padding: 0px;">
	<div class="mdl-cell mdl-cell--3-col contorno" align="center" style="margin: 0px; padding-top: 15px;">
	RESULTADO por GN <br>
	<br>
	<br>

	<span class="destaque" id="mediav"><?php echo $dados['dados']->media_checkout; ?></span> <br>
	<small>Média de Visitas</small> <br><br><br>

	<span class="adicional" id="totalv"><?php echo $dados['dados']->total_checkout; ?></span> <br>
	<small>Total de Visitas</small> <br><br><br>

	<span class="adicional" id="visitasfc"><?php echo $dados['dados']->fora_carteira; ?></span> <br>
	<small>Visitas fora da carteira</small>
	<br>
	<br>


	</div>

	<div class="mdl-cell mdl-cell--9-col contorno" style="padding: 12px; margin: 0px; border-right-style: none;">
		<div align="left">DESEMPENHO POR FILIAL</div>
		<div id="grafico_visitas" style="width:100%; height: 90%;"></div>
	</div>
</div>

<!-- Lista dos gns -->

<div class="mdl-grid contorno" style="background-color: white; margin-top: 10px;">

	<div class="mdl-cell mdl-cell--12-col">

		<table class="mdl-data-table mdl-js-data-table mdl-shadow--8dp semDataTable" style="width: 100%">
			<thead>
				<tr>
					<th width="40%" class="mdl-data-table__cell--non-numeric">GN</th>
					<th width="20%">Média do GN por período</th>
					<th width="20%">Total de Visitas a PDVs</th>
					<th width="20%">Fora da Carteira (PDVs)</th>
				</tr>
			</thead>
		  <tbody>

		  <?php 

	  			foreach ($dados['visitar_gn'] as $visitar_gn) {
  					echo '<tr>
				   		<td width="80%" class="mdl-data-table__cell--non-numeric">'.$visitar_gn->usuario.'</td>
				   		<td >'.$visitar_gn->media.'</td>
				   		<td cod="'.$visitar_gn->id_usuario.'" class="dadosTotais" width="20%" style="cursor: pointer;"><a href="#detaques">'.$visitar_gn->total.'</a></td>
				   		<td cod="'.$visitar_gn->id_usuario.'" class="dadosFora" width="20%" style="cursor: pointer;"><a href="#detaques">'.$visitar_gn->fora.'</a></td>
				   	</tr>';
	  			}

	  		 ?>

		  </tbody>
		</table>

	</div>

</div>

<div class="mdl-grid" style="margin-left: -30px;">

	<div class="mdl-cell mdl-cell--1-col">
		<a id="excel_geral" target="_blank" style="cursor: pointer;">
			<img src="<?php echo base_url() ?>style/imagens/excel.jpg">
		</a>
	</div>
	<div class="mdl-cell mdl-cell--1-col" style="margin-left: -30px;">
		<a id="pdf_geral" target="_blank" style="cursor: pointer;">
			<img src="<?php echo base_url() ?>style/imagens/pdf.jpg">
		</a>
	</div>

</div>

<div class="load" id="detaques">
	
</div>
