<style type="text/css">
	.resultados {
		border-bottom-style: solid;
		border-bottom-width: 2px;
		border-bottom-color: green;
		font-size: 18px;
		padding-bottom: 5px;
	}

	.resultadosDestaque {
		border-bottom-style: solid;
		border-bottom-width: 2px;
		border-bottom-color: #D52B1E;
		font-size: 20px;
		padding-bottom: 10px;
	}
	small {
		font-size: 10px;
		font-weight: bold;
	}
</style>


<div class="mdl-grid">

  <div class="mdl-cell mdl-cell--3-col">
	    <label class="label" for="cnpj">CNPJ</label>
	    <input type="text" class="mdl-textfield__input mascara_cnpj"  name="cnpj" id="cnpj"/>
   </div>

  <div class="mdl-cell mdl-cell--2-col" align="center">
  	<label for="de" class="label">DE</label>
  	<input type="text" class="mdl-textfield__input mascara_data" name="de" id="de">
  </div>

  <div class="mdl-cell mdl-cell--2-col" align="center">
  	<label for="ate" class="label">ATÉ</label>
  	<input type="text" class="mdl-textfield__input mascara_data" name="ate" id="ate">
  </div>

  <div class="mdl-cell mdl-cell--1-col" align="center">
  	<label for="filial" class="label">Filial</label>
  	<select name="filial" id="filial" style="width: 100%">
  		<option value="">Todas</option>
  		<option value="0">0</option>
  		<option value="1">1</option>
  		<option value="2">2</option>
  	</select>
  </div>

  <div class="mdl-cell mdl-cell--2-col">
		<button class="-col mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" id="buscarf"><i class="material-icons">search</i>Buscar</button>	
	</div>

</div>

<div class="mdl-grid contorno" style="background-color: white; margin-bottom: 30px;" align="left">

  <div class="mdl-cell--12-col">

    <strong>5 PALAVRAS QUE MAIS SE REPETEM</strong>
    

  </div>
  

</div>

<div id="loadfaturamento" class="contorno" style="background-color: white;" align="left">

  <div class="mdl-grid">

  		<div class="mdl-cell mdl-cell--2-col" align="center">
        <img src="<?php echo base_url() ?>style/imagens/relatorios/logo_claro.png" width="40px"> <br>
        CAMPANHA
      </div>
      <?php 

        for ($i=0; $i < $dados['campanhac']->num_rows(); $i++) { 
          echo '<div class="mdl-cell mdl-cell--2-col"><strong>'.$dados['campanhac']->row($i)->palavra.'</strong> <br> com ('.$dados['campanhac']->row($i)->quantidade.') Repetições</div>';
        }

       ?>

  </div>

    <hr>

  <div class="mdl-grid">

  		<div class="mdl-cell mdl-cell--2-col" align="center">
        <img src="<?php echo base_url() ?>style/imagens/relatorios/logo_claro.png" width="40px"> <br>
        PRODUTOS
      </div>
  		<?php 

        for ($i=0; $i < $dados['produtoc']->num_rows(); $i++) { 
          echo '<div class="mdl-cell mdl-cell--2-col"><strong>'.$dados['produtoc']->row($i)->palavra.'</strong> <br> com ('.$dados['produtoc']->row($i)->quantidade.') Repetições</div>';
        }

       ?>
  	
  </div>
<hr>
  <div class="mdl-grid">

      <div class="mdl-cell mdl-cell--2-col" align="center">
        <img src="<?php echo base_url() ?>style/imagens/relatorios/logo_oi.png" width="40px"> <br>
        CAMPANHA
      </div>
      <?php 

        for ($i=0; $i < $dados['campanhao']->num_rows(); $i++) { 
          echo '<div class="mdl-cell mdl-cell--2-col"><strong>'.$dados['campanhao']->row($i)->palavra.'</strong> <br> com ('.$dados['campanhao']->row($i)->quantidade.') Repetições</div>';
        }

       ?>

  </div>

    <hr>

  <div class="mdl-grid">

      <div class="mdl-cell mdl-cell--2-col" align="center">
        <img src="<?php echo base_url() ?>style/imagens/relatorios/logo_oi.png" width="40px"> <br>
        PRODUTOS
      </div>
      <?php 

        for ($i=0; $i < $dados['produtoo']->num_rows(); $i++) { 
          echo '<div class="mdl-cell mdl-cell--2-col"><strong>'.$dados['produtoo']->row($i)->palavra.'</strong> <br> com ('.$dados['produtoo']->row($i)->quantidade.') Repetições</div>';
        }

       ?>
    
  </div>
<hr>
    <div class="mdl-grid">

        <div class="mdl-cell mdl-cell--2-col" align="center">
        <img src="<?php echo base_url() ?>style/imagens/relatorios/logo_vivo.png" width="40px"> <br>
        CAMPANHA
      </div>
        <?php 

          for ($i=0; $i < $dados['campanhav']->num_rows(); $i++) { 
            echo '<div class="mdl-cell mdl-cell--2-col"><strong>'.$dados['campanhav']->row($i)->palavra.'</strong> <br> com ('.$dados['campanhav']->row($i)->quantidade.') Repetições</div>';
          }

         ?>

    </div>

      <hr>

    <div class="mdl-grid">

        <div class="mdl-cell mdl-cell--2-col" align="center">
        <img src="<?php echo base_url() ?>style/imagens/relatorios/logo_vivo.png" width="40px"> <br>
        PRODUTOS
      </div>
        <?php 

          for ($i=0; $i < $dados['produtov']->num_rows(); $i++) { 
            echo '<div class="mdl-cell mdl-cell--2-col"><strong>'.$dados['produtov']->row($i)->palavra.'</strong> <br> com ('.$dados['produtov']->row($i)->quantidade.') Repetições</div>';
          }

         ?>
      
    </div>

<hr>
  <div class="mdl-grid">

      <div class="mdl-cell mdl-cell--2-col" align="center">
        <img src="<?php echo base_url() ?>style/imagens/relatorios/logo_tim.png" width="40px"> <br>
        CAMPANHA
      </div>
      <?php 

        for ($i=0; $i < $dados['campanhat']->num_rows(); $i++) { 
          echo '<div class="mdl-cell mdl-cell--2-col"><strong>'.$dados['campanhat']->row($i)->palavra.'</strong> <br> com ('.$dados['campanhat']->row($i)->quantidade.') Repetições</div>';
        }

       ?>

  </div>

    <hr>

  <div class="mdl-grid">

      <div class="mdl-cell mdl-cell--2-col" align="center">
        <img src="<?php echo base_url() ?>style/imagens/relatorios/logo_tim.png" width="40px"> <br>
        PRODUTOS
      </div>
      <?php 

        for ($i=0; $i < $dados['produtot']->num_rows(); $i++) { 
          echo '<div class="mdl-cell mdl-cell--2-col"><strong>'.$dados['produtot']->row($i)->palavra.'</strong> <br> com ('.$dados['produtot']->row($i)->quantidade.') Repetições</div>';
        }

       ?>
    
  </div>

</div>

<div class="mdl-grid" style="margin-left: -30px;">

  <div class="mdl-cell mdl-cell--1-col">
    <a id="excel_geral" target="_blank" style="cursor: pointer;">
      <img src="<?php echo base_url() ?>style/imagens/excel.jpg">
    </a>
  </div>
  <div class="mdl-cell mdl-cell--1-col" style="margin-left: -20px;">
    <img src="<?php echo base_url() ?>style/imagens/pdf.jpg">
  </div>

</div>

<script type="text/javascript">
	$(document).ready(function(){

		$("#progress1").hide();

		$('#buscarf').click(function(){

			$("#progress1").show();

			$("#loadfaturamento").load("<?php echo base_url(); ?>controller_relatorios/ajax_campanha",{de: $("#de").val(),ate: $("#ate").val(),cnpj: $("#cnpj").val(),filial: $("#filial").val()}, function (){

				$("#progress1").hide();

			});

		});

    $('#excel_geral').click(function(){

      var cnpj = $("#data_filtro").val();
      var de = $("#de").val();
      var ate = $("#ate").val();
      var filial_filtro = $("#filial_filtro").val();

      var url = "<?php echo base_url(); ?>controller_excel/excel_campanhas?de="+de+"&ate="+ate+"&=cnpj"+cnpj+"$filial_filtro="+filial_filtro;

      $.ajax({
          url: url,
          type: "post",
        datatype: 'json',
          success: function(data){
        window.location = url;

        },
          error:function(){
              
          }   
        });

    });



	});
</script>
