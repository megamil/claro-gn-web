<?php echo form_open('controller_checkout/excel_export'); ?>
<div class="mdl-grid">

	<div class="mdl-cell mdl-cell--3-col">

	<label for="fk_usuario">Filtrar Por Usuário</label>
	<select name="fk_usuario" id="fk_usuario">
		<option value="">Todos</option>
		<?php foreach ($dados['usuarios'] as $usuario) {

	    	echo '<option value="'.$usuario->id_usuario.'">'.$usuario->usuario.'</option>';

		} ?>
	</select>

	</div>

	<div class="mdl-cell mdl-cell--3-col">

		<label for="ate">De</label>
	    <input class="mdl-textfield__input" type="text" id="de" name="de">

	</div>

	<div class="mdl-cell mdl-cell--3-col">

		<label for="ate">Até</label>
	    <input class="mdl-textfield__input" type="text" id="ate" name="ate">
	    
	</div>

	<div class="mdl-cell mdl-cell--3-col">
	<a type="button" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" id="filtrar">Aplicar Filtro</a>
	</div>

</div>


<div align="center">
	<button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent">Exportar para Excel</button>
</div>

<? echo form_close(); ?>

<div class="mdl-spinner mdl-js-spinner" id="loadSpinner"></div>
<div id="load" align="center">

	<table class="mdl-data-table mdl-js-data-table mdl-shadow--8dp">
	  <thead>
	    <tr>
	      <th class="mdl-data-table__cell--non-numeric">ID</th>
	      <th class="mdl-data-table__cell--non-numeric">Data</th>
	      <th class="mdl-data-table__cell--non-numeric">Usuário</th>
	      <th class="mdl-data-table__cell--non-numeric">Rede</th>
	      <th class="mdl-data-table__cell--non-numeric">Endereço de origem</th>
	    </tr>
	  </thead>
	  <tbody>
	    <?php foreach ($dados['checkout'] as $checkout) {
	    	echo '<tr>';
	    	echo '<td class="mdl-data-table__cell--non-numeric">'.$checkout->id_checkout.'</td>';
			echo '<td class="mdl-data-table__cell--non-numeric">'.$checkout->data_checkout.'</td>';
			echo '<td class="mdl-data-table__cell--non-numeric">'.$checkout->usuario.'</td>';
			echo '<td class="mdl-data-table__cell--non-numeric">'.$checkout->rede.'</td>';
			echo '<td class="mdl-data-table__cell--non-numeric">'.$checkout->endereco_exato.'</td>';
			echo '</tr>';
		} ?>
	  </tbody>
	</table>
</div>

<script type="text/javascript">
	$(document).ready(function(){

		$("#fk_usuario").select2();

		$("#de, #ate").datepicker({
		    dateFormat: 'dd/mm/yy',
		    dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
		    dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
		    dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
		    monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		    monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
		    nextText: 'Próximo',
		    prevText: 'Anterior'
		});

		$('#filtrar').click(function(){

			$('#loadSpinner').addClass('is-active');

			var usuario = $('#fk_usuario').val();
			var de = $('#de').val();
			var ate = $('#ate').val();

			$('#load').load(<?php echo "'".base_url()."controller_checkout/filtroAjax'"; ?>, {"usuario" : usuario, "de" : de, "ate" : ate}, function(){

				$('#loadSpinner').removeClass('is-active');

			});


		});

	});
</script>