<!DOCTYPE html>
<html>

	<head>

		<meta charset="UTF-8">	
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><?php echo $tela; ?></title>

		<!--ICON-->
		<link rel="shortcut icon" href="<?php echo base_url(); ?>stylebootstrap/img/favicon.jpg" type="image/x-icon">

		<!--CSS-->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>style/mdl/mdl.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>style/jquery-ui/jquery-ui.css">
<!-- 		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>style/DataTables/datatables.min.css"/> -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>style/css/estilo.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>style/mdl/material.min.css">

		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>style/css/mdl-modal.css">

		<!--JS-->
		<script type="text/javascript" src="<?php echo base_url(); ?>style/js/jquery.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>style/js/tinymce/tinymce.min.js"></script> 
		<script type="text/javascript" src="<?php echo base_url(); ?>style/js/jquery.maskMoney.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>style/jquery-ui/jquery-ui.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>style/js/maskedinput.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>style/js/script.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>style/mdl/material.min.js"></script>

		<script type="text/javascript" src="<?php echo base_url(); ?>style/js/canvasjs.min.js"></script>

		<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css">

		<script type="text/javascript" src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>

		<!-- DataTables -->
		<!-- <script type="text/javascript" src="<?php echo base_url(); ?>style/DataTables/DataTables/js/jquery.dataTables.js"></script>	 -->

		<script type="text/javascript" src="<?php echo base_url(); ?>style/js/datatable.js"></script>	 

		<!-- Select 2 -->
		<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
		<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
		
	</head>

	<body>

	<noscript>
		<div style="background-color: red; height: 100%; width: 100%; position: absolute; z-index: 200000;" align="center">
		HABILITE O JAVASCRIPT PARA USAR O SISTEMA!.
			<iframe src="http://enable-javascript.com/pt/" width="100%" height="100%"></iframe>

		</div>
	</noscript> 

		<div class="mdl-layout mdl-js-layout mdl-layout--fixed-header ">

			<header class="mdl-layout__header" > <!--Menu Superior-->
				
				<div class="mdl-layout__header-row" >
					
					<a href="<?php echo base_url() ?>"><span class="mdl-layout-title"><img height="35px" src="<?php echo base_url(); ?>/stylebootstrap/img/claro-gn.png"></span></a>
					<div class="mdl-layout-spacer"></div> <!--Espaçamento do título para os links-->
					<nav class="mdl-navigation mdl-cell--hide-phone mdl-cell--hide-tablet" style="margin-right: 70px;">

						<?php //echo anchor('main/redirecionar/listar-view_listar_pdvs', 'PDVS', array('class' => 'mdl-navigation__link')); ?>

						<a href="#" class="menu-superior" id="menu-comunicados"> <img src="<?php echo base_url(); ?>style/imagens/comunicação.png"> Comunicação</a>
						<ul class="mdl-menu mdl-menu--bottom-left mdl-js-menu mdl-js-ripple-effect" for="menu-comunicados">
						  <li class="mdl-menu__item"><?php echo anchor('main/redirecionar/listar-view_listar_destaques', 'Destaques', array('class' => 'mdl-navigation__link', 'style' => 'color: black; margin-top: -7px;')) ; ?></li>

						  <li class="mdl-menu__item"><?php echo anchor('main/redirecionar/listar-view_listar_comunicados', 'Comunicados', array('class' => 'mdl-navigation__link', 'style' => 'color: black; margin-top: -7px;')) ; ?></li>

						  <!-- <li class="mdl-menu__item"><?php //echo anchor('main/redirecionar/listar-view_listar_relatorio', 'Relatório de Desempenho', array('class' => 'mdl-navigation__link', 'style' => 'color: black; margin-top: -7px;')) ; ?></li> -->

						  <li class="mdl-menu__item"><?php echo anchor('main/redirecionar/listar-view_listar_concorrencia', 'Consulta a Concorrência', array('class' => 'mdl-navigation__link', 'style' => 'color: black; margin-top: -7px;')) ; ?></li>

						  <li class="mdl-menu__item"><?php echo anchor('main/redirecionar/listar-view_listar_atento', 'Fique Atento', array('class' => 'mdl-navigation__link', 'style' => 'color: black; margin-top: -7px;')) ; ?></li>

						  <li class="mdl-menu__item"><?php echo anchor('main/redirecionar/listar-view_listar_faq', 'FAQ', array('class' => 'mdl-navigation__link', 'style' => 'color: black; margin-top: -7px;')) ; ?></li>
						</ul>

						<a href="#" class="menu-superior mdl-badge" id="menu-relatorios" <?php  
							if($aviso['badge']['revisar'] > 0){
								echo 'data-badge="'.$aviso['badge']['revisar'].'" title="'.$aviso['badge']['revisar'].' PDV(s) não localizado(s) hoje!"';
							} ?> style="margin-right: 2px;" ><img src="<?php echo base_url(); ?>style/imagens/relatorios.png"> Relatórios</a>
							<ul class="mdl-menu mdl-menu--bottom-left mdl-js-menu mdl-js-ripple-effect" for="menu-relatorios">
							<li class="mdl-menu__item"><?php echo anchor('main/redirecionar/relatorios-view_listar_relatorios', 'Relatórios', array('class' => 'mdl-navigation__link', 'style' => 'color: black; margin-top: -7px;')); ?></li>

							  <li class="mdl-menu__item"><?php echo anchor('main/redirecionar/relatorios-view_geral_inputvendas', 'Input de Venda', array('class' => 'mdl-navigation__link', 'style' => 'color: black; margin-top: -7px;')); ?></li>
							  <li class="mdl-menu__item"><?php echo anchor('main/redirecionar/relatorios-view_relatorio_checkout', 'Check-out', array('class' => 'mdl-navigation__link', 'style' => 'color: black; margin-top: -7px;')) ; ?></li>
							  <li class="mdl-menu__item"><?php echo anchor('controller_checkout/exportar_pdvs_nlocalizados', 'PDVs Não encontrados', array('class' => 'mdl-navigation__link', 'style' => 'color: black; margin-top: -7px;')) ; ?></li>
							</ul>

						<?php echo anchor('main/redirecionar/listar-view_listar_pdv', '<img src="'.base_url().'style/imagens/pdv.png"> PDVs', array('class' => 'mdl-navigation__link')); ?>

						<?php echo anchor('main/redirecionar/listar-view_listar_carteira', '<img src="'.base_url().'style/imagens/carteira.png"> Carteira', array('class' => 'mdl-navigation__link')); ?>


						<a href="#" class="menu-superior mdl-badge" id="menu-seguranca"  <?php  
							if($aviso['badge']['roteiro'] > 0){
								echo 'data-badge="'.$aviso['badge']['roteiro'].'" title="'.$aviso['badge']['roteiro'].' Roteiro(s) aguardando aprovação!"';
							} ?> ><img src="<?php echo base_url(); ?>style/imagens/ajustes.png"> Gerencial</a>
							<ul class="mdl-menu mdl-menu--bottom-left mdl-js-menu mdl-js-ripple-effect" for="menu-seguranca">
							  <li class="mdl-menu__item"><?php echo anchor('main/redirecionar/seguranca-view_proprios_usuarios', 'Atualizar Conta', array('class' => 'mdl-navigation__link', 'style' => 'color: black; margin-top: -7px;')); ?></li>
							  <li class="mdl-menu__item"><?php echo anchor('main/redirecionar/seguranca-view_listar_grupos', 'Grupos', array('class' => 'mdl-navigation__link', 'style' => 'color: black; margin-top: -7px;')) ; ?></li>
							  <li class="mdl-menu__item"><?php echo anchor('main/redirecionar/seguranca-view_listar_usuarios', 'Usuários', array('class' => 'mdl-navigation__link', 'style' => 'color: black; margin-top: -7px;')); ?></li>

							  <li class="mdl-menu__item"><?php echo anchor('main/redirecionar/editar-view_metas_editar_usuarios', 'Metas', array('class' => 'mdl-navigation__link', 'style' => 'color: black; margin-top: -7px;')); ?></li>

							  <li class="mdl-menu__item"><?php echo anchor('main/redirecionar/listar-view_lista_roteiro', 'Roteiro', array('class' => 'mdl-navigation__link', 'style' => 'color: black; margin-top: -7px;')) ; ?></li>
							</ul>

						<?php echo anchor('main/Sair', '<img src="'.base_url().'style/imagens/Sair.png">', array('class' => 'mdl-navigation__link')); ?>

					</nav>

				</div>

			</header> <!--Menu Superior Fim-->

			<div class="mdl-layout__content">

			<?php if($aviso['tipo'] != ''){ /*Confirma a existencia de aviso para esta tela.*/ 

			switch ($aviso['tipo']) { /*Define o icone de acordo com o tipo do erro*/
				case 'erro':
					$icon = 'error';
					break;

				case 'aviso':
					$icon = 'warning';
					break;
							
				default:
					$icon = 'check';
					break;
			}

			echo '<div class="'.$aviso['tipo'].'-card-wide mdl-card mdl-shadow--3dp card_aviso">
				  <div class="mdl-card__title">
				    <h2 class="mdl-card__title-text"><i class="material-icons">'.$icon.'</i>'.$aviso['titulo'].'</h2>
				  </div>
				  <div class="mdl-card__supporting-text">
				    '.$aviso['mensagem'].'
				  </div>
				  <div class="mdl-card__menu">
				    <button class="mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect" id="fechar_aviso">
				      <i class="material-icons">clear</i>
				    </button>
				  </div>
				</div>';

			} ?>

				<main>

                    <!-- removi id="corpo" da div abaixo -->
					<div  align="center">
						<div id="aviso_de_erro"></div>

