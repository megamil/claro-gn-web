<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Claro GN | FAQ iOS</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.4 -->
    <link href="<?php echo base_url() ?>stylebootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="<?php echo base_url() ?>stylebootstrap/fonts/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="<?php echo base_url() ?>stylebootstrap/css/bootsz.css" rel="stylesheet" type="text/css" />
    <!-- iCheck -->
    <link href="<?php echo base_url() ?>stylebootstrap/css/blue.css" rel="stylesheet" type="text/css" />

    <!--ICON-->
		<link rel="shortcut icon" href="<?php echo base_url(); ?>stylebootstrap/img/favicon.jpg" type="image/x-icon">
	<body>

	<noscript>
		<div style="background-color: red; height: 100%; width: 100%; position: absolute; z-index: 200000;" align="center">
		HABILITE O JAVASCRIPT PARA USAR O SISTEMA!.
			<iframe src="http://enable-javascript.com/pt/" width="100%" height="100%"></iframe>

		</div>
	</noscript> 


	 <div class="row" align="center">
	 <?php 
	 if (count($pendentes) == 0) {
	 	echo '<h5>NENHUM ROTEIRO PARA SER AVALIADO</h5>';
	 } else {
	 	echo '<h3>ROTEIROS EM ABERTO</h3>';
	 } ?>
    
    <hr>
  </div>

  <div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-10">
      <table class="table table-hover">

        <thead>
          <th>DETALHES</th>
          <th>GN</th>
          <th>SOLICITADO EM:</th>
        </thead>

        <tbody>
          <?php 

            foreach ($pendentes as $pendente) {
              echo '<tr>';
              echo '<td><a class="btn btn-info" href="'.base_url().'controller_webservice/aprovarRoteiro_view?id_roteiro='.$pendente->id_roteiro.'">DETALHES</a></td>';
              echo '<td>'.strtoupper($pendente->nome).'</td>';
              echo '<td>'.$pendente->solicitacao.'</td>';
              echo '</tr>';
            }
          ?>
        </tbody>
      </table>
    </div>
  </div>