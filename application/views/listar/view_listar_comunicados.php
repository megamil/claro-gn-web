<div align="center">
	<?php echo anchor('main/redirecionar/cadastro-view_novo_comunicados', '<i class="material-icons">assignment</i>Novo Comunicado', array('class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent', 'title' => 'Adicionar Comunicado.', 'alt' => 'Novo Comunicado.')); ?>
</div>


<hr>

  <script>

  $(document).on('click',".excluirConteudo",function(){

    var id = $(this).attr("id");
    var img = $(this).attr("img");

    $("#dialog-confirm").show();

      $(function() {
        $( "#dialog-confirm" ).dialog({
          resizable: false,
          height:140,
          modal: true,
          buttons: {
            "Deletar": function() {
              window.location.href = "<?php echo base_url() ?>/controller_comunicados/deletar/"+id+"/"+img;
            },
            Cancelar: function() {
              $( this ).dialog( "close" );
            }
          }
        });
      });
      $("#dialog-confirm").hide();


      $('table').DataTable();

    });


  </script>

 
<div id="dialog-confirm" title="Deseja mesmo deletar este item?" hidden></div>

<table class="mdl-data-table mdl-js-data-table mdl-shadow--8dp">
  <thead>
    <tr>
      <th>Editar</th>
      <th>Criado em:</th>
      <th>Título</th>
      <th>Rede</th>
      <th>Válido até:</th>
    </tr>
  </thead>

  <tbody>
    <?php foreach ($dados as $comunicado) {
      echo '<tr>';
      echo '<td>'.anchor('main/redirecionar/editar-view_editar_comunicados/'.$comunicado->id_comunicado, '<i class="material-icons">mode_edit</i>Editar', array('class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent', 'title' => 'Editar.', 'alt' => 'Editar.', 'style' => 'margin-top: -7px;')).'&nbsp;&nbsp;<button class="mdl-button mdl-js-button excluirConteudo" id="'.$comunicado->id_comunicado.'" img="'.$comunicado->url_imagem.'" style="margin-top: -7px; background-color: #d52b1e; color: white;"> <i class="material-icons">delete</i>EXCLUIR</button></td>';

      echo '<td>'.$comunicado->upload.'</td>';
    echo '<td>'.$comunicado->titulo.'</td>';
    echo '<td>'.$comunicado->rede.'</td>';

    if ($comunicado->valido == 1) {
      if (is_null($comunicado->limite)) {
        echo '<td style="background-color: green; color: white;">Valido sem data limite!</td>';
      } else {
        echo '<td style="background-color: green; color: white;">Valido Até: '.$comunicado->limite.'</td>';
      }
      
    } else {
      echo '<td style="background-color: red; color: white;">Expirou em: '.$comunicado->limite.'</td>';
    }

    echo '</tr>';
  } ?>
  </tbody>
</table>