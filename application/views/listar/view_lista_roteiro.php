<h2>ROTEIROS</h2>

<hr>

<div align="center">

	<table class="mdl-data-table mdl-js-data-table mdl-shadow--8dp">
	  <thead>
	    <tr>
	      <th>Detalhes</th>
	      <th>GN</th>
	      <th>Início</th>
	      <th>Fim</th>
	      <th>Data da Solicitação</th>
	      <th>Status</th>
	      <th>KM Total</th>
	      <th>Total em Horas</th>
	    </tr>
	  </thead>
	  <tbody>
	    <?php foreach ($dados as $roteiro) {
	    	echo '<tr>';
	    	echo '<td>'.anchor('main/redirecionar/editar-view_aprovacao_roteiro/'.$roteiro->id, '<i class="material-icons">mode_edit</i>Detalhes', array('class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent', 'title' => 'Detalhes.', 'alt' => 'Detalhes.', 'style' => 'margin-top: -7px;')).'</td>';
			echo '<td>'.$roteiro->nome.'</td>';
			echo '<td>'.$roteiro->inicio.'</td>';
			echo '<td>'.$roteiro->fim.'</td>';
			echo '<td>'.$roteiro->solicitacao.'</td>';
			echo '<td>'.$roteiro->status.'</td>';
			echo '<td>'.$roteiro->km.'</td>';
			echo '<td></td>';
			echo '</tr>';
		} ?>
	  </tbody>
	</table>
</div>


<div class="mdl-grid" style="margin-left: -30px;">

  <div class="mdl-cell mdl-cell--1-col">
    <a id="excel_geral" target="_blank" style="cursor: pointer;">
      <img src="<?php echo base_url() ?>style/imagens/excel.jpg">
    </a>
  </div>

</div>

<script type="text/javascript">
	$(document).ready(function(){

		$('#excel_geral').click(function(){

	      var url = "<?php echo base_url(); ?>controller_excel/excel_roteiro";

	      $.ajax({
	          url: url,
	          type: "post",
	        datatype: 'json',
	          success: function(data){
	        window.location = url;

	        },
	          error:function(){
	              
	          }   
	        });

	    });


	});
</script>

