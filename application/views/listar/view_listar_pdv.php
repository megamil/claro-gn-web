<script type="text/javascript" src="<?php echo base_url(); ?>style/js/datatable.js"></script>
<?php echo form_open('controller_pdv/excel_export'); ?>
<div class="mdl-grid">

	<div class="mdl-cell mdl-cell--3-col">

		<label for="fk_usuario">Filtrar Por GN</label>
		<select name="usuario" id="usuario" style="width: 100%;">
			<option value="">Todos</option>
			<?php foreach ($dados['usuarios'] as $usuario) {

		    	echo '<option value="'.$usuario->id_usuario.'">'.$usuario->usuario.'</option>';

			} ?>
		</select>

	</div>

	<div class="mdl-cell mdl-cell--2-col">

		<label for="fk_rede">Filtrar Por Rede</label>
		<select name="rede" id="rede" style="width: 100%;">
			<option value="">Todas</option>
			<?php foreach ($dados['redes'] as $rede) {

		    	echo '<option value="'.$rede->rede.'">'.$rede->rede.'</option>';

			} ?>
		</select>
		    
	</div>

	<div class="mdl-cell mdl-cell--2-col">

		<label for="filial">Filtrar Por Filial</label>
		<select name="filial" id="filial" style="width: 100%;">
			<option value="">Todas</option>
			<option value="1">Filial 1</option>
			<option value="2">Filial 2</option>
		</select>
		    
	</div>

	<div class="mdl-cell mdl-cell--3-col">
		    <label class="label" for="cnpj">CNPJ</label>
		    <input type="text" class="mdl-textfield__input mascara_cnpj"  name="cnpj" id="cnpj"/>
	</div>		    
	

	<div class="mdl-cell mdl-cell--2-col">
	<a type="button" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" id="filtrar">Aplicar Filtro</a>
	</div>

</div>


<div class="mdl-grid">
	<div class="mdl-cell mdl-cell--3-col"></div>
	<div class="mdl-cell mdl-cell--3-col">
		<button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent">Exportar para Excel</button>
	</div>

	<div class="mdl-cell mdl-cell--3-col">
	<?php echo anchor('main/redirecionar/cadastro-view_novo_pdv', '<i class="material-icons">assignment</i>Novo PDV', array('class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent', 'title' => 'Adicionar novo PDV.', 'alt' => 'Novo PDV.')); ?>
	</div>
</div>

<? echo form_close(); ?>

<hr>
	<div class="mdl-spinner mdl-js-spinner" id="loadSpinner"></div>
	<div id="load" align="center">

	<table class="mdl-data-table mdl-js-data-table mdl-shadow--8dp">
	  <thead>
	    <tr>
	      <th></th>
	      <th>Rede</th>
	      <th>CNPJ</th>
	      <th>Lougradouro</th>
	      <th>Filial</th>
	      <th>GN</th>
	    </tr>
	  </thead>
	  <tbody>
	    <?php foreach ($dados['pdvs'] as $pdv) {
	    	echo '<tr>';
	    	echo '<td>'.anchor('main/redirecionar/editar-view_editar_pdv/'.$pdv->id_pdv, '<i class="material-icons">mode_edit</i>Editar', array('class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent', 'title' => 'Editar PDV.', 'alt' => 'Editar PDV.', 'style' => 'margin-top: -7px;')).'</td>';
			echo '<td>'.$pdv->rede.'</td>';
			echo '<td>'.$pdv->cnpj.'</td>';
			echo '<td>'.$pdv->lougradouro.'</td>';

			if ($pdv->filial == 1) {
				echo '<td>Filial 1</td>';
			} else {
				echo '<td>Filial 2</td>';
			}

			echo '<td>'.$pdv->nome.'</td>';

			echo '</tr>';
		} ?>
	  </tbody>
	</table>
</div>

<script type="text/javascript">
	$(document).ready(function(){

		$("#usuario").select2();
		$("#rede").select2();
		$("#filial").select2();

		$('#filtrar').click(function(){

			$('#loadSpinner').addClass('is-active');

			var usuario = $('#usuario').val();
			var rede = $('#rede').val();
			var filial = $('#filial').val();
			var cnpj = $('#cnpj').val();

			$('#load').load(<?php echo "'".base_url()."controller_pdv/filtroAjax'"; ?>, {
				"usuario" : usuario, 
				"rede" : rede, 
				"filial" : filial,
				"cnpj" : cnpj}, function(){

				$('#loadSpinner').removeClass('is-active');

			});


		});

	});
</script>