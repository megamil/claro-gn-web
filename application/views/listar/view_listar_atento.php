<div align="center">
	<?php echo anchor('main/redirecionar/cadastro-view_novo_atento', '<i class="material-icons">assignment</i>Novo Fique Atento', array('class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent', 'title' => 'Adicionar Fique Atento.', 'alt' => 'Novo Atento.')); ?>
</div>

<hr>

  <script>

  $(document).on('click',".excluirConteudo",function(){

    var id = $(this).attr("id");
    var img = $(this).attr("img");

    $("#dialog-confirm").show();

      $(function() {
        $( "#dialog-confirm" ).dialog({
          resizable: false,
          height:140,
          modal: true,
          buttons: {
            "Deletar": function() {
              window.location.href = "<?php echo base_url() ?>/controller_atento/deletar/"+id+"/"+img;
            },
            Cancelar: function() {
              $( this ).dialog( "close" );
            }
          }
        });
      });
      $("#dialog-confirm").hide();
    });


  </script>

 
<div id="dialog-confirm" title="Deseja mesmo deletar este item?" hidden></div>

<table class="mdl-data-table mdl-js-data-table mdl-shadow--8dp">
  <thead>
    <tr>
      <th>Editar</th>
      <th>Criado em:</th>
      <th>Título</th>
      <th>Válido até:</th>
    </tr>
  </thead>

  <tbody>
    <?php foreach ($dados as $atento) {
      echo '<tr>';
      echo '<td>'.anchor('main/redirecionar/editar-view_editar_atento/'.$atento->id_atento, '<i class="material-icons">mode_edit</i>Editar', array('class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent', 'title' => 'Editar.', 'alt' => 'Editar.', 'style' => 'margin-top: -7px;')).'&nbsp;&nbsp;<button class="mdl-button mdl-js-button excluirConteudo" id="'.$atento->id_atento.'" img="'.$atento->url_imagem.'" style="margin-top: -7px; background-color: #d52b1e; color: white;"> <i class="material-icons">delete</i>EXCLUIR</button></td>';

      echo '<td>'.$atento->upload.'</td>';
    echo '<td>'.$atento->titulo.'</td>';

    if ($atento->valido == 1) {
      if (is_null($atento->limite)) {
        echo '<td style="background-color: green; color: white;">Valido sem data limite!</td>';
      } else {
        echo '<td style="background-color: green; color: white;">Valido Até: '.$atento->limite.'</td>';
      }
      
    } else {
      echo '<td style="background-color: red; color: white;">Expirou em: '.$atento->limite.'</td>';
    }

    echo '</tr>';
  } ?>
  </tbody>
</table>