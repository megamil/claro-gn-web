<div align="center">
	<?php echo anchor('main/redirecionar/cadastro-view_nova_concorrencia', '<i class="material-icons">assignment</i>Nova Consulta', array('class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent', 'title' => 'Adicionar Consulta.', 'alt' => 'Nova consulta.')); ?>
</div>

<hr>

  <script>

$(document).on('click',".excluirConteudo",function(){

    var id = $(this).attr("id");
    var img = $(this).attr("img");

    $("#dialog-confirm").show();

      $(function() {
        $( "#dialog-confirm" ).dialog({
          resizable: false,
          height:140,
          modal: true,
          buttons: {
            "Deletar": function() {
              window.location.href = "<?php echo base_url() ?>/controller_concorrencia/deletar/"+id+"/"+img;
            },
            Cancelar: function() {
              $( this ).dialog( "close" );
            }
          }
        });
      });
      $("#dialog-confirm").hide();
    });

  </script>

 
<div id="dialog-confirm" title="Deseja mesmo deletar este item?" hidden></div>

<table class="mdl-data-table mdl-js-data-table mdl-shadow--8dp">
  <thead>
    <tr>
      <th>Editar</th>
      <th>Criado em:</th>
      <th>Título</th>
      <th>Operadora</th>
      <th>Válido até:</th>
    </tr>
  </thead>

  <tbody>
    <?php foreach ($dados as $concorrencia) {
      echo '<tr>';
      echo '<td>'.anchor('main/redirecionar/editar-view_editar_concorrencia/'.$concorrencia->id_concorrencia, '<i class="material-icons">mode_edit</i>Editar', array('class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent', 'title' => 'Editar.', 'alt' => 'Editar.', 'style' => 'margin-top: -7px;')).'&nbsp;&nbsp;<button class="mdl-button mdl-js-button excluirConteudo" id="'.$concorrencia->id_concorrencia.'" img="'.$concorrencia->url_imagem.'" style="margin-top: -7px; background-color: #d52b1e; color: white;"> <i class="material-icons">delete</i>EXCLUIR</button></td>';

      echo '<td>'.$concorrencia->upload.'</td>';
      echo '<td>'.$concorrencia->titulo.'</td>';

      switch ($concorrencia->operadora) {
      case 2:
        echo '<td>Oi</td>';
        break;
      case 3:
        echo '<td>Tim</td>';
        break;
      
      default:
        echo '<td>Vivo</td>';
        break;
    }


    if ($concorrencia->valido == 1) {
      if (is_null($concorrencia->limite)) {
        echo '<td style="background-color: green; color: white;">Valido sem data limite!</td>';
      } else {
        echo '<td style="background-color: green; color: white;">Valido Até: '.$concorrencia->limite.'</td>';
      }
      
    } else {
      echo '<td style="background-color: red; color: white;">Expirou em: '.$concorrencia->limite.'</td>';
    }

    echo '</tr>';
  } ?>
  </tbody>
</table>