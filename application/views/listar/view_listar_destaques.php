<div align="center">
	<?php echo anchor('main/redirecionar/cadastro-view_novo_destaques', '<i class="material-icons">assignment</i>Novo Destaque', array('class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent', 'title' => 'Adicionar Destaque.', 'alt' => 'Novo Destaque.')); ?>
</div>

<hr>

  <script>

$(document).on('click',".excluirConteudo",function(){

    var id = $(this).attr("id");
    var img = $(this).attr("img");

    $("#dialog-confirm").show();

      $(function() {
        $( "#dialog-confirm" ).dialog({
          resizable: false,
          height:140,
          modal: true,
          buttons: {
            "Deletar": function() {
              window.location.href = "<?php echo base_url() ?>/controller_destaques/deletar/"+id+"/"+img;
            },
            Cancelar: function() {
              $( this ).dialog( "close" );
            }
          }
        });
      });
      $("#dialog-confirm").hide();
    });

  </script>

 
<div id="dialog-confirm" title="Deseja mesmo deletar este item?" hidden></div>

<table class="mdl-data-table mdl-js-data-table mdl-shadow--8dp">
  <thead>
    <tr>
      <th>Editar</th>
      <th>Criado em:</th>
      <th>Título</th>
      <th>Válido até:</th>
    </tr>
  </thead>

  <tbody>
    <?php foreach ($dados as $destaque) {
    	echo '<tr>';
      echo '<td>'.anchor('main/redirecionar/editar-view_editar_destaques/'.$destaque->id_destaque, '<i class="material-icons">mode_edit</i>Editar', array('class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent', 'title' => 'Editar.', 'alt' => 'Editar.', 'style' => 'margin-top: -7px;')).'&nbsp;&nbsp;<button class="mdl-button mdl-js-button excluirConteudo" id="'.$destaque->id_destaque.'" img="'.$destaque->url_imagem.'" style="margin-top: -7px; background-color: #d52b1e; color: white;"> <i class="material-icons">delete</i>EXCLUIR</button></td>';

    	echo '<td>'.$destaque->upload.'</td>';
		echo '<td>'.$destaque->titulo.'</td>';

    if ($destaque->valido == 1) {
      if (is_null($destaque->limite)) {
        echo '<td style="background-color: green; color: white;">Valido sem data limite!</td>';
      } else {
        echo '<td style="background-color: green; color: white;">Valido Até: '.$destaque->limite.'</td>';
      }
      
    } else {
      echo '<td style="background-color: red; color: white;">Expirou em: '.$destaque->limite.'</td>';
    }

		echo '</tr>';
	} ?>
  </tbody>
</table>