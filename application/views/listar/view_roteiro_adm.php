<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Claro GN | FAQ iOS</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.4 -->
    <link href="<?php echo base_url() ?>stylebootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="<?php echo base_url() ?>stylebootstrap/fonts/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="<?php echo base_url() ?>stylebootstrap/css/bootsz.css" rel="stylesheet" type="text/css" />
    <!-- iCheck -->
    <link href="<?php echo base_url() ?>stylebootstrap/css/blue.css" rel="stylesheet" type="text/css" />

    <!--ICON-->
		<link rel="shortcut icon" href="<?php echo base_url(); ?>stylebootstrap/img/favicon.jpg" type="image/x-icon">
	<body>

	<noscript>
		<div style="background-color: red; height: 100%; width: 100%; position: absolute; z-index: 200000;" align="center">
		HABILITE O JAVASCRIPT PARA USAR O SISTEMA!.
			<iframe src="http://enable-javascript.com/pt/" width="100%" height="100%"></iframe>

		</div>
	</noscript> 

  <div class="row" align="center">
    <div class="col-md-3">
      <a href="<?php echo base_url(); ?>controller_webservice/webview_roteiro?id_usuario=<?php echo $id; ?>&novo=novo" class="btn btn-success" style="width: 100%;">CRIAR ROTEIRO</a> 
    </div>

    <div class="col-md-3">
      <a href="<?php echo base_url(); ?>controller_webservice/roteiro_pendentes?id_usuario=<?php echo $id; ?>" class="btn btn-info" style="width: 100%;">VISUALIZAR ROTEIROS PENDENTES</a>  
    </div>
  </div>

  <div class="row" align="center">
    <h3>ACOMPANHAMENTO GNS</h3>
    <hr>
  </div>

  <div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-10">
      <table class="table table-hover">

        <thead>
          <th colspan="11">GN</th>
          <th colspan="1">CHECKOUTS (HOJE)</th>
        </thead>

        <tbody>
          <?php 

            foreach ($gns as $gn) {
              echo '<tr>';
              echo '<td colspan="11">'.strtoupper($gn->usuario).'</td>';
              if ($gn->checkouts == 0) {
                echo '<td class="alert alert-danger" colspan="1">NENHUM CHECKOUT HOJE</td>';
              } else {
                echo '<td class="alert alert-success" colspan="1"><a href="'.base_url().'controller_webservice/detalhesCheckouts?id_usuario='.$id.'&fk_usuario='.$gn->id_usuario.'&nome='.strtoupper($gn->usuario).'">'.$gn->checkouts.' CHECKOUT(S)</a></td>';
              }
              echo '</tr>';
            }
          ?>
        </tbody>
      </table>
    </div>
  </div>


  </body>
</html>