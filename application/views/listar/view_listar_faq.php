<div align="center">
	<?php echo anchor('main/redirecionar/cadastro-view_novo_faq', '<i class="material-icons">assignment</i>Criar FAQ', array('class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent', 'title' => 'Adicionar FAQ.', 'alt' => 'Novo FAQ.')); ?>
</div>
<hr>
<div align="center">

	<table class="mdl-data-table mdl-js-data-table mdl-shadow--8dp">
	  <thead>
	    <tr>
	      <th>ID</th>
	      <th>Título</th>
	      <th>Data</th>
	      <th>Plataforma</th>
	      <th>Editar</th>
	    </tr>
	  </thead>
	  <tbody>
	    <?php foreach ($dados as $faq) {
	    	echo '<tr>';
			echo '<td>'.$faq->id_faq.'</td>';
			echo '<td>'.$faq->titulo_faq.'</td>';
			echo '<td>'.$faq->data_faq.'</td>';
			if($faq->tipo_faq == 1){
				echo '<td>Android</td>';
			} else {
				echo '<td>iOS</td>';
			}
			echo '<td>'.anchor('main/redirecionar/editar-view_editar_faq/'.$faq->id_faq, '<i class="material-icons">mode_edit</i>Editar', array('class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent', 'title' => 'Editar FAQ.', 'alt' => 'Editar FAQ', 'style' => 'margin-top: -7px;')).'</td>';
			echo '</tr>';
		} ?>
	  </tbody>
	</table>
</div>