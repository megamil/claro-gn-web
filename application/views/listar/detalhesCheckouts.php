<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Claro GN | Detalhes Checkout</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.4 -->
    <link href="<?php echo base_url() ?>stylebootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="<?php echo base_url() ?>stylebootstrap/fonts/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="<?php echo base_url() ?>stylebootstrap/css/bootsz.css" rel="stylesheet" type="text/css" />
    <!-- iCheck -->
    <link href="<?php echo base_url() ?>stylebootstrap/css/blue.css" rel="stylesheet" type="text/css" />

    <!--ICON-->
    <link rel="shortcut icon" href="<?php echo base_url(); ?>stylebootstrap/img/favicon.jpg" type="image/x-icon">
  <body>

  <noscript>
    <div style="background-color: red; height: 100%; width: 100%; position: absolute; z-index: 200000;" align="center">
    HABILITE O JAVASCRIPT PARA USAR O SISTEMA!.
      <iframe src="http://enable-javascript.com/pt/" width="100%" height="100%"></iframe>

    </div>
  </noscript> 

  <div class="row">
    <div class="col-md-6" align="center"><h3>Analítico CheckOut - GN <?php echo $nome; ?></h3></div>
  </div>

  <div class="row">
    <?php 

      foreach ($detalhes as $detalhe) {
          
        echo '<div class="col-md-12">';

        echo '<strong>Rede:</strong> '.$detalhe->rede.' ('.$detalhe->curva.')<br>';
        echo '<strong>Endereço:</strong> '.$detalhe->endereco_exato.'<br>';
        echo '<strong>Checkout:</strong> '.$detalhe->data_checkout.'<br><hr>';

        echo '</div>';  

      }

     ?>
    
      
    </div>

  <div class="row">
      <div class="col-md-1"></div><div class="col-md-10"><a class="btn btn-info" href="<?php echo base_url() ?>controller_webservice/webview_roteiro?id_usuario=<?php echo $id_usuario; ?>" style="width: 100%;">Voltar</a></div>
    </div>

  </body>
</html>