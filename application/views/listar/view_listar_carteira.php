<div class="mdl-grid" align="center">
	
	 <div class="mdl-cell mdl-cell--1-col"></div> 

	 <div class="mdl-cell mdl-cell--3-col"> Se preferir, digite o CNPJ para descobrir o GN responsável: </div> 

	 <div class="mdl-cell mdl-cell--3-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
	 
    	<input type="text" class="mdl-textfield__input mascara_cnpj" aviso="CNPJ" name="CNPJ" id="CNPJ" value="" size="50" maxlength="15" placeholder="CNPJ"/>
    </div>	

     <div class="mdl-cell mdl-cell--3-col">
		<button class="-col mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" id="enviar"><i class="material-icons">done</i>DESCOBRIR</button>	
	 </div>


</div>

<hr>
<div id="p2" class="mdl-progress mdl-js-progress mdl-progress__indeterminate"></div>
<div align="center" id="load">

	<table class="mdl-data-table mdl-js-data-table mdl-shadow--8dp">
	  <thead>
	    <tr>
	      <th>Usuários</th>
	      <th>Na carteira</th>
	      <th>Filial do GN</th>
	      <th>PDVs</th>
	    </tr>
	  </thead>
	  <tbody>
	    <?php foreach ($dados as $usuario) {
	    	echo '<tr>';
			echo '<td>'.$usuario->nome.'</td>';
			echo '<td>'.$usuario->total.'</td>';
			echo '<td>'.$usuario->filial_responsavel.'</td>';
			echo '<td>'.anchor('main/redirecionar/editar-view_crud_carteira/'.$usuario->id_usuario, '<i class="material-icons">mode_edit</i>PDVs', array('class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent', 'title' => 'PDVs Associados.', 'alt' => 'PDVs Associados..', 'style' => 'margin-top: -7px;')).'</td>';
			echo '</tr>';
		} ?>
	  </tbody>
	</table>
</div>

<script type="text/javascript">
	
$(document).ready(function(){

	var barra_progresso = $("#p2");
	var cnpj = $("#CNPJ");
	barra_progresso.hide();

	$("#enviar").click(function(){

		barra_progresso.show();
		cnpj.mask("99999999999999");

		if(cnpj.val() != '') {
			
			$("#load").load('<?php echo base_url(); ?>/Controller_carteira/buscar',{'cnpj': cnpj.val()},function(){
				barra_progresso.hide();
			});

			cnpj.mask("99.999.999/9999-99");
		} else {
			$("#erro").remove();
			$("#aviso_de_erro").fadeIn("slow", function() { $(this).append('<div id="erro"><i class="material-icons">error</i>DIGITE O CNPJ</div>'); });
			cnpj.mask("99.999.999/9999-99");
		}



	});

});


</script>

