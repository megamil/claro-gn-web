<html><body><strong>O QUE FAZEMOS COM AS INFORMAÇÕES?</strong><br> 

1. O termo "informações pessoais" usado aqui, é definido como qualquer informação que identifica ou pode ser usada para identificar, contatar ou localizar a pessoa, a quem tal informação pertence. As informações pessoais que coletamos estará sujeita a esta Política de Privacidade, a qual é alterada de tempos em tempos.
<br>2. O Claro GN utiliza as informações que são coletadas para os seguintes propósitos gerais: fornecimento de produtos e serviços,  identificação e autenticação, melhoria de serviços, contato e pesquisa.
<br><br>
<strong>DIVULGAÇÃO</strong><br>
<br>1. Claro GN pode utilizar terceiros prestadores de serviços para fornecer determinados serviços para você e nós podemos compartilhar informações pessoais com esses prestadores de serviços. Exigimos que qualquer empresa com a qual podemos compartilhar Informações Pessoais, proteja os dados de uma forma consistente com essa política, e limite o uso de tais Informações Pessoais para o desempenho dos serviços do Claro GN.
<br>2. Claro GN pode divulgar Informações Pessoais em circunstâncias especiais, como para cumprir ordens judiciais obrigando-nos a fazê-lo ou quando suas ações violam os Termos de Serviço.
<br>3. Nós não vendemos ou fornecemos Informações Pessoais para outras empresas, para que elas façam o marketing dos seus próprios produtos ou serviços.
<br><br>
O QUE SIGNIFICA? <br>
Em certas circunstâncias, podemos divulgar suas informações pessoais, como no caso de ordens judiciais.
<br><br>
<strong>ARMAZENAMENTO DE DADOS DO USUÁRIO</strong><br>
Claro GN possui o armazenamento de dados, bancos de dados e todos os direitos da aplicação Claro GN, entretanto, não fazemos nenhuma reivindicação dos direitos dos seus dados. Você retém todos os direitos sobre os seus dados. 
<br><br>
O QUE SIGNIFICA?<br>
Você possui seus dados e nós vamos respeitar isso. Não vamos tentar competir com você ou escrever para os seus clientes.
<br><br>
<strong>COOKIES</strong><br>
Um cookie é uma pequena quantidade de dados, que podem incluir um identificador único anônimo. Os cookies são enviados para o seu navegador a partir de um site da web e armazenado no disco rígido do seu computador. Para cada computador que acessa o nosso site é atribuído um cookie diferente por nós.
<br><br>
O QUE SIGNIFICA?<br>
Para identificá-lo eletronicamente, um cookie será armazenado no seu computador. Temos uma ferramenta de "remarketing" executando que nos permite tomar nota das suas visitas ao nosso site e mostrar anúncios relevantes no nosso site e em toda a Internet. Você pode sempre optar por não ver.
<br><br>
<strong>ALTERAÇÕES A ESTA POLÍTICA DE PRIVACIDADE</strong><br>
Reservamos o direito de modificar esta declaração de privacidade a qualquer momento, portanto, revise-a frequentemente. Se fizermos alterações materiais a esta política, iremos notificá-lo aqui ou por meio de um aviso em nossa homepage, de modo que você está ciente das informações que coletamos, como as usamos, e sob quais circunstâncias, se houver, as divulgamos .
<br><br>
O QUE SIGNIFICA?<br>
Podemos alterar esta Política de Privacidade. Se for uma grande mudança, iremos informá-lo, aqui.
<br><br>
<strong>PERGUNTAS</strong><br>
<br>Quaisquer dúvidas sobre esta Política de Privacidade devem ser endereçadas a <a href="mailto:contato@clarogn.com.br">contato@clarogn.com.br</a> ou por correio para:
<br>R. Flórida, 1970 - Itaim Bibi, São Paulo - SP, CEP 04565 907</body></html>