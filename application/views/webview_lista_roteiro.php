<!DOCTYPE html>
<html>
<head>
	<title>Lista</title>
	<link href="<?php echo base_url() ?>stylebootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<script src="<?php echo base_url(); ?>stylebootstrap/js/jquery.js"></script>
	<script src="<?php echo base_url(); ?>stylebootstrap/js/bootstrap.min.js"></script>
</head>
<body>

	<div class="row">
		<a href="<?php echo base_url(); ?>Controller_webservice/novo_roteiro?id_usuario=<?php echo $id; ?>" class="btn btn-success col-md-12" style="width: 100%;">CRIAR NOVO ROTEIRO</a>
	</div>

	<div class="row">
		<div class="col-md-12" align="center">
			<?php 
			if(isset($lista)){
				echo '<h3>ROTEIRO APROVADO PARA HOJE '.strtoupper(date('d/m/y')).'</h3>';
			} else {
				echo '<h3>NENHUM ROTEIRO APROVADO PARA HOJE '.strtoupper(date('d/m/y')).'</h3>';
			} ?>
			
		</div>
	</div>

	<div class="container">
		<div class="row">
			<?php 

		if(isset($lista)){

			echo '<ol> ';	

			foreach ($lista as $key => $value) {
				echo '<li>'.$value->endereco_google.'</li><hr>';
			}

			echo '</ol>';

		}

	 ?>
		</div>
	</div>

</body>
</html>