<!DOCTYPE html>
<html>
<head>
	<title>Check-out</title>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<link rel="stylesheet" href="<?php echo base_url(); ?>stylebootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>stylebootstrap/css/style.css">
	<script src="<?php echo base_url(); ?>stylebootstrap/js/jquery.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>style/js/jquery.maskMoney.js"></script>
	<script src="<?php echo base_url(); ?>stylebootstrap/js/bootstrap.min.js"></script>

	<?php 

	if(empty($envio_direto)) { //}

	?>

	<script type="text/javascript">

	$(function(){
		 $(".moeda").maskMoney({symbol:'R$ ', 
		showSymbol:true, thousands:'.', decimal:',', symbolStay: true});
		 });

	$(document).ready(function(){

		if($('#pc_promotor').val() == ''){
			$("#promotor").hide();	
		} else {
			$("#e_promotor").attr('checked','Checked');	
		}
		
		$('#checkpoint').val('');

		$("#e_promotor").click(function(){
			console.log("Promotor clique.");
			$("#promotor").toggle();
		});

		$('#continuar').click(function(e){
			e.preventDefault();
			$('#checkpoint').val('1');
			$('form').submit();
			return true;
		});

		$('#envio_formulario').click(function(e){
			e.preventDefault();
			if($('#cnpj').val() != "") {
				$('form').submit();
				return true;
			} else {
				alert('Preencha o CNPJ');
				return false;
			}
		});

		if($("#id_checkout").val() > 0) {
			alert('CONTINUE O FORMULÁRIO SALVO.');
		}

		//Oculta as progress bar iniciais.
		$('#barra1, #barra2, #barra3, #barra4').hide();

		/*VALIDAÇÃO INICIAL SE VIER PREENCHIDO BUSCA E SELECIONA*/
		if($('#marcas1').val() > 0){
			buscaInicial($('#marcas1'));
		} 
		if($('#marcas2').val() > 0){
			buscaInicial($('#marcas2'));
		} 
		if($('#marcas3').val() > 0){
			buscaInicial($('#marcas3'));
		} 
		if($('#marcas4').val() > 0){
			buscaInicial($('#marcas4'));
		} 
		$('#marcas1, #marcas2, #marcas3, #marcas4').change(function(){
			buscaInicial($(this));
		});

		function buscaInicial(marca) {
			var indice = marca.attr('indice');

			$('#barra'+indice).show();
			$('#aparelho'+indice).hide();
			$('#load'+indice).load('ajax_Listar_modelos', {"fk_marca" : marca.val(), "indice" : indice, "modelo" : $("#modeloAjax"+indice).val()}, function(){ 
					//console.log('Terminou Ajax');
					$('#barra'+indice).hide();
					$('#aparelho'+indice).show();
			});
		}

		// $("#continuar").click(function(e){
		 // 	alert("Função em construção!");
		 // 	e.preventDefault();
			// return false;
		 // });

		 //Impede o submit ao apertar enter.
		 $('form').on('keyup keypress', function(e) {
			  var keyCode = e.keyCode || e.which;
			  if (keyCode === 13) { 
			  	console.log("Envio bloqueado.");
			    e.preventDefault();
			    return false;
			  }
			});
		

		<?php if(!empty($sucesso)) {
				echo "var sucesso = ".$sucesso.";
				";
			} else if(!empty($_GET['sucesso'])) {
				echo "var sucesso = ".$_GET['sucesso'].";
				";
			} else {
				echo "var sucesso = false;
				";
			}

			if($dadosEsteMes['registros'] > 0) {
				echo "var readonly_dadosEsteMes = 1;
				";
			} else {
				echo "var readonly_dadosEsteMes = 0;
				";
			}

			if($dadosEstaSemana['registros'] > 0) {
				echo "var readonly_dadosEstaSemana = 1;
				";
			} else {
				echo "var readonly_dadosEstaSemana = 0;
				";
			}



		?>

		 if(sucesso == 1) {
		 	alert("CHECK-OUT REALIZADO COM SUCESSO!");
		 } else if(sucesso == 2) {
		 	alert("CHECK-OUT SALVO COM SUCESSO, PODE CONTINUAR MAIS TARDE!");
		 }

		 if(readonly_dadosEsteMes == 1){

		 	$('#dadosEsteMes').show();
		 	$('#dadosEsteMesFaturamento').show();

		 	$('.readonly_dadosEsteMes').attr('readonly',true);
		 	$('.readonly_dadosEsteMesFaturamento').attr('readonly',true);

		 } else {

		 	$('#dadosEsteMes').hide();
		 	$('#dadosEsteMesFaturamento').hide();

		 	$('.readonly_dadosEsteMes').attr('readonly',false);
		 	$('.readonly_dadosEsteMesFaturamento').attr('readonly',false);

		 }

		 $('#dadosEsteMes').click(function(){
		 	$('.readonly_dadosEsteMes').attr('readonly',false);
		 });

		 $('#dadosEsteMesFaturamento').click(function(){
		 	$('.readonly_dadosEsteMesFaturamento').attr('readonly',false);
		 });

		 if(readonly_dadosEstaSemana == 1){

		 	$('#dadosEstaSemanaVendas').show();
		 	$('#dadosEstaSemanaPromotor').show();

		 	$('.readonly_dadosEstaSemanaVendas').attr('readonly',true);
		 	$('.readonly_dadosEstaSemanaPromotor').attr('readonly',true);

		 } else {

		 	$('#dadosEstaSemanaVendas').hide();
		 	$('#dadosEstaSemanaPromotor').hide();

		 	$('.readonly_dadosEstaSemanaVendas').attr('readonly',false);
		 	$('.readonly_dadosEstaSemanaPromotor').attr('readonly',false);

		 }

		 $('#dadosEstaSemanaVendas').click(function(){
		 	$('.readonly_dadosEstaSemanaVendas').attr('readonly',false);
		 });

		 $('#dadosEstaSemanaPromotor').click(function(){
		 	$('.readonly_dadosEstaSemanaPromotor').attr('readonly',false);
		 });

	 });
	
	</script>

</head>
<body aling="center">

<style>
	
	hr {
		margin-top: -5px;
		margin-bottom: -1px;
	}

	.margem {
		padding-top: 15px;
	}

</style>

<?php
	/*   CAMPOS QUE VEM DA URL   */
	if(!empty($_GET['endereco_exato'])) { // se veio pela URL
		$endereco_exato = $_GET['endereco_exato'];
	} else if (empty($endereco_exato)) { // se não veio pelo controller.
		$endereco_exato = "";
	}

	if(!empty($_GET['fk_usuario'])) { // se veio pela URL
		$fk_usuario = $_GET['fk_usuario'];
	} else if (empty($fk_usuario)) { // se não veio pelo controller.
		$fk_usuario = "";
	}

	if(!empty($_GET['fk_pdv'])) { // se veio pela URL
		$fk_pdv = $_GET['fk_pdv'];
	} else if (empty($fk_pdv)) { // se não veio pelo controller.
		$fk_pdv = "1";
	}

	if(!empty($_GET['nome_usuario'])) { // se veio pela URL
		$nome_usuario = $_GET['nome_usuario'];
	} else if (empty($nome_usuario)) { // se não veio pelo controller.
		$nome_usuario = "";
	}

	if(!empty($_GET['rede'])) { // se veio pela URL
		$rede = $_GET['rede'];
	} else if (empty($rede)) { // se não veio pelo controller.
		$rede = "";
	}

	if(!empty($_GET['filial'])) { // se veio pela URL
		$filial = $_GET['filial'];
	} else if (empty($filial)) { // se não veio pelo controller.
		$filial = "";
	}

	if(!empty($_GET['cod_agente'])) { // se veio pela URL
		$cod_agente = $_GET['cod_agente'];
	} else if (empty($cod_agente) || is_null($cod_agente)) { // se não veio pelo controller.
		$cod_agente = "";
	}

?>

<?php 

	echo form_open('controller_checkout/salvar_dados');
	/*   CAMPOS QUE ESTAVAM SALVOS   */

	if(!empty($checkoutSalvo)) { // esse checkout já estava em andamento.
		$obs = $checkoutSalvo->row()->observacoes;
		$id_checkout = $checkoutSalvo->row()->id_checkout;
		
		//Atendimento
		if($atendimentoSalvo->row()->atendimento_telefonia == 1) {
			$atendimento_telefonia = 'checked';	
		} else {
			$atendimento_telefonia = '';	
		}

		if($atendimentoSalvo->row()->atendimento_aberto == 1) {
			$atendimento_aberto = 'checked';	
		} else {
			$atendimento_aberto = '';	
		}

		if($atendimentoSalvo->row()->atendimento_mobile == 1) {
			$atendimento_mobile = 'checked';	
		} else {
			$atendimento_mobile = '';	
		}
		
		if($atendimentoSalvo->row()->balcao_vitrine == 1) {
			$balcao_vitrine = 'checked';	
		} else {
			$balcao_vitrine = '';	
		}

		if($atendimentoSalvo->row()->balcao_degustador == 1) {
			$balcao_degustador = 'checked';	
		} else {
			$balcao_degustador = '';	
		}

		$qtd_vendedor = $atendimentoSalvo->row()->qtd_vendedor;
		$loja = $atendimentoSalvo->row()->loja;
		$qts_assistentes = $atendimentoSalvo->row()->qts_assistentes;
		$qts_caixas = $atendimentoSalvo->row()->qts_caixas;
		$setor = $atendimentoSalvo->row()->setor;

		//Faturamento
		$faturamento_pdv = $faturamentoSalvo->row()->faturamento_pdv;
		$faturamento_setor = $faturamentoSalvo->row()->faturamento_setor;
		$va_f1 = $faturamentoSalvo->row()->va_f1;
		$va_f2 = $faturamentoSalvo->row()->va_f2;
		$va_f3 = $faturamentoSalvo->row()->va_f3;
		$va_f4 = $faturamentoSalvo->row()->va_f4;
		$va_f5 = $faturamentoSalvo->row()->va_f5;
		$va_f6 = $faturamentoSalvo->row()->va_f6;

		//Promotor
		$promotor = $promotorSalvo->row()->promotor;
		$tempo_pdv = $promotorSalvo->row()->tempo_pdv;
		$agencia = $promotorSalvo->row()->agencia;
		$supervisor = $promotorSalvo->row()->supervisor;

		//atendepn
		$apresentacao = $atendepnSalvo->row()->apresentacao;
		$conhecimento = $atendepnSalvo->row()->conhecimento;
		$book = $atendepnSalvo->row()->book;


	} else { // Caso o formulário não esteja sendo resgatado, em primeiro momento as variaveis são zeradas
		$obs = "";
		$id_checkout = "";	

		//Atendimento
		$atendimento_telefonia = '';
		$atendimento_aberto = '';
		$atendimento_mobile = '';
		$qtd_vendedor = "";
		$loja = "";
		$qts_assistentes = "";
		$qts_caixas = "";
		$setor = "";
		$balcao_vitrine = '';
		$balcao_degustador = '';

		//Faturamento
		$faturamento_pdv = '';
		$faturamento_setor  = '';
		$va_f1 = '';
		$va_f2 = '';
		$va_f3 = '';
		$va_f4 = '';
		$va_f5 = '';
		$va_f6 = '';

		//Promotor
		$promotor = '';
		$tempo_pdv = '';
		$agencia = '';
		$supervisor = '';

		//atendepn DEIXA A PRIMEIRA OPÇÃO MARCADA OBRIGATÓRIAMENTE
		$apresentacao = 'a';
		$conhecimento = 'a';
		$book = 'a';
	} 

	if($dadosEsteMes['registros'] > 0) {

		//Atendimento
		if($dadosEsteMes['dados']->row()->atendimento_telefonia == 1) {
			$atendimento_telefonia = 'checked';	
		} else {
			$atendimento_telefonia = '';	
		}

		if($dadosEsteMes['dados']->row()->atendimento_aberto == 1) {
			$atendimento_aberto = 'checked';	
		} else {
			$atendimento_aberto = '';	
		}

		if($dadosEsteMes['dados']->row()->atendimento_mobile == 1) {
			$atendimento_mobile = 'checked';	
		} else {
			$atendimento_mobile = '';	
		}
		
		if($dadosEsteMes['dados']->row()->balcao_vitrine == 1) {
			$balcao_vitrine = 'checked';	
		} else {
			$balcao_vitrine = '';	
		}

		if($dadosEsteMes['dados']->row()->balcao_degustador == 1) {
			$balcao_degustador = 'checked';	
		} else {
			$balcao_degustador = '';	
		}

		$qtd_vendedor = $dadosEsteMes['dados']->row()->qtd_vendedor;
		$loja = $dadosEsteMes['dados']->row()->loja;
		$qts_assistentes = $dadosEsteMes['dados']->row()->qts_assistentes;
		$qts_caixas = $dadosEsteMes['dados']->row()->qts_caixas;
		$setor = $dadosEsteMes['dados']->row()->setor;

		//Faturamento
		$faturamento_pdv = $dadosEsteMes['dados']->row()->faturamento_pdv;
		$faturamento_setor = $dadosEsteMes['dados']->row()->faturamento_setor;

	} 

	if($dadosEstaSemana['registros'] > 0) {
		$va_f1 = $dadosEstaSemana['dados']->row()->va_f1;
		$va_f2 = $dadosEstaSemana['dados']->row()->va_f2;
		$va_f3 = $dadosEstaSemana['dados']->row()->va_f3;
		$va_f4 = $dadosEstaSemana['dados']->row()->va_f4;
		$va_f5 = $dadosEstaSemana['dados']->row()->va_f5;
		$va_f6 = $dadosEstaSemana['dados']->row()->va_f6;

		//Promotor
		$promotor = $dadosEstaSemana['dados']->row()->promotor;
		$tempo_pdv = $dadosEstaSemana['dados']->row()->tempo_pdv;
		$agencia = $dadosEstaSemana['dados']->row()->agencia;
		$supervisor = $dadosEstaSemana['dados']->row()->supervisor;

	}

?>

<input class="form-control" type="hidden" name="endereco_exato" value="<?php echo $endereco_exato; ?>">
<input class="form-control" type="hidden" name="fk_usuario" value="<?php echo $fk_usuario; ?>">
<input class="form-control" type="hidden" name="fk_pdv" value="<?php echo $fk_pdv; ?>">
<input class="form-control" type="hidden" name="nome_usuario" value="<?php echo $nome_usuario; ?>">
<input class="form-control" type="hidden" name="id_checkout" id="id_checkout" value="<?php echo $id_checkout; ?>">

<?php

	//Nova validação, se for o PDV 1 libera os campos e deixa em branco.
	if($fk_pdv == 1) {

?>

<h4 align="center">PREENCHA OS CAMPOS ABAIXO</h4>

<div class="row">
	<div class="col-xs-12">
	<select class="form-control" name="rede">
	<?php 

	foreach ($redes as $rede) {
		echo '<option value="'.$rede->rede.'">'.$rede->rede.'</option>';
	}
	 ?>
	</select>
	</div>
</div>

<div class="row">
	<div class="col-xs-12">
	<select class="form-control" name="filial">
		<option value="1">Filial 1</option>
		<option value="2">Filial 2</option>
	</select>
	</div>
</div>

<div class="row">
	<div class="col-xs-12"><input class="form-control" type="tel" name="cnpj" id="cnpj" placeholder="CNPJ" value=""></div>
</div>

<input type="hidden" name="codmod" value="" maxlength="4">
<?php 

	} else { // Caso Não seja o PDV 1

?>

<div class="row">
	<div class="col-xs-12"><input class="form-control" type="" name="rede" placeholder="REDE" value="<?php echo $rede; ?>" readonly></div>
</div>

<div class="row">
	<div class="col-xs-12"><input class="form-control" type="" name="filial" placeholder="FILIAL" value="<?php if($filial == 1) {echo 'Filial 1'; } else { echo 'Filial 2'; } ?>" readonly></div>
</div>

<div class="row">
	<div class="col-xs-8 nopaddingright"><input class="form-control" type="" name="pdv" placeholder="PDV" value="<?php echo $rede; ?>" readonly></div>
	<div class="col-xs-4 nopaddingleft"><input class="form-control" type="" name="codmod" placeholder="CÓD.MOD." value="<?php echo $cod_agente; ?>" readonly></div>
</div>

<?php

	} //Fim Else

?>

<!-- Informaçoes PDV -->

<div class="margem"><h5>INFORMAÇÃO DO PDV</h5></div>
<hr>
<h5>ATENDIMENTO</h5>

<div class="row">
	<div class="col-xs-3">
		<input type="checkbox" name="atendimento_telefonia" <?php echo $atendimento_telefonia ?> > Telefonia
	</div>
	<div class="col-xs-3">
		<input type="checkbox" name="atendimento_aberto" <?php echo $atendimento_aberto ?> > Aberto
	</div>
	<div class="col-xs-3">
		<input type="checkbox" name="atendimento_mobile" <?php echo $atendimento_mobile ?> > Mobile
	</div>
</div>

<div class="row">
	<div class="col-xs-4 nopaddingright">
		<input class="form-control readonly_dadosEsteMes" type="tel" name="qtd_vendedor" placeholder="Qtd. VENDEDOR" value="<?php echo $qtd_vendedor ?>">
	</div>
	<div class="col-xs-4 nopaddingright nopaddingleft">
		<input class="form-control readonly_dadosEsteMes" type="tel" name="loja" placeholder="Qtd. Func. (Geral)" value="<?php echo $loja ?>">
	</div>
	<div class="col-xs-4 nopaddingleft">
		<input class="form-control readonly_dadosEsteMes" type="" name="setor" placeholder="Setor" maxlength="100" value="<?php echo $setor ?>">
	</div>
</div>

<div class="row">
	<div class="col-xs-4 nopaddingright">
		<input class="form-control readonly_dadosEsteMes" type="tel" name="qts_assistentes" placeholder="Qtd. Assistente" value="<?php echo $qts_assistentes ?>">
	</div>
	<div class="col-xs-4 nopaddingleft nopaddingright">
		<input class="form-control readonly_dadosEsteMes" type="tel" name="qts_caixas" placeholder="Qtd. Caixas" value="<?php echo $qts_caixas ?>">
	</div>
</div>


<h5>BALCÃO & MOBILIÁRIO</h5>

<div class="row">
	<div class="col-xs-6">
		<input type="checkbox" name="balcao_vitrine" <?php echo $balcao_vitrine ?> > Vitrine
	</div>
	<div class="col-xs-6">
		<input type="checkbox" name="balcao_degustador" <?php echo $balcao_degustador ?> > Degustador
	</div>
</div>

<div class="row">
	<div class="col-xs-12">
		<a type="btn" class="col-xs-12 btn btn-info" id="dadosEsteMes" height="100px">EDITAR ATENDIMENTO</a>
	</div>
</div>

<h5>FATURAMENTO</h5>

<div class="row">
	<div class="col-xs-6 nopaddingright">
		<input class="form-control moeda readonly_dadosEsteMesFaturamento" type="tel" name="faturamento_pdv" placeholder="PDV (GERAL) R$" value="<?php echo $faturamento_pdv; ?>">
	</div>
	<div class="col-xs-6 nopaddingleft">
		<input class="form-control moeda readonly_dadosEsteMesFaturamento" type="tel" name="faturamento_setor" placeholder="SETOR TELEFONIA R$" value="<?php echo $faturamento_setor; ?>">
	</div>
</div>

<div class="row">
	<div class="col-xs-12">
		<a type="btn" class="col-xs-12 btn btn-info" id="dadosEsteMesFaturamento" height="100px">EDITAR FATURAMENTO</a>
	</div>
</div>

<!-- valorAparelhos_faixaNumero -->
<h5>VENDAS DE APARELHOS</h5> 

<div class="row">
	<div class="col-xs-6 nopaddingright">
		<input class="form-control readonly_dadosEstaSemanaVendas" type="tel" name="va_f1" placeholder="ATÉ R$ 500,00" value="<?php echo $va_f1; ?>">
	</div>
	<div class="col-xs-6 nopaddingleft">
		<input class="form-control readonly_dadosEstaSemanaVendas" type="tel" name="va_f2" placeholder="R$ 501,00 A R$ 999,00" value="<?php echo $va_f2; ?>">
	</div>
</div>
<div class="row">
	<div class="col-xs-6 nopaddingright">
		<input class="form-control readonly_dadosEstaSemanaVendas" type="tel" name="va_f3" placeholder="R$ 1000,00 A R$ 1499,00" value="<?php echo $va_f3; ?>">
	</div>
	<div class="col-xs-6 nopaddingleft">
		<input class="form-control readonly_dadosEstaSemanaVendas" type="tel" name="va_f4" placeholder="R$ 1500,00 A R$ 1999,00" value="<?php echo $va_f4; ?>">
	</div>
</div>
<div class="row">
	<div class="col-xs-6 nopaddingright">
		<input class="form-control readonly_dadosEstaSemanaVendas" type="tel" name="va_f5" placeholder="R$ 2000,00 A R$ 2499,00" value="<?php echo $va_f5; ?>">
	</div>
	<div class="col-xs-6 nopaddingleft">
		<input class="form-control readonly_dadosEstaSemanaVendas" type="tel" name="va_f6" placeholder="ACIMA R$ 2500,00" value="<?php echo $va_f6; ?>">
	</div>
</div>

<div class="row">
	<div class="col-xs-12">
		<a type="btn" class="col-xs-12 btn btn-info" id="dadosEstaSemanaVendas" height="100px">EDITAR VENDAS DE APARELHOS</a>
	</div>
</div>

<div class="margem"><h5>INFORMAÇÕES DE VENDA</h5></div>
<hr>
<h5>VENDA SEMANAL PRÉ-PAGO - PDV</h5>

<div class="row">
	<div class="col-xs-6 nopaddingright">
		<input class="form-control" type="tel" name="t1venda1" placeholder="CHIP CLARO" value="<?php if(!empty($informacaovendasSalvo)){echo $informacaovendasSalvo->row(0)->venda;}?>">
	</div>
	<div class="col-xs-6 nopaddingleft">
		<input class="form-control" type="tel" name="t1venda2" placeholder="CHIP OI" value="<?php if(!empty($informacaovendasSalvo)){echo $informacaovendasSalvo->row(1)->venda;}?>">
	</div>
</div>
<div class="row">
	<div class="col-xs-6 nopaddingright">
		<input class="form-control" type="tel" name="t1venda3" placeholder="CHIP VIVO" value="<?php if(!empty($informacaovendasSalvo)){echo $informacaovendasSalvo->row(2)->venda;}?>">
	</div>
	<div class="col-xs-6 nopaddingleft">
		<input class="form-control" type="tel" name="t1venda4" placeholder="CHIP TIM" value="<?php if(!empty($informacaovendasSalvo)){echo $informacaovendasSalvo->row(3)->venda;}?>">
	</div>
</div>

<h5>VENDA SEMANAL CONTROLE - PDV</h5>

<div class="row">
	<div class="col-xs-6 nopaddingright">
		<input class="form-control" type="tel" name="t2venda1" placeholder="CHIP CLARO" value="<?php if(!empty($informacaovendasSalvo)){echo $informacaovendasSalvo->row(4)->venda;}?>">
	</div>
	<div class="col-xs-6 nopaddingleft">
		<input class="form-control" type="tel" name="t2venda2" placeholder="CHIP OI" value="<?php if(!empty($informacaovendasSalvo)){echo $informacaovendasSalvo->row(5)->venda;}?>">
	</div>
</div>
<div class="row">
	<div class="col-xs-6 nopaddingright">
		<input class="form-control" type="tel" name="t2venda3" placeholder="CHIP VIVO" value="<?php if(!empty($informacaovendasSalvo)){echo $informacaovendasSalvo->row(6)->venda;}?>">
	</div>
	<div class="col-xs-6 nopaddingleft">
		<input class="form-control" type="tel" name="t2venda4" placeholder="CHIP TIM" value="<?php if(!empty($informacaovendasSalvo)){echo $informacaovendasSalvo->row(7)->venda;}?>">
	</div>
</div>

<h5>APARELHOS MAIS VENDIDOS</h5>

<?php

	$opcoes = '';

	foreach ($marcas as $marca) {
    	$opcoes .= '<option value="'.$marca->id_marca.'">'.$marca->marca.'</option>';
	}
?>

<div class="row">
	<div class="col-xs-12">
		<select class="form-control" name="marcas1" id="marcas1" indice="1">
			<option value="0">Selecione uma marca.</option>
			<?php 
				if(!empty($maisvendidosSalvo)){
					foreach ($marcas as $marca) {
						if($marca->id_marca == $maisvendidosSalvo->row(0)->fk_marca){
							echo '<option value="'.$marca->id_marca.'" selected>'.$marca->marca.'</option>';
						} else {
							echo '<option value="'.$marca->id_marca.'">'.$marca->marca.'</option>';
						}
					}
				} else {
					echo $opcoes;
				}
			 ?>
		</select>
	</div>
</div>

<div class="row">
	<div class="col-xs-6 nopaddingright">
		<input type="hidden" name="modeloAjax1" id="modeloAjax1" value="<?php if(!empty($maisvendidosSalvo)){echo $maisvendidosSalvo->row(0)->fk_modelo;}?>">
			<div class="progress" id="barra1">
			  <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 45%">
			    <span class="sr-only">45% Complete</span>
			  </div>
			</div>
		<div id="load1">
			<select class="form-control" type="" name="nome_aparelho1" id="aparelho1">
				<option value="0">Selecione uma marca acima...</option>
			</select>
		</div>

	</div>
	<div class="col-xs-3 nopaddingright nopaddingleft">
		<input class="form-control" type="tel" name="valor1" placeholder="Valor R$" value="<?php if(!empty($maisvendidosSalvo)){echo $maisvendidosSalvo->row(0)->valor;}?>">
	</div>
	<div class="col-xs-3 nopaddingleft">
		<input class="form-control" type="tel" name="media1" placeholder="Média Mensal" value="<?php if(!empty($maisvendidosSalvo)){echo $maisvendidosSalvo->row(0)->media;}?>">
	</div>
</div>

<div class="row">
	<div class="col-xs-12">
		<select class="form-control" name="marcas2" id="marcas2" indice="2">
			<option value="0">Selecione uma marca.</option>
			<?php 
				if(!empty($maisvendidosSalvo)){
					foreach ($marcas as $marca) {
						if($marca->id_marca == $maisvendidosSalvo->row(1)->fk_marca){
							echo '<option value="'.$marca->id_marca.'" selected>'.$marca->marca.'</option>';
						} else {
							echo '<option value="'.$marca->id_marca.'">'.$marca->marca.'</option>';
						}
					}
				} else {
					echo $opcoes;
				}
			 ?>
		</select>
	</div>
</div>

<div class="row">
	<div class="col-xs-6 nopaddingright">
	<input type="hidden" name="modeloAjax2" id="modeloAjax2" value="<?php if(!empty($maisvendidosSalvo)){echo $maisvendidosSalvo->row(1)->fk_modelo;}?>">
			<div class="progress" id="barra2">
			  <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 45%">
			    <span class="sr-only">45% Complete</span>
			  </div>
			</div>
		<div id="load2">
			<select class="form-control" type="" name="nome_aparelho2" id="aparelho2">
				<option value="0">Selecione uma marca acima...</option>
			</select>
		</div>
	</div>
	<div class="col-xs-3 nopaddingright nopaddingleft">
		<input class="form-control" type="tel" name="valor2" placeholder="Valor R$" value="<?php if(!empty($maisvendidosSalvo)){echo $maisvendidosSalvo->row(1)->valor;}?>">
	</div>
	<div class="col-xs-3 nopaddingleft">
		<input class="form-control" type="tel" name="media2" placeholder="Média Mensal" value="<?php if(!empty($maisvendidosSalvo)){echo $maisvendidosSalvo->row(1)->media;}?>">
	</div>
</div>

<div class="row">
	<div class="col-xs-12">
		<select class="form-control" name="marcas3" id="marcas3" indice="3">
			<option value="0">Selecione uma marca.</option>
			<?php 
				if(!empty($maisvendidosSalvo)){
					foreach ($marcas as $marca) {
						if($marca->id_marca == $maisvendidosSalvo->row(2)->fk_marca){
							echo '<option value="'.$marca->id_marca.'" selected>'.$marca->marca.'</option>';
						} else {
							echo '<option value="'.$marca->id_marca.'">'.$marca->marca.'</option>';
						}
					}
				} else {
					echo $opcoes;
				}
			 ?>
		</select>
	</div>
</div>

<div class="row">
	<div class="col-xs-6 nopaddingright">
	<input type="hidden" name="modeloAjax3" id="modeloAjax3" value="<?php if(!empty($maisvendidosSalvo)){echo $maisvendidosSalvo->row(2)->fk_modelo;}?>">
			<div class="progress" id="barra3">
			  <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 45%">
			    <span class="sr-only">45% Complete</span>
			  </div>
			</div>
		<div id="load3">
			<select class="form-control" type="" name="nome_aparelho3" id="aparelho3">
				<option value="0">Selecione uma marca acima...</option>
			</select>
		</div>
	</div>
	<div class="col-xs-3 nopaddingright nopaddingleft">
		<input class="form-control" type="tel" name="valor3" placeholder="Valor R$" value="<?php if(!empty($maisvendidosSalvo)){echo $maisvendidosSalvo->row(2)->valor;}?>">
	</div>
	<div class="col-xs-3 nopaddingleft">
		<input class="form-control" type="tel" name="media3" placeholder="Média Mensal" value="<?php if(!empty($maisvendidosSalvo)){echo $maisvendidosSalvo->row(2)->media;}?>">
	</div>
</div>

<div class="row">
	<div class="col-xs-12">
		<select class="form-control" name="marcas4" id="marcas4" indice="4">
			<option value="0">Selecione uma marca.</option>
			<?php 
				if(!empty($maisvendidosSalvo)){
					foreach ($marcas as $marca) {
						if($marca->id_marca == $maisvendidosSalvo->row(3)->fk_marca){
							echo '<option value="'.$marca->id_marca.'" selected>'.$marca->marca.'</option>';
						} else {
							echo '<option value="'.$marca->id_marca.'">'.$marca->marca.'</option>';
						}
					}
				} else {
					echo $opcoes;
				}
			 ?>
		</select>
	</div>
</div>

<div class="row">
	<div class="col-xs-6 nopaddingright">
	<input type="hidden" name="modeloAjax4" id="modeloAjax4" value="<?php if(!empty($maisvendidosSalvo)){echo $maisvendidosSalvo->row(4)->fk_modelo;}?>">
			<div class="progress" id="barra4">
			  <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 45%">
			    <span class="sr-only">45% Complete</span>
			  </div>
			</div>
		<div id="load4">
			<select class="form-control" type="" name="nome_aparelho4" id="aparelho4">
				<option value="0">Selecione uma marca acima...</option>
			</select>
		</div>
	</div>
	<div class="col-xs-3 nopaddingright nopaddingleft">
		<input class="form-control" type="tel" name="valor4" placeholder="Valor R$" value="<?php if(!empty($maisvendidosSalvo)){echo $maisvendidosSalvo->row(3)->valor;}?>">
	</div>
	<div class="col-xs-3 nopaddingleft">
		<input class="form-control" type="tel" name="media4" placeholder="Média Mensal" value="<?php if(!empty($maisvendidosSalvo)){echo $maisvendidosSalvo->row(3)->media;}?>">
	</div>
</div>

<div class="margem"><h5><strong>INFORMAÇÕES PROMOTOR</strong></h5></div>
<hr>

<div class="row">
	<div class="col-xs-12">
		Tem Promotor? <input type="checkbox" name="e_promotor" id="e_promotor" value=""> 
	</div>
</div>
<div id="promotor">
<div class="row">
		<div class="col-xs-12">
			<input class="form-control readonly_dadosEstaSemanaPromotor" type="" name="pc_promotor" id="pc_promotor" placeholder="PROMOTOR" maxlength="100" value="<?php echo $promotor; ?>">
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<input class="form-control readonly_dadosEstaSemanaPromotor" type="tel" name="pc_tempo_pdv" id="pc_tempo_pdv" placeholder="Quanto tempo no PDV? (Em meses)" value="<?php echo $tempo_pdv; ?>">
		</div>
	</div>
<div class="row">
	<div class="col-xs-12">
		<input class="form-control readonly_dadosEstaSemanaPromotor" type="" name="pc_agencia" placeholder="AGÊNCIA" maxlength="100" value="<?php echo $agencia; ?>">
	</div>
</div>
<div class="row">
	<div class="col-xs-12">
		<input class="form-control readonly_dadosEstaSemanaPromotor" type="" name="pc_supervisor" placeholder="SUPERVISOR" maxlength="100" value="<?php echo $supervisor; ?>">
	</div>
</div>
</div>

<div class="row">
	<div class="col-xs-12">
		<a type="btn" class="col-xs-12 btn btn-info" id="dadosEstaSemanaPromotor" height="100px">EDITAR PROMOTOR</a>
	</div>
</div>

<br>

<h6>Apresentação (Uniforme & Crachá)</h6>

<div class="row">
	<div class="col-xs-4">
		<?php if($apresentacao == 'a') {$checked = 'checked';} else {$checked = '';} ?>
		<input type="radio" name="apresentacao" value="a" <?php echo $checked?> > Atende
	</div>
	<div class="col-xs-4">
		<?php if($apresentacao == 'p') {$checked = 'checked';} else {$checked = '';} ?>
		<input type="radio" name="apresentacao" value="p" <?php echo $checked?> > A. Parcial
	</div>
	<div class="col-xs-4">
		<?php if($apresentacao == 'n') {$checked = 'checked';} else {$checked = '';} ?>
		<input type="radio" name="apresentacao" value="n" <?php echo $checked?> > N. Atende
	</div>
</div>

<h6>Conhecimento (Ofertas Claro)</h6>

<div class="row">
	<div class="col-xs-4">
		<?php if($conhecimento == 'a') {$checked = 'checked';} else {$checked = '';} ?>
		<input type="radio" name="conhecimento" value="a" <?php echo $checked?> > Atende
	</div>
	<div class="col-xs-4">
		<?php if($conhecimento == 'p') {$checked = 'checked';} else {$checked = '';} ?>
		<input type="radio" name="conhecimento" value="p" <?php echo $checked?> > A. Parcial
	</div>
	<div class="col-xs-4">
		<?php if($conhecimento == 'n') {$checked = 'checked';} else {$checked = '';} ?>
		<input type="radio" name="conhecimento" value="n" <?php echo $checked?> > N. Atende
	</div>
</div>

<h6>Book (Preenchimento)</h6>

<div class="row">
	<div class="col-xs-4">
		<?php if($book == 'a') {$checked = 'checked';} else {$checked = '';} ?>
		<input type="radio" name="book" value="a" <?php echo $checked?> > Atende
	</div>
	<div class="col-xs-4">
		<?php if($book == 'p') {$checked = 'checked';} else {$checked = '';} ?>
		<input type="radio" name="book" value="p" <?php echo $checked?> > A. Parcial
	</div>
	<div class="col-xs-4">
		<?php if($book == 'n') {$checked = 'checked';} else {$checked = '';} ?>
		<input type="radio" name="book" value="n" <?php echo $checked?> > N. Atende
	</div>
</div>

<h6>Claro</h6>
<div class="row">
	<div class="col-xs-4 nopaddingright">
		<input class="form-control" type="tel" name="claro_fixo" placeholder="FIXO" value="<?php if(!empty($parttimeSalvo)){echo $parttimeSalvo->row(0)->fixo;}?>">
	</div>
	<div class="col-xs-4 nopaddingright nopaddingleft">
		<input class="form-control" type="tel" name="claro_pt" placeholder="PART TIME" value="<?php if(!empty($parttimeSalvo)){echo $parttimeSalvo->row(0)->part_time;}?>">
	</div>
	<div class="col-xs-4 nopaddingleft">
		<input class="form-control" type="tel" name="claro_free" placeholder="FREE" value="<?php if(!empty($parttimeSalvo)){echo $parttimeSalvo->row(0)->free;}?>">
	</div>
</div>

<h6>Oi</h6>
<div class="row">
	<div class="col-xs-4 nopaddingright">
		<input class="form-control" type="tel" name="oi_fixo" placeholder="FIXO" value="<?php if(!empty($parttimeSalvo)){echo $parttimeSalvo->row(1)->fixo;}?>">
	</div>
	<div class="col-xs-4 nopaddingright nopaddingleft">
		<input class="form-control" type="tel" name="oi_pt" placeholder="PART TIME" value="<?php if(!empty($parttimeSalvo)){echo $parttimeSalvo->row(1)->part_time;}?>">
	</div>
	<div class="col-xs-4 nopaddingleft">
		<input class="form-control" type="tel" name="oi_free" placeholder="FREE" value="<?php if(!empty($parttimeSalvo)){echo $parttimeSalvo->row(1)->free;}?>">
	</div>
</div>

<h6>Vivo</h6>
<div class="row">
	<div class="col-xs-4 nopaddingright">
		<input class="form-control" type="tel" name="vivo_fixo" placeholder="FIXO" value="<?php if(!empty($parttimeSalvo)){echo $parttimeSalvo->row(2)->fixo;}?>">
	</div>
	<div class="col-xs-4 nopaddingright nopaddingleft">
		<input class="form-control" type="tel" name="vivo_pt" placeholder="PART TIME" value="<?php if(!empty($parttimeSalvo)){echo $parttimeSalvo->row(2)->part_time;}?>">
	</div>
	<div class="col-xs-4 nopaddingleft">
		<input class="form-control" type="tel" name="vivo_free" placeholder="FREE" value="<?php if(!empty($parttimeSalvo)){echo $parttimeSalvo->row(2)->free;}?>">
	</div>
</div>

<h6>Tim</h6>
<div class="row">
	<div class="col-xs-4 nopaddingright">
		<input class="form-control" type="tel" name="tim_fixo" placeholder="FIXO" value="<?php if(!empty($parttimeSalvo)){echo $parttimeSalvo->row(3)->fixo;}?>">
	</div>
	<div class="col-xs-4 nopaddingright nopaddingleft">
		<input class="form-control" type="tel" name="tim_pt" placeholder="PART TIME" value="<?php if(!empty($parttimeSalvo)){echo $parttimeSalvo->row(3)->part_time;}?>">
	</div>
	<div class="col-xs-4 nopaddingleft">
		<input class="form-control" type="tel" name="tim_free" placeholder="FREE" value="<?php if(!empty($parttimeSalvo)){echo $parttimeSalvo->row(3)->free;}?>">
	</div>
</div>

<div class="margem"><h5>CAMPANHA</h5></div>
<hr>

Claro
<div class="row">
	<div class="col-xs-12 nopaddingright">
		<input class="form-control" type="" name="con_claro_campanha" placeholder="CAMPANHA" maxlength="100" value="<?php if(!empty($campanhaSalvo)){echo $campanhaSalvo->row(0)->campanha;}?>">
	</div>
</div>
<div class="row">
	<div class="col-xs-12 nopaddingright">
		<input class="form-control" type="" name="con_claro_produto" placeholder="PRODUTO" maxlength="100" value="<?php if(!empty($campanhaSalvo)){echo $campanhaSalvo->row(0)->produto;}?>">
	</div>
</div>
<div class="row">
	<div class="col-xs-12 nopaddingright">
		<input class="form-control" type="" name="con_claro_periodo" placeholder="PERÍODO" maxlength="100" value="<?php if(!empty($campanhaSalvo)){echo $campanhaSalvo->row(0)->periodo;}?>">
	</div>
</div>

Oi
<div class="row">
	<div class="col-xs-12 nopaddingright">
		<input class="form-control" type="" name="con_oi_campanha" placeholder="CAMPANHA" maxlength="100" value="<?php if(!empty($campanhaSalvo)){echo $campanhaSalvo->row(1)->campanha;}?>">
	</div>
</div>
<div class="row">
	<div class="col-xs-12 nopaddingright">
		<input class="form-control" type="" name="con_oi_produto" placeholder="PRODUTO" maxlength="100" value="<?php if(!empty($campanhaSalvo)){echo $campanhaSalvo->row(1)->produto;}?>">
	</div>
</div>
<div class="row">
	<div class="col-xs-12 nopaddingright">
		<input class="form-control" type="" name="con_oi_periodo" placeholder="PERÍODO" maxlength="100" value="<?php if(!empty($campanhaSalvo)){echo $campanhaSalvo->row(1)->periodo;}?>">
	</div>
</div>

Vivo
<div class="row">
	<div class="col-xs-12 nopaddingright">
		<input class="form-control" type="" name="con_vivo_campanha" placeholder="CAMPANHA" maxlength="100" value="<?php if(!empty($campanhaSalvo)){echo $campanhaSalvo->row(2)->campanha;}?>">
	</div>
</div>
<div class="row">
	<div class="col-xs-12 nopaddingright">
		<input class="form-control" type="" name="con_vivo_produto" placeholder="PRODUTO" maxlength="100" value="<?php if(!empty($campanhaSalvo)){echo $campanhaSalvo->row(2)->produto;}?>">
	</div>
</div>
<div class="row">
	<div class="col-xs-12 nopaddingright">
		<input class="form-control" type="" name="con_vivo_periodo" placeholder="PERÍODO" maxlength="100" value="<?php if(!empty($campanhaSalvo)){echo $campanhaSalvo->row(2)->periodo;}?>">
	</div>
</div>

Tim
<div class="row">
	<div class="col-xs-12 nopaddingright">
		<input class="form-control" type="" name="con_tim_campanha" placeholder="CAMPANHA" maxlength="100" value="<?php if(!empty($campanhaSalvo)){echo $campanhaSalvo->row(3)->campanha;}?>">
	</div>
</div>
<div class="row">
	<div class="col-xs-12 nopaddingright">
		<input class="form-control" type="" name="con_tim_produto" placeholder="PRODUTO" maxlength="100" value="<?php if(!empty($campanhaSalvo)){echo $campanhaSalvo->row(3)->produto;}?>">
	</div>
</div>
<div class="row">
	<div class="col-xs-12 nopaddingright">
		<input class="form-control" type="" name="con_tim_periodo" placeholder="PERÍODO" maxlength="100" value="<?php if(!empty($campanhaSalvo)){echo $campanhaSalvo->row(3)->periodo;}?>">
	</div>
</div>

<div class="margem"><h5>MATERIAIS MERCHANDISING</h5></div>
<hr>

Claro
<div class="row">
	<div class="col-xs-12 nopaddingright">
		<input class="form-control" type="" name="mm_claro" placeholder="MATERIAL" maxlength="100" value="<?php if(!empty($marketingSalvo)){echo $marketingSalvo->row(0)->material;}?>">
	</div>
</div>

Oi
<div class="row">
	<div class="col-xs-12 nopaddingright">
		<input class="form-control" type="" name="mm_oi" placeholder="MATERIAL" maxlength="100" value="<?php if(!empty($marketingSalvo)){echo $marketingSalvo->row(1)->material;}?>">
	</div>
</div>

Vivo
<div class="row">
	<div class="col-xs-12 nopaddingright">
		<input class="form-control" type="" name="mm_vivo" placeholder="MATERIAL" maxlength="100" value="<?php if(!empty($marketingSalvo)){echo $marketingSalvo->row(2)->material;}?>">
	</div>
</div>

Tim
<div class="row">
	<div class="col-xs-12 nopaddingright">
		<input class="form-control" type="" name="mm_tim" placeholder="MATERIAL" maxlength="100" value="<?php if(!empty($marketingSalvo)){echo $marketingSalvo->row(3)->material;}?>">
	</div>
</div>

<!-- Final -->

<div class="margem"><h5>OBSERVAÇÕES PDV</h5></div>
<hr>

<div class="row">
	<div class="col-xs-12">
		<textarea placeholder="ALTERAÇÕES IMPORTANTES E/OU ACONTECIMENTOS" rows="15" class="col-xs-12" name="obs"><?php echo $obs;?></textarea>
	</div>
</div>
<?php

	//Nova validação, se for o PDV 1 não pode continuar mais tarde
	if($fk_pdv != 1) {

?>
<div class="row">
	<div class="col-xs-12">
		<input type="hidden" id="checkpoint" name="checkpoint" value="">
		<button type="submit" class="col-xs-12 btn btn-info" id="continuar" height="100px">CONTINUAR MAIS TARDE</button>
	</div>
</div>

<?php } ?>

<div class="row">
	<div class="col-xs-12">
		<button type="submit" class="col-xs-12 btn btn-danger" id="envio_formulario" height="100px">CHECK-OUT</button>
	</div>
</div>

<?php echo form_close(); ?>

</body>
</html>

<?php

} else  {

?>
</head>
<body>

	<?php

	if($envio_direto == 1) {
		echo 'Fomulário enviado automaticamente.';
	} else {
		echo 'Erro no envio, por favor tente novamente mais tarde.';
	}

	?>

</body>
</html>


<?php }?>