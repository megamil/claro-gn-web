<?php 
date_default_timezone_set('America/Sao_Paulo');
// Convert to PDF
$html = '
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Claro GN</title>

    <!-- Bootstrap -->
    <link href="'.base_url().'style/pdf/css/bootstrap.css" rel="stylesheet">
    <link href="'.base_url().'style/pdf/estilo_analise_promotor.css" rel="stylesheet">

     <script type="text/javascript">

       print();

    </script>

</head>
<body class="container">

    <!-- topo do logo -->
    <div class="row topo altura-topo" style="border-bottom: 1px solid #888888">
        <div class="col-lg-4">
            <img src="'.base_url().'style/pdf/imagens/logo.jpg" width="200" height="55">
        </div>
        <div class="col-lg-4">
        </div>
        <div class="col-lg-4 relatorio-topo">
            RELATÓRIO: ANÁLISE PROMOTOR DE PDV<br />
            <span class="data-topo">'.date('d/m/Y H:i:s').'</span>
        </div>
    </div>

    <!-- topo da fonte -->
    <div class="row topo">
        <div class="col-lg-2">
        </div>
        <div class="col-lg-8">
            <h5 class="fonte"><i>
                <span style="font-size: 16px">Dashboard de Análise Promotor no PDV no período de '.$de.' a '.$ate.'</span><br/>
                <span style="font-size: 11px">(FONTE: CLARO GN)</span></i>
            </h5>
        </div>
        <div class="col-lg-2">
        </div>
    </div>

        <!-- div da média 1 -->
    <div class="row topo borda-media">
        <div class="col-lg-12 media-div">
            <span class="texto-media">
                Média Filial 1
            </span>
        </div>
    </div>

   <table class="table topo topo-table">
       <thead>
            <tr>
                <th class="col-lg-3"></th>
                <th class="col-lg-1"></th>
                <th class="col-lg-1">ATENDE</th>
                <th class="col-lg-1" style="border-right: 1px solid #c3c3c3"></th>
                <th class="col-lg-1"></th>
                <th class="col-lg-1">ATENDE PARCIALMENTE</th>
                <th class="col-lg-1" style="border-right: 1px solid #c3c3c3"></th>
                <th class="col-lg-1"></th>
                <th class="col-lg-1">NÃO ATENDE</th>
                <th class="col-lg-1"></th>
            </tr>

            <tr>
                <th class="col-lg-3"></th>
                <th class="col-lg-1" style="text-align: center">A</th>
                <th class="col-lg-1" style="text-align: center">B</th>
                <th class="col-lg-1" style="border-right: 1px solid #c3c3c3; text-align: center">C</th>
                <th class="col-lg-1" style="text-align: center">A</th>
                <th class="col-lg-1" style="text-align: center">B</th>
                <th class="col-lg-1" style="border-right: 1px solid #c3c3c3; text-align: center">C</th>
                <th class="col-lg-1" style="text-align: center">A</th>
                <th class="col-lg-1" style="text-align: center">B</th>
                <th class="col-lg-1" style="text-align: center">C</th>
            </tr>

             <tr>
                <th class="col-lg-3" style="border-right: 1px solid #c3c3c3">APRESENTAÇÃO</th>
                <td class="col-lg-1"  style="text-align: center">'.$qts_apresentacao_a1.'</td>
                <td class="col-lg-1"  style="text-align: center">'.$qts_apresentacao_b1.'</td>
                <td class="col-lg-1" style="border-right: 1px solid #c3c3c3; text-align: center">'.$qts_apresentacao_c1.'</td>
                <td class="col-lg-1"  style="text-align: center">'.$qts_apresentacao_a_p1.'</td>
                <td class="col-lg-1"  style="text-align: center">'.$qts_apresentacao_b_p1.'</td>
                <td class="col-lg-1" style="border-right: 1px solid #c3c3c3; text-align: center">'.$qts_apresentacao_c_p1.'</td>
                <td class="col-lg-1"  style="text-align: center">'.$qts_apresentacao_a_n1.'</td>
                <td class="col-lg-1"  style="text-align: center">'.$qts_apresentacao_b_n1.'</td>
                <td class="col-lg-1"  style="text-align: center">'.$qts_apresentacao_c_n1.'</td>
            </tr>

            <tr>
                <th class="col-lg-3" style="border-right: 1px solid #c3c3c3">BOOK</th>
                <td class="col-lg-1"  style="text-align: center">'.$qts_book_a1.'</td>
                <td class="col-lg-1"  style="text-align: center">'.$qts_book_b1.'</td>
                <td class="col-lg-1" style="border-right: 1px solid #c3c3c3; text-align: center">'.$qts_book_c1.'</td>
                <td class="col-lg-1"  style="text-align: center">'.$qts_book_a_p1.'</td>
                <td class="col-lg-1"  style="text-align: center">'.$qts_book_b_p1.'</td>
                <td class="col-lg-1" style="border-right: 1px solid #c3c3c3; text-align: center">'.$qts_book_c_p1.'</td>
                <td class="col-lg-1"  style="text-align: center">'.$qts_book_a_n1.'</td>
                <td class="col-lg-1"  style="text-align: center">'.$qts_book_b_n1.'</td>
                <td class="col-lg-1"  style="text-align: center">'.$qts_book_c_n1.'</td>
            </tr>

            <tr>
                <th class="col-lg-3" style="border-right: 1px solid #c3c3c3">CONHECIMENTO</th>
                <td class="col-lg-1"  style="text-align: center">'.$qts_conhecimento_a1.'</td>
                <td class="col-lg-1"  style="text-align: center">'.$qts_conhecimento_b1.'</td>
                <td class="col-lg-1" style="border-right: 1px solid #c3c3c3; text-align: center">'.$qts_conhecimento_c1.'</td>
                <td class="col-lg-1"  style="text-align: center">'.$qts_conhecimento_a_p1.'</td>
                <td class="col-lg-1"  style="text-align: center">'.$qts_conhecimento_b_p1.'</td>
                <td class="col-lg-1" style="border-right: 1px solid #c3c3c3; text-align: center">'.$qts_conhecimento_c_p1.'</td>
                <td class="col-lg-1"  style="text-align: center">'.$qts_conhecimento_a_n1.'</td>
                <td class="col-lg-1"  style="text-align: center">'.$qts_conhecimento_b_n1.'</td>
                <td class="col-lg-1"  style="text-align: center">'.$qts_conhecimento_c_n1.'</td>
            </tr>
       </thead>
   </table>

    <div class="row">
        <div class="col-lg-4 texto-media-rodape padding-rodape" style="display: inline-block; text-align: center">
             <span  class="texto-rodape" style="text-align: center">
                ATENDE
            </span> <br/>
             <table class="table topo topo-table">
                <thead>
                    <tr>
                        <th style="text-align: center">Apresentação</th>
                        <th style="text-align: center">Book</th>
                        <th style="text-align: center">Conhecimento</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>'.($qts_apresentacao_a1 + $qts_apresentacao_b1 + $qts_apresentacao_c1).'</td>
                        <td>'.($qts_book_a1 + $qts_book_b1 + $qts_book_c1).'</td>
                        <td>'.($qts_conhecimento_a1 + $qts_conhecimento_b1 + $qts_conhecimento_c1).'</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="col-lg-4 texto-media-rodape padding-rodape" style="display: inline-block">
            <span class="texto-rodape" style="text-align: center">
                ATENDE PARCIALMENTE
            </span> <br/>
            <table class="table topo topo-table">
                <thead>
                    <tr>
                        <th style="text-align: center">Apresentação</th>
                        <th style="text-align: center">Book</th>
                        <th style="text-align: center">Conhecimento</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>'.($qts_apresentacao_a_p1 + $qts_apresentacao_b_p1 + $qts_apresentacao_c_p1).'</td>
                        <td>'.($qts_book_a_p1 + $qts_book_b_p1 + $qts_book_c_p1).'</td>
                        <td>'.($qts_conhecimento_a_p1 + $qts_conhecimento_b_p1 + $qts_conhecimento_c_p1).'</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="col-lg-4 texto-media-rodape padding-rodape" style="display: inline-block">
                <span class="texto-rodape" style="text-align: center">
                    NÃO ATENDE
                </span><br/>
            <table class="table topo topo-table">
                <thead>
                    <tr>
                        <th style="text-align: center">Apresentação</th>
                        <th style="text-align: center">Book</th>
                        <th style="text-align: center">Conhecimento</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>'.($qts_apresentacao_a_n1 + $qts_apresentacao_b_n1 + $qts_apresentacao_c_n1).'</td>
                        <td>'.($qts_book_a_n1 + $qts_book_b_n1 + $qts_book_c_n1).'</td>
                        <td>'.($qts_conhecimento_a_n1 + $qts_conhecimento_b_n1 + $qts_conhecimento_c_n1).'</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

    <!-- div da média 2 -->
    <div class="row topo borda-media">
        <div class="col-lg-12 media-div">
            <span class="texto-media">
                Média Filial 2
            </span>
        </div>
    </div>

    <table class="table topo topo-table">
        <thead>
        <tr>
            <th class="col-lg-3"></th>
            <th class="col-lg-1"></th>
            <th class="col-lg-1">ATENDE</th>
            <th class="col-lg-1" style="border-right: 1px solid #c3c3c3"></th>
            <th class="col-lg-1"></th>
            <th class="col-lg-1">ATENDE PARCIALMENTE</th>
            <th class="col-lg-1" style="border-right: 1px solid #c3c3c3"></th>
            <th class="col-lg-1"></th>
            <th class="col-lg-1">NÃO ATENDE</th>
            <th class="col-lg-1"></th>
        </tr>

        <tr>
            <th class="col-lg-3"></th>
            <th class="col-lg-1" style="text-align: center">A</th>
            <th class="col-lg-1" style="text-align: center">B</th>
            <th class="col-lg-1" style="border-right: 1px solid #c3c3c3; text-align: center">C</th>
            <th class="col-lg-1" style="text-align: center">A</th>
            <th class="col-lg-1" style="text-align: center">B</th>
            <th class="col-lg-1" style="border-right: 1px solid #c3c3c3; text-align: center">C</th>
            <th class="col-lg-1" style="text-align: center">A</th>
            <th class="col-lg-1" style="text-align: center">B</th>
            <th class="col-lg-1" style="text-align: center">C</th>
        </tr>

         <tr>
                <th class="col-lg-3" style="border-right: 1px solid #c3c3c3">APRESENTAÇÃO</th>
                <td class="col-lg-1"  style="text-align: center">'.$qts_apresentacao_a2.'</td>
                <td class="col-lg-1"  style="text-align: center">'.$qts_apresentacao_b2.'</td>
                <td class="col-lg-1" style="border-right: 1px solid #c3c3c3; text-align: center">'.$qts_apresentacao_c2.'</td>
                <td class="col-lg-1"  style="text-align: center">'.$qts_apresentacao_a_p2.'</td>
                <td class="col-lg-1"  style="text-align: center">'.$qts_apresentacao_b_p2.'</td>
                <td class="col-lg-1" style="border-right: 1px solid #c3c3c3; text-align: center">'.$qts_apresentacao_c_p2.'</td>
                <td class="col-lg-1"  style="text-align: center">'.$qts_apresentacao_a_n2.'</td>
                <td class="col-lg-1"  style="text-align: center">'.$qts_apresentacao_b_n2.'</td>
                <td class="col-lg-1"  style="text-align: center">'.$qts_apresentacao_c_n2.'</td>
            </tr>

            <tr>
                <th class="col-lg-3" style="border-right: 1px solid #c3c3c3">BOOK</th>
                <td class="col-lg-1"  style="text-align: center">'.$qts_book_a2.'</td>
                <td class="col-lg-1"  style="text-align: center">'.$qts_book_b2.'</td>
                <td class="col-lg-1" style="border-right: 1px solid #c3c3c3; text-align: center">'.$qts_book_c2.'</td>
                <td class="col-lg-1"  style="text-align: center">'.$qts_book_a_p2.'</td>
                <td class="col-lg-1"  style="text-align: center">'.$qts_book_b_p2.'</td>
                <td class="col-lg-1" style="border-right: 1px solid #c3c3c3; text-align: center">'.$qts_book_c_p2.'</td>
                <td class="col-lg-1"  style="text-align: center">'.$qts_book_a_n2.'</td>
                <td class="col-lg-1"  style="text-align: center">'.$qts_book_b_n2.'</td>
                <td class="col-lg-1"  style="text-align: center">'.$qts_book_c_n2.'</td>
            </tr>

            <tr>
                <th class="col-lg-3" style="border-right: 1px solid #c3c3c3">CONHECIMENTO</th>
                <td class="col-lg-1"  style="text-align: center">'.$qts_conhecimento_a2.'</td>
                <td class="col-lg-1"  style="text-align: center">'.$qts_conhecimento_b2.'</td>
                <td class="col-lg-1" style="border-right: 1px solid #c3c3c3; text-align: center">'.$qts_conhecimento_c2.'</td>
                <td class="col-lg-1"  style="text-align: center">'.$qts_conhecimento_a_p2.'</td>
                <td class="col-lg-1"  style="text-align: center">'.$qts_conhecimento_b_p2.'</td>
                <td class="col-lg-1" style="border-right: 1px solid #c3c3c3; text-align: center">'.$qts_conhecimento_c_p2.'</td>
                <td class="col-lg-1"  style="text-align: center">'.$qts_conhecimento_a_n2.'</td>
                <td class="col-lg-1"  style="text-align: center">'.$qts_conhecimento_b_n2.'</td>
                <td class="col-lg-1"  style="text-align: center">'.$qts_conhecimento_c_n2.'</td>
            </tr>
        </thead>
    </table>

    <!-- div rodapé -->
    <div class="row">
        <div class="col-lg-4 texto-media-rodape padding-rodape" style="display: inline-block; text-align: center">
             <span  class="texto-rodape" style="text-align: center">
                ATENDE
            </span> <br/>
             <table class="table topo topo-table">
                <thead>
                    <tr>
                        <th style="text-align: center">Apresentação</th>
                        <th style="text-align: center">Book</th>
                        <th style="text-align: center">Conhecimento</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>'.($qts_apresentacao_a2 + $qts_apresentacao_b2 + $qts_apresentacao_c2).'</td>
                        <td>'.($qts_book_a2 + $qts_book_b2 + $qts_book_c2).'</td>
                        <td>'.($qts_conhecimento_a2 + $qts_conhecimento_b2 + $qts_conhecimento_c2).'</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="col-lg-4 texto-media-rodape padding-rodape" style="display: inline-block">
            <span class="texto-rodape" style="text-align: center">
                ATENDE PARCIALMENTE
            </span> <br/>
            <table class="table topo topo-table">
                <thead>
                    <tr>
                        <th style="text-align: center">Apresentação</th>
                        <th style="text-align: center">Book</th>
                        <th style="text-align: center">Conhecimento</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>'.($qts_apresentacao_a_p2 + $qts_apresentacao_b_p2 + $qts_apresentacao_c_p2).'</td>
                        <td>'.($qts_book_a_p2 + $qts_book_b_p2 + $qts_book_c_p2).'</td>
                        <td>'.($qts_conhecimento_a_p2 + $qts_conhecimento_b_p2 + $qts_conhecimento_c_p2).'</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="col-lg-4 texto-media-rodape padding-rodape" style="display: inline-block">
                <span class="texto-rodape" style="text-align: center">
                    NÃO ATENDE
                </span><br/>
            <table class="table topo topo-table">
                <thead>
                    <tr>
                        <th style="text-align: center">Apresentação</th>
                        <th style="text-align: center">Book</th>
                        <th style="text-align: center">Conhecimento</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>'.($qts_apresentacao_a_n2 + $qts_apresentacao_b_n2 + $qts_apresentacao_c_n2).'</td>
                        <td>'.($qts_book_a_n2 + $qts_book_b_n2 + $qts_book_c_n2).'</td>
                        <td>'.($qts_conhecimento_a_n2 + $qts_conhecimento_b_n2 + $qts_conhecimento_c_n2).'</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

</body>
</html>';

 echo $html;
// die();

// require_once('autoload.inc.php');
// $dompdf = new Dompdf\Dompdf();
// $dompdf->load_html($html);
// $dompdf->set_paper('A4','landscape');
// $dompdf->render();
// $dompdf->stream("analise_promotor_pdv-".date('d-m-Y_H-i-s').".pdf");

 ?>