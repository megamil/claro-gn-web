<?php 
date_default_timezone_set('America/Sao_Paulo');
// Convert to PDF
$html = '
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Claro GN</title>

    <!-- Bootstrap -->
    <script type="text/javascript" src="'.base_url().'style/js/canvasjs.min.js"></script>
    <script type="text/javascript" src="'.base_url().'style/js/jquery.js"></script>
    <link href="'.base_url().'style/pdf/css/bootstrap.css" rel="stylesheet">
    <link href="'.base_url().'style/pdf/estilo_media_horas.css" rel="stylesheet">

    <script type="text/javascript">

        print();
        var grafico_pizza;
        var grafico_pizza2;
        var grafico_pizza3;

        window.onload = function () {

            grafico_pizza = new CanvasJS.Chart("grafico_pizza",{

                    data: [
                    {
                        type: "pie",
                        indexLabelFontColor: "black",    
                        indexLabelLineColor: "darkgrey",
                        indexLabelFontWeight: "bold",
                        indexLabelFontSize: 15,
                        indexLabelFontFamily: "Garamond",
                        toolTipContent: "{name} #percent% ({y})", 
                        indexLabel: "{name} #percent% ({y})", 
                        showInLegend: true,
                        dataPoints: [
                            { y: '.$graficof1.' , name: "Filial 1", color: "#5cb85c", indexLabel: "Filial 2 #percent%"},
                            { y: '.$graficof2.' , name: "Filial 2", color: "#d9534f", indexLabel: "Filial 1 #percent%"}
                            
                        ]
                    }
                    ]
                });

                grafico_pizza.render();

                grafico_pizza2 = new CanvasJS.Chart("grafico_pizza2",{

                    data: [
                    {
                        type: "pie",
                        indexLabelFontColor: "black",    
                        indexLabelLineColor: "darkgrey",
                        indexLabelFontWeight: "bold",
                        indexLabelFontSize: 15,
                        indexLabelFontFamily: "Garamond",
                        toolTipContent: "{name} #percent% ({y})", 
                        indexLabel: "{name} #percent% ({y})", 
                        showInLegend: true,
                        dataPoints: [
                            { y: '.$grafico2f1.' , name: "Filial 1", color: "#5cb85c", indexLabel: "Filial 2 #percent%"},
                            { y: '.$grafico2f2.' , name: "Filial 2", color: "#d9534f", indexLabel: "Filial 1 #percent%"}
                            
                        ]
                    }
                    ]
                });

                grafico_pizza2.render();

                grafico_pizza3 = new CanvasJS.Chart("grafico_pizza3",{

                    data: [
                    {
                        type: "pie",
                        indexLabelFontColor: "black",    
                        indexLabelLineColor: "darkgrey",
                        indexLabelFontWeight: "bold",
                        indexLabelFontSize: 15,
                        indexLabelFontFamily: "Garamond",
                        toolTipContent: "{name} #percent% ({y})", 
                        indexLabel: "{name} #percent% ({y})", 
                        showInLegend: true,
                        dataPoints: [
                            { y: 1 , name: "Filial 1", color: "#5cb85c", indexLabel: "Filial 2 #percent%"},
                            { y: 2 , name: "Filial 2", color: "#d9534f", indexLabel: "Filial 1 #percent%"}
                            
                        ]
                    }
                    ]
                });

                grafico_pizza3.render();

        }

        </script>

</head>
<body class="container">

    <!-- topo do logo -->
    <div class="row topo altura-topo" style="border-bottom: 1px solid #888888">
        <div class="col-xs-4 logo">
            <img src="'.base_url().'style/pdf/imagens/logo.jpg" width="200" height="55">
        </div>
        <div class="col-xs-4">
        </div>
        <div class="col-xs-4 relatorio-topo">
            RELATÓRIO: MÉDIA DE HORAS TRABALHADAS<br />
            <span class="data-topo">'.date('d/m/Y H:i:s').'</span>
        </div>
    </div>

    <!-- topo da fonte -->
    <div class="row topo">
        <div class="col-xs-2">
        </div>
        <div class="col-xs-8">
            <h5 class="fonte"><i>
                <span style="font-size: 16px">Dashboard de Média de Horas Trabalhadas para o período de '.$de.' a '.$ate.'</span><br/>
                <span style="font-size: 11px">(FONTE: CLARO GN)</span></i>
            </h5>
        </div>
        <div class="col-xs-2">
        </div>
    </div>

    <div class="row div-media-desempenho">

        <!-- div médias gerais -->
        <div class="col-xs-6 media-div " style="display: inline-block">
            <span class="texto-media">
                Médias Gerais por Filial
            </span>
        </div>

        <!-- div desempenho por filial -->

        <div class="col-xs-6 media-div desempenho-div" style="display: inline-block">
            <span class="texto-media">
                Desempenho por Filial
            </span>
        </div>

    </div>

    <div class="row">
        <div class="col-lg-6" style="display: inline-block">
            <table class="table" style="margin-top: 1px">
                <tr>
                    <th class=""></th>
                    <th class="" style="text-align: center">Filial 1</th>
                    <th class="" style="text-align: center">Filial 2</th>
                    <th class="" style="text-align: center">Filial 1 e 2</th>
                </tr>
                <tr>
                    <th>Horas Trabalhadas</th>
                    <td>'.$tempo_medio_f1.'</td>
                    <td>'.$tempo_medio_f2.'</td>
                    <td>'.$tempo_medio.'</td>
                </tr>
                <tr>
                    <th>Primeiro Check-in</th>
                    <td>'.$entrou_mediaf1.'</td>
                    <td>'.$entrou_mediaf2.'</td>
                    <td>'.$entrou_media.'</td>
                </tr>
                <tr>
                    <th>Último Check-out</th>
                    <td>'.$saiu_mediaf1.'</td>
                    <td>'.$saiu_mediaf2.'</td>
                    <td>'.$saiu_media.'</td>
                </tr>
            </table>
        </div>

        <div class="col-xs-6 graficos-0" style="display: inline-block">
            <div class="col-xs-6 graficos-1" style="display: inline-block">
                <div id="grafico_pizza" style="width:270px; height: 160px;"></div>
            </div>
            <div class="col-xs-6 graficos-2" style="display: inline-block">
                <div id="grafico_pizza2" style="width:270px; height: 160px;"></div>
            </div>
        </div>
    </div>

    <div class="row">

        <!-- Resultados finas por filial -->
        <div class="col-xs-6 media-div" style="display: inline-block">
            <span class="texto-media">
                Resultados Finais por Filial
            </span>
        </div>

        <!-- continuação div desempenho por filial -->

        <div class="col-xs-6 media-div" style="display: inline-block; border-top: 1px solid white">

        </div>

    </div>

    <div class="row">
        <div class="col-xs-6" style="display: inline-block">
            <table class="table">
                <tr>
                    <th class=""></th>
                    <th class="" style="text-align: center">Filial 1</th>
                    <th class="" style="text-align: center">Filial 2</th>
                    <th class="" style="text-align: center">Filial 1 e 2</th>
                </tr>
                <tr>
                    <th>Quantidade de<br/>Check-out</th>
                    <td>'.$checkouts_f1.'</td>
                    <td>'.$checkouts_f2.'</td>
                    <td>'.($checkouts_f1 + $checkouts_f2).'</td>
                </tr>
                <tr>
                    <th>Desfalque de<br/>Horas</th>
                    <td>'.$def_f1.'</td>
                    <td>'.$def_f2.'</td>
                    <td>'.$def_media.'</td>
                </tr>
            </table>
        </div>

        <div class="col-lg-6 graficos-0" style="display: inline-block">
            <div class="col-xs-6 graficos-3" style="display: inline-block;">
                <div id="grafico_pizza3" style="width:270px; height: 160px;"></div>
            </div>
        </div>
    </div>

    <div class="row resultado-gn">
        <div class="col-xs-12 media-div" style="display: inline-block">
            <span class="texto-media">
                Resultado por GN
            </span>
        </div>
    </div>

    <div class="row" style="border-bottom: 1px solid black">
        <div class="col-xs-6 filial-1" style="border-right: 1px solid black; display: inline-block">
            <span class="texto-media-titulos">
                Filial 1
            </span>
        </div>

        <div class="col-xs-6 filial-2" style="display: inline-block">
            <span class="texto-media-titulos">
                Filial 2
            </span>
        </div>
    </div>

    <!-- div das tabelas -->
    <div class="row">

        <div class="col-xs-6 table-1" style="border-right: 1px solid black; display: inline-block">
            <table>
                <!-- linha titulos -->
                <tr>
                    <th class="col-xs-2" style="text-align: center">
                        GN
                    </th>
                    <th class="col-xs-2" style="text-align: center">
                            Média<br/>Check-in
                    </th>
                    <th class="col-xs-2" style="text-align: center">
                            Média<br/>Check-out
                    </th>
                    <th class="col-xs-2" style="text-align: center">
                            Desf. Horas<br/>Trabalhadas
                    </th>
                    <th class="col-xs-2" style="text-align: center">
                            Horas<br/>Trabalhadas
                    </th>
                    <th class="col-xs-2" style="text-align: center">
                            Check-out<br/>Total
                    </th>
                </tr>';

                foreach ($gns_f1 as $gn_f1) {



                    $html .= '
                            <tr>
                            <th class="col-xs-2">
                                '.strtoupper($gn_f1->usuario).'
                            </th>
                            <td class="col-xs-2">
                                '.$gn_f1->entrou.'
                            </td>
                            <td class="col-xs-2">
                                '.$gn_f1->saiu.'
                            </td>
                            <td class="col-xs-2">
                                '.$gn_f1->tempo_def.'
                            </td>
                            <td class="col-xs-2">
                                '.$gn_f1->tempo_trab.'
                            </td>
                            <td class="col-xs-2">
                                '.$gn_f1->checkouts.'
                            </td>
                        </tr>
                        ';
                }
            $html .= '</table>
        </div>
        <div class="col-xs-6 table-1" style="display: inline-block">
            <table>
                <!-- linha titulos -->
                <tr>
                    <th class="col-xs-2" style="text-align: center">
                        GN
                    </th>
                    <th class="col-xs-2" style="text-align: center">
                        Média<br/>Check-in
                    </th>
                    <th class="col-xs-2" style="text-align: center">
                        Média<br/>Check-out
                    </th>
                    <th class="col-xs-2" style="text-align: center">
                        Desf. Horas<br/>Trabalhadas
                    </th>
                    <th class="col-xs-2" style="text-align: center">
                        Horas<br/>Trabalhadas
                    </th>
                    <th class="col-xs-2" style="text-align: center">
                        Check-out<br/>Total
                    </th>
                </tr>';

                foreach ($gns_f2 as $gn_f2) {
                    $html .= '
                            <tr>
                            <th class="col-xs-2">
                                '.strtoupper($gn_f2->usuario).'
                            </th>
                            <td class="col-xs-2">
                                '.$gn_f2->entrou.'
                            </td>
                            <td class="col-xs-2">
                                '.$gn_f2->saiu.'
                            </td>
                            <td class="col-xs-2">
                                '.$gn_f2->tempo_def.'
                            </td>
                            <td class="col-xs-2">
                                '.$gn_f2->tempo_trab.'
                            </td>
                            <td class="col-xs-2">
                                '.$gn_f2->checkouts.'
                            </td>
                        </tr>
                        ';
                }
            $html .= '</table>
        </div>
    </div>

</body>
</html>';

echo $html;
die();

require_once('autoload.inc.php');
$dompdf = new Dompdf\Dompdf();
$dompdf->load_html($html);
$dompdf->set_paper('A4','landscape');
$dompdf->render();
$dompdf->stream("media_horas-".date('d-m-Y_H-i-s').".pdf");