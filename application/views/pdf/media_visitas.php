<?php 
date_default_timezone_set('America/Sao_Paulo');
// Convert to PDF
$html = '<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Claro GN</title>

    <!-- Bootstrap -->
    <script type="text/javascript" src="'.base_url().'style/js/canvasjs.min.js"></script>
    <script type="text/javascript" src="'.base_url().'style/js/jquery.js"></script>
    <link href="'.base_url().'style/pdf/css/bootstrap.css" rel="stylesheet">
    <link href="'.base_url().'style/pdf/estilo_media_visitas.css" rel="stylesheet">
    <link href="'.base_url().'style/pdf/estilo_relatorio.css" rel="stylesheet">

    <script type="text/javascript">

       print();

        var curvaa;
        var curvab;
        var curvac;

        var grafico_pizza;

        window.onload = function () {
            //Total
            curvaa = new CanvasJS.Chart("curvaa", {

                      data: [

                      {
                        indexLabel: "{y} ({percentual} %)", 
                        indexLabelFontColor: "black",    
                        indexLabelLineColor: "darkgrey",
                        indexLabelFontWeight: "bold",
                        showInLegend: false,
                        dataPoints: [
                            {y: '.$curvas_filiais->fora_carteira_A1.', label: "Filial 1", color : "#38597A", percentual: "'.percentual(($curvas_filiais->fora_carteira_A1 + $curvas_filiais->fora_carteira_A2),$curvas_filiais->fora_carteira_A1).'"},
                            {y: '.$curvas_filiais->fora_carteira_A2.', label: "Filial 2", color : "#FF8000", percentual: "'.percentual(($curvas_filiais->fora_carteira_A1 + $curvas_filiais->fora_carteira_A2),$curvas_filiais->fora_carteira_A2).'"}
                        ]
                      }
                      ]
                    });

            curvaa.render();

            //Filial 1
            curvab = new CanvasJS.Chart("curvab", {

                      data: [

                      {
                        indexLabel: "{y} ({percentual} %)", 
                        indexLabelFontColor: "black",    
                        indexLabelLineColor: "darkgrey",
                        indexLabelFontWeight: "bold",
                        showInLegend: false,
                        dataPoints: [
                             {y: '.$curvas_filiais->fora_carteira_B1.', label: "Filial 1", color : "#38597A", percentual: "'.percentual(($curvas_filiais->fora_carteira_B1 + $curvas_filiais->fora_carteira_B2),$curvas_filiais->fora_carteira_B1).'"},
                            {y: '.$curvas_filiais->fora_carteira_B2.', label: "Filial 2", color : "#FF8000", percentual: "'.percentual(($curvas_filiais->fora_carteira_B1 + $curvas_filiais->fora_carteira_B2),$curvas_filiais->fora_carteira_B2).'"}
                        ]
                      }
                      ]
                    });

            curvab.render();

            //Filial 2
            curvac = new CanvasJS.Chart("curvac", {

                      data: [

                      {
                        indexLabel: "{y} ({percentual} %)", 
                        indexLabelFontColor: "black",    
                        indexLabelLineColor: "darkgrey",
                        indexLabelFontWeight: "bold",
                        showInLegend: false,
                        dataPoints: [
                            {y: '.$curvas_filiais->fora_carteira_C1.', label: "Filial 1", color : "#38597A", percentual: "'.percentual(($curvas_filiais->fora_carteira_C1 + $curvas_filiais->fora_carteira_C2),$curvas_filiais->fora_carteira_C1).'"},
                            {y: '.$curvas_filiais->fora_carteira_C2.', label: "Filial 2", color : "#FF8000", percentual: "'.percentual(($curvas_filiais->fora_carteira_C1 + $curvas_filiais->fora_carteira_C2),$curvas_filiais->fora_carteira_C2).'"}
                        ]
                      }
                      ]
                    });

            curvac.render();

            grafico_pizza = new CanvasJS.Chart("grafico_pizza",{

                data: [
                {
                    type: "pie",
                    indexLabelFontColor: "black",    
                    indexLabelLineColor: "darkgrey",
                    indexLabelFontWeight: "bold",
                    indexLabelFontSize: 15,
                    indexLabelFontFamily: "Garamond",
                    toolTipContent: "{name} #percent% ({y})", 
                    indexLabel: "{name} #percent% ({y})", 
                    showInLegend: true,
                    dataPoints: [
                        { y: '.$media_f1->visitas.' , name: "Filial 1", color: "#5cb85c", indexLabel: "Filial 2 #percent% ({y} Visitas)"},
                        { y: '.$media_f2->visitas.' , name: "Filial 2", color: "#d9534f", indexLabel: "Filial 1 #percent% ({y} Visitas)"}
                        
                    ]
                }
                ]
            });

            grafico_pizza.render();

         }
    
</script>

<style>

    @media print {
        .page-break { display: block; page-break-before: always; }
    }

</style>

</head>
<body class="container">
    <!-- topo do logo -->
    <div class="row topo altura-topo" style="border-bottom: 1px solid #888888;">
        <div class="col-lg-4 logo" style="display: inline-block">
        <img src="'.base_url().'style/pdf/imagens/logo.jpg" width="200" height="55">
        </div>
        <div class="col-lg-4" style="display: inline-block">
        </div>
        <div class="col-lg-4 relatorio-topo" style="display: inline-block">
            RELATÓRIO: MÉDIA DE VISITAS<br />
            <span class="data-topo">'.date('d/m/Y H:i:s').'</span>
        </div>
    </div>

    <!-- topo da fonte -->
    <div class="row topo">
        <div class="col-lg-2">
        </div>
        <div class="col-lg-8">
            <h5 class="fonte"><i>
                <span style="font-size: 16px">Dashboard de Média de Visitas para o período de '.$de.' a '.$ate.'</span><br/>
                <span style="font-size: 11px">(FONTE: CLARO GN)</span></i>
            </h5>
        </div>
    </div>';

    $html .= '<div class="row div-media-filial media-1">

        <!-- div médias gerais -->
        <div class="col-xs-6 media-div" style="display: inline-block">
            <span class="texto-media">
                Médias Gerais por Filial
            </span>
        </div>

        <!-- div desempenho por filial -->

        <div class="col-lg-6 media-div desempenho-div" style="display: inline-block">
            <span class="texto-media">
                Desempenho por Filial
            </span>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-6" style="display: inline-block">
            <table class="table" style="margin-top: 1px; text-align: center">

                <!-- tabela titulos -->
                <tr style="border-bottom: 1px solid black">
                    <th class="col-xs-3"></th>
                    <th class="col-xs-3" style="text-align: center"><span class="texto-media-titulos">Filial 1</span></th>
                    <th class="col-xs-3" style="text-align: center"><span class="texto-media-titulos">Filial 2</span></th>
                    <th class="col-xs-3" style="text-align: center"><span class="texto-media-titulos">Filial 1 e 2</span></th>
                </tr>

                <!-- tabela média de visitas -->
                <tr style="border-bottom: 1px solid black">
                    <th class="col-xs-3">Média de Visitas</th>
                    <td class="col-xs-3">'.$media_f1->media_checkout.'</td>
                    <td class="col-xs-3">'.$media_f2->media_checkout.'</td>
                    <td class="col-xs-3">'.round((($media_f1->media_checkout + $media_f2->media_checkout) / 2),2).'</td>
                </tr>

                <!-- tabela total de visitas -->
                <tr style="border-bottom: 1px solid black">
                    <th class="col-xs-3">Total de Visitas</th>
                    <td class="col-xs-3">'.$media_f1->visitas.'</td>
                    <td class="col-xs-3">'.$media_f2->visitas.'</td>
                    <td class="col-xs-3">'.round((($media_f1->visitas + $media_f2->visitas) / 2),2).'</td>
                </tr>

                <!-- tabela visitas fora da carteira -->
                <tr style="border-bottom: 1px solid black">
                    <th class="col-xs-3">Visitas fora de carteira</th>
                    <td class="col-xs-3">'.$media_f1->fora_carteira.'</td>
                    <td class="col-xs-3">'.$media_f2->fora_carteira.'</td>
                    <td class="col-xs-3">'.round((($media_f1->fora_carteira + $media_f2->fora_carteira) / 2),2).'</td>
                </tr>
            </table>

            <table class="table" style="text-align: center">

                <!-- tabela titulos -->
                <tr style="border-bottom: 1px solid black">
                    <th class="col-xs-3" style="text-align: center"><span class="texto-media-titulos">%</span></th>
                    <th class="col-xs-3" style="text-align: center"><span class="texto-media-titulos">Filial 1</span></th>
                    <th class="col-xs-3" style="text-align: center"><span class="texto-media-titulos">Filial 2</span></th>
                    <th class="col-xs-3"></th>
                </tr>


                <!-- tabela total de visitas -->
                <tr style="border-bottom: 1px solid black">
                    <th class="col-xs-3">Total de Visitas</th>
                     <td class="col-xs-3">'.percentual(($media_f1->visitas + $media_f2->visitas),$media_f1->visitas).'%</td>
                    <td class="col-xs-3">'.percentual(($media_f1->visitas + $media_f2->visitas),$media_f2->visitas).'%</td>
                    <td class="col-xs-3"></td>
                </tr>

                <!-- tabela visitas fora da carteira -->
                <tr style="border-bottom: 1px solid black">
                    <th class="col-xs-3">Visitas fora de carteira</th>
                     <td class="col-xs-3">'.percentual($media_f1->visitas,$media_f1->fora_carteira).'%</td>
                    <td class="col-xs-3">'.percentual($media_f2->visitas,$media_f2->fora_carteira).'%</td>
                    <td class="col-xs-3"></td>
                </tr>
            </table>
        </div>

        <div class="col-lg-6" style="display: inline-block">
            <div class="col-lg-2 legenda-div" style="display: inline-block">

            </div>
            <div class="col-lg-10 grafico-div" style="display: inline-block">
                Grafico desempenho final
                <div id="grafico_pizza" style="height: 280px; width: 90%;"></div>
            </div>
        </div>
    </div>

    <div class="row div-media-filial">
        <div class="col-lg-12 media-div" style="display: inline-block"><span class="texto-media">Comparativo PDVs nao visitados por Curva x Filial</span> </div>
    </div>

    <div class="row">
        <div class="col-lg-8" style="display: inline-block">
            <div class="col-lg-4 curva-a" style="display: inline-block">
                Gráfico Curva A
            <div id="curvaa" style="height: 280px; width: 90%;"></div>
            </div>
            <div class="col-lg-4 curva-b" style="display: inline-block">
            Gráfico Curva B
            <div id="curvab" style="height: 280px; width: 90%;"></div>
            </div>
            <div class="col-lg-4 curva-c" style="display: inline-block">
                Gráfico Curva C
            <div id="curvac" style="height: 280px; width: 90%;"></div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="col-lg-12 ranking-div" style="display: inline-block">
                <table class="table" style="text-align: center">
                    <tr>
                        <th><span class="texto-media">Ranking Redes menor número de visitas</span></th>
                    </tr>';

                    foreach ($cinco_menores as $key => $value) {
                        $html .= '<tr>
                            <td><span class="texto-media">'.($key + 1).'º</span> '.$value->rede.' ('.$value->visitas.' visitas)</td>
                        </tr>';
                    }
                $html .= '</table>
            </div>
        </div>
    </div>

    <div class="row final-div">
        <div class="col-lg-6">
            <div class="col-lg-4" style="display: inline-block">
                <span class="texto-media">
                    Curva A: <span style="font-weight: 100">700K<br/></span>
                </span>
                <span class="texto-media">
                    Curva B: <span style="font-weight: 100">300K - 699k<br/></span>
                </span>
                <span class="texto-media">
                    Curva C: <span style="font-weight: 100">0 - 299K<br/></span>
                </span>
            </div>
            <div class="col-lg-4" style="display: inline-block"></div>
            <div class="col-lg-4" style="display: inline-block">
                <div class="col-lg-5 " style="display: inline-block">
                    <span  style="width: 30px; height: 10px;border-left: 15px solid #000000;"></span>
                    <span class="texto-media">&nbsp; Filial 1</span>
                </div>
                <div class="col-lg-6" style="display: inline-block">
                    <span style="width: 30px; height: 10px; border-left: 15px solid #a9a9a9;"></span>
                    <span class="texto-media">&nbsp; Filial 2</span>
                </div>
            </div>
        </div>
    </div>
    <br>
    <br>
    <br>
    <br>
    <div class="page-break"></div>
    <br>
    <br>
    <br>
    <br>
    <hr>';

    //tabelas


     $html .= '<!-- div resultados por gn 1 -->
    <div class="row topo borda-gn">
        <div class="col-lg-12 media-div">
            <span class="texto-media">
                Resultados por GN
            </span>
        </div>
    </div>

    <div class="row topo">
        <div class="col-lg-12">
            <span class="texto-media">
                Filial 1
            </span>
        </div>
    </div>

    <table class="table table-1">

        <!-- tr do titulo -->
        <tr style="border-bottom: 1px solid black">
            <th class="col-lg-3" style="text-align: center">
                <span class="texto-media">
                GN
                </span>
            </th>
            <th class="col-lg-1" style="text-align: center">
                <span class="texto-media-titulos">
                    Total<br />PDVs
                </span>
            </th>
            <th class="col-lg-1" style="text-align: center">
                <span class="texto-media-titulos">
                    Total<br />Visitas
                </span>
            </th>
            <th class="col-lg-1" style="text-align: center">
                <span class="texto-media-titulos">
                    Qtd. PDVs<br />não visitas
                </span>
            </th>
            <th class="col-lg-1" style="border-right: 1px solid black; text-align: center">
                <span class="texto-media-titulos">
                    % PDVs na carteira<br />não visitados
                </span>
            </th>
            <th class="col-lg-1" style="text-align: center">
                <span class="texto-media-titulos">
                    Qtd. Curva A<br />não visitada
                </span>
            </th>
            <th class="col-lg-1" style="text-align: center">
                <span class="texto-media-titulos">
                     Qtd. Curva B<br />não visitada
                </span>
            </th>
            <th class="col-lg-1" style="border-right: 1px solid black; text-align: center">
                <span class="texto-media-titulos">
                     Qtd. Curva C<br />não visitada
                </span>
            </th>
            <th class="col-lg-1" style="text-align: center">
                <span class="texto-media-titulos">
                    Média do GN<br />no período
                </span>
            </th>
            <th class="col-lg-1" style="text-align: center">
                <span class="texto-media-titulos">
                    Visitas a PDVs<br />fora da carteira
                </span>
            </th>
        </tr>';

            foreach ($lista_f1 as $resultado) {

               $html .= '<tr>
                    <th class="col-lg-3">
                        <span class="texto-media">
                            '.$resultado->usuario.'
                        </span>
                    </th>
                    <td class="col-lg-1" style="text-align: center">
                        <span>
                            '.$resultado->total.'
                        </span>
                    </td>
                    <td class="col-lg-1" style="text-align: center">
                        <span>
                            '.$resultado->visitas.'
                        </span>
                    </td>
                    <td class="col-lg-1" style="text-align: center">
                        <span>
                            '.$resultado->n_visitados.'
                        </span>
                    </td>
                    <td class="col-lg-1" style="border-right: 1px solid black; text-align: center">
                        <span>
                            '.percentual($resultado->total,$resultado->n_visitados).'
                        </span>
                    </td>
                    <td class="col-lg-1" style="text-align: center">
                        <span>
                            '.$resultado->n_curva_a.'
                        </span>
                    </td>
                    <td class="col-lg-1" style="text-align: center">
                        <span>
                            '.$resultado->n_curva_b.'
                        </span>
                    </td>
                    <td class="col-lg-1" style="border-right: 1px solid black; text-align: center">
                        <span>
                            '.$resultado->n_curva_c.'
                        </span>
                    </td>
                    <td class="col-lg-1" style="text-align: center">
                        <span>
                            '.$resultado->media_checkout.'
                        </span>
                    </td>
                    <td class="col-lg-1" style="text-align: center">
                        <span>
                            '.$resultado->fora_carteira.'
                        </span>
                    </td>
                </tr>';

            }

        $html .= '</table>
    <br/><br/><br/>
    <div class="row topo filial-2">
        <div class="col-lg-12">
            <span class="texto-media">
                Filial 2
            </span>
        </div>
    </div>

    <table class="table">

        <!-- tr do titulo -->
        <tr style="border-bottom: 1px solid black">
            <th class="col-lg-3" style="text-align: center">
                <span class="texto-media">
                GN
                </span>
            </th>
            <th class="col-lg-1" style="text-align: center">
                <span class="texto-media-titulos">
                    Total<br />PDVs
                </span>
            </th>
            <th class="col-lg-1" style="text-align: center">
                <span class="texto-media-titulos">
                    Total<br />Visitas
                </span>
            </th>
            <th class="col-lg-1" style="text-align: center">
                <span class="texto-media-titulos">
                    Qtd. PDVs<br />não visitas
                </span>
            </th>
            <th class="col-lg-1" style="border-right: 1px solid black; text-align: center">
                <span class="texto-media-titulos">
                    % PDVs na carteira<br />não visitados
                </span>
            </th>
            <th class="col-lg-1" style="text-align: center">
                <span class="texto-media-titulos">
                    Qtd. Curva A<br />não visitada
                </span>
            </th>
            <th class="col-lg-1" style="text-align: center">
                <span class="texto-media-titulos">
                     Qtd. Curva B<br />não visitada
                </span>
            </th>
            <th class="col-lg-1" style="border-right: 1px solid black; text-align: center">
                <span class="texto-media-titulos">
                     Qtd. Curva C<br />não visitada
                </span>
            </th>
            <th class="col-lg-1" style="text-align: center">
                <span class="texto-media-titulos">
                    Média do GN<br />no período
                </span>
            </th>
            <th class="col-lg-1" style="text-align: center">
                <span class="texto-media-titulos">
                    Visitas a PDVs<br />fora da carteira
                </span>
            </th>
        </tr>';

            foreach ($lista_f2 as $resultado) {

               $html .= '<tr>
                    <th class="col-lg-3">
                        <span class="texto-media">
                            '.$resultado->usuario.'
                        </span>
                    </th>
                    <td class="col-lg-1" style="text-align: center">
                        <span>
                            '.$resultado->total.'
                        </span>
                    </td>
                    <td class="col-lg-1" style="text-align: center">
                        <span>
                            '.$resultado->visitas.'
                        </span>
                    </td>
                    <td class="col-lg-1" style="text-align: center">
                        <span>
                            '.$resultado->n_visitados.'
                        </span>
                    </td>
                    <td class="col-lg-1" style="border-right: 1px solid black; text-align: center">
                        <span>
                            '.percentual($resultado->total,$resultado->n_visitados).'
                        </span>
                    </td>
                    <td class="col-lg-1" style="text-align: center">
                        <span>
                            '.$resultado->n_curva_a.'
                        </span>
                    </td>
                    <td class="col-lg-1" style="text-align: center">
                        <span>
                            '.$resultado->n_curva_b.'
                        </span>
                    </td>
                    <td class="col-lg-1" style="border-right: 1px solid black; text-align: center">
                        <span>
                            '.$resultado->n_curva_c.'
                        </span>
                    </td>
                    <td class="col-lg-1" style="text-align: center">
                        <span>
                            '.$resultado->media_checkout.'
                        </span>
                    </td>
                    <td class="col-lg-1" style="text-align: center">
                        <span>
                            '.$resultado->fora_carteira.'
                        </span>
                    </td>
                </tr>';

            }

        $gn_atual = 0;

        foreach ($analitico_pdvs_visitados as $key => $visitados) {

            if ($visitados->id_usuario != $gn_atual) {
                
                if($gn_atual > 0){ //Caso esteja começando um outro.

                    $html .= '</table>';

                }

                $gn_atual = $visitados->id_usuario;

                $html .= '</table>
                              <!-- div resultados por gn 1 -->
                            <div class="row topo borda-gn">
                                <div class="col-lg-12 media-div">
                                    <span class="texto-media">
                                        Resultado por GN '.$visitados->usuario.' Filial: '.$visitados->filial_responsavel.'
                                    </span>
                                </div>
                            </div>

                            <div class="row topo">
                                <div class="col-lg-12">
                                    <span class="texto-media">
                                        Total de PDVs Visitados
                                    </span>
                                </div>
                            </div>

                            <table class="table table-1">

                            <!-- tr do titulo -->
                            <tr style="border-bottom: 1px solid black">
                                <th class="col-lg-3" style="text-align: center">
                                    <span class="texto-media-titulos">
                                        REDE
                                    </span>
                                </th>
                                <th class="col-lg-3" style="text-align: center">
                                    <span class="texto-media-titulos">
                                        CNPJ
                                    </span>
                                </th>
                                <th class="col-lg-3" style="text-align: center">
                                    <span class="texto-media-titulos">
                                        ENDEREÇO
                                    </span>
                                </th>
                                <td class="col-lg-3" style=" text-align: center">
                                    <span class="texto-media-titulos">
                                        CURVA
                                    </span>
                                </th>

                            </tr>
                            <!-- tr do loja1 -->
                            <tr>
                                <th class="col-lg-3">
                                    <span class="texto-media">
                                        '.$visitados->rede.'
                                    </span>
                                </th>

                                <td class="col-lg-3" style="text-align: center">
                                    <span>
                                         '.$visitados->cnpj.'
                                    </span>
                                </td>
                                <td class="col-lg-3" style="text-align: center">
                                    <span>
                                        '.$visitados->lougradouro.' '.$visitados->bairro.'
                                    </span>
                                </td>
                                </td>
                                <td class="col-lg-3" style=" text-align: center">
                                    <span>
                                        '.$visitados->curva.'
                                    </span>
                                </td>

                            </tr>';


            } else {

                $html .= '<tr>
                            <th class="col-lg-3">
                                <span class="texto-media">
                                    '.$visitados->rede.'
                                </span>
                            </th>

                            <td class="col-lg-3" style="text-align: center">
                                <span>
                                     '.$visitados->cnpj.'
                                </span>
                            </td>
                            <td class="col-lg-3" style="text-align: center">
                                <span>
                                    '.$visitados->lougradouro.' '.$visitados->bairro.'
                                </span>
                            </td>
                            </td>
                            <td class="col-lg-3" style=" text-align: center">
                                <span>
                                    '.$visitados->curva.'
                                </span>
                            </td>

                        </tr>';

            }


        }


function percentual($total = null, $quantidade = null){
            
            if ($quantidade > 0 && $total > 0) {
                return round((($quantidade * 100) / $total),2);
            } else {
                return 0;
            }


        }

echo $html;
die();

require_once('autoload.inc.php');
$dompdf = new Dompdf\Dompdf();
$dompdf->load_html($html);
$dompdf->set_paper('A4','landscape');
$dompdf->render();
$dompdf->stream("media_visitas-".date('d-m-Y_H-i-s').".pdf");