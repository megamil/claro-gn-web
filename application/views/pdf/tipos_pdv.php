<?php 
echo '<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Claro GN</title>

    <!-- Bootstrap -->
    <script type="text/javascript" src="'.base_url().'style/js/canvasjs.min.js"></script>
    <script type="text/javascript" src="'.base_url().'style/js/jquery.js"></script>
    <link href="'.base_url().'style/pdf/css/bootstrap.css" rel="stylesheet">
    <link href="'.base_url().'style/pdf/estilo_analise_pdv.css" rel="stylesheet">
   

</head>
<body class="container">

    <!-- topo do logo -->
    <div class="row topo altura-topo" style="border-bottom: 1px solid #888888">
        <div class="col-lg-4" style="display: inline-block; position: relative">
            <img src="'.base_url().'style/pdf/imagens/logo.jpg" width="200" height="55">
        </div>
        <div class="col-lg-4" style="display: inline-block;position: relative">
        </div>
        <div class="col-lg-4 relatorio-topo" style="display: inline-block; text-align: right; position: relative">
            RELATÓRIO: TIPOS DE PDVs<br />
            <span class="data-topo">'.date('d/m/Y H:i:s').'</span>
        </div>
    </div>

    <!-- topo da fonte -->
    <div class="row topo">
        <div class="col-lg-2">
        </div>
        <div class="col-lg-8">
            <h5 class="fonte"><i>
                <span style="font-size: 16px">Dashboard de Tipos de PDVs para o período de '.$de.' a '.$ate.'</span><br/>
                <span style="font-size: 11px">(FONTE: CLARO GN)</span></i>
            </h5>
        </div>
        <div class="col-lg-2">
        </div>
    </div>

    <div class="row borda-print">

        <!-- div médias gerais -->
        <div class="col-lg-6 media-div" style="display: inline-block">
            <span class="texto-media">
                Resultado Finais por Filial
            </span>
        </div>



    </div>

    <!-- resultado final -->
    <div class="row">
        <div class="col-lg-6" style="display: inline-block">
            <table class="table">
                <tr>
                    <th class="col-lg-3"></th>
                    <th class="col-lg-3" style="text-align: center">Filial 1</th>
                    <th class="col-lg-3" style="text-align: center">Filial 2</th>
                    <th class="col-lg-3" style="text-align: center">Filial 1 e 2</th>
                </tr>
                <tr style="text-align: center">
                    <th>Mobile</th>
                    <td>'.$filial1_mobile.'</td>
                    <td>'.$filial2_mobile.'</td>
                    <td>'.$total_mobile.'</td>
                </tr>
                <tr style="text-align: center">
                    <th>Telefonia</th>
                    <td>'.$filial1_telefonia.'</td>
                    <td>'.$filial2_telefonia.'</td>
                    <td>'.$total_telefonia.'</td>
                </tr>
                <tr style="text-align: center">
                    <th>Aberto</th>
                    <td>'.$filial1_aberto.'</td>
                    <td>'.$filial2_aberto.'</td>
                    <td>'.$total_aberto.'</td>
                </tr>
            </table>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-6 borda-resultado" style="display: inline-block">
            <div class="col-lg-6" style="display: inline-block"></div>
            <div class="col-lg-3 borda" style=" display: inline-block; text-align: center"/>
                <span class="texto-media">
                    Total Geral 
                </span>
            </div>
            <div class="col-lg-3 borda" style=" display: inline-block; text-align: center"> '.$total_geral.'</div>
        </div>
    </div>

    <br/><br/>

    <!-- div graficos -->
    <div class="row topo media-div">
        <div class="col-lg-12">
            <div class="col-lg-4" style="display:inline-block;"><span class="texto-media">Filial 1 e 2</span></div>
            <div class="col-lg-4 grafico2" style="display: inline-block"><span class="texto-media">Filial 1</span></div>
            <div class="col-lg-4 grafico3" style="display: inline-block"><span class="texto-media">Filial 2</span></div>
        </div>
    </div>

    <div class="row" style="margin-top: 25px">
        <div class="col-lg-12">
            <div class="col-lg-4" style="display: inline-block">
                <div id="graficoTotal" style="height: 280px; width: 90%;"></div>
            </div>
            <div class="col-lg-4 grafico2" style="display: inline-block">
                <div id="graficoFilial1" style="height: 280px; width: 90%;"></div>
            </div>
            <div class="col-lg-4 grafico3" style="display: inline-block">
                <div id="graficoFilial2" style="height: 280px; width: 90%;"></div>
            </div>
        </div>
    </div>

    <div class="row" style="margin-top: 25px; border: 1px solid black; padding: 12px">
        <div class="col-lg-12">
            <span class="col-lg-1 cor1" style="background-color: #000000; width: 30px; height: 30px; display: inline-block"></span>
            <div class="col-lg-1 titulo-legenda" style="display: inline-block"><span class="texto-media" style="margin-left: -10px">Mobile</span></div>
            <div class="col-lg-2 " style="display: inline-block"><span  class="fonte-legenda">PDVs com layout de ilha no setor de telefonia</span> </div>
            <span class="col-lg-1 cor2" style="background-color: #737373; width: 30px; height: 30px; display: inline-block"></span>
            <div class="col-lg-1 titulo-legenda" style="display: inline-block"><span class="texto-media" style="margin-left: -10px; display: inline-block">Telefonia</span></div>
            <div class="col-lg-2" style="display: inline-block"><span class=" fonte-legenda" >PDVs com balcão</span></div>
            <span class="col-lg-1 cor3" style="background-color: #b7b7b7; width: 30px; height: 30px; display: inline-block"></span>
            <div class="col-lg-1 titulo-legenda" style="display: inline-block"><span class="texto-media" style="margin-left: -10px">Aberto</span></div>
            <div class="col-lg-2 " style="display: inline-block"><span class="fonte-legenda" style=" margin-left: -10px">PDVs sem espaço dedicado <br/>para telefonia</span></div>
        </div>
    </div>

<script type="text/javascript">

       print();

        var graficoTotal;
        var graficoFilial1;
        var graficoFilial2;

        window.onload = function () {
            //Total
            graficoTotal = new CanvasJS.Chart("graficoTotal", {

                      data: [

                      {
                        indexLabel: "{y} ({percentual} %)", 
                        indexLabelFontColor: "black",    
                        indexLabelLineColor: "darkgrey",
                        indexLabelFontWeight: "bold",
                        showInLegend: false,
                        dataPoints: [
                            {y: '.$total_mobile.', label: "Mobile", color : "#38597A", percentual: "'.$total_perc_m.'"},
                            {y: '.$total_telefonia.', label: "Telefonia", color : "#D9A300", percentual: "'.$total_perc_t.'"},
                            {y: '.$total_aberto.', label: "Aberto", color : "#FF8000", percentual: "'.$total_perc_a.'"}
                        ]
                      }
                      ]
                    });

            graficoTotal.render();

            //Filial 1
            graficoFilial1 = new CanvasJS.Chart("graficoFilial1", {

                      data: [

                      {
                        indexLabel: "{y} ({percentual} %)", 
                        indexLabelFontColor: "black",    
                        indexLabelLineColor: "darkgrey",
                        indexLabelFontWeight: "bold",
                        showInLegend: false,
                        dataPoints: [
                            {y: '.$filial1_mobile.', label: "Mobile", color : "#38597A", percentual: "'.$f1_perc_m.'"},
                            {y: '.$filial1_telefonia.', label: "Telefonia", color : "#D9A300", percentual: "'.$f1_perc_t.'"},
                            {y: '.$filial1_aberto.', label: "Aberto", color : "#FF8000", percentual: "'.$f1_perc_a.'"}
                        ]
                      }
                      ]
                    });

            graficoFilial1.render();

            //Filial 2
            graficoFilial2 = new CanvasJS.Chart("graficoFilial2", {

                      data: [

                      {
                        indexLabel: "{y} ({percentual} %)", 
                        indexLabelFontColor: "black",    
                        indexLabelLineColor: "darkgrey",
                        indexLabelFontWeight: "bold",
                        showInLegend: false,
                        dataPoints: [
                            {y: '.$filial2_mobile.', label: "Mobile", color : "#38597A", percentual: "'.$f2_perc_t.'"},
                            {y: '.$filial2_telefonia.', label: "Telefonia", color : "#D9A300", percentual: "'.$f2_perc_a.'"},
                            {y: '.$filial2_aberto.', label: "Aberto", color : "#FF8000", percentual: "'.$f2_perc_m.'"}
                        ]
                      }
                      ]
                    });

            graficoFilial2.render();

         }
    
</script>

</body>
</html>';