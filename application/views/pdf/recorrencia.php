<?php 
date_default_timezone_set('America/Sao_Paulo');
// Convert to PDF
$html = '
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>RELATÓRIO: ANÁLISE PROMOTOR DE PDV</title>

    <!-- Bootstrap -->
    <link href="'.base_url().'style/pdf/css/bootstrap.css" rel="stylesheet">
    <link href="'.base_url().'style/pdf/estilo.css" rel="stylesheet">
    <link href="'.base_url().'style/pdf/estilo2.css" rel="stylesheet">

    <script type="text/javascript">

       print();

  </script>

</head>
<body class="container">

    <!-- topo do logo -->
    <div class="row topo altura-topo" style="border-bottom: 1px solid #888888">
        <div class="col-lg-4">
            <img src="'.base_url().'style/pdf/imagens/logo.jpg" width="200" height="55">
        </div>
        <div class="col-lg-4">
        </div>
        <div class="col-lg-4 relatorio-topo">
            RELATÓRIO: ANÁLISE PROMOTOR DE PDV<br />
            <span class="data-topo">'.date('d/m/Y H:i:s').'</span>
        </div>
    </div>

    <!-- topo da fonte -->
    <div class="row topo">
        <div class="col-lg-2">
        </div>
        <div class="col-lg-8">
            <h5 class="fonte"><i>
                <span style="font-size: 16px;">Dashboard de Ranking de Visitas a PDVs para o período de '.$de.' a '.$ate.'</span><br/>
                <span style="font-size: 11px">(FONTE: CLARO GN)</span></i>
            </h5>
        </div>
        <div class="col-lg-2">
        </div>
    </div>

        <!-- div da média 1 -->
    <div class="row topo">
        <div class="col-lg-12 media-div">
            <span class="texto-media">
                Ranking de Visitas a PDVs
            </span>
        </div>
    </div>

   <table class="table topo topo-table" style="width: 100%" border="1">
       <thead>
            <tr>
                <th></th>
                <th colspan="3" style="text-align: center;">Lojas Ativas</th>
                <th colspan="3" style="text-align: center;">Lojas Atendidas</th>
                <th colspan="3" style="text-align: center;">Visitas Realizadas</th>
                <th colspan="3" style="text-align: center;">% Rede Atendida</th>
                <th colspan="3" style="text-align: center;">% Não Atendida</th>
            </tr>


            <tr>
                <th style="padding-right: 115px">Rede</th>
                    <th style="text-align: center;">A</th>
                    <th style="text-align: center;">B</th>
                    <th style="text-align: center;">C</th>

                    <th style="text-align: center;">A</th>
                    <th style="text-align: center;">B</th>
                    <th style="text-align: center;">C</th>

                    <th style="text-align: center;">A</th>
                    <th style="text-align: center;">B</th>
                    <th style="text-align: center;">C</th>

                    <th style="text-align: center;">A</th>
                    <th style="text-align: center;">B</th>
                    <th style="text-align: center;">C</th>

                    <th style="text-align: center;">A</th>
                    <th style="text-align: center;">B</th>
                    <th style="text-align: center;">C</th>

            </tr>
            </thead>
            <tbody>';

    $totalizador_a = 0;
    $totalizador_b = 0;
    $totalizador_c = 0;

	foreach ($registros['dados'] as $key => $value) {

		$html .= '<tr style="margin-top: 30px; margin-left: -20%;">
	                <th style="padding-right: 80px">'.$value->rede.'</th>';

	    foreach ($registros['ativos'] as $key => $ativo) {
			
			if($value->rede == $ativo->rede){
				
				$html .= '<td style="text-align: center;">'.$ativo->lojas_ativas_a.'</td>';
				$html .= '<td style="text-align: center;">'.$ativo->lojas_ativas_b.'</td>';
				$html .= '<td style="text-align: center;">'.$ativo->lojas_ativas_c.'</td>';

				$total = $ativo->lojas_ativas_a + $ativo->lojas_ativas_b + $ativo->lojas_ativas_c;

				$atendida_a = percentual($total,$value->lojas_atendidas_a);
				$totalizador_a += $atendida_a;
				$atendida_b = percentual($total,$value->lojas_atendidas_b);
				$totalizador_b += $atendida_b;
				$atendida_c = percentual($total,$value->lojas_atendidas_c);
				$totalizador_c += $atendida_c;

			}

		}    
	                
	    $html .= '
                    <td style="text-align: center;">'.$value->lojas_atendidas_a.'</td>
                    <td style="text-align: center;">'.$value->lojas_atendidas_b.'</td>
                    <td  style="text-align: center;">'.$value->lojas_atendidas_c.'</td>

                    <td style="text-align: center;">'.$value->visitas_realizadas_a.'</td>
                    <td style="text-align: center;">'.$value->visitas_realizadas_b.'</td>
                    <td  style="text-align: center;">'.$value->visitas_realizadas_c.'</td>

                    <td style="text-align: center;">'.number_format($atendida_a, 2, ',', ' ').'%</td>
                    <td style="text-align: center;">'.number_format($atendida_b, 2, ',', ' ').'%</td>
                    <td style="text-align: center;">'.number_format($atendida_c, 2, ',', ' ').'%</td>

                    <td style="text-align: center;">'.number_format((100 - $atendida_a), 2, ',', ' ').'%</td>
                    <td style="text-align: center;">'.number_format((100 - $atendida_b), 2, ',', ' ').'%</td>
                    <td  style="text-align: center;">'.number_format((100 - $atendida_c), 2, ',', ' ').'%</td>
	            </tr>';

	}	

	$geral_atendida = $totalizador_a + $totalizador_b + $totalizador_c;
	$geral_atendida = (($geral_atendida / 3) / count($registros['dados']));

$html .= '
	</tbody>
</table>

<hr>

    <div class="row" style="margin-top: 25px">
        <div class="col-lg-2" style="display: inline-block"></div>
        <div class="col-lg-5" style="display: inline-block">
            <div class="col-lg-4" style="display: inline-block"></div>
            <div class="col-lg-4" style="display: inline-block"></div>
            <div class="col-lg-4 media-rodape" style="text-align: center; font-size: 15px; font-weight: 600; display: inline-block">Média Geral</div>
        </div>
        <div class="col-lg-5" style="display: inline-block">
            <div class="col-lg-6 vr1-rodape" style="text-align: center; font-size: 15px; font-weight: 600; display: inline-block">Atendida: '.number_format(($geral_atendida), 2, ',', ' ').'%</div>
            <div class="col-lg-6 vr2-rodape" style="text-align: center; font-size: 15px; font-weight: 600; display: inline-block">Não Atendida: '.number_format((100 - $geral_atendida), 2, ',', ' ').'%</div>
        </div>
    </div>
   

</body>
</html>';

function percentual($total = null, $quantidade = null){
			
	if ($quantidade > 0 && $total > 0) {
		return (($quantidade * 100) / $total);
	} else {
		return 0;
	}


}

 echo $html;
// die();

// require_once('autoload.inc.php');
// $dompdf = new Dompdf\Dompdf();
// $dompdf->load_html($html);
// $dompdf->set_paper('A4','landscape');
// $dompdf->render();
// $dompdf->stream("recorrencia-".date('d-m-Y_H-i-s').".pdf");

 ?>