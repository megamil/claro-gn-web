<span align="center"><h2>Fique Atento!</h2></span>

<hr>

<?php echo form_open_multipart('controller_atento/novo'); ?>

<div class="mdl-grid">

<div class="mdl-cell mdl-cell--3-col"></div>
  <div class="mdl-cell mdl-cell--4-col">
    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
    <input class="mdl-textfield__input obrigatorio" type="text" id="titulo_atento" name="titulo_atento" aviso="Título" maxlength="140" value="<?php echo $this->session->flashdata('titulo_atento'); ?>">
    <label class="mdl-textfield__label" for="titulo_atento">Título</label>
  </div>
  </div>

  <div class="mdl-cell mdl-cell--3-col">
    <label class="label" for="data_limite">Data Limite</label>
    <input class="mdl-textfield__input mascara_data obrigatorio" type="text" id="data_limite" name="data_limite" aviso="Data Limite" value="<?php echo $this->session->flashdata('data_limite'); ?>">
  </div>

</div>


<div class="mdl-grid">
    
  <div class="mdl-cell mdl-cell--10-col mdl-textfield mdl-js-textfield">
    Descrição: <textarea class="mdl-textfield__input" type="text" rows= "10"  Aviso="Descrição" id="descricao_atento" name="descricao_atento" maxlength="500"><?php echo $this->session->flashdata('descricao_atento'); ?></textarea>
   </div>

  <div class="mdl-cell mdl-cell--2-col" align="center">
    <label class="label" for="imagemdescricao">UPLOAD IMAGEM PARA DESCRIÇÃO</label>
    <input type="file" name="imagemdescricao" id="arquivo">
  </div>

</div>

<div class="mdl-grid">
  <div class="mdl-cell mdl-cell--4-col" style="margin-top: 25px;"></div>
  <div class="mdl-cell mdl-cell--4-col" style="margin-top: 25px;">
    <label class="label" for="imagem">Imagem Referência. (Usada nos Aplicativos)</label>
    <input class="mdl-textfield__input obrigatorio" type="file" aviso="Imagem" name="imagem" maxlength="200">
  </div>
</div>

<div class="mdl-grid">
  <div class="mdl-cell mdl-cell--4-col"></div>
  <div class="mdl-cell mdl-cell--4-col">
    <button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" id="validar_Enviar">
        Enviar Fique Atento
    </button>
  </div>
</div>

<?php echo form_close(); ?>

<script type="text/javascript">

    (function () {
      var container = document.getElementById("descricao_atento").parentNode;
      container.addEventListener('mdl-componentupgraded', function(){
        tinymce.init({
          selector: 'textarea',
      height: 500,
      theme: 'modern',
      plugins: [
        'advlist autolink lists link image charmap print preview hr anchor pagebreak',
        'searchreplace wordcount visualblocks visualchars code fullscreen',
        'insertdatetime media nonbreaking save table contextmenu directionality',
        'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
      ],
      toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
      toolbar2: 'print preview media | forecolor backcolor emoticons | codesample',
      image_advtab: true,
      templates: [
        { title: 'Test template 1', content: 'Test 1' },
        { title: 'Test template 2', content: 'Test 2' }
      ],
      content_css: [
        '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i'
      ]
        });
      }, false);
    })();

    $('#arquivo').change(function(){
      $('form').submit();
    });

</script>