<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Roteiro</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.4 -->
    <link href="<?php echo base_url() ?>stylebootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="<?php echo base_url() ?>stylebootstrap/fonts/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="<?php echo base_url() ?>stylebootstrap/css/bootsz.css" rel="stylesheet" type="text/css" />
    <!-- iCheck -->
    <link href="<?php echo base_url() ?>stylebootstrap/css/blue.css" rel="stylesheet" type="text/css" />

    <link href="<?php echo base_url() ?>stylebootstrap/css/select2.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>style/jquery-ui/jquery-ui.css">

    <!--ICON-->
		<link rel="shortcut icon" href="<?php echo base_url(); ?>stylebootstrap/img/favicon.jpg" type="image/x-icon">

    <script src="<?php echo base_url(); ?>stylebootstrap/js/jquery.js"></script>
    <script src="<?php echo base_url(); ?>stylebootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>stylebootstrap/js/select2.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>style/js/maskedinput.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>style/js/script.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>style/jquery-ui/jquery-ui.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBnSb1dl4CLw1T900XOtc-tIYdyMMvL4RU&libraries=places"></script>

    <!-- AIzaSyD1ZOBUYzpS9BzXlvPQ2BTmZB45ShOK8fU funciona hj...-->

  </head>

	<body>

	<noscript>
		<div style="background-color: red; height: 100%; width: 100%; position: absolute; z-index: 200000;" align="center">
		HABILITE O JAVASCRIPT PARA USAR O SISTEMA!.
			<iframe src="http://enable-javascript.com/pt/" width="100%" height="100%"></iframe>

		</div>
	</noscript> 

  <style type="text/css">
    .footer {
      position: relative;
      bottom: 0;
      width: 100%;
    }

    .novo {
      margin-bottom: 10px;
    }

    .lista{
       margin-bottom: 30px;
    }

    .numerador {
      background-color: black;
      color: white;
      border-radius: 90px;
      margin-left: 5px;
      margin-top: 26px;
      padding: 0px;
      width: 20px;
    }

    .espaco {
      color: white;
      border-radius: 90px;
      margin-left: 5px;
      padding: 0px;
      width: 20px;
    }


    .plus {
      background-color: green;
      color: white;
      border-radius: 90px;
      font-size: 25px !important;
      padding-top: 2px;
      padding-left: 14px;
      width: 40px;
      height: 40px;
    }

    .left{
      margin-right: -30px;
    }

    .right{
      margin-left: -30px;
    }

    .ui-datepicker-header {
     background: red;
     color: #EEE;
   }

   #ui-datepicker-div{
    z-index: 100 !important;
    position: absolute;
   }
  </style>

  <script type="text/javascript">
    
  $(document).ready(function(){

    <?php 
    //Se tiver algum aviso de erro ou sucesso
    if (isset($aviso) && $aviso != "") {
      echo 'alert(\''.$aviso.'\');';
    }

     ?>

    //Habilita aviso sobre o botão, quando tentar enviar com menos de 4 rotas
    $('#enviar').popover();

    var lista = 1; //Quantidade de endereços
    var km = [] //Total de KM
    var tempo = [] //Total de tempo em minutos

    //Clicou para adicionar novo endereço.
    $('#adicionar').click(function(){

      lista += 1;

      if (lista >= 5) { // quando chegar a 5 retira o alerta
        $('#enviar').popover('destroy')
      } else {
        $('#enviar').popover('hide')
      }

      $('#lista').append('<div class="novo"><div class="row"><div class="col-xs-1 numerador" align="center">'+lista+'</div><div class="col-xs-11"><div class="input-group"><input class="form-control address" cod="'+lista+'" id="address'+lista+'" name="address[]" type="search" placeholder="BUSCAR ENDEREÇO"><span class="input-group-addon"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></span></div></div><!-- /.col-lg-6 --></div><div class="row"><div class="col-xs-1 espaco"></div><div class="col-xs-11"><div class="input-group"><input type="hidden" name="tempoGoogle[]" id="tempoGoogle'+lista+'"><input type="hidden" name="km_google[]" id="km_google'+lista+'" value="0"><input class="form-control tempo" name="tempo[]" placeholder="TEMPO (Minutos)" type="tel"><span class="input-group-addon"><span class="glyphicon glyphicon-time" aria-hidden="true"></span></span></div></div></div>');

    });

    var indice;

    $(document).on('change','.address',function(){

      indice = $(this).attr('cod');

      CalculaDistancia(indice);

    });

    function CalculaDistancia(indice) {
            
      if (indice > 1) { //No primeiro endereço não tem o que calcular

        var anterior = indice - 1;

      //Instanciar o DistanceMatrixService
      var service = new google.maps.DistanceMatrixService();
      //executar o DistanceMatrixService
      service.getDistanceMatrix({
            //Origem
            origins: [$("#address"+anterior).val()],
            //Destino
            destinations: [$("#address"+indice).val()],
            //Modo (DRIVING | WALKING | BICYCLING)
            travelMode: google.maps.TravelMode.DRIVING,
            //Sistema de medida (METRIC | IMPERIAL)
            unitSystem: google.maps.UnitSystem.METRIC
            //Vai chamar o callback
        }, callback);

      } else {
        
        console.log('Primeiro');

      }

    }
        //Tratar o retorno do DistanceMatrixService
        function callback(response, status) {
            //Verificar o Status
            if (status != google.maps.DistanceMatrixStatus.OK) {
                //Se o status não for "OK"
            } else {
                //Se o status for OK
                //Endereço de origem = response.originAddresses
                //Endereço de destino = response.destinationAddresses
                //Distância = response.rows[0].elements[0].distance.text
                //Duração = response.rows[0].elements[0].duration.text

                km[(indice-2)] = parseInt(response.rows[0].elements[0].distance.value);
                tempo[(indice-2)] = parseInt(response.rows[0].elements[0].duration.value);

                //Levado para o banco
                $('#km_google'+indice).val(response.rows[0].elements[0].distance.value);
                $('#tempoGoogle'+indice).val(response.rows[0].elements[0].duration.value);

                console.log("<strong>Origem</strong>: " + response.originAddresses +
                    "<br /><strong>Destino:</strong> " + response.destinationAddresses +
                    "<br /><strong>Distância</strong>: " + response.rows[0].elements[0].distance.value +
                    " <br /><strong>Duração</strong>: " + response.rows[0].elements[0].duration.value
                    );

                calcularEPreencher();

            }
        } 

      function calcularEPreencher(){

        var horas_total = 0;
        var km_total = 0;

        for (var i = km.length - 1; i >= 0; i--) {
          km_total += km[i];
        }

        for (var i = tempo.length - 1; i >= 0; i--) {
          horas_total += tempo[i];
        }

        var horas_avulsas = 0; //Digitados avulso

        //Somando os tempos digitados avulsos (em minutos)
        $('.tempo').each(function(){
          if ($(this).val() != '') {
            horas_avulsas += parseInt($(this).val());  
          }
        });

        var horas_final = Math.round(((horas_total / 60) + horas_avulsas));
        var km_final = Math.round((km_total / 1000));

        if((Math.floor(horas_final/60)) > 0) { // Mais de uma hora
          if ((Math.floor(horas_final%60)) > 0) { // Mais de um minuto
            console.log(Math.floor(horas_final/60) + " Horas e " + Math.floor(horas_final%60) + " Minutos");
            $('#horas_total').val(Math.floor(horas_final/60) + " Horas e " + Math.floor(horas_final%60) + " Minutos");
          } else {
            console.log(Math.floor(horas_final/60) + " Horas");
            $('#horas_total').val(Math.floor(horas_final/60) + " Horas");
          }
          
        } else {
          console.log(Math.floor(horas_final%60) + " Minutos");
          $('#horas_total').val(Math.floor(horas_final%60) + " Minutos");
        }

        console.log(km_final + " KM");

        $('#km_total').val(km_final + " KM");

      }

      $(document).on('change','.tempo',function(){
        calcularEPreencher();
      });

    //Preenche o campo com o valor
    var geocoder;
    var input
    var autocomplete;
    var address;
    var endereco;

    $(document).on('keyup','.address',function(){

        geocoder = new google.maps.Geocoder();

      //console.log($(this).attr('cod'));

      input = document.getElementById("address"+$(this).attr('cod'));
      autocomplete = new google.maps.places.Autocomplete(input);
      address = document.getElementById("address"+$(this).attr('cod')).value;
      geocoder.geocode( { 'address': address }, function(r, s) {
        if (s === google.maps.GeocoderStatus.OK) {
          endereco = r[0].formatted_address;
          //console.log(endereco);
        } else {
          endereco = "Status: "+s;
        }
      });


    });


    $('#enviar').click(function(e){
      // Quando chegar a 5 pode enviar
      if (lista < 5) { e.preventDefault(); } 
    });

});

  </script>

  <?php echo form_open('Controller_webservice/gravar_roteiro'); ?>


  <div class="container">

  <input type="hidden" name="id_usuario" id="id_usuario" value="<?php echo $id; ?>">

    <div class="row">
      <div class="col-xs-6">
        <label>Inicio</label>
        <input class="form-control mascara_data"  type="text" name="inicio">
      </div>
      <div class="col-xs-6">
        <label>Fim</label>
        <input class="form-control mascara_data" type="text" name="fim">
      </div>
    </div>

    <hr>

    <div id="lista">

    <div class="novo">
      <div class="row">
        <div class="col-xs-1 numerador" align="center">1</div>
         <div class="col-xs-11">
          <div class="input-group">
            <input class="form-control address" cod="1" id="address1" name="address[]" type="search" placeholder="BUSCAR ENDEREÇO">
            <span class="input-group-addon">
              <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
            </span>
          </div>
        </div><!-- /.col-xs-11 -->
      </div>  

      <div class="row">
        <div class="col-xs-1 espaco"></div>
          <div class="col-xs-11">
          <div class="input-group">
            <input type="hidden" name="tempoGoogle[]" id="tempoGoogle1">
            <input type="hidden" name="km_google[]" id="km_google1" value="0">
            <input class="form-control tempo" name="tempo[]" placeholder="TEMPO (Minutos)" type="tel">
            <span class="input-group-addon">
              <span class="glyphicon glyphicon-time" aria-hidden="true"></span>
            </span>
            </div> 
          </div>
      </div>
    </div>

    </div>

  </div>

  <footer class="footer">
  <div class="container">

    <div class="row" align="center">
      <div class="col-xs-10">
      </div>
      <div class="col-xs-2 plus" id="adicionar">
        +
      </div>
    </div>

  <hr>

   <div class="row">
      <div class="col-xs-12">
        <label>Observação</label>
        <textarea class="form-control" name="obs" id="obs"></textarea>
      </div>
    </div>

    <div class="row" align="center" hidden>
      <div class="col-xs-6">
        <input class="form-control left" name="horas_total" id="horas_total" placeholder="TOTAL HORAS" readonly="readonly">
      </div>
      <div class="col-xs-6">
        <input class="form-control right" name="km_total" id="km_total" placeholder="TOTAL KM" readonly="readonly">
      </div>
    </div>

    <br>  

    <div class="row">
      <div class="col-xs-12">
        <button type="submit" class="btn btn-success" style="width: 100%; margin-bottom: 5px;" id="enviar"  data-container="body" data-toggle="popover" data-placement="top" data-content="Mínimo 5 Rotas">ENVIAR PARA APROVAÇÃO</button>
      </div>
    </div>
    
  </div>
</footer>

<?php echo form_close(); ?>

  </body>
</html>