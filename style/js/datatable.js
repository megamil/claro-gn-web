$(document).ready( function () {

	if(!$('table').hasClass('semDataTable')){

		start_DataTable();
		console.log('adicionar dataTable');

	} else {
		console.log('Não adicionar dataTable');
	}

	function start_DataTable(){
		// Adicionando um campos com input para as buscas indivíduais.
	    $('table thead th').each( function () {
	        var title = $(this).text();
	        if (title != '') {
	       		$(this).html('<input type="text" class="mdl-textfield__input" placeholder="'+title+'" alt="'+title+'" title="'+title+'"/>' ); 	
	        }
	    } );

	    var table = $('table').DataTable({
	    	"oLanguage": {
	            "sLengthMenu": "Mostrar _MENU_ registros por página",
	            "sZeroRecords": "Nenhum registro encontrado",
	            "sInfo": "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
	            "sInfoEmpty": "Mostrando 0 / 0 de 0 registros",
	            "sInfoFiltered": "(filtrado de _MAX_ registros)",
	            "sSearch": "Pesquisa Geral: ",
	            "oPaginate": {
	                "sFirst": "Início",
	                "sPrevious": "Anterior",
	                "sNext": "Próximo",
	                "sLast": "Último"
	            }
	        }
	    });
				 
	 
		    //Aplicando a busca nos campos
		    table.columns().every( function () {
		        var that = this;
		 
		        $( 'input', this.header() ).on( 'keyup change', function () {
		            if ( that.search() !== this.value ) {
		                that
		                    .search( this.value )
		                    .draw();
		            }
		        } );
		    });
	}

});